package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ControlSampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import spock.lang.Specification

class SampleReceiptPPVUnitTestSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test checkInputSize"() {
        setup:
        List<Analyte> analytes = []
        ActionHandler actionHandler = new SampleReceiptPPV()
        when:
        analytes << Mock(Analyte)
        then:
        assert actionHandler.checkInputSize(analytes) == ''
        when:
        analytes << Mock(Analyte)
        then:
        assert actionHandler.checkInputSize(analytes) != ''
    }

    void "test checkIsControlSample"() {
        setup:
        Analyte analyte = null
        ActionHandler actionHandler = new SampleReceiptPPV()
        when:
        analyte = Mock(Analyte)
        then:
        assert actionHandler.checkIsControlSample(analyte) != ''
        when:
        analyte = Mock(ControlSampleAnalyte)
        then:
        assert actionHandler.checkIsControlSample(analyte) == ''
    }
}