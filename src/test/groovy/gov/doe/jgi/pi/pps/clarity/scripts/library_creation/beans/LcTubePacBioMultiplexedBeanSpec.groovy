package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubePacBioMultiplexedBean
import spock.lang.Shared
import spock.lang.Specification

class LcTubePacBioMultiplexedBeanSpec extends Specification {

    @Shared
    String correctIndex = 'IT049'//'dummyIndex'

    def setup() {
    }

    def cleanup() {
    }

    void "test validate PacBioMultiplexed"(indexName, status, failureMode, volume, concentration, actualTemplateSize) {
        setup:
            def errorMsg
        LcTubePacBioMultiplexedBean bean = new LcTubePacBioMultiplexedBean(
                    libraryLimsId: 'libraryLimsId',
                    libraryCreationQueue: 'LC PacBio Multiplexed 2kb Amplicon, Tubes'
            )
            bean.libraryQcResult = new DropDownList(value:status)
            bean.libraryQcFailureMode = new DropDownList(value:failureMode)
            bean.libraryVolume = volume
            bean.libraryConcentration = concentration
            bean.libraryActualTemplateSize= actualTemplateSize
            bean.libraryIndexName = indexName
        when:
        errorMsg = bean.validate()
        then:
        //println "$bean.libraryIndexName ${bean.libraryQcResult?.value}  ${bean.libraryQcFailureMode?.value} ${bean.libraryVolume}   $bean.libraryConcentration  $bean.libraryActualTemplateSize"
        //println errorMsg
        errorMsg
        where:
        indexName   |status       | failureMode      | volume |concentration |actualTemplateSize
        correctIndex| Analyte.PASS | null              | null  |null    |null
        correctIndex| Analyte.PASS | null              | 0.0   |0.0     |0.0
        correctIndex| Analyte.PASS | null              | 27.27 |123.123 |null
        correctIndex| Analyte.PASS | null              | 27.27 |null    |123.123
        correctIndex|null         | null             | 27.27  |123.123       |123.123
        correctIndex|'dummy'      | null             | 27.27  |123.123       |123.123
        correctIndex| Analyte.FAIL | null              | 27.27 |123.123 |123.123
        correctIndex| Analyte.PASS | 'testFailureMode' | 27.27 |123.123 |123.123
        ''          | Analyte.PASS | null              | 27.27 |123.123 |123.123
        null        | Analyte.PASS | null              | 27.27 |123.123 |123.123
    }

    void "test validate no exception thrown PacBioMultiplexed"(
            indexName, status, failureMode, volume, concentration, actualTemplateSize
    ) {
        setup:
        LcTubePacBioMultiplexedBean bean = new LcTubePacBioMultiplexedBean(
                libraryLimsId: 'libraryLimsId',
                libraryCreationQueue: 'LC PacBio Multiplexed 2kb Amplicon, Tubes'
        )
        bean.libraryQcResult = new DropDownList(value:status)
        bean.libraryQcFailureMode = new DropDownList(value:failureMode)
        bean.libraryVolume = volume
        bean.libraryConcentration = concentration
        bean.libraryActualTemplateSize= actualTemplateSize
        bean.libraryIndexName = indexName
        when:
        def errorMsg = bean.validate()
        then:
        assert errorMsg == null
        where:
        indexName   |status        | failureMode       | volume              |concentration      |actualTemplateSize
        correctIndex| Analyte.PASS | null              | 27.27 |123.123 |123.123
        ''          | Analyte.FAIL | 'testFailureMode' | 27.27 |123.123 |123.123
        null        | Analyte.FAIL | 'testFailureMode' | 0     |0       |0
        ''          | Analyte.FAIL | 'testFailureMode' | null  |null    |null
    }

}

