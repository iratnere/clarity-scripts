package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.FractionsTableBean
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class SampleFractionationProcessUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validateBeans instrument limit"() {
        setup:
        DropDownList listPassed = new DropDownList(
                controlledVocabulary: Analyte.PASS_FAIL_LIST,
                value: Analyte.PASS)
        List<FractionsTableBean> beans = (1..193).collect {
            FractionsTableBean bean = new FractionsTableBean(
                    sourceStatus: listPassed,
                    destinationBarcode: "barcode_$it",
                    transferredVolumeUl: it as BigDecimal
            )
            bean
        }
        when:
        SampleFractionationProcess.validateBeans(beans)
        then:
        def e = thrown(WebException)
        e.message.contains('Invalid number of passed fraction: 193.')
        e.message.contains('The instrument cannot support more than 192 tubes.')
        when:
        beans.remove(0)
        SampleFractionationProcess.validateBeans(beans)
        then:
        noExceptionThrown()
    }

    void "test validateBeans barcodes are not unique"() {
        setup:
        DropDownList listPassed = new DropDownList(
                controlledVocabulary: Analyte.PASS_FAIL_LIST,
                value: Analyte.PASS)
        List<FractionsTableBean> beans = (1..3).collect {
            FractionsTableBean bean = new FractionsTableBean(
                    sampleLimsId: it*100,
                    sourceStatus: listPassed,
                    destinationBarcode: "barcode_$it",
                    transferredVolumeUl: it as BigDecimal
            )
            bean
        }
        //add the same barcode failed bean
        DropDownList listFailed = new DropDownList(
                controlledVocabulary: Analyte.PASS_FAIL_LIST,
                value: Analyte.FAIL)
        FractionsTableBean bean4 = new FractionsTableBean(
                sampleLimsId: 400,
                sourceStatus: listFailed,
                destinationBarcode: "barcode_1",
                transferredVolumeUl: 12.12
        )
        beans << bean4
        //add the same barcode passed bean
        FractionsTableBean bean5 = new FractionsTableBean(
                sampleLimsId: 500,
                sourceStatus: listPassed,
                destinationBarcode: "barcode_1",
                transferredVolumeUl: 2.12
        )
        beans << bean5
        when:
        SampleFractionationProcess.validateBeans(beans)
        then:
        def e = thrown(WebException)
        e.message.contains('Please correct the errors below and upload spreadsheet again.')
        e.message.contains('Sample Lims Id(500): Destination Barcode<barcode_1> is not unique')
        when:
        beans.remove(4) //last one
        SampleFractionationProcess.validateBeans(beans)
        then:
        noExceptionThrown()
    }
}