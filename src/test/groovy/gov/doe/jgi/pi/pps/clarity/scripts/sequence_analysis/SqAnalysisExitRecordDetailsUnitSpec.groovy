package gov.doe.jgi.pi.pps.clarity.scripts.sequence_analysis

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class SqAnalysisExitRecordDetailsUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validateInputAnalyte"(qcFlag, failureMode) {
        setup:
        SqAnalysisExitRecordDetails actionHandler = mockActionHandler()
        ClarityPhysicalRunUnit physicalRunUnit = Mock(ClarityPhysicalRunUnit)
            physicalRunUnit.systemQcFlag >> qcFlag
            physicalRunUnit.sequencingFailureMode >> failureMode
        when:
            actionHandler.validateInputAnalyte(physicalRunUnit)
        then:
            noExceptionThrown()
        where:
            qcFlag << [true, false]
            failureMode << ['', 'dummyFailureMode']
    }

    void "test validateInputAnalyte error message"(qcFlag, failureMode, errorMsg) {
        setup:
        SqAnalysisExitRecordDetails actionHandler = mockActionHandler()
        ClarityPhysicalRunUnit physicalRunUnit = Mock(ClarityPhysicalRunUnit)
            physicalRunUnit.systemQcFlag >> qcFlag
            physicalRunUnit.sequencingFailureMode >> failureMode
        when:
            actionHandler.validateInputAnalyte(physicalRunUnit)
        then:
            def e = thrown WebException
            e.message.contains(errorMsg)
        where:
            qcFlag << [true, false]
            failureMode << ['dummyFailureMode', '']
            errorMsg << ["The 'dummyFailureMode' Failure Mode is set to the passed analyte.", "The Failure Mode udf is not set."]
    }

    SqAnalysisExitRecordDetails mockActionHandler() {
        NodeManager nodeManager = Mock(NodeManager)
        def id = generateDummyId(ArtifactNode.class.simpleName)
        def outputId = generateDummyId(ArtifactNode.class.simpleName)
        SqAnalysisExitRecordDetails actionHandler = Spy(SqAnalysisExitRecordDetails)
        ProcessNode processNode = Mock(ProcessNode)
        processNode.inputArtifactIds >> [id]
        processNode.outputAnalyteIds >> [outputId]
        processNode.nodeManager >> nodeManager

        SequenceAnalysis process = new SequenceAnalysis(processNode)//Mock(SizeSelectionProcess, constructorArgs:[processNode])
        actionHandler.process >> process
        actionHandler
    }

    ArtifactNode generateMockArtifactNode(id) {
        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

}