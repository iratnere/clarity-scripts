package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.MiSeqSampleSheetBean
import spock.lang.Specification

class MiSeqUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test isDualIndex"() {
        when:
            def indexSequences = ['TCGA', 'ATAT']
        then:
        MiSeq.isDualIndex(indexSequences) == Boolean.FALSE
        when:
            indexSequences = ['TCGA', 'TCGA-ATAT','ATAT']
        then:
        MiSeq.isDualIndex(indexSequences) == Boolean.TRUE
    }

    void "test getMiSeqSampleSheetBeans"() {
        setup:
            def libraryName = 'AAAAA'
            def indexSequence = 'TCGATCGA'
            def libraryStockMap = getLibraryStockMap(libraryName, indexSequence)
        when:
            List<MiSeqSampleSheetBean> beans = MiSeq.getMiSeqSampleSheetBeans(libraryStockMap)
        then:
            beans
            beans[0].sampleId == libraryName
            beans[0].index == indexSequence
            beans[0].indexTwo == null
        when:
            indexSequence = 'TCGA-ATAT'
            libraryStockMap = getLibraryStockMap(libraryName, indexSequence)
            beans = MiSeq.getMiSeqSampleSheetBeans(libraryStockMap)
        then:
            beans
            beans[0].sampleId == libraryName
            beans[0].index == 'TCGA'
            beans[0].indexTwo == 'ATAT'
        when:
            indexSequence = ''
            libraryStockMap = getLibraryStockMap(libraryName, indexSequence)
            beans = MiSeq.getMiSeqSampleSheetBeans(libraryStockMap)
        then:
            beans[0].sampleId == libraryName
            beans[0].index == null
            beans[0].indexTwo == null
    }

    Map<ClarityLibraryStock, String> getLibraryStockMap(def libraryName, def sequence){
        Map<ClarityLibraryStock, String> libraryStocks = [:]
        ClarityLibraryStock ls = Mock(ClarityLibraryStock)
        ls.name >> libraryName
        libraryStocks[ls] = sequence
        libraryStocks
    }

}