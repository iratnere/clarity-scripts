package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerClass
import spock.lang.Ignore
import spock.lang.Specification

class LibraryQuantificationRoutingRulesUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void 'test for input analyte in TUBE if analyte passed qPCR assign analyte to pooling workflow(DOP>1) if not (iTag or Exome)'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 3)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof PoolCreationStageAction
    }

    @Ignore //Ignoring tests related to exome capture
    void 'test for input analyte in TUBE if analyte passed qPCR assign analyte to pooling workflow(DOP>12) if Exome'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, new Random().nextBoolean())
        state.setAttribute(AttributeKeys.EXOME, Boolean.TRUE)
        state.setAttribute(AttributeKeys.DOP, new Random().nextInt(Integer.MAX_VALUE) + 13)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof PoolCreationStageAction
    }

    @Ignore //Ignoring tests related to ITag
    void 'test for input analyte in TUBE if analyte passed qPCR assign analyte to pooling workflow(DOP=184) if iTag'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, Boolean.TRUE)
        state.setAttribute(AttributeKeys.EXOME, new Random().nextBoolean())
        state.setAttribute(AttributeKeys.DOP, 184)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof PoolCreationStageAction
    }

    void 'test for input analyte in TUBE if analyte passed qPCR assign analyte sequencing workflow(DOP=1) if not (iTag or Exome)'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof FindSequencingStageAction
    }

    @Ignore //Ignoring tests related to exome capture
    void 'test for input analyte in TUBE if analyte passed qPCR assign analyte sequencing workflow(DOP<=12) if Exome'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, new Random().nextBoolean())
        state.setAttribute(AttributeKeys.EXOME, Boolean.TRUE)
        state.setAttribute(AttributeKeys.DOP, new Random().nextInt(13))
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof FindSequencingStageAction
    }

    void 'test for input analyte in TUBE parent sample EXTERNAL if analyte failed qPCR no requeue'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.TRUE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }

    void 'test for input analyte in TUBE if analyte failed qPCR then Abandon only library'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }

    void 'test for input analyte in TUBE if analyte failed qPCR then assign the analyte to the “Abandoned” workflow if first library creation attempt'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonStageAction
    }

    void 'test for input analyte in TUBE if analyte failed qPCR then abandon and requeue if second library creation attempt or greater'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, new Random().nextInt(Integer.MAX_VALUE) + 2)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonAndRequeueStageAction
    }

    void 'test for input analyte in TUBE if analyte “Requeue” then assign the analyte to the “Library qPCR” workflow'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof LibraryQpcrStageAction
    }

    void 'test for not Single Cell Internal input analyte on PLATE,plate is Done,DOP > 0 and analyte passed qPCR'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof PoolCreationStageAction
    }

    void 'test for Single Cell Internal Nova Seq input analyte pool if analyte passed qPCR then assign analyte to pooling workflow if DOP > 0'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.TRUE)
        state.setAttribute(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SEQUENCER_MODEL, "NovaSeq")
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof PoolCreationStageAction
    }

    void 'test for Single Cell Internal non Nova Seq input analyte pool if analyte passed qPCR then assign analyte to appropriate sequencing workflow if DOP > 0'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.TRUE)
        state.setAttribute(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, '')
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SEQUENCER_MODEL, "NextSeq")
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof FindSequencingStageAction
    }

    void 'test for input analyte on PLATE,plate is Done,LCA=1 and analyte failed qPCR'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }

    void 'test for input analyte on PLATE,plate is Done,LCA>0,External and analyte failed qPCR'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.TRUE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }

    void 'test for input analyte on PLATE,plate is Done,LCA=2 and analyte failed qPCR'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonStageAction
    }

    void 'test for input analyte on PLATE,plate is Done,LCA>2 and analyte failed qPCR'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 3)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonAndRequeueStageAction
    }

    void 'test for input analyte on PLATE if input plate passed qPCR then analyte “Requeue” is not accepted'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.PASS, AttributeValues.FAIL])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof InvalidStageAction
    }

    void 'test for input analyte on PLATE if input plate failed qPCR then if any analyte is “Requeue” then assign all “Requeue” analytes to the “Library qPCR” workflow'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST,
                [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.REQUEUE])
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof LibraryQpcrStageAction
    }

    void 'test for input analyte on PLATE if input plate failed qPCR then if none of the analytes is “Requeue” and first library creation attempt then assign samples to sample aliquot creation'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST,
                [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.PASS])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonStageAction
    }

    void 'test for input analyte on PLATE if input plate failed qPCR then if none of the analytes is “Requeue” and second attempt or greater then abandon and requeue samples'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST,
                [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.PASS])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, new Random().nextInt(Integer.MAX_VALUE) + 2)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonAndRequeueStageAction
    }

    void 'test for input analyte on PLATE if input plate failed qPCR then if none of the analytes is “Requeue” and LCA 1 then Abandon library only'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST,
                [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.PASS])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.FALSE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }

    void 'test for input analyte on PLATE parent sample EXTERNAL if input plate failed qPCR then if none of the analytes is “Requeue” then Abandon library only'() {
        setup:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST,
                [AttributeValues.PASS, AttributeValues.FAIL, AttributeValues.PASS])
        state.setAttribute(AttributeKeys.EXTERNAL, Boolean.TRUE)
        state.setAttribute(AttributeKeys.DOP, 2)
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, Boolean.FALSE)
        state.setAttribute(AttributeKeys.ITAG, Boolean.FALSE)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
        when:
        Action action = routingRules.execute(state)
        then:
        action instanceof AbandonLibrariesOnlyAction
    }
}