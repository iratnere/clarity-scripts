package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.NovaSeqFractionalLanePooling
import org.apache.commons.lang.RandomStringUtils
import spock.lang.Specification

class NovaSeqFractionalLanePoolingUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate"() {
        setup:
            def sequence1 = 'TATTACTCG'
            def sequence2 = 'AATAGAGGC'
            def poolNumber = 1
        LibraryInformation bean1 = getMemberBean(poolNumber, sequence1)
        LibraryInformation bean2 = getMemberBean(poolNumber, sequence2)
            def members = [bean1, bean2]
        NovaSeqFractionalLanePooling pooling = new NovaSeqFractionalLanePooling(members, [:])
        when:
            def message = pooling.validate()
        then:
            !message
    }

    void "test validate duplicate indexes"() {
        setup:
            def sequence1 = 'TATTACTCG'
            def sequence2 = 'AATAGAGGC'
            def poolNumber = 1
        LibraryInformation bean1 = getMemberBean(poolNumber, sequence1)
        LibraryInformation bean2 = getMemberBean(poolNumber, sequence2)
        LibraryInformation bean3 = getMemberBean(poolNumber, sequence1)
        LibraryInformation bean4 = getMemberBean(poolNumber, sequence2)
            def members = [bean1, bean2, bean3, bean4]
        NovaSeqFractionalLanePooling pooling = new NovaSeqFractionalLanePooling(members, [:])
        when:
            def message = pooling.validate()
        then:
            message
            message[Pooling.ERROR]
            message[Pooling.ERROR].first().contains("pool #$poolNumber has duplicate indexes:")
            message[Pooling.ERROR].first().contains("$sequence1:" + [bean1, bean3]*.toErrorMessage())
            message[Pooling.ERROR].first().contains("$sequence2:" + [bean2, bean4]*.toErrorMessage())
    }

    void "test getErrorMessage"() {
        setup:
            def poolNumber = 1
            def sequence1 = 'ATTACTCG'
            def sequence2 = 'ATAGAGGC'
        LibraryInformation bean1 = getMemberBean(poolNumber, sequence1)
        LibraryInformation bean2 = getMemberBean(poolNumber, sequence2)
        LibraryInformation bean3 = getMemberBean(poolNumber, sequence1)
        LibraryInformation bean4 = getMemberBean(poolNumber, sequence2)
        NovaSeqFractionalLanePooling pooling = new NovaSeqFractionalLanePooling([bean1, bean2, bean3, bean4], [:])
            Map<String, List<LibraryInformation>> repSequenceToBeans = pooling.indexMembersMap?.findAll { it.value.size() > 1 }
        when:
            def errorMessage = NovaSeqFractionalLanePooling.getErrorMessage(repSequenceToBeans)
        then:
            errorMessage
            errorMessage.contains("$sequence1:" + [bean1,bean3]*.toErrorMessage())
            errorMessage.contains("$sequence2:" + [bean2,bean4]*.toErrorMessage())
        when:
        NovaSeqFractionalLanePooling pooling1 = new NovaSeqFractionalLanePooling([bean1, bean2], [:])
            Map<String, List<LibraryInformation>> noRepSequenceToBeans = pooling1.indexMembersMap?.findAll { it.value.size() > 1 }
        then:
            !noRepSequenceToBeans
    }

    LibraryInformation getMemberBean(poolNumber, sequence) {
        LibraryInformation bean = new LibraryInformation(indexSequence: sequence, poolNumber: poolNumber)
        def id = RandomStringUtils.random(7, false, true)
        bean.analyteLimsId = "2-$id"
        bean.libraryName = RandomStringUtils.random(5, true, false).toUpperCase()
        bean
    }

}
