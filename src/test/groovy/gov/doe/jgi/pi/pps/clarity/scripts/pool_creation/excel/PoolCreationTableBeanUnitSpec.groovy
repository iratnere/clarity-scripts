package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import spock.lang.Specification

class PoolCreationTableBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validateBean Actual FL"() {
        setup:
        PoolCreationTableBean bean = new PoolCreationTableBean(
                    sampleName: 'ABCD',
                    poolConcentrationpM: 123.123,
                    poolLabProcessResult: new DropDownList(value: Analyte.PASS),
                    runMode: SequencerModelTypeCv.NOVASEQ_S4.value
            )
        when:
            def errorMessage = bean.validateBean()
        then:
            errorMessage?.toString()?.contains('Library Actual Percentage with SOF is a required field for the analyte ABCD.')
        when:
            bean.lpActualWithSof = 0.96
            def errorMessage2 = bean.validateBean()
        then:
            !errorMessage2
    }

}
