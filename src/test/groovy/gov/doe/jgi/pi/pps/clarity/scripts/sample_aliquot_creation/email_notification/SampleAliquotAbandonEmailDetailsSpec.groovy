package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification.SampleAliquotAbandonEmailDetails
import spock.lang.Specification

class SampleAliquotAbandonEmailDetailsSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }
    void "test EmailEventsService"(){
        setup:
        SequencingProject sequencingProject = Mock(SequencingProject)
        PmoSample pmoSample = Mock(PmoSample)
        pmoSample.id >> '12345643'
        pmoSample.udfVolumeUl >> 10
        pmoSample.udfConcentration >> 1
        pmoSample.sequencingProject >> sequencingProject
        pmoSample.mass >> (pmoSample.udfVolumeUl * pmoSample.udfConcentration)

        ScheduledSample scheduledSample = Mock(ScheduledSample)
        scheduledSample.sequencingProject >> sequencingProject
        scheduledSample.pmoSample >> pmoSample
        scheduledSample.sowItemId >> 55555
        scheduledSample.udfSowItemType >> 'scheduledSample.udfSowItemType'
        scheduledSample.udfNotes >> 'scheduledSample.udfNotes'

        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
        sampleAnalyte.claritySample >> scheduledSample
        sampleAnalyte.getReagentLabel() >> 'reagentLabel'

        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
        sampleAliquot.claritySample >> scheduledSample
        sampleAliquot.parentPmoSamples >> [pmoSample]
        sampleAliquot.parentAnalyte >> sampleAnalyte
        when:
        SampleAliquotAbandonEmailDetails emailDetails = new SampleAliquotAbandonEmailDetails(sampleAliquot)
        then:
        emailDetails.sowItemId == scheduledSample.sowItemId
        emailDetails.sowItemType == scheduledSample.udfSowItemType
        emailDetails.comments == scheduledSample.udfNotes
        emailDetails.sampleMass == 10
        noExceptionThrown()
    }

}