package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans

import spock.lang.Specification

class DensityTableBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validateEnteredBarcodes"() {
        setup:
        List<String> inputBarcodes = ['353480','353481','353482','353483']
        Set<String> enteredBarcodes
        def errorMsg
        when:
        enteredBarcodes = null
        errorMsg = DensityTableBean.validateEnteredBarcodes(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Please correct the errors below and upload file(s) again.')
        errorMsg.contains('Cannot find the following Sample Barcodes in density files:')
        errorMsg.contains('[353480, 353481, 353482, 353483]')
        when:
        enteredBarcodes = ['353480','353482']
        errorMsg = DensityTableBean.validateEnteredBarcodes(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Please correct the errors below and upload file(s) again.')
        errorMsg.contains('Cannot find the following Sample Barcodes in density files:')
        errorMsg.contains('[353481, 353483]')
        when:
        enteredBarcodes = ['353480','353481','353482','353483']
        errorMsg = DensityTableBean.validateEnteredBarcodes(inputBarcodes, enteredBarcodes)
        then:
        !errorMsg
    }

    void "test validate"() {
        setup:
        List<String> inputBarcodes = ['353480','353481','353482','353483']
        Set<String> enteredBarcodes = ['353480','353482']
        DensityTableBean bean
        def errorMsg
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: 1234.1234
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: unspecified Sample Barcode.')
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: 1234.1234,
                sampleBarcode: '123'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: invalid Sample Barcode 123.')
        errorMsg.contains('Sample Barcode name should be in [353480, 353481, 353482, 353483].')
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: 1234.1234,
                sampleBarcode: '353480'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: invalid Sample Barcode 353480.')
        errorMsg.contains('Sample Barcode name is already used by an existing density file.')
        when:
        bean = new DensityTableBean(
                density: 1234.1234,
                sampleBarcode: '353481'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        !errorMsg
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: 0.00,
                sampleBarcode: '353481'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: invalid Density 0.00')
        errorMsg.contains('Density value should be greater than zero.')
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: -1.0,
                sampleBarcode: '353481'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: invalid Density -1.0')
        errorMsg.contains('Density value should be greater than zero.')
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: null,
                sampleBarcode: '353481'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        errorMsg.contains('Well Pos A5: invalid Density null')
        errorMsg.contains('Density value should be greater than zero.')
        when:
        bean = new DensityTableBean(
                wellPos: 'A5',
                density: 1.0,
                sampleBarcode: '353481'
        )
        errorMsg = bean.validate(inputBarcodes, enteredBarcodes)
        then:
        !errorMsg
    }
}
