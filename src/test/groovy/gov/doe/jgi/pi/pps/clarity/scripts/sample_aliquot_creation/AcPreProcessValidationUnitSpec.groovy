package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class AcPreProcessValidationUnitSpec extends Specification {

    static String PLATE = 'Plate'
    static String TUBE = 'Tube'

    ContainerNode containerNode
    ArtifactNode artifactNode

    def setup() {
    }

    def cleanup() {
    }

    void "test validate Inputs Analyte is not SampleAnalyte"() {
        setup:
        SampleAliquotCreationProcess process = mockProcess(PLATE)
        AcPreProcessValidation actionHandler = Spy(AcPreProcessValidation)
            actionHandler.process >> process
        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
            sampleAliquot.getId() >> 'sampleAliquotId'
        when:
            actionHandler.validateInputAnalyteClass([sampleAliquot])
        then:
            1 * actionHandler.validateInputAnalyteClass([sampleAliquot])
            1 * sampleAliquot.validateIsScheduledSample()
    }

    void "test validate Parent Sample Container Udf CONTAINER_LOCATION_NONE"() {
        setup:
            SampleAliquotCreationProcess process = mockProcess(PLATE, PmoSample.CONTAINER_LOCATION_NONE)
            AcPreProcessValidation actionHandler = Spy(AcPreProcessValidation)
            actionHandler.process >> process
        when:
            actionHandler.validateParentSampleContainerUdf([containerNode])
        then:
            def e = thrown WebException
            e.message.contains('Parent sample container \'null\' has an invalid Location udf: \'None\'. Expected udf: \'On Site\'.')
            1 * actionHandler.validateParentSampleContainerUdf([containerNode])
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

    SampleAliquotCreationProcess mockProcess(String containerType, String containerLocation = '') {
        ProcessNode processNode = Mock(ProcessNode)
        containerNode = Mock(ContainerNode)
        containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION.udf) >> containerLocation
        if (containerType.equals(PLATE)) {
            containerNode.getIsPlate() >> true
            containerNode.containerType >> PLATE
        } else {
            containerNode.getIsPlate() >> false
            containerNode.containerType >> TUBE
        }
        artifactNode = generateMockArtifactNode()
        artifactNode.getContainerNode() >> containerNode
        processNode.inputAnalytes >> [artifactNode]

        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess, constructorArgs:[processNode])
        process
    }

}
