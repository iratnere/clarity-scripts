package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.LpPreProcessValidation
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import spock.lang.Specification

class LpPreProcessValidationSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate input type"() {
        setup:
        LpPreProcessValidation ppv = new LpPreProcessValidation()
        when:
        Analyte analyte = Mock(ClaritySampleAliquot)
        String error = ppv.validateInputType(analyte)
        then:
        assert error
        when:
        analyte = Mock(ClarityLibraryStock)
        error = ppv.validateInputType(analyte)
        then:
        assert !error
        when:
        analyte = Mock(ClarityLibraryPool)
        error = ppv.validateInputType(analyte)
        then:
        assert !error
        when:
        analyte = Mock(ClarityPhysicalRunUnit)
        error = ppv.validateInputType(analyte)
        then:
        assert error
    }

    void "test validate input attributes"() {
        setup:
        LpPreProcessValidation ppv = new LpPreProcessValidation()
        when:
        Analyte analyte = Mock(ClarityLibraryPool)
        analyte.udfIndexName >> ""
        String error = ppv.validateInputAttributes(analyte)
        then:
        assert !error
        when:
        analyte = Mock(ClarityLibraryPool)
        analyte.udfIndexName >> ArtifactIndexService.INDEX_NAME_N_A
        error = ppv.validateInputAttributes(analyte)
        then:
        assert !error
        when:
        analyte = Mock(ClarityLibraryStock)
        analyte.udfIndexName >> ArtifactIndexService.INDEX_NAME_N_A
        error = ppv.validateInputAttributes(analyte)
        then:
        assert error
        when:
        analyte = Mock(ClarityLibraryStock)
        analyte.udfIndexName >> ""
        error = ppv.validateInputAttributes(analyte)
        then:
        assert error
        when:
        analyte = Mock(ClarityLibraryStock)
        analyte.udfIndexName >> "IT090"
        error = ppv.validateInputAttributes(analyte)
        then:
        assert !error
    }
}