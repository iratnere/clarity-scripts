package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity_node_manager.node.FileNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class ProcessDensityFilesUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test process validateFileExt DNA_CONCENTRATION"() {
        setup:
        String placeholder = ProcessDensityFiles.DNA_CONCENTRATION
        def fileNames
        when:
        fileNames = ['495333.xls', '495334.txt','495333.xlsx', '495334', '']
        ProcessDensityFiles.validateFileExt(fileNames, placeholder)
        then:
        def e = thrown(WebException)
        e.message.contains("The following file types are not supported")
        e.message.contains('[495333.xlsx]')
        e.message.contains("Please do not upload the '.xlsx' DNA Concentration file(s)")
        when:
        fileNames = ['495333.txt', '495334','495335.txt']
        ProcessDensityFiles.validateFileExt(fileNames, placeholder)
        then:
        noExceptionThrown()
    }

    void "test process validateFileExt DENSITY"() {
        setup:
        String placeholder = ProcessDensityFiles.DENSITY
        def fileNames
        when:
        fileNames = ['495333.xls', '495334.txt','495333.xlsx', '495334', '']
        ProcessDensityFiles.validateFileExt(fileNames, placeholder)
        then:
        def e = thrown(WebException)
        e.message.contains("The following file types are not supported")
        e.message.contains('[495333.xls, 495334.txt, 495334, ]')
        e.message.contains("Please  upload the '.xlsx' Density file(s)")
        when:
        fileNames = ['495333.xlsx', '495334.xlsx','495335.xlsx']
        ProcessDensityFiles.validateFileExt(fileNames, placeholder)
        then:
        noExceptionThrown()
    }

    void "test process validateFileNames"() {
        setup:
        String placeholder = ProcessDensityFiles.DENSITY
        def fileNames
        def e
        when:
        fileNames = []
        ProcessDensityFiles.validateFileNames(fileNames, placeholder)
        then:
        e = thrown(WebException)
        e.message.contains('Cannot find Density file(s).')
        e.message.contains('Please upload Density file(s).')
        when:
        fileNames = ['27-495333', '27-495334','27-495333', '27-495334','27-495335','27-495336']
        ProcessDensityFiles.validateFileNames(fileNames, placeholder)
        then:
        e = thrown(WebException)
        e.message.contains("The Density file names are not unique:")
        e.message.contains('[27-495333, 27-495334].')
        when:
        fileNames = ['27-495333', '27-495334','27-495335']
        ProcessDensityFiles.validateFileNames(fileNames, placeholder)
        then:
        noExceptionThrown()
    }

    void "test process validateConcentrationFiles"() {
        setup:
        Set<String> containerNames = ['495333','495334']
        def fileNames = ['495333','495334']
        when:
        ProcessDensityFiles.validateUploadedFiles(containerNames, fileNames)
        then:
        noExceptionThrown()
    }

    void "test error validateConcentrationFiles too many files"() {
        setup:
        Set<String> containerNames = ['495333','495334']
        def fileNames = ['495333','495334','92-4366108-2']
        when:
        ProcessDensityFiles.validateUploadedFiles(containerNames, fileNames)
        then:
        def e = thrown(WebException)
        e.message.contains('Invalid uploaded DNA Concentration files.')
        e.message.contains('Please upload 2 concentration file(s) with the following name(s):')
        e.message.contains('[495333, 495334].')
    }

    void "test error validateConcentrationFiles incorrect names"() {
        setup:
        Set<String> containerNames = ['495333','495334']
        def fileNames = ['495333','92-4366108-2']
        when:
        ProcessDensityFiles.validateUploadedFiles(containerNames, fileNames)
        then:
        def e = thrown(WebException)
        e.message.contains('Invalid concentration file name: 92-4366108-2')
        e.message.contains('The concentration file name should match the output container barcode(s):')
        e.message.contains('[495333, 495334].')
    }

    void "test error validateConcentrationFiles no files"() {
        setup:
        Set<String> containerNames = ['495333','495334']
        def fileNames = []
        when:
        ProcessDensityFiles.validateUploadedFiles(containerNames, fileNames)
        then:
        def e = thrown(WebException)
        e.message.contains('Invalid uploaded DNA Concentration files.')
        e.message.contains('Please upload 2 concentration file(s) with the following name(s):')
        e.message.contains('[495333, 495334].')
    }

    void "test process getConcentration"() {
        setup:
        def errorMsg = "ConcFile123: error in row BLK\t\tA6\t\t8\t>10.475\t0\t?????\t?????\t?????"
        def result
        when:
        result = ProcessDensityFiles.getConcentration('', errorMsg)
        then:
        result == BigDecimal.ZERO
        when:
        result = ProcessDensityFiles.getConcentration(null, errorMsg)
        then:
        result == BigDecimal.ZERO
        when:
        result = ProcessDensityFiles.getConcentration('>10.475', errorMsg)
        then:
        result == 10.475
        when:
        result = ProcessDensityFiles.getConcentration('<0.025', errorMsg)
        then:
        result == BigDecimal.ZERO
        when:
        result = ProcessDensityFiles.getConcentration('-11', errorMsg)
        then:
        result == BigDecimal.ZERO
        when:
        result = ProcessDensityFiles.getConcentration('25.34', errorMsg)
        then:
        result == 25.34
        when:
        ProcessDensityFiles.getConcentration('abcdef', errorMsg)
        then:
        def e = thrown(RuntimeException)
        e.message.contains(errorMsg)
        e.message.contains('Invalid concentration: abcdef.')
        e.message.contains('Cannot get a numeric value.')
    }

    void "test fileNameNoExt"() {
        setup:
        String fileName = 'b1_24-1187043'
        FileNode fileNode = Mock(FileNode)
        fileNode.originalLocation >> fileName + '.xlsx'
        when:
        def result = ProcessDensityFiles.fileNameNoExt(fileNode)
        then:
        noExceptionThrown()
        result == fileName
    }
}