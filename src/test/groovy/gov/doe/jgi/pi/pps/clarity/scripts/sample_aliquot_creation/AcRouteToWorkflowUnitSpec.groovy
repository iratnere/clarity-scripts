package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import spock.lang.Specification

class AcRouteToWorkflowUnitSpec extends Specification {

    def testMode = true
    static String PLATE = 'Plate'
    static String TUBE = 'Tube'

    ContainerNode containerNode
    ArtifactNode artifactNode

    def setup() {
    }

    def cleanup() {
    }

    void "test aliquotsByLabResult passed PLATE"() {
        setup:
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true
        ContainerNode outputNode = Mock(ContainerNode)
        outputNode.name >> '67894'
        outputNode.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf) >> Analyte.PASS

        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        process.outputPlateContainer >> outputNode
        def outputs = []
        def expectedPassedAliquots = []
        (1..4).each{
            ClaritySampleAliquot sampleAliquot = mockClaritySampleAliquot(false)
            outputs << sampleAliquot
            expectedPassedAliquots << sampleAliquot
        }
        ClaritySampleAliquot customSampleAliquot = mockClaritySampleAliquot(true)
        outputs << customSampleAliquot
        process.outputAnalytes >> outputs
        expect:
        outputs.size() == 5
        expectedPassedAliquots.size() == 4
        outputNode.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf) == Analyte.PASS
        process.outputPlateContainer == outputNode
        process.outputPlateContainer.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf) == outputNode.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf)
        when:
        def analytes = actionHandler.aliquotsByLabResult(Analyte.PASS, outputs)
        def failedAnalytes = actionHandler.aliquotsByLabResult(Analyte.FAIL, outputs)
        then:
        customSampleAliquot.isCustomAliquot
        analytes.each {
            assert it in expectedPassedAliquots
        }
        analytes.size() == 4
        failedAnalytes == null
    }

    void "test aliquotsByLabResult failed PLATE"() {
        setup:
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true
        ContainerNode outputNode = Mock(ContainerNode)
        outputNode.name >> '67894fghj'
        outputNode.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf) >> Analyte.FAIL

        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        process.outputPlateContainer >> outputNode
        def outputs = []
        def expectedFailedAliquots = []
        (1..4).each{
            ClaritySampleAliquot sampleAliquot = mockClaritySampleAliquot(false)
            outputs << sampleAliquot
            expectedFailedAliquots << sampleAliquot
        }
        ClaritySampleAliquot customSampleAliquot = mockClaritySampleAliquot(true)
        outputs << customSampleAliquot
        process.outputAnalytes >> outputs
        expect:
        outputs.size() == 5
        expectedFailedAliquots.size() == 4
        when:
        def analytes = actionHandler.aliquotsByLabResult(Analyte.PASS, outputs)
        def failedAnalytes = actionHandler.aliquotsByLabResult(Analyte.FAIL, outputs)
        then:
        analytes == null
        failedAnalytes.each{
            assert it in expectedFailedAliquots
        }
        failedAnalytes.size() == 4
    }

    void "test aliquotsByLabResult TUBES"() {
        setup:
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true
        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        List<ClaritySampleAliquot> outputs = []
        //def expectedFailedAliquots = []
        (1..4).each{
            ClaritySampleAliquot sampleAliquot = mockClaritySampleAliquot(false)
            outputs << sampleAliquot
        }
        ClaritySampleAliquot customSampleAliquot = mockClaritySampleAliquot(true)
        outputs << customSampleAliquot
        process.outputAnalytes >> outputs

        outputs[0].udfLabResult >> Analyte.PASS
        outputs[1].udfLabResult >> Analyte.PASS
        outputs[2].udfLabResult >> Analyte.FAIL
        outputs[3].udfLabResult >> Analyte.FAIL
        customSampleAliquot.udfLabResult >> Analyte.PASS
        when:
        def analytes = actionHandler.aliquotsByLabResult(Analyte.PASS, outputs)
        def failedAnalytes = actionHandler.aliquotsByLabResult(Analyte.FAIL, outputs)
        then:
        analytes == [outputs[0],outputs[1]]
        failedAnalytes == [outputs[2],outputs[3]]
    }

    void "test aliquotsByLabResult TUBES all passed"() {
        setup:
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true
        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        List<ClaritySampleAliquot> outputs = []
        //def expectedFailedAliquots = []
        (1..4).each{
            ClaritySampleAliquot sampleAliquot = mockClaritySampleAliquot(false)
            outputs << sampleAliquot
        }
        ClaritySampleAliquot customSampleAliquot = mockClaritySampleAliquot(true)
        outputs << customSampleAliquot
        process.outputAnalytes >> outputs

        outputs[0].udfLabResult >> Analyte.PASS
        outputs[1].udfLabResult >> Analyte.PASS
        outputs[2].udfLabResult >> Analyte.PASS
        outputs[3].udfLabResult >> Analyte.PASS
        customSampleAliquot.udfLabResult >> Analyte.PASS
        when:
        def analytes = actionHandler.aliquotsByLabResult(Analyte.PASS, outputs)
        def failedAnalytes = actionHandler.aliquotsByLabResult(Analyte.FAIL, outputs)
        then:
        noExceptionThrown()
        analytes.size() == 4
        [outputs[0],outputs[1],outputs[2],outputs[3]].each {
            assert it in analytes
        }
        failedAnalytes == []
    }

    void "test aliquotsByLabResult TUBES all failed"() {
        setup:
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true
        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        List<ClaritySampleAliquot> outputs = []
        //def expectedFailedAliquots = []
        (1..4).each{
            ClaritySampleAliquot sampleAliquot = mockClaritySampleAliquot(false)
            outputs << sampleAliquot
        }
        ClaritySampleAliquot customSampleAliquot = mockClaritySampleAliquot(true)
        outputs << customSampleAliquot
        process.outputAnalytes >> outputs

        outputs[0].udfLabResult >> Analyte.FAIL
        outputs[1].udfLabResult >> Analyte.FAIL
        outputs[2].udfLabResult >> Analyte.FAIL
        outputs[3].udfLabResult >> Analyte.FAIL
        customSampleAliquot.udfLabResult >> Analyte.FAIL
        when:
        def analytes = actionHandler.aliquotsByLabResult(Analyte.PASS, outputs)
        def failedAnalytes = actionHandler.aliquotsByLabResult(Analyte.FAIL, outputs)
        then:
        failedAnalytes.size() == 4
        [outputs[0],outputs[1],outputs[2],outputs[3]].each {
            assert it in failedAnalytes
        }
        analytes == []
    }

    void "test updateSamplesUdfs"(){
        setup:
        BigDecimal adjustedVolume = 2.4

        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
        process.testMode >> true

        AcRouteToWorkflow actionHandler = Spy(AcRouteToWorkflow)
        actionHandler.process >> process
        def outputs = []

        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
        PmoSample rootSample = Mock(PmoSample)
        rootSample.udfVolumeUl >> 123.23
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
        sampleAliquot.id >> generateDummyId('ClaritySampleAliquot')
        sampleAliquot.parentAnalyte >> sampleAnalyte
        sampleAliquot.parentPmoSamples >> [rootSample]
        sampleAliquot.udfNotes >> 'Notes'
        sampleAliquot.udfVolumeUsedUl >> adjustedVolume
        sampleAnalyte.claritySample >> scheduledSample
        outputs << sampleAliquot
        when:
        actionHandler.updateSamplesUdfs(outputs)
        then:
        noExceptionThrown()
        1 * scheduledSample.setUdfNotes(sampleAliquot.udfNotes)
        1 * rootSample.setUdfVolumeUl(rootSample.udfVolumeUl - sampleAliquot.udfVolumeUsedUl)
    }

    ClaritySampleAliquot mockClaritySampleAliquot(boolean isCustom) {
        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
        PmoSample rootSample = Mock(PmoSample)
        //rootSample.udfVolumeUl >> 123.23
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        ArtifactNode artifactNode = generateMockArtifactNode()
        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
        sampleAliquot.name >> artifactNode.id
        sampleAliquot.isCustomAliquot >> isCustom
        sampleAliquot.artifactNode >> artifactNode
        //sampleAliquot.containerNode >> containerNode
        sampleAliquot.parentAnalyte >> sampleAnalyte
        sampleAliquot.parentPmoSamples >> [rootSample]
        sampleAnalyte.claritySample >> scheduledSample
        sampleAliquot
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

/*
@Ignore
    void "test transferBeansDataToClaritySampleAliquots"() {
        when:
            actionHandler.transferBeansDataToClaritySampleAliquots()
        then:
            noExceptionThrown()
    }
*/
}
