package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class SizeSelectionRecordDetailsUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validateUdfs Exception "() {
        setup:
            String poolName = 'ABCD'
        SizeSelectionRecordDetails actionHandler = mockActionHandler()
        ClarityLibraryPool analyte = Mock(ClarityLibraryPool)
            analyte.name >> poolName
        expect:
            analyte.udfVolumeUl == null
            analyte.udfLibraryMolarityQcPm == null
            analyte.udfConcentrationNgUl == null
            analyte.udfActualTemplateSizeBp == null
        when:
            actionHandler.validateUdfs(analyte)
        then:
            def e = thrown WebException
            e.message.contains("Please correct the errors below Analyte($poolName):")
            e.message.contains("$ClarityUdf.ANALYTE_VOLUME_UL is required")
            e.message.contains("$ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM is required")
            e.message.contains("$ClarityUdf.ANALYTE_CONCENTRATION_NG_UL is required")
            e.message.contains("$ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP is required")
    }

    void "test validateUdfs"(def value) {
        setup:
        SizeSelectionRecordDetails actionHandler = mockActionHandler()
        ClarityLibraryPool analyte = Mock(ClarityLibraryPool)
            analyte.name >> 'ABCD'
            analyte.udfVolumeUl >> (value as BigDecimal)
            analyte.udfLibraryMolarityQcPm >> (value as BigDecimal)
            analyte.udfConcentrationNgUl >> (value as BigDecimal)
            analyte.udfActualTemplateSizeBp >> (value as BigDecimal)
        when:
            actionHandler.validateUdfs(analyte)
        then:
            noExceptionThrown()
        where:
            value << [12.12, 0.0, 0.0]
    }

    SizeSelectionRecordDetails mockActionHandler() {
        NodeManager nodeManager = Mock(NodeManager)
        def id = generateDummyId(ArtifactNode.class.simpleName)
        def outputId = generateDummyId(ArtifactNode.class.simpleName)
        SizeSelectionRecordDetails actionHandler = Spy(SizeSelectionRecordDetails)
        ProcessNode processNode = Mock(ProcessNode)
        processNode.inputArtifactIds >> [id]
        processNode.outputAnalyteIds >> [outputId]
        processNode.nodeManager >> nodeManager

        SizeSelectionProcess process = new SizeSelectionProcess(processNode)//Mock(SizeSelectionProcess, constructorArgs:[processNode])
        actionHandler.process >> process
        actionHandler
    }

    ArtifactNode generateMockArtifactNode(id) {
        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }
}