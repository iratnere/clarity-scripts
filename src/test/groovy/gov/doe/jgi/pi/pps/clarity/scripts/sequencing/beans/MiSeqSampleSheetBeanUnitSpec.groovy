package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans


import spock.lang.Specification

class MiSeqSampleSheetBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test setIndexSequence"() {
        setup:
            String indexSequence
        MiSeqSampleSheetBean dataRow = new MiSeqSampleSheetBean()
        when:
            indexSequence = null
            dataRow.setIndexSequence(indexSequence)
        then:
            !dataRow.index
            !dataRow.indexTwo
        when:
            indexSequence = ''
            dataRow.setIndexSequence(indexSequence)
        then:
            !dataRow.index
            !dataRow.indexTwo
        when:
            indexSequence = 'AAA'
            dataRow.setIndexSequence(indexSequence)
        then:
            dataRow.index == 'AAA'
            !dataRow.indexTwo
        when:
            indexSequence = 'AAA-BBB'
            dataRow.setIndexSequence(indexSequence)
        then:
            dataRow.index == 'AAA'
            dataRow.indexTwo == 'BBB'

    }
}