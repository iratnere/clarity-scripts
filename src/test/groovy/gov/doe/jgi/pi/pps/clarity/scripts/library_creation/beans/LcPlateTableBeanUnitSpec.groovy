package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean
import spock.lang.Specification

class LcPlateTableBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate Illumina"(
            status, volume, concentration, actualTemplateSize,
            molarityQc
    ) {
        setup:
            def errorMsg
        LcPlateTableBean bean = new LcPlateTableBean(
                    libraryLimsId: 'libraryLimsId'
            )
            bean.passed = status == Analyte.PASS
            bean.libraryVolume = volume
            bean.libraryConcentration = concentration
            bean.libraryTemplateSize= actualTemplateSize
            bean.libraryMolarity = molarityQc
        when:
            errorMsg = bean.validate()
        then:
            errorMsg
        where:
        status        | volume              |concentration      |actualTemplateSize  |molarityQc
        Analyte.PASS | null |null |null | null
        Analyte.PASS | 0    |0    |0    | 0

    }

    void "test validate no exception thrown Illumina"(
            indexName, status, volume, concentration, actualTemplateSize,
            molarityQc
    ) {
        setup:
        LcPlateTableBean bean = new LcPlateTableBean(
                    libraryLimsId: 'libraryLimsId'
            )
            bean.passed = status == Analyte.PASS
            bean.libraryVolume = volume
            bean.libraryConcentration = concentration
            bean.libraryTemplateSize= actualTemplateSize
            bean.libraryIndexName = indexName
            bean.libraryMolarity = molarityQc
        when:
            def errorMsg = bean.validate()
        then:
            assert errorMsg == null
        where:
        indexName   |status        | volume   |concentration      |actualTemplateSize  |molarityQc
        ''          | Analyte.PASS |  27.27 |123.123 |123.123 | 27.27
        'n/a'       | Analyte.PASS |  27.27 |123.123 |123.123 | 27.27
        ''          | Analyte.FAIL |  27.27 |123.123 |123.123 | 27.27
        ''          | Analyte.FAIL |  0     |0       |0       | 0
        ''          | Analyte.FAIL |  null  |null    |null    | null
    }

}