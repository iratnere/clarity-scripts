package gov.doe.jgi.pi.pps.scriptexecution

import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class ScriptParamsSpec extends Specification {


    static final Logger log = LoggerFactory.getLogger(ScriptParamsSpec.class)



    //void setup() {
        //Jackson2ObjectMapperBuilder jacksonBuilder = new Jackson2ObjectMapperBuilder()
        //objectMapper = jacksonBuilder.build()
        //objectMapper = new ObjectMapper()
//        SimpleModule module =
//                new SimpleModule("CustomScriptOptionsDeserializer", new Version(1, 0, 0, null, null, null));
//        module.addDeserializer(ScriptOptions.class, new CustomScriptOptionsDeserializer());
//
//        objectMapper.registerModule(module);
        //jacksonBuilder.propertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE)
        //jacksonBuilder.propertyNamingStrategy = new LowerCaseWithHyphensStrategy()

    //}

    void "serialize process execution controller payload"() {
        given:
        def content = """
{
  "options": {
    "P": "24-994552",
    "A": "ApproveForShippingPPV",
    "S": "http://localhost:9080/api/v2/processes/24-994552",
    "U": "APIUser",
    "L": []
  },
  "arguments": []
}
"""
        when:
        ScriptParams processParams = new ObjectMapper().readValue(content, ScriptParams.class)

        then:
        processParams
        processParams.validate()
        log.info "${processParams.toString()}"
    }

    void "serialize ScriptOptions"() {
        given:
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module =
                new SimpleModule("CustomScriptOptionsDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(ScriptOptions.class, new CustomScriptOptionsDeserializer());

        mapper.registerModule(module);
        def content = """
{
    "P": "24-994552",
    "A": "ApproveForShippingPPV",
    "S": "http://localhost:9080/api/v2/processes/24-994552",
    "U": "APIUser",
    "L": []
  }
"""
        when:
        ScriptOptions scriptOptions = mapper.readValue(content, ScriptOptions.class)

        then:
        scriptOptions
        log.info "${scriptOptions.toString()}"
    }

}