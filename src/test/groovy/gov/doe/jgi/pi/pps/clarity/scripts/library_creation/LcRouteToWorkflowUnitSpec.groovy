package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcKeyValueResultsBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import spock.lang.Specification

class LcRouteToWorkflowUnitSpec extends Specification{

    static final def notes = 'dummy notes'

    def setup() {
    }

    def cleanup() {
    }

    void "test updateScheduledSampleNotes"(boolean isOnPlate) {
        setup:
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(scheduledSample)
            clarityLibraryStock.isOnPlate >> isOnPlate

        LibraryCreationProcess process = Spy(LibraryCreationProcess)
            process.testMode >> true
            process.outputAnalytes >> [clarityLibraryStock]
            if (isOnPlate) {
                LcKeyValueResultsBean keyValueResultsBean = new LcKeyValueResultsBean(smNotes: notes)
                process.resultsBean >> keyValueResultsBean
            } else {
                LcTubeBean lcTubeBean = new LcTubeBean(smNotes: notes)
                process.getLcTableBean(clarityLibraryStock.id) >> lcTubeBean
            }
        when:
            process.updateScheduledSampleNotes([clarityLibraryStock])
        then:
            1 * process.updateScheduledSampleNotes([clarityLibraryStock])
            1 * scheduledSample.setUdfNotes(notes)
        where:
            isOnPlate << [false, true]
    }

    ClarityLibraryStock mockClarityLibraryStock(ScheduledSample scheduledSample) {
        ClarityLibraryStock clarityLibraryStock = Mock(ClarityLibraryStock)
        clarityLibraryStock.claritySample >> scheduledSample
        clarityLibraryStock
    }
}