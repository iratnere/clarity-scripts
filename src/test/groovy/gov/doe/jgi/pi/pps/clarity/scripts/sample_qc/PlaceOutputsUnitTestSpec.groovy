package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import spock.lang.Specification

class PlaceOutputsUnitTestSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test build output placements"(){
        setup:
        List<String> outputAnalyteIds = ["analyte1", "analyte2", "analyte3"]
        String containerId = "defaultPlate"
        SampleQcPlaceOutputs placeOutputs = new SampleQcPlaceOutputs()
        when:
        List<OutputPlacement> outputPlacements = placeOutputs.buildOutputPlacements(containerId, outputAnalyteIds)
        then:
        assert outputPlacements[0].artifactId == "analyte1"
        assert outputPlacements[0].containerLocation.wellLocation == "B1"
        assert outputPlacements[1].artifactId == "analyte2"
        assert outputPlacements[1].containerLocation.wellLocation == 'C1'
        assert outputPlacements[2].artifactId == "analyte3"
        assert outputPlacements[2].containerLocation.wellLocation == 'D1'
    }

    void "test validate containers"(List<Integer> sizes, boolean result){
        setup:
        SampleQcValidatePlacements validatePlacements = new SampleQcValidatePlacements()
        when:
        Map<String, Integer> map = [:]
        sizes.eachWithIndex { int size, int index ->
            map["${index+1}"] = size
        }
        String error = validatePlacements.validateContainers(map)
        then:
        assert result?error=="":error!=""
        where:
        sizes                   | result
        [10,92,40]              | true
        [92,10,40]              | false
        [10,92,40,50]           | false
        [10,92,2,92,2,92,2,92]  | false
    }
}