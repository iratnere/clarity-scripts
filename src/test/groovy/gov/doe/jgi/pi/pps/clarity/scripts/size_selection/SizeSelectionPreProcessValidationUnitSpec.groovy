package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class SizeSelectionPreProcessValidationUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test Single Cell validate Input Analyte is not ClarityLibraryPool"() {
        setup:
        SizeSelectionPreProcessValidation actionHandler = mockActionHandler()
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
            libraryStock.getId() >> 'ClarityLibraryStockId'
        when:
            actionHandler.validateInputAnalyteClass([libraryStock])
        then:
            def e = thrown WebException
            e.message.contains('Input Analyte \'ClarityLibraryStockId\' has an invalid analyte class: ')
            e.message.contains('Expected class: \'ClarityLibraryPool\'')
    }

    void "test Single Cell validate Input Analyte is ClarityLibraryPool"() {
        setup:
        SizeSelectionPreProcessValidation actionHandler = mockActionHandler()
        ClarityLibraryPool pool = Mock(ClarityLibraryPool)
            pool.getId() >> 'ClarityLibraryPoolId'
        when:
            actionHandler.validateInputAnalyteClass([pool])
        then:
            noExceptionThrown()
    }

    SizeSelectionPreProcessValidation mockActionHandler() {
        NodeManager nodeManager = Mock(NodeManager)
        def id = generateDummyId(ArtifactNode.class.simpleName)
        def outputId = generateDummyId(ArtifactNode.class.simpleName)
        SizeSelectionPreProcessValidation actionHandler = Spy(SizeSelectionPreProcessValidation)
        ProcessNode processNode = Mock(ProcessNode)
        processNode.inputArtifactIds >> [id]
        processNode.outputAnalyteIds >> [outputId]
        processNode.nodeManager >> nodeManager

        SizeSelectionProcess process = new SizeSelectionProcess(processNode)//Mock(SizeSelectionProcess, constructorArgs:[processNode])
        actionHandler.process >> process
        actionHandler
    }

    ArtifactNode generateMockArtifactNode(id) {
        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }
}
