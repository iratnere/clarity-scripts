package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules.*
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.Action
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerClass
import spock.lang.Specification

class TestRoutingRulesUnitTestSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test routing rules for internal single cell pools"() {
        when:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.PASS)
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.REWORK])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, false)
        state.setAttribute(AttributeKeys.ITAG, false)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, true)
        state.setAttribute(AttributeKeys.EXTERNAL, false)
        state.setAttribute(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class)
        state.setAttribute(AttributeKeys.SEQUENCER_MODEL, "NovaSeq")
        Action action = routingRules.execute(state)
        then:
        assert action instanceof PoolCreationStageAction
        when:
        state.setAttribute(AttributeKeys.DOP, 3)
        action = routingRules.execute(state)
        then:
        assert action instanceof PoolCreationStageAction
        when:
        state.setAttribute(AttributeKeys.SEQUENCER_MODEL, "NextSeq")
        action = routingRules.execute(state)
        then:
        assert action instanceof FindSequencingStageAction
        when:
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.FAIL)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonLibrariesOnlyAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonStageAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 3)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonAndRequeueStageAction
    }

    void "test routing rules for exome pools"() {
        when:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.PASS)
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.REWORK])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, true)
        state.setAttribute(AttributeKeys.ITAG, false)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, false)
        state.setAttribute(AttributeKeys.EXTERNAL, false)
        state.setAttribute(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class)
        Action action = routingRules.execute(state)
        then:
        assert action instanceof FindSequencingStageAction
        when:
        state.setAttribute(AttributeKeys.DOP, 13)
        action = routingRules.execute(state)
        then:
        assert action instanceof PoolCreationStageAction
        when:
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.FAIL)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonLibrariesOnlyAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonStageAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 3)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonAndRequeueStageAction
    }

    void "test routing rules for iTag pools"() {
        when:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.PASS)
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.DOP, 92)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.REWORK])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, false)
        state.setAttribute(AttributeKeys.ITAG, true)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, false)
        state.setAttribute(AttributeKeys.EXTERNAL, false)
        state.setAttribute(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class)
        Action action = routingRules.execute(state)
        then:
        assert action instanceof FindSequencingStageAction
        when:
        state.setAttribute(AttributeKeys.DOP, 184)
        action = routingRules.execute(state)
        then:
        assert action instanceof PoolCreationStageAction
        when:
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.FAIL)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonLibrariesOnlyAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonStageAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 3)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonAndRequeueStageAction
    }

    void "test routing rules for Illumina libraries"() {
        when:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.PASS)
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.REWORK])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, false)
        state.setAttribute(AttributeKeys.ITAG, false)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, false)
        state.setAttribute(AttributeKeys.EXTERNAL, false)
        Action action = routingRules.execute(state)
        then:
        assert action instanceof FindSequencingStageAction
        when:
        state.setAttribute(AttributeKeys.DOP, 184)
        action = routingRules.execute(state)
        then:
        assert action instanceof PoolCreationStageAction
        when:
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.FAIL)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonLibrariesOnlyAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 2)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonStageAction
        when:
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 3)
        action = routingRules.execute(state)
        then:
        assert action instanceof AbandonAndRequeueStageAction
    }

    void "test routing rules for external libraries"() {
        when:
        LibraryQuantificationRoutingRules routingRules = new LibraryQuantificationRoutingRules()
        LibraryQuantificationState state = new LibraryQuantificationState()
        state.setAttribute(AttributeKeys.ANALYTE_STATUS, Analyte.FAIL)
        state.setAttribute(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value)
        state.setAttribute(AttributeKeys.DOP, 1)
        state.setAttribute(AttributeKeys.PLATE_STATUS, AttributeValues.DONE)
        state.setAttribute(AttributeKeys.ANALYTE_STATUS_LIST, [AttributeValues.REWORK])
        state.setAttribute(AttributeKeys.LC_ATTEMPT, 1)
        state.setAttribute(AttributeKeys.EXOME, false)
        state.setAttribute(AttributeKeys.ITAG, false)
        state.setAttribute(AttributeKeys.SINGLE_CELL_INTERNAL, false)
        state.setAttribute(AttributeKeys.EXTERNAL, true)
        Action action = routingRules.execute(state)
        then:
        assert action instanceof AbandonLibrariesOnlyAction
    }
}