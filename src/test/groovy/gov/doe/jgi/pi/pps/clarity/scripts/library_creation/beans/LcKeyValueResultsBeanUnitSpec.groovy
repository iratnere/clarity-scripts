package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcKeyValueResultsBean
import spock.lang.Specification

class LcKeyValueResultsBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test getIndexContainerBarcode"() {
        setup:
            String barcode = 'IndexContainerBarcode'
            String dropDownValue = 'libIndexSet'
            String result
        LcKeyValueResultsBean bean = new LcKeyValueResultsBean(
                    libraryIndexSet: barcode
            )
        when:
            bean.libraryIndexSet = null
            result = bean.getIndexContainerBarcode()
        then:
            result == null
        when:
            bean.libraryIndexSet = new DropDownList(value: dropDownValue)
            result = bean.getIndexContainerBarcode()
        then:
            result == dropDownValue
        when:
            bean.libraryIndexSet = new DropDownList()
            result = bean.getIndexContainerBarcode()
        then:
            result == null
        when:
            bean.libraryIndexSet = new DropDownList(value: '')
            result = bean.getIndexContainerBarcode()
        then:
            result == ''
    }

    void "test validate no exception thrown Illumina"(indexName, status, fmValue, numberPcrCycles) {
        setup:
            def errorMsg
        LcKeyValueResultsBean bean = new LcKeyValueResultsBean()
            bean.plateResult = new DropDownList(value:status)
            bean.failureMode = new DropDownList(value:fmValue)
            bean.libraryIndexSet = new DropDownList(value:indexName)
            bean.numberPcrCycles= numberPcrCycles
        when:
            errorMsg = bean.validate()
        then:
            errorMsg == null
        where:
        indexName       |status        | fmValue            |numberPcrCycles
        '1-04182018-01' | Analyte.PASS | ''               |1
        '1-04182018-01' | Analyte.PASS | ''               |0
        '1-04182018-01' | Analyte.FAIL | 'Sample Problem' |1
        '1-04182018-01' | Analyte.FAIL | 'Sample Problem' |-1
        '1-04182018-01' | Analyte.FAIL | 'Sample Problem' |null
        ''              | Analyte.PASS | ''               |1
        ''              | Analyte.FAIL | 'Sample Problem' |1
    }

    void "test validate exception thrown Illumina"(indexName, status, fmValue, BigDecimal numberPcrCycles, exception) {
        setup:
            def errorMsg
        LcKeyValueResultsBean bean = new LcKeyValueResultsBean()
            bean.plateResult = new DropDownList(value:status)
            bean.failureMode = new DropDownList(value:fmValue)
            bean.libraryIndexSet = new DropDownList(value:indexName)
            bean.numberPcrCycles= numberPcrCycles
        when:
            errorMsg = bean.validate()
        then:
            errorMsg
            errorMsg.contains(LibraryCreationProcess.START_ERROR_MSG)
            errorMsg.contains('Plate Results Section:')
            errorMsg.contains(exception)
        where:
        indexName       |status        | fmValue            |numberPcrCycles    |exception
        '1-04182018-01' |'dummy'       | ''                 |1                  |"invalid PASS or FAIL? 'dummy'"
        '1-04182018-01' | Analyte.PASS | 'Sample Problem' |0    |"incorrect failure mode 'Sample Problem' or status 'Pass'"
        '1-04182018-01' | Analyte.FAIL | ''               |1    |"unspecified Failure Mode "
        '1-04182018-01' | Analyte.PASS | ''               |null |"unspecified Number of PCR Cycles"
        '1-04182018-01' | Analyte.PASS | ''               |-1   |"unspecified Number of PCR Cycles"
    }
}