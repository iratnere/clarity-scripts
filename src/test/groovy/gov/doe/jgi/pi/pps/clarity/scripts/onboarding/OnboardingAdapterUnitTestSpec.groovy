package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.PoolDetailsBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.HudsonAlphaIlluminaOnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryPoolsAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryStocksAdapter
import spock.lang.Specification

class OnboardingAdapterUnitTestSpec extends Specification {
    OnboardingAdapter adapter
    String libraryOnboardTemplateFile = "resources/templates/excel/LibraryOnboardingTemplate.xls"
    String hudsonAlphaTemplateFile = "resources/templates/excel/HudsonAlphaSequencingFile.xls"

    def setup() {

    }

    def cleanup() {
    }

    def "test Libary stocks adapter Check Missing Fields"() {
        setup:
        adapter = new OnboardingLibraryStocksAdapter(new ExcelWorkbook(libraryOnboardTemplateFile), 12)
        OnboardingBean bean = new OnboardingBean()
        List<String> errors
        when:
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 12
        when:
        bean.sowRunMode = "Illumina NovaSeq S2 2 X 150"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 11
        when:
        bean.sampleName = "LakshmiSample"
        bean.dop = 32
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 9
        when:
        bean.aliquotAmount = 123
        bean.aliquotVolume = 231
        bean.pcrCycles = 1
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 6
        when:
        bean.libFragmentSize = 232
        bean.libVolume = 332
        bean.libMolarityPm = 847
        bean.libConcentration = 863
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 2
        when:
        bean.libraryProtocol = "Protocol"
        bean.libraryCreationSite = "JGI"
        errors = adapter.checkMissingFields([bean])
        then:
        assert !errors
    }

    def "test Library pools  Check Missing Fields"() {
        setup:
        adapter = new OnboardingLibraryPoolsAdapter(new ExcelWorkbook(libraryOnboardTemplateFile), 12)
        OnboardingBean bean = new OnboardingBean()
        List<String> errors
        when:
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 5
        when:
        bean.sowRunMode = "Illumina NovaSeq S2 2 X 150"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 4
        when:
        bean.sampleName = "LakshmiSample"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 3
        when:
        bean.poolNumber = 2
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 2
        when:
        bean.indexName = "my index"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 1
        when:
        bean.libraryCreationSite = "JGI"
        errors = adapter.checkMissingFields([bean])
        then:
        assert !errors
    }

    def "test Library pools validatePools"() {
        setup:
        adapter = new OnboardingLibraryPoolsAdapter(new ExcelWorkbook(libraryOnboardTemplateFile), 12)
        List<OnboardingBean> libBeans = []
        PoolDetailsBean poolBean = new PoolDetailsBean(poolNumber: 2)
        libBeans << new OnboardingBean(poolNumber: 1, indexSequence: "TAGGCATG-TCTCTCCG")
        libBeans << new OnboardingBean(poolNumber: 1, indexSequence: "TAGGCATG-TCGACTAG")
        adapter.poolDetailsBeansCached = [poolBean]
        when:
        List<String> errors = adapter.validatePools(libBeans)
        then:
        assert errors.size() == 1
        when:
        poolBean.poolNumber = 1
        errors = adapter.validatePools(libBeans)
        then:
        assert errors.size() == 5
        when:
        poolBean.poolConcentration = 1.234
        poolBean.poolMolarity = 213.323
        errors = adapter.validatePools(libBeans)
        then:
        assert errors.size() == 3
        when:
        poolBean.poolVolume = 2342.343
        poolBean.poolFragmentSize = 800
        libBeans[0].sowRunMode = "runMode1"
        libBeans[1].sowRunMode = "runMode2"
        errors = adapter.validatePools(libBeans)
        then:
        assert errors.size() == 2
        when:
        libBeans[1].sowRunMode = "runMode1"
        poolBean.collaboratorPoolName = "pool name"
        errors = adapter.validatePools(libBeans)
        then:
        assert !errors
    }

    def "test Illumina Hudson alpha Check Missing Fields"() {
        setup:
        adapter = new HudsonAlphaIlluminaOnboardingAdapter(new ExcelWorkbook(hudsonAlphaTemplateFile), 12)
        HudsonAlphaBean bean = new HudsonAlphaBean()
        List<String> errors
        when:
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 4
        when:
        bean.collaboratorLibraryName = "my lib"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 3
        when:
        bean.sampleName = "my Sample"
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 2
        when:
        bean.libraryCreationSite = 'JGI'
        errors = adapter.checkMissingFields([bean])
        then:
        assert errors.size() == 1
        when:
        bean.indexSequence = "ACGT"
        errors = adapter.checkMissingFields([bean])
        then:
        assert !errors
    }

    def "test Illumina Hudson alpha validate all beans"() {
        setup:
        adapter = new HudsonAlphaIlluminaOnboardingAdapter(new ExcelWorkbook(hudsonAlphaTemplateFile), 12)
        HudsonAlphaBean bean = new HudsonAlphaBean()
        List<String> errors
        when:
        errors = adapter.validateAllBeans([bean])
        then:
        assert errors.size() == 5
        when:
        bean.indexSequence = HudsonAlphaIlluminaOnboardingAdapter.UNDETERMINED
        bean.fastqFileName = HudsonAlphaIlluminaOnboardingAdapter.UNDETERMINED
        errors = adapter.validateAllBeans([bean])
        then:
        assert errors.size() == 2
        when:
        bean.flowcellBarcode = "my Flowcell"
        errors = adapter.validateAllBeans([bean])
        then:
        assert errors.size() == 1
        when:
        bean.laneNumber = 2
        bean.indexSequence = "ACGT"
        bean.fastqFileName = "fastq file name"
        errors = adapter.validateAllBeans([bean])
        then:
        assert errors.size() == 1
        when:
        bean.sampleName = "my sample"
        errors = adapter.validateAllBeans([bean])
        then:
        assert errors.size() == 1
        when:
        bean.collaboratorLibraryName = "my lib"
        errors = adapter.validateAllBeans([bean])
        then:
        assert !errors
    }
}