package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import spock.lang.Specification

class SupplyChainManagementUnitTestSpec extends Specification {
    ActionHandler actionHandler
    def setup() {
    }

    def cleanup() {
    }

    void "test check is sample"() {
        setup:
        actionHandler = new ApproveForShippingPPV()
        when:
        SampleAnalyte analyte1 = Mock(SampleAnalyte)
        PmoSample pmoSample = Mock(PmoSample)
        analyte1.claritySample >> pmoSample
        actionHandler.checkIsSample(analyte1)
        then:
        assert !actionHandler.errorMsg
        when:
        SampleAnalyte analyte2 = Mock(SampleAnalyte)
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        analyte2.claritySample >> scheduledSample
        actionHandler.checkIsSample(analyte2)
        then:
        assert actionHandler.errorMsg
    }

    void "test checkSampleStatus"(){
        setup:
        actionHandler = new ApproveForShippingPPV()
        Random r = new Random()
        Long sampleId1 = r.nextLong()
        Long sampleId2 = r.nextLong()
        when:
        Map map = [:]
        map[sampleId1] = "${SampleStatusCv.AWAITING_SHIPPING_APPROVAL.value}"
        map[sampleId2] = "${SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value}"
        actionHandler.checkSampleStatus(map)
        then:
        assert actionHandler.errorMsg.split("\n").size() == 1
    }

    void "test getSampleStatusMap"(){
        setup:
        actionHandler = new RouteToSampleReceipt()
        Random r = new Random()

        PmoSample pmoSample1 = Mock(PmoSample), pmoSample2 = Mock(PmoSample)
        pmoSample1.pmoSampleId >> r.nextLong()
        pmoSample2.pmoSampleId >> r.nextLong()

        SampleAnalyte analyte1 = Mock(SampleAnalyte), analyte2 = Mock(SampleAnalyte)
        analyte1.claritySample >> pmoSample1
        analyte2.claritySample >> pmoSample2
        when:
        Map map = actionHandler.getSampleStatusMap([analyte1,analyte2])
        def statuses = map.values() as List
        then:
        assert statuses.unique().size() == 1
        assert statuses[0]==SampleStatusCv.AWAITING_SAMPLE_RECEIPT
    }
}