package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import spock.lang.Specification

class MetabolomicsQcUnitTestSpec extends Specification {
    def setup() {
    }

    def cleanup() {
    }

    void "test checkInputSize"() {
        setup:
        MetabolomicsQCPPV actionHandler = new MetabolomicsQCPPV()
        when:
        String error = actionHandler.checkInputSize([Mock(Analyte)])
        then:
        assert !error
        when:
        List<Analyte> analytes = (0..91).collect{Mock(Analyte)}
        error = actionHandler.checkInputSize(analytes)
        then:
        assert !error
        when:
        analytes << Mock(Analyte)
        error = actionHandler.checkInputSize(analytes)
        then:
        assert error
    }

    void "test build output placements"(){
        setup:
        List<String> analyteIds = ["analyte1", "analyte2", "analyte3"]
        String containerId = "outputPlate"
        PlaceOutputs placeOutputs = new PlaceOutputs()
        when:
        List<OutputPlacement> outputPlacements = placeOutputs.getOutputPlacements(analyteIds, containerId)
        then:
        assert outputPlacements[0].artifactId == "analyte1"
        assert outputPlacements[0].containerLocation.wellLocation == "B1"
        assert outputPlacements[1].artifactId == "analyte2"
        assert outputPlacements[1].containerLocation.wellLocation == 'C1'
        assert outputPlacements[2].artifactId == "analyte3"
        assert outputPlacements[2].containerLocation.wellLocation == 'D1'
    }

    void "test getMetabolomicsTableSection"(){
        setup:
        List<Analyte> outputAnalytes = mockAnalytes(2).values() as List
        when:
        List<MetabolomicsTableBean> beans = getMetabolomicsTableSection(outputAnalytes).data
        then:
        assert beans
        assert beans.size() == 2
        assert beans[0].sampleId == 123451
        assert beans[0].plateBarcode == "OutputPlate"
        assert beans[0].wellLocation == "B1"
        assert beans[0].collaboratorSampleName == 'CollabName1'
        assert beans[0].groupName == 'groupName'
        assert beans[0].isotopeLabel == 'IsoLabel1'
        assert beans[1].sampleId == 123452
        assert beans[1].plateBarcode == "OutputPlate"
        assert beans[1].wellLocation == "B2"
        assert beans[1].collaboratorSampleName == 'CollabName2'
        assert beans[1].groupName == 'groupName'
        assert beans[1].isotopeLabel == 'IsoLabel2'
    }

    void "test validateProcessUdfs"(){
        setup:
        ProcessMetabolomicsSheet actionHandler = new ProcessMetabolomicsSheet()
        when:
        MetabolomicsQCProcess process = Mock(MetabolomicsQCProcess)
        process.udfMinimumIsotopeEnrichment >> 0
        process.udfReplicatesFailurePercentAllowed >> 100
        actionHandler.process = process
        String errors = actionHandler.validateProcessUdfs()
        then:
        assert !errors
        when:
        process = Mock(MetabolomicsQCProcess)
        process.udfMinimumIsotopeEnrichment >> 0.6
        process.udfReplicatesFailurePercentAllowed >> 12.8
        actionHandler.process = process
        errors = actionHandler.validateProcessUdfs()
        then:
        assert !errors
        when:
        process = Mock(MetabolomicsQCProcess)
        process.udfMinimumIsotopeEnrichment >> null
        process.udfReplicatesFailurePercentAllowed >> null
        actionHandler.process = process
        errors = actionHandler.validateProcessUdfs()
        then:
        assert errors
    }

    void "test importDataFromMetabolomicsWorksheet"() {
        setup:
        MetabolomicsQCProcess process = mockProcess()
        process.testMode >> true
        Map<Long, SampleQcHamiltonAnalyte> outputMap = mockAnalytes(1)
        SampleQcHamiltonAnalyte outputAnalyte = outputMap.values()[0]
        TableSection section = getMetabolomicsTableSection([outputAnalyte])
        MetabolomicsTableBean bean = section.data[0]
        when:
        process.importDataFromMetabolomicsWorksheet(outputMap, section, 10, 10)
        then:
        noExceptionThrown()
        1 * outputAnalyte.setUdfIsotopeEnrichment(bean.isotopeEnrichment)
        1 * outputAnalyte.setUdfMetabolomicsSampleId(bean.metabolomicsSampleId)
        1 * outputAnalyte.setUdfSampleQCResult(Analyte.FAIL)
        1 * outputAnalyte.setUdfSampleQCFailureMode(MetabolomicsTableBean.SAMPLE_QC_FAILURE_MODE)
        when:
        resetBeans(section.data)
        process.importDataFromMetabolomicsWorksheet(outputMap, section, 10, 10)
        then:
        noExceptionThrown()
        1 * outputAnalyte.setUdfSampleQCResult(Analyte.PASS)
        1 * outputAnalyte.setUdfSampleQCFailureMode('')
    }

    void "test getWarning"() {
        setup:
            String groupName = 'groupName'
        ProcessRecordDetails actionHandler = new ProcessRecordDetails()
        when:
            List<SampleQcHamiltonAnalyte> analytes = []
            (1..5).each{
                SampleQcHamiltonAnalyte analyte = Mock(SampleQcHamiltonAnalyte)
                analyte.passedQc >> true
                analytes << analyte
            }
            String warning = actionHandler.getWarning(groupName, analytes)
        then:
            assert !warning
        when:
        SampleQcHamiltonAnalyte analyte = Mock(SampleQcHamiltonAnalyte)
            analyte.passedQc >> false
            analytes << analyte
            warning = actionHandler.getWarning(groupName, analytes)
        then:
            assert !warning
        when:
            analytes = []
            (1..5).each{
                analyte = Mock(SampleQcHamiltonAnalyte)
                analyte.passedQc >> false
                analytes << analyte
            }
            warning = actionHandler.getWarning(groupName, analytes)
        then:
            assert warning
        when:
            analyte = Mock(SampleQcHamiltonAnalyte)
            analyte.passedQc >> true
            analytes << analyte
            warning = actionHandler.getWarning(groupName, analytes)
        then:
            assert !warning
    }

    MetabolomicsQCProcess mockProcess() {
        ProcessNode processNode = Mock(ProcessNode)
        processNode.nodeManager >> Mock(NodeManager)
        return Spy(MetabolomicsQCProcess, constructorArgs:[processNode])
    }

    void resetBeans(List<MetabolomicsTableBean> beans) {
        beans.each { MetabolomicsTableBean bean->
            bean.metabolomicsSampleId = "${bean.sampleId}"
            bean.isotopeEnrichment = 13
        }
    }

    Section getMetabolomicsTableSection(List<Analyte> analytes) {
        PrepareAliquotCreationMetabolomicsSheets actionHandler = new PrepareAliquotCreationMetabolomicsSheets()
        return actionHandler.getMetabolomicsTableSection(analytes)
    }

    Map<Long, Analyte> mockAnalytes(int size){
        Map<Long, Analyte> analytes = [:]
        for (int i=1; i<= size; i++) {
            Analyte outputAnalyte = Mock(SampleQcHamiltonAnalyte)
            ArtifactNode artifactNode = Mock(ArtifactNode)
            artifactNode.isSample >> true
            Analyte sampleAnalyte = new SampleAnalyte(artifactNode)
            PmoSample pmoSample = Mock(PmoSample)
            Long pmoSampleId = (123450L+i)
            pmoSample.pmoSampleId >> pmoSampleId
            pmoSample.udfCollaboratorSampleName >> "CollabName$i"
            pmoSample.udfGroupName >> 'groupName'
            String isoLabel = "IsoLabel$i"
            if(i%4 == 0)
                isoLabel = "Unlabeled"
            pmoSample.udfIsotopeLabel >> isoLabel
            ContainerNode containerNode = Mock(ContainerNode)
            containerNode.name >> "OutputPlate"
            containerNode.id >> '27-1234544'
            outputAnalyte.containerLocation >> new ContainerLocation(containerNode.id,"B:$i")
            outputAnalyte.parentAnalyte >> sampleAnalyte
            sampleAnalyte.claritySample >> pmoSample
            outputAnalyte.claritySample >> pmoSample
            outputAnalyte.containerNode >> containerNode
            analytes[pmoSampleId] = outputAnalyte
        }
        return analytes
    }
}