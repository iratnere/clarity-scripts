package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreation
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationPlateSingleCellInternal
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationTubeIllumina
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.junit.jupiter.api.Test
import spock.lang.Specification

//@SpringBootTest
class LcPreProcessValidationUnitSpec extends Specification {
//class LcPreProcessValidationUnitSpec extends Specification implements GrailsUnitTest{

    static String PLATE = 'Plate'
    static String TUBE = 'Tube'

    ContainerNode containerNode
    ArtifactNode artifactNode

    def setup() {
    }

    def cleanup() {
    }

    @Test
    void "empty test" () {
        given:
        LibraryCreationProcess process = mockProcess(PLATE)
        LibraryCreation lcAdapter = new LibraryCreationPlateSingleCellInternal(process)
        process.lcAdapter >> lcAdapter
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
        libraryStock.name >> 'ClarityLibraryStockName'
        when:
        process.lcAdapter.validateInputAnalyteClass([libraryStock])
        then:
        def e = thrown WebException
        e.message.contains('Input Analyte \'ClarityLibraryStockName\' has an invalid analyte class: ')
        e.message.contains('Expected class: \'SampleAnalyte\'')
    }


    void "test Single Cell validate Input Analyte is not SampleAnalyte"() {
        setup:
        LibraryCreationProcess process = mockProcess(PLATE)
        LibraryCreation lcAdapter = new LibraryCreationPlateSingleCellInternal(process)
        process.lcAdapter >> lcAdapter
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
        libraryStock.name >> 'ClarityLibraryStockName'
        when:
        process.lcAdapter.validateInputAnalyteClass([libraryStock])
        then:
        def e = thrown WebException
        e.message.contains('Input Analyte \'ClarityLibraryStockName\' has an invalid analyte class: ')
        e.message.contains('Expected class: \'SampleAnalyte\'')
    }

    void "test validate Input Analyte is not ClaritySampleAliquot"() {
        setup:
        LibraryCreationProcess process = mockProcess(PLATE)
        LibraryCreation lcAdapter = new LibraryCreationTubeIllumina(process)
        process.lcAdapter >> lcAdapter
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
        libraryStock.getId() >> 'ClarityLibraryStockId'
        when:
        process.lcAdapter.validateInputAnalyteClass([libraryStock])
        then:
        def e = thrown WebException
        e.message.contains('Input Analyte \'ClarityLibraryStockId\' has an invalid analyte class: ')
        e.message.contains('Expected class: \'ClaritySampleAliquot\'')
    }

    void "test validateInputsContainerType mix of containers"() {
        setup:
        LibraryCreationProcess process = mockProcess(PLATE)
        LibraryCreation lcAdapter = new LibraryCreationTubeIllumina(process)
        process.lcAdapter >> lcAdapter
        ContainerNode containerNodePlate = Mock(ContainerNode)
        containerNodePlate.getIsPlate() >> true
        containerNodePlate.getContainerTypeEnum() >> ContainerTypes.WELL_PLATE_96

        ContainerNode containerNodeTube = Mock(ContainerNode)
        containerNodeTube.getIsPlate() >> false
        containerNodeTube.getContainerTypeEnum() >> ContainerTypes.TUBE
        when:
        process.lcAdapter.validateInputsContainerType([containerNodePlate, containerNodeTube])
        then:
        def e = thrown WebException
        e.message.contains("Inputs can only be in containers of the same type.")
        e.message.contains("Please select only one ${ContainerTypes.WELL_PLATE_96.value} or ${ContainerTypes.TUBE.value}s as containers for inputs.")
    }

    void "test validateInputsContainerType two input plates"() {
        setup:
        LibraryCreationProcess process = mockProcess()
        LibraryCreation lcAdapter = new LibraryCreationTubeIllumina(process)
        process.lcAdapter >> lcAdapter
        ContainerNode containerNodePlate = Mock(ContainerNode)
        containerNodePlate.id >> generateDummyId('ContainerNode')
        containerNodePlate.getIsPlate() >> true
        containerNodePlate.getContainerTypeEnum() >> ContainerTypes.WELL_PLATE_96

        ContainerNode containerNodePlate1 = Mock(ContainerNode)
        containerNodePlate1.id >> generateDummyId('ContainerNode')
        containerNodePlate1.getIsPlate() >> true
        containerNodePlate1.getContainerTypeEnum() >> ContainerTypes.WELL_PLATE_96
        when:
        process.lcAdapter.validateInputsContainerType([containerNodePlate, containerNodePlate1])
        then:
        def e = thrown WebException
        e.message.contains("Process can only run on a single ${ContainerTypes.WELL_PLATE_96.value}.")
        e.message.contains("Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the input.")
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

    LibraryCreationProcess mockProcess(String containerType = PLATE) {
        ProcessNode processNode = Mock(ProcessNode)
        containerNode = Mock(ContainerNode)
        if (containerType == PLATE) {
            containerNode.getIsPlate() >> true
            containerNode.containerType >> PLATE
        } else {
            containerNode.getIsPlate() >> false
            containerNode.containerType >> TUBE
        }
        artifactNode = generateMockArtifactNode()
        artifactNode.getContainerNode() >> containerNode
        processNode.inputAnalytes >> [artifactNode]

        LibraryCreationProcess process = Mock(LibraryCreationProcess, constructorArgs:[processNode]) as LibraryCreationProcess
        process
    }
}