package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.PoolCreation
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.ActionBean
import spock.lang.Specification

class ActionBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate"() {
        setup:
        ActionBean bean = new ActionBean(
                    poolingPrepAction: null,
                    threshold: null
            )
        when:
            def errorMessage = bean.validate()
        then:
            errorMessage?.toString()?.contains('Please correct the errors below and upload spreadsheet again')
            errorMessage?.toString()?.contains('Action Section')
            errorMessage?.toString()?.contains('invalid Action')
            errorMessage?.toString()?.contains('unspecified Threshold')
        when:
            bean.poolingPrepAction = PoolCreation.PROCESS_ACTIONS[0]
            bean.threshold = 0.0 //not valid
            def errorMessage2 = bean.validate()
        then:
            errorMessage2?.toString()?.contains('Please correct the errors below and upload spreadsheet again')
            errorMessage2?.toString()?.contains('Action Section')
            errorMessage2?.toString()?.contains('unspecified Threshold')
        when:
            bean.poolingPrepAction = PoolCreation.PROCESS_ACTIONS[0]
            bean.threshold = 100.0
            def errorMessage3 = bean.validate()
        then:
            !errorMessage3
    }
}
