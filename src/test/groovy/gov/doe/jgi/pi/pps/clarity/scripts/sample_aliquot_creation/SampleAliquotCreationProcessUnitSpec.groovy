package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification

class SampleAliquotCreationProcessUnitSpec extends Specification {

    static String PLATE = 'Plate'
    static String TUBE = 'Tube'

    def setup() {
    }

    def cleanup() {
    }

    void "test validateDataTab"(){
        setup:
            List<SampleAnalyteTableBean> beansList = [
                    new SampleAnalyteTableBean(aliquotStatus: new DropDownList(value: Analyte.PASS), passed: true, destinationBarcode: '27-1243', destinationLabware: '96 well plate'),
                    new SampleAnalyteTableBean(aliquotStatus: new DropDownList(value: Analyte.FAIL), passed: false, destinationBarcode: '27-1244', destinationLabware: '96 well plate'),
                    new SampleAnalyteTableBean(aliquotStatus: new DropDownList(value: Analyte.PASS), passed: true, destinationBarcode: '27-1245', destinationLabware: '96 well plate')
            ]
            ContainerNode containerNode
        SampleAliquotCreationProcess process = spyProcess(PLATE, containerNode)
        when:
            process.validateDataTab(beansList, containerNode)
        then:
            def e = thrown WebException
            e.message.contains("""
                    Please correct the errors below and upload spreadsheet again.
                    Data Tab: more than one destination barcode found for passed aliquots on a plate
            """)
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

    SampleAliquotCreationProcess spyProcess(String containerType, ContainerNode containerNode, String containerLocation = '') {
        ProcessNode processNode = Mock(ProcessNode)
        containerNode = Mock(ContainerNode)
        containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION.udf) >> containerLocation
        if (containerType.equals(PLATE)) {
            containerNode.getIsPlate() >> true
            containerNode.containerType >> PLATE
        } else {
            containerNode.getIsPlate() >> false
            containerNode.containerType >> TUBE
        }
        ArtifactNode artifactNode = generateMockArtifactNode()
        artifactNode.getContainerNode() >> containerNode
        processNode.inputAnalytes >> [artifactNode]

        SampleAliquotCreationProcess process = Spy(SampleAliquotCreationProcess, constructorArgs:[processNode])
        NodeManager nodeManager = Mock()
        ProgramStatusNode programStatusNode = Mock()
        nodeManager.getProgramStatusNode(process.processNode.id) >> programStatusNode
        process.nodeManager >> nodeManager
        process
    }

}
