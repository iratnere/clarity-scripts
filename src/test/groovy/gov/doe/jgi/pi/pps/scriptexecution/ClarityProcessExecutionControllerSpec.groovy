package gov.doe.jgi.pi.pps.scriptexecution

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import spock.lang.Specification

class ClarityProcessExecutionControllerSpec extends Specification {

    ClarityProcessExecutionController controller
    ProcessExecutionService service = Mock()

    void setup() {
        controller = new ClarityProcessExecutionController(processExecutionService: service)
    }


    void "should process a payload"() {
        given:
        def payLoad =
    """
{
  "options": {
    "P": "24-994552",
    "A": "ApproveForShippingPPV",
    "S": "http://localhost:9080/api/v2/processes/24-994552",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
    """

//        def json =
//                """
//    """
        ObjectMapper objectMapper = new ObjectMapper()
        ScriptParams scriptParams = objectMapper.readValue(payLoad, ScriptParams.class)
        //objectMapper.writeValue()
        and:
        //1 * service.processScriptParams(scriptParams) >> json
        1 * service.processScriptParams(scriptParams)

        when:
        def response = controller.process(scriptParams)

        then:
        response.statusCode == HttpStatus.OK
        //response.body == json
    }

}
