package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import spock.lang.Specification

class ValidateBarcodeAssociationUnitTestSpec extends Specification {
    ActionHandler actionHandler
    Random random

    def setup() {
        actionHandler = new ValidateBarcodeAssociation()
        random = new Random()
    }

    def cleanup() {
    }

    void "test checkSampleStatus"(){
        setup:
        Long sampleId1 = random.nextLong()
        Long sampleId2 = random.nextLong()
        when:
        Map map = [:]
        map[sampleId1] = "${SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value}"
        map[sampleId2] = "${SampleStatusCv.AWAITING_SAMPLE_QC.value}"
        String errors = actionHandler.checkStatus(map)
        then:
        assert errors.split("\n").size() == 1
    }

    void "test checkIsSample"() {
        setup:
        ContainerNode containerNode = Mock(ContainerNode)
        containerNode.id >> '12343'
        when:
        Analyte analyte = Mock(SampleAnalyte)
        analyte.id >> '7y3834'
        analyte.containerNode >> containerNode
        ClaritySample sample = Mock(PmoSample)
        analyte.claritySample >> sample
        then:
        assert actionHandler.checkIsSample(analyte) == ''
        when:
        analyte = Mock(SampleAnalyte)
        analyte.id >> '7y3834'
        analyte.containerNode >> containerNode
        sample = Mock(ScheduledSample)
        analyte.claritySample >> sample
        then:
        assert actionHandler.checkIsSample(analyte) != ''
    }
}