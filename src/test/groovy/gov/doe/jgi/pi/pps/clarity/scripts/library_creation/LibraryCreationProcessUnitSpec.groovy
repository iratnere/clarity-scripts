package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProtocolNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import spock.lang.Specification
import spock.lang.Unroll

class LibraryCreationProcessUnitSpec extends Specification {

    String SINGLE_CELL_INTERNAL = 'Fragment - Internal Selection and Amplification'

    def setup() {
    }

    def cleanup() {
    }

    void "test isMultiplexed"(
            String protocolName,
            boolean pacBioMulti
    ) {
        setup:
        LibraryCreationProcess process = Spy(LibraryCreationProcess)

            ProtocolNode protocolNode = Mock(ProtocolNode)
            //inputAnalyte.artifactNode?.getProtocolNodeInProgress(processNode)?.name
            protocolNode.name >> protocolName

            ArtifactNode artifactNode = generateMockArtifactNode()
            artifactNode.getProtocolNodeInProgress(null) >> protocolNode

        Analyte inputAnalyte = Mock(Analyte)
            inputAnalyte.artifactNode >> artifactNode
        expect:
            inputAnalyte.artifactNode
            inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)
            inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)?.name == protocolName
        when:
            boolean isPacBioMultiplexed = process.isMultiplexed(inputAnalyte)
        then:
            assert pacBioMulti == isPacBioMultiplexed : "protocol name:$protocolName    isPacBioMultiplexed:$isPacBioMultiplexed"
        where:
        protocolName                                                            |pacBioMulti
        ClarityWorkflow.LC_PACBIO_MULTIPLEXED_2KB_AMPLICON_TUBES.value     |true
        ClarityWorkflow.LC_PACBIO_MULTIPLEXED_3KB_TUBES.value              |true
        ClarityWorkflow.LC_PACBIO_MULTIPLEXED_10KB_TUBES.value             |true
        ClarityWorkflow.LC_PACBIO_MULTIPLEXED_PRODUCTION_RND_TUBES.value   |true
        ClarityWorkflow.LC_PACBIO_MULTIPLEXED_10KB_BLUE_PIPPIN_TUBES.value |true
        'LC PacBio 2kb'                                                         |false
        'LC Illumina Regular LMP, 8kb, CLRS'                                    |false
        'LC Production RnD, Tubes'                                              |false
    }

    @Unroll
    void "test initializeLcAdapter #className isIllumina=#illumina"(
            def className,
            Boolean itag,
            Boolean exomeCapture,
            Boolean plate,
            Boolean smallRna,
            Boolean illumina,
            Boolean pacBio,
            Boolean pacBioMulti,
            Boolean singleCell,
            Boolean dapSeq
    ) {
        setup:
            def libraryCreation
            String protocolName = pacBioMulti ? ClarityWorkflow.LC_PACBIO_MULTIPLEXED_2KB_AMPLICON_TUBES.value : 'DummyProtocolName'
        LibraryCreationProcess process = Spy(LibraryCreationProcess)

            ProtocolNode protocolNode = Mock(ProtocolNode)
            protocolNode.name >> protocolName

            ArtifactNode artifactNode = generateMockArtifactNode()
            artifactNode.getProtocolNodeInProgress(null) >> protocolNode

        Analyte inputAnalyte = Mock(Analyte)
            inputAnalyte.isItag >> itag
            inputAnalyte.isExomeCapture >> exomeCapture
            inputAnalyte.isIllumina >> illumina
            inputAnalyte.isSmallRna >> smallRna
            inputAnalyte.isPacBio >> pacBio
            inputAnalyte.isInternalSingleCell >> singleCell
            inputAnalyte.isDapSeq >> dapSeq
            inputAnalyte.artifactNode >> artifactNode
        expect:
            inputAnalyte.artifactNode
            inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)
            inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)?.name == protocolName
        when:
            libraryCreation = process.initializeLcAdapter(inputAnalyte, plate)
        then:
            libraryCreation
            libraryCreation.class == className
        where:
        className                               |itag    |exomeCapture   |plate  |smallRna   |illumina   |pacBio |pacBioMulti|singleCell    |dapSeq
        LibraryCreationItag                    |true |null |true  |null |null |null |null  |null |null
        LibraryCreationExomeCapture            |null |true |true  |null |null |null |null  |null |null
        LibraryCreationPlateSmallRna           |null |null |true  |true |null |null |null  |null |null
        LibraryCreationTubeIllumina            |null |null |false |true |null |null |null  |null |null
        LibraryCreationPlateIllumina           |null |null |true  |null |true |null |null  |null |null
        LibraryCreationTubeIllumina            |null |null |false |null |true |null |null  |null |null
        LibraryCreationPlatePacBio             |null |null |true  |null |null |true |null  |null |null
        LibraryCreationTubePacBioMultiplexed   |null |null |false |null |null |true |true  |null |null
        LibraryCreationTubePacBio              |null |null |false |null |null |true |false |null |null
        LibraryCreationPlateSingleCellInternal |null |null |true  |null |null |null |null  |true |null
        LibraryCreationDapSeq                  |null |null |false |null |true |null |null  |null |true
    }

    void "test initializeLcAdapter Exception thrown"(
            def className,
            Boolean itag,
            Boolean exomeCapture,
            Boolean plate,
            Boolean smallRna,
            Boolean illumina,
            Boolean pacbio,
            Boolean singleCell
    ) {
        setup:
        LibraryCreationProcess process = Spy(LibraryCreationProcess)

        Analyte inputAnalyte = Mock(Analyte)
        inputAnalyte.name >> 'AnalyteName'
        inputAnalyte.isItag >> itag
        inputAnalyte.isExomeCapture >> exomeCapture
        inputAnalyte.isIllumina >> illumina
        inputAnalyte.isSmallRna >> smallRna
        inputAnalyte.isPacBio >> pacbio
        inputAnalyte.isInternalSingleCell >> singleCell
        when:
        process.initializeLcAdapter(inputAnalyte, plate)
        then:
        def e = thrown WebException
        e.message.contains('Analyte(AnalyteName) :unknown template type and/or platform')
        where:
        className           |itag | exomeCapture | plate | smallRna | illumina | pacbio |singleCell
        LibraryCreationItag |null | null | true  | null | null | null |null
        LibraryCreationItag |null | null | false | null | null | null |null
    }

    void "test initializeLcAdapter Single Cell Exception thrown"(
            def className,
            Boolean itag,
            Boolean exomeCapture,
            Boolean plate,
            Boolean smallRna,
            Boolean illumina,
            Boolean pacbio,
            Boolean singleCell
    ) {
        setup:
        String protocolName = 'LC Illumina Nextera - Acoustic, Plates'
        LibraryCreationProcess process = Spy(LibraryCreationProcess)

        ProtocolNode protocolNode = Mock(ProtocolNode)
        protocolNode.name >> protocolName

        ArtifactNode artifactNode = generateMockArtifactNode()
        artifactNode.getProtocolNodeInProgress(null) >> protocolNode

        Analyte inputAnalyte = Mock(Analyte)
        inputAnalyte.name >> 'AnalyteName'
        inputAnalyte.isItag >> itag
        inputAnalyte.isExomeCapture >> exomeCapture
        inputAnalyte.isIllumina >> illumina
        inputAnalyte.isSmallRna >> smallRna
        inputAnalyte.isPacBio >> pacbio
        inputAnalyte.isInternalSingleCell >> singleCell
        inputAnalyte.artifactNode >> artifactNode
        expect:
        inputAnalyte.artifactNode
        inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)
        inputAnalyte.artifactNode?.getProtocolNodeInProgress(null)?.name == protocolName
        when:
        process.initializeLcAdapter(inputAnalyte, plate)
        then:
        def e = thrown WebException
        !(e.message.contains("Analyte(AnalyteName) is not $SINGLE_CELL_INTERNAL"))
        e.message.contains("Analyte(AnalyteName) :unknown template type and/or platform")
        where:
        className                               |itag    |exomeCapture   |plate  |smallRna   |illumina   |pacbio |singleCell
        LibraryCreationPlateSingleCellInternal |null |null |true  |null |null |null |false
        LibraryCreationPlateSingleCellInternal |null |null |false |null |null |null |false
    }

    void "test validateOutputContainerType input plate output tube"() {
        setup:
        ContainerNode containerNodeInputPlate = Mock(ContainerNode)
        containerNodeInputPlate.id >> generateDummyId('ContainerNode')
        containerNodeInputPlate.getIsPlate() >> true
        containerNodeInputPlate.getContainerTypeEnum() >> ContainerTypes.WELL_PLATE_96

        ContainerNode containerNodeOutputTube = Mock(ContainerNode)
        containerNodeOutputTube.id >> generateDummyId('ContainerNode')
        containerNodeOutputTube.getIsPlate() >> false
        containerNodeOutputTube.getContainerTypeEnum() >> ContainerTypes.TUBE

        LibraryCreationProcess process = spyProcess(containerNodeInputPlate, containerNodeOutputTube)
        when:
        process.validateOutputContainerType([containerNodeOutputTube],[containerNodeInputPlate])
        then:
        def e = thrown WebException
        1 * process.validateOutputContainerType([containerNodeOutputTube],[containerNodeInputPlate])
        e.message.contains("""
                Process output container type 'Tube'
                does not match input container type '96 well plate'.
                Please select '96 well plate' as the output container.
            """)
    }

    void "test getIndexNames"(){
        setup:
            String indexName1 = 'IT051'
            String indexName2 = 'D701-D503'
            List<ClarityLibraryStock> libraryStocks = []
            [indexName1, ArtifactIndexService.INDEX_NAME_N_A, '', indexName2, null].each{
                ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
                libraryStock.udfIndexName >> it
                libraryStocks << libraryStock
            }
        expect:
            libraryStocks
        when:
            def indexNames = LibraryCreationProcess.getIndexNames(libraryStocks)
        then:
            assert indexName1 in indexNames
            assert indexName2 in indexNames
            indexNames.size() == 2
    }

    void "test getIndexContainerBarcodes"(){
        setup:
            String barcode1 = 'Illumina_smRNA_Single-Index'
            String barcode2 = 'Illumina_TruSeq-HT_Dual-Index'
            List<ClarityLibraryStock> libraryStocks = []
            [barcode1, '', barcode2, null].each{
                ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
                libraryStock.udfIndexContainerBarcode >> it
                libraryStocks << libraryStock
            }
        expect:
            libraryStocks
        when:
            def indexBarcodes = LibraryCreationProcess.getIndexContainerBarcodes(libraryStocks)
        then:
            assert barcode1 in indexBarcodes
            assert barcode2 in indexBarcodes
            indexBarcodes.size() == 2
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

    LibraryCreationProcess spyProcess(ContainerNode containerNodeInputPlate, ContainerNode containerNodeOutputTube) {
        ProcessNode processNode = Mock(ProcessNode)

        ArtifactNode artifactNode = generateMockArtifactNode()
        artifactNode.getContainerNode() >> containerNodeInputPlate
        processNode.inputAnalytes >> [artifactNode]

        ArtifactNode artifactNodeOutput = generateMockArtifactNode()
        artifactNodeOutput.getContainerNode() >> containerNodeOutputTube
        processNode.outputAnalytes >> [artifactNodeOutput]

        LibraryCreationProcess process = Spy(LibraryCreationProcess, constructorArgs:[processNode]) as LibraryCreationProcess
        process
    }
}