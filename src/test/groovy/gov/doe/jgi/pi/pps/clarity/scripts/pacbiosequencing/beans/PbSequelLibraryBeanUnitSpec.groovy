package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing.beans

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import spock.lang.Specification

class PbSequelLibraryBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate"() {
        setup:
        PbSequelLibraryBean bean = new PbSequelLibraryBean(
                sampleName: 'GHYAT_2-3826792',
                sampleWell: 'A01'
        )
        def errorMsg
        when:
        errorMsg = bean.validate()
        then:
        errorMsg.toString().contains('Please correct the <GHYAT_2-3826792 A01> value(s):')
        errorMsg.toString().contains('invalid System Name value <null>, expected values [Sequel, Sequel II]')
        errorMsg.toString().contains('invalid Is Collection value <null>, expected values [TRUE, FALSE]')
        errorMsg.toString().contains('invalid Sample is Barcoded value <null>, expected values [TRUE, FALSE]')
/*
Please correct the <GHYAT_2-3826792 A01> value(s):
invalid System Name value <null>, expected values [Sequel, Sequel II]
invalid Is Collection value <null>, expected values [TRUE, FALSE]
invalid movieTimeHrs value <null>, <Acquisition Time> value shoud be >= 6
invalid <PacBio Insert Size> value <null>, <PacBio Insert Size> value shoud be >= 10
invalid <PacBio Loading Concentration (nM)> value <null>
invalid Sample is Barcoded value <null>, expected values [TRUE, FALSE]
""")
 */
        when:
        bean.systemName = SequencerModelTypeCv.SEQUEL.value
        bean.isCollection = Boolean.TRUE.toString().toUpperCase()
        bean.sampleIsBarcoded = Boolean.FALSE.toString().toUpperCase()
        errorMsg = bean.validate()
        then:
        errorMsg.toString().contains('invalid movieTimeHrs value <null>, <Acquisition Time> value should be >= 6')
        errorMsg.toString().contains('invalid <PacBio Insert Size> value <null>, <PacBio Insert Size> value should be >= 10')
        errorMsg.toString().contains('invalid <PacBio Loading Concentration (nM)> value <null>')
        when:
        bean.movieTimeHrs = 6
        bean.insertSizeBp = 12
        bean.onPlateLoadingConcentrationPm = 22.2
        errorMsg = bean.validate()
        then:
        !errorMsg
    }

    void "test validate movieTimeHrs Sequel"() {
        setup:
        PbSequelLibraryBean bean = new PbSequelLibraryBean(
                sampleName: 'GHYAT_2-3826792',
                sampleWell: 'A01',
                isCollection: Boolean.TRUE.toString().toUpperCase(),
                sampleIsBarcoded: Boolean.FALSE.toString().toUpperCase(),
                insertSizeBp: 12,
                onPlateLoadingConcentrationPm: 22.2

        )
        bean.systemName = SequencerModelTypeCv.SEQUEL.value
        bean.movieTimeHrs = 30
        def errorMsg
        when:
        errorMsg = bean.validate()
        then:
        errorMsg?.length()
        errorMsg.toString().contains('Please correct the <GHYAT_2-3826792 A01> value(s):')
        errorMsg.toString().contains('invalid movieTimeHrs value <30>, <Acquisition Time> value should be <= 1200 ')
        when:
        bean.movieTimeHrs = 20
        errorMsg = bean.validate()
        then:
        !errorMsg
    }

    void "test validate movieTimeHrs Sequel II"() {
        setup:
        PbSequelLibraryBean bean = new PbSequelLibraryBean(
                sampleName: 'GHYAT_2-3826792',
                sampleWell: 'A01',
                isCollection: Boolean.TRUE.toString().toUpperCase(),
                sampleIsBarcoded: Boolean.FALSE.toString().toUpperCase(),
                insertSizeBp: 12,
                onPlateLoadingConcentrationPm: 22.2

        )
        bean.systemName = SequencerModelTypeCv.SEQUEL_II.value
        bean.movieTimeHrs = 60
        def errorMsg
        when:
        errorMsg = bean.validate()
        then:
        errorMsg?.length()
        errorMsg.toString().contains('Please correct the <GHYAT_2-3826792 A01> value(s):')
        errorMsg.toString().contains('invalid movieTimeHrs value <60>, Acquisition Time value should be <= 1800')
        when:
        bean.movieTimeHrs = 30
        errorMsg = bean.validate()
        then:
        !errorMsg
    }
}