package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import spock.lang.Specification

class FractionsTableBeanUnitSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate barcodes are unique"() {
        setup:
        DropDownList listPassed = new DropDownList(
                controlledVocabulary: Analyte.PASS_FAIL_LIST,
                value: Analyte.PASS)
        DropDownList listFailed = new DropDownList(
                controlledVocabulary: Analyte.PASS_FAIL_LIST,
                value: Analyte.FAIL)
        List<FractionsTableBean> beans = (1..3).collect {
            FractionsTableBean bean = new FractionsTableBean(
                    sourceStatus: listPassed,
                    destinationBarcode: "barcode_$it",
                    transferredVolumeUl: it as BigDecimal
            )
            bean
        }
        List<String> validBarcodes
        when:
        validBarcodes = []
        then:
        beans.each {
            assert !it.validate(validBarcodes)
        }
        when:
        FractionsTableBean beanFailed = new FractionsTableBean(
                sourceStatus: listFailed,
                destinationBarcode: "barcode_1",
                transferredVolumeUl: 12.12
        )
        beans << beanFailed
        validBarcodes = []
        then:
        beans.each {
            assert !it.validate(validBarcodes)
        }
        when:
        FractionsTableBean beanPassed = new FractionsTableBean(
                sampleLimsId: '12345',
                sourceStatus: listPassed,
                destinationBarcode: "barcode_1",
                transferredVolumeUl: 22.22
        )
        beans << beanPassed
        validBarcodes = []
        then:
        beans.eachWithIndex {bean, index ->
            def error = bean.validate(validBarcodes)
            if (index != 4) {
                assert !error
            }
            else {
                assert error.contains('Please correct the errors below and upload spreadsheet again.')
                assert error.contains('Sample Lims Id(12345): Destination Barcode<barcode_1> is not unique')
            }
        }
    }
}
