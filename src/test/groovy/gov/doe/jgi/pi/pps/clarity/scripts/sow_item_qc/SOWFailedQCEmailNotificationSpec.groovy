package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.notification.SowFailedQcEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.notification.SowQcFailedEmailDetails
import spock.lang.Specification

class SOWFailedQCEmailNotificationSpec extends Specification {

    String FRAGMENT = 'Fragment'
    List<EmailDetails> testEmailDetails = []
    Analyte analyte

    def setup() {
        analyte = Mock(Analyte)
        ClaritySample sample = Mock(ScheduledSample)
        analyte.claritySample >> sample
        sample.sequencingProject >> Mock(SequencingProject)
        sample.pmoSample >> Mock(PmoSample)
        Random random = new Random()
        (1..4).each{
            Long projectId = random.nextLong()
            int sampleCount = random.nextInt() % 4
            (1..sampleCount).each{
                Long sampleContactId = random.nextLong()
                Long pmoSampleId = random.nextLong()
                Long sowItemId = random.nextLong()
                testEmailDetails << prepareEmailDetails(projectId, sampleContactId, pmoSampleId, sowItemId)
            }
        }
    }

    def cleanup() {
    }

    EmailDetails prepareEmailDetails(Long projectId, Long sampleContactId, Long pmoSampleId, Long sowItemId){
        EmailDetails emailDetails = new SowQcFailedEmailDetails(analyte)
        emailDetails.sequencingProjectId = projectId
        emailDetails.sequencingProjectManagerId = projectId
        emailDetails.sequencingProjectName = "Project $projectId"
        emailDetails.sequencingProjectPIName = "Project PI $projectId"
        emailDetails.sampleContactName = "Sample Contact $sampleContactId"
        emailDetails.sampleContactId = sampleContactId
        emailDetails.sequencingProjectManagerName = "Manager $projectId"
        emailDetails.pmoSampleId = pmoSampleId
        emailDetails.sampleLimsId = "AKS${pmoSampleId}A1"
        emailDetails.sowItemId = sowItemId
        emailDetails.sowItemType = FRAGMENT
        emailDetails.defaultLibraryQueue = "Default queue"
        emailDetails.sampleQCResult = "Pass"
        emailDetails.sowQCResult = "Fail"
        emailDetails.failureMode = 'My failure mode'
        return emailDetails
    }

    void "test makeGroups"() {
        expect:
        testEmailDetails
        when:
        EmailNotification notification = new SowFailedQcEmailNotification()
        notification.makeGroups(testEmailDetails)
        then:
        notification.emailDetailsGroups.size() == 4
    }

    void "test binding"(){
        expect:
        testEmailDetails
        when:
        EmailNotification notification = new SowFailedQcEmailNotification()
        notification.makeGroups(testEmailDetails)
        then:
        notification.emailDetailsGroups.each{emailDetails ->
            notification.getBinding(emailDetails)
            for(int i=1; i<emailDetails.size(); i++){
                assert emailDetails[i-1].sampleContactId <= emailDetails[i].sampleContactId
            }
        }
    }
}