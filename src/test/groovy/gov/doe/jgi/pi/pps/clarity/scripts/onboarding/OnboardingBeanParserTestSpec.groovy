package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingBeansParser
import spock.lang.Specification

class OnboardingBeanParserTestSpec extends Specification {
    OnboardingBeansParser parser

    def setup() {
        parser = new OnboardingBeansParser()
    }

    def cleanup() {
    }

    def "test processLibrary"() {
        setup:
        ClarityLibraryStock ls = Mock(ClarityLibraryStock)
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        PmoSample pmoSample = Mock(PmoSample)
        HudsonAlphaBean bean = new HudsonAlphaBean()
        bean.sampleName = "my sample"
        scheduledSample.pmoSample >> pmoSample
        ls.claritySamples >> [scheduledSample]
        when:
        String error = parser.processLibrary(bean, ls)
        then:
        assert error
        assert !bean.libraryStock
        when:
        pmoSample.udfCollaboratorSampleName >> "my sample"
        error = parser.processLibrary(bean, ls)
        then:
        assert !error
        assert bean.libraryStock
    }

    def "test processSample"() {
        setup:
        PmoSample pmoSample = Mock(PmoSample)
        pmoSample.pmoSample >> pmoSample
        pmoSample.udfExternal >> OnboardingAdapter.EXTERNAL_UDF_VALUE
        HudsonAlphaBean bean = new HudsonAlphaBean()
        when:
        parser.processSample(bean, pmoSample)
        then:
        assert !bean.checkStatus
        assert bean.isExternal
        when:
        pmoSample = Mock(PmoSample)
        pmoSample.pmoSample >> pmoSample
        parser.processSample(bean, pmoSample)
        then:
        assert bean.checkStatus
        assert bean.isExternal
    }
}