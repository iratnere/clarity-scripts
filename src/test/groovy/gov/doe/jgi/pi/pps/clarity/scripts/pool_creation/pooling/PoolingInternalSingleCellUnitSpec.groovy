package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.PoolingInternalSingleCell
import spock.lang.Ignore
import spock.lang.Specification

class PoolingInternalSingleCellUnitSpec extends Specification {

    String SINGLE_CELL_INTERNAL = 'Fragment - Internal Selection and Amplification'

    def setup() {
    }

    def cleanup() {
    }
//comment d_TEST1
//comment2 d_TEST1    
    void "test validate"() {
        setup:
            boolean isInternalSingleCell = true
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(isInternalSingleCell)
        LibraryInformation bean = getLibraryInformation(clarityLibraryStock)
            bean.poolNumber = 1
            def members = [bean]
        PoolingInternalSingleCell singleCell = new PoolingInternalSingleCell(members)
        when:
            def message = singleCell.validate()
        then:
            !message
    }

    void "test validateSingleCellInternalPool"() {
        setup:
            boolean isInternalSingleCell = true
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(isInternalSingleCell)
        LibraryInformation bean = getLibraryInformation(clarityLibraryStock)
        when:
            def message = PoolingInternalSingleCell.validateSingleCellInternalPool([bean], 1)
        then:
            !message
    }

    void "test validate invalid pool number"(poolNumber) {
        setup:
            boolean isInternalSingleCell = true
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(isInternalSingleCell)
        LibraryInformation bean = getLibraryInformation(clarityLibraryStock)
            bean.poolNumber = poolNumber
            def members = [bean]
        PoolingInternalSingleCell singleCell = new PoolingInternalSingleCell(members)
        when:
            def message = singleCell.validate()
        then:
            message[Pooling.ERROR]
            message[Pooling.ERROR].first().contains("Single Cell Internal pool #$poolNumber is not valid.")
            message[Pooling.ERROR].first().contains("Pool number value should be greater than zero.")
        where:
            poolNumber << [0, -1]
    }

    void "test validateSingleCellInternalPool invalid pool number"(poolNumber) {
        setup:
            boolean isInternalSingleCell = true
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(isInternalSingleCell)
        LibraryInformation bean = getLibraryInformation(clarityLibraryStock)
        when:
            def message = PoolingInternalSingleCell.validateSingleCellInternalPool([bean], poolNumber)
        then:
            message.contains("Single Cell Internal pool #$poolNumber is not valid.")
            message.contains("Pool number value should be greater than zero.")
        where:
            poolNumber << [0, -1]
    }
@Ignore
    void "test validateSingleCellInternalPool is not Single Cell Internal"() {
        setup:
            boolean isInternalSingleCell = false
        ClarityLibraryStock clarityLibraryStock = mockClarityLibraryStock(isInternalSingleCell)
        LibraryInformation bean = getLibraryInformation(clarityLibraryStock)
        when:
            def message =  PoolingInternalSingleCell.validateSingleCellInternalPool([bean], 1)
        then:
            message.contains("library is not ${SINGLE_CELL_INTERNAL}")
    }

    void "test validate invalid udfBatchId and udfPoolNumber"() {
        setup:
            boolean isInternalSingleCell = true
            def poolNumber = 5
        LibraryInformation bean1 = getLibraryInformation(
                    mockClarityLibraryStock(isInternalSingleCell, 1.0, 4444 as BigInteger)
            )
            bean1.poolNumber = poolNumber
        LibraryInformation bean2 = getLibraryInformation(
                    mockClarityLibraryStock(isInternalSingleCell, 2.0, 5555 as BigInteger)
            )
            bean2.poolNumber = poolNumber
            def members = [bean1, bean2]
        PoolingInternalSingleCell singleCell = new PoolingInternalSingleCell(members)
        when:
            def message = singleCell.validate()
        then:
            message[Pooling.ERROR]
            message[Pooling.ERROR].first().contains("Single Cell Internal pool #$poolNumber is not valid")
            message[Pooling.ERROR].first().contains("Pool members should have the same Batch Id.")
            message[Pooling.ERROR].first().contains("Pool members should have the same Pool Number.")
    }

    void "test validateSingleCellInternalPool invalid udfBatchId and udfPoolNumber"() {
        setup:
            boolean isInternalSingleCell = true
            def poolNumber = 1
        LibraryInformation bean1 = getLibraryInformation(
                    mockClarityLibraryStock(isInternalSingleCell, 1.0, 4444 as BigInteger)
            )
        LibraryInformation bean2 = getLibraryInformation(
                    mockClarityLibraryStock(isInternalSingleCell, 2.0, 5555 as BigInteger)
            )
        when:
            def message = PoolingInternalSingleCell.validateSingleCellInternalPool([bean1, bean2], poolNumber)
        then:
            message.contains("Single Cell Internal pool #$poolNumber is not valid")
            message.contains("Pool members should have the same Batch Id.")
            message.contains("Pool members should have the same Pool Number.")
    }

    LibraryInformation getLibraryInformation(ClarityLibraryStock clarityLibraryStock) {
        LibraryInformation bean = new LibraryInformation()
        bean.analyte = clarityLibraryStock
        bean.libraryName = 'AAAAA'
        return bean
    }

    ClarityLibraryStock mockClarityLibraryStock(boolean isInternalSingleCell,
                                                BigDecimal claritySamplePoolNumber = 1,
                                                BigInteger batchId = 4444){
        ScheduledSample claritySample = Mock(ScheduledSample)
        claritySample.udfPoolNumber >> claritySamplePoolNumber
        claritySample.udfBatchId >> batchId
        ClarityLibraryStock libraryStock = Mock(ClarityLibraryStock)
        libraryStock.isInternalSingleCell >> isInternalSingleCell
        libraryStock.claritySample >> claritySample
        libraryStock
    }
}
