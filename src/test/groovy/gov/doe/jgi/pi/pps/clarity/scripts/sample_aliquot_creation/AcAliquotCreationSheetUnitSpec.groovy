package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AcKeyValuePlateResultsBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AliquotCreationSheet
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleAliquotFailureModesService
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import spock.lang.Specification

class AcAliquotCreationSheetUnitSpec extends Specification {

    def cleanup() {
    }

    void "test getAliquotCreationBeans"() {
        setup:
            def barcode = 'dummyBarcode'
            def locationInput = 'C:1'
            def location = 'B:1'
            def isPlate = true
            def containerType = '96 well plate'
            def queueName = 'dummyQueueName'
            def sowItemId = 224556

        ScheduledSample scheduledSample = Mock(ScheduledSample)
            scheduledSample.sowItemId >> sowItemId
            ContainerNode inputContainerNode = Mock(ContainerNode)
            inputContainerNode.name >> barcode
            inputContainerNode.containerType >> containerType
            inputContainerNode.isPlate >> isPlate

            ArtifactNode inputArtifactNode = generateMockArtifactNode(locationInput)
        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
            sampleAnalyte.artifactNode >> inputArtifactNode
            sampleAnalyte.claritySample >> scheduledSample
            sampleAnalyte.containerNode >> inputContainerNode
            sampleAnalyte.worksheetSourceLocation >> locationInput.minus(':')

        PmoSample rootSample = Mock(PmoSample)
            def id = generateDummyId(PmoSample.class.simpleName)
            rootSample.getId() >> id

        LibraryCreationQueueCv queue = Mock(LibraryCreationQueueCv)
            queue.libraryCreationQueue >> queueName
            ArtifactNode artifactNode = generateMockArtifactNode(location)
            ContainerNode outputContainerNode = Mock(ContainerNode)
            outputContainerNode.containerType >> containerType
            outputContainerNode.isPlate >> isPlate

        ClaritySampleAliquot claritySampleAliquot = Mock(ClaritySampleAliquot)
            claritySampleAliquot.parentAnalyte >> sampleAnalyte
            claritySampleAliquot.parentPmoSamples >> [rootSample]
            claritySampleAliquot.libraryCreationQueue >> queue
            claritySampleAliquot.containerNode >> outputContainerNode
            claritySampleAliquot.artifactNode >> artifactNode

        FreezerContainer freezerContainer = Mock(FreezerContainer)
            freezerContainer.barcode >> barcode

        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)
            process.testMode >> true
            FreezerService freezerService = Mock()
            freezerService.freezerLookupInputAnalytes(process) >> [freezerContainer]
            process.outputPlateContainer >> outputContainerNode
            process.plateLcQueue >> queue

            SampleAliquotFailureModesService service = Mock()
        AcPrepareAliquotCreationSheet actionHandler = Spy(AcPrepareAliquotCreationSheet)
            actionHandler.process >> process
            process.freezerService >> freezerService
            process.sampleAliquotFailureModesService >> service
        when:
            List<SampleAnalyteTableBean> beans = actionHandler.getAliquotCreationSheet([claritySampleAliquot]).aliquotCreationBeans
        then:
//            1 * actionHandler.getAliquotCreationSheet([claritySampleAliquot]).aliquotCreationBeans
            //1 * freezerContainer.getLocation()
            1 * rootSample.getUdfConcentration()
            1 * rootSample.getUdfVolumeUl()
            1 * rootSample.getUdfSampleHMWgDNAYN()
            1 * rootSample.getUdfSampleQcNotes()
            1 * scheduledSample.getUdfTargetAliquotMassNg()
            1 * process.researcher
            //1 * freezerContainer.getCheckInStatus()
            1 * scheduledSample.getUdfSmInstructions()
            1 * scheduledSample.getUdfNotes()
        and:
            beans.each { SampleAnalyteTableBean bean ->
                assert bean.sampleLimsId == rootSample.getId()
                assert bean.sowItemId == scheduledSample.sowItemId
                assert bean.sourceLocation == locationInput.minus(':')
                assert bean.libraryQueue == queueName
            }
    }

    void "test getAcKeyValuePlateResultsBean"() {
        setup:
        BigDecimal plateTargetMassLibTrialNg = 23.3
        BigDecimal plateTargetVolumeLibTrialUl = 46.23

        AcPrepareAliquotCreationSheet actionHandler = new AcPrepareAliquotCreationSheet()
        SampleAliquotCreationProcess process = Mock(SampleAliquotCreationProcess)


        LibraryCreationQueueCv queue = Mock(LibraryCreationQueueCv)
        queue.plateTargetMassLibTrialNg >> plateTargetMassLibTrialNg
        queue.plateTargetVolumeLibTrialUl >> plateTargetVolumeLibTrialUl
        LibraryCreationQueueCv customLcQueue = null
        when:
        actionHandler.process = process
        AliquotCreationSheet acs = actionHandler.getAliquotCreationSheet([])
        acs.details.plateLcQueueCached = queue
        AcKeyValuePlateResultsBean bean = acs.getAcKeyValuePlateResultsBean(queue)
        then:
        bean.totalVolumeUl == plateTargetVolumeLibTrialUl
        bean.targetAliquotMassNg == plateTargetMassLibTrialNg
        when:
        actionHandler.process >> process
        acs = actionHandler.getAliquotCreationSheet([])
        acs.details.plateLcQueueCached = customLcQueue
        bean = acs.getAcKeyValuePlateResultsBean()
        then:
        bean.totalVolumeUl == 0.0
        bean.targetAliquotMassNg == 0.0
    }

    ArtifactNode generateMockArtifactNode(String location = '1:1') {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode.location >> location
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

}
