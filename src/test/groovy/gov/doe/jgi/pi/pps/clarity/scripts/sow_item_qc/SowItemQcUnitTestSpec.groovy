package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.GenericSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SIPSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.LastKeyValueSection
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.TubeSowQCTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import spock.lang.Specification

class SowItemQcUnitTestSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate Sow QC table bean"(BigDecimal targetAliquotMass, String sowQCStatus, String failureMode, BigDecimal adjustedAliquotVolume, boolean error){
        setup:
        TubeSowQCTableBean bean = new TubeSowQCTableBean()
        when:
        bean.adjustedAliquotVolume = adjustedAliquotVolume
        bean.targetAliquotMass = targetAliquotMass
        bean.sowQCStatus = new DropDownList(value: sowQCStatus)
        bean.failureMode = new DropDownList(value: failureMode)
        String errors = bean.validate()
        then:
        assert (errors != "") == error
        where:
        targetAliquotMass   |sowQCStatus    |failureMode    |adjustedAliquotVolume      |error
        12.1234             |"Pass"         |null           |3435                       |false
        0                   |"Pass"         |"failure"      |3435                       |false
        12.1234             |"Fail"         |null           |3435                       |true
        12.1234             |"Fail"         |""             |3425                       |true
        null                |"Fail"         |"failure"      |3425                       |true
        12.1234             |"Pass"         |null           |0                          |false
        12.12               |"Pass"         |null           |null                       |true
    }

    void "test checkIsScheduledSample"() {
        setup:
        SowItemQcPpv ppv = new SowItemQcPpv()
        when:
        SampleAnalyte analyte1 = Mock(SampleAnalyte)
        PmoSample pmoSample = Mock(PmoSample)
        analyte1.claritySample >> pmoSample
        String error = ppv.checkIsScheduledSample(analyte1)
        then:
        assert error
        when:
        SampleAnalyte analyte2 = Mock(SampleAnalyte)
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        analyte2.claritySample >> scheduledSample
        error = ppv.checkIsScheduledSample(analyte2)
        then:
        assert !error
    }

    void "test LastKeyValueSection validate"(){
        setup:
        LastKeyValueSection bean = prepareLastKeyValueSection()
        when:
        def errors = bean.validate("sheetName")
        then:
        assert !errors
        when:
        bean.failureMode = null
        errors = bean.validate('sheetName')
        then:
        assert errors
        when:
        bean.sowItemStatus.value = "Pass"
        errors = bean.validate('sheetName')
        then:
        assert !errors
        when:
        String queue = bean.libraryCreationQueue
        bean.libraryCreationQueue = null
        errors = bean.validate('sheetName')
        then:
        assert errors
        when:
        bean.libraryCreationQueue = queue
        bean.targetAliquotMass = null
        errors = bean.validate('sheetName')
        then:
        assert errors
        when:
        bean.targetAliquotMass = "jfhf"
        errors = bean.validate('sheetName')
        then:
        assert errors
    }

    void "test process udfs"() {
        setup:
        SowItemQc sowItemQc = Mock(SowItemQc)
        sowItemQc.udfReplicatesFailurePercentAllowed >> 50
        sowItemQc.udfControlsFailurePercentAllowed >> 50
        SampleAnalyte analyte = Mock(SampleAnalyte)
        GenericSowQcAdapter ga = new GenericSowQcAdapter([analyte])
        SIPSowQcAdapter sip = new SIPSowQcAdapter([analyte])
        when:
        String error = ga.checkProcessUdfs(sowItemQc)
        then:
        assert !error
        when:
        error = sip.checkProcessUdfs(sowItemQc)
        then:
        assert !error
        when:
        sowItemQc = Mock(SowItemQc)
        sowItemQc.udfControlsFailurePercentAllowed >> 0
        sowItemQc.udfReplicatesFailurePercentAllowed >> 0
        error = ga.checkProcessUdfs(sowItemQc)
        then:
        assert !error
        when:
        error = sip.checkProcessUdfs(sowItemQc)
        then:
        assert !error
        when:
        sowItemQc = Mock(SowItemQc)
        sowItemQc.udfReplicatesFailurePercentAllowed >> null
        sowItemQc.udfControlsFailurePercentAllowed >> null
        error = ga.checkProcessUdfs(sowItemQc)
        then:
        assert !error
        when:
        error = sip.checkProcessUdfs(sowItemQc)
        then:
        assert error
        assert error.contains("Process udf 'Controls Failure Percent Allowed' is required.")
        assert error.contains("Process udf 'Replicates Failure Percent Allowed' is required.")
    }

    void "test generic update status map"() {
        setup:
        Analyte analyte = Mock(SampleAnalyte)
        ContainerNode containerNode = Mock(ContainerNode)
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        scheduledSample.sowItemId >> 123456
        analyte.claritySample >> scheduledSample
        analyte.containerNode >> containerNode
        GenericSowQcAdapter adapter = new GenericSowQcAdapter([analyte])
        when:
        adapter.containerAutoScheduleSowItems[containerNode] = true
        Map statusMap = adapter.updateStatusMap(analyte, Analyte.PASS)
        then:
        assert !statusMap
        when:
        adapter.containerAutoScheduleSowItems[containerNode] = false
        statusMap = adapter.updateStatusMap(analyte, Analyte.PASS)
        then:
        assert statusMap
        assert statusMap[123456L] == SowItemStatusCv.AWAITING_SAMPLE_QC_REVIEW
        when:
        adapter.sampleStatusMap.clear()
        adapter.containerAutoScheduleSowItems[containerNode] = false
        statusMap = adapter.updateStatusMap(analyte, Analyte.FAIL)
        then:
        assert statusMap
        assert statusMap[123456L] == SowItemStatusCv.NEEDS_ATTENTION
        when:
        adapter.sampleStatusMap.clear()
        adapter.containerAutoScheduleSowItems[containerNode] = true
        statusMap = adapter.updateStatusMap(analyte, Analyte.FAIL)
        then:
        assert statusMap
        assert statusMap[123456L] == SowItemStatusCv.NEEDS_ATTENTION
    }

    void "test generic get stage"() {
        setup:
        GenericSowQcAdapter adapter = new GenericSowQcAdapter([])
        when:
        Stage stage = adapter.getStage(MaterialCategoryCv.DNA.materialCategory)
        then:
        assert stage == Stage.ALIQUOT_CREATION_DNA
        when:
        stage = adapter.getStage(MaterialCategoryCv.RNA.materialCategory)
        then:
        assert stage == Stage.ALIQUOT_CREATION_RNA
    }

    LastKeyValueSection prepareLastKeyValueSection(){
        LastKeyValueSection bean = new LastKeyValueSection()
        bean.libraryCreationQueue = 'Library queue'
        bean.sowItemStatus = new DropDownList(value: "Fail")
        bean.failureMode = new DropDownList(value: 'Some Failure Mode')
        bean.targetAliquotMass = 12
        return bean
    }

    SampleAnalyte mockAnalyte(ContainerNode containerNode, Long sowId){
        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
        sampleAnalyte.containerNode >> containerNode
        ClaritySample sample = Mock(ScheduledSample)
        sample.sowItemId >>  sowId
        sampleAnalyte.claritySample >> sample
        return sampleAnalyte
    }
}