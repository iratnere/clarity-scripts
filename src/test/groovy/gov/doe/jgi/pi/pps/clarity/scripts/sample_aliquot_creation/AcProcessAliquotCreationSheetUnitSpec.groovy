package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.util.IdGenerator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import spock.lang.Specification

class AcProcessAliquotCreationSheetUnitSpec extends Specification {

    static String PLATE = 'Plate'
    static String TUBE = 'Tube'

    ContainerNode containerNode
    ArtifactNode artifactNode


    def setup() {
    }

    def cleanup() {
    }

    void "test transferBeansDataToClaritySampleAliquots output TUBE PASS"(){
        setup:
        SampleAnalyteTableBean bean = new SampleAnalyteTableBean(
                    aliquotStatus: new DropDownList(value: Analyte.PASS),
                    destinationBarcode: '00271243',
                    finalAliquotVolumeUl: 110.0,
                    finalAliquotMassNg: 123.00,
                    smQcInstruction: 'instructions',
            )
            List<SampleAnalyteTableBean> beansList = [bean]
        SampleAliquotCreationProcess process = mockProcess(TUBE)
            process.testMode >> true
            process.getSampleAnalyteBeans() >> beansList

        AcProcessAliquotCreationSheet actionHandler = Spy(AcProcessAliquotCreationSheet)
            actionHandler.process >> process
            def outputs = []

        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
            sampleAliquot.artifactNode >> artifactNode
            bean.aliquotLimsId = artifactNode.id
            sampleAliquot.containerNode >> containerNode
            process.outputPlateContainer >> null
            outputs << sampleAliquot
        when:
            bean.validate()
        then:
            noExceptionThrown()
        when:
            actionHandler.transferBeansDataToClaritySampleAliquots(outputs)
            then:
            noExceptionThrown()
            1 * sampleAliquot.setSystemQcFlag(bean.passed)
            1 * sampleAliquot.setUdfLabResult(bean.aliquotStatus.value)
            1 * sampleAliquot.setUdfFailureMode(null)
            1 * sampleAliquot.setUdfVolumeUl(bean.finalAliquotVolumeUl)
            1 * sampleAliquot.setUdfConcentrationNgUl(bean.finalAliquotMassNg/bean.finalAliquotVolumeUl)

            1 * containerNode.setProperty('name', bean.destinationBarcode)
            1 * sampleAliquot.setContainerUdfFinalAliquotVolumeUl(bean.finalAliquotVolumeUl)
            1 * sampleAliquot.setContainerUdfFinalAliquotMassNg(bean.finalAliquotMassNg)
            1 * sampleAliquot.setContainerUdfLabResult(bean.aliquotStatus.value)
    }

    void "test transferBeansDataToClaritySampleAliquots output TUBE FAIL"(){
        setup:
        SampleAnalyteTableBean bean = new SampleAnalyteTableBean(
                    aliquotStatus: new DropDownList(value: Analyte.FAIL),
                    finalAliquotVolumeUl: 120.0,
                    finalAliquotMassNg: 123.00,
                    //destinationBarcode: '00271243',
                    failureMode: new DropDownList(value:'operator error'),
                    smQcInstruction: 'instructions',
            )
            List<SampleAnalyteTableBean> beansList = [bean]
            SampleAliquotCreationProcess process = mockProcess(TUBE)
            process.testMode >> true
            process.getSampleAnalyteBeans() >> beansList

            AcProcessAliquotCreationSheet actionHandler = Spy(AcProcessAliquotCreationSheet)
            actionHandler.process >> process
            def outputs = []

        ClaritySampleAliquot sampleAliquot = Mock(ClaritySampleAliquot)
            sampleAliquot.artifactNode >> artifactNode
            bean.aliquotLimsId = artifactNode.id
            sampleAliquot.containerNode >> containerNode
            process.outputPlateContainer >> null
            outputs << sampleAliquot
        when:
            bean.validate()
        then:
            noExceptionThrown()
        when:
            actionHandler.transferBeansDataToClaritySampleAliquots(outputs)
        then:
            noExceptionThrown()
            1 * sampleAliquot.setSystemQcFlag(bean.passed)
            1 * sampleAliquot.setUdfLabResult(bean.aliquotStatus.value)
            1 * sampleAliquot.setUdfFailureMode(bean.failureMode.value)
            1 * sampleAliquot.setUdfVolumeUl(bean.finalAliquotVolumeUl)
            1 * sampleAliquot.setUdfConcentrationNgUl(0)

            //1 * containerNode.setProperty('name', bean.destinationBarcode)
            1 * sampleAliquot.setContainerUdfFinalAliquotVolumeUl(bean.finalAliquotVolumeUl)
            1 * sampleAliquot.setContainerUdfFinalAliquotMassNg(bean.finalAliquotMassNg)
            1 * sampleAliquot.setContainerUdfLabResult(bean.aliquotStatus.value)
    }

    SampleAliquotCreationProcess mockProcess(String containerType, String containerLocation = '') {
        ProcessNode processNode = Mock(ProcessNode)
        containerNode = Mock(ContainerNode)
        containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION.udf) >> containerLocation
        if (containerType.equals(PLATE)) {
            containerNode.getIsPlate() >> true
            containerNode.containerType >> PLATE
        } else {
            containerNode.getIsPlate() >> false
            containerNode.containerType >> TUBE
        }
        artifactNode = generateMockArtifactNode()
        artifactNode.getContainerNode() >> containerNode
        processNode.outputAnalytes >> [artifactNode]

        SampleAliquotCreationProcess process = Spy(SampleAliquotCreationProcess, constructorArgs:[processNode])
        process
    }

    ArtifactNode generateMockArtifactNode() {
        def id = generateDummyId(ArtifactNode.class.simpleName)

        ArtifactNode artifactNode = Mock(ArtifactNode)
        artifactNode.getId() >> id
        artifactNode.getName() >> "artifactNode name: ${id}"
        artifactNode
    }

    String generateDummyId( name) {
        return "$name-${IdGenerator.nextId()}"
    }

}
