package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PrePoolingWorksheet
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.*
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import spock.lang.Ignore
import spock.lang.Specification

class PoolCreationAlgoTestSpec extends Specification {
    ArtifactIndexService artifactIndexService

    def setup() {
        artifactIndexService = new ArtifactIndexService()
    }

    def cleanup() {
    }

    void "test PPS-4122 iTag"(){
        setup:
        List<List> data = [
                ['CPTNS','B','2-3167095','27-353734',184,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['CPBPX','B','2-3133741','27-350354',184,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['CPTNN','B','2-3167068','27-353728',184,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
        ]
        Map<Integer, List<LibraryInformation>> beans = [:].withDefault {[]}
        Map<String, Analyte> inputAnalytes = [:]
        when:
        data.each{List fields ->
            LibraryInformation bean = new LibraryInformation()
            bean.libraryName = fields[0]
            bean.analyte = mockAnalyte(ClarityLibraryPool.class, fields, true)
            inputAnalytes[bean.analyte.id] = bean.analyte
            bean.indexName = fields[1]
            bean.analyteLimsId = fields[2]
            bean.containerId = fields[3]
            bean.dop = fields[4]
            bean.platform = fields[5]
            bean.sequencerModel = fields[6]
            bean.runMode = fields[7]
            bean.poolNumber = 1
            beans[bean.poolNumber] << bean
        }
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(inputAnalytes: inputAnalytes)
        def errors = worksheet.validateAllPools(artifactIndexService, beans)
        then:
        assert errors["Error"].find{it.contains("has duplicate indexes")}
    }

    void "test grouping"(){
        setup:
        Map indexMap = ['N704': 'TCCTGAGC', 'IT006': 'GCCAAT', 'IT083': 'GCAAGG', 'Swift717': 'ATCCTTCC', 'bc1001_BAK8A': 'CACATATCAGAGTGCG', 'N704-S505': 'TCCTGAGC-GTAAGGAG', 'N710-S505': 'CGAGGCTG-GTAAGGAG', 'Lex-i7023-i5023': 'CACACT-GACGTC', 'Lex-i7019-i5019': 'ATACTG-CTGCGC']
        Map<String, List<LibraryInformation>> libGroups = [:].withDefault {[]}

        when:
        for(Map.Entry<String, String> entry : indexMap) {
            LibraryInformation li = new LibraryInformation(indexName: entry.key, indexSequence: entry.value)
             li.service = artifactIndexService
            libGroups[li.getValidationKey()] << li
        }

        then:
        assert libGroups.size() == 5
    }

    void "test makePools"(int dop, int unpooled, int poolsCount, List<String> indexes){
        setup:
        PoolingIlluminaDualIndexes pooling = new PoolingIlluminaDualIndexes([])
        pooling.degreeOfPooling = dop
        when:
        List<LibraryInformation> lis = []
        indexes.each{String index ->
            lis << new LibraryInformation(indexSequence: index)
        }
        pooling.members = lis
        pooling.makePools()
        def pools = lis.findAll{it.poolNumber > 0}.groupBy {it.poolNumber}
        then:
        pooling.members.findAll{!it.poolNumber}?.size() == unpooled
        assert pools.size() == poolsCount
        pools.each{
            assert it.value.size() == dop
        }
        where:
        dop     |unpooled   |poolsCount   | indexes
        3       |23         |23           |["CGTACTAG-CTCTCTAT", "AGGCAGAA-CTCTCTAT", "TCCTGAGC-CTCTCTAT", "GGACTCCT-CTCTCTAT", "TAGGCATG-CTCTCTAT", "CTCTCTAC-CTCTCTAT", "CAGAGAGG-CTCTCTAT", "GCTACGCT-CTCTCTAT", "CGAGGCTG-CTCTCTAT", "AAGAGGCA-CTCTCTAT", "TAAGGCGA-TATCCTCT", "CGTACTAG-TATCCTCT", "AGGCAGAA-TATCCTCT", "TCCTGAGC-TATCCTCT", "GGACTCCT-TATCCTCT", "TAGGCATG-TATCCTCT", "CTCTCTAC-TATCCTCT", "CAGAGAGG-TATCCTCT", "GCTACGCT-TATCCTCT", "CGAGGCTG-TATCCTCT", "AAGAGGCA-TATCCTCT", "GTAGAGGA-TATCCTCT", "TAAGGCGA-AGAGTAGA", "CGTACTAG-AGAGTAGA", "AGGCAGAA-AGAGTAGA", "TCCTGAGC-AGAGTAGA", "GGACTCCT-AGAGTAGA", "TAGGCATG-AGAGTAGA", "CTCTCTAC-AGAGTAGA", "CAGAGAGG-AGAGTAGA", "GCTACGCT-AGAGTAGA", "CGAGGCTG-AGAGTAGA", "AAGAGGCA-AGAGTAGA", "GTAGAGGA-AGAGTAGA", "TAAGGCGA-GTAAGGAG", "CGTACTAG-GTAAGGAG", "AGGCAGAA-GTAAGGAG", "TCCTGAGC-GTAAGGAG", "GGACTCCT-GTAAGGAG", "TAGGCATG-GTAAGGAG", "CTCTCTAC-GTAAGGAG", "CAGAGAGG-GTAAGGAG", "GCTACGCT-GTAAGGAG", "CGAGGCTG-GTAAGGAG", "AAGAGGCA-GTAAGGAG", "GTAGAGGA-GTAAGGAG", "TAAGGCGA-ACTGCATA", "CGTACTAG-ACTGCATA", "AGGCAGAA-ACTGCATA", "TCCTGAGC-ACTGCATA", "GGACTCCT-ACTGCATA", "TAGGCATG-ACTGCATA", "CTCTCTAC-ACTGCATA", "CAGAGAGG-ACTGCATA", "GCTACGCT-ACTGCATA", "CGAGGCTG-ACTGCATA", "AAGAGGCA-ACTGCATA", "GTAGAGGA-ACTGCATA", "TAAGGCGA-AAGGAGTA", "CGTACTAG-AAGGAGTA", "AGGCAGAA-AAGGAGTA", "TCCTGAGC-AAGGAGTA", "GGACTCCT-AAGGAGTA", "TAGGCATG-AAGGAGTA", "CTCTCTAC-AAGGAGTA", "CAGAGAGG-AAGGAGTA", "GCTACGCT-AAGGAGTA", "CGAGGCTG-AAGGAGTA", "AAGAGGCA-AAGGAGTA", "GTAGAGGA-AAGGAGTA", "TAAGGCGA-CTAAGCCT", "CGTACTAG-CTAAGCCT", "AGGCAGAA-CTAAGCCT", "TCCTGAGC-CTAAGCCT", "GGACTCCT-CTAAGCCT", "TAGGCATG-CTAAGCCT", "CTCTCTAC-CTAAGCCT", "CAGAGAGG-CTAAGCCT", "GCTACGCT-CTAAGCCT", "CGAGGCTG-CTAAGCCT", "AAGAGGCA-CTAAGCCT", "GTAGAGGA-CTAAGCCT", "CGTACTAG-GCGTAAGA", "AGGCAGAA-GCGTAAGA", "TCCTGAGC-GCGTAAGA", "GGACTCCT-GCGTAAGA", "TAGGCATG-GCGTAAGA", "CTCTCTAC-GCGTAAGA", "CAGAGAGG-GCGTAAGA", "GCTACGCT-GCGTAAGA", "CGAGGCTG-GCGTAAGA", "AAGAGGCA-GCGTAAGA"]
        8       |4          |11           |["CGTACTAG-CTCTCTAT", "AGGCAGAA-CTCTCTAT", "TCCTGAGC-CTCTCTAT", "GGACTCCT-CTCTCTAT", "TAGGCATG-CTCTCTAT", "CTCTCTAC-CTCTCTAT", "CAGAGAGG-CTCTCTAT", "GCTACGCT-CTCTCTAT", "CGAGGCTG-CTCTCTAT", "AAGAGGCA-CTCTCTAT", "TAAGGCGA-TATCCTCT", "CGTACTAG-TATCCTCT", "AGGCAGAA-TATCCTCT", "TCCTGAGC-TATCCTCT", "GGACTCCT-TATCCTCT", "TAGGCATG-TATCCTCT", "CTCTCTAC-TATCCTCT", "CAGAGAGG-TATCCTCT", "GCTACGCT-TATCCTCT", "CGAGGCTG-TATCCTCT", "AAGAGGCA-TATCCTCT", "GTAGAGGA-TATCCTCT", "TAAGGCGA-AGAGTAGA", "CGTACTAG-AGAGTAGA", "AGGCAGAA-AGAGTAGA", "TCCTGAGC-AGAGTAGA", "GGACTCCT-AGAGTAGA", "TAGGCATG-AGAGTAGA", "CTCTCTAC-AGAGTAGA", "CAGAGAGG-AGAGTAGA", "GCTACGCT-AGAGTAGA", "CGAGGCTG-AGAGTAGA", "AAGAGGCA-AGAGTAGA", "GTAGAGGA-AGAGTAGA", "TAAGGCGA-GTAAGGAG", "CGTACTAG-GTAAGGAG", "AGGCAGAA-GTAAGGAG", "TCCTGAGC-GTAAGGAG", "GGACTCCT-GTAAGGAG", "TAGGCATG-GTAAGGAG", "CTCTCTAC-GTAAGGAG", "CAGAGAGG-GTAAGGAG", "GCTACGCT-GTAAGGAG", "CGAGGCTG-GTAAGGAG", "AAGAGGCA-GTAAGGAG", "GTAGAGGA-GTAAGGAG", "TAAGGCGA-ACTGCATA", "CGTACTAG-ACTGCATA", "AGGCAGAA-ACTGCATA", "TCCTGAGC-ACTGCATA", "GGACTCCT-ACTGCATA", "TAGGCATG-ACTGCATA", "CTCTCTAC-ACTGCATA", "CAGAGAGG-ACTGCATA", "GCTACGCT-ACTGCATA", "CGAGGCTG-ACTGCATA", "AAGAGGCA-ACTGCATA", "GTAGAGGA-ACTGCATA", "TAAGGCGA-AAGGAGTA", "CGTACTAG-AAGGAGTA", "AGGCAGAA-AAGGAGTA", "TCCTGAGC-AAGGAGTA", "GGACTCCT-AAGGAGTA", "TAGGCATG-AAGGAGTA", "CTCTCTAC-AAGGAGTA", "CAGAGAGG-AAGGAGTA", "GCTACGCT-AAGGAGTA", "CGAGGCTG-AAGGAGTA", "AAGAGGCA-AAGGAGTA", "GTAGAGGA-AAGGAGTA", "TAAGGCGA-CTAAGCCT", "CGTACTAG-CTAAGCCT", "AGGCAGAA-CTAAGCCT", "TCCTGAGC-CTAAGCCT", "GGACTCCT-CTAAGCCT", "TAGGCATG-CTAAGCCT", "CTCTCTAC-CTAAGCCT", "CAGAGAGG-CTAAGCCT", "GCTACGCT-CTAAGCCT", "CGAGGCTG-CTAAGCCT", "AAGAGGCA-CTAAGCCT", "GTAGAGGA-CTAAGCCT", "CGTACTAG-GCGTAAGA", "AGGCAGAA-GCGTAAGA", "TCCTGAGC-GCGTAAGA", "GGACTCCT-GCGTAAGA", "TAGGCATG-GCGTAAGA", "CTCTCTAC-GCGTAAGA", "CAGAGAGG-GCGTAAGA", "GCTACGCT-GCGTAAGA", "CGAGGCTG-GCGTAAGA", "AAGAGGCA-GCGTAAGA"]
        3       |0          |2            |["0010101101011100", "1101010010100011", "0100010100100011", "1111100011011100", "1011101100011001", "0010011011000110"]
    }

    void "test splitMembers"(){
        setup:
        List<String> indexes = ["CGTACTAG-CTCTCTAT", "AGGCAGAA-CTCTCTAT", "TCCTGAGC-CTCTCTAT", "GGACTCCT-CTCTCTAT", "TAGGCATG-CTCTCTAT", "CTCTCTAC-CTCTCTAT", "CAGAGAGG-CTCTCTAT", "GCTACGCT-CTCTCTAT", "CGAGGCTG-CTCTCTAT", "AAGAGGCA-CTCTCTAT", "TAAGGCGA-TATCCTCT", "CGTACTAG-TATCCTCT", "AGGCAGAA-TATCCTCT", "TCCTGAGC-TATCCTCT", "GGACTCCT-TATCCTCT", "TAGGCATG-TATCCTCT", "CTCTCTAC-TATCCTCT", "CAGAGAGG-TATCCTCT", "GCTACGCT-TATCCTCT", "CGAGGCTG-TATCCTCT", "AAGAGGCA-TATCCTCT", "GTAGAGGA-TATCCTCT", "TAAGGCGA-AGAGTAGA", "CGTACTAG-AGAGTAGA", "AGGCAGAA-AGAGTAGA", "TCCTGAGC-AGAGTAGA", "GGACTCCT-AGAGTAGA", "TAGGCATG-AGAGTAGA", "CTCTCTAC-AGAGTAGA", "CAGAGAGG-AGAGTAGA", "GCTACGCT-AGAGTAGA", "CGAGGCTG-AGAGTAGA", "AAGAGGCA-AGAGTAGA", "GTAGAGGA-AGAGTAGA", "TAAGGCGA-GTAAGGAG", "CGTACTAG-GTAAGGAG", "AGGCAGAA-GTAAGGAG", "TCCTGAGC-GTAAGGAG", "GGACTCCT-GTAAGGAG", "TAGGCATG-GTAAGGAG", "CTCTCTAC-GTAAGGAG", "CAGAGAGG-GTAAGGAG", "GCTACGCT-GTAAGGAG", "CGAGGCTG-GTAAGGAG", "AAGAGGCA-GTAAGGAG", "GTAGAGGA-GTAAGGAG", "TAAGGCGA-ACTGCATA", "CGTACTAG-ACTGCATA", "AGGCAGAA-ACTGCATA", "TCCTGAGC-ACTGCATA", "GGACTCCT-ACTGCATA", "TAGGCATG-ACTGCATA", "CTCTCTAC-ACTGCATA", "CAGAGAGG-ACTGCATA", "GCTACGCT-ACTGCATA", "CGAGGCTG-ACTGCATA", "AAGAGGCA-ACTGCATA", "GTAGAGGA-ACTGCATA", "TAAGGCGA-AAGGAGTA", "CGTACTAG-AAGGAGTA", "AGGCAGAA-AAGGAGTA", "TCCTGAGC-AAGGAGTA", "GGACTCCT-AAGGAGTA", "TAGGCATG-AAGGAGTA", "CTCTCTAC-AAGGAGTA", "CAGAGAGG-AAGGAGTA", "GCTACGCT-AAGGAGTA", "CGAGGCTG-AAGGAGTA", "AAGAGGCA-AAGGAGTA", "GTAGAGGA-AAGGAGTA", "TAAGGCGA-CTAAGCCT", "CGTACTAG-CTAAGCCT", "AGGCAGAA-CTAAGCCT", "TCCTGAGC-CTAAGCCT", "GGACTCCT-CTAAGCCT", "TAGGCATG-CTAAGCCT", "CTCTCTAC-CTAAGCCT", "CAGAGAGG-CTAAGCCT", "GCTACGCT-CTAAGCCT", "CGAGGCTG-CTAAGCCT", "AAGAGGCA-CTAAGCCT", "GTAGAGGA-CTAAGCCT", "CGTACTAG-GCGTAAGA", "AGGCAGAA-GCGTAAGA", "TCCTGAGC-GCGTAAGA", "GGACTCCT-GCGTAAGA", "TAGGCATG-GCGTAAGA", "CTCTCTAC-GCGTAAGA", "CAGAGAGG-GCGTAAGA", "GCTACGCT-GCGTAAGA", "CGAGGCTG-GCGTAAGA", "AAGAGGCA-GCGTAAGA"]
        PoolingIlluminaDualIndexes pooling = new PoolingIlluminaDualIndexes([])
        pooling.degreeOfPooling = 8
        when:
        List<LibraryInformation> lis = []
        indexes.each{String index ->
            lis << new LibraryInformation(indexSequence: index)
        }
        pooling.members = lis
        pooling.reqdPoolsCount = pooling.members.size() / pooling.degreeOfPooling
        List<List<LibraryInformation>> split = pooling.splitMembers(lis)
        then:
        assert split.size() == 4
        assert split[1].size() == 11
        assert split[2].size() == 11
        assert split[3].size() == 11
    }

    @Ignore
    void "test ITag pools"(int startPoolNum, List<LibraryInformation> indexes){
        when:
            List<LibraryInformation> data = []
            indexes.eachWithIndex{String indexName, int i ->
                LibraryInformation li = new LibraryInformation()
                li.dop = 2
                li.indexName = indexName
                data << li
            }
        Pooling pooling = new PoolingItagPools(data)
            int startNumber = pooling.completePoolPrepBeans(1)
        then:
            assert startNumber == startPoolNum
        where:
        startPoolNum    | indexes
        2               | ['A', 'B']
        3               | ['A','A','B','B']
        1               | ['A','A']
        2               | ['A','A','B']
    }

    @Ignore
    void "test exome pools"(List<String> indexes){
        when:
        List<LibraryInformation> data = prepareData(indexes, 5)
        Pooling pooling = new PoolingPoolsByDOP(data)
        int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == 1
        assert !data.findAll{it.poolNumber != null}
        where:
        indexes << [['TGACCA', 'CAGATC', 'ACTTGA', 'GATCAG', 'TAGCTT', 'GGCTAC', 'AGTCAA', 'AGTCAA', 'CTTGTA'],
                    ['CGGCTATG-ATAGAGGC', 'TAATGCGC-AGGCGAAG', 'TCCGCGAA-TATAGCCT', 'TCCGCGAA-GGCTCTGA', 'CGGCTATG-TATAGCCT', 'CTGAAGCT-TAATCTTA', 'TCCGGAGA-AGGCGAAG', 'GAATTCGT-TATAGCCT', 'ATTCAGAA-ATAGAGGC']]
    }

    void "test pooling pools by DOP"(List<String> indexes){
        when:
        List<LibraryInformation> data = prepareData(indexes, 5)
        Pooling pooling = new PoolingPoolsByDOP(data)
        int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == 1
        assert !data.findAll{it.poolNumber != null}
        where:
        indexes << [['TGACCA', 'CAGATC', 'ACTTGA', 'GATCAG', 'TAGCTT', 'GGCTAC', 'AGTCAA', 'AGTCAA', 'CTTGTA'],
                    ['CGGCTATG-ATAGAGGC', 'TAATGCGC-AGGCGAAG', 'TCCGCGAA-TATAGCCT', 'TCCGCGAA-GGCTCTGA', 'CGGCTATG-TATAGCCT', 'CTGAAGCT-TAATCTTA', 'TCCGGAGA-AGGCGAAG', 'GAATTCGT-TATAGCCT', 'ATTCAGAA-ATAGAGGC']]
    }

    void "test pacbio pools"(List<String> indexes){
        when:
        List<LibraryInformation> data = prepareData(indexes, 5)
        Pooling pooling = new PoolingPacBioSingleIndexes(data)
        int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == 1
        assert !data.findAll{it.poolNumber != null}
        where:
        indexes << [['TGACCA', 'CAGATC', 'ACTTGA', 'GATCAG', 'TAGCTT', 'GGCTAC', 'AGTCAA', 'AGTCAA', 'CTTGTA'],
                    ['CACATATCAGAGTGCG', 'ACACACAGACTGTGAG', 'CACGCACACACGCGCG', 'ACAGTCGAGCGCTGCG', 'ACACACGCGAGACAGA', 'ACGCGCTATCTCAGAG', 'ACACTAGATCGCGTGT', 'CTCACTACGCGCGCGT', 'CGCATGACACGTGTGT']]
    }

    void "test Illumina dual index"(int dop, int startPoolNumber, List<String> indexes){
        when:
            List<LibraryInformation> data = prepareData(indexes, dop)
        Pooling pooling = new PoolingIlluminaDualIndexes(data)
            int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == startPoolNumber
        where:
        dop     | startPoolNumber   | indexes
        5       | 2                 | ['CGGCTATG-ATAGAGGC', 'TAATGCGC-AGGCGAAG', 'TCCGCGAA-TATAGCCT', 'TCCGCGAA-GGCTCTGA', 'CGGCTATG-TATAGCCT', 'CTGAAGCT-TAATCTTA', 'TCCGGAGA-AGGCGAAG', 'GAATTCGT-TATAGCCT', 'ATTCAGAA-ATAGAGGC']
        5       | 2                 | ['CGGCTATG-ATAGAGGC', 'CGGCTATG-ATAGAGGC', 'CGGCTATG-ATAGAGGC', 'TCCGCGAA-GGCTCTGA', 'CGGCTATG-TATAGCCT', 'CTGAAGCT-TAATCTTA', 'TCCGGAGA-AGGCGAAG', 'CGGCTATG-ATAGAGGC', 'ATTCAGAA-ATAGAGGC']
        4       | 4                 | ["GTAGAGGA-CTCTCTAT", "GTAGAGGA-CTAAGCCT", "CGTACTAG-CTCTCTAT", "CGTACTAG-CTCTCTAT", "GAATTCGT-GGCTCTGA", "TCCTGAGC-AGAGTAGA", "ATTACTCG-CCTATCCT", "GAGATTCC-TAATCTTA", "CGTACTAG-CTCTCTAT", "CGCTCATT-AGGCGAAG", "ATTCAGAA-CCTATCCT", "GGACTCCT-GCGTAAGA", "TAGGCATG-CTCTCTAT"]
        3       | 3                 | ["GACTTAGA-ACCATGCT","AGACCTCG-TTGGACTC","ATTACTTG-GAATTCCT","ATTACTTG-GAATTGCT","AGACCTCG-TTGGACTC","GACTTAGA-ACCATCCT"]
        4       | 2                 | ["GACTTAGA-ACCATGCT","AGTCATCT-GGTGACTC","TCCTAGAG-GCTACTGA","AGTCGCTC-ATCGTACT","ACGCATCC-TAGAGAAT","CATAGTAG-AAGATCCT"]
        4       | 2                 | ["GACTTA-ACCATG","AGTCAT-GGTGAC","TCCTAG-GCTACT","AGTCGC-ATCGTA","ACGCAT-TAGAGA","CATAGT-AAGATC"]
        9       | 3                 | ["TAAGGCGA-CTCTCTAT", "TAAGGCGA-CTCTCTAT", "TAAGGCGA-TATCCTCT", "TAAGGCGA-TATCCTCT", "TAAGGCGA-AGAGTAGA", "TAAGGCGA-AGAGTAGA", "CGTACTAG-CTCTCTAT", "CGTACTAG-CTCTCTAT", "CGTACTAG-TATCCTCT", "CGTACTAG-TATCCTCT", "CGTACTAG-AGAGTAGA", "AGGCAGAA-CTCTCTAT", "AGGCAGAA-TATCCTCT", "AGGCAGAA-AGAGTAGA", "TCCTGAGC-CTCTCTAT", "TCCTGAGC-CTCTCTAT", "TCCTGAGC-TATCCTCT", "TCCTGAGC-TATCCTCT", "TCCTGAGC-AGAGTAGA", "TCCTGAGC-AGAGTAGA", "GGACTCCT-CTCTCTAT", "GGACTCCT-TATCCTCT", "GGACTCCT-AGAGTAGA", "TAGGCATG-CTCTCTAT", "TAGGCATG-TATCCTCT", "TAGGCATG-AGAGTAGA"]
        9       | 2                 | ["CGGCTATG-ATAGAGGC", "TAATGCGC-AGGCGAAG", "TCCGCGAA-TATAGCCT", "TCCGCGAA-GGCTCTGA", "CGGCTATG-TATAGCCT", "CTGAAGCT-TAATCTTA", "TCCGGAGA-AGGCGAAG", "GAATTCGT-TATAGCCT", "ATTCAGAA-ATAGAGGC"]
        3       | 3                 | ["GACTTAGA-ACCATGCT", "AGACCTCG-TTGGACTC", "ATTACTTG-GAATTCCT", "ATTACTTG-GAATTGCT", "AGACCTCG-TTGGACTC", "GACTTAGA-ACCATCCT"]
        4       | 2                 | ["GACTTAGA-ACCATGCT", "AGTCATCT-GGTGACTC", "TCCTAGAG-GCTACTGA", "AGTCGCTC-ATCGTACT", "ACGCATCC-TAGAGAAT", "CATAGTAG-AAGATCCT"]
    }

    void "test Illumina Single index"(String repIndex, List<String> indexes){
        when:
        List<LibraryInformation> data = prepareData(indexes, 5)
        Pooling pooling = new PoolingIlluminaSingleIndexes(data)
        int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == 2
        if(repIndex)
            assert data.findAll{it.indexSequence == repIndex}.collect{it.poolNumber} == [1, null, null]
        where:
        repIndex     |   indexes
        ''           |   ['TGACCA', 'CAGATC', 'ACTTGA', 'GATCAG', 'TAGCTT', 'GGCTAC', 'AGTCAA', 'AGTCAA', 'CTTGTA']
        'TGACCA'     |   ['TGACCA', 'TGACCA', 'TGACCA', 'GATCAG', 'TAGCTT', 'GGCTAC', 'AGTCAA', 'AGTCAA', 'CTTGTA']
    }

    void "test PacBio Dual Index Validation" (boolean result, List<String> indexes) {
        when:
        List<LibraryInformation> data = prepareData(indexes, 3)
        data.eachWithIndex { LibraryInformation li, int i ->
            li.poolNumber = 1
            li.indexName = indexes[i]
        }
        Pooling pooling = new PoolingPacBioDualIndexes(data)
        Map errors = pooling.validate()
        then:
        errors.isEmpty() == result
        where:
        result  |   indexes
        false   |   ["bc1001_BAK8A--bc1001_BAK8A", "bc1001_BAK8A_OA--bc1001_BAK8A_OA", "bc1001_IsoSeq--bc1001_IsoSeq"]
        true    |   ["bc1002_BAK8A--bc1002_BAK8A", "bc1011_BAK8A_OA--bc1011_BAK8A_OA", "bc1019_IsoSeq--bc1019_IsoSeq"]
    }

    void "test get pooling technique"(String key, Class poolingClass){
        when:
        Pooling pooling = PoolingFactory.getPoolingTechnique(key, [new LibraryInformation(dop: 5)])
        then:
        assert pooling.class == poolingClass
        where:
        key                                                                                | poolingClass
        '48 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150'                                 | PoolingIlluminaSingleIndexes.class
        '48 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 isDual 16'                       | PoolingIlluminaDualIndexes.class
        '184 Illumina MiSeq Illumina MiSeq 1 X 50 iTag'                                    | PoolingItagPools.class
        '20 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 ExomeCapture'                    | PoolingPoolsByDOP.class
        '20 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 ExomeCapture isDual 12'          | PoolingPoolsByDOP.class
        '20 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 PoolByDOP'                       | PoolingPoolsByDOP.class
        '20 Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 PoolByDOP isDual 12'             | PoolingPoolsByDOP.class
        'InternalSingleCell 1 Container1 Fragment - Internal Selection and Amplification'  | PoolingInternalSingleCell.class
        'FractionalLane Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150'                     | NovaSeqFractionalLanePooling.class
        'FractionalLane Illumina HiSeq-1TB Illumina HiSeq-1Tb 2 X 150 isDual 16'           | NovaSeqFractionalLanePooling.class
        '48 Illumina Sequel PacBio Sequel 1 X 1200'                                        | PoolingPacBioSingleIndexes.class
    }

    void "test pooling compound indexes"(int dop, int startPoolNumber, String key, List<String> indexes){
        when:
        List<LibraryInformation> data = prepareData(indexes, dop)
        Map<String, LaneFraction> laneFractionMap = key.contains(LibraryInformation.FRACTIONAL_LANE)?getLaneFraction(data):null
        Pooling pooling = PoolingFactory.getPoolingTechnique(key, data, laneFractionMap)
        int startNumber = pooling.completePoolPrepBeans(1)
        then:
        assert startNumber == startPoolNumber
        where:
        dop      | startPoolNumber   | key                                                                                   | indexes
        7        | 3                 | "7 Illumina NextSeq-HO Illumina NextSeq-HO 2 X 150 isDual index length-17 "           | ['GAGGAAGA-GAACATGG', 'TGAAGGCA-AGAGGGTT', 'ATCAGCAC-AGGGATGT', 'GAGGAAGA-GAACATGG', 'TCTTAAAG+CGAGGCTC+GTCCTTCT+AAGACGGA-TCTTTCCC', 'AGTGGAAC+GTCTCCTT+TCACATCA+CAGATGGG-TCTTTCCC', 'TCAGCCGT+CAGAGGCC+GGTCAATA+ATCTTTAG-TCTTTCCC', 'CACTCGGA+GCTGAATT+TGAAGTAC+ATGCTCCG-TCTTTCCC', 'ACATTACT+TTTGGGTA+CAGCCCAC+GGCAATGG-TCTTTCCC', 'CCCTAACA+ATTCCGAT+TGGATTGC+GAAGGCTG-TCTTTCCC', 'CAATACCC+TGTCTATG+ACCACGAA+GTGGGTGT-TCTTTCCC', 'CTTTGCGG+TGCACAAA+AAGCAGTC+GCAGTTCT-TCTTTCCC', 'GCACAATG+CTTGGTAC+TGCACCGT+AAGTTGCA-TCTTTCCC', 'AAATGTGC+GGGCAAAT+TCTATCCG+CTCGCGTA-TCTTTCCC', 'GCGAGAGT+TACGTTCA+AGTCCCAC+CTATAGTG-TCTTTCCC', 'TGATTCTA+ACTAGGAG+CAGCCACT+GTCGATGC-TCTTTCCC']
        36       | 2                 | "FractionalLane Illumina NovaSeq Illumina NovaSeq 2 X 150 isDual index length-17 "    | ['AGTGGAAC+GTCTCCTT+TCACATCA+CAGATGGG-TCTTTCCC', 'CACTCGGA+GCTGAATT+TGAAGTAC+ATGCTCCG-TCTTTCCC', 'ACATTACT+TTTGGGTA+CAGCCCAC+GGCAATGG-TCTTTCCC', 'AAATGTGC+GGGCAAAT+TCTATCCG+CTCGCGTA-TCTTTCCC', 'TGATTCTA+ACTAGGAG+CAGCCACT+GTCGATGC-TCTTTCCC']
    }

    Map<String, LaneFraction> getLaneFraction(List<LibraryInformation> data) {
        Map<String, LaneFraction> map = [:]
        Random r = new Random()
        data.eachWithIndex { LibraryInformation li, int i ->
            def val = Math.abs(r.nextInt(100)) * 0.001
            map[li.analyte.name] = new LaneFraction(laneFraction: val, sowItemId: 12345 + i, sequencingOptimizationFactor: val)
        }
        return map
    }

    List<LibraryInformation> prepareData(List<String> indexes, int dop){
        List<LibraryInformation> data = []
        Random r =  new Random()
        indexes.eachWithIndex { String index, int i ->
            LibraryInformation li = new LibraryInformation()
            li.analyteLimsId = "analyte${(i+1)}"
            li.indexName = "index${(i+1)}"
            li.indexSequence = index
            li.dop = dop
            li.analyte = mockAnalyte(ClarityLibraryStock.class, ["analyte${(i+1)}", "container${(i+1)}"])
            li.analyte.name >> "name${(i+1)}"
            li.libraryPercentage = Math.abs(r.nextInt(100)) * 0.001
            data << li
        }
        return data
    }

    Analyte mockAnalyte(Class clazz, fields, boolean isItag = false){
        Analyte analyte = Mock(clazz)
        analyte.id >> fields[2]
        analyte.containerId >> fields[3]
        analyte.isItag >> isItag
        return analyte
    }
}