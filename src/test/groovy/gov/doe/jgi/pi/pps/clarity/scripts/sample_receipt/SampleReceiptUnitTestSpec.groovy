package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel.SampleReceiptTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import spock.lang.Specification

class SampleReceiptUnitTestSpec extends Specification {
    ActionHandler actionHandler
    Random random
    SequencingProject project

    def setup() {
        random = new Random()
    }

    def cleanup() {
    }

    void "test update routing"(){
        setup:
        SampleReceiptTableBean bean = new SampleReceiptTableBean()
        bean.receiptStatus = new DropDownList()
        actionHandler = new ReceiveSamples()
        when:
        PmoSample pmoSample = mockSample()
        SampleAnalyte sampleAnalyte = pmoSample.sampleAnalyte
        bean.receiptStatus.value = Analyte.PASS
        project.udfMaterialCategory = MaterialCategoryCv.DNA.materialCategory
        pmoSample.udfStopAtReceipt >> false
        def routingRequests = actionHandler.updateRoutingRequests(bean, sampleAnalyte, true)
        then:
        assert routingRequests
        assert routingRequests.action == Routing.Action.assign
        assert routingRequests.routingUri == Stage.SAMPLE_QC_DNA.value
        when:
        pmoSample = mockSample()
        sampleAnalyte = pmoSample.sampleAnalyte
        bean.receiptStatus.value = Analyte.PASS
        project.udfMaterialCategory = MaterialCategoryCv.RNA.materialCategory
        pmoSample.udfStopAtReceipt >> false
        routingRequests = actionHandler.updateRoutingRequests(bean, sampleAnalyte, true)
        then:
        assert routingRequests
        assert routingRequests.action == Routing.Action.assign
        assert routingRequests.routingUri == Stage.SAMPLE_QC_RNA.value
    }

    void "test updateSampleMap"(){
        setup:
        SampleReceiptTableBean bean = new SampleReceiptTableBean()
        bean.receiptStatus = new DropDownList()
        actionHandler = new ReceiveSamples()
        when:
        PmoSample pmoSample1 = mockSample()
        bean.receiptStatus.value = Analyte.PASS
        pmoSample1.udfStopAtReceipt >> true
        actionHandler.updateStatusMap(bean, pmoSample1)
        then:
        actionHandler.sampleStatusCvMap[pmoSample1.pmoSampleId] == SampleStatusCv.AVAILABLE_FOR_USE
        when:
        PmoSample pmoSample2 = mockSample()
        pmoSample2.udfStopAtReceipt >> false
        actionHandler.updateStatusMap(bean, pmoSample2)
        then:
        actionHandler.sampleStatusCvMap[pmoSample2.pmoSampleId] == SampleStatusCv.AWAITING_SAMPLE_QC
        when:
        pmoSample2 = mockSample()
        pmoSample2.udfStopAtReceipt >> ""
        actionHandler.updateStatusMap(bean, pmoSample2)
        then:
        actionHandler.sampleStatusCvMap[pmoSample2.pmoSampleId] == SampleStatusCv.AWAITING_SAMPLE_QC
        when:
        PmoSample pmoSample3 = mockSample()
        bean.receiptStatus.value = Analyte.FAIL
        actionHandler.updateStatusMap(bean, pmoSample3)
        then:
        actionHandler.sampleStatusCvMap[pmoSample3.pmoSampleId] == SampleStatusCv.ABANDONED
        actionHandler.sampleReceivedStatusMap.size() == 4
        actionHandler.passedSamples.size() == 3
        actionHandler.failedSamples.size() == 1
    }

    PmoSample mockSample(){
        SampleAnalyte analyte = Mock(SampleAnalyte)
        PmoSample pmoSample = Mock(PmoSample)
        analyte.claritySample >> pmoSample
        pmoSample.sampleAnalyte >> analyte
        pmoSample.pmoSampleId >> random.nextLong()
        project = Mock(SequencingProject)
        pmoSample.sequencingProject >> project
        return pmoSample
    }

}