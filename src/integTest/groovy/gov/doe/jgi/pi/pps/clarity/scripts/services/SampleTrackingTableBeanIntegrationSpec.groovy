package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.sample_receipt.SampleTrackingTableBean
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleTrackingTableBeanIntegrationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager

    def setup() {
    }

    def cleanup() {
    }

    void "test validate Bean"(String barcode, String status, Long operatorId, String error) {
        expect:
        clarityNodeManager
        when:
        SampleTrackingTableBean bean = new SampleTrackingTableBean(containerBarcode: barcode, status: status, operatorId: operatorId)
        String errors = bean.validateBean(clarityNodeManager)
        then:
        assert errors.contains(error)
        where:
        barcode     | status                                | operatorId        | error
        null        | SampleTrackingService.STATUS_ONSITE  | 2633 | 'Barcode is a required field'
        ''          | SampleTrackingService.STATUS_SHIPPED | 2633 | 'Barcode is a required field'
        'barcode'   | null                                  | 2633              | 'Invalid status'
        'barcode'   | ''                                    | 2633              | 'Invalid status'
        'barcode'   | 'my status'                           | 2633              | 'Invalid status'
        'barcode'   | SampleTrackingService.STATUS_TRASHED | null | 'Operator Id is required'
        'barcode'   | SampleTrackingService.STATUS_TRASHED | 0000 | 'Invalid operator Id'
    }

    void "test add pmoSampleIds"(Class sampleType, int size){
        setup:
        Analyte analyte = Mock(Analyte)
        analyte.claritySample >> Mock(sampleType)
        when:
        SampleTrackingTableBean bean = new SampleTrackingTableBean()
        bean.addPmoSampleId(analyte)
        then:
        assert bean.pmoSampleIds.size() == size
        where:
        sampleType        | size
        PmoSample       | 1
        ScheduledSample | 0

    }

    void "test add routing requests and stage"(String status, int size, Stage stage){
        setup:
        ArtifactNode analyte = Mock(ArtifactNode)
        analyte.activeStages >> [Mock(ArtifactWorkflowStage)]
        when:
        SampleTrackingTableBean bean = new SampleTrackingTableBean(status: status)
        bean.addRoutingRequests(analyte)
        then:
        assert bean.routingRequests.size() == size
        assert bean.stage == stage
        where:
        status                                      | size  | stage
        SampleTrackingService.STATUS_ONSITE  | 0 | Stage.ON_SITE
        SampleTrackingService.STATUS_SHIPPED | 1 | Stage.SHIP_OFFSITE
        SampleTrackingService.STATUS_TRASHED | 1 | Stage.TRASH
    }
}
