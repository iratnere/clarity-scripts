package gov.doe.jgi.pi.pps.clarity.scripts.librarycreation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationPlatePacBio
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryCreationPlatePacBioSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId = '24-183488' //LC PacBio >10kb w/ AMPure Bead Size Selection, Plates
    LibraryCreationProcess clarityProcess

    def setup() {

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new LibraryCreationProcess(processNode)
    }

/*
"SMRTbell Template Prep Kit" is null and "PacBio SMRTbell Template Prep Kit" is null then exception
"SMRTbell Template Prep Kit" is NOT null and "PacBio SMRTbell Template Prep Kit" is NOT null then exception
 */
    def "test validateProcessUdfs exception both specified"() {
        // "SMRTbell Template Prep Kit" = "8888"
        expect:"clarityProcess"
            clarityProcess
        when:
            def expectedUdf = '8888'
            clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()

            def smrtbellTemplatePrepKit = (clarityProcess.outputAnalytes[0] as ClarityLibraryStock).udfSmrtbellTemplatePrepKit
            def pacBioSmrtbellTemplatePrepKit = (clarityProcess.outputAnalytes[0] as ClarityLibraryStock).udfPacBioSmrtbellTemplatePrepKit
        then:
            clarityProcess.lcAdapter instanceof LibraryCreationPlatePacBio
            smrtbellTemplatePrepKit == expectedUdf
            !pacBioSmrtbellTemplatePrepKit
        when:
            clarityProcess.lcAdapter.validateProcessUdfs()
        then:
            noExceptionThrown()
        when:
            clarityProcess.processNode.setUdf(ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT.udf, '0123456789')
            clarityProcess.lcAdapter.validateProcessUdfs()
        then:
        WebException e = thrown()
            e.message.contains('Unable to Complete Work')
            e.message.contains("Both udfs were specified the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf")
    }

    def "test validateProcessUdfs exception both null"() {
        // "SMRTbell Template Prep Kit" = "8888"
        expect:"clarityProcess"
            clarityProcess
        when:
            def expectedUdf = '8888'
            clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()

            def smrtbellTemplatePrepKit = (clarityProcess.outputAnalytes[0] as ClarityLibraryStock).udfSmrtbellTemplatePrepKit
            def pacBioSmrtbellTemplatePrepKit = (clarityProcess.outputAnalytes[0] as ClarityLibraryStock).udfPacBioSmrtbellTemplatePrepKit
        then:
            clarityProcess.lcAdapter instanceof LibraryCreationPlatePacBio
            smrtbellTemplatePrepKit == expectedUdf
            !pacBioSmrtbellTemplatePrepKit
        when:
            clarityProcess.lcAdapter.validateProcessUdfs()
        then:
            noExceptionThrown()
        when:
            clarityProcess.processNode.setUdf(ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT.udf, null)
            clarityProcess.lcAdapter.validateProcessUdfs()
        then:
        WebException e = thrown()
            e.message.contains('Unable to Complete Work')
            e.message.contains("Please specify the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf")
    }
}
