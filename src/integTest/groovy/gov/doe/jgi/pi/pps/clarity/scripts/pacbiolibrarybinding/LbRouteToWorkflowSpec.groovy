package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibrarybinding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import grails.gorm.transactions.Transactional
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import spock.lang.Shared

@RunWith(SpringRunner.class)
@SpringBootTest(classes = [Application.class])
@Transactional
class LbRouteToWorkflowSpec {

    String action = 'RouteToNextWorkflow'

    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId = '24-639980' //PacBio Sequel
    LibraryBinding clarityProcess
    LbRouteToWorkflow actionHandler
    String uriSequelII
    String uriSequel

    @Before
    void setup() {

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new LibraryBinding(processNode)
        actionHandler = clarityProcess.getActionHandler(action) as LbRouteToWorkflow

        uriSequelII = getWorkflowUri(ClarityWorkflow.PACBIO_SEQUEL_II_SEQUENCING_PREP.value)
        uriSequel = getWorkflowUri(ClarityWorkflow.PACBIO_SEQUEL_SEQUENCING_PREP.value)
    }

    @Test
    void "test getUriToOutputIds"() {
        setup:
            List<PacBioBindingComplex> outputs = clarityProcess.getOutputAnalytes() as List<PacBioBindingComplex>
        PacBioBindingComplex bindingComplex1 = outputs.first()
            (bindingComplex1.parentLibrary as ClarityLibraryStock).udfRunMode = 'PacBio Sequel II 1 X 120'
        when:
            Map<String, List<String>> result = LbRouteToWorkflow.getUriToOutputIds(outputs)
        then:
            result[uriSequelII].first() == bindingComplex1.id
            result[uriSequel].first() == outputs[1].id
    }

    @Test
    void "test routeSequelOutputs"() {
        setup:
            List<PacBioBindingComplex> outputs = clarityProcess.getOutputAnalytes() as List<PacBioBindingComplex>
        PacBioBindingComplex bindingComplex1 = outputs.first()
        PacBioBindingComplex bindingComplex2 = outputs[1]
            (bindingComplex1.parentLibrary as ClarityLibraryStock).udfRunMode = 'PacBio Sequel II 1 X 120'
            (bindingComplex2.parentLibrary as ClarityLibraryStock).udfRunMode = 'PacBio Sequel 1 X 120'
        when:
            actionHandler.routeSequelOutputs(outputs)
        then:
            clarityProcess.routingRequests
            // check PacBio Sequel II: bindingComplex1
            def routingRequests1 = clarityProcess.routingRequests.find{it.artifactId == bindingComplex1.id}
            def expected1 = new RoutingRequest(
                    action: Routing.Action.assign,
                    artifactId: bindingComplex1.id,
                    routingUri: uriSequelII
            )
            routingRequests1.action == expected1.action
            routingRequests1.routingUri == expected1.routingUri
            routingRequests1.artifactId == expected1.artifactId
            // check PacBio Sequel: bindingComplex2
            def routingRequests2 = clarityProcess.routingRequests.find{it.artifactId == bindingComplex2.id}
            def expected2 = new RoutingRequest(
                    action: Routing.Action.assign,
                    artifactId: bindingComplex2.id,
                    routingUri: uriSequel
            )
            routingRequests2.action == expected2.action
            routingRequests2.routingUri == expected2.routingUri
            routingRequests2.artifactId == expected2.artifactId
    }

    String getWorkflowUri(String workflowName){
        WorkflowNode workflowNode = clarityNodeManager.getWorkflowNodeByName(workflowName)
        if(!workflowNode)
            throw new RuntimeException("WorkflowNode.notFound: $workflowName")
        return workflowNode.workflowUri
    }

}
