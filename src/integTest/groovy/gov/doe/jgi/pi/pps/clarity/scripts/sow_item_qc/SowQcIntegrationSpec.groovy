package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SowQcIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager
    String processId
    SowItemQc process

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        processId = testUtil.getProcessIds(ProcessType.SM_SOW_ITEM_QC.value).first()
        process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
    }

    def cleanup() {
    }

    @Ignore
    void "execute process actions"(String action){
        setup:
        processId = '24-1162566'
        expect:
        testUtil
        when:
        testUtil.executeAction(ProcessType.SM_SOW_ITEM_QC, action, processId)
        then:
        noExceptionThrown()
        where:
        action << [
                'PreProcessValidation',
                'PrepareSowItemQcSheet',
                'ProcessSowItemQcSheet',
                'RouteToNextWorkflow'
        ]
    }

    void "test prepare SOW QC worksheet"(){
        setup:
        process.testMode = true
        when:
        ActionHandler ah = new PrepareSowItemQcSheet()
        ah.process = process
        ExcelWorkbook workbook = ah.generateSowQCWorksheet()
        List<Section> sections = workbook.sections.values() as List
        List<ContainerNode> sampleContainers = process.inputAnalytes.collect{it.claritySample.pmoSample.sampleAnalyte.containerNode}.unique()
        List<ContainerNode> plates = sampleContainers.findAll{it.containerTypeEnum == ContainerTypes.WELL_PLATE_96}
        int plateSectionCount = 0, kvSectionCount = 0, oneDCount = 0
        plates?.each{
            PmoSample sample = AnalyteFactory.analyteInstance(it.contentsArtifactNodes[0]).claritySample.pmoSample
            int ssCount = process.plateSamples[it.id][sample]?.size()
            plateSectionCount += 3 + ssCount * 3
            kvSectionCount += 1 + ssCount
            oneDCount += 2 * ssCount
        }
        then:
        if(sampleContainers.find{it.containerTypeEnum == ContainerTypes.TUBE})
            assert sections.find{it instanceof TableSection}
        if(plates){
            assert sections.findAll{it instanceof PlateSection}.size() == plateSectionCount
            assert sections.findAll{it instanceof KeyValueSection}.size() == kvSectionCount
            assert sections.findAll{it instanceof OneDSection}.size() == oneDCount
        }
    }
}
