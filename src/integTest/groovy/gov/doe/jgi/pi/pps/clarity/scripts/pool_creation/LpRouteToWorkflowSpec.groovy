package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LpRouteToWorkflowSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    TestUtility testUtil
    ClarityLibraryPool clarityLibraryPool
    @Shared
    def processIds

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        processIds = processIds ? processIds : testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value)
        ClarityProcess process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processIds.first()))
        clarityLibraryPool = (ClarityLibraryPool) process.outputAnalytes[0]
        assert clarityLibraryPool instanceof ClarityLibraryPool
    }

    def cleanup() {
    }

    void "test updateInputs"() {
        setup:
            def actualFl = 13.4323
            def pools = [:].withDefault {[]}
            clarityLibraryPool.poolMembers.each{
                it.setUdfLpActualWithSof(null)
                PoolCreationTableBean bean = new PoolCreationTableBean(
                        analyte: it,
                        lpActualWithSof: actualFl,
                        dilute: 'DILUTE'
                )
                pools[clarityLibraryPool.name] << bean
            }
        expect:
            clarityLibraryPool.poolMembers.each {
                assert it.getUdfLpActualWithSof() == null
            }
        when:
        LpRouteToWorkflow.updateInputs(pools)
        then:
            clarityLibraryPool.poolMembers.each {
                assert it.getUdfLpActualWithSof() == actualFl
            }
    }

    ProcessNode getProcessNode() {
        def processId = processIds.find { nodeId ->
            ProcessNode processNode = clarityNodeManager.getProcessNode(nodeId)
            ArtifactNode output = processNode.outputAnalytes[0]
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(output)
            (!outputAnalyte.isInternalSingleCell && !(outputAnalyte instanceof ClarityPoolLibraryPool) && !outputAnalyte.isPacBio)
        }
        println processId
        return clarityNodeManager.getProcessNode(processId)
    }
}
