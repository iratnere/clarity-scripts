package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import grails.gorm.transactions.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class FreezerServiceSpec extends Specification {

    Logger logger = LoggerFactory.getLogger(this.class.name)
    @Autowired
    FreezerService freezerService
    @Value("\${freezerService.url}")
    String freezerServiceUrl
    List<FreezerContainer> outContainers

    String firstName = 'FreezerService'
    String lastName = 'IntegrationSpec'

    def setup() {
    }

    def cleanup() {
        if (outContainers) {
            try {
                freezerService.checkIn(outContainers, firstName, lastName)
            } catch (t) {
                logger.error "cleanup check-in error:", t
            }
        }
    }

    private List<String> getCheckedInBarcodes() {
        String uri  = "${freezerServiceUrl}?format=xml"
        List<String> barcodes = []
        HTTPBuilder http = new HTTPBuilder()
        http.get(uri:uri, contentType:ContentType.XML) { resp, xml ->
            xml.barcode.each {
                barcodes << it.text()
            }
        }
        return barcodes
    }

    void "test FreezerService"() {
        setup:
            List<String> barcodes = checkedInBarcodes
        expect:
            barcodes
        when:
            List<FreezerContainer> containers = freezerService.lookup(barcodes)
        then:
            noExceptionThrown()
            containers
            containers*.barcode?.sort()?.join(' ') == barcodes.sort()?.join(' ')
        when:
            outContainers = freezerService.checkout(barcodes, firstName, lastName)
        then:
            noExceptionThrown()
            outContainers
            outContainers*.barcode?.sort()?.join(' ') == barcodes.sort()?.join(' ')
            outContainers.each { FreezerContainer container ->
                assert container.checkInStatus == 'out'
            }
        when:
            List<FreezerContainer> inContainers = freezerService.checkIn(outContainers, firstName, lastName)
        then:
            noExceptionThrown()
            inContainers
            outContainers*.barcode?.sort()?.join(' ') == barcodes.sort()?.join(' ')
            inContainers.each { FreezerContainer container ->
                assert container.checkInStatus == 'in'
            }
        }

}
