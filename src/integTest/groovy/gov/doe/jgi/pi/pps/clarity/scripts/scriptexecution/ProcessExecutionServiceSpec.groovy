package gov.doe.jgi.pi.pps.clarity.scripts.scriptexecution

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import grails.gorm.transactions.Transactional
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ProcessExecutionServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }



    void "test ProcessType"() {
        setup:
            String name = 'Size Selection'
        when:
        ProcessType processType = ProcessType.toEnum(name)
        then:
            noExceptionThrown()
            processType
    }
}

