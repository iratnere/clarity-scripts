package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcUtility
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcAdapterIntegrationSpec extends Specification {
    SampleQcAdapter adapter
    Random random
    @Autowired
    ScheduledSampleService scheduledSampleService
    @Autowired
    NodeManager nodeManager

    def setup() {
        random = new Random()
    }

    def cleanup() {
    }

    void "test checkInputSize"() {
        setup:
        List<Analyte> analytes = []
        (0..367).each{
            analytes << Mock(SampleAnalyte)
        }
        adapter = new GenericSampleQC(new SampleQcUtility(analytes, scheduledSampleService), analytes)
        when:
        String error = adapter.checkInputSize(analytes)
        then:
        assert error == ""
        when:
        analytes << Mock(SampleAnalyte)
        error = adapter.checkInputSize(analytes)
        then:
        assert error.contains("Too many input samples.")
    }

    void "test checkIsSample"() {
        setup:
        SampleAnalyte sampleAnalyte1 = Mock(SampleAnalyte)
        adapter = new GenericSampleQC(new SampleQcUtility([sampleAnalyte1], scheduledSampleService), [sampleAnalyte1])
        when:
        PmoSample pmoSample = Mock(PmoSample)
        sampleAnalyte1.claritySample >> pmoSample
        String error = adapter.checkIsSample(sampleAnalyte1)
        then:
        assert error == ""
        when:
        SampleAnalyte sampleAnalyte2 = Mock(SampleAnalyte)
        ScheduledSample scheduledSample = Mock(ScheduledSample)
        sampleAnalyte2.claritySample >> scheduledSample
        error = adapter.checkIsSample(sampleAnalyte2)
        then:
        assert error.contains("Expecting Root samples as input.")
    }

    void "test checkSampleStatus"() {
        setup:
        SampleAnalyte sampleAnalyte = Mock(SampleAnalyte)
        adapter = new GenericSampleQC(new SampleQcUtility([sampleAnalyte], scheduledSampleService), [sampleAnalyte])
        Random r = new Random()
        Long sampleId1 = r.nextLong()
        Long sampleId2 = r.nextLong()
        when:
        Map statusMap = [:]
        statusMap[sampleId1] = SampleStatusCv.AWAITING_SAMPLE_QC.value
        statusMap[sampleId2] = SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value
        List<String> errors = adapter.checkSampleStatus(statusMap)
        then:
        assert errors.size() == 1
        assert errors[0].contains("with current status ${SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value}")
        when:
        statusMap.clear()
        statusMap[sampleId1] = SampleStatusCv.AWAITING_SAMPLE_QC.value
        statusMap[sampleId2] = SampleStatusCv.AWAITING_SAMPLE_QC.value
        errors = adapter.checkSampleStatus(statusMap)
        then:
        assert !errors
    }

    void "test checkContainerContents"() {
        setup:
        SampleAnalyte sampleAnalyte1 = Mock(SampleAnalyte)
        adapter = new GenericSampleQC(new SampleQcUtility([sampleAnalyte1], scheduledSampleService),[sampleAnalyte1])
        ContainerNode containerNode = Mock(ContainerNode)
        List analyteIds = []
        (1..5).each{
            analyteIds << "${random.nextLong()}"
        }
        containerNode.contentsIds >> analyteIds
        when:
        Map map = [:]
        map[containerNode] = analyteIds
        String error = adapter.checkContainerContents(map)
        then:
        assert error == ""
        when:
        map.clear()
        def ids = []
        ids.addAll(analyteIds[0..3])
        map[containerNode] = ids
        error = adapter.checkContainerContents(map)
        then:
        assert error.contains("All samples from the input containers must be placed on the qc plate.")
    }
}
