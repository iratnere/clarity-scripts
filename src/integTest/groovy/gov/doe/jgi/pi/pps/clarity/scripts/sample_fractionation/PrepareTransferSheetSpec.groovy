package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.FractionsTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.PrepareTransferSheet
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.SampleFractionationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class PrepareTransferSheetSpec extends Specification {

    static final String action = 'PrepareTransferSheet'
    @Autowired
    NodeManager clarityNodeManager

    @Autowired
    Environment environment

    String processId

    def setup() {
        processId = environment.activeProfiles.find{true} == 'claritydev1' ? '24-911723' : '24-911623'
    }

    def cleanup() {
    }

    void "test process getFractionsBeans"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        clarityProcess.testMode = true
        PrepareTransferSheet actionHandler = clarityProcess.getActionHandler(action) as PrepareTransferSheet
        when:
        List<FractionsTableBean> beans = actionHandler.getFractionsBeans(clarityProcess.outputAnalytes)
        then:
        beans.each{ FractionsTableBean bean ->
            Analyte fraction = bean.sourceAnalyte
            assert fraction
            boolean sourceStatus = PrepareTransferSheet.getSourceStatus(
                    fraction.udfConcentrationNgUl,
                    clarityProcess.udfProcessMinDnaConcNgUl)
            assert bean.sampleLimsId == fraction.id
            assert bean.sourceBarcode == fraction.containerName
            assert bean.sourceLabware == ContainerTypes.WELL_PLATE_96.value
            assert bean.sourceLocation == fraction.containerLocation.wellLocation
            assert bean.dnaConcentrationNgUl == fraction.udfConcentrationNgUl
            assert bean.density == fraction.udfFractionDensity
            assert bean.sourceStatus.value == PrepareTransferSheet.getDropDownPassFail(sourceStatus).value
            assert bean.transferredVolumeUl == SampleFractionationProcess.DEFAULT_VOLUME_UL
            assert bean.destinationLabware == ContainerTypes.TUBE.value
        }
        noExceptionThrown()
    }
    @Ignore
    void "test process PrepareTransferSheet"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        clarityProcess.testMode = false
        PrepareTransferSheet actionHandler = clarityProcess.getActionHandler(action) as PrepareTransferSheet
        when:
        //actionHandler.execute()
        actionHandler.uploadTransferWorksheet(SampleFractionationProcess.SCRIPT_GENERATED_TRANSFER_SHEET)
        then:
        noExceptionThrown()
    }
}