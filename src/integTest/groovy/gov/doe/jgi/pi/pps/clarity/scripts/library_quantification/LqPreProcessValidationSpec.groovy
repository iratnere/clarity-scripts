package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LqPreProcessValidationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager

    def setup() {
    }

    def cleanup() {
    }

    void "test validateAnalytes"() {
        setup: "using production ids"
            def itagPoolId = '2-3854479' //iTag
        ClarityLibraryPool itagPool = AnalyteFactory.analyteInstance(
                    clarityNodeManager.getArtifactNode(itagPoolId)
            ) as ClarityLibraryPool

            def poolId = '2-3705697'
        ClarityLibraryPool clarityLibraryPool = AnalyteFactory.analyteInstance(
                    clarityNodeManager.getArtifactNode(poolId)
            ) as ClarityLibraryPool //LP 2-3164470

            def libraryId = '2-3964972'
            def aliquotId = '2-3141102'
        ClarityLibraryStock clarityLibraryStock = AnalyteFactory.analyteInstance(
                    clarityNodeManager.getArtifactNode(libraryId)
            ) as ClarityLibraryStock //LS 2-3964972
        ClaritySampleAliquot claritySampleAliquot = AnalyteFactory.analyteInstance(
                    clarityNodeManager.getArtifactNode(aliquotId)
            ) as ClaritySampleAliquot //SA 2-3141102
        when:
        LqPreProcessValidation.validateAnalytes([
                    itagPool,
                    clarityLibraryPool,
                    clarityLibraryStock,
                    claritySampleAliquot
            ])
        then:
            def e = thrown(WebException)
            e.message.contains('ClarityLibraryPool(2-3705697)')
            e.message.contains('Library Molarity QC (pm) is required')
            e.message.contains('Concentration (ng/uL) is required')
            e.message.contains('Actual Template Size (bp) is required')
        when: "correct clarityLibraryPool"
            clarityLibraryPool.udfLibraryMolarityQcPm = 123.123 //need to set for a random pool on production
            clarityLibraryPool.udfConcentrationNgUl = 123.123
            clarityLibraryPool.udfActualTemplateSizeBp = 123.123
        LqPreProcessValidation.validateAnalytes([
                    itagPool,
                    clarityLibraryPool,
                    clarityLibraryStock,
                    claritySampleAliquot
            ])
        then:
            def e1 = thrown(WebException)
            e1.message.contains('ClaritySampleAliquot(2-3141102)')
            e1.message.contains('is not ClarityLibraryPool or ClarityLibraryStock')
        when: "remove ClaritySampleAliquot"
        LqPreProcessValidation.validateAnalytes([
                    itagPool,
                    clarityLibraryPool,
                    clarityLibraryStock
            ])
        then:
        noExceptionThrown()
    }
}
