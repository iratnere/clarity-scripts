package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class PreparePoolPrepSheetIntegrationSpec extends Specification {
    static final logger = LoggerFactory.getLogger(PreparePoolPrepSheetIntegrationSpec.class)
    TestUtility testUtil
    @Autowired
    NodeManager nodeManager

    def setup() {
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "test prepare Pre-pooling sheet"() {
        setup:
            def processid = '122-943048'
        ProcessNode processNode = nodeManager.getProcessNode(processid)
        PoolCreation process = ProcessFactory.processInstance(processNode) as PoolCreation
        //LibraryPercentageService libraryPercentageService = (LibraryPercentageService) ClarityWebTransaction.requireCurrentTransaction().requireApplicationBean(LibraryPercentageService)
        //process.libraryPercentageService = libraryPercentageService
        //LpPreparePoolingPrepSheet actionHandler = new LpPreparePoolingPrepSheet()
            //actionHandler.process = ProcessFactory.processInstance(processNode)
            process.testMode = true
        when:
/*        nodeManager.getContainerNode('27-467029').getContentsArtifactNodes().each{
            it.setUdf(ClarityUdf.ANALYTE_RUN_MODE.udf, 'Illumina NovaSeq 2 X 150')
        }
        nodeManager.httpPutDirtyNodes()*/
            logger.info "Starting ${this.class.name} action...."
            process.batchSamples()
            process.threshold = PoolCreation.DEFAULT_THRESHOLD
            // parse analytes and populate the excel template beans accordingly
            def pools = process.preparePools()
            def fileNode = processNode.outputResultFiles.find{
                it.name.contains(PoolCreation.SCRIPT_GENERATED_POOLING_PREP_SHEET)
            }
            //populating the pooling prep worksheet and uploading the same
            ExcelWorkbook workbook = process.uploadPoolingPrepSheet(pools.values().flatten(), fileNode)
        then:
            noExceptionThrown()
    }

    void "test Single Index Pooling"(){
        def pooling = new LpProcessPoolPrepWorksheet().new LpProcessPoolPrepWorksheet.PoolingSingleIndexes(2,1)
        def members = []
        def indexes = ['A','B','A','C','D','A','E']
        (0..6).each{
            LibraryInformation bean = new LibraryInformation()
            bean.indexName = indexes[it]
            members << bean
        }
        pooling.makePools('SingleIndex',members)
    }

    void "test create Pre-pooling sheet"() {
        setup:
        String fileName = 'PoolingPrepWorksheetTest'
        when:
        ExcelWorkbook workbookToWrite = getWorkbookToWrite(fileName)
        ExcelWorkbook readWorkbook = new ExcelWorkbook(fileName+ '.xls')
        readWorkbook.load()
        logger.info "${readWorkbook.toString()}"
        then:
        (new File(fileName + '.xls')).delete()
        //assert workbookToWrite.sections.size() == workbookRead.sections.size()
    }

    ExcelWorkbook getWorkbookToWrite(String outputFile){
        String template = 'resources/templates/excel/PoolingPrepWorksheet'
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(template,true)
        Section tableSection = getTableSection()
        excelWorkbook.addSection(tableSection)
        excelWorkbook.store(null,outputFile)
        return excelWorkbook
    }

    Section getTableSection(){
        Section tableSection = new TableSection(0, LibraryInformation.class, 'A3', null, true)
        def tableBeansArray = []
        (1..4).each { counter ->
            def rowData = data.get(counter - 1)
            LibraryInformation bean = new LibraryInformation()
            bean.analyteLimsId = rowData[0]
            bean.containerId = rowData[1]
            bean.libraryName = rowData[2]
            bean.queueDate = rowData[3]
            bean.dop = rowData[4]
            bean.indexName = rowData[5]
            bean.platform = rowData[6]
            bean.sequencerModel = rowData[7]
            bean.runMode = rowData[8]
            bean.materialType = rowData[9]
            bean.amplified = rowData[10]
            bean.poolNumber = rowData[11]
            bean.plateMap = rowData[12]
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }
    def data = [
            ['KOL63668A1','alk1210-8','ASPAZ',new Date(),9,'IT100','Platform1','Model1','RunMode1','DNA','Y',1,'PlateMap1'],
            ['KOL63668A2','alk1210-6','ASPBB',new Date(),9,'IT101','Platform1','Model1','RunMode1','DNA','Y',1,'PlateMap1'],
            ['KOL63668A3','alk1210-4','ASPBG',new Date(),9,'IT102','Platform1','Model1','RunMode1','DNA','Y',1,'PlateMap1'],
            ['KOL63668A4','alk1210-2','ASPBN',new Date(),9,'IT103','Platform1','Model1','RunMode1','DNA','Y',1,'PlateMap1']
    ]
}
