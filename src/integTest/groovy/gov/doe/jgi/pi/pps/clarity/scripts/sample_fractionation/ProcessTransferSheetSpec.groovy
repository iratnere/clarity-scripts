package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.ProcessTransferSheet
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.SampleFractionationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ProcessTransferSheetSpec extends Specification {

    static final String action = 'ProcessTransferSheet'
    NodeConfig nodeConfig
    @Autowired
    NodeManager clarityNodeManager
    @Autowired
    Environment environment

    String processId

    def setup() {
        processId = environment.activeProfiles.find{true} == 'claritydev1' ? '24-911723' : '24-911623'
        nodeConfig = clarityNodeManager.nodeConfig
    }

    def cleanup() {
    }

    void "test process ProcessTransferSheet"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        clarityProcess.testMode = false
        ProcessTransferSheet actionHandler = clarityProcess.getActionHandler(action) as ProcessTransferSheet
        when:
        //actionHandler.execute()
        actionHandler.transferBeansDataToOutputs()
        then:
        noExceptionThrown()
    }
}