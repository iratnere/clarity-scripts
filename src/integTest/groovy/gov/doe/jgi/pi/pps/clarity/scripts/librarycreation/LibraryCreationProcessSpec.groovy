package gov.doe.jgi.pi.pps.clarity.scripts.librarycreation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.OutputTube
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryCreationProcessSpec {

    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def libraryStockId = '2-2965394' // output of the 'LC Library Creation' process and input of the 'LQ Library qPCR' process
    ClarityLibraryStock clarityLibraryStock
    LibraryCreationProcess clarityProcess

    def setup() {
        clarityLibraryStock = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(libraryStockId)) as ClarityLibraryStock
        ProcessNode processNode = clarityNodeManager.getProcessNode(clarityLibraryStock.artifactNodeInterface.parentProcessId)
        clarityProcess = new LibraryCreationProcess(processNode)
    }

    def "test process"(String action) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-1049406')
        LibraryCreationProcess clarityProcess = new LibraryCreationProcess(processNode)
        ActionHandler actionHandler = clarityProcess.getActionHandler(action)
        when:
            actionHandler.execute()
        then:
            noExceptionThrown()
        where:
            action << [
                    //+'PreProcessValidation',//:LcPreProcessValidation,
                    //'Automatic Placement',//:LcAutomaticPlacement,
                    'PrepareLibraryCreationSheet',//:LcPrepareLibraryCreationSheet,
                    //'ProcessLibraryCreationSheet',//:LcProcessLibraryCreationSheet,
                    //'Print Labels',//:LcPrintLabels,
                    //'Print Fragment Analyzer Labels',//:LcPrintFragmentAnalyzerLabels,
                    //'Route to Next Workflow'//:LcRouteToWorkflow
            ]
    }

    def "test validateLibraryIndexUdfBatch tubes"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-646262') //tubes
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputTube outputTube = new OutputTube(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = processNode.outputAnalytes.collect{
                AnalyteFactory.analyteInstance(it) as ClarityLibraryStock
            }
            clarityLibraryStocks.each{
                it.udfLibraryQcResult = Analyte.PASS
                it.udfIndexContainerBarcode = 'dummyIndexContainerBarcode'
                it.udfIndexName = ''
            }
        when:
            outputTube.validateLibraryIndexUdfBatch(clarityLibraryStocks)
        then:
            def e = thrown(IllegalArgumentException)
            e.message.contains("Invalid Index Container Barcode 'dummyIndexContainerBarcode'/Index Name ''")
        when:
            clarityLibraryStocks.each{
                it.udfLibraryQcResult = Analyte.FAIL
                it.udfIndexName = ''
            }
            outputTube.validateLibraryIndexUdfBatch(clarityLibraryStocks)
        then:
            noExceptionThrown()
        when:
            clarityLibraryStocks[0].udfLibraryQcResult = Analyte.PASS
            clarityLibraryStocks[0].udfIndexName = 'dummyIndexName'
            clarityLibraryStocks[2].udfLibraryQcResult = Analyte.PASS
            clarityLibraryStocks[2].udfIndexName = 'dummyIndexName'
            outputTube.validateLibraryIndexUdfBatch(clarityLibraryStocks)
        then:
            noExceptionThrown()
    }

    def "test getBatchPmoSampleNodes"() {
        expect:
            clarityLibraryStock
            clarityProcess
        when:
            List<SampleNode> sampleNodes = clarityProcess.batchPmoSampleNodes
        then:
            !sampleNodes
    }

    def "test getBatchPmoSampleArtifactNodes LibraryCreationProcess"() {
        expect:
            clarityLibraryStock
            clarityProcess
        when:
            List<ArtifactNode> artifactNodes = clarityProcess.getBatchPmoSampleArtifactNodes()
        then:
        !artifactNodes
    }

    def "test getBatchPmoSampleContainerNodes LibraryCreationProcess"() {
        expect:
            clarityLibraryStock
            clarityProcess
        when:
            List<ContainerNode> containerNodes = clarityProcess.getBatchPmoSampleContainerNodes()
        then:
            !containerNodes
    }
}