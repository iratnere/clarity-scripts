package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.scripts.services.SequenceService
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SequencingServiceIntegrationSpec extends Specification {

    @Autowired
    SequenceService sequenceService

    def setup() {
    }

    def cleanup() {
    }

    void "test PacBio run number"() {
        expect:"no pacbio run number returned"
        sequenceService.currentPacBioRunNumber()
    }
}
