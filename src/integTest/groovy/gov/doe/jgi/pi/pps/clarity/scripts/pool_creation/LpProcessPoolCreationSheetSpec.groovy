package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LpProcessPoolCreationSheetSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    NodeConfig nodeConfig
    TestUtility testUtil
    ClarityLibraryPool clarityLibraryPool
    LpProcessPoolCreationSheet actionHandler
    @Shared
    def processIds

    def setup() {
        nodeConfig = clarityNodeManager.nodeConfig
        testUtil = new TestUtility(clarityNodeManager)
        processIds = processIds ? processIds : testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value)
        ClarityProcess process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processIds.first()))
        clarityLibraryPool = (ClarityLibraryPool) process.outputAnalytes[0]
        assert clarityLibraryPool instanceof ClarityLibraryPool
        actionHandler = new LpProcessPoolCreationSheet(process: process)
        actionHandler.process.testMode = true
    }

    def cleanup() {
    }

    void "test validatePoolUdfs"() {
        setup:
            def actualTemplateSizeBp = 12.232
            def volumeUl = 13.4323
        RunModeCv novaSeqRunModeCv = findRunModeCv(SequencerModelTypeCv.MISEQ)
            clarityLibraryPool.setUdfRunMode(novaSeqRunModeCv.runMode)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, BigDecimal.ZERO)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, BigDecimal.ZERO)
        expect:
            clarityLibraryPool.udfActualTemplateSizeBp == BigDecimal.ZERO
            clarityLibraryPool.udfVolumeUl == BigDecimal.ZERO
        when:
            actionHandler.validatePoolUdfs([clarityLibraryPool])
        then:
            noExceptionThrown()
        when:
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, volumeUl)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, actualTemplateSizeBp)
            actionHandler.validatePoolUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfActualTemplateSizeBp == actualTemplateSizeBp
            clarityLibraryPool.udfVolumeUl == volumeUl
            noExceptionThrown()
    }

    void "test validatePoolUdfs PacBio"() {
        setup:
            def actualTemplateSizeBp = 32.32
            def volumeUl = 23.23
        RunModeCv pacBioRunModeCv = findRunModeCv(SequencerModelTypeCv.SEQUEL)
            clarityLibraryPool.setUdfRunMode(pacBioRunModeCv.runMode)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, actualTemplateSizeBp)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, volumeUl)
        when:
            actionHandler.validatePoolUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfActualTemplateSizeBp == actualTemplateSizeBp
            clarityLibraryPool.udfVolumeUl == volumeUl
            noExceptionThrown()
        when:
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, BigDecimal.ZERO)
            actionHandler.validatePoolUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfActualTemplateSizeBp == BigDecimal.ZERO
            clarityLibraryPool.udfVolumeUl == volumeUl
            def e = thrown(WebException)
            e.message.contains("The '${ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.value}' udf is not set to the '${PlatformTypeCv.PACBIO.value} ${SequencerModelTypeCv.SEQUEL.value}' analyte")
        when:
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, actualTemplateSizeBp)
            clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, BigDecimal.ZERO)
            actionHandler.validatePoolUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfActualTemplateSizeBp == actualTemplateSizeBp
            clarityLibraryPool.udfVolumeUl == BigDecimal.ZERO
            def e1 = thrown(WebException)
            e1.message.contains("The '${ClarityUdf.ANALYTE_VOLUME_UL.value}' udf is not set to the '${PlatformTypeCv.PACBIO.value} ${SequencerModelTypeCv.SEQUEL.value}' analyte")
    }

    void "test updatePoolUdfs volumeUl"() {
        setup:
        BigDecimal volumeUl = 1234.1234
        def poolSize = 11
        PoolCreationTableBean bean = new PoolCreationTableBean(
                poolConcentrationpM: 1122,
                poolSize: poolSize,
                poolLabProcessResult: new DropDownList(value: Analyte.PASS),
                poolLabProcessFailureMode : new DropDownList(value: '')
        )
        when:
        clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, volumeUl)
        actionHandler.updatePoolUdfs(clarityLibraryPool, [bean])
        then:
        clarityLibraryPool.udfVolumeUl == volumeUl
        noExceptionThrown()
        when:
        clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, BigDecimal.ZERO)
        actionHandler.updatePoolUdfs(clarityLibraryPool, [bean])
        then:
        clarityLibraryPool.udfVolumeUl == poolSize * ClarityLibraryPool.DEFAULT_VOLUME_UL
        noExceptionThrown()
        when:
        clarityLibraryPool.artifactNode.setUdf(ClarityUdf.ANALYTE_VOLUME_UL.udf, null)
        actionHandler.updatePoolUdfs(clarityLibraryPool, [bean])
        then:
        clarityLibraryPool.udfVolumeUl == poolSize * ClarityLibraryPool.DEFAULT_VOLUME_UL
        noExceptionThrown()
    }

    RunModeCv findRunModeCv(SequencerModelTypeCv sequencerModelTypeCv){
        SequencerModelCv sequencerModelCv = SequencerModelCv.findWhere(active: 'Y', sequencerModel: sequencerModelTypeCv.value)
        return RunModeCv.findWhere(active: 'Y', sequencerModel: sequencerModelCv)
    }

    ProcessNode getProcessNode() {
        def processId = processIds.find { nodeId ->
            ProcessNode processNode = clarityNodeManager.getProcessNode(nodeId)
            ArtifactNode output = processNode.outputAnalytes[0]
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(output)
            (!outputAnalyte.isInternalSingleCell && !(outputAnalyte instanceof ClarityPoolLibraryPool) && !outputAnalyte.isPacBio)
        }
        println processId
        return clarityNodeManager.getProcessNode(processId)
    }
}
