package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.HudsonAlphaIlluminaOnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryPoolsAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryStocksAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.Migrator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class OnboardingAdapterIntegrationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Value("\${sampleMaker.url}")
    private String sampleMakerUrl
    OnboardingAdapter adapter
    String libraryOnboardTemplateFile = "resources/templates/excel/LibraryOnboardingTemplate.xls"
    String hudsonAlphaTemplateFile = "resources/templates/excel/HudsonAlphaSequencingFile.xls"

    def setup() {

    }

    def cleanup() {
    }

    def "test Illumina Hudson alpha validateBeansWithUSS"() {
        when:
        adapter = new HudsonAlphaIlluminaOnboardingAdapter(new ExcelWorkbook(hudsonAlphaTemplateFile), 12)
        HudsonAlphaBean bean = new HudsonAlphaBean(indexSequence: "AAGTGCAG-AAGTGCAG")
        adapter.validateBeansWithUSS([bean])
        then:
        noExceptionThrown()
        when:
        bean = new HudsonAlphaBean(indexSequence: "invalid")
        adapter.validateBeansWithUSS([bean])
        then:
        Exception e = thrown()
        e instanceof IllegalArgumentException
    }

    @Ignore
    def "test Illumina Hudson alpha processFlowcell"() {
        setup:
        adapter = new HudsonAlphaIlluminaOnboardingAdapter(new ExcelWorkbook(hudsonAlphaTemplateFile), 12)
        ContainerNode flowcell = clarityNodeManager.getContainerNodesByName("FC200311205426").first()
        HudsonAlphaBean bean = new HudsonAlphaBean(laneNumber: 1, sampleName: "sample252172", collaboratorLibraryName: "")
        when:
        String error = adapter.processFlowcell(bean, flowcell)
        then:
        assert !error
        assert bean.physicalRunUnit
        assert bean.libraryStock
        assert bean.pmoSample
        when:
        bean = new HudsonAlphaBean(laneNumber: 1, sampleName: "sample252172", collaboratorLibraryName: "lib")
        error = adapter.processFlowcell(bean, flowcell)
        then:
        assert !error
        assert bean.physicalRunUnit
        assert !bean.libraryStock
        assert !bean.pmoSample
        when:
        bean = new HudsonAlphaBean(laneNumber: 1, sampleName: "sample25217", collaboratorLibraryName: "")
        error = adapter.processFlowcell(bean, flowcell)
        then:
        assert error
        assert !bean.physicalRunUnit
        assert !bean.libraryStock
        assert !bean.pmoSample
    }

    def "test Library stocks validateBeansWithUSS"() {
        when:
        adapter = new OnboardingLibraryStocksAdapter(new ExcelWorkbook(libraryOnboardTemplateFile), 12)
        OnboardingBean bean = new OnboardingBean(sowRunMode: "Illumina NextSeq-MO 2 X 75", indexName: "N706-N520")
        adapter.validateBeansWithUSS([bean])
        then:
        noExceptionThrown()
        when:
        bean = new OnboardingBean(sowRunMode: "PacBio Sequel 1 X 600", indexName: "invalid")
        adapter.validateBeansWithUSS([bean])
        then:
        Exception e = thrown()
        e instanceof IllegalArgumentException
    }

    def "test Library pools validateBeansWithUSS"() {
        when:
        adapter = new OnboardingLibraryPoolsAdapter(new ExcelWorkbook(libraryOnboardTemplateFile), 12)
        OnboardingBean bean = new OnboardingBean(indexName: "N706-N520")
        adapter.validateBeansWithUSS([bean])
        then:
        noExceptionThrown()
        when:
        bean = new OnboardingBean(indexName: "invalid")
        adapter.validateBeansWithUSS([bean])
        then:
        Exception e = thrown()
        e instanceof IllegalArgumentException
    }

    @Ignore//Rest
    def "create sample for onboarding" () {
        setup:
        TestUtility testUtility = new TestUtility(clarityNodeManager)
        List<String> collabSampleNames = ["FN3215-S_Lak1", "FN3215-S_Lak1", "FN3226-S_Lak1", "FN3226-S_Lak1", "FN3201-S_Lak1", "FN3201-S_Lak1", "FN3229-S_Lak1", "FN3229-S_Lak1", "FN2663-S_Lak1", "FN2663-S_Lak1", "FN2696-S_Lak1", "FN2696-S_Lak1", "FN3266-S_Lak1", "FN3266-S_Lak1", "FN3183-S_Lak1", "FN3183-S_Lak1", "FN3254-S_Lak1", "FN3254-S_Lak1", "FN2711-S_Lak1", "FN2711-S_Lak1", "FN3189-S_Lak1", "FN3189-S_Lak1", "FN2753-S_Lak1", "FN2753-S_Lak1", "FN3253-S_Lak1", "FN3253-S_Lak1", "FN2652-S_Lak1", "FN2652-S_Lak1", "FN2657-S_Lak1", "FN2657-S_Lak1", "FN3223-S_Lak1", "FN3223-S_Lak1", "FN3252-S_Lak1", "FN3252-S_Lak1", "FN3205-S_Lak1", "FN3205-S_Lak1", "FN3236-S_Lak1", "FN3236-S_Lak1", "FN3211-S_Lak1", "FN3211-S_Lak1", "FN3210-S_Lak1", "FN3210-S_Lak1", "FN3216-S_Lak1", "FN3216-S_Lak1", "FN3241-S_Lak1", "FN3241-S_Lak1", "FN3206-S_Lak1", "FN3206-S_Lak1", "NxG_Lak1", "NxG_Lak1", "FN3271-S_Lak1", "FN3271-S_Lak1", "FN3245-S_Lak1", "FN3245-S_Lak1", "FN3272-S_Lak1", "FN3272-S_Lak1", "FN3255-S_Lak1", "FN3255-S_Lak1", "FN1052BCF2-34", "FN1052BCF2-34", "FN3174-S_Lak1", "FN3174-S_Lak1", "FN3142-S_Lak1", "FN3142-S_Lak1", "FN3263-S_Lak1", "FN3263-S_Lak1", "FN3239-S_Lak1", "FN3239-S_Lak1", "FN2648-S_Lak1", "FN2648-S_Lak1", "FN3273-S_Lak1", "FN3273-S_Lak1", "FN2703-S_Lak1", "FN2703-S_Lak1", "UxG_Lak1", "UxG_Lak1", "NxM_Lak1", "NxM_Lak1", "FN3217-S_Lak1", "FN3217-S_Lak1", "FN3222-S_Lak1", "FN3222-S_Lak1", "FN3203-S_Lak1", "FN3203-S_Lak1", "FN3233-S_Lak1", "FN3233-S_Lak1", "FN2692-S_Lak1", "FN2692-S_Lak1", "FN3200-S_Lak1", "FN3200-S_Lak1", "FN3250-S_Lak1", "FN3250-S_Lak1", "FN2669-S_Lak1", "FN2669-S_Lak1", "FN3230-S_Lak1", "FN3230-S_Lak1", "FN3212-S_Lak1", "FN3212-S_Lak1", "FN3227-S_Lak1", "FN3227-S_Lak1", "FN3276-S_Lak1", "FN3276-S_Lak1", "Xa21_Flag_Lak1", "Xa21_Flag_Lak1", "FN3243-S_Lak1", "FN3243-S_Lak1", "FN2709-S_Lak1", "FN2709-S_Lak1", "FN3279-S_Lak1", "FN3279-S_Lak1", "FN3269-S_Lak1", "FN3269-S_Lak1", "FN3187-S_Lak1", "FN3187-S_Lak1", "FN2755-S_Lak1", "FN2755-S_Lak1", "FN2660-S_Lak1", "FN2660-S_Lak1", "FN3261-S_Lak1", "FN3261-S_Lak1", "FN2756-S_Lak1", "FN2756-S_Lak1", "FN3278-S_Lak1", "FN3278-S_Lak1", "FN2724-S_Lak1", "FN2724-S_Lak1", "FN3251-S_Lak1", "FN3251-S_Lak1", "FN3209-S_Lak1", "FN3209-S_Lak1", "FN567-1-3_Lak1", "FN567-1-3_Lak1", "FN3268-S_Lak1", "FN3268-S_Lak1", "FN3235-S_Lak1", "FN3235-S_Lak1", "FN3262-S_Lak1", "FN3262-S_Lak1", "FN3234-S_Lak1", "FN3234-S_Lak1", "FN3256-S_Lak1", "FN3256-S_Lak1", "FN2646-S_Lak1", "FN2646-S_Lak1", "FN3207-S_Lak1", "FN3207-S_Lak1", "FN3151-S_Lak1", "FN3151-S_Lak1", "FN3242-S_Lak1", "FN3242-S_Lak1", "FN3248-S_Lak1", "FN3248-S_Lak1", "FN2708-S_Lak1", "FN2708-S_Lak1", "FN3228-S_Lak1", "FN3228-S_Lak1", "FN3231-S_Lak1", "FN3231-S_Lak1", "FN3177-S_Lak1", "FN3177-S_Lak1", "FN3132-S_Lak1", "FN3132-S_Lak1", "FN2647-S_Lak1", "FN2647-S_Lak1", "FN2707-S_Lak1", "FN2707-S_Lak1", "FN3257-S_Lak1", "FN3257-S_Lak1", "FN3270-S_Lak1", "FN3270-S_Lak1", "FN3202-S_Lak1", "FN3202-S_Lak1", "FN2661-S_Lak1", "FN2661-S_Lak1", "FN3208-S_Lak1", "FN3208-S_Lak1", "FN3204-S_Lak1", "FN3204-S_Lak1", "FN3199-S_Lak1", "FN3199-S_Lak1", "FN2678-S_Lak1", "FN2678-S_Lak1", "FN3219-S_Lak1", "FN3219-S_Lak1", "FN2683-S_Lak1", "FN2683-S_Lak1", "FN3232-S_Lak1", "FN3232-S_Lak1", "FN3218-S_Lak1", "FN3218-S_Lak1", "FN3221-S_Lak1", "FN3221-S_Lak1"]
        List<String> existing = []
        when:
        collabSampleNames.unique().each {String sampleName ->
            try {
                String sampleLimsId = Migrator.lookupPmoSampleByCollaboratorName(nodeManager, sampleName)
                if (sampleLimsId) {
                    existing << sampleName
                }
            }
            catch(Exception e) {
//            do nothing
            }
        }
        collabSampleNames.removeAll(existing)
        List<String> sampleLimsIds  = testUtility.createSamplesInClarity(sampleMakerUrl, collabSampleNames.size(), 1,
                ContainerTypes.TUBE, true, 17828)
        List<SampleNode> sampleNodes = clarityNodeManager.getSampleNodes(sampleLimsIds)
        sampleNodes.eachWithIndex { SampleNode sampleNode, int index ->
            sampleNode.setUdf(ClarityUdf.SAMPLE_COLLABORATOR_SAMPLE_NAME.udf, collabSampleNames[index])
        }
        clarityNodeManager.httpPutNodesBatch(sampleNodes)
        then:
        noExceptionThrown()
    }
}
