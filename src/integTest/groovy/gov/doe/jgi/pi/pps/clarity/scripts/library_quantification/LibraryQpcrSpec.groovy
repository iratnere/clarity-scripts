package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.LibraryQpcrFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryQpcrSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager
    NodeConfig nodeConfig
    @Autowired
    LibraryStockFailureModesService libraryStockFailureModesService

    def setup() {
        nodeConfig = clarityNodeManager.nodeConfig
    }

    def cleanup() {
    }

    void "test validLibraryStockFailureModes"() {
        when:
        def modes = libraryStockFailureModesService.validFailureModes
        then:
        modes.size() == 7
        noExceptionThrown()
    }

    void "test LibraryQpcrFailureModeCv"() {
        when:
        def cvs = LibraryQpcrFailureModeCv.list()
        then:
        cvs.size()
        cvs.each{
            //println it.failureMode
            assert it.failureMode
        }
        [
                'Scheduling Error',
                'DNA contamination',
                'Sample failed qc',
                'Other'
        ].each{ assert it in cvs*.failureMode}
    }

    void "test email notification"(){
        setup:
        TestUtility testUtility = new TestUtility(clarityNodeManager)
        ActionHandler actionHandler = new LqRouteToNextWorkflow()
        List<ContainerNode> containerNodes = testUtility.getContainerInStage(Stage.POOL_CREATION, ContainerTypes.WELL_PLATE_96, 1)
        expect:
        containerNodes
        when:
        actionHandler.sendEmailNotification(containerNodes[0].contentsArtifactNodes.collect{ AnalyteFactory.analyteInstance(it)})
        then:
        noExceptionThrown()
    }
}