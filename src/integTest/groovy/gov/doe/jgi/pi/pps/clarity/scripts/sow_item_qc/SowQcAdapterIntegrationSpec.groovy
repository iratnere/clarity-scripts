package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.GenericSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SIPSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SowQcAdapterIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager
//    String processId
//    ClarityProcess process
//    SampleAnalyte analyte

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
//        processId = testUtil.getProcessIds(ProcessType.SM_SOW_ITEM_QC.value, 1, 'Lakshmi', 'Vishwas').first()
//        process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
//        analyte = testUtil.getAnalytesInStage(Stage.SOW_ITEM_QC,1).first()
    }

    def cleanup() {
    }

    void "test generic build routing requests"(String sowResult, String materialCategory, boolean autoSchedule, Routing.Action action, Object routing) {
        setup:
        Analyte sampleAnalyte = Mock(SampleAnalyte)
        ContainerNode containerNode = Mock(ContainerNode)
        sampleAnalyte.containerNode >> containerNode
        GenericSowQcAdapter ga = new GenericSowQcAdapter([sampleAnalyte])
        sampleAnalyte.activeWorkflows >> [ClarityWorkflow.PLATE_TO_TUBE_TRANSFER]
        when:
        ga.containerAutoScheduleSowItems[containerNode] = autoSchedule
        List<RoutingRequest> rr = ga.buildRoutingRequests(sampleAnalyte, sowResult, materialCategory)
        then:
        assert rr
        assert rr.first().action == action
        assert rr.first().routingUri == routing.uri
        where:
        sowResult           | materialCategory                          | autoSchedule  | action                    | routing
        Analyte.PASS | MaterialCategoryCv.DNA.materialCategory | true  | Routing.Action.assign   | Stage.ALIQUOT_CREATION_DNA
        Analyte.PASS | MaterialCategoryCv.DNA.materialCategory | false | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
        Analyte.PASS | MaterialCategoryCv.RNA.materialCategory | true  | Routing.Action.assign   | Stage.ALIQUOT_CREATION_RNA
        Analyte.PASS | MaterialCategoryCv.RNA.materialCategory | false | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
        Analyte.FAIL | MaterialCategoryCv.DNA.materialCategory | true  | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
        Analyte.FAIL | MaterialCategoryCv.DNA.materialCategory | false | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
        Analyte.FAIL | MaterialCategoryCv.RNA.materialCategory | true  | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
        Analyte.FAIL | MaterialCategoryCv.RNA.materialCategory | false | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER
    }

    void "test sip build routing requests"(String sowResult, Routing.Action action, Object routing, String artifactId) {
        setup:
        Analyte sampleAnalyte = Mock(SampleAnalyte)
        sampleAnalyte.id >> "MyScheduledSample"
        ScheduledSample ss = Mock(ScheduledSample)
        ss.pmoSampleArtifactLimsId >> "MyPmoSample"
        sampleAnalyte.claritySample >> ss
        SIPSowQcAdapter sip = new SIPSowQcAdapter([sampleAnalyte])
        sampleAnalyte.activeWorkflows >> [ClarityWorkflow.PLATE_TO_TUBE_TRANSFER]
        when:
        List<RoutingRequest> rr = sip.buildRoutingRequests(sampleAnalyte, sowResult)
        then:
        assert rr
        assert rr.first().action == action
        assert rr.first().routingUri == routing.uri
        assert rr.artifactId.size() == 1
        assert rr.artifactId.contains(artifactId)
        where:
        sowResult          | action                    | routing                                | artifactId
        Analyte.PASS | Routing.Action.assign   | Stage.SAMPLE_QC_METABOLOMICS           | "MyPmoSample"
        Analyte.FAIL | Routing.Action.unassign | ClarityWorkflow.PLATE_TO_TUBE_TRANSFER | "MyScheduledSample"

    }
}
