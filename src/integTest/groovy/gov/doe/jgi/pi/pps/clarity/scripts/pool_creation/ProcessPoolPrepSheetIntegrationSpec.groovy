package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PrePoolingWorksheet
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ProcessPoolPrepSheetIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager nodeManager

    def setup() {
        testUtil = new TestUtility(nodeManager)
    }

    def cleanup() {
    }

    void "test postErrorMessage Pool is inconsistent"(){
        setup:
        List<List> data = [
                ['CSZAG', 'N701-S505', '2-3317558', '27-370196', 48, 'Illumina', 'MiSeq', 'Illumina NextSeq-HO 2 X 150', LibraryInformation.INTERNAL_SINGLE_CELL],
                ['CSZAH', 'N701-S507', '2-3317560', '27-370196', 48, 'Illumina', 'NovaSeq', 'Illumina NovaSeq 2 X 150', LibraryInformation.FRACTIONAL_LANE],
        ]
            Map<Integer, List<LibraryInformation>> beans = [:].withDefault {[]}
        ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService.class)
            Map<String, Analyte> inputAnalytes = [:]
        when:
            data.each { List fields ->
                ClarityLibraryStock libraryStock = mockAnalyte(ClarityLibraryStock.class, fields) as ClarityLibraryStock
                libraryStock.isInternalSingleCell >> (fields[8] == LibraryInformation.INTERNAL_SINGLE_CELL)
                LibraryInformation bean = new LibraryInformation()
                bean.libraryName = fields[0]
                bean.analyte = libraryStock
                bean.analyte
                inputAnalytes[bean.analyte.id] = bean.analyte
                bean.indexName = fields[1]
                bean.analyteLimsId = fields[2]
                bean.containerId = fields[3]
                bean.dop = fields[4]
                bean.platform = fields[5]
                bean.sequencerModel = fields[6]
                bean.runMode = fields[7]
                bean.poolNumber = 1
                beans[bean.poolNumber as int] << bean
            }
        PoolCreation process = new PoolCreation(nodeManager.getProcessNode(testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value).first())) as PoolCreation
            process.testMode = true
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(inputAnalytes: inputAnalytes)
            def errors = worksheet.validateAllPools(artifactIndexService, beans)
        then:
            assert errors[Pooling.ERROR].find{
                it.contains('Pool #1 is inconsistent. Libraries in this pool [[2-3317558], [2-3317560]]')
                it.contains('cannot be pooled: [InternalSingleCell index length-17:[2-3317558], FractionalLane Illumina NovaSeq Illumina NovaSeq 2 X 150 isDual index length-17 :[2-3317560]]')
            }
        when:
            if(errors[Pooling.ERROR])
                process.postErrorMessage(errors[Pooling.ERROR]?.join("\n"))
        then:
        WebException e = thrown()
        e.message.contains('Pool #1 is inconsistent. Libraries in this pool [[2-3317558], [2-3317560]] cannot be pooled:')
    }

    void "test postErrorMessage no pool number"(){
        setup:
            Map<Integer, List<LibraryInformation>> beans = [:].withDefault {[]}
            ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService.class)
            Map<String, Analyte> inputAnalytes = [:]
        when:
        LibraryInformation bean = new LibraryInformation()
            bean.poolNumber = 0
            beans[bean.poolNumber as int] << bean
        PoolCreation process = new PoolCreation(nodeManager.getProcessNode(testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value).first())) as PoolCreation
            process.testMode = true
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(inputAnalytes: inputAnalytes)
            def errors = worksheet.validateAllPools(artifactIndexService, beans)
        then:
            assert errors[Pooling.ERROR].find{it.contains('You must create at least one pool.')}
        when:
            if(errors[Pooling.ERROR])
                process.postErrorMessage(errors[Pooling.ERROR]?.join("\n"))
        then:
        WebException e = thrown()
            e.message.contains('You must create at least one pool.')
    }

    Map<Integer, List<LibraryInformation>> getPoolsForValidation(){
        Map<Integer, List<LibraryInformation>> testDualIndexData = [:].withDefault {[]}
        List<List<List<String>>> testData = []
        List<List<String>> testData1 = [
                ["ABCD", "GACTTAGA-ACCATGCT", 6,1],
                ["EFGH", "AGACCT", 6,1],
                ["IJKL", "ATTACTTG-GAATTCCT",6, 1],
                ["MNOP", "ATTACTTG-GAATTGCT",6, 1],
                ["QRST", "AGACCTCG-TTGGACTC",6, 1],
                ["UVWX", "GACTTAGA-ACCATCCT",6, 1]
        ]
        testData << testData1
        List<List<String>> testData2 = [
                ["ABCD", "GACTTA-ACCATG", 6,2],
                ["EFGH", "AGTCATCT-GGTGACTC", 6,2],
                ["IJKL", "TCCTAGAG-GCTACTGA", 6,2],
                ["MNOP", "AGTCGCTC-ATCGTACT", 6,2],
                ["QRST", "ACGCATCC-TAGAGAAT", 6,2],
                ["UVWX", "CATAGTAG-AAGATCCT", 6,2]
        ]
        testData << testData2
        testData.each{List<List<String>> data ->
            data.each{List<String> dt ->
                LibraryInformation bean = new LibraryInformation()
                bean.libraryName = dt[0]
                bean.indexName = dt[0]
                bean.indexSequence = dt[1]
                bean.dop = dt[2] as Integer
                bean.poolNumber = dt[3] as Integer
                testDualIndexData[bean.poolNumber] << bean
            }
        }
        testDualIndexData
    }

    @Ignore
    void "test PPS-4112"(){
        setup:
        List<List> data = [
                ['GBUYT','2-3909126','27-496823',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYY','2-3909130','27-496825',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYZ','2-3909131','27-496824',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYX','2-3909129','27-496826',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYU','2-3909127','27-496828',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYW','2-3909128','27-496827',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300']
        ]
        def indexNames = ['IT051','Miseq_St_Rev_37','N711','NG001','Swift722','N704-S505']
        Set<String> poolKeys = [] as Set
        Set<String> validationKeys = [] as Set
        ArtifactIndexService artifactIndexService = (ArtifactIndexService) webTransaction.requireApplicationBean(ArtifactIndexService.class)
        when:
        def indexNamesSequences = artifactIndexService.getSequences(indexNames)
        data.eachWithIndex{List fields, int i ->
            LibraryInformation bean = new LibraryInformation()
            bean.libraryName = fields[0]
            bean.indexName = indexNames[i]
            bean.indexSequence = indexNamesSequences[bean.indexName]
            bean.analyte = mockAnalyte(ClarityLibraryStock.class, fields)
            bean.analyteLimsId = fields[1]
            bean.containerId = fields[2]
            bean.dop = fields[3]
            bean.platform = fields[4]
            bean.sequencerModel = fields[5]
            bean.runMode = fields[6]
            poolKeys << bean.poolingKey
            validationKeys << bean.validationKey
        }
        then:
        assert poolKeys.size() > 1
        assert validationKeys.size() > 1
    }

    Analyte mockAnalyte(Class clazz, fields, boolean isItag = false){
        Analyte analyte = Mock(clazz)
        analyte.id >> fields[2]
        analyte.containerId >> fields[3]
        analyte.isItag >> isItag
        return analyte
    }

    @Ignore
    void "test PPS-4122 Illumina"(){
        setup:
        List<List> data = [
                ['GBUYT','2-3909126','27-496823',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYY','2-3909130','27-496825',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYZ','2-3909131','27-496824',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYX','2-3909129','27-496826',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYU','2-3909127','27-496828',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300'],
                ['GBUYW','2-3909128','27-496827',6,'Illumina','MiSeq','Illumina MiSeq 2 X 300']
        ]
        def indexNames = ['N701-S507','N701-N507','N701-S517','N701-S506','N702-S503','N702-S507']
        Map<Integer, List<LibraryInformation>> beans = [:].withDefault {[]}
        ArtifactIndexService artifactIndexService = (ArtifactIndexService) webTransaction.requireApplicationBean(ArtifactIndexService.class)
        Map<String, Analyte> inputAnalytes = [:]
        when:
        def indexSequences = artifactIndexService.getSequences(indexNames)
        data.eachWithIndex{List fields, int i ->
            LibraryInformation bean = new LibraryInformation()
            bean.libraryName = fields[0]
            bean.indexName = indexNames[i]
            bean.indexSequence = indexSequences[bean.indexName]
            bean.analyte = mockAnalyte(ClarityLibraryStock.class, fields)
            inputAnalytes[bean.analyte.id] = bean.analyte
            bean.analyteLimsId = fields[1]
            bean.containerId = fields[2]
            bean.dop = fields[3]
            bean.platform = fields[4]
            bean.sequencerModel = fields[5]
            bean.runMode = fields[6]
            bean.poolNumber = 1
            beans[bean.poolNumber] << bean
        }
        PoolCreation process = ProcessFactory.processInstance(nodeManager.getProcessNode(testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value).first())) as PoolCreation
        process.testMode = true
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(inputAnalytes: inputAnalytes)
        def	errors = worksheet.validateAllPools(artifactIndexService, beans)
        then:
        assert errors["Error"].find{it.contains("has duplicate indexes")}
    }

    @Ignore
    void "test PPS-4122 Exome capture"(){
        //GCOSA, GCOSB
        setup:
        List<String> indexes = ['NG011','NG014','NG016','NG019','NG022','NG024']
        List<List> data = [
                ['GCOSA',indexes[0..2],'2-3953763','27-499819',2,'Illumina','NovaSeq','Illumina NovaSeq 2 X 150'],
                ['GCOSB',indexes[3..5],'2-3909130','27-499818',2,'Illumina','NovaSeq','Illumina NovaSeq 2 X 150'],
        ]
        Map<Integer, List<LibraryInformation>> beans = [:].withDefault {[]}
        ArtifactIndexService artifactIndexService = webTransaction.requireApplicationBean(ArtifactIndexService.class) as ArtifactIndexService
        Map indexNameSeqs = artifactIndexService.getSequences(indexes)
        Map<String, Analyte> inputAnalytes = [:]
        when:
        data.each{List fields ->
            LibraryInformation bean = new LibraryInformation()
            bean.libraryName = fields[0]
            bean.analyte = mockAnalyte(ClarityLibraryPool.class, fields)
            inputAnalytes[bean.analyte.id] = bean.analyte
            bean.allIndexes = fields[1]
            bean.setUpIndexes(indexNameSeqs)
            bean.analyteLimsId = fields[2]
            bean.containerId = fields[3]
            bean.dop = fields[4]
            bean.platform = fields[5]
            bean.sequencerModel = fields[6]
            bean.runMode = fields[7]
            bean.poolNumber = 1
            beans[bean.poolNumber] << bean
        }
        PoolCreation process = ProcessFactory.processInstance(nodeManager.getProcessNode(testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value).first())) as PoolCreation
        process.testMode = true
        PrePoolingWorksheet worksheet = new PrePoolingWorksheet(inputAnalytes: inputAnalytes)
        def	errors = worksheet.validateAllPools(artifactIndexService, beans)
        then:
        assert errors["Error"].find{it.contains("has duplicate indexes")}
    }

    @Ignore
    void "test process Pre-pooling sheet Single Cell Internal and regular pools"() {
        setup:
        String testProcessPrePooling
        ExcelWorkbook excelWorkbook
        String action = 'ProcessPoolingPrepSheet'
        def processid = '122-1136902' //tempest
        ProcessNode processNode = nodeManager.getProcessNode(processid)

        LpProcessPoolPrepWorksheet actionHandler = (LpProcessPoolPrepWorksheet) ProcessType.actionHandler(processNode, action)
        actionHandler.process.testMode = true
        when:
        boolean hasInternalSingleCells = actionHandler.process.inputAnalytes.any{ it.isInternalSingleCell }
        then:
        hasInternalSingleCells in [true]
        when:
        testProcessPrePooling = 'resources/templates/excel/test_excel/92-prepooling1'
        excelWorkbook = new ExcelWorkbook(testProcessPrePooling, true)
        excelWorkbook.load()
        then:
        noExceptionThrown()
        when:
        actionHandler.process.getPoolPrepMemberBeans(true, excelWorkbook)
        then:
        noExceptionThrown()
        when:
        testProcessPrePooling = 'resources/templates/excel/test_excel/92-prepooling2'
        excelWorkbook = new ExcelWorkbook(testProcessPrePooling, true)
        excelWorkbook.load()
        then:
        noExceptionThrown()
        when:
        actionHandler.process.getPoolPrepMemberBeans(true, excelWorkbook)
        then:
        def e2 = thrown(WebException)
        e2.message.contains("Pool #3 is not valid.")
        e2.message.contains("BHWNS library is not ${SowItemTypeCv.SINGLE_CELL_INTERNAL}.")
        e2.message.contains("Pool members should have the same ${ClarityUdf.SAMPLE_BATCH_ID}.")
        e2.message.contains("Pool members should have the same ${ClarityUdf.SAMPLE_POOL_NUMBER}.")
        when:
        testProcessPrePooling = 'resources/templates/excel/test_excel/92-prepooling3'
        excelWorkbook = new ExcelWorkbook(testProcessPrePooling, true)
        excelWorkbook.load()
        then:
        noExceptionThrown()
        when:
        actionHandler.process.getPoolPrepMemberBeans(true, excelWorkbook)
        then:
        def e3 = thrown(WebException)
        e3.message.contains("Pool #4 is not valid.")
        e3.message.contains("Pool members should have the same ${ClarityUdf.SAMPLE_BATCH_ID}.")
        e3.message.contains("Pool members should have the same ${ClarityUdf.SAMPLE_POOL_NUMBER}.")
    }

    @Ignore//Rest
    void "test process Pre-pooling sheet"() {
        setup:
            String action = 'ProcessPoolingPrepSheet'
            def processId = '122-813793'//'122-692675'//'122-681192' //tempest //122-656259 big
        ProcessNode processNode = nodeManager.getProcessNode(processId)
        PoolCreation process = new PoolCreation(processNode)
        LpProcessPoolPrepWorksheet actionHandler = (LpProcessPoolPrepWorksheet) process.getActionHandler(action)
        when:
            actionHandler.execute()
        then:
            noExceptionThrown()
    }

}
