package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.sample_receipt.SampleTrackingTableBean
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.researcher.ResearcherFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

import java.text.DateFormat
import java.text.SimpleDateFormat

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleTrackingServiceSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager
    @Autowired
    SampleTrackingService sampleTrackingService
    String fileName = 'TrashAndShipTemplate'
    String samFolder = '/mnt/smb1/SAMfiles/clarity/dev1/'
    List<ArtifactNode> sampleArtifacts = []
    List<SampleTrackingTableBean> beans = []

    def setup() {
    }

    def cleanup() {
    }

    void "test validate"() {
        setup:
            def limsid = 'BAR19856A1PA1'
        Analyte artifactNode = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(limsid))
            Long pmoid = artifactNode.name as Long
            Map statusMap = [:]
            statusMap[pmoid] = 'Awaiting Sample QC'
        when:
            sampleTrackingService.validate([artifactNode], statusMap)
        then:
            noExceptionThrown()
    }

    void "test validate invalid statuses"(String invalidStatus) {
        setup:
            def limsid ='BAR19856A1PA1'
        Analyte artifactNode = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(limsid))
            Long pmoid = artifactNode.name as Long
            Map statusMap = [:]
            statusMap[pmoid] = invalidStatus
        when:
            sampleTrackingService.validate([artifactNode], statusMap)
        then:
            RuntimeException e = thrown()
            e.message.contains("Invalid status [${invalidStatus}]. Samples must have a status >= [Sample Received].")
        where:
            invalidStatus << SampleTrackingService.INVALID_STATUSES
    }

    @Ignore
    void "test posting processes"(){
        List<String> containerIds = ["27-464555", "27-464451", "27-464359", "27-464166", "27-462253"]
        Map<ContainerNode, Set<Analyte>> containerInputMap = [:]
        expect:
        sampleTrackingService
        when:
        clarityNodeManager.getContainerNodes(containerIds).each{ ContainerNode containerNode ->
            containerInputMap[containerNode] = containerNode.contentsArtifactNodes.collect{ AnalyteFactory.analyteInstance(it)} as Set
        }
        Tuple tuple = new Tuple([Stage.ON_SITE, ResearcherFactory.researcherForContactId(clarityNodeManager, 17828L)] as Object[])
        Map map = [:]
        map[tuple] = containerInputMap
        sampleTrackingService.postProcesses(map, null, clarityNodeManager)
        then:
        noExceptionThrown()
    }

    @Ignore
    void "test process Files"(){
        setup:
        createInputSamples()
        List<ContainerNode> containers = sampleArtifacts.collect{it.containerNode}.unique()
        //sampleTrackingService = webTransaction.requireApplicationBean(SampleTrackingService.class)
        DateFormat format = new SimpleDateFormat('yyyy-MM-dd')
        containers.each{ ContainerNode containerNode->
            SampleTrackingTableBean bean = new SampleTrackingTableBean(
                    containerBarcode: containerNode.name,
                    status: SampleTrackingService.STATUS_SHIPPED,
                    operatorId: 2633, date: format.format(new Date())
            )
            beans << bean
        }
        File file =  new File(samFolder)
        if(!file.exists())
            file.mkdirs()
        when:
        writeContainerBarcodeFile()
        sampleTrackingService.processFiles()
        then:
        noExceptionThrown()
    }

    void writeContainerBarcodeFile(){
        String filePath = "resources/templates/excel/test_excel/Book1"
        ExcelWorkbook workbook = new ExcelWorkbook(filePath, true)
        Section section = new TableSection(0, SampleTrackingTableBean.class,'A1')
        workbook.addSection(section)
        section.setData(beans)
        workbook.store(null, "${samFolder}${fileName}")
    }

    void createInputSamples(){
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        //sampleArtifacts = testUtil.createAnalytesInClarity(TestUtil.JSON_TEMPLATE,EndToEndCommand.AnalyteType.ROOT_SAMPLE, EndToEndCommand.ContainerType.PLATE, 5)
        ["TES72820A1",
         "TES72820A2",
         "TES72820A3",
         "TES72820A4",
         "TES72820A5"].each{ sampleArtifacts << clarityNodeManager.getArtifactNode(it) }
        Map<Long, SampleStatusCv> sampleStatusMap = [:]
        sampleArtifacts.each{ ArtifactNode sampleArtifact ->
            sampleStatusMap[sampleArtifact.name as Long] = SampleStatusCv.SAMPLE_RECEIVED
        }
        statusService.submitSampleStatus(sampleStatusMap, 2633)
    }

}
