package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioSequencingTemplate
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class PbPrepareRunDesignSpec extends Specification {

    String action = 'PrepareRunDesign'
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId
    PacBioSequencingPlateCreation clarityProcess
    PbPrepareRunDesign actionHandler

    def setup() {
        processId = '24-898017'

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new PacBioSequencingPlateCreation(processNode)
        actionHandler = clarityProcess.getActionHandler(action) as PbPrepareRunDesign
    }

    def cleanup() {
    }

    void "test generatePacBioRunNumber"() {
        when:
            def runId = PbPrepareRunDesign.generatePacBioRunNumber()
        then:
            runId
    }

    void "test updateOutputContainerName"() {
        setup:
            String pacBioRunNumber = '4789'
        ContainerNode containerNode = clarityProcess.outputContainerNode
        when:
            actionHandler.updateOutputContainerName(pacBioRunNumber)
        then:
            containerNode.name == PbPrepareRunDesign.runNumberToContainerName(pacBioRunNumber)
    }

    void "test getIndexNameToLibraryMap"(){
        setup:
        PacBioSequencingTemplate pacBioPool = clarityProcess.outputAnalytes.find{
                PacBioSequencingTemplate sequencingTemplate = it as PacBioSequencingTemplate
            Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
                library instanceof ClarityLibraryPool
            } as PacBioSequencingTemplate
        ClarityLibraryPool clarityLibraryPool = pacBioPool.parentLibrary as ClarityLibraryPool
            def expectedIndexNameToLs = [:]
            clarityLibraryPool.poolMembers.each{
                expectedIndexNameToLs[(it as ClarityLibraryStock).udfIndexName] = it
            }
        when:
            def indexToParentLibrary = PbPrepareRunDesign.getIndexNameToLibraryMap(clarityLibraryPool)
        then:
            indexToParentLibrary == expectedIndexNameToLs
    }

    void "test runName"() { //runName = 'SQII02_Run5309'
        setup:
            String sequencerName = 'SQ01'
            String pacBioRunNumber = '4789'
        when:
            def runName = PbPrepareRunDesign.runName(sequencerName, pacBioRunNumber)
        then:
            runName == (sequencerName + '_Run' + pacBioRunNumber)
    }
}
