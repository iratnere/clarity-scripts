package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity_node_manager.node.FileNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

import java.text.SimpleDateFormat

@SpringBootTest(classes = [Application.class])
@Transactional
class PbProcessRunDesignSpec extends Specification {

    final static String DOWNLOAD_RUN_DESIGN = 'Download RunDesign.csv'
    String action = 'ProcessRunDesign'
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId
    PacBioSequencingPlateCreation clarityProcess
    PbProcessRunDesign actionHandler

    def setup() {
        processId = '24-898017' //PacBio Sequel II production

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new PacBioSequencingPlateCreation(processNode)
        actionHandler = clarityProcess.getActionHandler(action) as PbProcessRunDesign
    }

    def cleanup() {
    }

    void "test getFileNode"() {
        when:
        def placeHolder = DOWNLOAD_RUN_DESIGN
        FileNode fileNode = actionHandler.getFileNode(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7)
        then:
        fileNode.id
        when:
        actionHandler.getFileNode(placeHolder)
        then:
        WebException e = thrown()
        e.message//.contains("$placeHolder was not attached")
    }

    void "test validateSequelRunSheet"() {
        setup:
        String sequencerName = clarityProcess.processNode.getUdfAsString(ClarityUdf.PROCESS_SEQUEL_SEQUENCER.udf, 'unknown')
        String runNumber = actionHandler.extractRunNumberFromContainerName()
        String runName = PbPrepareRunDesign.runName(sequencerName, runNumber)
        def prefix = runName + '_' + new SimpleDateFormat('MMddyyyy').format(new Date()) + '_'
        PbPrepareRunDesign actionHandlerPrepare = clarityProcess.getActionHandler('PrepareRunDesign') as PbPrepareRunDesign
        when:
        String runDesign = actionHandlerPrepare.createLibraryContents(clarityProcess.getPbSequelLibraryBeans(runName))
        actionHandlerPrepare.uploadSequelLibraryFile(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7, runDesign, prefix)
        then:
        noExceptionThrown()
        when:
        clarityProcess.getFileNode(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7).httpRefresh()
        actionHandler.validateSequelRunSheet(runName)
        then:
        noExceptionThrown()
        when:
        clarityProcess.processNode.setUdf(ClarityUdf.PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT.udf, '11222333444')
        actionHandler.validateSequelRunSheet(runName)
        then:
        WebException e = thrown()
        e.message.contains('Attached combined run design file is not consistent with latest UDF values: please generate the file again.')
    }
}
