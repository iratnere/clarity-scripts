package gov.doe.jgi.pi.pps.clarity.scripts.librarycreation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationDapSeq
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

//import org.junit.Before
//import org.junit.jupiter.api.Test

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryCreationDapSeqSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager

    @Shared
    def processId
    LibraryCreationProcess clarityProcess

    def setup() {
        //clarityNodeManager = BeanUtil.nodeManager
        processId = '24-916905'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new LibraryCreationProcess(processNode)
    }


    def "test valid dateRunModes"() {
        setup:
            def limsids = ['2-3785386', '2-3885125']
            //def limsid = '2-3785386' // "Run Mode">Illumina NovaSeq S4 2 X 150
            //def limsid = '2-3885125' // "Run Mode">Illumina NextSeq-HO 1 X 75
            List<Analyte> sampleAliquots = limsids.collect{ getAnalyte(it) }
            def runModes = sampleAliquots.collect{ (it.claritySample as ScheduledSample).udfRunMode }?.unique()
        when:
        LibraryCreationDapSeq.validateRunModes(sampleAliquots)
        then:
            runModes.find()
            def e = thrown(WebException)
            e.message.contains("Cannot create a DAP-Seq Pool")
            e.message.contains("Libraries in this pool cannot be pooled")
            e.message.contains("Invalid parent samples run modes:")
            e.message.contains(runModes as String)
        when:
        LibraryCreationDapSeq.validateRunModes([sampleAliquots[0]])
        then:
            noExceptionThrown()
        when:
            sampleAliquots.each{ (it.claritySample as ScheduledSample).udfRunMode = null }
        LibraryCreationDapSeq.validateRunModes(sampleAliquots)
        then:
            def e1 = thrown(WebException)
            e1.message.contains("Invalid parent samples run modes: []")
    }

    def "test validateParentProcesses"() {
        setup:
            def limsid = '2-3735811' // two SAC processes were run
        Analyte analyte = getAnalyte(limsid)
        when:
        LibraryCreationDapSeq.validateParentProcesses([analyte])
        then:
            def e = thrown(WebException)
            e.message.contains("Input analyte: ClaritySampleAliquot(2-3735811)")
            e.message.contains("Invalid parent 'AC Sample Aliquot Creation' processes")
            e.message.contains("Expected only one parent 'AC Sample Aliquot Creation' process: 24-844158")
        when:
            limsid = '2-3785386' // one SAC process was run
            analyte = getAnalyte(limsid)
        LibraryCreationDapSeq.validateParentProcesses([analyte])
        then:
            noExceptionThrown()
    }

    Analyte getAnalyte(String limsid) {
        ArtifactNode artifactNode = clarityNodeManager.getArtifactNode(limsid)
        return AnalyteFactory.analyteInstance(artifactNode)
    }

    @Test
    void "test findCreatedPool"() {
        setup:
            def limsid = "2-3975639"//"2-3975639" pool //"2-3975702" library stock //"2-3950575 sample aliquot"
        Analyte analyte = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(limsid))
        when:
        LibraryCreationDapSeq.findCreatedPool(analyte)
        then:
            def e = thrown(WebException)
            e.message.contains('Cannot find valid DAP-Seq pool')
        when:
            analyte.artifactNode.setSystemQcFlag(null)
            analyte.artifactNode.httpPut()
            limsid = "2-3975702"
        Analyte libraryStock = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(limsid))
            List<ClarityLibraryPool> result = LibraryCreationDapSeq.findCreatedPool(libraryStock as ClarityLibraryStock)
        then:
            noExceptionThrown()
            result[0]
        when:
            analyte.artifactNode.setSystemQcFlag(Boolean.TRUE)
        LibraryCreationDapSeq.findCreatedPool(libraryStock as ClarityLibraryStock)
        then:
            def e1 = thrown(WebException)
            e1.message.contains('Found multiple valid DAP-Seq submissions.')
            e1.message.contains('Please abort the process.')
    }
}