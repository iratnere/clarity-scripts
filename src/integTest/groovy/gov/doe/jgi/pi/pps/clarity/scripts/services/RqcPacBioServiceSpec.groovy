package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class RqcPacBioServiceSpec extends Specification {

    @Autowired
    RqcPacBioService rqcPacBioService
    @Autowired
    NodeManager clarityNodeManager
    def setup() {
    }
    def cleanup() {
    }

    void "test callPacBioSequencingCompleteService"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-688244') //24-733803 two lb pools as input
        ClarityProcess clarityProcess = new ClarityProcess(processNode)
        expect:
            rqcPacBioService
        when:
            rqcPacBioService.callPacBioSequencingCompleteService(clarityProcess)
        then:
            noExceptionThrown()
    }
}
