package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryPoolFailureModesServiceSpec extends Specification {

    @Autowired
    LibraryPoolFailureModesService libraryPoolFailureModesService
    def insrumentError = 'Instrument Error'
    def operatorError = 'Operator Error'
    def abandonWorkError = 'Abandoned Work'
    def size = 7

    def setup() {
    }

    def cleanup() {
    }

    void "test getActiveFailureModes"() {
        when:
            def modes = libraryPoolFailureModesService.getActiveFailureModes()
        then:
            modes.size() == size + 1
            insrumentError in modes
            operatorError in modes
            abandonWorkError in modes
    }

    void "test getValidFailureModes"() {
        when:
            def modes = libraryPoolFailureModesService.getValidFailureModes()
        then:
            modes.size() == size
            insrumentError in modes
            operatorError in modes
            !(abandonWorkError in modes)
    }

    void "test getDropDownFailureModes"() {
        when:
        DropDownList dropDownList = libraryPoolFailureModesService.dropDownFailureModes
        then:
            dropDownList.controlledVocabulary.size() == size
            insrumentError in dropDownList.controlledVocabulary
            operatorError in dropDownList.controlledVocabulary
    }

    void "test getDropDownPassFail"() {
        when:
        DropDownList dropDownList = libraryPoolFailureModesService.getDropDownPassFail()
        then:
            dropDownList.controlledVocabulary.size() == 2
        Analyte.PASS in dropDownList.controlledVocabulary
        Analyte.FAIL in dropDownList.controlledVocabulary
    }
}
