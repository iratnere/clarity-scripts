package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolCreationTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PrePoolingWorksheet
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class PreparePoolCreationSheetIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processIds
    @Shared SequencerModelCv sequencerModelCv
    @Shared RunModeCv novaSeqRunModeCv
    LpPreparePoolCreationSheet actionHandler

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        processIds = processIds ? processIds : testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value, 2)
        sequencerModelCv = SequencerModelCv.findWhere(active: 'Y', sequencerModel: SequencerModelTypeCv.NOVASEQ_S4.value)
        novaSeqRunModeCv = RunModeCv.findWhere(active: 'Y', sequencerModel: sequencerModelCv)
        ProcessNode processNode = clarityNodeManager.getProcessNode(processIds.first())
        actionHandler = new LpPreparePoolCreationSheet(process: ProcessFactory.processInstance(processNode))
    }

    def cleanup() {
    }

    void "test getPoolCreationBeansForPools run mode"() {
        expect:
            actionHandler
            sequencerModelCv
            novaSeqRunModeCv
        when:
            List<PoolCreationTableBean> poolCreationTableBeans =  actionHandler.getPoolCreationBeansForPools()
        then:
            poolCreationTableBeans.each{ PoolCreationTableBean bean ->
                Analyte input = actionHandler.process.inputAnalytes.find{ it.id == bean.sampleLimsId }
                assert input
                assert bean.runMode == input.udfRunMode
            }
    }

    void "test updateOutputsUdfs"() {
        expect:
            actionHandler
            sequencerModelCv
            novaSeqRunModeCv
        when:
        ClarityLibraryPool clarityLibraryPool = (ClarityLibraryPool) actionHandler.process.outputAnalytes[0]
            List poolMembers = clarityLibraryPool.poolMembers
            String testProcessPrePooling = './src/integTest/resources/92-prepooling4.xls'
        PrePoolingWorksheet excelWorkbook = new PrePoolingWorksheet(testProcessPrePooling, poolMembers)
            actionHandler.process.workbookCached = excelWorkbook
            String currentRunMode = poolMembers[0].udfRunMode
            poolMembers.each{
                it.udfRunMode = novaSeqRunModeCv.runMode
            }
        then:
            poolMembers.each{
                assert it.udfRunMode == novaSeqRunModeCv.runMode
            }
        when:
            actionHandler.updateOutputsUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfRunMode == novaSeqRunModeCv.runMode
        when:
            poolMembers.each{
                it.udfRunMode = currentRunMode
            }
        then:
            poolMembers.each{
                assert it.udfRunMode == currentRunMode
            }
        when:
            actionHandler.updateOutputsUdfs([clarityLibraryPool])
        then:
            clarityLibraryPool.udfRunMode == currentRunMode
            noExceptionThrown()
    }
}
