package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.DensityTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.ProcessDensityFiles
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.SampleFractionationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ProcessDensityFilesSpec extends Specification {

    static final String action = 'ProcessDensityFiles'
    @Autowired
    NodeManager clarityNodeManager

    @Autowired
    Environment environment

    String processId

    def setup() {
        String activeProfile = environment.activeProfiles.find{true}
        processId = activeProfile == 'claritydev1' ? '24-911723' : '24-911623'
    }

    def cleanup() {
    }

    void "test process processConcentrationFiles"() {
        //"Well ID\tName\tWell\tConc/Dil\t485,528\t[Concentration]\tCount\tMean\tStd Dev\tCV (%)"
        //"BLK\t\tA6\t\t8\t>10.475\t0\t?????\t?????\t?????"
        //"\t\tA12\t\t13\t<0.025\t\t\t\t"
        //"SPL1\t\tA1\t\t14\t<0.025\t0\t?????\t?????\t?????"
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        ProcessDensityFiles actionHandler = clarityProcess.getActionHandler(action) as ProcessDensityFiles
        when:
        actionHandler.processConcentrationFiles()
        then:
        noExceptionThrown()
        clarityProcess.outputAnalytes.each{
            assert it.udfConcentrationNgUl >= BigDecimal.ZERO
        }
    }

    void "test process validateFiles"(String placeholder) {
        setup:
        //processId = '24-948123'// file ext error clarityint1 24-948122 24-948123
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        ProcessDensityFiles actionHandler = clarityProcess.getActionHandler(action) as ProcessDensityFiles
        when:
        List<ArtifactNode> files = actionHandler.validateFiles(placeholder)
        then:
        noExceptionThrown()
        files.size()
        where:
        placeholder << [ProcessDensityFiles.DENSITY, ProcessDensityFiles.DNA_CONCENTRATION]
    }

    void "test process ProcessDensityFiles"() {
        setup:
        //processId = '24-949412'//'24-948118'//'24-948121' //24-948118 //24-948123
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        ProcessDensityFiles actionHandler = clarityProcess.getActionHandler(action) as ProcessDensityFiles
        when:
        //actionHandler.execute()
        Map<String, List<DensityTableBean>> plateBarcodeToBeans = actionHandler.processDensityFiles()
        then:
        noExceptionThrown()
        plateBarcodeToBeans.size()
    }
@Ignore
    void "test process ProcessDensityFiles duplicate sample barcode error"() {
        setup:
        processId = System.getProperty('grails.env') == 'claritydev1' ? '24-1218485' : '24-948118'//'24-948121'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleFractionationProcess clarityProcess = new SampleFractionationProcess(processNode)
        ProcessDensityFiles actionHandler = clarityProcess.getActionHandler(action) as ProcessDensityFiles
        when:
        actionHandler.processDensityFiles()
        then:
        def e = thrown(WebException)
        e.message.contains('Sample Barcode name is already used by an existing density file')
    }
}