package gov.doe.jgi.pi.pps.clarity.scripts.services.util

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import grails.gorm.transactions.Transactional
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleAnalyteTableBeanIntegrationSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test validate"(aliquotStatus, failureMode, destinationBarcode,
                         finalAliquotMassNg,adjustedAliquotVolumeUl,finalAliquotVolumeUl
    ) {
        setup:
            def errorMsg
        SampleAnalyteTableBean bean = new SampleAnalyteTableBean(aliquotLimsId: 'testLimsId')
            bean.aliquotStatus = new DropDownList(value:aliquotStatus)
            bean.failureMode = new DropDownList(value:failureMode)
            bean.destinationBarcode = destinationBarcode
            bean.finalAliquotMassNg = finalAliquotMassNg
            bean.adjustedAliquotVolumeUl = adjustedAliquotVolumeUl
            bean.finalAliquotVolumeUl = finalAliquotVolumeUl
        when:
            errorMsg = bean.validate()
        then:
            errorMsg
        where:
     //GOOD:Analyte.PASS  | null              | '27-27272'          |123.123            |123.123                    |123.123
     //GOOD:Analyte.PASS  | null              | '27-27272'          |123.123            |0.0                        |123.123
            aliquotStatus | failureMode       | destinationBarcode |finalAliquotMassNg |adjustedAliquotVolumeUl |finalAliquotVolumeUl
        Analyte.PASS | null              | '27-27272'  |123.123 |null    |123.123
        Analyte.PASS | null              | '27-27272'  |null    |123.123 |123.123
        Analyte.PASS | null              | '27-27272'  |123.123 |123.123 |null
        Analyte.PASS | null              | '27-27272'  |123.123 |123.123 |0.0
            null          | null              | '27-27272'         |123.123            |123.123                 |123.123
            'dummy'       | null              | '27-27272'         |123.123            |123.123                 |123.123
        Analyte.FAIL | null              | '27-27272'  |123.123 |123.123 |123.123
        Analyte.PASS | 'testFailureMode' |  '27-27272' |123.123 |123.123 |123.123
    }

    void "test validate no exception thrown"(aliquotStatus, failureMode, destinationBarcode,
                                             finalAliquotMassNg,adjustedAliquotVolumeUl,finalAliquotVolumeUl
    ) {
        setup:
        SampleAnalyteTableBean bean = new SampleAnalyteTableBean()
            bean.aliquotStatus = new DropDownList(value:aliquotStatus)
            bean.failureMode = new DropDownList(value:failureMode)
            bean.destinationBarcode = destinationBarcode
            bean.finalAliquotMassNg = finalAliquotMassNg
            bean.adjustedAliquotVolumeUl = adjustedAliquotVolumeUl
            bean.finalAliquotVolumeUl = finalAliquotVolumeUl
        when:
            def errorMsg = bean.validate()
        then:
            assert errorMsg == null
        where:
            aliquotStatus | failureMode | destinationBarcode |finalAliquotMassNg |adjustedAliquotVolumeUl |finalAliquotVolumeUl
        Analyte.PASS | null | '27-27272' |123.123 |123.123 |123.123
        Analyte.PASS | null | '27-27272' |123.123 |0.0     |123.123
    }
}
