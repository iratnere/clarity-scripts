package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibraryannealing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class RequeueLibraryAnnealingSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def action = 'Route to Next Workflow'

    def setup() {
    }

    def cleanup() {
    }

    //LS requeue process 24-894627
    //LP requeue process 24-904536
    void "test assignToNextWorkflow"(def processId) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        ClarityProcess clarityProcess = ProcessFactory.processInstance(processNode)

            RequeueLibraryAnnealingRouteToWorkflow actionHandler = clarityProcess.getActionHandler(action) as RequeueLibraryAnnealingRouteToWorkflow
        when:
            actionHandler.assignToNextWorkflow()
        then:
            noExceptionThrown()
        where:
        processId << ['24-894627', '24-904536']
    }
}
