package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.scripts.services.ProcessRegistrationService
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import java.lang.reflect.Constructor

@SpringBootTest(classes = [Application.class])
@Transactional
class AbandonRegistrationIntegrationSpec extends Specification {

    @Autowired
    ProcessRegistrationService processRegistrationService

    def setup() {
    }

    def cleanup() {
    }

    def "test ProcessType.ABANDON_WORK registration"() {
        setup:
        ProcessType processType = ProcessType.ABANDON_WORK

        expect:"fix me"
        ProcessType.toEnum(processType.value) == processType

        when:
        Constructor constructor = processRegistrationService.processConstructor(processType)

        then:
        constructor
        constructor.declaringClass == AbandonWorkProcess
    }
}
