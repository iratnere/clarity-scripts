package gov.doe.jgi.pi.pps.clarity.scripts.services.util

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ClaritySampleAnalyteComparator
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ClaritySampleAnalyteComparatorSpec extends Specification {

    static final String microbial = ClaritySampleAnalyteComparator.MICROBIAL_SCIENTIFIC_PROGRAM
    static final String plate = ContainerTypes.WELL_PLATE_96.value
    static final String tube = ContainerTypes.TUBE.value
    static final String TYPE_FRAGMENT = 'Fragment'
    static final String TYPE_LMP = 'LMP'
    static final String TYPE_RNA = 'RNA'
    static final String TYPE_RND = 'RnD'

    static final String PLATFORM_ILLUMINA = PlatformTypeCv.ILLUMINA
    static final String PLATFORM_PACBIO = PlatformTypeCv.PACBIO

    def setup() {
    }

    def cleanup() {
    }

    def "test customSorterByTargetInsertSize"(aTargetInsertSizeKb, bTargetInsertSizeKb, expectedResult) {
        setup:
        def result
        when:
        result = ClaritySampleAnalyteComparator.customSorterByTargetInsertSize(aTargetInsertSizeKb, bTargetInsertSizeKb)
        then:
        assert result == expectedResult : "aTargetInsertSizeKb = $aTargetInsertSizeKb, bTargetInsertSizeKb = $bTargetInsertSizeKb, result = $result"
        where:
        aTargetInsertSizeKb |bTargetInsertSizeKb    |expectedResult
        23                  |46.34                  |1
        45.63               |13.67                  |-1
        null                |null                   |0
        45.3421             |45.3421                |0
        null                |123                    |1
        123                 |null                   |-1
    }

    def "test sortbySowItemType by scientific program"(aScientificProgram, bScientificProgram, expectedResult) {
        setup:
        def result
        when:
        result = ClaritySampleAnalyteComparator.customSorter(aScientificProgram, bScientificProgram, ClaritySampleAnalyteComparator.predefinedOrderByProgram)
        then:
        assert result == expectedResult : "aScientificProgram = $aScientificProgram, bScientificProgram = $bScientificProgram, result = $result"
        where:
        aScientificProgram   |bScientificProgram  |expectedResult
        microbial            |'non microbial'     |-1
        microbial            |''                  |-1
        microbial            |null                |-1
        'non microbial'      |''                  |0
        ''                   |null                |0
        null                 |null                |0
    }

    def "test sortbySowItemType by sow item type"(aSowItemType, bSowItemType, expectedResult) {
        setup:
        def result
        when:
        result = (SowItemTypeCv.toSortIndex(aSowItemType) <=> SowItemTypeCv.toSortIndex(bSowItemType))
        then:
        assert result == expectedResult : "aSowItemType = $aSowItemType, bSowItemType = $bSowItemType, result = $result"
        where:
        aSowItemType   |bSowItemType  |expectedResult
        TYPE_FRAGMENT  |TYPE_FRAGMENT |0
        TYPE_FRAGMENT  |TYPE_LMP      |-1
        TYPE_FRAGMENT  |TYPE_RNA      |-1

        TYPE_LMP       |TYPE_LMP      |0
        TYPE_LMP       |TYPE_RNA      |-1
        TYPE_RND       |TYPE_RNA      |0
    }

    def "test sortbySowItemType by container type"(aType, bType, expectedResult) {
        setup:
        def result
        when:
        result = ClaritySampleAnalyteComparator.customSorter(aType, bType, ClaritySampleAnalyteComparator.predefinedOrderByContainer)
        then:
        assert result == expectedResult: "aType = $aType, bType = $bType, result = $result"
        where:
        aType | bType | expectedResult
        plate | tube  | 1
        tube  | plate | -1
    }

    def "test sortbySowItemType"(
            aSowItemType,bSowItemType,
            aScientificProgram,bScientificProgram,
            aPlatform,bPlatform,
            aTargetInsertSizeKb,bTargetInsertSizeKb,
            expectedResult) {
        setup:
        def result
        ScheduledSample ss2 = Mock(ScheduledSample)
        ScheduledSample ss1 = Mock(ScheduledSample)

        ss1.udfScientificProgram >> aScientificProgram
        ss2.udfScientificProgram >> bScientificProgram

        ss1.udfSowItemType >> aSowItemType
        ss2.udfSowItemType >> bSowItemType

        ss1.platform >> aPlatform
        ss2.platform >> bPlatform

        ss1.udfTargetInsertSizeKb >> (aTargetInsertSizeKb as BigDecimal)
        ss2.udfTargetInsertSizeKb >> (bTargetInsertSizeKb as BigDecimal)
        when:
        result = ClaritySampleAnalyteComparator.sortbySowItemType(ss1,ss2)
        then:
        assert result == expectedResult: "aSowItemType = $aSowItemType, aScientificProgram = $aScientificProgram, result = $result"
        where:
        aSowItemType    |bSowItemType   |aScientificProgram  |bScientificProgram |aPlatform         | bPlatform           |aTargetInsertSizeKb   |bTargetInsertSizeKb   | expectedResult
        TYPE_FRAGMENT   |TYPE_FRAGMENT  |microbial           |microbial          |PLATFORM_ILLUMINA | PLATFORM_PACBIO     |null                  |null                  | -1
        TYPE_FRAGMENT   |TYPE_FRAGMENT  |'non microbial'     |'non microbial'    |PLATFORM_ILLUMINA | PLATFORM_PACBIO     |null                  |null                  | -1

        TYPE_LMP        |TYPE_LMP       |microbial           |microbial          |null              |null                 |23                    |13                    | -1
        TYPE_LMP        |TYPE_LMP       |'non microbial'     |'non microbial'    |PLATFORM_ILLUMINA | PLATFORM_PACBIO     |23                    |13                    | -1

        TYPE_RNA        |TYPE_RNA       |microbial           |microbial          |null              |null                 |null                  |null                  | 0
        TYPE_RNA        |TYPE_RNA       |'non microbial'     |'non microbial'    |null              |null                 |null                  |null                  | 0
    }

    def "test compare tubes"(aContainerType, bContainerType,
                             aName,bName,
                             expectedResult) {
        setup:
        ClaritySampleAnalyteComparator comparator = new ClaritySampleAnalyteComparator()
        def result

        ScheduledSample ss2 = Mock(ScheduledSample)
        ScheduledSample ss1 = Mock(ScheduledSample)
        ss1.udfScientificProgram >> microbial
        ss2.udfScientificProgram >> microbial
        ss1.udfSowItemType >> TYPE_RNA
        ss2.udfSowItemType >> TYPE_RNA

        SampleAnalyte sa1 = Mock(SampleAnalyte)
        SampleAnalyte sa2 = Mock(SampleAnalyte)
        sa1.claritySample >> ss1
        sa2.claritySample >> ss2

        sa1.containerType >> aContainerType
        sa2.containerType >> bContainerType

        sa1.worksheetSourceLocation >> null
        sa2.worksheetSourceLocation >> null

        sa1.name >> aName
        sa2.name >> bName

        when:
        result = comparator.compare(sa1,sa2)
        then:
        assert result == expectedResult
        where:
        aContainerType   |bContainerType   |aName   |bName  |expectedResult
        tube             |plate            |null    |null   |-1

        tube             |tube             |'1234'  |'1235' |-1
        tube             |tube             |'1236'  |'1235' |1
        tube             |tube             |'1235'  |'1235' |0
    }

    def "test compare plates"(
            aContainerType, bContainerType,
            aName,bName,
            aLocation, bLocation,
            expectedResult) {
        setup:
        ClaritySampleAnalyteComparator comparator = new ClaritySampleAnalyteComparator()
        def result

        ScheduledSample ss2 = Mock(ScheduledSample)
        ScheduledSample ss1 = Mock(ScheduledSample)
        ss1.udfScientificProgram >> microbial
        ss2.udfScientificProgram >> microbial
        ss1.udfSowItemType >> TYPE_RNA
        ss2.udfSowItemType >> TYPE_RNA

        SampleAnalyte sa1 = Mock(SampleAnalyte)
        SampleAnalyte sa2 = Mock(SampleAnalyte)
        sa1.claritySample >> ss1
        sa2.claritySample >> ss2

        sa1.containerType >> aContainerType
        sa2.containerType >> bContainerType

        sa1.containerName >> aName
        sa2.containerName >> bName

        sa1.worksheetSourceLocation >> aLocation
        sa2.worksheetSourceLocation >> bLocation
        when:
        result = comparator.compare(sa1,sa2)
        then:
        assert result == expectedResult
        where:
        aContainerType   |bContainerType   |aName     |bName        |aLocation  |bLocation  |expectedResult
        plate            |tube             |null      |null         |'C1'       |'C1'       |1

        plate            |plate            |'27-1234' |'27-1235'    |'C1'       |'C1'       |-1
        plate            |plate            |'27-1236' |'27-1234'    |'C1'       |'C1'       |1
        plate            |plate            |'27-1236' |'27-1236'    |'C1'       |'A2'       |-1
        plate            |plate            |'27-1236' |'27-1236'    |'H10'      |'A11'      |-1
        plate            |plate            |'27-1236' |'27-1236'    |'C1'       |'C1'       |0
    }
}
