package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryStocksAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.libraries.OnboardLibraries
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class OnboardLibraryStocksIntegrationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Value("\${sampleMaker.url}")
    private String sampleMakerUrl
    TestUtility testUtil
    @Shared
    ClarityProcess process
    PmoSample existingSample
    @Shared
    OnboardingLibraryStocksAdapter adapter

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        Analyte sampleAnalyte = testUtil.getAnalytesInStage(Stage.APPROVE_FOR_SHIPPING,1).first()
        existingSample = sampleAnalyte.claritySample
        process = processFromClarity
        def srcFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardLibraries.USER_LIBRARY_WORSHEET)}
        adapter = new OnboardingLibraryStocksAdapter(new ExcelWorkbook(srcFileNode?.id, process.testMode), 17828)

    }

    List<PmoSample> createSamplesInClarity(int sampleCount) {
        List<String> sampleIds = testUtil.createSamplesInClarity(sampleMakerUrl, sampleCount, 1, ContainerTypes.TUBE, true, 17828)
        return clarityNodeManager.getSampleNodes(sampleIds).collect { SampleFactory.sampleInstance(it)}
    }

    ClarityProcess getProcessFromClarity() {
        String processId = testUtil.getProcessIds(ProcessType.ONBOARD_LIBRARY_FILES.value, 1).first()
        return ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "execute process actions"(String action){
        setup:
        processId = '24-994556'
        expect:
        testUtil
        when:
        testUtil.executeAction(ProcessType.ONBOARD_LIBRARY_FILES, action, processId)
        then:
        noExceptionThrown()
        where:
        action << ['ProcessLibraryFile',
                   'FinishStep'
        ]
    }

    void "test Illumina onboarding scenarios" () {
        setup:
        OnboardingBean bean = getBeanForSample("Illumina NovaSeq 2 X 150", "Tag447_i7-Tag447_i5")
        adapter.onboardingBeansCached = [bean]
        when:
        List<String> errors = adapter.validateOnboardingData() //invalid runmodes and invalid index names
        then:
        assert errors.size() == 1
        when:
        PmoSample sample = createSamplesInClarity(1).first() // valid data with a new sample
        bean = getBeanForSample("Illumina NextSeq-HO 2 X 75", "Tag445_i7-Tag445_i5", sample)
        adapter.onboardingBeansCached = [bean]
        errors = adapter.validateOnboardingData()
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding samle
        List<Analyte> analytes = bean.analytes
        then:
        assert bean.libraryStock
        assert bean.analytes.size() == 4
        assert bean.libraryStock.udfExternal != "Y"
        assert checkAnalyte(bean, PmoSample.class)
        assert !bean.pmoSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ScheduledSample.class)
        assert !bean.scheduledSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ClaritySampleAliquot.class)
        assert !bean.sampleAliquot.activeWorkflows
        assert checkAnalyte(bean, ClarityLibraryStock.class)
        assert bean.libraryStock.activeWorkflows.contains(ClarityWorkflow.LIBRARY_QUANTIFICATION)
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean.analytes.containsAll(analytes)
        when:
        bean.sampleName = sample.udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 1
    }

    void "test pacbio onboarding scenarios" () {
        setup: //Pacbio without index
        PmoSample sample = createSamplesInClarity(1).first() // valid data with a new sample
        OnboardingBean bean = getBeanForSample("PacBio Sequel 1 X 600", "", sample)
        adapter.onboardingBeansCached = [bean]
        when:
        List<String> errors = adapter.validateOnboardingData() //invalid runmodes and invalid index names
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding samle
        List<Analyte> analytes = bean.analytes
        then:
        assert bean.libraryStock
        assert bean.analytes.size() == 4
        assert bean.libraryStock.udfExternal != "Y"
        assert checkAnalyte(bean, PmoSample.class)
        assert !bean.pmoSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ScheduledSample.class)
        assert !bean.scheduledSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ClaritySampleAliquot.class)
        assert !bean.sampleAliquot.activeWorkflows
        assert checkAnalyte(bean, ClarityLibraryStock.class)
        assert bean.libraryStock.activeWorkflows.contains(ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_ANNEALING)
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean.analytes.containsAll(analytes)
        when:
        bean.sampleName = sample.udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 1
    }

    boolean checkAnalyte(OnboardingBean bean, Class clazz) {
        for(Map.Entry entry : bean.udfMaps[clazz]) {
            ClarityUdf udf = entry.key
            Object value = entry.value
            if(bean.getAnalyteForType(clazz).getUdfAsString(udf.udf) != "$value")
                return false
        }
        return true
    }

    OnboardingBean getBeanForSample(String runMode, String indexName, PmoSample pmoSample = null) {
        if(!pmoSample)
            return new OnboardingBean(sampleName: "sample234025", sowRunMode: runMode,
                    dop: 1, libraryProtocol: "My protocol", labLibraryName: "library234025", aliquotVolume: 10, aliquotAmount: 12, pcrCycles: 2,
                    indexName: indexName, libFragmentSize: 3456, libVolume: 32, libMolarityPm: 1234, libConcentration: 1, libraryCreationSite: "JGI")
        OnboardingBean bean = new OnboardingBean(dop: 1, libraryProtocol: "My protocol", aliquotVolume: 10, aliquotAmount: 12, pcrCycles: 2,
                indexName: indexName, libFragmentSize: 3456, libVolume: 32, libMolarityPm: 1234, libConcentration: 1, libraryCreationSite: "JGI", sowRunMode: runMode)
        bean.sampleName = pmoSample.udfCollaboratorSampleName
        bean.labLibraryName = bean.sampleName.replace("sample", "library")
        return bean
    }
}
