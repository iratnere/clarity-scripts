package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel.SampleReceiptTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class ReceiveSamplesIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager
    SampleAnalyte sampleAnalyte
    ClarityProcess process

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        sampleAnalyte = testUtil.getAnalytesInStage(Stage.SAMPLE_RECEIPT).first()
        String processId = testUtil.getProcessIds(ProcessType.SM_SAMPLE_RECEIPT.value).first()
        process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
    }

    def cleanup() {
    }

    @Ignore
    void "execute process actions"(String action){
        setup:
        String processId = null
        processId = '24-1098116'
        expect:
        testUtil
        when:
        testUtil.executeAction(ProcessType.RECEIVE_SAMPLES, action, processId)
        then:
        noExceptionThrown()
        where:
        action << [//'PreProcessValidation',
                   'ValidateBarcodeAssociation',
                   'RouteReceivedSamples']
    }

    void "test process bean"(){
        setup:
        SampleReceiptTableBean bean = prepareBean(sampleAnalyte.containerNode)
        bean.receiptStatus.value = Analyte.PASS
        ReceiveSamples receiveSamples = new ReceiveSamples()
        receiveSamples.process = process
        when:
        PmoSample pmoSample = sampleAnalyte.claritySample
        receiveSamples.pmoSampleIds << pmoSample.pmoSampleId
        receiveSamples.processBean(bean, receiveSamples.getDateString(new Date()))
        then:
        assert pmoSample.udfSampleReceiptDate
        assert pmoSample.udfSampleReceiptResult
        assert receiveSamples.sampleReceivedStatusMap.size() == sampleAnalyte.containerNode.contentsArtifactNodes.size()
        assert receiveSamples.sampleStatusCvMap[pmoSample.pmoSampleId]
        assert receiveSamples.process.routingRequests.size() == sampleAnalyte.containerNode.contentsArtifactNodes.size()
        assert receiveSamples.process.routingRequests.collect{it.action}.unique().first() == Routing.Action.assign
        assert pmoSample.udfContainerLocation
        assert pmoSample.udfContainerLocationDate
        when:
        bean = prepareBean(sampleAnalyte.containerNode)
        bean.receiptStatus.value = Analyte.FAIL
        receiveSamples = new ReceiveSamples()
        receiveSamples.pmoSampleIds << pmoSample.pmoSampleId
        receiveSamples.process = process
        receiveSamples.processBean(bean, receiveSamples.getDateString(new Date()))
        then:
        assert pmoSample.udfSampleReceiptDate
        assert pmoSample.udfSampleReceiptResult
        assert receiveSamples.sampleReceivedStatusMap.size() == sampleAnalyte.containerNode.contentsArtifactNodes.size()
        assert receiveSamples.sampleStatusCvMap[pmoSample.pmoSampleId]
        assert receiveSamples.process.routingRequests.size() == sampleAnalyte.containerNode.contentsArtifactNodes.size()
        assert pmoSample.udfContainerLocation
        assert pmoSample.udfContainerLocationDate
    }

    SampleReceiptTableBean prepareBean(ContainerNode containerNode){
        SampleReceiptTableBean bean = new SampleReceiptTableBean()
        bean.operatorId = 2633
        bean.receiptStatus = new DropDownList()
        bean.dryIce = new DropDownList()
        bean.dryIce.value = 'Y'
        if(!containerNode.isPlate)
            bean.containerBarcode = containerNode.name
        bean.containerNode = containerNode
        return bean
    }
}
