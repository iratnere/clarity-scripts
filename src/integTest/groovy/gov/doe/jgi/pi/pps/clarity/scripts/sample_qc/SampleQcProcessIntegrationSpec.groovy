package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SIPMetagenomicsSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcProcessIntegrationSpec extends Specification {
    @Autowired
    NodeManager nodeManager
    TestUtility testUtility
    ContainerNode plate
    SampleQcProcess process
    List<ContainerNode> tubes
    String processId

    def setup() {
        testUtility = new TestUtility(nodeManager)
        plate = testUtility.getContainers(ContainerTypes.WELL_PLATE_96, 1).first()
        plate = SampleFactory.sampleInstance(plate.contentsArtifactNodes[0].sampleNode).pmoSample.sampleAnalyte.containerNode
        processId = testUtility.getProcessIds(ProcessType.SM_SAMPLE_QC.value,1,"Lakshmi","Vishwas").first()
        process = new SampleQcProcess(nodeManager.getProcessNode(processId))
        process.processNode.stepsNode.nodeManager = nodeManager
        tubes = testUtility.getContainers(ContainerTypes.TUBE, 5).collect{ SampleFactory.sampleInstance(it.contentsArtifactNodes[0].sampleNode).pmoSample.sampleAnalyte.containerNode}
    }

    def cleanup() {
    }

    void prepareSIPSamples(String processId) {
        SampleQcProcess sampleQcProcess = ProcessFactory.processInstance(nodeManager.getProcessNode(processId))
        List<PmoSample> pmoSamples = sampleQcProcess.inputAnalytes.collect{it.claritySample}
        String timeStamp = "${new Date().time}"
        pmoSamples.eachWithIndex { PmoSample pmoSample, int index ->
            pmoSample.udfGroupName = "Lakshmi$timeStamp"
            if(index < (pmoSamples.size()/3))
                pmoSample.udfIsotopeLabel = 'Unlabeled'
        }
        nodeManager.httpPutDirtyNodes()
    }

    @Ignore
    void "execute process actions"(String action){
        setup:
        processId = '24-1157133'
//        prepareSIPSamples(processId)
        expect:
        testUtility
        when:
        testUtility.executeAction(ProcessType.SM_SAMPLE_QC, action, processId)
        then:
        noExceptionThrown()
        where:
        action << [
//                'PreProcessValidation',
//                   'PlaceOutputs',
//                   'ValidatePlacement',
//                   'PrepareSampleQcSheet',
//                    'ProcessSampleQcSheet',
                    'RouteToNextWorkflow'
        ]
    }

    void "test sort inputs"() {
        expect:
        tubes
        plate
        process
        when:
        List<Analyte> analytes = plate.contentsArtifactNodes.collect{ AnalyteFactory.analyteInstance(it)}
        tubes.each{
            analytes << AnalyteFactory.analyteInstance(it.contentsArtifactNodes[0])
        }
        List<Analyte> sortedAnalytes = process.getSortedInputAnalytes(analytes)
        List<ContainerNode> containers = sortedAnalytes.collect{it.containerNode}.unique()
        int plateIndex = tubes.size()
        then:
        assert containers[plateIndex].id == plate.id
        (0..4).each{
            assert sortedAnalytes[it].name < sortedAnalytes[it+1].name
        }

    }

    void "test sample qc adapter factory"(){
        setup:
        Analyte analyte1 = process.inputAnalytes[0]
        Analyte analyte2 = process.inputAnalytes[1]
        Map<Long, JSONArray> sowMetadata = [:]
        when:
        sowMetadata.clear()
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "SIP Source Sample"
        sowMetadata[analyte2.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j2])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        List<SampleQcAdapter> adapters = process.getSampleQcAdapters([analyte2])
        then:
        assert adapters.size() == 1
        assert adapters[0] instanceof SIPMetagenomicsSampleQC
        when:
        sowMetadata.clear()
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "SIP Source Sample"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1])
        j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "SIP Source Sample"
        sowMetadata[analyte2.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j2])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1, analyte2])
        then:
        assert adapters.size() == 1
        assert adapters[0] instanceof SIPMetagenomicsSampleQC
        when:
        sowMetadata.clear()
        j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "Fragment"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1])
        j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "Fragment"
        sowMetadata[analyte2.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j2])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1, analyte2])
        then:
        assert adapters.size() == 1
        assert adapters[0] instanceof GenericSampleQC
        when:
        sowMetadata.clear()
        j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "SIP Source Sample"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1])
        j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "Fragment"
        sowMetadata[analyte2.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j2])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1, analyte2])
        then:
        assert adapters.size() == 2
        assert adapters[1] instanceof SIPMetagenomicsSampleQC
        assert adapters[0] instanceof GenericSampleQC
        when:
        sowMetadata.clear()
        j1 = new JSONObject()
        j1.'current-status' = 'Deleted'
        j1.'sow-item-type' = "SIP Source Sample"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1])
        then:
        thrown AssertionError
        when:
        sowMetadata.clear()
        j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "SIP Source Sample"
        j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "SIP Metagenome Fraction"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1, j2])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1])
        then:
        thrown AssertionError
        when:
        sowMetadata.clear()
        j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "SIP Metagenome Fraction"
        sowMetadata[analyte1.claritySample.pmoSample.pmoSampleId] = JSONArray.fromObject([j1])
        process.sampleQcUtility.sampleSowItems = sowMetadata
        adapters = process.getSampleQcAdapters([analyte1])
        then:
        thrown Exception
    }
}
