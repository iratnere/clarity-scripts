package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class DropDownServiceSpec extends Specification {

    @Autowired
    DropDownService dropDownService

    def setup() {
    }

    def cleanup() {
    }

    void "test getDropDownPassFail"() {
        when:
        DropDownList dropDownList = dropDownService.getDropDownPassFail()
        then:
        Analyte.PASS in dropDownList.controlledVocabulary
        Analyte.FAIL in dropDownList.controlledVocabulary
    }
}
