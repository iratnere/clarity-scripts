package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.LastKeyValueSection
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.PlateQCKeyValueSection
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.TubeSowQCTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SowQcWorksheetIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
    }

    def cleanup() {
    }

    void "test populate Tube Sow QC Table Bean"() {
        setup:
        ContainerNode containerNode = testUtil.getContainerInStage(Stage.SOW_ITEM_QC, ContainerTypes.TUBE, 1).first()
        Analyte analyte = AnalyteFactory.analyteInstance(containerNode.contentsArtifactNodes[0])
        TubeSowQCTableBean bean = new TubeSowQCTableBean()
        expect:
        analyte
        when:
        bean.prepareDropDowns()
        then:
        assert bean.sowQCStatus != null
        assert bean.sowQCStatus.controlledVocabulary != null
        assert bean.sowQCStatus.value == null
        assert bean.failureMode != null
        assert bean.failureMode.controlledVocabulary != null
        assert bean.failureMode.value == null
        when:
        PmoSample pmoSample = analyte.claritySample.pmoSample
        bean.copyDataFromRootSample(pmoSample)
        then:
        assert bean.itsSampleId == pmoSample.pmoSampleId
        assert bean.sampleLimsId == pmoSample.id
        assert bean.sampleBarcode == pmoSample.containerName
        assert bean.concentrationNgUl == pmoSample.udfConcentration
        assert bean.initSampleVolume == pmoSample.udfVolumeUl
        assert bean.hmwGDNA_YN == pmoSample.udfSampleHMWgDNAYN
        assert bean.qualityScore == pmoSample.udfQualityScore
        assert bean.rRnaRatio == pmoSample.udfRRnaRatio
        assert bean.qcComments == pmoSample.udfSampleQcNotes
        assert bean.smNotes == pmoSample.udfNotes
        when:
        ScheduledSample scheduledSample = analyte.claritySample
        bean.copyDataFromScheduledSample(scheduledSample, pmoSample)
        then:
        assert bean.libraryQueue == scheduledSample.libraryCreationQueue?.libraryCreationQueue
        assert bean.smQCInstructionFromPM == scheduledSample?.udfSmInstructions
        assert bean.sowItemId == scheduledSample?.sowItemId
    }

    void "test populate Plate Sow QC Key Value Bean"() {
        setup:
        ContainerNode containerNode = testUtil.getContainerInStage(Stage.SOW_ITEM_QC, ContainerTypes.WELL_PLATE_96, 1).first()
        Analyte analyte = AnalyteFactory.analyteInstance(containerNode.contentsArtifactNodes[0])
        PlateQCKeyValueSection bean = new PlateQCKeyValueSection()
        expect:
        analyte
        when:
        PmoSample pmoSample = analyte.claritySample.pmoSample
        bean.copyDataFromRootSample(pmoSample)
        then:
        assert bean.qcDate == pmoSample.udfSampleQcDate
        assert bean.materialType == pmoSample?.sequencingProject?.udfMaterialCategory
        assert bean.plateBarcode == pmoSample?.containerName
        assert bean.plateLimsId == pmoSample.containerLimsId
        assert bean.plateName == pmoSample.containerUdfLabel
        assert bean.smNotes == pmoSample.udfNotes
    }

    void "test populate Last Key Value Bean"() {
        setup:
        ContainerNode containerNode = testUtil.getContainerInStage(Stage.SOW_ITEM_QC, ContainerTypes.WELL_PLATE_96, 1).first()
        Analyte analyte = AnalyteFactory.analyteInstance(containerNode.contentsArtifactNodes[0])
        LastKeyValueSection bean = new LastKeyValueSection()
        expect:
        analyte
        when:
        bean.prepareDropDowns()
        then:
        assert bean.sowItemStatus != null
        assert bean.sowItemStatus.controlledVocabulary != null
        assert bean.sowItemStatus.value == null
        assert bean.failureMode != null
        assert bean.failureMode.controlledVocabulary != null
        assert bean.failureMode.value == null
        when:
        bean.populateBean(analyte.claritySample)
        then:
        assert bean.libraryCreationQueue == analyte.claritySample.libraryCreationQueue?.libraryCreationQueue
        assert bean.targetAliquotMass == analyte.claritySample.libraryCreationQueue?.plateTargetMassLibTrialNg
    }
}
