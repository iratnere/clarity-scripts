package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.Sequencing
import gov.doe.jgi.pi.pps.clarity.scripts.services.ProcessRegistrationService
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import java.lang.reflect.Constructor

@SpringBootTest(classes = [Application.class])
@Transactional
class SequencingRegistrationIntegrationSpec extends Specification {

    @Autowired
    ProcessRegistrationService processRegistrationService


    def setup() {

    }

    def cleanup() {
    }



    void "test ProcessType.SQ_SEQUENCING registration"() {
        setup:
        ProcessType processType = ProcessType.SQ_SEQUENCING

        expect:"fix me"
        ProcessType.toEnum(processType.value) == processType

        when:
        Constructor constructor = processRegistrationService.processConstructor(processType)

        then:
        constructor
        constructor.declaringClass == Sequencing
    }
}
