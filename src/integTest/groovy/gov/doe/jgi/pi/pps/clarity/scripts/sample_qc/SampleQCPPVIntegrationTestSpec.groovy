package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SIPMetagenomicsSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcUtility
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQCPPVIntegrationTestSpec extends Specification {
    TestUtility testUtility
    @Autowired
    NodeManager nodeManager
    Map<ContainerNode, List<Analyte>> queuedSamples
    ClarityProcess process
    List<Long> sampleIds = []

    def setup() {
        prepareDataToTest()
    }

    def cleanup() {
    }

    void prepareDataToTest(){
        queuedSamples = [:].withDefault {[]}
        testUtility = new TestUtility(nodeManager)
        List<ContainerNode> containers = testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.WELL_PLATE_96, 1)
        containers.addAll(testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.TUBE, 2))
        containers.each { ContainerNode container ->
            container.contentsArtifactNodes.each{ ArtifactNode artifactNode ->
                Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
                queuedSamples[container] << analyte
                sampleIds << analyte.claritySample.pmoSample.pmoSampleId
            }
        }
        process = ProcessFactory.processInstance(nodeManager.getProcessNode(testUtility.getProcessIds(ProcessType.SM_SAMPLE_QC.value,1,"Lakshmi", "Vishwas").first()))
    }

    def getStatuses(List<Long> pmoSampleIds = sampleIds){
        StatusService service = BeanUtil.getBean(StatusService.class)
        return service.getSampleStatusBatch(pmoSampleIds)
    }

    void "test check Is Sample"() {
        expect:
        queuedSamples
        process
        when:
        List<Analyte> analytes = queuedSamples.values().flatten()
        SampleQcUtility sampleQcUtility = new SampleQcUtility(analytes, Mock(ScheduledSampleService))
        GenericSampleQC genericAdapter = new GenericSampleQC(sampleQcUtility, analytes)
        SIPMetagenomicsSampleQC sipAdapter = new SIPMetagenomicsSampleQC(sampleQcUtility, analytes)
        List<String> gErrors = [], sipErrors = []
        analytes.each {
            gErrors << genericAdapter.checkIsSample(it)
            sipErrors << sipAdapter.checkIsSample(it)
        }
        gErrors.removeAll("")
        sipErrors.removeAll("")
        then:
        assert !gErrors
        assert !sipErrors
        when:
        analytes = process.inputAnalytes
        gErrors = []
        sipErrors = []
        analytes.each {Analyte analyte ->
            gErrors << genericAdapter.checkIsSample(analyte)
            sipErrors << sipAdapter.checkIsSample(analyte)
        }
        gErrors.removeAll("")
        sipErrors.removeAll("")
        then:
        assert !gErrors
        assert !sipErrors
        when:
        Analyte analyte = process.inputAnalytes.first().claritySample.pmoSample.getScheduledSamples(nodeManager).first().sampleAnalyte
        String error = genericAdapter.checkIsSample(analyte)
        then:
        assert error.contains("Invalid input $analyte. Expecting Root samples as input")
        when:
        error = sipAdapter.checkIsSample(analyte)
        then:
        assert error.contains("Invalid input $analyte. Expecting Root samples as input")
    }

    void "test check sample Status"(){
        expect:
        queuedSamples
        process
        when:
        SampleQcUtility sampleQcUtility = new SampleQcUtility(process.inputAnalytes, Mock(ScheduledSampleService))
        GenericSampleQC gAdapter = new GenericSampleQC(sampleQcUtility, process.inputAnalytes)
        SIPMetagenomicsSampleQC sipAdapter = new SIPMetagenomicsSampleQC(sampleQcUtility, process.inputAnalytes)
        def statuses = getStatuses()
        List<String> gErrors = gAdapter.checkSampleStatus(statuses)
        List<String> sipErrors = sipAdapter.checkSampleStatus(statuses)
        gErrors.removeAll("")
        sipErrors.removeAll("")
        then:
        assert !gErrors
        assert !sipErrors
        when:
        List<Long> pmoSampleIds = process.inputAnalytes.collect{it.claritySample.pmoSample.pmoSampleId}
        statuses = getStatuses(pmoSampleIds)
        gErrors = gAdapter.checkSampleStatus(statuses)
        sipErrors = sipAdapter.checkSampleStatus(statuses)
        then:
        assert gErrors.size() == pmoSampleIds.size()
        assert sipErrors.size() == pmoSampleIds.size()
    }

    void "test check Container Contents"(){
        setup:
        GenericSampleQC gAdapter = new GenericSampleQC(new SampleQcUtility(process.inputAnalytes, Mock(ScheduledSampleService)), process.inputAnalytes)
        when:
        Map<ContainerNode, Set<String>> containerAnalyteIds = [:]
        queuedSamples.each{
            containerAnalyteIds[it.key] = it.value.collect{it.id} as Set
        }
        String error = gAdapter.checkContainerContents(containerAnalyteIds)
        then:
        assert !error
        when:
        ContainerNode containerNode = containerAnalyteIds.keySet().first()
        List<String> contents = containerAnalyteIds[containerNode] as List
        contents.remove(contents.first())
        Map map = [:]
        map[containerNode] = contents
        error = gAdapter.checkContainerContents(map)
        then:
        assert error.contains("All samples from the input containers must be placed on the qc plate.")
    }

    void "test check Group Contents"() {
        setup:
        List<Analyte> analytes = queuedSamples.values().flatten()
        when:
        SampleQcUtility sampleQcUtility = new SampleQcUtility(analytes, Mock(ScheduledSampleService))
        String error = sampleQcUtility.checkIfGroupComplete("1", analytes.collect{it.claritySample.id})
        then:
        assert error == ""
        when:
        int size = analytes.size()
        sampleQcUtility = new SampleQcUtility(analytes[0..(size-2)], Mock(ScheduledSampleService))
        error = sampleQcUtility.checkIfGroupComplete("1", analytes.collect{it.claritySample.id})
        then:
        assert error == "All samples of a SIP group 1 should be QC'ed together. Please add samples [${analytes.last().claritySample.id}] to the icebucket and restart the process."
    }
}