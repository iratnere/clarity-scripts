package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.HudsonAlphaIlluminaOnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryStocksAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files.OnboardHudsonAlphaSequencingFiles
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
@Ignore
class OnboardingWithProductionDataIntegrationSpec extends Specification {
    static final logger = LoggerFactory.getLogger(OnboardingWithProductionDataIntegrationSpec.class)
    ExcelWorkbook workbook
    @Autowired
    NodeManager clarityNodeManager
    @Value("\${sampleMaker.url}")
    private String sampleMakerUrl
    TestUtility testUtil
    String seqFileDataFile = "./src/integTest/resources/SeqFilesTestData.xls"
    String libraryStocksDataFile = "./src/integTest/resources/LibraryStocksTestData.xls"

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
    }

    def cleanup() {
    }

    void "test illumina hudson alpha onboarding with production data"() {
        setup:
        workbook = new ExcelWorkbook(seqFileDataFile, true)
        when:
        workbook.sheetNames.each {String sheetName ->
            logger.info "Onboarding data from sheet $sheetName"
            testOnboardingSeqFilesWithData(getDataFromSheet(workbook, sheetName))
        }
        then:
        noExceptionThrown()
    }

    void "test Library stocks onboarding with production data"() {
        setup:
        workbook = new ExcelWorkbook(libraryStocksDataFile, true)
        when:
        workbook.sheetNames.each {String sheetName ->
            logger.info "Onboarding data from sheet $sheetName"
            testOnboardingLibrariesWithData(getDataFromSheet(workbook, sheetName))
        }
        then:
        noExceptionThrown()
    }

    List<PmoSample> createSamplesInClarity(int sampleCount) {
        List<String> sampleIds = testUtil.createSamplesInClarity(sampleMakerUrl, sampleCount, 1, ContainerTypes.TUBE, true, 17828)
        return clarityNodeManager.getSampleNodes(sampleIds).collect { SampleFactory.sampleInstance(it) }
    }

    void testOnboardingLibrariesWithData(List<OnboardingBean> beans) {
        updateSampleAndLibraryNames(beans)
        OnboardingLibraryStocksAdapter adapter = new OnboardingLibraryStocksAdapter(workbook, 17828)
        adapter.onboardingBeansCached = beans
        adapter.validateOnboardingData()
        adapter.mergeDataToClarity()
    }

    void testOnboardingSeqFilesWithData(List<HudsonAlphaBean> beans) {
        updateSampleAndLibraryNames(beans)
        updateFlowcellBarcodes(beans)
        HudsonAlphaIlluminaOnboardingAdapter adapter = new HudsonAlphaIlluminaOnboardingAdapter(workbook, 17828)
        adapter.allOnboardingBeansCached = beans
        adapter.validateOnboardingData()
        adapter.mergeDataToClarity()
    }

    void updateSampleAndLibraryNames(List<OnboardingBean> beans) {
        Map<String, List<OnboardingBean>> sampleBeans = beans.groupBy {it.sampleName}
        List<PmoSample> samples = createSamplesInClarity(sampleBeans.keySet().findAll{it != null}.size())
        sampleBeans.each { String sampleName, List<OnboardingBean> sBeans ->
            if(sampleName) {
                PmoSample sample = samples.pop()
                sBeans.each { OnboardingBean bean ->
                    bean.sampleName = sample.udfCollaboratorSampleName
                    bean.labLibraryName = bean.sampleName.replace("sample", "")
                }
            }
        }
    }

    void updateFlowcellBarcodes(List<HudsonAlphaBean> beans) {
        Map<String, List<HudsonAlphaBean>> flowcellBeans = beans.groupBy {it.flowcellBarcode}
        String timeStamp = new Date().timeString
        flowcellBeans.each {String flowcellBarcode, List<HudsonAlphaBean> fBeans ->
            fBeans.each { HudsonAlphaBean bean ->
                bean.flowcellBarcode = "$timeStamp$flowcellBarcode"
            }
        }
    }

    List<HudsonAlphaBean> getDataFromSheet(ExcelWorkbook workbook, String sheetName) {
        workbook.sections.clear()
        TableSection section = new TableSection(workbook.getSheetIndex(sheetName), HudsonAlphaBean.class, 'A2')
        workbook.addSection(section)
        workbook.load()
        return section.data
    }

    @Ignore//Rest
    void "collect production data for testing" () {
        setup:
        List<String> processIds = testUtil.getProcessIds(ProcessType.ONBOARD_LIBRARY_FILES.value, 1)
        when:
        processIds?.each {String processId ->
            logger.info "Process id $processId"
            ClarityProcess cp = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
            ExcelWorkbook workbook = cp.getOnboardingWorkbook(OnboardHudsonAlphaSequencingFiles.USER_SEQUENCING_WORSHEET)
            workbook.testMode = true
            workbook.store(null, processId)
        }
        then:
        noExceptionThrown()
    }
}
