package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.SequencerType
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.Sequencing
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SequencingSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager

    def setup() {
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "test Sequencing actionHandler"() {
        setup:
            //String action = 'PrepareDilutionSheet'
            ProcessNode processNode = clarityNodeManager.getProcessNode('24-718040')
        Sequencing clarityProcess = new Sequencing(processNode)
        SequencerType sequencerType = clarityProcess.sequencerType
        when:
            sequencerType.uploadSampleSheet()
        then:
            noExceptionThrown()
    }

}
