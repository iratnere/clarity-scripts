package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteSorter
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.abandon.email_notification.AbandonWorkEmailNotification
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class AbandonWorkSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager

    def setup() {
    }

    def cleanup() {
    }

//    @IgnoreRest
    def "test sendEmailNotification"() {
        setup:
        def processId = '24-191022' //5 tubes
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        AbandonWorkProcess clarityProcess = new AbandonWorkProcess(processNode)
        String action = 'Route to Next Workflow'
        AbandonWorkRouteToWorkflow actionHandler = clarityProcess.getActionHandler(action) as AbandonWorkRouteToWorkflow
        when:
        List<EmailEvent> emailEvents = actionHandler.sendEmailNotification()
        then: //AbandonQueueProcess:
        emailEvents
        emailEvents.size() == 5
        when:
        def analytes = actionHandler.process.inputAnalytes
        AnalyteSorter.sortInPlace(analytes)
        SampleAnalyte firstInputAnalyte = (SampleAnalyte) analytes[0]
        ScheduledSample scheduledSample = (ScheduledSample) firstInputAnalyte.claritySample
        def sowItemId = scheduledSample.sowItemId
        PmoSample pmoSample = scheduledSample.pmoSample
        SequencingProject sequencingProject = pmoSample.sequencingProject
        def sequencingProjectManagerId = sequencingProject.udfSequencingProjectManagerId
        def sampleContactName = EmailDetails.formatName(sequencingProject.udfSampleContact)
        def proposalId = sequencingProject.udfSequencingProjectProposalId as Integer
        Set<Integer> entityIds = [proposalId]
        EmailEvent ee = emailEvents.first()
        def body = "$ee.body"
        then:
        assert ee.cc == AbandonWorkEmailNotification.CC_EMAIL_GROUP
        assert ee.entity_id == entityIds
        assert ee.entity_type == EmailEvent.ENTITY_TYPE_PROPOSAL
        assert ee.subject == "Sow Item $sowItemId $AbandonWorkEmailNotification.SUBJECT $sequencingProject.udfSequencingProjectName" as String
        assert ee.from == sequencingProjectManagerId as String
        assert ee.to == sequencingProjectManagerId as String
        assert ee.body.startsWith("Dear $sampleContactName,")
        assert body.contains("This is an automated notification that sow item has been abandoned in the lab on")
        assert ee.body.endsWith("Sincerely,\nJGI Project Management Office")
        assert body.contains("""
Sequencing Project ID               $sequencingProject.name,
Sequencing Project Name             $sequencingProject.udfSequencingProjectName,
PI Name                             ${EmailDetails.formatName(sequencingProject.udfSequencingProjectPI)},
PM Name                             ${EmailDetails.formatName(sequencingProject.udfSequencingProjectManager)},
PMO Sample ID                       $pmoSample.name,
LIMS Sample ID                      $pmoSample.id,
SOW Item ID                         $sowItemId,
SOW Item Type                       $scheduledSample.udfSowItemType,
SOW Item Target Template size       $scheduledSample.udfTargetTemplateSizeBp,
SOW Item Target Insert size         $scheduledSample.udfTargetInsertSizeKb,
Notes                               $scheduledSample.udfNotes,
Sample mass                         $pmoSample.mass
""")
        noExceptionThrown()
    }
}
