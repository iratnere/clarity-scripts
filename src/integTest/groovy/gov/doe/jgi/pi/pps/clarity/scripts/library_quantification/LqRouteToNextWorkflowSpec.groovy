package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LqRouteToNextWorkflowSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    NodeConfig nodeConfig
    //production def limsid = '24-820691' tubes to tubes
    def setup() {
        nodeConfig = clarityNodeManager.nodeConfig
    }

    def cleanup() {
    }

    void "test populateStageQueue PLATE Done few wells failed"() {
        setup:
            Map<String,List<Analyte>> stageQueueMap
            def action = 'RouteToNextWorkflow'
            def limsid = '24-890204'
        ProcessNode processNode = clarityNodeManager.getProcessNode(limsid)
        ClarityProcess process = ProcessFactory.processInstance(processNode)
        LqRouteToNextWorkflow actionHandler = process.getActionHandler(action) as LqRouteToNextWorkflow

            String testFile = './src/integTest/resources/PlateDoneWellsFailed.xls'
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(testFile, true)
            actionHandler.loadExcelWorkbook(excelWorkbook)

            def libraryStockIds = process.inputAnalytes.findAll{
                (it as ClarityLibraryStock).udfLibraryQpcrResult == Analyte.FAIL
            }*.id
            List<ClarityLibraryStock> libraryStocks = libraryStockIds.collect{
                (ClarityLibraryStock) AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(it))
            }
            def scheduledSampleIds = libraryStocks.collect{ it.claritySample.sampleArtifactNode.id }
        expect:
            excelWorkbook
            libraryStocks.each { (it.claritySample as ScheduledSample).udfLcAttempt = 3 }
        when:
            stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
            stageQueueMap.size() == 4
            stageQueueMap[Stage.POOL_CREATION.uri].size() == 80
            stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 12
            stageQueueMap[Stage.REQUEUE_LIBRARY_CREATION.uri].size() == 12
            stageQueueMap[Stage.ABANDON_WORK.uri].size() == 12

            stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id in libraryStockIds
            stageQueueMap[Stage.REQUEUE_LIBRARY_CREATION.uri].first().id in scheduledSampleIds
            stageQueueMap[Stage.ABANDON_WORK.uri].first().id in scheduledSampleIds
        when:
            libraryStocks.each { (it.claritySample as ScheduledSample).udfLcAttempt = 2 }
            stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
            stageQueueMap.size() == 3
            stageQueueMap[Stage.POOL_CREATION.uri].size() == 80
            stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 12
            stageQueueMap[Stage.ALIQUOT_CREATION_RNA.uri].size() == 12

            stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id in libraryStockIds
            stageQueueMap[Stage.ALIQUOT_CREATION_RNA.uri].first().id in scheduledSampleIds
        when:
            libraryStocks.each { (it.claritySample as ScheduledSample).udfExternal = 'Y' }
            stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
            stageQueueMap.size() == 2
            stageQueueMap[Stage.POOL_CREATION.uri].size() == 80
            stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 12
            stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id in libraryStockIds
    }

    void "test populateStageQueue"() {
        setup:
        def action = 'RouteToNextWorkflow'
        def limsid = '24-128531'
        ProcessNode processNode = clarityNodeManager.getProcessNode(limsid)
        ClarityProcess process = ProcessFactory.processInstance(processNode)
        LqRouteToNextWorkflow actionHandler = process.getActionHandler(action) as LqRouteToNextWorkflow

        String testFile = './src/integTest/resources/PlateDoneLcRequeue.xls'
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(testFile, true)
        actionHandler.loadExcelWorkbook(excelWorkbook)

        def lsLimsid = '2-1520130'
        ClarityLibraryStock libraryStock = (ClarityLibraryStock) AnalyteFactory.analyteInstance(
                clarityNodeManager.getArtifactNode(lsLimsid)
        )
        expect:
        excelWorkbook
        libraryStock.maxLcAttempt > 2
        when:
        Map<String,List<Analyte>> stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
        stageQueueMap.size() == 4
        stageQueueMap[Stage.POOL_CREATION.uri].size() == 48
        stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 1
        stageQueueMap[Stage.REQUEUE_LIBRARY_CREATION.uri].size() == 1
        stageQueueMap[Stage.ABANDON_WORK.uri].size() == 1

        stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id == lsLimsid
        stageQueueMap[Stage.REQUEUE_LIBRARY_CREATION.uri].first().id == 'BAR13545A2PA1'
        stageQueueMap[Stage.ABANDON_WORK.uri].first().id == 'BAR13545A2PA1'
        when:
        (libraryStock.claritySample as ScheduledSample).udfLcAttempt = 2
        stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
        stageQueueMap.size() == 3
        stageQueueMap[Stage.POOL_CREATION.uri].size() == 48
        stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 1
        stageQueueMap[Stage.ALIQUOT_CREATION_RNA.uri].size() == 1

        stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id == lsLimsid
        stageQueueMap[Stage.ALIQUOT_CREATION_RNA.uri].first().id == 'BAR13545A2PA1'
        when:
        (libraryStock.claritySample as ScheduledSample).udfExternal = 'Y'
        stageQueueMap = actionHandler.populateStageQueue(excelWorkbook)
        then:
        stageQueueMap.size() == 2
        stageQueueMap[Stage.POOL_CREATION.uri].size() == 48
        stageQueueMap[Stage.ABANDON_QUEUE.uri].size() == 1

        stageQueueMap[Stage.ABANDON_QUEUE.uri].first().id == lsLimsid
    }
@Ignore
    void "test process"(){
        setup:
        def action = 'RouteToNextWorkflow'
        def limsid = '24-984193'//'24-907133' //clarityint1
        ProcessNode processNode = clarityNodeManager.getProcessNode(limsid)
        ClarityProcess process = ProcessFactory.processInstance(processNode)
        LqRouteToNextWorkflow actionHandler = process.getActionHandler(action) as LqRouteToNextWorkflow
        when:
        actionHandler.execute()
        then:
        noExceptionThrown()
    }
}
