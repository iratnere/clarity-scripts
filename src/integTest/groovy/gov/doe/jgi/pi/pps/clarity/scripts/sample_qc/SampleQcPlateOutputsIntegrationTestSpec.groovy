package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96Iterator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcPlateOutputsIntegrationTestSpec extends Specification {
    TestUtility testUtility
    @Autowired
    NodeManager nodeManager
    SampleQcProcess process
    List<Analyte> inputAnalytes = []

    def setup() {
        testUtility = new TestUtility(nodeManager)
        prepareDataToTest()
    }

    def cleanup() {
    }

    void prepareDataToTest(){
        String processId = testUtility.getProcessIds(ProcessType.SM_SAMPLE_QC.value, 1, "Lakshmi", "Vishwas").first()
        process = ProcessFactory.processInstance(nodeManager.getProcessNode(processId))
        List<ContainerNode> containerNodes = testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.TUBE, 10)
        containerNodes.addAll(testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.WELL_PLATE_96, 2))
        containerNodes.each{ ContainerNode containerNode ->
            inputAnalytes = containerNode.contentsArtifactNodes.collect{ ArtifactNode an -> AnalyteFactory.analyteInstance(an) }
        }
    }

    void "test empty output plate"() {
        setup:
        SampleQcPlaceOutputs placeOutputs = new SampleQcPlaceOutputs(process: process)
        expect:
        process
        when:
        Set<String> processOutputPlate = process.outputAnalytes.collect{it.containerId}
        String plateId = placeOutputs.emptyOutputPlate
        then:
        assert processOutputPlate.contains(plateId)
    }

    void "test get output analyte id"(){
        setup:
        SampleQcPlaceOutputs placeOutputs = new SampleQcPlaceOutputs(process: process)
        expect:
        process
        when:
        process.outputAnalytes.each{ Analyte op ->
            Analyte analyte = op.parentAnalyte
            String opId = placeOutputs.getOutputArtifactId(analyte)
            assert opId == op.id
        }
        then:
        noExceptionThrown()
    }

    void "test get output placements with existing process"() {
        setup:
        SampleQcPlaceOutputs placeOutputs = new SampleQcPlaceOutputs(process: process)
        expect:
        process
        when:
        OutputPlacements expected = process.stepsNode.placementsNode.outputPlacements
        OutputPlacements actual = placeOutputs.outputPlacements
        then:
        expected.placementsMap.each{ String key, OutputPlacement eop ->
            OutputPlacement aop = actual.placementsMap[key]
            assert aop.containerLocation.wellLocation.toString() == eop.containerLocation.wellLocation.toString()
            assert aop.artifactId == eop.artifactId
        }
    }

    void "test get output placements for queued analytes"() {
        setup:
        def outputPlates = ["Container1", "Container2", "Container3","Container4","Container5","Container6"]
        String plateLocations = "B1,C1,D1,E1,F1,G1,A2,B2,C2,D2,E2,F2,G2,H2,A3,B3,C3,D3,E3,F3,G3,H3,A4,B4,C4,D4,E4,F4,G4,H4,A5,B5,C5,D5,E5,F5,G5,H5,A6,B6,C6,D6,E6,F6,G6,H6,A7,B7,C7,D7,E7,F7,G7,H7,A8,B8,C8,D8,E8,F8,G8,H8,A9,B9,C9,D9,E9,F9,G9,H9,A10,B10,C10,D10,E10,F10,G10,H10,A11,B11,C11,D11,E11,F11,G11,H11,B12,C12,D12,E12,F12,G12"
        expect:
        inputAnalytes
        when:
        SampleQcPlaceOutputs placeOutputs = new SampleQcPlaceOutputs(process: process)
        placeOutputs.outputPlates = outputPlates
        List<Analyte> analytes = process.getSortedInputAnalytes(inputAnalytes)
        Map<Analyte, String> inputOutput = [:]
        analytes.each{
            inputOutput[it] = "Output${it.id}"
        }
        placeOutputs.inputOutput = inputOutput
        OutputPlacements outputPlacements = placeOutputs.getOutputPlacements(analytes)
        int i = 0
        List<String> wells = []
        analytes.each{ Analyte analyte ->
            OutputPlacement op = outputPlacements.placementsMap[inputOutput[analyte]]
            if(op.containerLocation.containerId == outputPlates[i]){
                wells << op.containerLocation.wellLocation
            }
            else{
                assert plateLocations.startsWith(wells.join(","))
                wells.clear()
                i++
            }
        }
        then:
        noExceptionThrown()
    }
@Ignore
    void "test validate location"(){
        setup:
        SampleQcValidatePlacements validatePlacements = new SampleQcValidatePlacements(process: process)
        when:
        WellAddress96Iterator iterator = new WellAddress96Iterator()
        inputAnalytes.each { Analyte analyte ->
            String error = validatePlacements.validateLocation(analyte, new WellAddress96(analyte.containerLocation))
            assert !error
            if(!iterator.hasNext())
                iterator = new WellAddress96Iterator()
            WellAddress96 wellAddress96 = iterator.next()
            if(wellAddress96.wellName == analyte.containerLocation.wellLocation)
                wellAddress96 = iterator.next()
            error = validatePlacements.validateLocation(analyte, wellAddress96)
            assert error
        }
        then:
        noExceptionThrown()
    }
}
