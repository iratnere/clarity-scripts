package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing.beans.PbSequelLibraryBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class PacBioSequencingPlateCreationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId
    PacBioSequencingPlateCreation clarityProcess
    String runName

    def setup() {
        processId = '24-898017' //PacBio Sequel II production
        runName = 'SQII02_Run5309'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        clarityProcess = new PacBioSequencingPlateCreation(processNode)
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "test actionHandler"(String action) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-899709')//(processId)
            PacBioSequencingPlateCreation clarityProcess = new PacBioSequencingPlateCreation(processNode)
        ActionHandler actionHandler = clarityProcess.getActionHandler(action) as ActionHandler
        when:
            actionHandler.execute()
		then:
            noExceptionThrown()
        where:
            action << [
                    'PrepareRunDesign'//'RouteToNextWorkflow'//'PrepareRunDesign'//'PreProcessValidation'
            ]
    }

    void "test read process udfs"() {
        setup:
            //def dnaTemplate = '010268100259100113017'
            //def binding = '012945101365900043019'
            def dnaControlComplex = '015124101717600022720'
            def sequencingKit = '015298101644500101019'
            def smrtLinkUuid = 'de5c4f36-fe57-4d54-8920-484b3952f7e4'

        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
            PacBioSequencingPlateCreation clarityProcess = new PacBioSequencingPlateCreation(processNode)
        when:
            processNode.setUdf(ClarityUdf.PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT.udf, dnaControlComplex)
            processNode.setUdf(ClarityUdf.PROCESS_DNA_SEQUENCING_REAGENT_KIT.udf, sequencingKit)
        then:
            smrtLinkUuid == clarityProcess.smartLinkUuid
            dnaControlComplex == clarityProcess.dnaInternalControlComplexLot
            sequencingKit == clarityProcess.dnaSequencingReagentKit
    }

    void "test getPbSequelLibraryBean"() {
        setup:
        PacBioSequencingTemplate pacBioPool = clarityProcess.outputAnalytes.find{
                PacBioSequencingTemplate sequencingTemplate = it as PacBioSequencingTemplate
                //sequencingTemplate.sequencingMode = 'CLR'
                //sequencingTemplate.generateCcsData = Boolean.FALSE.toString().toUpperCase()
                Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
                library instanceof ClarityLibraryPool
            } as PacBioSequencingTemplate
        PacBioSequencingTemplate pacBioLibrary = clarityProcess.outputAnalytes.find{
                PacBioSequencingTemplate sequencingTemplate = it as PacBioSequencingTemplate
                //sequencingTemplate.sequencingMode = 'CLR'
                //sequencingTemplate.generateCcsData = Boolean.FALSE.toString().toUpperCase()
                Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
                library instanceof ClarityLibraryStock
            } as PacBioSequencingTemplate
        when:
        PbSequelLibraryBean bean = clarityProcess.getPbSequelLibraryBean(runName, pacBioPool)
        then:
            !bean.experimentName
            !bean.experimentId
            !bean.experimentDescription
            bean.runName == runName
            bean.systemName == pacBioPool.runMode
            !bean.runComments
            bean.isCollection == Boolean.TRUE.toString().toUpperCase()
            bean.isCollection == bean.isCollection
            bean.sampleWell == pacBioPool.wellLocationInPacBioFormat
            bean.sampleName == pacBioPool.sampleNameForCsv
            bean.sequencingMode == 'CLR'
            !bean.sampleComment
            bean.movieTimeHrs == pacBioPool.acquisitionTime / 60
            bean.insertSizeBp == pacBioPool.insertSize
            bean.onPlateLoadingConcentrationPm == pacBioPool.udfPacBioLoadingConcentrationNm * 1000
            !bean.usePredictiveLoading
            !bean.loadingTarget
            !bean.maximumLoadingTimeHrs
             bean.sizeSelection == pacBioPool.sizeSelection
            bean.templatePrepKitBoxBarcode == pacBioPool.dnaTemplatePrepKitBoxBarcode
            bean.bindingKitBoxBarcode == pacBioPool.bindingKitBoxBarcodeAsExcelString //from paren analyte (binding complex)
            bean.sequencingKitBoxBarcode == clarityProcess.dnaSequencingReagentKit
            bean.dnaControlComplexBoxBarcode == clarityProcess.dnaInternalControlComplexLot
            bean.automationName == pacBioPool.automationName
            !bean.automationParameters
            bean.generateCcsData == Boolean.FALSE.toString().toUpperCase()
            bean.barcodeSet == clarityProcess.smartLinkUuid
            bean.sampleIsBarcoded == Boolean.TRUE.toString().toUpperCase()
            bean.sampleIsBarcoded == pacBioPool.sampleIsBarcoded
            bean.sameBarcodesOnBothEndsOfSequence == Boolean.TRUE.toString().toUpperCase()
            bean.sameBarcodesOnBothEndsOfSequence == pacBioPool.sameBarcodesOnBothEndsOfSequence
            !bean.barcodeName // == pacBioPool.barcodedSampleName
            !bean.bioSampleName
            !bean.pipelineId
            !bean.analysisName
            !bean.entryPoints
            !bean.taskOptions
        when:
            pacBioPool.artifactNode.setUdf(ClarityUdf.ANALYTE_ACQUISITION_TIME.udf, 3600)
            clarityProcess.getPbSequelLibraryBean(runName, pacBioPool)
        then:
            def e = thrown(WebException)
            e.message.contains('invalid movieTimeHrs value <60>')
        when:
            PbSequelLibraryBean lb = clarityProcess.getPbSequelLibraryBean(runName, pacBioLibrary)
        then:
            !lb.experimentName
            !lb.experimentId
            !lb.experimentDescription
            lb.runName == runName
            lb.systemName == pacBioLibrary.runMode
            !lb.runComments
            lb.isCollection == Boolean.TRUE.toString().toUpperCase()
            bean.isCollection == bean.isCollection
            lb.sampleWell == pacBioLibrary.wellLocationInPacBioFormat
            lb.sampleName == pacBioLibrary.sampleNameForCsv
            lb.sequencingMode == 'CLR'
            !lb.sampleComment
            lb.movieTimeHrs == pacBioLibrary.acquisitionTime / 60
            lb.insertSizeBp == pacBioLibrary.insertSize
            lb.onPlateLoadingConcentrationPm == pacBioLibrary.udfPacBioLoadingConcentrationNm * 1000
            !lb.usePredictiveLoading
            !lb.loadingTarget
            !lb.maximumLoadingTimeHrs
            lb.sizeSelection == pacBioLibrary.sizeSelection
            lb.templatePrepKitBoxBarcode == pacBioLibrary.dnaTemplatePrepKitBoxBarcode
            lb.bindingKitBoxBarcode == pacBioLibrary.bindingKitBoxBarcodeAsExcelString //from paren analyte (binding complex)
            lb.sequencingKitBoxBarcode == clarityProcess.dnaSequencingReagentKit
            lb.dnaControlComplexBoxBarcode == clarityProcess.dnaInternalControlComplexLot
            lb.automationName == pacBioLibrary.automationName
            !lb.automationParameters
            bean.generateCcsData == Boolean.FALSE.toString().toUpperCase()
            !lb.barcodeSet
            lb.sampleIsBarcoded == Boolean.FALSE.toString().toUpperCase()
            lb.sampleIsBarcoded == pacBioLibrary.sampleIsBarcoded
            !lb.sameBarcodesOnBothEndsOfSequence
            lb.sameBarcodesOnBothEndsOfSequence == pacBioLibrary.sameBarcodesOnBothEndsOfSequence
            !lb.barcodeName
            !lb.bioSampleName
            !lb.pipelineId
            !lb.analysisName
            !lb.entryPoints
            !lb.taskOptions
    }

    @Ignore//Rest
    void "test combined RunDesign"(){
        setup:
        def action = "PrepareRunDesign"
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-837361')
        PacBioSequencingPlateCreation clarityProcess = new PacBioSequencingPlateCreation(processNode)
        PbPrepareRunDesign actionHandler = clarityProcess.getActionHandler(action) as PbPrepareRunDesign
        clarityProcess.testMode = true
        String fileName = "ScannedBarcodes_$PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7" + '.csv'
        when:
        //if (clarityProcess.isPacBioSequel) {
        List beans = clarityProcess.getPbSequelLibraryBeans('SQ02_Run5033')
        beans.each{
            println it
        }
        String combinedRunDesign = actionHandler.createLibraryContents(beans)
        File file = new File(fileName)
        file.withWriter{ it.write(combinedRunDesign) }
        // }
        then:
        noExceptionThrown()
    }

    void "test getPoolMembersBeans template created from pool"() {
        setup:
            def processId = '24-907826' //PacBio Sequel II
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
            List<PacBioSequencingTemplate> templates = processNode.getOutputAnalytes().collect {
                AnalyteFactory.analyteInstance(it) as PacBioSequencingTemplate
            }
        PacBioSequencingTemplate pacBioPool = templates.find { PacBioSequencingTemplate sequencingTemplate ->
            Analyte library = sequencingTemplate.parentLibrary //could be ClarityLibraryStock or ClarityLibraryPool
                library instanceof ClarityLibraryPool
            } as PacBioSequencingTemplate
        expect:
            pacBioPool
        when:
            List<PbSequelLibraryBean> beans = PacBioSequencingPlateCreation.getPoolMembersBeans(pacBioPool)
        then:
            (pacBioPool.parentLibrary as ClarityLibraryPool).poolMembers.each { Analyte poolMember ->
                PbSequelLibraryBean pbSequelLibraryBean = beans.find { it.bioSampleName == poolMember.name }
                assert pbSequelLibraryBean.isCollection == 'FALSE'
                assert pbSequelLibraryBean.sampleName == "${pacBioPool.parentLibrary.name}_${pacBioPool.id}"
                assert pbSequelLibraryBean.barcodeName == (poolMember as ClarityLibraryStock).udfIndexName
                assert pbSequelLibraryBean.bioSampleName == poolMember.name
            }
    }
}