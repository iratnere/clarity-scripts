package gov.doe.jgi.pi.pps.clarity.scripts.librarycreation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.OutputPlate
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class OutputPlateSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def processId = '24-228539'
    String indexContainerBarcode = 'Illumina_TruSeq-HT_Dual-Index'
    String indexName = 'Tag058_i7-Tag058_i5' //['Tag058_i7-Tag058_i5': 'TGCTTGGT-ACCAAGCA']

    def "test processIndexContainerBarcodes plate"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
            clarityLibraryStocks.each{
                it.udfIndexContainerBarcode = 'dummyIndexContainerBarcode'
                it.udfIndexName = ''
            }
        when:
            outputPlate.processIndexContainerBarcodes(clarityLibraryStocks)
        then:
            def e = thrown(IllegalArgumentException)
            e.message.contains("Index names [dummyIndexContainerBarcode] are not registered with Clarity.")
        when:
            clarityLibraryStocks.each{
                it.udfIndexContainerBarcode = indexContainerBarcode
            }
            outputPlate.processIndexContainerBarcodes(clarityLibraryStocks)
        then:
            noExceptionThrown()
    }

    def "test validateLibraryIndexUdfBatch plate"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
            clarityLibraryStocks.each{
                it.udfIndexContainerBarcode = 'dummyIndexContainerBarcode'
                it.udfIndexName = ''
            }
        when:
            outputPlate.validateLibraryIndexUdfBatch(clarityLibraryStocks)
        then:
            def e = thrown(WebException)
            e.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e.message.contains("Unspecified Index Name ''")
        when:
            clarityLibraryStocks.each{
                it.udfIndexName = 'dummyIndexName'
            }
            outputPlate.validateLibraryIndexUdfBatch(clarityLibraryStocks)
        then:
            noExceptionThrown()
    }

    def "test processIndexNames plate"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
            clarityLibraryStocks.each{
                it.udfIndexName = 'dummyIndexName'
            }
        when:
            outputPlate.processIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            def e = thrown(IllegalArgumentException)
            e.message.contains("Index names [dummyIndexName] are not registered with Clarity.")
        when:
            clarityLibraryStocks.each{
                it.udfIndexName = indexName
            }
            outputPlate.processIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            noExceptionThrown()
            clarityLibraryStocks.each{
                assert it.udfIndexName == indexName
                assert it.udfIndexContainerBarcode == indexName
            }
        when:
            clarityLibraryStocks[0].udfIndexName = ArtifactIndexService.INDEX_NAME_N_A
            outputPlate.validateIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            def e1 = thrown(WebException)
            e1.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e1.message.contains("Invalid Index Name '$ArtifactIndexService.INDEX_NAME_N_A'")
    }

    def "test validateIndexNames plate"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
            clarityLibraryStocks.each{
                it.udfIndexName = 'dummyIndexName'
            }
        when:
            outputPlate.validateIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            def e = thrown(IllegalArgumentException)
            e.message.contains("Index names [dummyIndexName] are not registered with Clarity.")
        when:
            clarityLibraryStocks.each{
                it.udfIndexName = indexName
            }
            outputPlate.validateIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            noExceptionThrown()
            clarityLibraryStocks.each{
                assert it.udfIndexName == indexName
            }
        when:
            clarityLibraryStocks[0].udfIndexName = ArtifactIndexService.INDEX_NAME_N_A
            outputPlate.validateIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
        then:
            def e1 = thrown(WebException)
            e1.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e1.message.contains("Invalid Index Name '$ArtifactIndexService.INDEX_NAME_N_A'")
    }

    def "test validateLibraryIndexesSize"(){
        setup:
            def indexContainerBarcode = 'Illumina_smRNA_Single-Index'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
            List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
            clarityLibraryStocks.each{
                it.udfIndexContainerBarcode = indexContainerBarcode
            }
        when:
            outputPlate.validateLibraryIndexesSize(clarityLibraryStocks, [:])
        then:
            def e = thrown(WebException)
            e.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e.message.contains("Invalid Library Index Set '$indexContainerBarcode'")
        when:
            Map<String, List<Index>> barcodeToIndexes = [:]
            barcodeToIndexes[indexContainerBarcode] = []
            outputPlate.validateLibraryIndexesSize(clarityLibraryStocks, barcodeToIndexes)
        then:
            def e1 = thrown(WebException)
            e1.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e1.message.contains("Invalid Library Index Set '$indexContainerBarcode'")
        when:
        Index index = new Index(
                    indexName: 'dummyIndexName',
                    indexSequence: 'ACTACT'
            )
            barcodeToIndexes[indexContainerBarcode] = [index]
            def size = barcodeToIndexes[indexContainerBarcode].size()
            outputPlate.validateLibraryIndexesSize(clarityLibraryStocks, barcodeToIndexes)
        then:
            def e2 = thrown(WebException)
            e2.message.contains(LibraryCreationProcess.START_ERROR_MSG)
            e2.message.contains("Not enough indexes for the input plate.")
            e2.message.contains("Index Set plate '$indexContainerBarcode' has only '$size' indexes")
            e2.message.contains("Input plate has '${clarityLibraryStocks.size()}' libraries.")
        when:
            (2..clarityLibraryStocks.size()).each{
                barcodeToIndexes[indexContainerBarcode] << new Index(
                        indexName: 'dummyIndexName',
                        indexSequence: 'ACTACT'
                )
            }
        then:
            barcodeToIndexes[indexContainerBarcode].size() == clarityLibraryStocks.size()
        when:
            outputPlate.validateLibraryIndexesSize(clarityLibraryStocks, barcodeToIndexes)
        then:
            noExceptionThrown()
    }

    @Ignore
    def "test updateLibraryIndexUdf plate"() {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId) //plate
        LibraryCreationProcess libraryCreationProcess = new LibraryCreationProcess(processNode)
        OutputPlate outputPlate = new OutputPlate(libraryCreationProcess)
        List<ClarityLibraryStock> clarityLibraryStocks = libraryCreationProcess.outputAnalytes as List<ClarityLibraryStock>
        clarityLibraryStocks.each{
            it.udfIndexName = indexName
        }
        when:
        outputPlate.updateLibraryIndexUdf(clarityLibraryStocks)
        then:
        1 * outputPlate.processIndexNames(clarityLibraryStocks, LibraryCreationProcess.getIndexNames(clarityLibraryStocks))
    }
}
