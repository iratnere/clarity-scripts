package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.Libraries
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import groovy.json.JsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryPercentageServiceSpec extends Specification {

    @Autowired
    LibraryPercentageService libraryPercentageService
    @Autowired
    NodeManager nodeManager
//2-2236907 library all environments
    def setup() {
    }

    def cleanup() {
    }

    void "test convertJson"() {
        when:
/*
            def expectedContents =
'{"libraries":[{"library-lims-id":"2-2236907","sow-item-id":232263,"sequencing-optimization-factor":1.7624,"total-number-of-reads":2988606902,"number-of-reads-bp":25000000,"lane-fraction":0.02310775638360}]}'
*/
            def responseJson = libraryPercentageService.laneFractionsResponseJson(['2-2236907'])
            /*
            url: http://clarity-dev01.jgi-psf.org:60966/lane-fraction
            request json: {"libraries":[{"library-lims-id":"2-2236907"}]}
            response json: [libraries:[[
                                    library-lims-id:2-2236907,
                                    sow-item-id:232263,
                                    sequencing-optimization-factor:1.7624,
                                    total-number-of-reads:2988606902,
                                    number-of-reads-bp:25000000,
                                    lane-fraction:0.02310775638360
                            ]]]
             */
            String fileContents = convert(responseJson)
        then:
            fileContents.startsWith('{"libraries":[{"library-lims-id"')
            fileContents.endsWith('}]}')
            //fileContents == expectedContents
        when:
            convert(null)
        then:
            noExceptionThrown()
        when:
            def result = convert(null)
        then:
            result == 'null'
            noExceptionThrown()
    }

    static final String convert(def responseJson) {
        return new JsonBuilder(responseJson) as String
    }

    void "test laneFractionsResponseJson one library"() {
        when:

            def responseJson = libraryPercentageService.laneFractionsResponseJson(['2-2236907'])
        /*
        response json: [libraries:[[
                                    library-lims-id:2-2236907,
                                    sow-item-id:232263,
                                    sequencing-optimization-factor:1.7624,
                                    total-number-of-reads:2988606902,
                                    number-of-reads-bp:25000000,
                                    lane-fraction:0.02310775638360
                        ]]]
         */
        then:
            responseJson
        Libraries units = new Libraries(responseJson,[AnalyteFactory.analyteInstance(nodeManager.getArtifactNode('2-2236907'))])
            units.libraries.size() == 1
        LaneFraction library = units.libraries[0]
            library.libraryLimsId == '2-2236907'
            library.sowItemId == 232263
            library.sequencingOptimizationFactor != null
            library.totalNumberOfReads
            library.numberOfReadsBp
            library.laneFraction
    }
    @Ignore
    void "test laneFractionsResponseJson"() {
        setup:
            def bigPoolProcessLimsId = '122-1111299' //claritydev1
        ProcessNode processNode = nodeManager.getProcessNode(bigPoolProcessLimsId)
            List libraryLimsIds = processNode.inputAnalytes*.id
        when:
            def responseJson = libraryPercentageService.laneFractionsResponseJson(libraryLimsIds)
        then:
            responseJson
        Libraries units = new Libraries(responseJson)
            units.libraries.size() == 3864
            units.libraries.each { library ->
                library.libraryLimsId
                library.sowItemId
                library.sequencingOptimizationFactor
                library.totalNumberOfReads
                library.numberOfReadsBp
                library.laneFraction
            }
    }

    void "test laneFractionsResponseJson error"() {
        setup:
        List libraryLimsIds = ['2-97572', '2-97571', '2-97570', '2-97569']
        when:
        def responseJson = libraryPercentageService.laneFractionsResponseJson(libraryLimsIds)
        then:
        def e = thrown(WebException)
        e.message.contains('BBUNU/2-97572: value [reads per barcode] is not supported')
        e.message.contains('BBUNT/2-97571: value [reads per barcode] is not supported')
        e.message.contains('BBUNS/2-97570: value [reads per barcode] is not supported')
        e.message.contains('BBUNO/2-97569: value [reads per barcode] is not supported')
        !responseJson
        /*
        http://clarity-dev01.jgi.doe.gov:60966/lane-fraction service response status: [422]
        BBUNU/2-97572: value [reads per barcode] is not supported
        BBUNT/2-97571: value [reads per barcode] is not supported
        BBUNS/2-97570: value [reads per barcode] is not supported
        BBUNO/2-97569: value [reads per barcode] is not supported
         */
    }

    void "test laneFractionsSubmissionUrl"() {
        when:
            def result = libraryPercentageService.laneFractionsSubmissionUrl
        then:
            !result.contains('/pps-1/lane-fraction')
            result.contains('/lane-fraction')
    }

    void "test laneFractionsResponseJson invalid library Actual Template Size (bp)"() {
        when:
        /*
        http://clarity-dev01.jgi.doe.gov:60966/lane-fraction service response status: [422]
        CAYGN/2-2281187: Run Mode has not been set for artifact
        CAYGN/2-2281187: Actual Template Size (bp) has not been set for artifact
        CAYNC/2-2281188: Run Mode has not been set for artifact
        CAYNC/2-2281188: Actual Template Size (bp) has not been set for artifact
        CAYHP/2-2281189: Run Mode has not been set for artifact
        CAYHP/2-2281189: Actual Template Size (bp) has not been set for artifact
        CAYHA/2-2281190: Run Mode has not been set for artifact
        CAYHA/2-2281190: Actual Template Size (bp) has not been set for artifact
         */
            libraryPercentageService.laneFractionsResponseJson(['2-2281187','2-2281188','2-2281189','2-2281190'])
        then:
            def e = thrown(WebException)
            e.message.contains('service response status: [422]')
            e.message.contains('CAYGN/2-2281187: Actual Template Size (bp) has not been set for artifact')
            e.message.contains('CAYNC/2-2281188: Actual Template Size (bp) has not been set for artifact')
    }
}