package gov.doe.jgi.pi.pps.clarity.scripts.services

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.SamplesFractionationModel
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleFractionationServiceSpec extends Specification {

    @Autowired
    SampleFractionationService sampleFractionationService
    @Autowired
    NodeManager nodeManager
    @Autowired
    Environment environment
    def processLimsId

    def setup() {
        processLimsId = environment.activeProfiles.find{true} == 'claritydev1' ? '24-911723' : '24-911623'
    }

    def cleanup() {
    }

    void "test getSamplesFractionationModel"() {
        setup:
            def contactId = 2633
        when:
/*
submission: {
  "submitted-by": 2633,
  "fraction-samples": [
    {
      "original-sample-id": 229020,
      "fraction-density-g-ml": 1.4095244444444444,
      "fraction-dna-concentration-ng-ul": 0.025,
      "fraction-sample-barcode": "2-3972821",
      "fraction-sample-name": "229020_3",
      "fraction-sample-volume-ul": 15
    }
  ]
}
*/
            def limsIds = environment.activeProfiles.find{true} == 'claritydev1' ? '2-3972821' : '2-3967575'
        SamplesFractionationModel model = sampleFractionationService.getSamplesFractionationModel([limsIds], contactId)
            String jsonBody = new ObjectMapper().writeValueAsString(model)
            //def json = new JSONObject(jsonBody)
        then:
            jsonBody.startsWith('{"submitted-by":2633,"fraction-samples":[{"original-sample-id":')
            jsonBody.endsWith('}]}')
            model.submittedBy == contactId
            model.fractionSamples.size() == 1
            model.fractionSamples.each { // only one
                assert it.originalSampleId == 229020
                assert it.fractionDensityGMl == 1.4095244444444444
                assert it.fractionDnaConcentrationNgUl == 0.025
                assert it.fractionSampleBarcode != 'b1-247691'
                assert it.fractionSampleName == '229020_3'
                assert it.fractionSampleVolumeUl == 15
                if (environment.activeProfiles.find{true} == 'claritydev1') {
                    assert it.fractionSampleBarcode == '2-3972821'
                } else {
                    assert it.fractionSampleBarcode == '2-3967575'
                }
            }
            noExceptionThrown()
    }
    @Ignore
    void "test createFractionSamples"() {
        setup:
        ProcessNode processNode = nodeManager.getProcessNode('24-949445')
            List fractionLimsIds = processNode.outputAnalytes.findAll{it.systemQcFlag}*.id
        when:
            def responseJson = sampleFractionationService.createFractionSamples(fractionLimsIds, 2633)
        then:
            responseJson
    }

    void "test createFractionSamples only failed samples"() {
        setup:
        List fractionLimsIds = []
        when:
        def result = sampleFractionationService.createFractionSamples(fractionLimsIds, 2633)
        then:
        noExceptionThrown()
        !result
    }

    void "test createFractionSamples error"() {
        setup:
        ProcessNode processNode = nodeManager.getProcessNode(processLimsId)
            List fractionLimsIds = processNode.outputAnalytes*.id//.findAll{it.systemQcFlag}*.id
        when:
            sampleFractionationService.createFractionSamples(fractionLimsIds, 2633)
        then:
            def e = thrown(WebException)
            e.message.contains('service response status: [422]')
            /*
            http://clarity-dev01.jgi.doe.gov:60966/sample-fractionation service response status: [422]
[229020_1] fraction-dna-concentration-ng-ul: numeric instance is not strictly greater than the required minimum 0
[229020_10] fraction-dna-concentration-ng-ul: numeric instance is not strictly greater than the required minimum 0
[229020_11] fraction-dna-concentration-ng-ul: numeric instance is not strictly greater than the required minimum 0
...
             */
    }

    void "test getSubmissionUrl"() {
        when:
            def result = sampleFractionationService.submissionUrl
        then:
            !result.contains('/v2/sample-fractionation')
            result.contains('/sample-fractionation')
    }
}