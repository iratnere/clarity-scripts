package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.domain.LibraryNames
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.HudsonAlphaIlluminaOnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files.FinishStep
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files.OnboardHudsonAlphaSequencingFiles
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files.ProcessHudsonAlphaSequencingFile
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class OnboardSequencingFilesIntegrationSpec extends Specification {
    static final Logger logger = LoggerFactory.getLogger(OnboardSequencingFilesIntegrationSpec.class)
    @Autowired
    NodeManager clarityNodeManager
    @Value("\${sampleMaker.url}")
    private String sampleMakerUrl
    @Autowired
    StatusService service
    TestUtility testUtil
    List<Analyte> sampleAnalytes = []
    String processId
    PmoSample existingSample
    @Shared
    HudsonAlphaIlluminaOnboardingAdapter adapter
    OnboardHudsonAlphaSequencingFiles process

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        processId = testUtil.getProcessIds(ProcessType.ONBOARD_SEQUENCING_FILES.value).first()
        process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
        Analyte sampleAnalyte = testUtil.getAnalytesInStage(Stage.APPROVE_FOR_SHIPPING,1).first()
        existingSample = sampleAnalyte.claritySample
        def srcFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardHudsonAlphaSequencingFiles.USER_SEQUENCING_WORSHEET)}
        adapter = new HudsonAlphaIlluminaOnboardingAdapter(new ExcelWorkbook(srcFileNode?.id, process.testMode),17828)
    }

    def cleanup() {
    }

    List<PmoSample> createSamplesInClarity(int sampleCount) {
        List<String> sampleIds = testUtil.createSamplesInClarity(sampleMakerUrl, sampleCount, 1, ContainerTypes.TUBE, true, 17828)
        return clarityNodeManager.getSampleNodes(sampleIds).collect { SampleFactory.sampleInstance(it) }
    }

    @Ignore//Rest
    void "execute process actions"(String action){
        setup:
        processId = '24-994556'
        expect:
        testUtil
        when:
        testUtil.executeAction(ProcessType.ONBOARD_SEQUENCING_FILES, action, processId)
        then:
        noExceptionThrown()
        where:
        action << ['ProcessSequencingFile',
//                'FinishStep'
        ]
    }

    void "test Illumina onboarding without pool scenarios" () {
        setup:
        HudsonAlphaBean bean = getBeanForSample("TGTGCGTT-AACGCACA", "Flowcell")
        adapter.allOnboardingBeansCached = [bean]
        when:
        List<String> errors = adapter.validateOnboardingData() //invalid runmodes and invalid index names
        then:
        assert errors.size() == 1
        when:
        PmoSample sample = createSamplesInClarity(1).first() // valid data with a new sample
        String flowcellBarcode = sample.udfCollaboratorSampleName.replace("sample", "Flowcell")
        bean = getBeanForSample("TGTGCGTT-AACGCACA",flowcellBarcode,  sample)
        adapter.allOnboardingBeansCached = [bean]
        adapter.uniqueOnboardingBeansCached.clear()
        errors = adapter.validateOnboardingData()
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding sample
        List<Analyte> analytes = bean.analytes
        then:
        assert bean.physicalRunUnit
        assert !bean.libraryPool
        assert bean.analytes.size() == 5
        assert bean.physicalRunUnit.udfExternal == "Y"
        assert checkAnalyte(bean, PmoSample.class)
        assert !bean.pmoSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ScheduledSample.class)
        assert !bean.scheduledSample.sampleAnalyte.activeWorkflows
        assert checkAnalyte(bean, ClaritySampleAliquot.class)
        assert !bean.sampleAliquot.activeWorkflows
        assert checkAnalyte(bean, ClarityLibraryStock.class)
        assert !bean.libraryStock.activeWorkflows
        assert checkAnalyte(bean, ClarityPhysicalRunUnit.class)
        assert !bean.physicalRunUnit.activeWorkflows
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean.analytes.containsAll(analytes)
        when:
        bean.sampleName = sample.udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 1
    }

    void "test Illumina onboarding with pool scenarios" () {
        setup:
        OnboardingBean bean = getBeanForSample("TTCGTACC-GGTACGAA","Flowcell")
        adapter.allOnboardingBeansCached = [bean]
        when:
        List<String> errors = adapter.validateOnboardingData() //invalid runmodes and invalid index names
        then:
        assert errors.size() == 1
        when:
        List<PmoSample> samples = createSamplesInClarity(2) // valid data with a new sample
        String flowcellBarcode = """Flowcell${samples.collect{it.udfCollaboratorSampleName.replace('sample','')}.join('')}"""
        OnboardingBean bean1 = getBeanForSample("ATTGAGCC-GGCTCAAT", flowcellBarcode, samples[0])
        OnboardingBean bean2 = getBeanForSample("CCGTAAGA-TCTTACGG", flowcellBarcode, samples[1])
        adapter.allOnboardingBeansCached = [bean1, bean2]
        adapter.uniqueOnboardingBeansCached.clear()
        errors = adapter.validateOnboardingData()
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding sample
        List<Analyte> analytes = bean1.analytes
        then:
        assert bean1.libraryPool == bean2.libraryPool
        assert bean1.libraryStock != bean2.libraryStock
        [bean1, bean2].each { OnboardingBean b ->
            assert b.analytes.size() == 6
            assert b.libraryPool.udfExternal == "Y"
            assert checkAnalyte(b, PmoSample.class)
            assert !b.pmoSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ScheduledSample.class)
            assert !b.scheduledSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ClaritySampleAliquot.class)
            assert !b.sampleAliquot.activeWorkflows
            assert !b.libraryStock.activeWorkflows
            assert !b.libraryPool.activeWorkflows
            assert !b.physicalRunUnit.activeWorkflows
        }
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean1.analytes.containsAll(analytes)
        when:
        bean.sampleName = samples[0].udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 2
    }

    boolean checkAnalyte(HudsonAlphaBean bean, Class clazz) {
        for(Map.Entry entry : bean.udfMaps[clazz]) {
            ClarityUdf udf = entry.key
            Object value = entry.value
            if(bean.getAnalyteForType(clazz).getUdfAsString(udf.udf) != "$value")
                return false
        }
        return true
    }

    HudsonAlphaBean getBeanForSample(String indexSequence, String flowcellBarcode, PmoSample pmoSample = null) {
        if(!pmoSample)
            return new HudsonAlphaBean(sampleName: "sample234025", collaboratorLibraryName: "library234025", flowcellBarcode: flowcellBarcode,
                    indexSequence: indexSequence, libraryCreationSite: "JGI", laneNumber: 1, fastqFileName: "filename")
        HudsonAlphaBean bean = new HudsonAlphaBean(indexSequence: indexSequence, libraryCreationSite: "JGI", laneNumber: 1, fastqFileName: "filename")
        bean.sampleName = pmoSample.udfCollaboratorSampleName
        bean.collaboratorLibraryName = bean.sampleName.replace("sample", "library")
        bean.flowcellBarcode = flowcellBarcode
        return bean
    }

    List<HudsonAlphaBean> prepareData(List<PmoSample> pmoSamples){
        String fileName = "./src/integTest/resources/Onboarding.xls"
        List<HudsonAlphaBean> beans = process.getAllHudsonAlphaBeans(fileName)
        Map<Long, List<HudsonAlphaBean>> beansMap = beans.groupBy {it.pmoSampleId}
        int i = 0
        Date date = new Date()
        for(Map.Entry<Long, List<HudsonAlphaBean>> entry : beansMap) {
            PmoSample pmoSample = pmoSamples[i++]
            entry.value.each{ HudsonAlphaBean bean ->
                bean.pmoSampleId = pmoSample.pmoSampleId
                bean.sampleName = pmoSample.udfCollaboratorSampleName
                bean.collaboratorLibraryName += "${date.time}"
                bean.flowcellBarcode += "${date.time}"
            }
        }
        return beans
    }

    void "check reading hudson alpha file"(){
        setup:
        String fileName = './src/integTest/resources/ILL615-JGI.xls'
        expect:
        testUtil
        when:
        //webTransaction.performTransaction {
            ProcessHudsonAlphaSequencingFile actionHandler = new ProcessHudsonAlphaSequencingFile(process: process)
            List<HudsonAlphaBean> beans = actionHandler.process.getAllHudsonAlphaBeans(fileName)
            assert beans.size() != beans.unique().size()
        //}
        then:
        noExceptionThrown()
        println "PASSED"
    }

    void "check onboarding hudson alpha file with Library pools"(){
        setup:
        List<HudsonAlphaBean> beans = prepareLibraryPoolData()
        expect:
        testUtil
        when:
        //webTransaction.performTransaction {
            ProcessHudsonAlphaSequencingFile actionHandler = new ProcessHudsonAlphaSequencingFile(process: process)
            beans = actionHandler.mergeDataToClarity(beans)
//			ExcelWorkbook workbook = new ExcelWorkbook('resources/templates/excel/HudsonAlphaSequencingFile', true)
//			Section section = new TableSection(0,HudsonAlphaBean.class,'A2')
//			section.setData(beans)
//			workbook.addSection(section)
//			workbook.store(null, 'HudsonDuplicateTestDataFile.xls', false)
            beans.each{
                assert it.physicalRunUnit
                assert it.jgiLibraryStockName
                assert it.libraryStock
                Analyte analyte = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(it.jgiPruLimsId))
                assert analyte
                assert analyte.udfExternal == 'Y'
                logger.info "PRU - ${analyte.id}"
                analyte = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(it.jgiLibraryLimsId))
                assert analyte
                assert analyte.udfExternal == 'Y'
                assert analyte.udfCollaboratorLibraryName == it.collaboratorLibraryName
                logger.info "Library - ${analyte.id}"
                logger.info "Pmo sample Id - ${analyte.claritySample.pmoSample.pmoSampleId}"
            }
        //}
        then:
        noExceptionThrown()
        println "PASSED"
    }

    List<String> getLibraryNames(int count){
        List<LibraryNames> libraryNames = LibraryNames.createCriteria().list {
            and {
                isNull('geneusLimsId')
                isNull('libraryType')
                isNull('dateGranted')
            }
            maxResults(20)
            order("id","asc")
        }
        Date date = new Date()
        List<String> names = []
        (1..count).each{
            names << libraryNames[(date.seconds+it) % 20]?.libraryName
        }
        return names
    }

    List<HudsonAlphaBean> prepareLibraryPoolData(){
        List<HudsonAlphaBean> beans = []
        int i = 0
        List<String> indexes = ['GAATTCGT','GAATTCGT','Undetermined','Undetermined','AGCGATAG','AGCGATAG','Undetermined','Undetermined']
        List<String> libraryNames = getLibraryNames(sampleAnalytes.size())
        sampleAnalytes.eachWithIndex{ ArtifactNode artifactNode, int index ->
            SampleAnalyte sampleAnalyte = AnalyteFactory.analyteInstance(artifactNode)
            (1..4).each {
                HudsonAlphaBean bean = new HudsonAlphaBean()
                bean.jgiProjectId = sampleAnalyte.claritySample.sequencingProject.name as Long
                bean.pmoSampleId = sampleAnalyte.claritySample.name as Long
                bean.flowcellBarcode = "FlowcellBarcode ${bean.jgiProjectId}"
                bean.indexSequence = indexes[i++]
                bean.laneNumber = 1
                bean.collaboratorLibraryName=libraryNames[index]
            }
        }
        return beans
    }

    void "check duplicate onboarding hudson alpha file"(){
        setup:
        String fileName = './src/integTest/resources/HudsonDuplicateTestDataFile.xls'
        expect:
        testUtil
        when:
        //webTransaction.performTransaction {
            ProcessHudsonAlphaSequencingFile actionHandler = new ProcessHudsonAlphaSequencingFile(process: process)
            List<HudsonAlphaBean> originalBeans = actionHandler.process.getAllHudsonAlphaBeans(fileName)
            List<HudsonAlphaBean> beans = []
            beans.addAll(originalBeans)
            beans.each{
                it.physicalRunUnit = null
                it.jgiLibraryStockName = ''
                it.updateLibraryStock(null)
            }
            beans = actionHandler.mergeDataToClarity(beans)
            beans.eachWithIndex{ HudsonAlphaBean bean, int index ->
                HudsonAlphaBean originalBean = originalBeans[index]
                assert bean.jgiLibraryStockName == originalBean.jgiLibraryStockName
                assert bean.libraryStock == originalBean.libraryStock
                assert bean.physicalRunUnit == originalBean.physicalRunUnit
            }
        //}
        then:
        noExceptionThrown()
        println "PASSED"
    }

    void "check onboarding email notification"(){
        setup:
        String fileName = './src/integTest/resources/HudsonDuplicateTestDataFile.xls'
        expect:
        process
        when:
        //webTransaction.performTransaction {
            def actionHandler = new ProcessHudsonAlphaSequencingFile(process: process)
            List<HudsonAlphaBean> originalBeans = actionHandler.process.getAllHudsonAlphaBeans(fileName)
            originalBeans = actionHandler.mergeDataToClarity(originalBeans)
            actionHandler = new FinishStep(process: process)
            actionHandler.process.testMode = true
            List<Analyte> analytes = actionHandler.getAllAnalytes(originalBeans.unique())
            List<EmailEvent> emailEvents = actionHandler.sendEmailNotification(analytes)
            assert emailEvents.size() == 1
            println "${emailEvents.first().toString()}"
        //}
        then:
        noExceptionThrown()
        println "PASSED"
    }

    void "check logging"(){
        setup:
        String fileName = './src/integTest/resources/HudsonDuplicateTestDataFile.xls'
        expect:
        process
        when:
        //webTransaction.performTransaction {
            def actionHandler = new ProcessHudsonAlphaSequencingFile(process: process)
            List<HudsonAlphaBean> originalBeans = actionHandler.process.getAllHudsonAlphaBeans(fileName)
            originalBeans = actionHandler.mergeDataToClarity(originalBeans)
            originalBeans.unique()
            actionHandler.recordUpdates(originalBeans.collect{it.analytes}.flatten())
        //}
        then:
        noExceptionThrown()
        println "PASSED"
    }

    void "test logic"(){
        setup:
        List<PmoSample> pmoSamples = createSamplesInClarity(3)
        Date date = new Date()
        String prefix = "${date.dateTimeString.replaceAll('[^0-9]', '')}"
        List<OnboardingBean> beans = prepareTestBeans(pmoSamples, prefix)
        when:
        adapter.allOnboardingBeansCached = beans
        adapter.mergeDataToClarity()
        then:
        List<ContainerNode> flowcells = beans.collect{it.physicalRunUnit.containerNode}.unique()
        ContainerNode flowcellHWH3VCCXX = flowcells.find{it.name == "${prefix}HWH3VCCXX"}
        ContainerNode flowcellHYNGHCCXX = flowcells.find{it.name == "${prefix}HYNGHCCXX"}
        ContainerNode flowcellHYNK7CCXX = flowcells.find{it.name == "${prefix}HYNK7CCXX"}
        ContainerNode flowcellH325WALXX = flowcells.find{it.name == "${prefix}H325WALXX"}
        assert flowcellHWH3VCCXX
        assert flowcellHYNGHCCXX
        assert flowcellHYNK7CCXX
        assert flowcellH325WALXX
        Map<ContainerLocation, ArtifactNode> hwhContents = flowcellHWH3VCCXX.containerLocationArtifactNodes()
        assert hwhContents.size() == 3
        assert hwhContents.keySet().collect{it.row}.containsAll(['1','2','3'])
        assert hwhContents.collect{ AnalyteFactory.analyteInstance(it.value).ancestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION]).first().udfCollaboratorLibraryName}.containsAll(["${prefix}IERS".toString(), "${prefix}IERU".toString(), "${prefix}IERX".toString()])
        Map<ContainerLocation, ArtifactNode> hyngContents = flowcellHYNGHCCXX.containerLocationArtifactNodes()
        assert hyngContents.size() == 6
        assert hyngContents.keySet().collect{it.row}.containsAll(['1','2','3','4','5','6'])
        assert hyngContents.collect{ AnalyteFactory.analyteInstance(it.value).ancestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION]).first().udfCollaboratorLibraryName}.containsAll(["${prefix}IERT".toString(), "${prefix}IERW".toString()])
        Map<ContainerLocation, ArtifactNode> hynkContents = flowcellHYNK7CCXX.containerLocationArtifactNodes()
        assert hynkContents.size() == 3
        assert hynkContents.keySet().collect{it.row}.containsAll(['6','7','8'])
        assert hynkContents.collect{ AnalyteFactory.analyteInstance(it.value).ancestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION]).first().udfCollaboratorLibraryName}.containsAll(["${prefix}IERY".toString()])
        Map<ContainerLocation, ArtifactNode> h325Contents = flowcellH325WALXX.containerLocationArtifactNodes()
        assert h325Contents.size() == 2
        assert h325Contents.keySet().collect{it.row}.containsAll(['7','8'])
        assert h325Contents.collect{ AnalyteFactory.analyteInstance(it.value).ancestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION]).first().udfCollaboratorLibraryName}.containsAll(["${prefix}IERS".toString()])
    }

    List<OnboardingBean> prepareTestBeans(List<PmoSample> rootSamples, String prefix){
        List<OnboardingBean> beans = []
        List<List<String>> data = [['1', 'IERS', 'HWH3VCCXX', '1', 'ATCACG', 'HWH3VCCXX_s1_R1_illumina12index_5_SL158357.fastq.gz', 'JGI RnD'],
                                   ['2', 'IERU', 'HWH3VCCXX', '2', 'CGATGT', 'HWH3VCCXX_s7_R1_illumina12index_6_SL158358.fastq.gz', 'JGI RnD'],
                                   ['3', 'IERX', 'HWH3VCCXX', '3', 'TTAGGC', 'HWH3VCCXX_s13_R1_illumina12index_5_SL158359.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERT', 'HYNGHCCXX', '1', 'TGACCA', 'HYNGHCCXX_L004_R1_illumina12index_19_SL161162.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERT', 'HYNGHCCXX', '2', 'ACAGTG', 'HYNGHCCXX_L005_R1_illumina12index_19_SL161162.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERT', 'HYNGHCCXX', '3', 'GCCAAT', 'HYNGHCCXX_lane6_R1_illumina12index_19_SL161162.fastq.gz', 'JGI RnD'],
                                   ['2', 'IERW', 'HYNGHCCXX', '4', 'CAGATC', 'HYNGHCCXX_L010_R1_illumina12index_12_SL161163.fastq.gz', 'JGI RnD'],
                                   ['2', 'IERW', 'HYNGHCCXX', '5', 'ACTTGA', 'HYNGHCCXX_L011_R1_illumina12index_12_SL161163.fastq.gz', 'JGI RnD'],
                                   ['2', 'IERW', 'HYNGHCCXX', '6', 'GATCAG', 'HYNGHCCXX_L012_R1_illumina12index_12_SL161163.fastq.gz', 'JGI RnD'],
                                   ['3', 'IERY', 'HYNK7CCXX', '6', 'TAGCTT', 'HYNK7CCXX_L016_R1_illumina12index_19_SL161164.fastq.gz', 'JGI RnD'],
                                   ['3', 'IERY', 'HYNK7CCXX', '7', 'GGCTAC', 'HYNK7CCXX_L017_R1_illumina12index_19_SL161164.fastq.gz', 'JGI RnD'],
                                   ['3', 'IERY', 'HYNK7CCXX', '8', 'CTTGTA', 'HYNK7CCXX_L018_R1_illumina12index_19_SL161164.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERS', 'H325WALXX', '7', 'AGTCAA', 'H325WALXX_lane2_R1_illumina12index_5_SL158357.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERS', 'H325WALXX', '8', 'AGTTCC', 'H325WALXX_lane3_R1_illumina12index_5_SL158357.fastq.gz', 'JGI RnD']]


        data.each{List<String> fieldValues ->
            int sampleIndex = fieldValues[0] as int
            ClaritySample rootSample = rootSamples[sampleIndex - 1]
            rootSample.udfExternal = "Y"
            rootSample.sampleNode.httpPut()
            OnboardingBean bean = new HudsonAlphaBean()
            bean.pmoSample = rootSample.pmoSample
            bean.pmoSampleId = rootSample.pmoSample.pmoSampleId
            bean.sampleName = rootSample.udfCollaboratorSampleName
            bean.collaboratorLibraryName = "${prefix}${fieldValues[1]}"
            bean.flowcellBarcode = "${prefix}${fieldValues[2]}"
            bean.laneNumber = fieldValues[3]
            bean.indexSequence = fieldValues[4]
            bean.fastqFileName = fieldValues[5]
            bean.libraryCreationSite = fieldValues[6]
            bean.contactId = 17828
            beans << bean
        }
        return beans
    }

    List<OnboardingBean> preparePPS3715Beans(String prefix){
        List<OnboardingBean> beans = []
        List<List<String>> data = [['1', 'IERS', 'HWH3VCCXX', '1', 'ACAGTG', 'HWH3VCCXX_s1_R1_illumina12index_5_SL158357.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERS', 'HWH3VCCXX', '1', 'GCCAAT', 'HWH3VCCXX_s7_R1_illumina12index_6_SL158358.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERS', 'HWH3VCCXX', '1', 'ACAGTG', 'HWH3VCCXX_s13_R1_illumina12index_5_SL158359.fastq.gz', 'JGI RnD'],
                                   ['1', 'IERS', 'HYNGHCCXX', '1', 'GTGAAA', 'HYNGHCCXX_L004_R1_illumina12index_19_SL161162.fastq.gz', 'JGI RnD']]

        data.each{List<String> fieldValues ->
            int sampleIndex = fieldValues[0] as int
            ClaritySample rootSample = sampleAnalytes[sampleIndex-1].claritySample
            OnboardingBean bean = new HudsonAlphaBean()
            bean.sampleName = rootSample.udfCollaboratorSampleName
            bean.collaboratorLibraryName = "${prefix}${fieldValues[1]}"
            bean.flowcellBarcode = "${prefix}${fieldValues[2]}"
            bean.laneNumber = fieldValues[3]
            bean.indexSequence = fieldValues[4]
            bean.fastqFileName = fieldValues[5]
            bean.libraryCreationSite = fieldValues[6]
            beans << bean
        }
        return beans
    }

    void setSampleStatus(List<Long> sampleIds, SampleStatusCv status){
        Map map = [:]
        sampleIds.each{sampleId ->
            map[sampleId] = status
        }
        service.submitSampleStatus(map, 17828L)
    }

    void "post pooling process"(){
        setup:
        Stage stage = Stage.POOL_CREATION
        clarityNodeManager.nodeConfig.geneusUser = 'Migration'
        clarityNodeManager.nodeConfig.geneusPassword = 'Migration'
        List<Analyte> analytes = testUtil.getAnalytesInStage(stage,3)
        StepConfigurationNode stepConfigurationNode = StageUtility.getStepConfigurationNode(clarityNodeManager, stage)
        when:
        Map locationArtifactMap = [:]
        locationArtifactMap[1] = analytes.collect{it.artifactNode}
        ProcessParams processParams = new ProcessParams(stepConfigurationNode, locationArtifactMap)
        then:
        clarityNodeManager.executeClarityPoolingProcess(processParams)
    }
}
