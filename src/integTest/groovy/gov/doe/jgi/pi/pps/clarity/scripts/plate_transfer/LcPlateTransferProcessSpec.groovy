package gov.doe.jgi.pi.pps.clarity.scripts.plate_transfer

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class LcPlateTransferProcessSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    NodeConfig nodeConfig

    def setup() {
        nodeConfig = clarityNodeManager.nodeConfig
    }

    def cleanup() {
    }

    void "test SequencerModelCv"() {
        when:
        def cvs = SequencerModelCv.list()
        then:
        cvs.size()
        cvs.each{
            //println it.sequencerModel
            assert it.sequencerModel
        }
        [
                'MiSeq',
                'NovaSeq'
        ].each{ assert it in cvs*.sequencerModel}
    }
}
