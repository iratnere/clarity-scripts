package gov.doe.jgi.pi.pps.clarity.scripts.onboarding

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.PoolDetailsBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryPoolsAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.libraries.OnboardLibraries
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class OnboardLibraryPoolsIntegrationSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Value("\${sampleMaker.url}")
    private String sampleMakerUrl

    TestUtility testUtil
    @Shared
    ClarityProcess process
    PmoSample existingSample
    @Shared
    OnboardingLibraryPoolsAdapter adapter

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        Analyte sampleAnalyte = testUtil.getAnalytesInStage(Stage.APPROVE_FOR_SHIPPING,1).first()
        existingSample = sampleAnalyte.claritySample
        process = processFromClarity
        def srcFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardLibraries.USER_LIBRARY_WORSHEET)}
        adapter = new OnboardingLibraryPoolsAdapter(new ExcelWorkbook(srcFileNode?.id, process.testMode),17828)

    }

    List<PmoSample> createSamplesInClarity(int sampleCount) {
        List<String> sampleIds = testUtil.createSamplesInClarity(sampleMakerUrl, sampleCount, 1, ContainerTypes.TUBE, true, 17828)
        return clarityNodeManager.getSampleNodes(sampleIds).collect { SampleFactory.sampleInstance(it) }
    }

    ClarityProcess getProcessFromClarity() {
        String processId = testUtil.getProcessIds(ProcessType.ONBOARD_LIBRARY_FILES.value, 1).first()
        return ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
    }

    def cleanup() {
    }

    void "test Illumina onboarding scenarios" () {
        setup:
        OnboardingBean bean = getBeanForSample("Illumina NovaSeq 2 X 150", "2B02i7-2B02i5")
        adapter.onboardingBeansCached = [bean]
        when:
        List<String> errors = adapter.validateOnboardingData() //invalid runmodes and invalid index names
        then:
        assert errors.size() == 2
        when:
        List<PmoSample> samples = createSamplesInClarity(2) // valid data with a new sample
        OnboardingBean bean1 = getBeanForSample("Illumina NextSeq-HO 2 X 75", "2H02i7-2H02i5", samples[0])
        OnboardingBean bean2 = getBeanForSample("Illumina NextSeq-HO 2 X 75", "2C03i7-2C03i5", samples[1])
        adapter.onboardingBeansCached = [bean1, bean2]
        adapter.poolDetailsBeansCached = getPoolBeanForBeans(adapter.onboardingBeans)
        errors = adapter.validateOnboardingData()
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding sample
        List<Analyte> analytes = bean1.analytes
        then:
        assert bean1.libraryPool == bean2.libraryPool
        assert bean1.libraryStock != bean2.libraryStock
        [bean1, bean2].each { OnboardingBean b ->
            assert b.analytes.size() == 5
            assert b.analytes.size() == 5
            assert b.libraryPool.udfExternal != "Y"
            assert checkAnalyte(b, PmoSample.class)
            assert !b.pmoSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ScheduledSample.class)
            assert !b.scheduledSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ClaritySampleAliquot.class)
            assert !b.sampleAliquot.activeWorkflows
            assert !b.libraryStock.activeWorkflows
            assert b.libraryPool.artifactNode.activeStages.size() >= 1
        }
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean1.analytes.containsAll(analytes)
        when:
        bean.sampleName = samples[0].udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 2
    }

    void "test Pacbio onboarding scenarios" () {
        when:
        List<PmoSample> samples = createSamplesInClarity(2) // valid data with a new sample
        OnboardingBean bean1 = getBeanForSample("PacBio Sequel 1 X 600", "bc1001_BAK8A_OA--bc1001_BAK8A_OA", samples[0])
        OnboardingBean bean2 = getBeanForSample("PacBio Sequel 1 X 600", "bc1015_BAK8B_OA--bc1015_BAK8B_OA", samples[1])
        adapter.onboardingBeansCached = [bean1, bean2]
        adapter.poolDetailsBeansCached = getPoolBeanForBeans(adapter.onboardingBeans)
        List<String> errors = adapter.validateOnboardingData()
        then:
        assert !errors
        when:
        adapter.mergeDataToClarity() //onboarding sample
        List<Analyte> analytes = bean1.analytes
        then:
        assert bean1.libraryPool == bean2.libraryPool
        assert bean1.libraryStock != bean2.libraryStock
        [bean1, bean2].each { OnboardingBean b ->
            assert b.analytes.size() == 5
            assert b.analytes.size() == 5
            assert b.libraryPool.udfExternal != "Y"
            assert checkAnalyte(b, PmoSample.class)
            assert !b.pmoSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ScheduledSample.class)
            assert !b.scheduledSample.sampleAnalyte.activeWorkflows
            assert checkAnalyte(b, ClaritySampleAliquot.class)
            assert !b.sampleAliquot.activeWorkflows
            assert !b.libraryStock.activeWorkflows
            assert b.libraryPool.artifactNode.activeStages.size() >= 1
        }
        when:
        adapter.mergeDataToClarity() // reonboarding same data again
        then:
        assert bean1.analytes.containsAll(analytes)
        when:
        bean1.sampleName = samples[0].udfCollaboratorSampleName //reonboarding same library again, sample-library mismatch scenario
        errors = adapter.validateOnboardingData()
        then:
        assert errors.size() == 2
    }

    boolean checkAnalyte(OnboardingBean bean, Class clazz) {
        for(Map.Entry entry : bean.udfMaps[clazz]) {
            ClarityUdf udf = entry.key
            Object value = entry.value
            if(bean.getAnalyteForType(clazz).getUdfAsString(udf.udf) != "$value")
                return false
        }
        return true
    }

    OnboardingBean getBeanForSample(String runMode, String indexName, PmoSample pmoSample = null) {
        if(!pmoSample)
            return new OnboardingBean(sampleName: "sample234025", sowRunMode: runMode,
                    dop: 1, libraryProtocol: "My protocol", labLibraryName: "library234025", aliquotVolume: 10, aliquotAmount: 12, pcrCycles: 2,
                    indexName: indexName, libFragmentSize: 3456, libVolume: 32, libMolarityPm: 1234, libConcentration: 1, libraryCreationSite: "JGI")
        OnboardingBean bean = new OnboardingBean(dop: 1, libraryProtocol: "My protocol", aliquotVolume: 10, aliquotAmount: 12, pcrCycles: 2,
                indexName: indexName, libFragmentSize: 3456, libVolume: 32, libMolarityPm: 1234, libConcentration: 1, libraryCreationSite: "JGI", sowRunMode: runMode)
        bean.sampleName = pmoSample.udfCollaboratorSampleName
        bean.labLibraryName = bean.sampleName.replace("sample", "library")
        bean.poolNumber = 1
        return bean
    }

    List<PoolDetailsBean> getPoolBeanForBeans(List<OnboardingBean> onboardingBeans) {
        List poolNumbers = onboardingBeans.collect { it.poolNumber}.unique()
        String poolName = onboardingBeans.collect { it.sampleName.replace("sample","") }.join("")
        List<PoolDetailsBean> poolBeans = []
        poolNumbers.each { poolNumber ->
            poolBeans << new PoolDetailsBean(poolNumber: poolNumber, poolFragmentSize: 1000, poolVolume: 200, poolMolarity: 23.34, poolConcentration: 1.23, collaboratorPoolName: "Pool#$poolName")
        }
        return poolBeans
    }
}
