package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcUtility
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import grails.gorm.transactions.Transactional
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcUtilityIntegrationSpec extends Specification {
    Random random
    @Autowired
    ScheduledSampleService scheduledSampleService
    @Autowired
    NodeManager nodeManager
    SampleQcUtility sampleQcUtility
    Long pmoSampleId = 123343

    def setup() {
        random = new Random()
        Analyte analyte = Mock(SampleAnalyte)
        ClaritySample sample = Mock(PmoSample)
        sample.pmoSampleId >> pmoSampleId
        analyte.claritySample >> sample
        sampleQcUtility = new SampleQcUtility([analyte], Mock(ScheduledSampleService))
        sampleQcUtility.sampleSowItems = [:]
    }

    def cleanup() {
    }

    void "test is SIP sample"(String status, String sowType, boolean expected) {
        setup:
        JSONObject jsonObject = new JSONObject()
        jsonObject.'current-status' = status
        jsonObject.'sow-item-type' = sowType
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([jsonObject])
        when:
        boolean result = sampleQcUtility.isSipSample(pmoSampleId)
        then:
        assert result == expected
        where:
        status          | sowType                       | expected
        'Created'       | "SIP Metagenome Replicate"    | true
        'Created'       | "SIP Metagenome Fraction"     | true
        'Created'       | "Fragment"                    | false
    }

    void "test is SIP Original Sample"(String status, String sowType, boolean expected) {
        setup:
        sampleQcUtility.sampleSowItems = [:]
        JSONObject jsonObject = new JSONObject()
        jsonObject.'current-status' = status
        jsonObject.'sow-item-type' = sowType
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([jsonObject])
        when:
        boolean result = sampleQcUtility.isSipOriginalSample(pmoSampleId)
        then:
        assert result == expected
        where:
        status          | sowType                       | expected
        'Created'       | "SIP Source Sample"           | true
        'Created'       | "SIP Metagenome Fraction"     | false
        'Created'       | "Fragment"                    | false
    }

    void "test check Group Contents"() {
        setup:
        List<Analyte> analytes = []
        List<String> sampleLimsIds = []
        (0..4).each{
            Analyte analyte = Mock(SampleAnalyte)
            ClaritySample claritySample = Mock(PmoSample)
            analyte.claritySample >> claritySample
            analytes << analyte
            String sampleId = "Lims Id ${it+1}"
            claritySample.id >> sampleId
            sampleLimsIds << sampleId
        }
        when:
        SampleQcUtility sampleQcUtility = new SampleQcUtility(analytes, Mock(ScheduledSampleService))
        String error = sampleQcUtility.checkIfGroupComplete("1", sampleLimsIds)
        then:
        assert !error
        when:
        sampleLimsIds << "another lims id"
        error = sampleQcUtility.checkIfGroupComplete("1", sampleLimsIds)
        then:
        assert error == "All samples of a SIP group 1 should be QC'ed together. Please add samples [another lims id] to the icebucket and restart the process."
    }

    void "test getSampleQcTypeIds"(){
        setup:
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-id' = 123
        j1.'qc-type-id' = 1
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-id' = 234
        j2.'qc-type-id' = 4
        JSONObject j3 = new JSONObject()
        j3.'current-status' = 'Deleted'
        j3.'sow-item-id' = 345
        j3.'qc-type-id' = 8
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([j1, j2, j3])
        when:
        def qcTypeIds = sampleQcUtility.getSampleQcTypeIds(pmoSampleId)
        then:
        assert qcTypeIds.values() as List == [1,4]
    }

    void "test getSMInstructions"(){
        setup:
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sm-instructions' = "Instructions 1"
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sm-instructions' = "Instructions 2"
        JSONObject j3 = new JSONObject()
        j3.'current-status' = 'Deleted'
        j3.'sm-instructions' = "Instructions 3"
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([j1, j2, j3])
        when:
        def smInstructions = sampleQcUtility.getSMInstructions(pmoSampleId)
        then:
        assert smInstructions == "[Instructions 1][Instructions 2][Instructions 3]"
    }

    void "test getSowItemTypes"(){
        setup:
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-type' = "Fragment"
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sow-item-type' = "Sample Receipt FasTrack"
        JSONObject j3 = new JSONObject()
        j3.'current-status' = 'Deleted'
        j3.'sow-item-type' = "Fragment - Internal Selection and Amplification"
        JSONArray jsonArray = JSONArray.fromObject([j1, j2, j3])
        sampleQcUtility.sampleSowItems[pmoSampleId] = jsonArray
        when:
        def sowItemTypes = sampleQcUtility.getSowItemTypes(pmoSampleId)
        then:
        assert sowItemTypes.containsAll(["Fragment", "Sample Receipt FasTrack"])
        assert !sowItemTypes.contains("Fragment - Internal Selection and Amplification")
    }

    void "test getSowItemsStatus"(){
        setup:
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sow-item-id' = 111111
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Available For Use'
        j2.'sow-item-id' = 222222
        JSONObject j3 = new JSONObject()
        j3.'current-status' = 'Deleted'
        j3.'sow-item-id' = 333333
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([j1, j2, j3])
        when:
        def sowStatus = sampleQcUtility.getSowItemsStatus(pmoSampleId)
        then:
        assert sowStatus[111111] == 'Created'
        assert sowStatus[222222] == 'Available For Use'
        assert sowStatus[333333] == 'Deleted'
    }

    void "test getIsITag"(){
        setup:
        JSONObject j1 = new JSONObject()
        j1.'current-status' = 'Created'
        j1.'sm-instructions' = "Instructions 1"
        j1.'itag-primer-set' = "vdsgfdh"
        JSONObject j2 = new JSONObject()
        j2.'current-status' = 'Created'
        j2.'sm-instructions' = "Instructions 2"
        when:
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([j1])
        boolean isITag = sampleQcUtility.getIsITag(pmoSampleId)
        then:
        assert isITag == true
        when:
        sampleQcUtility.sampleSowItems[pmoSampleId] = JSONArray.fromObject([j2])
        isITag = sampleQcUtility.getIsITag(pmoSampleId)
        then:
        assert isITag == false
    }

    void "test Update Sample Maps"(){
        setup:
        Long pmoSampleId = 242353
        PmoSample pmoSample = Mock(PmoSample)
        pmoSample.pmoSampleId >> pmoSampleId
        SampleQcAdapter adapter = new GenericSampleQC(sampleQcUtility, sampleQcUtility.analytes)
        when:
        adapter.updateStatusMaps(pmoSample, Analyte.PASS)
        then:
        assert adapter.sampleQcCompleteMap[pmoSampleId] == SampleStatusCv.SAMPLE_QC_COMPLETE
        assert adapter.sampleStatusMap[pmoSampleId] == SampleStatusCv.AVAILABLE_FOR_USE
        when:
        adapter.sampleQcCompleteMap.clear()
        adapter.sampleStatusMap.clear()
        adapter.updateStatusMaps(pmoSample, Analyte.FAIL)
        then:
        assert adapter.sampleQcCompleteMap[pmoSampleId] == SampleStatusCv.SAMPLE_QC_COMPLETE
        assert adapter.sampleStatusMap[pmoSampleId] == SampleStatusCv.ABANDONED
    }
}
