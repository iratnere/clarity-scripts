package gov.doe.jgi.pi.pps.clarity.scripts.librarycreation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.ProcessRegistrationService
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

import java.lang.reflect.Constructor

@SpringBootTest(classes = [Application.class])
@Transactional
class LibraryCreationRegistrationIntegrationSpec {

    @Autowired
    ProcessRegistrationService processRegistrationService

    def "test ProcessType.LC_LIBRARY_CREATION registration"() {
        setup:
        ProcessType processType = ProcessType.LC_LIBRARY_CREATION

        expect:"fix me"
        ProcessType.toEnum(processType.value) == processType

        when:
        Constructor constructor = processRegistrationService.processConstructor(processType)

        then:
        constructor
        constructor.declaringClass == LibraryCreationProcess
    }
}
