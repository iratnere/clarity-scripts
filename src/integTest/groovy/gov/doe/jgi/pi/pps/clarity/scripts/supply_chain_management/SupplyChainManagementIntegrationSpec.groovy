package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SupplyChainManagementIntegrationSpec extends Specification {
    TestUtility testUtil
    @Autowired
    NodeManager clarityNodeManager
    String processId

    def setup() {
        testUtil = new TestUtility(clarityNodeManager)
        processId = testUtil.getProcessIds(ProcessType.SM_APPROVE_FOR_SHIPPING.value).first()
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "execute process actions"(String action){
        setup:
        processId = '24-770920'
        expect:
        testUtil
        when:
        testUtil.executeAction(ProcessType.SM_APPROVE_FOR_SHIPPING, action, processId)
        then:
        noExceptionThrown()
        where:
        action << [//'ApproveForShippingPPV',
                   //'ApproveForShipping',
                   'RouteToNextStep']
    }
    @Ignore//Rest
    void "test actionHandler"(String action) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-770920')
        SmApproveForShipping clarityProcess = new SmApproveForShipping(processNode)
        RouteToSampleReceipt actionHandler = clarityProcess.getActionHandler(action) as RouteToSampleReceipt
        when:
        actionHandler.execute()
        then:
        noExceptionThrown()
        where:
        action << ['RouteToNextStep']
    }

}
