package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.SIPUtility
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.RecordDetailsInfo
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcUtility
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel.GLSStartletUseTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel.SMWorkspaceSampleQCPassFailKeyValueBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcRouteToWorkflowIntegrationSpec extends Specification {
    @Autowired
    NodeManager nodeManager
    TestUtility testUtility
    ContainerNode plate
    SampleQcProcess process
    List<ContainerNode> tubes
    String processId

    def setup() {
        testUtility = new TestUtility(nodeManager)
        plate = testUtility.getContainers(ContainerTypes.WELL_PLATE_96, 1).first()
        plate = SampleFactory.sampleInstance(plate.contentsArtifactNodes[0].sampleNode).pmoSample.sampleAnalyte.containerNode
        processId = testUtility.getProcessIds(ProcessType.SM_SAMPLE_QC.value,1,"Lakshmi","Vishwas").first()
        process = new SampleQcProcess(nodeManager.getProcessNode(processId))
        process.processNode.stepsNode.nodeManager = nodeManager
        tubes = testUtility.getContainers(ContainerTypes.TUBE, 5).collect{SampleFactory.sampleInstance(it.contentsArtifactNodes[0].sampleNode).pmoSample.sampleAnalyte.containerNode}
    }

    def cleanup() {
    }

    void "make record details info"() {
        expect:
        process
        when:
        RecordDetailsInfo recordDetailsInfo = new RecordDetailsInfo(process)
        then:
        assert recordDetailsInfo.glsStarletUseSection
        assert recordDetailsInfo.researcherContactId
//        assert recordDetailsInfo.replicatesFailurePercentAllowed
//        assert recordDetailsInfo.controlsFailurePercentAllowed
//        assert !recordDetailsInfo.minimumIsotopeMass
    }

    void "test transfer data to Udfs"() {
        setup:
        TableSection tSection = new TableSection(0,GLSStartletUseTableBean,'A1')
        KeyValueSection kvSection = new KeyValueSection(1,  SMWorkspaceSampleQCPassFailKeyValueBean, 'C2')
        GenericSampleQC genericAdapter = new GenericSampleQC(new SampleQcUtility(process.inputAnalytes), process.inputAnalytes)
        expect:
        process
        when:
        GLSStartletUseTableBean bean = prepareGlsStarletBean()
        tSection.beanObjects = [bean]
        kvSection.data = prepareKVBean()
        genericAdapter.transferDataFromTableToUdfs(tSection)
        genericAdapter.transferDataFromKeyValueSection([kvSection])
        ContainerNode containerNode = bean.sample.sampleAnalyte.containerNode
        PmoSample sample = bean.sample
        then:
        assert sample.udfSampleQCResult == bean.qcStatus.value
        assert sample.udfInitialVolumeUl == bean.initialVolume
        assert sample.udfConcentration == bean.concentration
        assert sample.udfVolumeUl == bean.availableVolume
        assert sample.udfSampleHMWgDNAYN == bean.hMWgDNAYN.value
        assert !sample.udfSampleQCFailureMode
        assert containerNode.getUdfAsString(ClarityUdf.CONTAINER_SAMPLE_QC_RESULT.udf) == 'Pass'
    }

    void "test get Group Sample Qc Result"() {
        setup:
        List<Analyte> analytes = []
        List<Analyte> controlSamples = []
        int ci=0,cj=0,ck=0,cl=0
        tubes.eachWithIndex { ContainerNode cn, int i ->
            Analyte analyte = AnalyteFactory.analyteInstance(cn.contentsArtifactNodes.first())
            analytes << analyte
            if(i%3 == 0) {
                analyte.claritySample.pmoSample.udfIsotopeLabel = "Unlabeled"
                controlSamples << analyte
                cj++
                if(analyte.claritySample.pmoSample.udfSampleQCResult == Analyte.FAIL)
                    ci++
            }
            else{
                ck++
                if(analyte.claritySample.pmoSample.udfSampleQCResult == Analyte.FAIL)
                    cl++
            }
        }
        RecordDetailsInfo recordDetailsInfo = new RecordDetailsInfo(process)
        recordDetailsInfo.replicatesFailurePercentAllowed = 50
        boolean expectedSampleResult = (cl/ck*100) > 50 ? false : true
        recordDetailsInfo.controlsFailurePercentAllowed = 50
        expectedSampleResult = (expectedSampleResult && ((ci/cj*100) > 50 ? false : true))
        when:
        SampleQcUtility sampleQcUtility = new SampleQcUtility(analytes, Mock(ScheduledSampleService))
        String result = SIPUtility.updateGroupQcResult(analytes.collect{it.claritySample.pmoSample}, recordDetailsInfo.replicatesFailurePercentAllowed, recordDetailsInfo.controlsFailurePercentAllowed)
        then:
        assert result == (expectedSampleResult?Analyte.PASS:Analyte.FAIL)
    }

    GLSStartletUseTableBean prepareGlsStarletBean(){
        GLSStartletUseTableBean bean = new GLSStartletUseTableBean()
        bean.reqdQCType = new DropDownList(value: SampleQcProcess.activeQcTypes[3])
        bean.initialVolume = 1000
        bean.volForQuantity = 10
        bean.volForQuality = 10
        bean.volForPurity = 10
        bean.concentration = 1.32
        bean.gelDiluentVol = 12
        bean.a260a230 = 0.53
        bean.a260a280 = 0.43
        bean.hMWgDNAYN = new DropDownList(value:'Y')
        bean.qualityScore = 1
        bean.rRNARatio = 0.35
        bean.availableVolume = 270
        bean.availableMass = 30
        bean.qcStatus = new DropDownList(value: 'Pass')
        bean.failureMode = new DropDownList(value: 'testFailureMode')
        bean.sample = process.inputAnalytes[0].claritySample
        return bean
    }

    SMWorkspaceSampleQCPassFailKeyValueBean prepareKVBean(){
        SMWorkspaceSampleQCPassFailKeyValueBean bean = new SMWorkspaceSampleQCPassFailKeyValueBean()
        bean.plateQCResult = 'Pass'
        bean.firstSampleOnPlate = process.inputAnalytes[0]
        return bean
    }
}