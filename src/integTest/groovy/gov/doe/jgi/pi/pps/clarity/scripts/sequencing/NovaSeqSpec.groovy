package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.NextSeq
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.NovaSeq
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.Sequencing
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class NovaSeqSpec extends Specification {

    @Autowired
    NodeManager clarityNodeManager
    def novaSeqProcessId = '24-656424' //all environments
    Sequencing clarityProcess

    def setup() {
        ProcessNode processNode = clarityNodeManager.getProcessNode(novaSeqProcessId)
        clarityProcess = new Sequencing(processNode)
        assert clarityProcess.inputAnalytes.each { assert it instanceof ClarityLibraryPool }
    }

    def cleanup() {
    }
    /* 2-2864949 - Illumina NovaSeq S4 2 X 150
Illumina NovaSeq SP 2 X 150
Illumina NovaSeq SP 2 X 250
Illumina NovaSeq S2 2 X 150
Illumina NovaSeq S4 2 X 150
     */
    static RunModeCv getRunMode(ContainerTypes containerTypes) {
        String sequencerModel = "$SequencerModelCv.NOVASEQ $containerTypes.value"
        SequencerModelCv sequencerModelCv = SequencerModelCv.findBySequencerModel(sequencerModel)
        RunModeCv runMode = RunModeCv.findBySequencerModel(sequencerModelCv)
        assert runMode.runMode.contains(containerTypes.value)
        return runMode
    }

    void "test validateOutputContainerType ClarityLibraryPool"() {
        setup:
        NovaSeq novaSeq = new NovaSeq(clarityProcess)
        String runModeSp = getRunMode(ContainerTypes.FLOW_CELL_SP).runMode
        String runModeS4 = getRunMode(ContainerTypes.FLOW_CELL_S4).runMode
        when:
        clarityProcess.inputAnalytes*.setUdfRunMode(runModeSp)
        novaSeq.validateOutputContainerType()
        then:
        clarityProcess.sequencerModelCv.flowcellType == ContainerTypes.FLOW_CELL_SP.value
        def e = thrown(WebException)
        e.message.contains('Process output container type')
        e.message.contains('does not match the inputs flowcell type from sequencer model')
        when:
        clarityProcess.sequencerModelCvCached = null
        clarityProcess.inputAnalytes*.setUdfRunMode(runModeS4)
        novaSeq.validateOutputContainerType()
        then:
        clarityProcess.sequencerModelCv.flowcellType == ContainerTypes.FLOW_CELL_S4.value
        noExceptionThrown()
    }

    void "test validateInputFlowcellType ClarityLibraryPool"() {
        setup:
        String runModeSp = getRunMode(ContainerTypes.FLOW_CELL_SP).runMode
        NovaSeq novaSeq = new NovaSeq(clarityProcess)
        clarityProcess.inputAnalytes.each{ it.udfRunMode = 'Illumina NovaSeq 2 X 150'}
        when:
        novaSeq.validateInputFlowcellType()
        then:
        clarityProcess.sequencerModelCv.flowcellType == ''
        def e = thrown(WebException)
        e.message.contains('The flowcell type is not set for selected inputs.')
        e.message.contains('Inputs: [CNPPA, CNSYC, CNSZO, CNTCG]')
        e.message.contains('Sequencer Model: NovaSeq')
        when:
        clarityProcess.sequencerModelCvCached = null
        clarityProcess.inputAnalytes*.setUdfRunMode(runModeSp)
        novaSeq.validateInputFlowcellType()
        then:
        clarityProcess.sequencerModelCv.flowcellType == ContainerTypes.FLOW_CELL_SP.value
        noExceptionThrown()
    }

    void "test validateInputIsLibrary ClarityLibraryPool"() {
        setup:
        def artifactId = '2-3061664'
        Sequencing clarityProcess = getSequencingProcess(artifactId)
        NovaSeq novaSeq = new NovaSeq(clarityProcess)
        expect:
        clarityProcess.inputAnalytes.each{ assert it instanceof ClarityLibraryPool }
        when:
        novaSeq.validateInputIsLibrary()
        then:
        noExceptionThrown()
    }

    void "test validateInputIsLibrary ClarityLibraryStock"() {
        setup:
        def artifactId = '2-3126592'
        Sequencing clarityProcess = getSequencingProcess(artifactId)
        expect:
        assert clarityProcess.inputAnalytes.find{ it instanceof ClarityLibraryStock }
        when:
        NextSeq nextSeq = new NextSeq(clarityProcess)
        nextSeq.validateInputIsLibrary()
        then:
        noExceptionThrown()
        when:
        NovaSeq novaSeq = new NovaSeq(clarityProcess)
        novaSeq.validateInputIsLibrary()
        then:
        noExceptionThrown()
    }

    Sequencing getSequencingProcess(def artifactId) {
        ProcessNode processNode = clarityNodeManager.getArtifactNode(artifactId).parentProcessNode
        new Sequencing(processNode)
    }
}