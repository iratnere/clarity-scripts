package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import grails.gorm.transactions.Transactional
import org.apache.poi.ss.util.CellReference
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleQcWorksheetIntegrationTestSpec extends Specification {
    TestUtility testUtility
    @Autowired
    NodeManager nodeManager
    SampleQcProcess process
    List<Analyte> inputAnalytes = []
    List<ContainerNode> plates = []
    List<ContainerNode> outputPlates

    def setup() {
        testUtility = new TestUtility(nodeManager)
        prepareDataToTest()
    }

    def cleanup() {
    }

    void prepareDataToTest(){
        String processId = testUtility.getProcessIds(ProcessType.SM_SAMPLE_QC.value,1,"Lakshmi","Vishwas").first()
        process = ProcessFactory.processInstance(nodeManager.getProcessNode(processId))
        process.testMode = true
        List<ContainerNode> containerNodes = testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.TUBE, 10)
        plates = testUtility.getContainerInStage(Stage.SAMPLE_QC_DNA, ContainerTypes.WELL_PLATE_96, 2)
        containerNodes.addAll(plates)
        containerNodes.each{ ContainerNode containerNode ->
            inputAnalytes = containerNode.contentsArtifactNodes.collect{ ArtifactNode an -> AnalyteFactory.analyteInstance(an) }
        }
        outputPlates = testUtility.getEmptyContainers(ContainerTypes.WELL_PLATE_96, 2)
    }

    void "test create and populate table section"(){
        setup:
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        worksheet.sampleQcWorkbook = new ExcelWorkbook(process.SAMPLE_QC_WORKSHEET_TEMPLATE, process.testMode)
        worksheet.init()
        expect:
        process
        inputAnalytes
        when:
        TableSection actual = worksheet.createAndPopulateTableSection()
        TableSection expected = process.getSampleQcWorksheetSections()[0]
        assert actual.data.size() == expected.data.size()
        then:
        noExceptionThrown()
        when:
        worksheet.sortedInputs = process.getSortedInputAnalytes(inputAnalytes)
        actual = worksheet.createAndPopulateTableSection()
        String actualOrder = actual.data.collect{it.pmoSampleId}.join(',')
        String expectedOrder = inputAnalytes.collect{it.claritySample.pmoSample.pmoSampleId}.join(',')
        then:
        assert actualOrder==expectedOrder
    }

    void "test cell address with offset"() {
        setup:
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        worksheet.sortedInputs = process.getSortedInputAnalytes(inputAnalytes)
        when:
        TableSection tableSection = worksheet.createAndPopulateTableSection()
        worksheet.sampleQcWorkbook = new ExcelWorkbook(process.SAMPLE_QC_WORKSHEET_TEMPLATE, process.testMode)
        worksheet.sampleQcWorkbook.addSection(tableSection)
        CellReference cellReference = new CellReference('A4')
        worksheet.sortedInputs.eachWithIndex{ Analyte analyte, int index ->
            String cellAddress = worksheet.getCellAddressWithOffset(tableSection, 'initialVolume', index, 0)
            CellReference expected = new CellReference(cellReference.row+index, cellReference.col)
            CellReference actual = new CellReference(cellAddress)
            assert expected.row == actual.row
        }
        then:
        noExceptionThrown()
    }

    void "test add formula to plate section"() {
        setup:
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        worksheet.init()
        TableSection tableSection = worksheet.createAndPopulateTableSection()
        worksheet.sampleQcWorkbook = new ExcelWorkbook(process.SAMPLE_QC_WORKSHEET_TEMPLATE, process.testMode)
        worksheet.sampleQcWorkbook.addSection(tableSection)
        SampleQcWorksheet.SMWorkspaceModel model = SampleQcWorksheet.SMWorkspaceModel.newInstance(worksheet, 1)
        PlateSection plateSection = model.initVolModelSection
        when:
        CellReference cellReference = new CellReference('Q4')
        worksheet.sortedInputs.eachWithIndex { Analyte analyte, int index ->
            String plateLocation = process.getOutputAnalytes(analyte)[0].containerLocation.wellLocation
            worksheet.addFormulaToPlateSection(tableSection, plateSection, 'initialVolume', index)
            CellReference actual = new CellReference(cellReference.row+index+1, cellReference.col)
            assert plateSection.data[plateLocation] == "'GLS & Starlet Use'!${actual.formatAsString()}"
        }
        then:
        noExceptionThrown()
    }

    void "test populate dynamic plate section"() {
        setup:
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        worksheet.sortedInputs = process.getSortedInputAnalytes(inputAnalytes.findAll{it.isOnPlate})
        TableSection tableSection = worksheet.createAndPopulateTableSection()
        worksheet.sampleQcWorkbook = new ExcelWorkbook(process.SAMPLE_QC_WORKSHEET_TEMPLATE, process.testMode)
        worksheet.sampleQcWorkbook.addSection(tableSection)
        String sheetName = SampleQcWorksheet.GlsStarletUse.GLS_STARLET_SHEET_NAME
        when:
        PlateSection plateSection = new PlateSection(1, CellTypeEnum.FORMULA, 'A47')
        worksheet.populateDynamicPlateSection(plateSection, worksheet.sortedInputs, sheetName, tableSection)
        then:
        worksheet.sortedInputs.eachWithIndex{ Analyte analyte, int index ->
            String plateLocation = analyte.containerLocation.wellLocation
            CellReference cellReference = new CellReference(tableSection.startCell.row+index+1, tableSection.startCell.col+29)
            assert plateSection.data[plateLocation].contains("'$sheetName'!${cellReference.formatAsString()}")
        }
    }

    void "test prepare sample qc worksheet"() {
        setup:
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        when:
        ExcelWorkbook actual = worksheet.makeSampleQcWorksheet()
        ExcelWorkbook expectedAll = worksheet.loadSampleQcWorksheet()
        then:
        assert actual.sections.size() == expectedAll.sections.size()
        actual.sections.values().eachWithIndex{ Section asection, int index ->
            Section esection = expectedAll.sections.values()[index]
            if(!(asection instanceof KeyValueSection)){
                assert esection.data.size() == asection.data.size()
            }
        }

    }
}
