package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteSorter
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification.SampleAliquotEmailNotification
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.queues.QueueArtifact
import grails.gorm.transactions.Transactional
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.*

@SpringBootTest(classes = [Application.class])
@Transactional
@Stepwise
class AcRouteToWorkflowIntegrationSpec extends Specification {
    static final int NUMBER_SAMPLES = 1

    @Autowired
    NodeManager clarityNodeManager

    @Shared
    List libraryCreationQueueNames
    @Shared
    ArtifactNode inputArtifactNode

    def setup() {
    }

    def cleanup() {
    }

    void "test get LibraryCreationQueueCv"(){
        when:
            libraryCreationQueueNames = LibraryCreationQueueCv.findAllWhere(active: 'Y')?.collect{it.libraryCreationQueue}
        then:
            libraryCreationQueueNames?.size() > 1
    }

    @Ignore
    void "test PPS-4712"() {
        setup:
        String processId = '24-1017071'
        when:
        ClarityProcess cp = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId))
        AcRouteToWorkflow handler = new AcRouteToWorkflow(process: cp)
        List<EmailEvent> emailEvents = handler.sendEmailNotification(cp.outputAnalytes)
        String json = emailEvents[0].marshallJson()
        then:
        assert json.contains("SM Notes scheduled sample analyte “Notes” udf")
    }

    void "test move analyte to LC queues"(String queueName) {
        setup:
            //String artifactId = '2-564949'
            ArtifactNode artifactNode = findArtifactNode()//clarityNodeManager.getArtifactNode(artifactId)
        WorkflowNode workflowNode = getLibraryCreationWorkflowStageUri(queueName)
            def uri = workflowNode.stages?.first()?.uri
            List<Routing> routings = []
             routings << new Routing(Routing.Action.assign,uri,artifactNode.url)
        expect:
            artifactNode.activeStages == []
        when:
        Routing.route(clarityNodeManager.nodeConfig,routings)
        then:
            assert artifactNode.activeStages : "queueName  $queueName"
            artifactNode.activeStages[0]
            artifactNode.activeStages[0].uri == uri
            noExceptionThrown()
        when:
            new Routing(Routing.Action.unassign, workflowNode.url, artifactNode.url).route(clarityNodeManager.nodeConfig)
        then:
            artifactNode.activeStages == []
            noExceptionThrown()
        where:
            queueName << libraryCreationQueueNames
    }

    ArtifactNode findArtifactNode() {
        if (inputArtifactNode)
            return inputArtifactNode
        def workflowName = ClarityWorkflow.SUPPLY_CHAIN_MANAGEMENT.value
        def stepId = findWorkflowStageId(workflowName)
        assert stepId
        int batchSize = NUMBER_SAMPLES
        QueueNode queueNode = clarityNodeManager.getQueueNode(stepId)
        assert queueNode
        List<QueueArtifact> queueArtifacts = queueNode.queueArtifacts.sort(false){it.queueTimeString}.reverse()?.take(batchSize)
        ArtifactNode artifactNode = queueArtifacts[0].artifactNode
        new Routing(Routing.Action.unassign, artifactNode.activeStages[0].uri, artifactNode.url).route(clarityNodeManager.nodeConfig)
        inputArtifactNode = artifactNode
        artifactNode
    }

    def findWorkflowStageId(String workflowName){
        WorkflowNode workflowNode = clarityNodeManager.getWorkflowNodeByName(workflowName)
        StageNode stageNode= workflowNode.stages[0].stageNode
        StepConfigurationNode stepConfigurationNode = stageNode.stepConfigurationNode
        return stepConfigurationNode.stepId
    }

    WorkflowNode getLibraryCreationWorkflowStageUri(String queueName){
        String workflowName = ClaritySampleAliquot.LIBRARY_CREATION_WORKFLOW_PREFIX + queueName
        return clarityNodeManager.getWorkflowNodeByName(workflowName)
    }

    void "test sendEmailNotificaion all aliquots pass"() {
        setup:
            def processId = '24-142546'//production
            AcRouteToWorkflow actionHandler = actionHandlerPlate(processId)
        when:
            List<EmailEvent> emailEvents = actionHandler.sendEmailNotification()
        then:
            !emailEvents
    }

    @IgnoreRest
    void "test sendEmailNotificaion TUBE some aliquots failed"() {
        setup:
            def processId = '24-130423'//production
            AcRouteToWorkflow actionHandler = actionHandlerTubes(processId)
        when:
            List<EmailEvent> emailEvents = actionHandler.sendEmailNotification()
        then:
            emailEvents
            emailEvents.size() == 2
        when:
            def analytes = actionHandler.process.outputAnalytes.findAll{ (it as ClaritySampleAliquot).udfLabResult == Analyte.FAIL }
        AnalyteSorter.sortInPlace(analytes)

        ClaritySampleAliquot analyte = (ClaritySampleAliquot) analytes[0]
        ScheduledSample scheduledSample = (ScheduledSample) analyte.claritySample
            def libraryCreationQueue = scheduledSample.libraryCreationQueue?.libraryCreationQueue
            def sowItemId = scheduledSample.sowItemId
        PmoSample pmoSample = scheduledSample.pmoSample
        SequencingProject sp = pmoSample.sequencingProject
            String spManagerId = sp.udfSequencingProjectManagerId as String
            def proposalId = sp.udfSequencingProjectProposalId as Long
            def sampleMass = pmoSample.udfVolumeUl * pmoSample.udfConcentration
            Set<Long> entityIds = [proposalId]

        EmailEvent ee = emailEvents[0]
            def body = "$ee.body"
        then:
            ee.cc == SampleAliquotEmailNotification.CC_EMAIL_GROUP
            ee.entity_id == entityIds
            ee.entity_type == EmailEvent.ENTITY_TYPE_PROPOSAL
            ee.subject == SampleAliquotEmailNotification.SUBJECT
            ee.from == spManagerId
            ee.to == spManagerId

            ee.body.startsWith("\nThis is an automated notification that an aliquot has been failed during sample aliquot creation on")
            ee.body.endsWith("Sincerely,\nJGI Project Management Office")

            body.contains("Proposal ID; Sequencing Project ID; Sequencing Project Name; SP Product; SP PI Name; ")
            body.contains("Sample Contact(s); PM Name; PMO Sample ID; Sample Name; Sample Mass; LIMS Sample ID; ")
            body.contains("SOW Item ID; SOW Item Type; Default library queue; Failure Mode; Comments")

            body.contains("$proposalId; $sp.name; $sp.udfSequencingProjectName; $sp.udfSequencingProductName; ${EmailDetails.formatName(sp.udfSequencingProjectPI)}; ")
            body.contains("${EmailDetails.formatName(getSampleContactName(sp))}; ${EmailDetails.formatName(sp.udfSequencingProjectManager)}; ${pmoSample.name}; $pmoSample.udfCollaboratorSampleName; $sampleMass; $pmoSample.id; ")
            body.contains("$sowItemId; $scheduledSample.udfSowItemType; $libraryCreationQueue; $analyte.udfFailureMode; $analyte.udfNotes")
        when:
            ee = emailEvents[1]
            entityIds = [2777]
            spManagerId = '3313'
        then:
            assert ee.cc == SampleAliquotEmailNotification.CC_EMAIL_GROUP
            assert ee.entity_id == entityIds
            assert ee.entity_type == EmailEvent.ENTITY_TYPE_PROPOSAL
            assert ee.subject == SampleAliquotEmailNotification.SUBJECT
            assert ee.from == spManagerId
            assert ee.to == spManagerId

            assert ee.body.startsWith("\nThis is an automated notification that an aliquot has been failed during sample aliquot creation on")
            assert ee.body.endsWith("Sincerely,\nJGI Project Management Office")

            assert ee.body.contains("Proposal ID; Sequencing Project ID; Sequencing Project Name; SP Product; SP PI Name; ")
            assert ee.body.contains("Sample Contact(s); PM Name; PMO Sample ID; Sample Name; Sample Mass; LIMS Sample ID; ")
            assert ee.body.contains("SOW Item ID; SOW Item Type; Default library queue; Failure Mode; Comments")

            //assert ee.body.contains("$proposalId; $sp.name; $sp.udfSequencingProjectName; $sp.udfSequencingProductName; $sp.udfSequencingProjectPI; ")
            //assert ee.body.contains("${getSampleContactName(sp)}; $sp.udfSequencingProjectManager; $pmoSample.name; $pmoSample.udfCollaboratorSampleName; $sampleMass; $pmoSample.id; ")
            //assert ee.body.contains("$sowItemId; $scheduledSample.udfSowItemType; $libraryCreationQueue; $analyte.udfFailureMode; $analyte.udfNotes")
            noExceptionThrown()
    }

    def getSampleContactName(sqProject) {
        String[] sampleContactSplit = sqProject.udfSampleContact?.split(', ')
        def sampleContactName = ''
        if (sampleContactSplit && sampleContactSplit.length == 2)
            sampleContactName = "${sampleContactSplit[1]} ${sampleContactSplit[0]}"
        sampleContactName
    }

    @Ignore
    void "test sendEmailNotificaion PLATE some aliquots failed"() {
        setup:
            Set<Long> entityIds = [2408]
            def processId = '24-145244'//'24-119329'//production
            AcRouteToWorkflow actionHandler = actionHandlerPlate(processId)
        when:
            //actionHandler.process.outputAnalytes*.setSystemQcFlag(false)
            List<EmailEvent> emailEvents = actionHandler.sendEmailNotification()
        then:
            emailEvents
            emailEvents.size() == 1
        when:
        EmailEvent ee = emailEvents[0]
            def sequencingProjectManagerId = '5179'
        def test = "$ee.body"
        List<Analyte> failedAliquots = findFailedAliquotsByProjectManagerId(actionHandler, sequencingProjectManagerId)
        then:
            assert ee.cc == SampleAliquotEmailNotification.CC_EMAIL_GROUP
            assert ee.entity_id == entityIds
            assert ee.entity_type == EmailEvent.ENTITY_TYPE_PROPOSAL
            assert ee.subject == SampleAliquotEmailNotification.SUBJECT
            assert ee.body.startsWith("\nThis is an automated notification that an aliquot has been failed during sample aliquot creation on")
            assert ee.body.endsWith("Sincerely,\nJGI Project Management Office")
            assert ee.from == sequencingProjectManagerId
            assert ee.to == sequencingProjectManagerId

        failedAliquots.each { sampleAliquot ->
            ScheduledSample scheduledSample = sampleAliquot.claritySample as ScheduledSample
            PmoSample pmoSample = scheduledSample.pmoSample
            SequencingProject sequencingProject = pmoSample.sequencingProject
            def sequencingProjectId = sequencingProject.name
            assert test.contains(sequencingProjectId)
            assert StringUtils.countMatches(ee.body, sequencingProjectId) == 2
            assert test.contains(sequencingProject.udfSequencingProjectName)
            assert test.contains(sequencingProject.udfSequencingProjectPI)
            assert test.contains(sequencingProject.udfSequencingProjectManager)
            assert test.contains(pmoSample.name)
            assert test.contains(pmoSample.id)
            assert test.contains(scheduledSample.name)
            assert test.contains(scheduledSample.udfSowItemType)
            assert test.contains(scheduledSample.libraryCreationQueue?.libraryCreationQueue)
            assert test.contains((sampleAliquot as ClaritySampleAliquot).udfFailureMode)
            assert test.contains(scheduledSample.udfNotes)
        }
            assert test.contains("Proposal ID; Sequencing Project ID; Sequencing Project Name; SP Product; SP PI Name; Sample Contact(s); PM Name; PMO Sample ID; Sample Name; Sample Mass; LIMS Sample ID; SOW Item ID; SOW Item Type; Default library queue; Failure Mode; Comments")
            assert test.contains("1126074;   seq proj 1474332621566;   Paley, Tatiana;   Paley, Tatiana;   129926;   PAL18539A1;   182358;   Fragment;   Illumina Regular Fragment, 300bp, Plates;   Operator Error;")
            assert test.contains("1126074;   seq proj 1474332621566;   Paley, Tatiana;   Paley, Tatiana;   129928;   PAL18539A3;   182360;   Fragment;   Illumina Regular Fragment, 300bp, Plates;   Instrument Error;")
/*
        when:
            ee = emailEvents[1]
            sequencingProjectManagerId = '2672'
            def sequencingProjectId = '1127013'
            test = "$ee.body"
        then:
            assert ee.cc == SampleAliquotEmailNotification.CC_EMAIL_GROUP
            assert ee.entity_id == entityIds
            assert ee.entity_type == EmailEvent.ENTITY_TYPE_PROPOSAL
            assert ee.subject == SampleAliquotEmailNotification.SUBJECT
            assert ee.from == sequencingProjectManagerId
            assert ee.to == sequencingProjectManagerId

            assert ee.body.startsWith("\nThis is an automated notification that an aliquot has been failed during sample aliquot creation on")
            assert ee.body.endsWith("Sincerely,\nJGI Project Management Office")
            assert test.contains("$sequencingProjectId")
            assert StringUtils.countMatches(ee.body, sequencingProjectId) == 2
            assert test.contains("Sequencing Project ID; Sequencing Project Name; PI Name; PM Name; PMO Sample ID; LIMS Sample ID; SOW Item ID; SOW Item Type; Default library queue; Failure Mode; Comments")
            assert test.contains("1127013;   seq proj 1474586661960;   Scott, Duncan;   Scott, Duncan;   136015;   SCO19478A2;   188451;   Fragment;   Illumina Regular Fragment, 300bp, Plates;   Informatics Error;")
            assert test.contains("1127013;   seq proj 1474586661960;   Scott, Duncan;   Scott, Duncan;   136014;   SCO19478A3;   188450;   Fragment;   Illumina Regular Fragment, 300bp, Plates;   Informatics Error;")
*/
            noExceptionThrown()
    }

    AcRouteToWorkflow actionHandlerPlate(processId) {
        String action = 'RouteToNextWorkflow'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleAliquotCreationProcess process = new SampleAliquotCreationProcess(processNode)
        AcRouteToWorkflow actionHandler = process.getActionHandler(action) as AcRouteToWorkflow
        process.testMode = true
        actionHandler
    }

    AcRouteToWorkflow actionHandlerTubes(processId) {
        String action = 'RouteToNextWorkflow'
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
        SampleAliquotCreationProcess process = new SampleAliquotCreationProcess(processNode)
        AcRouteToWorkflow actionHandler = process.getActionHandler(action) as AcRouteToWorkflow
        process.testMode = true
        actionHandler
    }

    List<Analyte> findFailedAliquotsByProjectManagerId(AcRouteToWorkflow actionHandler, String sequencingProjectManagerId) {
        return actionHandler.process.outputAnalytes.findAll{ it.systemQcFlag == Boolean.FALSE &&
                it.claritySample.pmoSample.sequencingProject.udfSequencingProjectManagerId as String == sequencingProjectManagerId
        }
    }
}
