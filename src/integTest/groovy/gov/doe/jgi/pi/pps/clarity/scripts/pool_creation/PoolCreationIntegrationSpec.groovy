package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.TestUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling.PoolingIlluminaDualIndexes
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import grails.gorm.transactions.Transactional
import net.sf.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.IgnoreRest
import spock.lang.Specification

import java.text.DateFormat
import java.text.SimpleDateFormat

@SpringBootTest(classes = [Application.class])
@Transactional
class PoolCreationIntegrationSpec extends Specification {
    static final logger = LoggerFactory.getLogger(PoolCreationIntegrationSpec.class)
    NodeConfig clarityNodeConfig
    @Autowired
    NodeManager clarityNodeManager
    def webTransaction
    String processId
    PoolCreation process
    TestUtility testUtil

    def setup() {
        clarityNodeConfig = clarityNodeManager.nodeConfig
        testUtil = new TestUtility(clarityNodeManager)
        processId = testUtil.getProcessIds(ProcessType.LP_POOL_CREATION.value).first()
        process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(processId)) as PoolCreation
        process.testMode = true
    }

    def cleanup() {
    }

    @Ignore//Rest
    void "test isSAG"(){
        setup:
        String libraryLimsId = "2-3585940"
        when:
        ClarityLibraryStock ls = new ClarityLibraryStock(clarityNodeManager.getArtifactNode(libraryLimsId))
        then:
        assert ls.isSAG
        when:
        String poolLimsId = '2-3458559'
        ClarityLibraryPool lp = new ClarityLibraryPool(clarityNodeManager.getArtifactNode(poolLimsId))
        then:
        assert lp.isSAG
    }

//    @IgnoreRest
    void "test minimal pools" (){
        setup:
        ArtifactIndexService ais = BeanUtil.getBean(ArtifactIndexService.class)
        Map<String, List<Index>> indexSets = ais.getIndexes(['NexteraXT_v2_SetD', 'NexteraXT_v2_SetC', 'NexteraXT_v2_SetB', 'NexteraXT_v2_SetA', 'NexteraXT_v1'])
        when:
        indexSets.each {String setName, List<Index> indexes ->
            List<LibraryInformation> lis = indexes.collect{new LibraryInformation(indexName: it.indexName, indexSequence: it.indexSequence, libraryName: it.plateLocation)}
            Pooling poolingDop = new PoolingIlluminaDualIndexes(lis)
            poolingDop.degreeOfPooling = 10
            poolingDop.reqdPoolsCount = 10
            def pools = poolingDop.getMinimalPools(poolingDop.splitMembers(lis),10)
            logger.info "Index set: $setName"
            logger.info "Minimal pools: $pools"
        }
        then:
        noExceptionThrown()
    }

    @Ignore
    void "test request json"(){
        setup:
        String containerId = '27-463610'
        ContainerNode containerNode = clarityNodeManager.getContainerNode(containerId)
        when:
        JSONObject submissionJson = new JSONObject()
        submissionJson['turn-off-action-scripts'] = false
        submissionJson['clarity-password'] = 'TestUserClarity'
        submissionJson['process-type'] = 'LP Pool Creation'
        submissionJson['input-artifacts'] = containerNode.contentsArtifactNodes.collect{it.id}
        submissionJson['clarity-username'] = 'TestUserClarity'
        then:
        println submissionJson.toString(2)
    }

    @Ignore//Rest
    void "execute process actions"(String action){
        setup:
        processId = '122-911750'
        PoolCreation poolCreation = new PoolCreation(clarityNodeManager.getProcessNode(processId))
        ActionHandler actionHandler = poolCreation.getActionHandler(action)
        when:
             actionHandler.execute()
        then:
            noExceptionThrown()
        where:
            action << [
//                'PreProcessValidation',
//                'PreparePoolingPrepSheet',
                'ProcessPoolingPrepSheet',
//                'PoolInputs',
//                'PreparePoolCreationSheet',
//                'PrintLabels',
//                'ProcessPoolCreationSheet',
//                'RouteToNextWorkflow'
        ]
    }

    //TODO create NovaSeq
    String libraryLimsId = '2-3041697'

    void "test print labels"(){
        expect:
            process
        when:
        def actionHandler = new PrintLabels(process: process)
        def workbookName = PoolCreation.SCRIPT_GENERATED_POOL_CREATION_SHEET
        def resultFile = process.processNode.outputResultFiles.find { it.name.contains(workbookName)}
        if (resultFile.fileNode && resultFile.fileNode.id) {
            actionHandler.execute()
        }
        then:
        noExceptionThrown()
    }

    Map<String, List<LibraryInformation>> getPoolingData(){
        Map<String, List<LibraryInformation>> testDualIndexData = [:].withDefault {[]}
        List<List<List<String>>> testData = []
        List<List<String>> testData1 = [
                ["N712-S502", "GTAGAGGA-CTCTCTAT", 4],
                ["N712-S508", "GTAGAGGA-CTAAGCCT", 4],
                ["N702-S502", "CGTACTAG-CTCTCTAT", 4],
                ["N702-S502", "ATTACTCG-CCTATCCT", 4],
                ["D706-D504", "GAATTCGT-GGCTCTGA", 4],
                ["N704-S504", "TCCTGAGC-AGAGTAGA", 4],
                ["D701-D503", "CGTACTAG-CTCTCTAT", 4],
                ["D704-D506", "GAGATTCC-TAATCTTA", 4],
                ["N702-S502", "CGTACTAG-CTCTCTAT", 4],
                ["D703-D505", "CGCTCATT-AGGCGAAG", 4],
                ["D701-D503", "ATTCAGAA-CCTATCCT", 4],
                ["N705-S517", "GGACTCCT-GCGTAAGA", 4],
                ["N706-S502", "TAGGCATG-CTCTCTAT", 4]
        ]
        testData << testData1
        List<List<String>> testData2 = [
                ["N706-S502", "GACTTAGA-ACCATGCT", 3],
                ["N705-S517", "AGACCTCG-TTGGACTC", 3],
                ["D701-D503", "ATTACTTG-GAATTCCT", 3],
                ["N702-S502", "ATTACTTG-GAATTGCT", 3],
                ["N702-S502", "AGACCTCG-TTGGACTC", 3],
                ["D706-D504", "GACTTAGA-ACCATCCT", 3]
        ]
        testData << testData2
        List<List<String>> testData3 = [
                ["N706-S502", "GACTTAGA-ACCATGCT", 4],
                ["N705-S517", "AGTCATCT-GGTGACTC", 4],
                ["D701-D503", "TCCTAGAG-GCTACTGA", 4],
                ["N702-S502", "AGTCGCTC-ATCGTACT", 4],
                ["N712-S508", "ACGCATCC-TAGAGAAT", 4],
                ["D706-D504", "CATAGTAG-AAGATCCT", 4]
        ]
        testData << testData3
        List<List<String>> testData4 = [
                ["N701-S502", "TAAGGCGA-CTCTCTAT", 9],
                ["N701-S502", "TAAGGCGA-CTCTCTAT", 9],
                ["N701-S503", "TAAGGCGA-TATCCTCT", 9],
                ["N701-S503", "TAAGGCGA-TATCCTCT", 9],
                ["N701-S504", "TAAGGCGA-AGAGTAGA", 9],
                ["N701-S504", "TAAGGCGA-AGAGTAGA", 9],
                ["N702-S502", "CGTACTAG-CTCTCTAT", 9],
                ["N702-S502", "CGTACTAG-CTCTCTAT", 9],
                ["N702-S503", "CGTACTAG-TATCCTCT", 9],
                ["N702-S503", "CGTACTAG-TATCCTCT", 9],
                ["N702-S504", "CGTACTAG-AGAGTAGA", 9],
                ["N703-S502", "AGGCAGAA-CTCTCTAT", 9],
                ["N703-S503", "AGGCAGAA-TATCCTCT", 9],
                ["N703-S504", "AGGCAGAA-AGAGTAGA", 9],
                ["N704-S502", "TCCTGAGC-CTCTCTAT", 9],
                ["N704-S502", "TCCTGAGC-CTCTCTAT", 9],
                ["N704-S503", "TCCTGAGC-TATCCTCT", 9],
                ["N704-S503", "TCCTGAGC-TATCCTCT", 9],
                ["N704-S504", "TCCTGAGC-AGAGTAGA", 9],
                ["N704-S504", "TCCTGAGC-AGAGTAGA", 9],
                ["N705-S502", "GGACTCCT-CTCTCTAT", 9],
                ["N705-S503", "GGACTCCT-TATCCTCT", 9],
                ["N705-S504", "GGACTCCT-AGAGTAGA", 9],
                ["N706-S502", "TAGGCATG-CTCTCTAT", 9],
                ["N706-S503", "TAGGCATG-TATCCTCT", 9],
                ["N706-S504", "TAGGCATG-AGAGTAGA", 9]
        ]
        testData << testData4
        int index = 1
        testData.each{List<List<String>> data ->
            List<LibraryInformation> libraries = []
            data.each{List<String> dt ->
                LibraryInformation bean = new LibraryInformation()
                bean.libraryName = dt[0]
                bean.indexName = dt[0]
                bean.indexSequence = dt[1]
                bean.dop = dt[2] as Integer
                libraries<< bean
            }
            testDualIndexData['testData' + index++] = libraries
        }
        testDualIndexData
    }

    Map<String, List<LibraryInformation>> getSingleAndDoubleIndexData(){
        String testDataString = """TAY41247A2AR74,27-233369,CWNZ,1/10/2015,D709-D502,CGGCTATG-ATAGAGGC,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR75,27-233369,CWNW,1/10/2015,D708-D505,TAATGCGC-AGGCGAAG,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR76,27-233369,CWOC,1/10/2015,D710-D501,TCCGCGAA-TATAGCCT,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR77,27-233369,CWNY,1/10/2015,D710-D504,TCCGCGAA-GGCTCTGA,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR78,27-233369,CWNX,1/10/2015,D709-D501,CGGCTATG-TATAGCCT,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR79,27-233369,CWOA,1/10/2015,D707-D506,CTGAAGCT-TAATCTTA,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR80,27-233369,CWOB,1/10/2015,D702-D505,TCCGGAGA-AGGCGAAG,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR81,27-233369,CWOD,1/10/2015,D706-D501,GAATTCGT-TATAGCCT,Illumina,MiSeq,1X100,DNA,Y,1;TAY41247A2AR82,27-233369,CWOE,1/10/2015,D705-D502,ATTCAGAA-ATAGAGGC,Illumina,MiSeq,1X100,DNA,Y,1;&TAY41247A2AR74,27-233369,CWNZ,1/10/2015,NG001,CGGCTATG,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR75,27-233369,CWNW,1/10/2015,NG002,TAATGCGC,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR76,27-233369,CWOC,1/10/2015,NG003,TCCGCGAA,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR77,27-233369,CWNY,1/10/2015,NG004,TCCGCGAT,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR78,27-233369,CWNX,1/10/2015,NG005,CGGCTATC,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR79,27-233369,CWOA,1/10/2015,NG006,CTGAAGCT,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR80,27-233369,CWOB,1/10/2015,NG007,TCCGGAGA,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR81,27-233369,CWOD,1/10/2015,NG008,GAATTCGT,Illumina,MiSeq,1X150,RNA,Y,1;TAY41247A2AR82,27-233369,CWOE,1/10/2015,NG009,ATTCAGAA,Illumina,MiSeq,1X150,RNA,Y,1;"""
        DateFormat format = new SimpleDateFormat('MM/dd/yyyy')
        Map<String, List<LibraryInformation>> testDataMap = [:]
        testDataString.split('&').each {testData ->
            (2..9).each{int dop->
                String[] testDataArray = testData?.split(";")
                List<LibraryInformation> beansList = []
                testDataArray.each { String singleData ->
                    String[] fields = singleData.split(",")
                    LibraryInformation bean = new LibraryInformation()
                    bean.analyteLimsId = fields[0]
                    bean.containerId = fields[1]
                    bean.libraryName = fields[2]
                    bean.queueDate = format.parse(fields[3])
                    bean.dop = dop
                    bean.indexName = fields[4]
                    bean.indexSequence = fields[5]
                    bean.platform = fields[6]
                    bean.sequencerModel = fields[7]
                    bean.runMode = fields[8]
                    bean.materialType = fields[9]
                    bean.amplified = fields[10]
                    bean.concentration = fields[11]
                    beansList << bean
                }
                String key = "${beansList[0].containerId}${beansList[0].dop}${beansList[0].platform}${beansList[0].sequencerModel}${beansList[0].runMode}${beansList[0].materialType}${beansList[0].amplified}${beansList[0].indexName.contains('-')?'isDual':''}"
                testDataMap[key] = beansList
            }
        }
        testDataMap
    }

    @IgnoreRest
    void "test routing"(){
        setup:
        List<RunModeCv> runModeCvs = RunModeCv.where {active == 'Y'}.list()
        Map<String, String> runModeSeqQueue = [:]
        when:
        runModeCvs?.each{ RunModeCv runMode ->
            runModeSeqQueue[runMode.runMode] = process.getUriToRoute(runMode)
        }
        then:
        noExceptionThrown()
    }
}
