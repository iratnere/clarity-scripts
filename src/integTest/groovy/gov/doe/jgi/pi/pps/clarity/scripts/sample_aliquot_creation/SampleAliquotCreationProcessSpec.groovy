package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.Application
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import grails.gorm.transactions.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

import java.text.SimpleDateFormat

@SpringBootTest(classes = [Application.class])
@Transactional
class SampleAliquotCreationProcessSpec extends Specification {
    @Autowired
    NodeManager clarityNodeManager
    @Shared
    def sampleAliquotId = '2-3047383' // output of the 'AC Sample Aliquot Creation' process: tubes->tubes
    @Shared
    def plateSaId = '2-3938697' // output of the 'AC Sample Aliquot Creation' process
    SampleAliquotCreationProcess clarityProcess
    ClaritySampleAliquot claritySampleAliquot

    def setup() {
        claritySampleAliquot = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(sampleAliquotId)) as ClaritySampleAliquot
        ProcessNode processNode = clarityNodeManager.getProcessNode(claritySampleAliquot.artifactNodeInterface.parentProcessId)
        clarityProcess = new SampleAliquotCreationProcess(processNode)
    }

    def cleanup() {
    }

    void "test getBatchPmoSampleNodes"() {
        expect:
            claritySampleAliquot
            clarityProcess
        when:
            List<SampleNode> sampleNodes = clarityProcess.batchPmoSampleNodes
        then:
            sampleNodes.size() == 1
            sampleNodes[0].id == claritySampleAliquot.parentPmoSamples.find{true}.id
    }

    void "test getBatchPmoSampleArtifactNodes"() {
        expect:
            claritySampleAliquot
            clarityProcess
        when:
            List<ArtifactNode> artifactNodes = clarityProcess.getBatchPmoSampleArtifactNodes()
        then:
            artifactNodes.size() == 1
            artifactNodes[0].id == claritySampleAliquot.parentPmoSamples.find{true}.sampleAnalyte.id
    }

    void "test getBatchPmoSampleContainerNodes"() {
        expect:
            claritySampleAliquot
            clarityProcess
        when:
            List<ContainerNode> containerNodes = clarityProcess.getBatchPmoSampleContainerNodes()
        then:
            containerNodes.size() == 1
            containerNodes[0].id == claritySampleAliquot.parentPmoSamples.find{true}.sampleAnalyte.containerNode.id
    }

    @Ignore//Rest
    void "test actionHandler"(String action) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-1024025')
        SampleAliquotCreationProcess clarityProcess = new SampleAliquotCreationProcess(processNode)
        AcProcessAliquotCreationSheet actionHandler = clarityProcess.getActionHandler(action) as AcProcessAliquotCreationSheet
        when:
            actionHandler.execute()
		then:
            noExceptionThrown()
        where:
            action << ['ProcessAliquotCreationSheet']
    }

    @Ignore//If({!(System.getProperty('grails.env') in ['claritydev1'])})
    void "test process"(String action) {
        setup:
        ProcessNode processNode = clarityNodeManager.getProcessNode('24-1053315')
            SampleAliquotCreationProcess clarityProcess = new SampleAliquotCreationProcess(processNode)
        ActionHandler actionHandler = clarityProcess.getActionHandler(action)
        when:
            actionHandler.execute()
        then:
            noExceptionThrown()
        where:
            action << [
                    //+'PreProcessValidation',
                    'PrepareAliquotCreationSheet',
                    //'ProcessAliquotCreationSheet',
                    //'Print Labels',
                    //'RouteToNextWorkflow'
            ]
    }

    void "test LibraryCreationQueueCv"() {
        when:
            def cvs = LibraryCreationQueueCv.list()
        then:
            cvs.size()
            cvs.each{
                //println it.libraryCreationQueue
                assert it.libraryCreationQueue
            }
            [
                'Illumina Regular Fragment, 300bp, Plates',
                'Illumina Regular Fragment, 600bp, Plates',
                'PacBio >10kb w/ AMPure Bead Size Selection, Tubes',
                'Not JGI',
                'Illumina Nextera - Acoustic, Plates'//Single Cell Internal, Plates'
            ].each{ assert it in cvs*.libraryCreationQueue}
    }

    void "test getPlateLcQueue Plate"() {
        setup:
        ClaritySampleAliquot claritySampleAliquot = AnalyteFactory.analyteInstance(clarityNodeManager.getArtifactNode(plateSaId)) as ClaritySampleAliquot
        ProcessNode processNode = clarityNodeManager.getProcessNode(claritySampleAliquot.artifactNodeInterface.parentProcessId)
            SampleAliquotCreationProcess process = new SampleAliquotCreationProcess(processNode)
        expect:
            process.outputAnalytes
            def outputContainer = process.outputAnalytes[0].containerNode
            outputContainer.isPlate
            process.plateLcQueueCached == null
        when:
            def queue = process.getPlateLcQueue()
        then:
            noExceptionThrown()
            queue
            process.plateLcQueueCached
    }

    void "test getPlateLcQueue Tubes"() {
        setup:
            SampleAliquotCreationProcess process = clarityProcess
        expect:
            process.outputAnalytes
            def outputContainer = process.outputAnalytes[0].containerNode
            !outputContainer.isPlate
            process.plateLcQueueCached == null
        when:
            def queue = process.getPlateLcQueue()
        then:
            noExceptionThrown()
            !queue
            process.plateLcQueueCached == null
    }

    void "test buildSampleTubePlateLabel"(String dateStr, int counter) {
        setup:
            SampleAliquotCreationProcess process = clarityProcess
            process.testMode = true
        when:
            Date date = dateStr ? new SimpleDateFormat('yyyy-MM-dd').parse(dateStr) : new Date()
            def label = process.buildSampleTubePlateLabel(1000, date)
        then:
            String formatDate = new SimpleDateFormat('yy-MMM-dd').format(date)
            label == "${formatDate}_RA_$counter"
            noExceptionThrown()
        where:
            dateStr      | counter
            //'2017-07-12'|6
            //'2015-07-16'    |5          //4 re-array plate containers
            '2016-09-08' | 2          //1 re-array plate container
            '2016-08-30' | 1          //no re-array plate containers
            '2016-09-04' | 1          //no SA Creation processes
            //''              |6        //today
    }

    void "test buildSampleTubePlateLabel error"(String dateStr, int counter) {
        setup:
            SampleAliquotCreationProcess process = clarityProcess
            process.testMode = true
        when:
            Date date = dateStr ? new SimpleDateFormat('yyyy-MM-dd').parse(dateStr) : new Date()
            process.buildSampleTubePlateLabel(counter, date)
        then:
        WebException exception = thrown()
            exception.message.contains('Cannot find an empty re-array label')
            exception.message.contains("max counter $counter")
        where:
            dateStr      | counter
            '2016-09-08' | 1
    }
    @Ignore//Rest
    void "test getSampleTubePlateLabel"() {
        setup:
            Date processDate = new Date()
            ArtifactNodeService artifactNodeService = BeanUtil.getBean(ArtifactNodeService.class)
            def counter = (1..10).find {
                List<String> containerLimsIds = artifactNodeService.collectPlateContainerLimsIds(it, processDate)
                if (!containerLimsIds)
                    return it
                return null
            }
        ProcessNode processNode = clarityNodeManager.getProcessNode(limsid)
            SampleAliquotCreationProcess process = new SampleAliquotCreationProcess(processNode)
            process.testMode = true
        when:
            String label = process.getSampleTubePlateLabel()
        then:
            String formatDate = new SimpleDateFormat('yy-MMM-dd').format(processDate)
            label == "${formatDate}_RA_$counter"
        where:
            limsid << ['24-140441']//, '24-341169''24-1456013']
            //  15-Jul-16_RA_6  15-Jul-16_RA_6
    }

    void "test getSampleTubePlateLabel inputs 1 plate&tubes to 1 output plate"() {
        setup:
            def processId = '24-140441'
            Date processDate = new Date()
        when:
        ProcessNode processNode = clarityNodeManager.getProcessNode(processId)
            SampleAliquotCreationProcess process = new SampleAliquotCreationProcess(processNode)
            process.testMode = true
            String label = process.getSampleTubePlateLabel()
        then:
            String formatDate = new SimpleDateFormat('yy-MMM-dd').format(processDate)
            label.contains("${formatDate}_RA_")
    }
}
