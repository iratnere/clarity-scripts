package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 9/16/2015.
 */
class PbRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbRouteToWorkflow.class)
    List<NextAction> nextActions

    void performLastStepActions(){
        Set<PacBioBindingComplex> bindingComplexes = new HashSet<PacBioBindingComplex>()
        Set<PacBioAnnealingComplex> annealingComplexes = new HashSet<PacBioAnnealingComplex>()
        Set<Analyte> analytes = new HashSet<Analyte>()
        nextActions = new ArrayList<NextAction>()
        process.inputAnalytes.each { Analyte inputAnalyte ->
            PacBioBindingComplex bindingComplex = inputAnalyte as PacBioBindingComplex
            bindingComplexes.add(bindingComplex)

            PacBioAnnealingComplex annealingComplex = bindingComplex.parentAnalyte
            annealingComplexes.add(annealingComplex)

            Analyte library = annealingComplex.parentAnalyte //could be ClarityLibraryStock or ClarityLibraryPool
            analytes.add(library)

            setNextActions(process.processNode.getOutputAnalytes(inputAnalyte.id))
        }
        if (process.isPacBioSequel) {
            process.routeAnalytes(Stage.PACBIO_SEQUEL_SEQUENCING_COMPLETE, bindingComplexes)
        } else {
            throw new WebException("no handler for input stage ${process.inputStage}",422)
        }
        process.routeAnalytes(Stage.REQUEUE_LIBRARY_BINDING, annealingComplexes)
        process.routeAnalytes(Stage.REQUEUE_LIBRARY_ANNEALING, analytes)
        if (process.isPacBioSequel) {
            process.routeAnalytes(Stage.PACBIO_SEQUEL_MAGBEAD_CLEANUP, analytes)
        } else {
            throw new WebException("no handler for input stage ${process.inputStage}",422)
        }
    }

    void setNextActions(List<ArtifactNode> outputAnalytes){
        outputAnalytes.each { ArtifactNode outputArtifact ->
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(outputArtifact)
            nextActions << outputAnalyte.getNextAction(ActionType.REPEAT, '')
        }
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        performLastStepActions()
        /*
        process.routeArtifactNodes(Stage.PACBIO_SEQUENCING_COMPLETE, bindingComplexes)
        process.routeArtifactNodes(Stage.REQUEUE_LIBRARY_BINDING, annealingComplexes)
        process.routeArtifactNodes(Stage.REQUEUE_LIBRARY_ANNEALING, libraryStocks)
        process.routeArtifactNodes(Stage.PACBIO_MAGBEAD_CLEANUP, libraryStocks)
        process.setArtifactActions(nextActions)
        */
        process.setArtifactActions(nextActions)
    }
}
