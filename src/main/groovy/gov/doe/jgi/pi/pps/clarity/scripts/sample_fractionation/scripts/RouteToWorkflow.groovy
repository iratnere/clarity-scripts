package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleFractionationService

class RouteToWorkflow extends ActionHandler {

    SampleFractionationService sampleFractionationService

    void execute() {
        sampleFractionationService = gov.doe.jgi.pi.pps.util.util.BeanUtil.getBean(SampleFractionationService.class)
        sampleFractionationService.createFractionSamples(process.outputAnalytes.findAll{it.systemQcFlag}*.id, process.researcherContactId)
    }
}