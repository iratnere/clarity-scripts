package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.RoutingService
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.GenericIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfFieldTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/14/16.
 */
abstract class Migrator {
    static final logger = LoggerFactory.getLogger(Migrator.class)
    Migrator parentMigrator
    NodeManager clarityNodeManager
    static Map<ContainerLocation, Analyte> containerContents = [:]
    static List<RoutingRequest> routingRequests = []
    String currentUser = null
    String currentPassword = null

    static final String DEFAULT_PROCESS_UDF='Unknown'
    static final Long DEFAULT_CONTACT_ID = 17828
    static final int PARENT_UDFS = 1
    static final int PARENT_CONTAINER_UDFS = 2
    static final int SELF_UDFS = 3
    static final int SELF_CONTAINER_UDFS = 4
    static final String ONBOARDING_USERNAME='Migration'
    static final String ONBOARDING_PASSWORD='Migration'

    Migrator(NodeManager clarityNodeManager, Migrator parentMigrator){
        this.clarityNodeManager = clarityNodeManager
        this.parentMigrator = parentMigrator
    }

    void prepareNodeManager() {
        NodeManager nodeManager = BeanUtil.nodeManager
        currentUser = nodeManager.nodeConfig.geneusUser
        currentPassword = nodeManager.nodeConfig.geneusPassword
        nodeManager.nodeConfig.geneusUser = ONBOARDING_USERNAME
        nodeManager.nodeConfig.geneusPassword = ONBOARDING_PASSWORD
    }

    void resetNodeManager() {
        if(currentUser && currentPassword) {
            NodeManager nodeManager = BeanUtil.nodeManager
            nodeManager.nodeConfig.geneusUser = currentUser
            nodeManager.nodeConfig.geneusPassword = currentPassword
        }
    }

    def createAnalytesInClarity(List<OnboardingBean> onboardingBeans, boolean startingPoint = false){
        if(startingPoint) {
            prepareNodeManager()
        }
        List<Analyte> parents = parentMigrator?.createAnalytesInClarity(onboardingBeans)
        List<OnboardingBean> unmergedBeans = getUnmergedBeans(onboardingBeans)
        if(unmergedBeans) {
            parentMigrator?.performAdditionalActions(parents, onboardingBeans.first().contactId)
            parents.addAll(unmergedBeans.collect{parentMigrator.getAnalyte(it)})
            parents.unique()
        }
        clarityNodeManager.getContainerNodes(parents.collect {it.containerId})
        Map containerTypeParents = parents.groupBy{it.containerNode?.containerTypeEnum}
        def analytes = []
        containerTypeParents.each{ ContainerTypes type, List<Analyte> parentAnalytes ->
            List<ContainerNode> oldContainers = parentAnalytes.collect{it.containerNode}.unique()
            List<ContainerNode> newContainers = clarityNodeManager.createContainersBulk(getContainerType(parentAnalytes.first()), oldContainers.size())
            Map<ContainerLocation, List<ArtifactNode>> containerParents = [:].withDefault {[]}
            int i=0
            oldContainers.each{ ContainerNode containerNode ->
                containerNode.contentsArtifactNodes.each{ ArtifactNode artifactNode ->
                    def location = artifactNode?.location
                    ContainerLocation outputLocation = new ContainerLocation(newContainers[i].id, location)
                    containerParents[outputLocation] << artifactNode
                }
                i++
            }
            setDefaultUdfs(parentAnalytes, this.defaultUdfs[PARENT_UDFS], this.defaultUdfs[PARENT_CONTAINER_UDFS])
            clarityNodeManager.dirtyNodes.each {it.readOnly = false}
            clarityNodeManager.httpPutDirtyNodes()
            def workflowStage = getWorkflowStage(type)
            List<ClarityProcess> processes = createChildrenInClarity(workflowStage, containerParents)
//            newContainers = clarityNodeManager.batchRefreshGeneusNodes(newContainers)
            analytes.addAll(processes?.collect{it.outputAnalytes}?.flatten())
        }
        analytes*.setSystemQcFlag(true)
        logger.info "Done creating analytes $analytes...."
        updateBeans(unmergedBeans, analytes)
        updateUdfs(onboardingBeans)
        clarityNodeManager.dirtyNodes.each {it.readOnly = false}
        clarityNodeManager.httpPutDirtyNodes()
        logger.info "Done updating analytes $analytes..."
        analytes = this.performAdditionalActions(analytes, onboardingBeans.first().contactId)
//        updateBeans(unmergedBeans, analytes)
        def anaList = onboardingBeans.collect{getAnalyte(it)}
        routeParentsAndChildren(anaList.collect{it.parents}?.flatten(), anaList)
        if(startingPoint){
            resetNodeManager()
        }
        return analytes
    }

    void updateBeans(List<OnboardingBean> unmergedBeans, List<Analyte> analytes){
        clarityNodeManager.batchRefreshGeneusNodes(analytes.collect {it.containerNode}.unique())
        unmergedBeans.each{ OnboardingBean bean ->
            Analyte analyte = analytes.find{it.singleParent.id == parentMigrator.getAnalyte(bean).id}
            if(!analyte)
                analyte = analytes.find{it.singleParent.id == getAnalyte(bean).id}
            bean.addAnalyte(analyte)
        }
    }

    void routeParentsAndChildren(def parents, def children){
        parents?.each {parent ->
            def reqToRemove = routingRequests?.findAll{it.artifactId == parent.id && it.action == Routing.Action.assign}
            if(reqToRemove)
                routingRequests.removeAll(reqToRemove)
        }
        children?.each{ Analyte analyte ->
            String stageUri = getDestinationStage(analyte)
            def allActiveStages = analyte.artifactNode.activeStages?.collect{it.uri}
            logger.info "Active stages for $analyte -> $allActiveStages"
            if(stageUri) {
                if(!allActiveStages?.contains(stageUri)) {
                    logger.info "Adding routing request $stageUri for $analyte"
                    addRoutingRequest(new RoutingRequest(artifactId: analyte.id, routingUri: stageUri, action: Routing.Action.assign))
                }
            }
            allActiveStages?.each{addRoutingRequest(new RoutingRequest(artifactId: analyte.id, routingUri: it, action: Routing.Action.unassign))}
        }
    }

    void routeAnalytes(String stageUri, Collection<String> analyteIds){
        if(!stageUri || !analyteIds)
            return
        def routingRequests = RoutingRequest.generateRequestsToRouteArtifactIdsToUri(stageUri, analyteIds)
        RoutingService routingService = BeanUtil.getBean(RoutingService.class)
        routingService.submitRoutingRequests(routingRequests)
    }

    static void addRoutingRequest(RoutingRequest routingRequest){
        def existingRouting = routingRequests.find{it.artifactId == routingRequest.artifactId && routingRequest.routingUri == it.routingUri}
        if(!existingRouting)
            routingRequests << routingRequest
        else if(existingRouting.action != routingRequest.action)
            routingRequests.remove(existingRouting)
    }

    static void submitRoutingRequests(){
        RoutingService routingService = BeanUtil.getBean(RoutingService.class)
        routingService.submitRoutingRequests(routingRequests)
        routingRequests.clear()
    }

    List<ClarityProcess> createChildrenInClarity(Stage workflowStage, Map containerParents){
        def containerLocationAnalyteIdsMap = [:]
        List<ArtifactNode> inputs = []
        List<ClarityProcess> processes = []
        containerParents.each{ ContainerLocation outputLocation, List<ArtifactNode> parentNodes ->
            if(parentNodes.intersect(inputs)){
                routeAnalytes(workflowStage.uri, containerLocationAnalyteIdsMap?.values()?.flatten()?.collect { it.id }.unique())
                ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, workflowStage), containerLocationAnalyteIdsMap)
                processDetails.processUdfMap = getProcessUdfsMap(workflowStage)
                logger.info "Posting process ${workflowStage.value}.."
                ProcessNode processNode = clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityOutputProcess(processDetails))
                logger.info "Process execution complete $processNode"
                refreshProcessNode(processNode)
                processes << ProcessFactory.processInstance(processNode)
                inputs.clear()
            }
            parentNodes.each{ ArtifactNode parent ->
                containerLocationAnalyteIdsMap[outputLocation] = parent
            }
            inputs.addAll(parentNodes)
        }
        routeAnalytes(workflowStage.uri, containerLocationAnalyteIdsMap?.values()?.flatten()?.collect { it.id }?.unique() as Collection<String>)
        ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, workflowStage), containerLocationAnalyteIdsMap)
        processDetails.processUdfMap = getProcessUdfsMap(workflowStage)
        logger.info "Posting process ${workflowStage.value}.."
        ProcessNode processNode = clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityOutputProcess(processDetails))
        logger.info "Process execution complete $processNode"
        refreshProcessNode(processNode)
        processes << ProcessFactory.processInstance(processNode)
        return processes
    }

    void refreshProcessNode(ProcessNode processNode) {
        logger.info "Refresh process.... $processNode"
        List<ArtifactNode> outputAnalytes = clarityNodeManager.batchRefreshGeneusNodes(processNode.outputAnalytes)
        clarityNodeManager.batchRefreshGeneusNodes(outputAnalytes.collect { it.containerNode })
    }

    Map<UserDefinedField, Object> getProcessUdfsMap(Stage stage){
        Map<UserDefinedField, Object> processUdfMap = [:]
        StageUtility.getProcessUdfs(stage, clarityNodeManager)?.each{ UserDefinedField udf ->
            if(udf.type == UdfFieldTypes.DATE)
                processUdfMap[udf] = GeneusNode.defaultDateFormat.format(new Date())
            else
                processUdfMap[udf] = Migrator.DEFAULT_PROCESS_UDF
        }
        return processUdfMap
    }

    void updateUdfs(List<OnboardingBean> beans){
        if(!beans)
            return
        def defaultUdfs = this.defaultUdfs
        if(defaultUdfs)
            setDefaultUdfs(beans.collect{getAnalyte(it)}, this.defaultUdfs[SELF_UDFS], this.defaultUdfs[SELF_CONTAINER_UDFS])
        beans.each{
            it.updateUdfs(getAnalyteType())
        }
    }

    void setDefaultUdfs(List<Analyte> analytes, Map defaultArtifactUdfsMap, Map defaultContainerUdfMap) {
        if(defaultContainerUdfMap) {
            clarityNodeManager.getContainerNodes(analytes.collect{it.containerId})
        }
        analytes?.each { Analyte analyte ->
            def entity = analyte instanceof SampleAnalyte? analyte.claritySample.sampleNode:analyte.artifactNode
            defaultArtifactUdfsMap?.each { ClarityUdf udfEnum, Object value ->
                entity.setUdf(udfEnum.udf, value)
            }
            defaultContainerUdfMap?.each { ClarityUdf udfEnum, Object value ->
                if (entity instanceof ArtifactNode)
                    entity.containerNode.setUdf(udfEnum.udf, value)
                else
                    entity.artifactNode.containerNode.setUdf(udfEnum.udf, value)
            }
        }
    }

    String getDestinationStage(Analyte analyte){return null}

    Map getDefaultUdfs(){return null}

    void updateBean(OnboardingBean bean, Analyte analyte){
        bean.addAnalyte(analyte)
    }

    static List<String> lookupSamplesByPmoSampleId(NodeManager clarityNodeManager, Object pmoSampleId) {
        List<String> sampleIds = []
        List<SampleParameterValue> sampleParameters = []
        sampleParameters << SampleParameter.NAME.setValue(pmoSampleId)
        Iterator<String> sampleIdIterator = new SampleIdIterator(clarityNodeManager.nodeConfig, sampleParameters)
        if (sampleIdIterator.hasNext()) {
            while (sampleIdIterator.hasNext()) {
                sampleIds << sampleIdIterator.next()
            }
        }
        return sampleIds
    }

    static Map<Object, ClaritySample> lookupSamplesByPmoSampleIds(NodeManager clarityNodeManager, List<Object> pmoSampleIds, Class sampleType = PmoSample.class) {
        List<String> sampleIds = []
        pmoSampleIds.each {
            sampleIds.addAll(lookupSamplesByPmoSampleId(it))
        }
        List<ClaritySample> samples = clarityNodeManager.getSampleNodes(sampleIds).collect { SampleFactory.sampleInstance(it)}
        Map<Object, ClaritySample> samplesMap = [:]
        samples.each { ClaritySample sample ->
            if(sample.class == sampleType)
                samplesMap[sample.pmoSample.pmoSampleId] = sample
        }
        return samplesMap
    }

    static Map<String, ClaritySample> lookupPmoSamplesByCollaboratorName(NodeManager clarityNodeManager, Collection<String> collabSampleNames) {
        List<String> sampleIds = collabSampleNames.collect {lookupPmoSampleByCollaboratorName(clarityNodeManager, it)}
        cacheSamples(sampleIds, clarityNodeManager)
        List<SampleNode> samples = clarityNodeManager.getSampleNodes(sampleIds)
        Map<String, ClaritySample> samplesMap = [:]
        samples.each { SampleNode sampleNode ->
            ClaritySample sample = SampleFactory.sampleInstance(sampleNode)
            samplesMap[sample.udfCollaboratorSampleName] = sample
        }
        return samplesMap
    }

    static String lookupPmoSampleByCollaboratorName(NodeManager clarityNodeManager, String collabSampleName) {
        String sampleQueryUrl = clarityNodeManager.nodeConfig.geneusUrl + 'samples?udf.' +
                URLEncoder.encode(ClarityUdf.SAMPLE_COLLABORATOR_SAMPLE_NAME.value, "UTF-8") + '=' +
                URLEncoder.encode(collabSampleName, "UTF-8")
        Iterator<String> sampleIdIterator = new GenericIdIterator(clarityNodeManager.nodeConfig, sampleQueryUrl, 'sample')
        Set<String> sampleIds = []
        while (sampleIdIterator.hasNext()) {
            sampleIds << sampleIdIterator.next()
        }
        if(!sampleIds)
            throw new RuntimeException("Sample with Collaborator Sample Name $collabSampleName does not exist in clarity.")
        if(sampleIds.size() > 1)
            throw new RuntimeException("More than one sample exist with Collaborator sample name $collabSampleName.")
        return sampleIds.first()
    }

    static Map<String, ContainerNode> lookUpFlowcellsByBarcode(NodeManager clarityNodeManager, Collection<String> flowCellBarcodes){
        List<ContainerNode> containerNodes = flowCellBarcodes.collect{clarityNodeManager.getContainerNodesByName(it)}.flatten()
        Map<String, ContainerNode> containersMap = [:]
        containerNodes.each { ContainerNode container ->
            if (container.containerTypeEnum == ContainerTypes.FLOW_CELL)
                containersMap[container.name] = container
        }
        return containersMap
    }

    static Analyte getAnalyteByLocation(NodeManager clarityNodeManager, ContainerLocation location){
        Analyte analyte = null
        ArtifactNode artifactNode = containerContents[location]
        if(!artifactNode) {
            ContainerNode flowCell = clarityNodeManager.getContainerNode(location.containerId)
            containerContents.putAll(flowCell.containerLocationArtifactNodes())
            artifactNode = containerContents[location]
        }
        if(artifactNode)
            analyte = AnalyteFactory.analyteInstance(artifactNode)
        return analyte
    }

    static List<ContainerNode> getClarityContainers(NodeManager clarityNodeManager, List<String> flowCellBarcodes){
        List<ContainerNode> newContainers = []
        def containerType = ContainerTypes.FLOW_CELL
        Map<String, ContainerNode> flowcells = lookUpFlowcellsByBarcode(clarityNodeManager, flowCellBarcodes)
            flowCellBarcodes.each { String flowCellBarcode ->
                ContainerNode containerNode = flowcells[flowCellBarcode]
                if(!containerNode)
                containerNode = clarityNodeManager.createContainer(containerType)
            newContainers << containerNode
        }
        newContainers
    }

    static Analyte lookUpPoolForLibraries(NodeManager clarityNodeManager, List<OnboardingBean> beans){
        List pools = beans.collect{ it.libraryPool }?.unique()
        if(pools)
            return pools[0]
        List<List<ProcessNode>> poolingProcesses = []
        Analyte library
        lookupLibrariesByCollaboratorName(clarityNodeManager, beans)
        beans.each{ OnboardingBean bean ->
            if(bean.libraryStock) {
                library = bean.libraryStock
                poolingProcesses << clarityNodeManager.lookupProcessNodesByInputArtifactIdAndProcessTypeName(bean.libraryStock.id, ProcessType.LP_POOL_CREATION.value)
            }
        }
        for(int i=1; i<poolingProcesses.size(); i++){
            poolingProcesses[0] = poolingProcesses[0].intersect(poolingProcesses[i])
        }

        if(poolingProcesses[0] && poolingProcesses[0][0]) {
            ArtifactNode pool = poolingProcesses[0][0].getOutputAnalytes(library.id)[0]
            if(beans[0].collaboratorPoolName) {
                List<ArtifactNode> allPools = poolingProcesses[0].collect { it.getOutputAnalytes(library.id)[0] }
                pool = allPools.find { it.getUdfAsString(ClarityUdf.ANALYTE_COLLABORATOR_POOL_NAME.udf) == beans[0].collaboratorPoolName }
            }
            return AnalyteFactory.analyteInstance(pool)
        }
        return null
    }

    static Map lookupLibrariesByCollaboratorName(NodeManager clarityNodeManager, List<OnboardingBean> onboardingBeans) {
        Map<OnboardingBean, List<String>> libraryIds = [:]
        for(OnboardingBean bean : onboardingBeans) {
            if(!bean.libraryStock)
                libraryIds[bean] = lookupLibraryByCollaboratorName(clarityNodeManager, bean)
        }
        Map<String, ArtifactNode> result = [:]
        Map<String, ArtifactNode> libraries = clarityNodeManager.getArtifactNodesMap(libraryIds.values().flatten())
        cacheAncestors(libraries.values(), clarityNodeManager)
        libraryIds.each { OnboardingBean bean, List<String> libIds ->
            List<Analyte> analytes = libIds.collect { AnalyteFactory.analyteInstance(libraries[it]) }
            if(analytes) {
                if (analytes.size() > 2 ) {
                    throw new RuntimeException("Multiple libraries ${analytes*.id.sort().join(',')} by name ${bean.getLabLibraryName()?.toString()} exist in clarity.")
                }
                else if(analytes.size() == 2) {
                    if(analytes.find{!it.isPacBio})
                        throw new RuntimeException("Multiple libraries ${analytes*.id.sort().join(',')} by name ${bean.getLabLibraryName()?.toString()} exist in clarity.")
                    else
                        result[bean.getLabLibraryName()] = analytes.find{it.containerType == ContainerTypes.TUBE.value}
                }
                else{
                    result[bean.getLabLibraryName()] = analytes[0]
                }
            }
        }
        return result
    }

    static Map lookupPoolsByCollaboratorName(NodeManager clarityNodeManager, List<OnboardingBean> onboardingBeans) {
        Map<OnboardingBean, List<String>> libraryIds = [:]
        for(OnboardingBean bean : onboardingBeans) {
            if(!bean.libraryPool)
                libraryIds[bean] = lookupLibraryByCollaboratorName(clarityNodeManager, bean, true)
        }
        Map<String, ArtifactNode> result = [:]
        Map<String, ArtifactNode> libraries = clarityNodeManager.getArtifactNodesMap(libraryIds.values().flatten())
        cacheAncestors(libraries.values(), clarityNodeManager)
        libraryIds.each { OnboardingBean bean, List<String> libIds ->
            List<Analyte> analytes = libIds.collect { AnalyteFactory.analyteInstance(libraries[it]) }
            if(analytes) {
                if (analytes.size() > 2 ) {
                    throw new RuntimeException("Multiple libraries ${analytes*.id.sort().join(',')} by name ${bean.collaboratorPoolName?.toString()} exist in clarity.")
                }
                else{
                    result[bean.collaboratorPoolName] = analytes[0]
                }
            }
        }
        return result
    }

    static List<String> lookupLibraryByCollaboratorName(NodeManager clarityNodeManager, OnboardingBean onboardingBean, boolean findPool = false) {
        String libraryName = findPool?onboardingBean.collaboratorPoolName:onboardingBean.getLabLibraryName()
        String udfName = findPool? ClarityUdf.ANALYTE_COLLABORATOR_POOL_NAME.value: ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME.value
        String artifactQueryUrl = clarityNodeManager.nodeConfig.geneusUrl + 'artifacts?type=Analyte' +
                '&udf.' +
                URLEncoder.encode(udfName, "UTF-8") + '=' +
                URLEncoder.encode(libraryName, "UTF-8")
        Iterator<String> artifactIdIterator = new GenericIdIterator(clarityNodeManager.nodeConfig, artifactQueryUrl, 'artifact')
        Set<String> artifactIds = []
        while (artifactIdIterator.hasNext()) {
            artifactIds << artifactIdIterator.next()
        }
        return artifactIds as List
    }

    static List<String> lookupSABySampleId(NodeManager clarityNodeManager, String jgiSampleId) {
        if(!clarityNodeManager || !jgiSampleId)
            return null
        String artifactQueryUrl = clarityNodeManager.nodeConfig.geneusUrl + 'artifacts?process-type=' +
                URLEncoder.encode(ProcessType.AC_ALIQUOT_CREATION.value, "UTF-8")+
                '&name=' +
                URLEncoder.encode('aq '+ jgiSampleId, "UTF-8")
        Iterator<String> artifactIdIterator = new GenericIdIterator(clarityNodeManager.nodeConfig, artifactQueryUrl, 'artifact')
        Set<String> artifactIds = []
        while (artifactIdIterator.hasNext()) {
            artifactIds << artifactIdIterator.next()
        }
        return artifactIds as List
    }

//    static Map<String, Analyte> lookupSAsBySampleId(NodeManager clarityNodeManager, Map<String, Long> jgiSampleIdSowId)  {
//        Map<String, List<String>> sampleSALimsId = [:]
//        jgiSampleIdSowId.each { String jgiSampleId, Long sowId ->
//            sampleSALimsId[jgiSampleId] = lookupSABySampleId(clarityNodeManager, jgiSampleId)
//        }
//        List<String, ArtifactNode> sas = clarityNodeManager.getArtifactNodesMap(sampleSALimsId.values().flatten())
//        Map<String, Analyte> sampleSAMap = null
//        jgiSampleIdSowId.each { String jgiSampleId, Long sowId ->
//            List<String> saLimsIds = sampleSALimsId[jgiSampleId]
//            List<Analyte> analytes = saLimsIds.collect { AnalyteFactory.analyteInstance(sas[it]) }
//            sampleSAMap[jgiSampleId] = analytes.find { it.claritySample.sowItemId == sowId}
//        }
//        return sampleSAMap
//    }

    List<Analyte> performAdditionalActions(List<Analyte> analytes, Long contactId){
        return analytes
    }

    static void cacheAncestors(Collection<ArtifactNode> analytes, NodeManager nodeManager) {
        List<String> artifactIds = analytes.collect { it.parentAnalyteIds}.flatten()
        while(artifactIds) {
            List<ArtifactNode> artifactNodes = nodeManager.getArtifactNodes(artifactIds)
            artifactIds = artifactNodes.collect { it.parentAnalyteIds }.flatten()
        }
        List<String> sampleIds = analytes.collect { it.sampleIds }.flatten()
        cacheSamples(sampleIds, nodeManager)
    }

    static void cacheSamples(Collection<String> sampleIds, NodeManager nodeManager) {
        List<SampleNode> samples = nodeManager.getSampleNodes(sampleIds)
        List<String> rootSampleIds = samples.collect { it.getUdfAsString(ClarityUdf.SAMPLE_LIMSID.udf) }
        rootSampleIds = rootSampleIds.findAll { it }
        if(rootSampleIds)
            samples.addAll(nodeManager.getSampleNodes(rootSampleIds))
        List<ArtifactNode> artifactNodes = nodeManager.getArtifactNodes(samples.collect{ it.artifactId})
        nodeManager.getContainerNodes(artifactNodes.collect {it.containerId})
    }

    abstract List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans)

    abstract ContainerTypes getContainerType(Analyte parentAnalyte)

    abstract Stage getWorkflowStage(ContainerTypes containerTypes)

    abstract void checkExistingAnalytes(List<OnboardingBean> beans)

    abstract Analyte getAnalyte(OnboardingBean bean)

    abstract Class getAnalyteType()
}
