package gov.doe.jgi.pi.pps.clarity.scripts.place_work

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.PostProcessService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/24/16.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 *
 * Run “On Hold”/“Needs Attention” process programmatically on the batch of analytes designated by the sheet
 * Place the analytes in “Release from Hold”/“Release from Needs Attention” queue
 */
class PlaceWorkRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(PlaceWorkRouteToWorkflow.class)

    PostProcessService taskGenerationService
    StatusService statusService

    void execute() {
        logger.info "${this.class} execute()"
        process.nodeManager.readOnly = false
        taskGenerationService = BeanUtil.getBean(PostProcessService.class)
        statusService = BeanUtil.getBean(StatusService.class)

        boolean isNeedsAttentionProcess = process.processNode.processType == ProcessType.PLACE_IN_NEEDS_ATTENTION.value
        Stage stage = isNeedsAttentionProcess ? Stage.NEEDS_ATTENTION : Stage.ON_HOLD
        Stage stageToMove = isNeedsAttentionProcess ? Stage.RELEASE_NEEDS_ATTENTION : Stage.RELEASE_HOLD
        taskGenerationService.postInputProcess(process, stage, process.analytes*.artifactNode)
        process.analytes*.validateIsInQueuedStages()
        process.unassignQueuedAnalytes(process.analytes)
        process.routeAnalytes(stageToMove, process.analytes)
    }

}