package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean

interface OnboardingAdapter {
    static final String EXTERNAL_UDF_VALUE = 'Y'

    static final String ONBOARDING_DETAILS='Onboarding Details'
    static final List<ClarityWorkflow> WORKFLOWS_FOR_VALIDATION = [ClarityWorkflow.LIBRARY_QUANTIFICATION, ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_ANNEALING]

    List<String> validateOnboardingData()

    void mergeDataToClarity()

    void archiveOnboardingResult(String artifactId)

    List<Analyte> getAnalytes()

    String getAction()

    void prepareUdfsMap(OnboardingBean bean)
}