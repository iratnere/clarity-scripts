package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Bean corresponding to the Dya=namic Key-Value sections per plate on the  SM Workspace sheet
 * Created by lvishwas on 4/30/2015.
 */
class SMWorkspaceSampleQCPassFailKeyValueBean {
    @FieldMapping(header = 'Plate Barcode', cellType = CellTypeEnum.STRING)
    public def plateBarcode
    @FieldMapping(header = 'Plate LIMSID', cellType = CellTypeEnum.STRING)
    public def plateLimsId
    @FieldMapping(header = 'Plate Name', cellType = CellTypeEnum.STRING)
    public def plateName
    @FieldMapping(header = '# samples', cellType = CellTypeEnum.NUMBER)
    public def samplesCount
    @FieldMapping(header = '# passed samples', cellType = CellTypeEnum.FORMULA)
    public def passesSamplesCount// Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populateKeyValueSection()
    @FieldMapping(header = 'Plate QC Pass Percentage', cellType = CellTypeEnum.NUMBER)
    public def plateQCPassPercentage// Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populateKeyValueSection()
    @FieldMapping(header = 'Plate QC Result', cellType = CellTypeEnum.FORMULA)
    public def plateQCResult

    SampleAnalyte firstSampleOnPlate
    static final int PLATE_QC_PASS_PERCENTAGE = 50

    String validateBean(){
        if(!plateQCResult)
            return "Plate QC Result not set for the plate ${firstSampleOnPlate.containerName}.\n"
        return ""
    }

    void updateContainerUdfs(){
        //Sample QC Result <-- Plate QC Result
        firstSampleOnPlate.claritySample.udfContainerSampleQcResult = plateQCResult
    }

    void populateBean(ContainerNode plate){
        plateBarcode = plate?.name
        plateLimsId = plate?.id
        ClaritySample sample = SampleFactory.sampleInstance(plate.contentsArtifactNodes.first().sampleNode)
        plateName = sample.containerUdfLabel
        samplesCount = plate?.contentsArtifactNodes?.size()
        plateQCPassPercentage = PLATE_QC_PASS_PERCENTAGE
    }
}
