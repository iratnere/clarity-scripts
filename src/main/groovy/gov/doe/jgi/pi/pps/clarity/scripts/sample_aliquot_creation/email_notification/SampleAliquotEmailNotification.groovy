package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by datjandra on 3/27/2015.
 */
class SampleAliquotEmailNotification extends EmailNotification {
    static final String SUBJECT = 'Aliquot failed in aliquot creation'
    static final String CC_EMAIL_GROUP = 'jgi-its-abandon-aliquot@lbl.gov' //PPS-4849 - update mailing lists
    static final String TEMPLATE_FILE_NAME = 'sa_abandon_sample_aliquot'

    def getToList(List<EmailDetails> emailDetailsList){
        def to = emailDetailsList.first().sequencingProjectManagerId
        return to
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return SUBJECT
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new SampleAliquotAbandonEmailDetails(it)}
    }

    @Override
    def getBinding(List<EmailDetails> emailDetailsList) {
        return [samples: getSampleList(emailDetailsList), abandonDate: new Date()]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}