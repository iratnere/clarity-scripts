package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.PoolingDualIndexesByDOP

class PoolingIlluminaDualIndexes extends PoolingDualIndexesByDOP{
    PoolingIlluminaDualIndexes(List<LibraryInformation> members) {
        super(members)
    }
}
