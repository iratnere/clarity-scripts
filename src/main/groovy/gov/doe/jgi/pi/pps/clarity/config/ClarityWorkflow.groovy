package gov.doe.jgi.pi.pps.clarity.config

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseSensitive
import gov.doe.jgi.pi.pps.util.util.OnDemandCache

enum ClarityWorkflow {
    SUPPLY_CHAIN_MANAGEMENT('Supply Chain Management'),
    RECEIVE_SAMPLES('Receive Samples'),
	SAMPLE_RECEIPT('Sample Receipt Workflow'),
    SAMPLE_QC_DNA('Sample QC_DNA'),
    SAMPLE_QC_RNA('Sample QC_RNA'),
    SOW_ITEM_QC('SOW Item QC'),
	SAMPLE_QC_METABOLOMICS('SM Sample QC Metabolomics'),
	SAMPLE_FRACTIONATION('SM Sample Fractionation'),
    ALIQUOT_CREATION_DNA('Sample Aliquot DNA'),
    ALIQUOT_CREATION_RNA('Sample Aliquot RNA'),

	LC_PACBIO_MULTIPLEXED_2KB_AMPLICON_TUBES('LC PacBio Multiplexed 2kb Amplicon, Tubes'),
	LC_PACBIO_MULTIPLEXED_3KB_TUBES('LC PacBio Multiplexed 3kb, Tubes'),
	LC_PACBIO_MULTIPLEXED_10KB_TUBES('LC PacBio Multiplexed 10kb, Tubes'),
	LC_PACBIO_MULTIPLEXED_PRODUCTION_RND_TUBES('LC PacBio Multiplexed Production RnD, Tubes'),
	LC_PACBIO_MULTIPLEXED_10KB_BLUE_PIPPIN_TUBES('LC PacBio Multiplexed >10kb w/ Blue Pippin Size Selection, Tubes'),
	LC_NOT_JGI('LC Not JGI'),

    SIZE_SELECTION('Size Selection'),
    PLATE_TO_TUBE_TRANSFER('Plate To Tube Transfer'),
	LIBRARY_QUANTIFICATION('Library Quantification Workflow'),
	POOL_CREATION('Pool Creation Workflow'),

	PACBIO_SEQUEL_LIBRARY_ANNEALING('PacBio Sequel Library Annealing'),
	PACBIO_SEQUEL_LIBRARY_BINDING('PacBio Sequel Library Binding'),
	PACBIO_SEQUEL_SEQUENCING_PREP('PacBio Sequel Sequencing Prep'),
	PACBIO_SEQUEL_II_SEQUENCING_PREP('PacBio Sequel II Sequencing Prep'),
	PACBIO_SEQUEL_SEQUENCING_COMPLETE('PacBio Sequel Sequencing Complete'),
	PACBIO_SEQUEL_MAGBEAD_CLEANUP('PacBio Sequel MagBead Cleanup'),

//	SEQUENCING_ILLUMINA_HISEQ_1TB('SQ Sequencing Illumina HiSeq-1TB'),//PPS-5150: inactivate discontinued run modes
	SEQUENCING_ILLUMINA_HISEQ_RAPID('SQ Sequencing Illumina HiSeq-Rapid'),
	SEQUENCING_ILLUMINA_MISEQ('SQ Sequencing Illumina MiSeq'),
    SEQUENCING_ILLUMINA_NOVASEQ('SQ Sequencing Illumina NovaSeq'),
//	CLUSTERGEN_ILLUMINA_HISEQ_1TB('SQ Cluster Generation Illumina HiSeq-1TB'),//PPS-5150: inactivate discontinued run modes

	ABANDON_QUEUE('Abandon Queue'),
	ABANDON_WORK('Abandon Work'),
	ON_HOLD_WORKFLOW('On Hold'),
	RELEASE_HOLD_WORKFLOW('Release from Hold'),
	NEEDS_ATTENTION_WORKFLOW('Needs Attention'),
	RELEASE_NEEDS_ATTENTION_WORKFLOW('Release from Needs Attention'),

	REQUEUE_LIBRARY_CREATION('Requeue for Library Creation'),
	REQUEUE_LIBRARY_ANNEALING('Requeue for Library Annealing'),

    REQUEUE_LIBRARY_BINDING('Requeue for Library Binding'),
	AUTOMATIC_REQUEUE_LIBRARY_CREATION('Automatic Requeue for Library Creation'),
	AUTOMATIC_REQUEUE_SEQUENCING('Automatic Requeue for Sequencing'),
	AUTOMATIC_REQUEUE_SEQUENCE_ANALYSIS('Automatic Requeue for Sequencing Analysis'),
	AUTOMATIC_REQUEUE_QPCR('Automatic Requeue for qPCR'),
	AUTOMATIC_POOL_CREATION_WORKFLOW('Automatic Pool Creation Workflow'),
	AUTOMATIC_ROUTE_TO_WORKFLOW('Automatic Route to Workflow'),
	AUTOMATIC_SOW_ITEM_QC_WORKFLOW('Automatic SOW Item QC'),

	SHIP_OFFSITE('Ship Off-site'),
	TRASH('Trash'),
	ON_SITE('On Site'),
	ADJUST_MASS('Adjust Mass'),
	ADJUST_MOLARITY('Adjust Molarity'),
	ADJUST_VOLUME('Adjust Volume'),

	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_LIBRARY_CREATION('Automatic Sequence QC Requeue for Library Creation'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_ANNEALING('Automatic Sequence QC Requeue for PacBio Library Annealing'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_BINDING('Automatic Sequence QC Requeue for PacBio Library Binding'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_POOL_CREATION('Automatic Sequence QC Requeue for Pool Creation'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_QPCR('Automatic Sequence QC Requeue for qPCR'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_SEQUENCING('Automatic Sequence QC Requeue for Sequencing'),
	AUTOMATIC_SOWITEM_REQUEUE_FOR_POOL_CREATION('Automatic SowItem Requeue for Pool Creation'),
	AUTOMATIC_SOWITEM_REQUEUE_FOR_SEQUENCING('Automatic SowItem Requeue for Sequencing'),

	ONBOARD_SEQUENCING_FILES('Onboard Sequencing Files'),
	PLACE_IN_NEEDS_ATTENTION('Place in Needs Attention'),
	PLACE_ON_HOLD('Place on Hold'),
	PRINT_LABELS('Print Labels'),
	REQUEUE_FOR_QC('Requeue for QC'),
	SEQUENCE_QC_REQUEUE_FOR_LIBRARY_CREATION('Sequence QC Requeue for Library Creation'),
	SEQUENCE_QC_REQUEUE_FOR_QPCR('Sequence QC Requeue for qPCR'),
    SEQUENCE_QC_REQUEUE_FOR_SEQUENCING('Sequence QC Requeue for Sequencing')

    final String value

	private final OnDemandCache cachedStages = new OnDemandCache<>()
	List<Stage> getStages() {
		cachedStages.fetch {
			List<Stage> stageList = []
			Stage.values().each { Stage stage ->
				if (stage.workflow.is(this)) {
					stageList << stage
				}
			}
			stageList
		}
	}

	ClarityWorkflow(String value, SequencerModelTypeCv sequencerModel = null) {
		this.value = value
	}

	private static final EnumConverterCaseSensitive<ClarityWorkflow> converter = new EnumConverterCaseSensitive(ClarityWorkflow)


	WorkflowNode getWorkflowNode() {
		BeanUtil.nodeManager.getWorkflowNodeByName(value)
	}

	String getUri() {
		workflowNode?.workflowUri
	}

    Integer getId() {
		workflowNode?.workflowId
    }

	static ClarityWorkflow toEnum(value) {
		converter.toEnum(value)
	}

	String toString() {
		value
	}

	Boolean getIsPacBioSequel() {
		name().startsWith('PACBIO_SEQUEL')
	}

	Boolean getIsPacBio() {
		name().startsWith('PACBIO')
	}

	static List<ClarityWorkflow> sampleAliquotCreationQueues() {
		[ALIQUOT_CREATION_DNA, ALIQUOT_CREATION_RNA]
	}

}