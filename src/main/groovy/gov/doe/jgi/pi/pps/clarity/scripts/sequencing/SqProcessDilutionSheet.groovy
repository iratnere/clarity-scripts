package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 3/2/15.
 * Clarity Illumina MiSeq, NextSeq, HiSeq Support Requirements
 * https://docs.google.com/document/d/15g6IcLuFzb-1Q8LtFCdfukaT2f2l0m32j9b8yo3_51Q/edit#
 * Clarity Illumina NovaSeq Support Requirements
 * https://docs.google.com/document/d/1CmDCgNkQwyHQRqk4Gx3LgeWzNYiPuJ6X9X6BKPffq9w/edit
 * Clarity Illumina HiSeq 2000, 1T Requirements
 * https://docs.google.com/document/d/1DhaLc-e3zebpmqOYUSMp19CcsvrP5QPlB5iFlsZNgAs/edit#heading=h.7bnr80mxwz1b
 */
class SqProcessDilutionSheet extends ActionHandler {

    SequencerType sequencerType

    void execute() {
        sequencerType = process.sequencerType
        validateOutputAnalytes()
        updateOutputAnalytes()
        process.setCompleteStage()
    }

    def validateOutputAnalytes() {
        process.outputAnalytes.each { validateOutputAnalyte(it as ClarityPhysicalRunUnit) }
    }

    def updateOutputAnalytes() {
        process.outputAnalytes[0].containerNode.name = process.udfProcessFlowcellBarcode
        process.outputAnalytes.each {
            ClarityPhysicalRunUnit physicalRunUnit = it as ClarityPhysicalRunUnit
            def tableBean = process.findTableBean(physicalRunUnit, sequencerType.validateLane)
            tableBean.updatePhysicalRunUnit(physicalRunUnit)
        }
    }

    void validateOutputAnalyte(ClarityPhysicalRunUnit physicalRunUnit) {
        Boolean systemQcFlag = physicalRunUnit.systemQcFlag
        String failureMode = getFailureMode(physicalRunUnit)
        if (systemQcFlag && failureMode) {
            process.postErrorMessage("""
$physicalRunUnit / $physicalRunUnit.name:
The '$failureMode' Failure Mode is set to the passed analyte.
""")
        }
        if (!systemQcFlag && !failureMode) {
            process.postErrorMessage("""
$physicalRunUnit / $physicalRunUnit.name:
The Failure Mode udf is not set.
""")
        }
    }

    String getFailureMode(ClarityPhysicalRunUnit physicalRunUnit) {
        //PPS-5150: inactivate discontinued run modes
        return physicalRunUnit.sequencingFailureMode
    }

}
