package gov.doe.jgi.pi.pps

import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.common.LowerCaseWithHyphensStrategy
import gov.doe.jgi.pi.pps.util.ClaritySettings
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.context.ApplicationPidFileWriter
import org.springframework.boot.web.context.WebServerPortFileWriter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.context.WebApplicationContext

@SpringBootApplication(exclude = [HibernateJpaAutoConfiguration.class])
class Application {

    static void main(String[] args) {
        //SpringApplication.run(Application, args)
        SpringApplication app = new SpringApplication(Application.class)
        app.addListeners(new ApplicationPidFileWriter())
        app.addListeners(new WebServerPortFileWriter())
        app.run(args)
    }

    @Autowired
    ClaritySettings clarity

    @Bean
    Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
        builder.propertyNamingStrategy = new LowerCaseWithHyphensStrategy()
        builder
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST)
    NodeManager nodeManager() {
        new NodeManager(clarity.toNodeConfig())
    }


}
