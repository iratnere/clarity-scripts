package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcPlateTableBean {
	@FieldMapping(header = 'Well', cellType = CellTypeEnum.STRING, isRowHeader = true)
	public String well
	@FieldMapping(header = 'Library LIMS ID', cellType = CellTypeEnum.STRING)
	public String libraryLimsId
	@FieldMapping(header = 'Library Name', cellType = CellTypeEnum.STRING)
	public String libraryName
	@FieldMapping(header = 'Index', cellType = CellTypeEnum.STRING)
	public def libraryIndexName
	@FieldMapping(header='Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal aliquotMass
	@FieldMapping(header='Library Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryConcentration
	@FieldMapping(header='Library Template Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryTemplateSize
	@FieldMapping(header='Library Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryVolume
	@FieldMapping(header='Library Molarity (pM)', cellType = CellTypeEnum.FORMULA)
	public BigDecimal libraryMolarity
	@FieldMapping(header='Pool Number', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolNumber
/*
	@FieldMapping(header = 'LC - Notes', cellType = CellTypeEnum.STRING)
	public String lcNotes

	@FieldMapping(header='Library QC (Pass/Fail)', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryQcResult
	@FieldMapping(header='Library Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryFailureMode
*/

    Boolean passed

	def validate() {
        //passed == library stock 'Pass' as analyte level
        def errorMsg = StringBuilder.newInstance()
		if (!passed) {
			return  null
		}
        if (!libraryConcentration) {
			errorMsg << 'unspecified Library Concentration (ng/ul)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryTemplateSize) {
			errorMsg << 'unspecified Library Template Size (bp)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryVolume) {
			errorMsg << 'unspecified Library Volume (ul)' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (!libraryMolarity) {
			errorMsg << "unspecified Library Molarity (pM)" << ClarityProcess.WINDOWS_NEWLINE
		}
        return buildErrorMessage(errorMsg)
    }

    def buildErrorMessage(def errorMsg) {
        def startErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Plate Info Section $ClarityProcess.WINDOWS_NEWLINE"
		startErrorMsg << "Library Stock($libraryName):$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

    def validateLibraryStockInPool(NodeManager nodeManager = BeanUtil.nodeManager) {
        def errorMsg = StringBuilder.newInstance()
        if (poolNumber && !libraryLimsId) {
            errorMsg << "Please check the Library LIMS ID column for the library pool number '${poolNumber as BigInteger}' $ClarityProcess.WINDOWS_NEWLINE"
        }
        try {
            if (libraryLimsId)
                nodeManager.getArtifactNode(libraryLimsId)
        } catch (Exception e) {
            //errorMsg << e.message
            errorMsg << "invalid Library LIMS ID '$libraryLimsId'"
        }
        return buildErrorMessage(errorMsg)
    }

	void populateRequiredFields(ClarityProcess process, int passCount){
		libraryConcentration = 1
		libraryTemplateSize = 1
		libraryVolume = 10
	}

	boolean getRequiresIndex(){
		return !libraryIndexName
	}

	void setIndexName(String indexName){
		libraryIndexName = indexName
	}
}
