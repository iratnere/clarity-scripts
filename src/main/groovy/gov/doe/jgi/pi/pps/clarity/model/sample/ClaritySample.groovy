package gov.doe.jgi.pi.pps.clarity.model.sample

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.project.ProjectFactory
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNodeInterface
import gov.doe.jgi.pi.pps.util.exception.WebException

/**
 * Created by dscott on 4/22/2014.
 */
class ClaritySample implements Comparable<ClaritySample> {

    private SampleNodeInterface sampleNode

    static final String EXTERNAL_Y = 'Y'

    static final String GROUP_FAILURE = 'Group Failure'

    ClaritySample(SampleNodeInterface sampleNode) {
        this.sampleNode = sampleNode
    }

    String getId() {
        return sampleNode.id
    }

    int compareTo(ClaritySample other) {
        return this.sampleNode.id.compareTo(other?.sampleNode?.id)
    }

    boolean equals(other) {
        if (is(other)) return true
        if (!(other instanceof ClaritySample)) return false
        return sampleNode?.id?.equals(((ClaritySample) other).sampleNode?.id)
    }

    int hashCode() {
        return (sampleNode?.id?.hashCode())?:0
    }

    PmoSample getPmoSample() {
        return null
    }

    @Override
    String toString() {
        return "${this.class.simpleName}(${sampleNode.id})"
    }

    SampleNodeInterface getSampleNode() {
        return sampleNode
    }

    String getName() {
        return sampleNode.name
    }

    void setName(String sampleName) {
        sampleNode.name = sampleName
    }

    String getContainerName() {
        return sampleNode.getContainerName()
    }

    String getContainerLimsId() {
        return sampleNode.artifactNode?.containerId
    }

    ArtifactNode getSampleArtifactNode() {
        return sampleNode.getArtifactNode()
    }

    SampleAnalyte getSampleAnalyte() {
        (SampleAnalyte) AnalyteFactory.analyteInstance(sampleNode.artifactNode)
    }

    String getLocation(){
        return sampleNode.artifactNode.location
    }

    SequencingProject getSequencingProject(){
        return ProjectFactory.projectInstance(sampleNode.projectNode)
    }

    Boolean getIsDnaProject() {
        return sequencingProject.udfMaterialCategory?.equalsIgnoreCase(MaterialCategoryCv.DNA.materialCategory)
    }

    BigDecimal getUdfVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_VOLUME_UL.udf)
    }

    void setUdfVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_VOLUME_UL.udf, volume)
    }

    String getUdfMaterialType() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_MATERIAL_TYPE.udf)
    }

    void setUdfMaterialType(String materialType) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_MATERIAL_TYPE.udf, materialType)
    }

    String getContainerUdfLabel(){
        return sampleNode.artifactNode?.containerNode?.getUdfAsString(ClarityUdf.CONTAINER_LABEL.udf)
    }

    void setContainerUdfLabel(String label){
        sampleNode.artifactNode.containerNode.setUdf(ClarityUdf.CONTAINER_LABEL.udf, label)
    }

    void setUdfNotes(String udfSmNotes) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_NOTES.udf, udfSmNotes)
    }

    String getUdfNotes() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_NOTES.udf)
    }

    String getUdfSmInstructions(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SM_INSTRUCTIONS.udf)
    }

    String setUdfSmInstructions(String smInstructions){
        return sampleNode.setUdf(ClarityUdf.SAMPLE_SM_INSTRUCTIONS.udf, smInstructions)
    }

    String getUdfLcInstructions(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_LC_INSTRUCTIONS.udf)
    }

    String setUdfLcInstructions(String lcInstructions){
        return sampleNode.setUdf(ClarityUdf.SAMPLE_LC_INSTRUCTIONS.udf, lcInstructions)
    }

    String getUdfSqInstructions(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_SQ_INSTRUCTIONS.udf)
    }

    String setUdfSqInstructions(String sqInstructions){
        return sampleNode.setUdf(ClarityUdf.SAMPLE_SQ_INSTRUCTIONS.udf, sqInstructions)
    }

    Integer getRequiredQcType() {
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_REQUIRED_QC_TYPE.udf)
    }

    void setRequiredQcType(Integer qcType) {
        QcTypeCv qcTypeCv = QcTypeCv.get(qcType)
        sampleNode.setUdf(ClarityUdf.SAMPLE_REQUIRED_QC_TYPE.udf, qcTypeCv.qcType)
    }

    String getUdfExternal() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_EXTERNAL.udf)
    }

    void setUdfExternal(String value) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_EXTERNAL.udf, value)
    }

    boolean getIsExternal() {
        return EXTERNAL_Y == udfExternal
    }

    boolean getIsItag() {
        throw new WebException([code:'ClaritySample.getIsItag().notImplemented', args:[this]], 422)
    }

    boolean getIsExomeCapture() {
        throw new WebException([code:'ClaritySample.getIsExomeCapture().notImplemented', args:[this]], 422)
    }

    boolean getIsPacBio() {
        throw new WebException([code:'ClaritySample.getIsPacBio().notImplemented', args:[this]], 422)
    }

    boolean getIsIllumina() {
        throw new WebException([code:'ClaritySample.getIsIllumina().notImplemented', args:[this]], 422)
    }

    boolean getIsSmallRna() {
        throw new WebException([code:'ClaritySample.getIsSmallRna().notImplemented', args:[this]], 422)
    }

    boolean getIsInternalSingleCell() {
        throw new WebException([code:"Clarity Sample has an invalid sample class: 'PmoSample'. Expected class: 'ScheduledSample'.", args:[this]], 422)
    }

    boolean getIsDapSeq() {
        throw new WebException([code:"Clarity Sample has an invalid sample class: 'PmoSample'. Expected class: 'ScheduledSample'.", args:[this]], 422)
    }

    LibraryCreationQueueCv getLibraryCreationQueue() {
        throw new WebException([code:'ClaritySample.getLibraryCreationQueue().notImplemented', args:[this.toString()]], 422)
    }

    String getPmoSampleArtifactLimsId() {
        throw new WebException([code:'ClaritySample.getPmoSampleArtifactLimsId().notImplemented', args:[this.toString()]], 422)
    }

    void passOnGroupFailure(){
        throw new WebException([code:'ClaritySample.passOnGroupFailure().notImplemented', args:[this.toString()]], 422)
    }

    boolean getPassedQc(){
        throw new WebException([code:'ClaritySample.getPassedQc().notImplemented', args:[this.toString()]], 422)
    }
}