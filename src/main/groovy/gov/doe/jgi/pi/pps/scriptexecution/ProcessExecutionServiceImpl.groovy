package gov.doe.jgi.pi.pps.scriptexecution

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity.model.researcher.ResearcherFactory
import gov.doe.jgi.pi.pps.clarity.scripts.services.RoutingService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.step.EppTrigger
import gov.doe.jgi.pi.pps.util.util.RequestCash
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.context.WebApplicationContext

import java.util.concurrent.ConcurrentHashMap

@Service
@Transactional
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
class ProcessExecutionServiceImpl implements ProcessExecutionService {

    static final Logger logger = LoggerFactory.getLogger(ProcessExecutionServiceImpl.class)

    @Autowired
    RequestCash requestCash

    @Autowired
    NodeManager nodeManager

    @Autowired
    RoutingService routingService

    @Autowired
    ScriptResponseModel responseModel

    @Override
    ScriptResponseModel processScriptParams(ScriptParams scriptParams) {
        String processId = scriptParams.options.processId
        assert processId
        ProcessNode processNode = nodeManager.getProcessNode(processId)
        assert processNode
        ClarityProcess clarityProcess = ProcessFactory.processInstance(processNode)
        assert clarityProcess
        Researcher researcher = ResearcherFactory.researcherForLimsId(nodeManager, processNode.technicianId)
        if (researcher) {
            requestCash.with {
                if (!data)
                    data = new ConcurrentHashMap<>()
                Long submittedBy = researcher.contactId
                if (submittedBy)
                    data.submittedBy = submittedBy
            }
        }
        logger.info "$scriptParams.options"
        logger.info "process type: [${processNode.processType}]"
        logger.info "ClarityProcess: ${clarityProcess}"
        StepConfigurationNode stepConfigurationNode = processNode.stepsNode?.stepConfigurationNode
        EppTrigger completeTrigger = stepConfigurationNode.completeTrigger
        EppTrigger beginningTrigger = stepConfigurationNode.beginningTrigger
        logger.info "StepConfigurationNode\n${stepConfigurationNode.nodeString}"

        if (stepConfigurationNode.eppTriggers && !completeTrigger) {
            throw new RuntimeException("clarity configuration error: step configuration [${stepConfigurationNode.url}] has triggers but no complete trigger")
        }
        if (stepConfigurationNode.eppTriggers && !beginningTrigger) {
            throw new RuntimeException("clarity configuration error: step configuration [${stepConfigurationNode.url}] has triggers but no beginning trigger")
        }
        String action = scriptParams.options.action
        assert action
        ActionHandler actionHandler = clarityProcess.getActionHandler(action)
        if (!actionHandler) {
            throw new RuntimeException("no handler registered for process type [${processNode.processType}], " +
                    "process class [${clarityProcess.class.name}], and action [${action}]")
        }
        logger.info "ActionHandler class: \"${actionHandler.class.simpleName}\""
        logger.info "Process class: \"${actionHandler.process.class.simpleName}\""
        Boolean lastAction = actionHandler.lastAction
        if (completeTrigger) {
            logger.info "Complete Trigger name ${completeTrigger.name}"
            logger.info "Action handler name ${actionHandler.action}"
            lastAction = completeTrigger.name == actionHandler.action
        }

        if (!lastAction) {
            nodeManager.readOnly = true
            List<ArtifactNodeInterface> outputArtifactNodes = clarityProcess.outputAnalytes*.artifactNode
            List<ContainerNode> outputContainer = nodeManager.getContainerNodes(outputArtifactNodes*.containerId)
            outputArtifactNodes*.readOnly = false
            outputContainer*.readOnly = false
        }

        scriptParams.options.with {
            actionHandler.stepUrl = stepUrl
            actionHandler.fileIds = fileIds as List
        }

        logger.info "Starting action ${actionHandler.class.name}"
        actionHandler.doExecute()
        nodeManager.httpPutDirtyNodes()
        logger.info "Is last action handler: $lastAction"
        if (lastAction) {
            logger.info "Submitting all routing requests..."
            routingService.submitRoutingRequests(clarityProcess.routingRequests)
        }

        ScriptResponseData responseData = new ScriptResponseData(researcher: researcher,
                actionHandler: actionHandler.class.simpleName,
                processClass: actionHandler.process.class.simpleName,
                lastAction: lastAction
        )

        assert responseModel
        responseModel.responseData = responseData
        responseData.lastAction = lastAction
        responseModel.lastAction = lastAction
        responseModel
    }
}
