package gov.doe.jgi.pi.pps.clarity.scripts.services

import doe.jgi.pi.pps.dapseq.couch.CouchDb
import doe.jgi.pi.pps.dapseq.couch.CouchDbResponse
import doe.jgi.pi.pps.dapseq.parser.DapSeqJsonSubmission
import duncanscott.org.groovy.utils.json.util.DateUtil
import duncanscott.org.groovy.utils.json.util.JsonUtil
import gov.doe.jgi.pi.pps.util.PpsException
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import gov.doe.jgi.pi.pps.util.util.RequestCash
import groovy.util.logging.Slf4j
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.apache.http.HttpStatus
import org.json.simple.JSONValue
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.text.DateFormat
import java.text.SimpleDateFormat

@Slf4j
@Service
class DapSeqJsonRetrievalService {

    @Value("\${couchdb.dapseq.url}")
    private String couchDbUrl

    @Value("\${couchdb.dapseq.username}")
    private String username

    @Value("\${couchdb.dapseq.password}")
    private String password



    CouchDb getCouchDb() {
        return new CouchDb(couchDbUrl,username,password)
    }

    CouchDbResponse getSchemas() {
        String designDocument = 'schema'
        String viewName = 'schemas'
        Map params = ['descending':true]
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        if (couchDbResponse.success) {
            return couchDbResponse
        }
        throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
    }

    private CouchDbResponse extractResponseForId(String designDocument, String viewName, Map params, String notFoundErrorMessage = 'nothing found') {
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        if (couchDbResponse.success) {
            String docId = couchDbResponse.json.rows?.find()?.id
            if (docId) {
                CouchDbResponse docResponse = couchDb.get(docId)
                if (docResponse.success) {
                    return  docResponse
                }
                throw new WebException("error retrieving document ${docResponse}", docResponse.statusCode)
            }
            throw new WebException(notFoundErrorMessage, 404)
        }
        throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
    }

    private CouchDbResponse getSchemaForId(String schemaId) {
        String designDocument = 'schema'
        String viewName = 'schema-id'
        Map params = ['key': JSONValue.toJSONString(schemaId)]
        extractResponseForId(designDocument, viewName, params, "no schema found for \$id [${schemaId}]")
    }


    CouchDbResponse getLatestSchema() {
        String designDocument = 'schema'
        String viewName = 'schemas'
        Map params = ['descending':true, 'limit':1]
        extractResponseForId(designDocument, viewName, params, 'no schema registered')
    }


    void retrieveSchemas() {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        CouchDbResponse couchDbResponse = schemas
        requestCash.statusCode = couchDbResponse.statusCode
        requestCash.couchdbResponse = JsonUtil.toJsonArray(couchDbResponse.json.rows)
    }


    void retrieveSchemaForId(String schemaId) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        CouchDbResponse couchDbResponse = getSchemaForId(schemaId)
        requestCash.statusCode = couchDbResponse.statusCode
        requestCash.couchdbResponse = JsonUtil.toJsonObject(couchDbResponse.json)
    }


    void retrieveLatestSchema() {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        CouchDbResponse couchDbResponse = latestSchema
        requestCash.statusCode = couchDbResponse.statusCode
        requestCash.couchdbResponse = JsonUtil.toJsonObject(couchDbResponse.json)
    }


    JSONArray retrieveViewRowsSortedByInsertDate(String designDocument, String viewName, String key) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONObject transactionParams = new JSONObject()
        requestCash.couchdbQueries = transactionParams

        JSONArray json = new JSONArray()
        CouchDb couchDb = getCouchDb()
        Map params = ['key': JSONValue.toJSONString(key)]
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        transactionParams["${viewName}"] = couchDbResponse.toJson()

        if (!couchDbResponse.success) {
            throw new WebException("error querying view ${couchDbResponse}", couchDbResponse.statusCode)
        }
        List<JSONObject> documents = []
        couchDbResponse.json.rows?.each { JSONObject row ->
            CouchDbResponse docResponse = couchDb.get((String) row.id)
            transactionParams["${row.id}"] = docResponse.toJson()
            if (docResponse.success) {
                documents << docResponse.json
            } else {
                throw new WebException("error retrieving view document ${docResponse}", docResponse.statusCode)
            }
        }
        //sort documents by insert-date descending
        documents.sort { JSONObject row ->
            DapSeqJsonSubmission submission = DapSeqJsonSubmission.newSubmission(row)
            submission.submissionDate
        }.reverse().each { JSONObject row ->
            json << new JSONObject(row.toJSONString())
        }
        json
    }

    JSONArray submissionsForDemultiplexedLibraryName(String demultiplexedLibraryName) {
        String designDocument = 'lookup'
        String viewName = 'demultiplexed-library-name'
        JSONArray allDocs = retrieveViewRowsSortedByInsertDate(designDocument,viewName,demultiplexedLibraryName)
        Map<String,Object> poolIdDocument = [:] //linked hash map of string,JSONObject
        for (doc in allDocs) {
            DapSeqJsonSubmission submission = DapSeqJsonSubmission.newSubmission(doc)
            if (submission?.dapSeqPoolLimsId) {
                if (!poolIdDocument.containsKey(submission.dapSeqPoolLimsId)) {
                    poolIdDocument[submission.dapSeqPoolLimsId] = doc
                }
            }
        }
        JSONArray selectedDocs = new JSONArray()
        if (poolIdDocument) {
            selectedDocs.addAll(poolIdDocument.values())
        }
        selectedDocs
    }

    JSONArray submissionsForDapSeqPoolLimsId(String dapSeqPoolLimsId) {
        String designDocument = 'lookup'
        String viewName = 'library-limsid'
        retrieveViewRowsSortedByInsertDate(designDocument,viewName,dapSeqPoolLimsId)
    }

    JSONArray submissionsForDapSeqPoolName(String dapSeqPoolName) {
        String designDocument = 'lookup'
        String viewName = 'library-name'
        retrieveViewRowsSortedByInsertDate(designDocument,viewName,dapSeqPoolName)
    }

    void retrieveSubmissionsForDemultiplexedLibraryName(String demultiplexedLibraryName) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONArray submissions = submissionsForDemultiplexedLibraryName(demultiplexedLibraryName)
        requestCash.dapSeqJson = submissions
        if (submissions) {
            requestCash.statusCode = HttpStatus.SC_OK
        } else {
            requestCash.statusCode = HttpStatus.SC_NOT_FOUND
        }

    }

    void retrieveSubmissionForDapSeqPoolLimsId(String dapSeqPoolLimsId) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONArray submissions = submissionsForDapSeqPoolLimsId(dapSeqPoolLimsId)
        if (submissions) {
            requestCash.dapSeqJson = submissions[0]
            requestCash.statusCode = HttpStatus.SC_OK
        } else {
            requestCash.dapSeqJson = new JSONObject()
            requestCash.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }

    void retrieveSubmissionForDapSeqPoolName(String dapSeqPoolName) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONArray submissions = submissionsForDapSeqPoolName(dapSeqPoolName)
        if (submissions) {
            requestCash.dapSeqJson = submissions[0]
            requestCash.statusCode = HttpStatus.SC_OK
        } else {
            requestCash.dapSeqJson = new JSONObject()
            requestCash.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }

    private static final Integer submissionDateLimit = 50
    private static final Map submissionDateViewParams = [
            'limit':submissionDateLimit,
            'descending':true
    ]

    private static String localDateFormatText = 'yyyy-MMM-dd HH:mm:ss.SSS z'
    private static final DateFormat localDateFormat = new SimpleDateFormat(localDateFormatText)
    static {
        localDateFormat.setTimeZone(TimeZone.getTimeZone('PST'))
    }

    JSONArray latestSubmissions(Map queryParams) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONObject transactionParams = new JSONObject()
        requestCash.couchdbQueries = transactionParams

        String designDocument = 'lookup'
        String viewName = 'submission-date_library-limsid'
        JSONArray json = new JSONArray()
        Map params = new HashMap<>()
        params.putAll(submissionDateViewParams)
        if (queryParams) {
            params.putAll(queryParams)
        }
        if (queryParams['limit']?.toString()?.isInteger()) {
            params['limit'] = queryParams['limit'].toString()
        } else {
            params['limit'] = submissionDateLimit
        }
        CouchDb couchDb = getCouchDb()
        CouchDbResponse couchDbResponse = couchDb.getFromView(designDocument,viewName,params)
        transactionParams["${viewName}"] = couchDbResponse.toJson()

        couchDbResponse.json.rows?.each { org.json.simple.JSONObject row ->
            JSONObject responseRow = new JSONObject(row.value) //put all elements of value object
            responseRow['id'] = row.'id'
            responseRow['key'] = row.'key'
            responseRow['submission-date-pst'] = localDateFormat.format(DateUtil.stringToDate(row.'key' as String))
            json << responseRow
        }
        json
    }

    void retrieveLatestSubmission(Map queryParams) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        JSONArray submissions = latestSubmissions(queryParams)
        requestCash.dapSeqJson = submissions
        if (submissions) {
            requestCash.statusCode = HttpStatus.SC_OK
        } else {
            requestCash.statusCode = HttpStatus.SC_NOT_FOUND
        }
    }


    void retrieveSubmission(String documentId) {
        RequestCash requestCash = BeanUtil.getBean(RequestCash.class)
        CouchDbResponse couchDbResponse = couchDb.get(documentId)
        requestCash.statusCode = couchDbResponse.statusCode
        if (requestCash.statusCode != HttpStatus.SC_OK) {
            if (requestCash.statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new PpsException("no dap-seq document found for ID [${documentId}]")
            }
            throw new PpsException(couchDbResponse.reasonPhrase)
        }
        requestCash.couchdbResponse = JsonUtil.toJsonObject(couchDbResponse.json)
    }

}
