package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 3/2/15.
 * Clarity Illumina MiSeq, NextSeq, HiSeq Support Requirements
 * https://docs.google.com/document/d/15g6IcLuFzb-1Q8LtFCdfukaT2f2l0m32j9b8yo3_51Q/edit#
 * Clarity Illumina NovaSeq Support Requirements
 * https://docs.google.com/document/d/1CmDCgNkQwyHQRqk4Gx3LgeWzNYiPuJ6X9X6BKPffq9w/edit
 * Clarity Illumina HiSeq 2000, 1T Requirements
 * https://docs.google.com/document/d/1DhaLc-e3zebpmqOYUSMp19CcsvrP5QPlB5iFlsZNgAs/edit#heading=h.7bnr80mxwz1b
 */
class SqPrepareDilutionSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SqPrepareDilutionSheet.class)
    SequencerType sequencerType

    void execute() {
        sequencerType = (process as Sequencing).sequencerType
        sequencerType.validateOutputContainerType()
        ExcelWorkbook excelWorkbook = createExcelWorkbook()
        uploadDilutionSheet(excelWorkbook)
        sequencerType.uploadSampleSheet()
    }

    ExcelWorkbook createExcelWorkbook() {
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(sequencerType.templateName, (process as Sequencing).testMode)
        sequencerType.updateWorkbookStyle(excelWorkbook)
        excelWorkbook.addSection(tableSection)
        excelWorkbook
    }

    TableSection getTableSection(){
        List tableData = getTableData()
        TableSection tableSection = new TableSection(
                sequencerType.activeTabIndex,
                tableData[0].class,
                sequencerType.getStartAddress(),
                sequencerType.getEndAddress()
        )

        tableSection.setData(tableData)
        tableSection
    }

    List getTableData() {
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        List<FreezerContainer> freezerContainers = freezerService.freezerLookupInputAnalytes(process)
        return process.outputAnalytes.sort{ a, b -> a.lane <=> b.lane}.collect{
            sequencerType.getTableBean(it as ClarityPhysicalRunUnit, freezerContainers)
        }
    }

    void uploadDilutionSheet(ExcelWorkbook excelWorkbook) {
        ArtifactNode fileNode = process.getFileNode(Sequencing.DILUTION_DOWNLOAD_FILE)
        def responseNode = excelWorkbook.store(processNode.nodeManager.nodeConfig, fileNode.id)
        logger.info "uploaded $sequencerType.templateName to $responseNode"
    }

}