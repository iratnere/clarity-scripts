package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.PoolDetailsBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.LibraryPoolsMigrator
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.Migrator
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

class OnboardingLibraryPoolsAdapter implements OnboardingAdapter {
    static final logger = LoggerFactory.getLogger(OnboardingLibraryPoolsAdapter.class)
    ExcelWorkbook onboardingWorkbook
    List<OnboardingBean> onboardingBeansCached
    List<PoolDetailsBean> poolDetailsBeansCached
    Long researcherContactId
    List<String> runModeCvs
    @Delegate
    OnboardingBeansParser parser

    NodeManager getNodeManager() {
        return BeanUtil.nodeManager
    }

    OnboardingLibraryPoolsAdapter(ExcelWorkbook onboardingWorkbook, Long researcherId) {
        this.onboardingWorkbook = onboardingWorkbook
        this.researcherContactId = researcherId
        parser = new OnboardingBeansParser()
    }

    List<OnboardingBean> getOnboardingBeans(){
        if(!onboardingBeansCached) {
            Section section = onboardingWorkbook.sections.values().find { it.beanClass == OnboardingBean.class }
            onboardingBeansCached = section.data
        }
        return onboardingBeansCached
    }

    List<PoolDetailsBean> getPoolDetailsBeans() {
        if(!poolDetailsBeansCached) {
            Section section = onboardingWorkbook.sections.values().find { it.beanClass == PoolDetailsBean.class }
            poolDetailsBeansCached = section.data
        }
        return poolDetailsBeansCached
    }

    @Override
    List<String> validateOnboardingData() {
        List<String> errors = []
        logger.info "Validating pools data for onboarding....."
        if(onboardingBeans.find {!it.labLibraryName})
            errors << "'Lab Library Name' is required for all libraries."
        if(!errors) {
            errors = checkMissingFields(onboardingBeans)
            errors.addAll(validateBeansWithUSS(onboardingBeans))
            if(!errors)
                errors = validatePools(onboardingBeans)
            if(!errors)
                errors = validateBeansWithClarity(onboardingBeans)
        }
        if(!errors)
            errors.addAll(validate(onboardingBeans))
        return errors
    }

    List<String> validateBeansWithUSS(List<OnboardingBean> beans, ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService.class)) {
        List<String> errors = []
        List<String> indexNames = []
        for(OnboardingBean bean : beans) {
            if(!isRunModeValid(bean.sowRunMode))
                errors << "'SOW run mode' is not valid. Check ${bean.labLibraryName}."
            if(!bean.indexName)
                errors << "Index name field is required. Check ${bean.labLibraryName}."
            else
                indexNames << bean.indexName
        }
        if(indexNames) {
            Map<String, String> indexSequences = artifactIndexService.getSequences(indexNames)
            for(OnboardingBean bean : beans) {
                String sequence = indexSequences[bean.indexName]
                if(!sequence)
                    errors << "The index name ${bean.indexName} is not registered with clarity. Check ${bean.labLibraryName}."
                bean.indexSequence = sequence
            }
        }
        return errors
    }

    @Override
    void mergeDataToClarity(NodeManager nodeManager = BeanUtil.nodeManager) {
        onboardingBeans.each {
            it.contactId = researcherContactId
            prepareUdfsMap(it)
        }
        prepareBeansForOnboarding(onboardingBeans)
        LibraryPoolsMigrator migrator = new LibraryPoolsMigrator(nodeManager)
        migrator.pruBeans = onboardingBeans.groupBy { it.poolKey }
        logger.info "Onboarding pools from file....."
        migrator.createAnalytesInClarity(onboardingBeans, true)
        Migrator.submitRoutingRequests()
        nodeManager.httpPutDirtyNodes()
        ScheduledSampleService service = BeanUtil.getBean(ScheduledSampleService.class)
        service.editSow(researcherContactId, onboardingBeans.collect{it.scheduledSample})
    }

    @Override
    void archiveOnboardingResult(String artifactId, NodeManager nodeManager = BeanUtil.nodeManager){
        logger.info "Adding Geneology to result file...."
        int sheetIndex = getOnboardingDetailsSheetIndex(onboardingWorkbook)
        onboardingWorkbook.addSection(prepareOnboardingDetails(onboardingBeans, sheetIndex))
        onboardingWorkbook.store(nodeManager.nodeConfig, artifactId, false)
    }

    @Override
    List<Analyte> getAnalytes() {
        onboardingBeans.collect{it.analytes}.flatten()
    }

    @Override
    String getAction() {
        return "Onboarding Libraries"
    }

    List<String> checkMissingFields(List<OnboardingBean> beans) {
        List<String> errors = []
        for(OnboardingBean bean : beans) {
            if (!bean.sampleName)
                errors << "'sample name' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.poolNumber)
                errors << "'Pool Number' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.sowRunMode)
                errors << "'SOW run mode' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libraryCreationSite)
                errors << "'Library Creation Site' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.indexName)
                errors << "'Index name' is required for all libraries. Check ${bean.labLibraryName}."
        }
        return errors
    }

    List<String> validateBeansWithClarity(List<OnboardingBean> beans, NodeManager nodeManager = this.nodeManager) {
        List<String> errors = lookUpPools(beans, nodeManager)
        if(!errors) {
            List<OnboardingBean> unMergedBeans = beans.findAll { !it.libraryStock }
            errors = lookUpLibraries(unMergedBeans, nodeManager)
            if(!errors) {
                unMergedBeans = beans.findAll { !it.pmoSample }
                if (unMergedBeans)
                    errors.addAll(lookUpSamples(unMergedBeans, nodeManager))
            }
        }
        if(!errors)
            nodeManager.httpPutNodesBatch(beans.collect{it.pmoSample.sampleNode})
        return errors.unique()
    }

    List<String> validatePools(List<OnboardingBean> beans) {
        List<String> errors = []
        Map<Integer,List<OnboardingBean>> poolBeans = beans.groupBy {getPoolKey(it.poolNumber)}
        List<String> poolNames = poolDetailsBeans?.collect {it.collaboratorPoolName}?.unique()
        if(poolNames?.size() < poolDetailsBeans?.size())
            errors << "All pools must have unique PI Pool Name."
        poolBeans.each {String poolKey, List<OnboardingBean> libBeans ->
            errors.addAll(validatePoolConfiguration(poolKey, libBeans))
        }
        return errors
    }

    String getPoolKey(def poolNumber) {
        return "Pool #${poolNumber as int}"
    }

    List<String> validatePoolConfiguration(String poolKey, List<OnboardingBean> libraryBeans) {
        PoolDetailsBean poolBean = poolDetailsBeans.find{ getPoolKey(it.poolNumber) == poolKey}
        List<String> errors = []
        if(!poolBean)
            errors << "All pools must be listed in the 'Pool Details' tab. $poolKey is missing."
        else {
            if(!poolBean.collaboratorPoolName) {
                errors << "'PI Pool Name' is required for all pools, Check $poolKey."
            }
            if(!poolBean.poolConcentration || poolBean.poolConcentration <= 0)
                errors << "Non-zero 'Pool Concentration (ng/ul)' is required for all pools, Check $poolKey."
            if(!poolBean.poolVolume || poolBean.poolVolume <= 0)
                errors << "Non-zero 'Pool Volume (ul)' is required for all pools, Check $poolKey."
            if(!poolBean.poolMolarity || poolBean.poolMolarity <= 0)
                errors << "Non-zero 'Pool Molarity (pM)' is required for all pools, Check $poolKey."
            if(!poolBean.poolFragmentSize || poolBean.poolFragmentSize <= 0)
                errors << "Non-zero 'Pool Fragment Size w/ adaptors (bp)' is required for all pools, Check $poolKey."
        }
        Set<String> runModes = libraryBeans.collect { it.sowRunMode } as Set
        if(runModes.size() > 1)
            errors << "All libraries of a pool must have the same run mode. Check $poolKey."
        errors.addAll(ArtifactIndexService.validatePoolIndexes(poolKey, libraryBeans.collect { it.indexSequence } as List))
        if(poolBean) {
            libraryBeans.each {
                it.poolBean = poolBean
                it.dop = libraryBeans.size()
            }
        }
        return errors
    }

    @Override
    void prepareUdfsMap(OnboardingBean bean) {
        PoolDetailsBean poolDetailsBean = poolDetailsBeans.find {it.poolNumber == bean.poolNumber}
        int dop = onboardingBeans.findAll { it.poolNumber == bean.poolNumber}?.size()?:1
        bean.udfMaps[PmoSample.class][ClarityUdf.SAMPLE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_DEGREE_OF_POOLING] = bean.dop?:dop
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_RUN_MODE] = bean.sowRunMode
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_VOLUME_UL] = bean.aliquotVolume?:10
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = (bean.aliquotAmount && bean.aliquotVolume > 0)?bean.aliquotAmount / bean.aliquotVolume:1.2
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_NUMBER_PCR_CYCLES] = bean.pcrCycles?:2
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP] = bean.libFragmentSize?:poolDetailsBean.poolFragmentSize
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_VOLUME_UL] = bean.libVolume?:poolDetailsBean.poolVolume
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM] = bean.libMolarityPm?:poolDetailsBean.poolMolarity
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM] = bean.libMolarityPm?:poolDetailsBean.poolMolarity
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = bean.libConcentration?:poolDetailsBean.poolConcentration
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_NOTES] = "Library protocol: ${bean.libraryProtocol}"
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME] = bean.labLibraryName
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_INDEX_NAME] = bean.indexName
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM] = poolDetailsBean.poolMolarity
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM] = poolDetailsBean.poolMolarity
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = poolDetailsBean.poolConcentration
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.SAMPLE_DEGREE_OF_POOLING] = dop
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP] = poolDetailsBean.poolFragmentSize
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_VOLUME_UL] = poolDetailsBean.poolVolume
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_RUN_MODE] = bean.sowRunMode
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_COLLABORATOR_POOL_NAME] = poolDetailsBean.collaboratorPoolName
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT] = Analyte.PASS
    }

    boolean isRunModeValid(String runMode) {
        if(!runModeCvs) {
            runModeCvs = RunModeCv.findAllByActive("Y")?.collect {it.runMode}
        }
        return runModeCvs.contains(runMode)
    }
}
