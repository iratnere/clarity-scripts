package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

class RecordDetailsInfo {
    BigDecimal replicatesFailurePercentAllowed
    BigDecimal controlsFailurePercentAllowed
    long researcherId

    RecordDetailsInfo (SowItemQc sowQcProcess) {
        replicatesFailurePercentAllowed = sowQcProcess.udfReplicatesFailurePercentAllowed
        controlsFailurePercentAllowed = sowQcProcess.udfControlsFailurePercentAllowed
        researcherId = sowQcProcess.researcherContactId
    }
}
