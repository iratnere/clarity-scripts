package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 1/31/17.
 * Required: Pool Number	Pool Concentration (ng/ul)	Pool Molarity (pM)	Pool Volume (ul) 	Pool Fragment Size w/ adaptors (bp)
 */
class PoolDetailsBean {
    @FieldMapping(header = 'Pool Number', cellType = CellTypeEnum.NUMBER, required = true)
    public def poolNumber
    @FieldMapping(header = 'Pool Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER, required = true)
    public def poolConcentration
    @FieldMapping(header = 'Pool Molarity (pM)', cellType = CellTypeEnum.NUMBER, required = true)
    public def poolMolarity
    @FieldMapping(header = 'Pool Volume (ul)', cellType = CellTypeEnum.NUMBER, required = true)
    public def poolVolume
    @FieldMapping(header = 'Pool Fragment Size w/ adaptors (bp)', cellType = CellTypeEnum.NUMBER, required = true)
    public def poolFragmentSize
    @FieldMapping(header = 'PI Pool Name', cellType = CellTypeEnum.STRING)
    public def collaboratorPoolName
    @FieldMapping(header = "JGI Pool Name", cellType = CellTypeEnum.STRING)
    public def jgiPoolName

    int hashCode(){
        new HashCodeBuilder(17, 37).
                append(poolNumber).toHashCode()
    }

    boolean equals(PoolDetailsBean bean){
        bean.hashCode() == this.hashCode()
    }
}
