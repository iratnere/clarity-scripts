package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ClarityAttachmentUtil
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.DensityTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.apache.commons.io.FilenameUtils
import org.slf4j.LoggerFactory

class ProcessDensityFiles extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ProcessDensityFiles.class)
    static final String DENSITY = 'Density'
    static final String DNA_CONCENTRATION = 'DNA Concentration'
    private static final String XLSX = 'xlsx'

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        Map<String, List<DensityTableBean>> plateBarcodeToBeans = processDensityFiles()
        automaticPlacement(plateBarcodeToBeans)
        List<DensityTableBean> densityTableBeans = plateBarcodeToBeans.values().flatten() as List<DensityTableBean>
        process.outputAnalytes.each{ output ->
            DensityTableBean bean = densityTableBeans.find { output.containerLocation.wellLocation == it.wellPos }
            output.udfFractionDensity = bean?.density
        }
        processConcentrationFiles()
    }

    void automaticPlacement(Map<String, List<DensityTableBean>> plateBarcodeToBeans) {
        List<ContainerNode> containerNodes = createDestContainers(plateBarcodeToBeans.keySet())
        OutputPlacements outputPlacements = new OutputPlacements()
        // For destination containers, iterate through its corresponding rows and add their placement to the placement node
        List<DensityTableBean> densityTableBeans = plateBarcodeToBeans.values().flatten() as List<DensityTableBean>
        process.outputAnalytes.each { Analyte output ->
            Analyte input = output.parentAnalyte //SampleAnalyte
            DensityTableBean bean = densityTableBeans.find { it.sampleBarcode && input.claritySample.name == it.sampleBarcode }
            if (!bean) {
                def errorMsg = """
Sample Barcode: ${input.claritySample.name}
Sample Barcode values are not provided for all fraction samples.
Please correct density file(s).
"""
                process.postErrorMessage(errorMsg as String)
            }
            densityTableBeans.remove(bean)
            String containerId = containerNodes.find{ it.name == bean.plateBarcode }.id
            ContainerLocation containerLocation = new ContainerLocation(
                    containerId,
                    convertToContainerNodeLocation(bean.wellPos)
            )
            outputPlacements << new OutputPlacement(artifactId: output.id, containerLocation: containerLocation)
        }
        if (densityTableBeans) {
            def errorMsg = """
Requested number of fraction samples is greater than number of replicates.
Please correct density file(s) or abort the process.
Cannot place the following fraction samples:
${densityTableBeans.collect{"$it.sampleBarcode  $it.wellPos"}}
"""
            process.postErrorMessage(errorMsg as String)
        }
        process.processNode.placementsNode.setOutputPlacements(outputPlacements).httpPost()
        logger.info "Automatic Placement Script has completed successfully."
        process.refreshOutputs()
    }

    static String convertToContainerNodeLocation(String location) {
        return "${location.substring(0, 1)}:${location.substring(1, location.length())}"
    }

    List<ContainerNode> createDestContainers(Set<String> plateBarcodes) {
        NodeManager clarityNodeManager = BeanUtil.nodeManager
        def numberOutputContainers = plateBarcodes.size()
        if (!numberOutputContainers)
            return null
        String containerId = process.processNode.placementsNode.selectedContainerIds[0] //first available output plate container
        List<ContainerNode> outputContainerNodes = [clarityNodeManager.getContainerNode(containerId)]
        if (numberOutputContainers > 1) {
            outputContainerNodes.addAll(clarityNodeManager.createContainersBulk(ContainerTypes.FULL_PLATE_96, numberOutputContainers - 1))
        }
        outputContainerNodes.eachWithIndex { ContainerNode containerNode, index ->
            String plateBarcode = plateBarcodes[index]
            containerNode.readOnly = false
            containerNode.name = plateBarcode
            containerNode.httpPut()
        }
        outputContainerNodes
    }

    Map<String, List<DensityTableBean>> processDensityFiles() {
        Map<String, List<DensityTableBean>> plateBarcodeToBeans = [:].withDefault {[]}
        List<String> inputBarcodes = process.inputAnalytes.collect{it.sampleNode.name}
        List<ArtifactNode> files = validateFiles(DENSITY)
        files.each{ file ->
            ArtifactNode node = process.getFileNode(file.name)
            String fileNameNoExt = fileNameNoExt(node.fileNode)
            List<DensityTableBean> beansList = processExcelWorkbook(node.id)

            StringBuilder errorMsg = new StringBuilder()
            beansList.each{ bean ->
                bean.plateBarcode = fileNameNoExt
                def beanErrorMsg = bean.validate(inputBarcodes, getEnteredBarcodes(plateBarcodeToBeans))
                if (beanErrorMsg) {
                    errorMsg << beanErrorMsg
                }
            }
            def fileSampleBarcodes = beansList.findResults{it.sampleBarcode ? it.sampleBarcode : null}?.unique()
            if (!fileSampleBarcodes) {
                errorMsg << "Cannot find Sample Barcodes in $fileNameNoExt file."
            }
            if (errorMsg) {
                def fullErrorMsg = "Please correct the errors below and upload $fileNameNoExt file again." << process.WINDOWS_NEWLINE
                fullErrorMsg << errorMsg
                process.postErrorMessage(fullErrorMsg as String)
            }
            plateBarcodeToBeans[fileNameNoExt] = beansList.findAll{it.sampleBarcode}
        }
        Set<String> enteredBarcodes = getEnteredBarcodes(plateBarcodeToBeans)
        def errorMsg = DensityTableBean.validateEnteredBarcodes(inputBarcodes, enteredBarcodes)
        errorMsg ? process.postErrorMessage(errorMsg as String): null
        plateBarcodeToBeans
    }

    static ExcelWorkbook getDensityWorkbook(def fileNodeId) {
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(fileNodeId)
        Section tableSection = new TableSection(0, DensityTableBean.class, 'A1', 'E98')
        excelWorkbook.addSection(tableSection)
        excelWorkbook.load()
        excelWorkbook
    }

    List<DensityTableBean> processExcelWorkbook(def fileNodeId) {
        ExcelWorkbook excelWorkbook = getDensityWorkbook(fileNodeId)
        List<DensityTableBean> beansList = (process as SampleFractionationProcess)
                .getBeanList(DensityTableBean.class.simpleName, excelWorkbook) as List<DensityTableBean>
        beansList
    }

    def getEnteredBarcodes = { Map<String, List<DensityTableBean>> plateBarcodeToBeans ->
        return plateBarcodeToBeans
                .values()
                .findResults { it.sampleBarcode ? it.sampleBarcode : null }
                .flatten() as Set<String>
    }

    static def validateFileExt = { List<String> fileNames, String placeholder ->
        def unsupportedFiles = fileNames.findAll{ fileName ->
            String extension = FilenameUtils.getExtension(fileName)
            (placeholder == DENSITY && extension != XLSX) || (placeholder == DNA_CONCENTRATION && extension == XLSX)
        }
        if (unsupportedFiles) {
            throw new WebException("""
The following file types are not supported:
$unsupportedFiles.
Please ${placeholder == DENSITY ? "":"do not"} upload the '.$XLSX' $placeholder file(s).
""", 500)
        }
    }

    static def validateFileNames = { List<String> fileNamesNoExt, String placeholder ->
        if (!fileNamesNoExt?.size()) {
            throw new WebException("""
Cannot find $placeholder file(s).
Please upload $placeholder file(s).
""", 500)
        }
        def duplicates = fileNamesNoExt.findAll{a -> fileNamesNoExt.findAll{b -> b == a}.size() > 1}.unique()
        if (duplicates) {
            throw new WebException("""
The $placeholder file names are not unique:
$duplicates.
""", 500)
        }
    }

    def findFilesByPlaceholder = { String placeholder ->
        processNode.outputResultFiles.findAll {
            it && it.name?.startsWith(placeholder) && it.fileNode
        }
    }

    def fileNamesNoExt = { List<ArtifactNode> uploadedFiles ->
        uploadedFiles.collect{
            fileNameNoExt(it.fileNode)
        }
    }

    static def fileNameNoExt = { FileNode fileNode ->
        String fileName = fileNode.originalLocation
        FilenameUtils.getBaseName(fileName)
    }

    List<ArtifactNode> validateFiles(String placeholder) {
        List<ArtifactNode> files = findFilesByPlaceholder(placeholder)
        validateFileExt(files.collect{it.fileNode.originalLocation}, placeholder)
        def fileNamesNoExt = fileNamesNoExt(files)
        validateFileNames(fileNamesNoExt, placeholder)
        if (placeholder == DNA_CONCENTRATION) {
            Set<String> containerNames = process.outputAnalytes*.containerName
            validateUploadedFiles(containerNames, fileNamesNoExt)
        }
        files
    }

    void processConcentrationFiles(){
        List<ArtifactNode> files = validateFiles(DNA_CONCENTRATION)
        files.each {
            int index = 2 // Well
            Map<String, List<String>> indexValueToRow =  ClarityAttachmentUtil.downloadFileAsMap(it.id, index)
            String fileName = fileNameNoExt(it.fileNode)
            List<Analyte> outputs = process.outputAnalytes.findAll {it.containerName == fileName}
            outputs.eachWithIndex{ output, i ->
                def location = output.artifactNode.containerLocation.wellLocation
                def row = indexValueToRow[location]
                //"Well ID\tName\tWell\tConc/Dil\t485,528\t[Concentration]\tCount\tMean\tStd Dev\tCV (%)"
                //"BLK\t\tA6\t\t8\t>10.475\t0\t?????\t?????\t?????"
                output.udfConcentrationNgUl = getConcentration(row[5], "$fileName: error in row $row" )
            }
        }
    }

    static getConcentration = { String rule, def errorMsg ->
        if (!rule || rule?.startsWith('<')) {
            return BigDecimal.ZERO
        }
        if (rule?.startsWith('>')) {
            BigDecimal value = rule.substring(1) as BigDecimal
            return value
        }
        if (SampleFractionationProcess.parseToBigDecimalError(rule)) {
            throw new RuntimeException("""
$errorMsg
Invalid concentration: $rule.
Cannot get a numeric value.
""")
        }
        if (rule.toBigDecimal() < 0) {
            return BigDecimal.ZERO
        }
        rule as BigDecimal
    }

    static def validateUploadedFiles = { Set<String>  containerNames, List<String> fileNamesNoExt ->
        int containersNumber = containerNames.size()
        if (!fileNamesNoExt?.size() || fileNamesNoExt?.size() != containersNumber) {
            throw new WebException("""
Invalid uploaded $DNA_CONCENTRATION files.
Please upload $containersNumber concentration file(s) with the following name(s):
$containerNames.
""", 500)
        }
        fileNamesNoExt.each { fileName ->
            if (!(fileName in containerNames)) {
                throw new WebException("""
Invalid concentration file name: $fileName
The concentration file name should match the output container barcode(s):
$containerNames.
""", 500)
            }
        }
    }
}