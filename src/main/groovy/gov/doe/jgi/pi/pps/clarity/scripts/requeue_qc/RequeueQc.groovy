package gov.doe.jgi.pi.pps.clarity.scripts.requeue_qc

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class RequeueQc extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueQc.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.setCompleteStage()
    }

}
