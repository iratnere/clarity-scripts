package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.SampleQcProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil

class GenericSampleQC extends SampleQcAdapter {
    def sampleQcCompleteMap = [:]
    Map<Long, String> sampleStatusMap = [:]

    GenericSampleQC(SampleQcUtility sampleQcUtility, List<Analyte> analytes) {
        super(sampleQcUtility,analytes)
    }

    String checkContainerContents(Map<ContainerNode, Set<String>> containerAnalyteIds){
        for(Map.Entry entry: containerAnalyteIds){
            ContainerNode container = entry.key
            Set<String> analyteIds = entry.value
            if(!container){
                errors << "Analytes ${analyteIds} have no Container."
                return errors.last()
            }
            //all samples from the input containers must be placed on the qc plate
            if(container.contentsIds?.toSet()?.size() > analyteIds?.size()){
                errors << "All samples from the input containers must be placed on the qc plate."
                return errors.last()
            }
        }
        return ""
    }

    void commitSampleStatus(def researcherContactId){
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        statusService.submitSampleStatus(sampleQcCompleteMap, researcherContactId)
        statusService.submitSampleStatus(sampleStatusMap, researcherContactId)
    }

    void updateStatusMaps(PmoSample pmoSample, String sampleQcResult) {
        def pmoSampleId = pmoSample.pmoSampleId
        sampleQcCompleteMap[pmoSampleId] = SampleStatusCv.SAMPLE_QC_COMPLETE
        //tube--Pass--update PLUSS sample status to “Available For Use”
        //plate--Pass(plate passes, individual sample qc result does not matter)--update PLUSS sample status to “Available For Use” for all samples on the plate
        if (sampleQcResult == Analyte.PASS)
            sampleStatusMap[pmoSampleId] = SampleStatusCv.AVAILABLE_FOR_USE
        if (sampleQcResult == Analyte.FAIL) {
            //plate--Fail(plate fails,individual sample qc result does not matter)--update PLUSS sample status to “Abandoned” for all  samples on the plate
            sampleStatusMap[pmoSampleId] = SampleStatusCv.ABANDONED
        }
    }

    @Override
    String validateAnalytes() {
        checkInputSize(analytes)
        Map<ContainerNode, Set<String>> containerAnalyteIds = [:].withDefault {[] as Set}
        analytes.each { Analyte analyte ->
            checkIsSample(analyte)
            containerAnalyteIds[analyte.containerNode] << analyte.id
        }
        validateSampleStatus()
        checkContainerContents(containerAnalyteIds)
        return errors ? errors.join("\n") : ""
    }

    @Override
    List<ArtifactNode> processRecordDetails(RecordDetailsInfo recordDetailsInfo) {
        failedTubeSamples = []
        Map<ClaritySample, List<String>> sampleSowItemIds = [:].withDefault {[]}
        transferDataFromTableToUdfs(recordDetailsInfo.glsStarletUseSection)
        transferDataFromKeyValueSection(recordDetailsInfo.keyValueSections)
        //create scheduled samples for each sample PLUSS sow items(excluding the sow items in PLUSS state “Abandoned”, “Complete”, “Deleted”, “In Progress”, “Awaiting QA/QC Analysis”, “Awaiting Sample QC Review”) regardless of sample qc result
        analytes.each { Analyte sampleAnalyte ->
            getSowItemsStatus(sampleAnalyte.claritySample.pmoSample.pmoSampleId)?.each{sowItemId, sampleStatus ->
                if(!(sampleStatus in SOW_ITEM_STATUS))
                    sampleSowItemIds[sampleAnalyte.claritySample] << sowItemId
            }
            String sampleQcResult = sampleAnalyte.claritySample?.pmoSample?.udfContainerSampleQcResult
            updateStatusMaps(sampleAnalyte.claritySample, sampleQcResult)
            //tube--Fail--request replacement sample; this will change sample status to “Abandoned”
            if (sampleQcResult == Analyte.FAIL && !sampleAnalyte.isOnPlate)
                failedTubeSamples << sampleAnalyte
        }
        List<ArtifactNode> scheduleSampleArtifacts = createScheduledSamples(sampleSowItemIds, recordDetailsInfo.researcherContactId)
        commitSampleStatus(recordDetailsInfo.researcherContactId)
        return scheduleSampleArtifacts
    }

    @Override
    Stage getDestinationStage() {
        return Stage.SOW_ITEM_QC
    }

    @Override
    List<String> checkProcessUdfs(SampleQcProcess sampleQcProcess) {
        return []
    }
}
