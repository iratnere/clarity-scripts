package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by dscott on 4/22/2014.
 */
class AbandonQueueProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.ABANDON_QUEUE

    AbandonQueueProcess(ProcessNode processNode) {
        super(processNode)
    }
}
