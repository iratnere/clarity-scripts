package gov.doe.jgi.pi.pps.clarity.welladdress

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
/**
 * Created by dscott on 6/20/2014.
 */
class WellAddress96 extends WellAddress {

    static final Integer minRowIndex = 0
    static final Integer maxRowIndex = 7
    static final Integer minColIndex = 0
    static final Integer maxColIndex = 11

    static List<String> rows = ('A'..'H').toList()

    boolean getIsCorner() {
        return (minRowIndex == rowIndex || maxRowIndex == rowIndex) && (minColIndex == colIndex || maxColIndex == colIndex)
    }

    boolean getIsValid() {
        return rowIndex != null &&
               colIndex != null &&
               rowIndex >= minRowIndex &&
               rowIndex <= maxRowIndex &&
               colIndex >= minColIndex &&
               colIndex <= maxColIndex
    }

    WellAddress96() {}

	WellAddress96(ContainerLocation containerLocation) {
		this(containerLocation.rowColumn)
	}
	
    WellAddress96(String wellName) {
		String row
		String col
		if (wellName.contains(':')) {
			List<String> rowCol = wellName.split(':').toList()
			if (rowCol.size() == 2) {
				row = rowCol[0]
				col = rowCol[1]
			}
		} else if (wellName?.size() >= 2) {
            row = wellName[0]
            col = wellName[1..-1]
        }
		if (col?.isInteger()) {
			colIndex = col.toInteger() - 1
			rowIndex = rows.indexOf(row.toUpperCase())
		}
        if (!isValid) {
            throw new RuntimeException("invalid well name ${wellName}")
        }
    }

    String getRow() {
        return rows[rowIndex]
    }

    String getColumn() {
        return "${colIndex + 1}"
    }

}
