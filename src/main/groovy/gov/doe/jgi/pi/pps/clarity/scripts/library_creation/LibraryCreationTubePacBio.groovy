package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil

/**
 * Created by tlpaley on 4/1/15.
 */
class LibraryCreationTubePacBio extends LibraryCreation {

    LibraryCreationTubePacBio(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputTube(process)
    }

    @Override
    Stage getDefaultStage() {
        throw new WebException("coding error, invalid access to property defaultStage: no default stage defined for PacBio library stocks since addition of Sequel workflow", 500)
    }

    @Override
    String getLcTableBeanClassName() {
        (output as OutputTube).TABLE_CLASS_NAME
    }

    @Override
    void updateLibraryStockUdfs() {
        transferBeansDataToClarityLibraryStocks()
        process.updateNonWorksheetUdfs()
    }

    void transferBeansDataToClarityLibraryStocks() {
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcTubeBean lcTubeBean = (LcTubeBean) process.getLcTableBean(clarityLibraryStock.id)
            clarityLibraryStock.udfLibraryQcResult = lcTubeBean.libraryQcResult?.value
            clarityLibraryStock.systemQcFlag = lcTubeBean.libraryQcResult?.value == Analyte.PASS
            clarityLibraryStock.udfLibraryQcFailureMode = lcTubeBean.libraryQcFailureMode?.value
            clarityLibraryStock.udfVolumeUl = lcTubeBean.libraryVolume
            clarityLibraryStock.udfConcentrationNgUl = lcTubeBean.libraryConcentration
            clarityLibraryStock.udfActualTemplateSizeBp = lcTubeBean.libraryActualTemplateSize
            clarityLibraryStock.udfNotes = lcTubeBean.lcNotes

            clarityLibraryStock.setContainerUdfLibraryQcFailureMode(lcTubeBean.libraryQcFailureMode?.value)
            clarityLibraryStock.setContainerUdfLibraryQcResult(lcTubeBean.libraryQcResult?.value)
        }
    }

    @Override
    Section getTableSectionLibraries(){
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        LibraryStockFailureModesService libraryStockFailureModesService = BeanUtil.getBean(LibraryStockFailureModesService.class)
        Section tableSection = new TableSection(0, LcTubeBean.class, 'A1', null, true)
        def tableBeansArray = []
        List<FreezerContainer> freezerContainers = freezerService.freezerLookupInputAnalytes(process)
        process.outputAnalytes.sort{it.name}.eachWithIndex { ClarityLibraryStock clarityLibraryStock, index ->
            ClaritySampleAliquot claritySampleAliquot = clarityLibraryStock.parentAnalyte as ClaritySampleAliquot
            def containerName = claritySampleAliquot.containerName
            ClaritySample claritySample = claritySampleAliquot.claritySample

            LcTubeBean bean = new LcTubeBean()
            bean.aliquotConcentration = claritySampleAliquot.udfConcentrationNgUl
            bean.aliquotVolume = claritySampleAliquot.udfVolumeUl
            bean.aliquotMass = claritySampleAliquot.massNg
            bean.aliquotContainerName = containerName // Label was "aq $containerName"
            bean.aliquotFreezerPath = process.getFreezerPath(freezerContainers.findAll{it.barcode == containerName})
            bean.aliquotName = claritySampleAliquot.name
            bean.counter = index + 1
            bean.libraryName = clarityLibraryStock.name
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryIndexName = ArtifactIndexService.INDEX_NAME_N_A
            bean.libraryCreationQueue = claritySampleAliquot.libraryCreationQueue?.libraryCreationQueue
            bean.dop = claritySample.udfDegreeOfPooling
            bean.lcInstructions = claritySample.udfLcInstructions
            bean.smNotes = claritySample.udfNotes
            bean.libraryQcResult = libraryStockFailureModesService.dropDownPassFail
            bean.libraryQcFailureMode = libraryStockFailureModesService.dropDownFailureModes
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return (output as OutputTube).populateLibraryCreationSheet(getTableSectionLibraries())
    }

    @Override
    void moveToNextWorkflow() {

        List<ClarityLibraryStock> passedPacBioSequel = []
        List<Analyte> failedLibraryStocks = []

        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            String libraryQcResult = clarityLibraryStock.udfLibraryQcResult
            if (!(libraryQcResult in [Analyte.PASS, Analyte.FAIL])) {
                new WebException("Library Stock($clarityLibraryStock.id): invalid Library QC Result '${libraryQcResult}'", 422)
            }

            if (libraryQcResult == Analyte.FAIL) {
                failedLibraryStocks << clarityLibraryStock
                return
            }

            SequencerModelCv sequencerModelCv = clarityLibraryStock.sequencerModelCv //returns not null or throws exception
            if (!sequencerModelCv.isPacBio) {
                throw new WebException("invalid platform [${sequencerModelCv.platform}] for ${clarityLibraryStock} with run mode ${clarityLibraryStock.udfRunMode}, expected PacBio",422)
            }
            if (sequencerModelCv.isSequel) {
                passedPacBioSequel << clarityLibraryStock
            } else {
                throw new WebException("no handler for sequencer model [${sequencerModelCv.sequencerModel}] for ${clarityLibraryStock} with run mode ${clarityLibraryStock.udfRunMode}",422)
            }
        }

        process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING, passedPacBioSequel)
        process.processCompleteFailure(failedLibraryStocks)
    }

    String getTemplate(){
        return OutputTube.TEMPLATE_NAME
    }

    @Override
    void validateProcessUdfs() {
        def processUdfSmrtbellTemplatePrepKit = (process.outputAnalytes[0] as ClarityLibraryStock).udfSmrtbellTemplatePrepKit
        def processUdfPacBioSmrtbellTemplatePrepKit = (process.outputAnalytes[0] as ClarityLibraryStock).udfPacBioSmrtbellTemplatePrepKit
        if (!processUdfSmrtbellTemplatePrepKit && !processUdfPacBioSmrtbellTemplatePrepKit)
            process.postErrorMessage("""
Unable to Complete Work 
Please specify the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf.
""")
        if (processUdfSmrtbellTemplatePrepKit && processUdfPacBioSmrtbellTemplatePrepKit)
            process.postErrorMessage("""
Unable to Complete Work 
Both udfs were specified the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf.
""")
    }

}
