package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

class MetabolomicsTableBean {
    @FieldMapping(header = 'PMOS Sample ID', cellType = CellTypeEnum.NUMBER)
    public def sampleId
    @FieldMapping(header = 'Plate Barcode', cellType = CellTypeEnum.STRING)
    public String plateBarcode
    @FieldMapping(header = 'Well', cellType = CellTypeEnum.STRING)
    public def wellLocation
    @FieldMapping(header = 'Name passed for fraction FD name creation', cellType = CellTypeEnum.STRING)
    public def collaboratorSampleName
    @FieldMapping(header = 'Group', cellType = CellTypeEnum.STRING)
    public def groupName
    @FieldMapping(header = 'Label', cellType = CellTypeEnum.STRING)
    public def isotopeLabel
    @FieldMapping(header = 'Isotope Enrichment (at%)', cellType = CellTypeEnum.NUMBER, required = true)
    public def isotopeEnrichment
    @FieldMapping(header = 'Metabolomics Sample Id', cellType = CellTypeEnum.STRING, required = true)
    public String metabolomicsSampleId

    static final String SAMPLE_QC_FAILURE_MODE = "Insufficient Isotope Labeling"

    void populateBean(Analyte analyte) {
        PmoSample pmoSample = (PmoSample) analyte.claritySample
        this.sampleId = pmoSample.pmoSampleId
        ContainerNode plate = analyte.containerNode
        this.plateBarcode = plate.name
        ContainerLocation location = analyte.containerLocation
        this.wellLocation = location.wellLocation
        this.collaboratorSampleName = pmoSample.udfCollaboratorSampleName
        this.groupName = pmoSample.udfGroupName
        this.isotopeLabel = pmoSample.udfIsotopeLabel
    }

    void updateUdfs(SampleQcHamiltonAnalyte analyte, BigDecimal minimumIsotopeEnrichment){
        analyte.udfIsotopeEnrichment = isotopeEnrichment
        analyte.udfMetabolomicsSampleId = metabolomicsSampleId
        if (isotopeEnrichment < minimumIsotopeEnrichment) {
            analyte.udfSampleQCResult = Analyte.FAIL
            analyte.udfSampleQCFailureMode = SAMPLE_QC_FAILURE_MODE
        }
        else {
            analyte.udfSampleQCResult = Analyte.PASS
            analyte.udfSampleQCFailureMode = ''
        }
    }
}
