package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibrarybinding

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.NodeManagerUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryBindingRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryBindingRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        assignToNextWorkflow()
    }

    void assignToNextWorkflow(){
        List<Analyte> pacBioSequel = []
        NodeManagerUtility nmu = new NodeManagerUtility(process.nodeManager)
        process.inputAnalytes.each { Analyte analyte -> //input is PacBioAnnealingComplex, please see RequeueLibraryBindingPreProcessValidation
            PacBioAnnealingComplex annealingComplex = analyte as PacBioAnnealingComplex
            nmu.getBindingComplexes(annealingComplex.parentAnalyte)?.each { PacBioBindingComplex bindingComplex ->
                process.removeAnalyteFromActiveWorkflows(bindingComplex)
                logger.info("Removed $bindingComplex from all workflows")
            }
            SequencerModelCv sequencerModelCv = annealingComplex.parentAnalyte.sequencerModelCv
            if (sequencerModelCv.isSequel) {
                pacBioSequel << analyte
            } else {
                throw new WebException("${analyte}: no handler for sequencer model [${sequencerModelCv.sequencerModel}]",422)
            }
        }
        process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_BINDING,pacBioSequel)
    }
}
