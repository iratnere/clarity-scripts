package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel

import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.SowItemQc

/**
 * Created by lvishwas on 6/16/2015.
 */
class LastKeyValueSection {
    @FieldMapping(header = 'Library Creation Queue', cellType = CellTypeEnum.STRING, required = true)
    public def libraryCreationQueue
    @FieldMapping(header = 'Target Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER, required = true)
    public def targetAliquotMass
    @FieldMapping(header = 'Sow Item Status', cellType = CellTypeEnum.DROPDOWN, required = true)
    public DropDownList sowItemStatus
    @FieldMapping(header = 'Failure Mode', cellType = CellTypeEnum.DROPDOWN)
    public DropDownList failureMode
    @FieldMapping(header = 'Aliquot Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def aliquotVolume

    static final String DEFAULT_SOW_QC_FAILURE_MODE = 'Sample Failed QC'

    String validate(String sheetName){
        String errors = ''
        if(!libraryCreationQueue){
            errors += "Library Creation Queue is a required field (container ${sheetName})\n"
        }
        if(!targetAliquotMass || parseToBigDecimalError(targetAliquotMass)){
            errors += "Target Aliquot Mass is a required field (container ${sheetName})\n"
        }
        if(!sowItemStatus || !sowItemStatus.value){
            errors += "Plate Sow Item Status is a required field (container ${sheetName})\n"
        }
        if(sowItemStatus?.value == Analyte.FAIL && !failureMode?.value){
            errors += "Failure Mode is required if the Plate SOW QC status is ${Analyte.FAIL} (container ${sheetName})\n"
        }
        return errors
    }

    void prepareDropDowns(){
        sowItemStatus = SowItemQc.dropDownPassFail
        failureMode = SowItemQc.dropDownSowQCFailureModes
    }

    void populateBean(ScheduledSample scheduledSample){
        prepareDropDowns()
        LibraryCreationQueueCv libraryCreationQueueCv = scheduledSample.libraryCreationQueue
        libraryCreationQueue = libraryCreationQueueCv?.libraryCreationQueue
        targetAliquotMass = libraryCreationQueueCv?.plateTargetMassLibTrialNg
        boolean plateQCFailed = scheduledSample.pmoSample.udfContainerSampleQcResult == Analyte.FAIL
        sowItemStatus.value = plateQCFailed? Analyte.FAIL:''
        failureMode.value = plateQCFailed?DEFAULT_SOW_QC_FAILURE_MODE:''
    }

    void updateUdfs(ScheduledSample scheduledSample){
        scheduledSample.udfSowItemQcResult = sowItemStatus.value
        scheduledSample.udfSowItemQcFailureMode = failureMode.value
        scheduledSample.udfTargetAliquotMassNg = targetAliquotMass
    }

    static def parseToBigDecimalError(def object) {
        try{
            object as BigDecimal
        } catch (e) {
            return e
        }
        return null
    }
}
