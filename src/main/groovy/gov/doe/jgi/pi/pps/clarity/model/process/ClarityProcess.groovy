package gov.doe.jgi.pi.pps.clarity.model.process

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import gov.doe.jgi.pi.pps.clarity.model.researcher.ResearcherFactory
import gov.doe.jgi.pi.pps.clarity.scripts.services.RoutingServiceImpl
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorklowStageStatus
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import gov.doe.jgi.pi.pps.util.util.OnDemandCacheMapped
import groovy.text.SimpleTemplateEngine
import groovy.util.logging.Slf4j
import org.slf4j.LoggerFactory

import java.util.regex.Matcher
import java.util.regex.Pattern

@Slf4j
class ClarityProcess {

    static final logger = LoggerFactory.getLogger(ClarityProcess.class)

    static ProcessType processType = null
    static List<ProcessType> processTypes = null

    protected LinkedHashMap<String,Class<ActionHandler>> actionHandlers
    protected String lastActionKey

    private OnDemandCache<List<Analyte>> cachedInputAnalytes = new OnDemandCache<>()
    private OnDemandCache<List<Analyte>> cachedOutputAnalytes = new OnDemandCache<>()
    private OnDemandCacheMapped<Analyte,List<Analyte>> cachedAnalyteInputAnalytes = new OnDemandCacheMapped<>()
    private OnDemandCacheMapped<Analyte,List<Analyte>> cachedAnalyteOutputAnalytes = new OnDemandCacheMapped<>()

    static final String LCPlateSingleCellInternalDefaultProcessPrinter = 'ECHO_BCode8'

    private final static String WATCH_FOLDER = "${File.separator}mnt${File.separator}bartender${File.separator}to_be_printed"
    private final static String WATCH_FOLDER_LOCAL = "${System.getProperty("user.home")}${File.separator}bartender${File.separator}to_be_printed"
    private final static String HEADER_START =
            '%BTW% /AF="${File.separator}${File.separator}BARTENDER${File.separator}shared${File.separator}templates${File.separator}JGI_Label_${printer}.btw" /D="%Trigger File Name%" /PRN="${printer}" /R=3 /P /DD'
    private final static String HEADER_START_ECHO_BCODE8 =
            '%BTW% /AF="${File.separator}${File.separator}BARTENDER${File.separator}shared${File.separator}templates${File.separator}${printer}.btw" /D="%Trigger File Name%" /PRN="bcode8" /R=3 /P /DD'
    private final static String HEADER_END = '%END%'
    final static String WINDOWS_NEWLINE = '\r\n'
    static final PRINT_LABELS_ACTION = 'Print Labels'
    final static String DEFAULT_PROCESS_PRINTER = 'dummy'

    ProcessNode processNode
    Researcher processResearcher
    OnDemandCache<Stage> cachedInputStage = new OnDemandCache<>()

    final List<RoutingRequest> routingRequests = []

    void setActionHandlers(LinkedHashMap actionHandlers) {
        this.actionHandlers = (LinkedHashMap<String,Class<ActionHandler>>) actionHandlers
        this.lastActionKey = actionHandlers.keySet().last()
    }

    ClarityProcess(ProcessNode processNode) {
        this.processNode = processNode
        def pInputs = grails.async.Promises.task { getInputAnalytes() }
        def pOutputs = grails.async.Promises.task { getOutputAnalytes() }
        pInputs.onError { Throwable t ->
            log.error "${this} error fetching inputs", t
        }
        pOutputs.onError { Throwable t ->
            log.error "${this} error fetching outputs", t
        }
        pInputs.onComplete { "${this} done fetching inputs in background"}
        pOutputs.onComplete { "${this} done fetching outputs in background"}
    }

    Boolean getIsOutputProcess() {
        processNode.isOutputProcess
    }

    void refresh() {
        nodeManager.clearSelectCache(inputAnalytes*.artifactNode)
        refreshInputs()
        refreshOutputs()
        processNode.httpRefresh()
    }

    void refreshOutputs() {
        nodeManager.clearSelectCache(outputAnalytes*.artifactNode)
        cachedOutputAnalytes.clear()
        cachedAnalyteOutputAnalytes.clear()
    }

    void refreshInputs() {
        nodeManager.clearSelectCache(inputAnalytes*.artifactNode)
        cachedInputAnalytes.clear()
        cachedAnalyteInputAnalytes.clear()
    }

    final List<Analyte> getInputAnalytes() {
        cachedInputAnalytes.fetch {
            //processNode.inputAnalytes.collect{AnalyteFactory.analyteInstance(it)}}
            nodeManager.getArtifactNodesFetchRelatives(processNode.inputArtifactIds).collect {
                AnalyteFactory.analyteInstance(it)
            }
        }
    }

    final List<Analyte> getOutputAnalytes() {
        cachedOutputAnalytes.fetch {
            //processNode.outputAnalytes.collect { AnalyteFactory.analyteInstance(it) }
            nodeManager.getArtifactNodesFetchRelatives(processNode.outputAnalyteIds).collect {
                AnalyteFactory.analyteInstance(it)
            }
        }
    }

    final List<Analyte> getOutputAnalytes(Analyte analyte) {
        cachedAnalyteOutputAnalytes.fetch(analyte) { Analyte alyte ->
            processNode.getOutputAnalytes(alyte.id).collect { ArtifactNode artifactNode ->
                AnalyteFactory.analyteInstance(artifactNode)
            }
        }
    }

    final List<Analyte> getInputAnalytes(Analyte analyte) {
        cachedAnalyteInputAnalytes.fetch(analyte) { Analyte alyte ->
            processNode.getInputAnalytes(alyte.id).collect{ ArtifactNode artifactNode ->
                AnalyteFactory.analyteInstance(artifactNode)
            }
        }
    }

    Boolean getIsPacBio() {
        inputStage?.isPacBio
    }

    Boolean getIsPacBioSequel() {
        inputStage?.isPacBioSequel
    }

    Stage getInputStage() {
        return cachedInputStage.fetch {
            boolean staticMode = true
            return inputAnalytes?.find()?.stageInProgress(processNode, staticMode)
        }
    }

    void setArtifactActions(Collection<NextAction> nextActions){
        log.info "${nextActions}"
        ActionsNode actionsNode = processNode.actionsNode
        actionsNode.nextActions.clear()
        actionsNode.nextActions.addAll(nextActions)
        actionsNode.httpPut()
    }

    void completeRepeat(Set<String> repeatIds=null) {
        if (isOutputProcess) {
            completeRepeatOutputs(repeatIds)
        } else {
            completeRepeatInputs(repeatIds)
        }
    }

    void completeRepeatInputs(Set<String> repeatIds=null) {
        List<NextAction> nextActions = []
        logger.info "repeating ${inputAnalytes.size()} inputs"
        inputAnalytes.each { Analyte inputAnalyte ->
            if (repeatIds?.contains(inputAnalyte.id)) {
                nextActions << inputAnalyte.getNextAction(ActionType.REPEAT, '')
            } else {
                nextActions << inputAnalyte.getNextAction(ActionType.COMPLETE, '')
            }
        }
        if (nextActions) {
            setArtifactActions(nextActions)
        }
    }

    void completeRepeatOutputs(Set<String> repeatIds=null) {
        List<NextAction> nextActions = getNextActions(repeatIds)
        setArtifactActions(nextActions)
    }

    List<NextAction> getNextActions(Set<String> repeatIds){
        return outputAnalytes.collect{
            def nextAction = it.id in repeatIds ? ActionType.REPEAT : ActionType.COMPLETE
            it.getNextAction(nextAction)
        }
    }

    ActionHandler getActionHandler(String action) {
        ActionHandler actionHandler = actionHandlers?.get(action)?.newInstance() ?: new ActionHandler()
        actionHandler.process = this
        actionHandler.action = action
        if (action == lastActionKey) {
            actionHandler.lastAction = true
        }
        actionHandler
    }

    ActionHandler getLastActionHandler(){
        ActionHandler actionHandler = actionHandlers?.values()?.last()?.newInstance() ?: new ActionHandler()
        actionHandler.process = this
        actionHandler.lastAction = true
        actionHandler
    }

    String getId(){
        return processNode.id
    }

    void setCompleteStage() {
        if (isOutputProcess) {
            setOutputsCompleteStage()
        } else {
            setInputsCompleteStage()
        }
    }

    void setInputsCompleteStage() {
        ActionsNode actionsNode = processNode.actionsNode
        Collection<NextAction> artifactActions = processNode.inputAnalytes.collect{ArtifactNode artifactNode -> artifactNode.nextAction(ActionType.COMPLETE)
            //not needed "isControlAnalyte" check -> control samples are configured as a "single step control" sample (no output anlyte)
            //artifactNode.isControlAnalyte ? artifactNode.nextAction(ActionType.REMOVE) : artifactNode.nextAction(ActionType.COMPLETE)
        }
        actionsNode.nextActions.addAll(artifactActions)
        actionsNode.httpPut() //TODO: should this be here?
    }

    void setOutputsCompleteStage() {
        ActionsNode actionsNode = processNode.actionsNode
        Collection<NextAction> artifactActions = processNode.outputAnalytes.collect{ArtifactNode artifactNode ->
            //needed "isControlAnalyte" check (Phix) -> Phix will be placed on flowcell lane
            //Single Step Controls must have Remove from workflow as the next action
            artifactNode.isControlAnalyte ? artifactNode.nextAction(ActionType.REMOVE) : artifactNode.nextAction(ActionType.COMPLETE)
        }
        actionsNode.nextActions.addAll(artifactActions)
        actionsNode.httpPut() //TODO: should this be here?
    }

    void processRoutingRequests() {
        if (!routingRequests) {
            return
        }
        RoutingServiceImpl routingService = BeanUtil.getBean(RoutingServiceImpl.class)
        routingService.submitRoutingRequests(routingRequests)
        routingRequests.clear()
    }

    void routeArtifactIdsToUri(String uri, Collection<String> artifactIds){
        routingRequests.addAll(RoutingRequest.generateRequestsToRouteArtifactIdsToUri(uri, artifactIds))
    }

    void routeArtifactIds(Stage stage, Collection<String> artifactIds) {
        logger.info "Routing artifact ids <$artifactIds> to stage ${stage.name()}<${stage?.value}>"
        if (!artifactIds) {
            return
        }
        routeArtifactIdsToUri(stage.uri, artifactIds)
    }

    void routeArtifactNodes(Stage stage, Collection<ArtifactNodeInterface> artifactNodes) {
        logger.info "Routing artifact nodes <$artifactNodes> to stage <${stage?.value}>"
        routeArtifactIdsToUri(stage.uri, artifactNodes*.id)
    }

    void removeAnalyteFromActiveWorkflows(Analyte analyte) {
        analyte.artifactNodeInterface.workflowStages.collect { ArtifactWorkflowStage workflowStage ->
            log.debug "${this}.removeAnalyteFromActiveWorkflows(${analyte}): ${workflowStage} status ${workflowStage.status}"
            ClarityWorkflow clarityWorkflow = null
            if (workflowStage.status.active) {
                clarityWorkflow = ClarityWorkflow.toEnum(workflowStage.workflowNode.name)
                if (!clarityWorkflow) {
                    log.error "${this}: for analyte ${analyte}, failed to map active workflow [${workflowStage.workflowNode.name}] to enum"
                }
            } else {
                log.debug "${this}: ${workflowStage} of analyte ${analyte} is inactive, status is ${workflowStage.status}"
            }
            clarityWorkflow
        }.findAll().unique().each { ClarityWorkflow clarityWorkflow ->
            log.info "${this}: removing ${analyte} from workflow [${clarityWorkflow.value}]"
            removeAnalyteFromWorkflow(analyte, clarityWorkflow)
        }
    }

    void removeAnalyteFromWorkflow(Analyte analyte, ClarityWorkflow workflow) {
        if (analyte && workflow) {
            routingRequests << new RoutingRequest(routingUri: workflow.uri, artifactId: analyte.id, action: Routing.Action.unassign)
        }
    }

    void removeAnalytesFromWorkflow(Collection<Analyte> analytes, ClarityWorkflow workflow) {
        analytes?.each { Analyte analyte ->
            removeAnalyteFromWorkflow(analyte,workflow)
        }
    }

    void removeAnalyteFromStage(Analyte analyte, Stage stage) {
        if (analyte && stage) {
            routingRequests << new RoutingRequest(routingUri: stage.uri, artifactId: analyte.id, action: Routing.Action.unassign)
        }
    }

    void removeAnalytesFromStage(Collection<Analyte> analytes, Stage stage) {
        analytes?.each { Analyte analyte ->
            removeAnalyteFromStage(analyte,stage)
        }
    }

    void unassignQueuedAnalytes(Collection<Analyte> analytes) {
        analytes.each {
            it.artifactNodeInterface.activeStages.findAll {
                ArtifactWorklowStageStatus.QUEUED == ArtifactWorklowStageStatus.toEnum(it.status)
            }.each { ArtifactWorkflowStage stage ->
                logger.info "Removing analyte <$it> from stage <$stage.uri>"
                routingRequests << new RoutingRequest(routingUri: stage.uri, artifactId: it.id, action: Routing.Action.unassign)
            }
        }
    }

    void routeAnalytes(Stage stage, Collection<Analyte> analytes) {
        logger.info "Routing analytes <$analytes> to stage ${stage.name()}<${stage?.value}>"
        if (!analytes) {
            return
        }
        routeArtifactIdsToUri(stage.uri, analytes*.id)
    }

    void moveOutputsToStage(Stage stage) {
        routeArtifactIds(stage,processNode.outputAnalyteIds)
    }

    void moveInputsToStage(Stage stage) {
        routeArtifactIds(stage,processNode.inputArtifactIds)
    }

    StepsNode getStepsNode() {
        processNode.stepsNode
    }

    NodeManager getNodeManager() {
        return processNode.nodeManager
    }

    NodeConfig getNodeConfig() {
        return nodeManager.nodeConfig
    }

    Date getRunDate() {
        return processNode.getDateRunAsDate()
    }

    @Override
    String toString() {
        return "script:${this.class.simpleName},process ID:${processNode?.id},process type:${processNode?.processType}"
    }

    ArtifactNode getFileNode(String fileName) {
        ArtifactNode fileNode = processNode.outputResultFiles.find { it.name.contains(fileName) }
        if (!fileNode || !fileNode.id) {
            throw new WebException([code:'fileNode.null', args:[fileName]], 422)
        }
        fileNode
    }

    Researcher getResearcher() {
        if (!processResearcher)
            processResearcher = ResearcherFactory.researcherForLimsId(processNode.nodeManager, processNode.technicianId)
        processResearcher
    }

    String getExecutedBy() {
        Researcher executor = getResearcher()
        if (executor) {
            return executor.contactId?.toString() ?: executor.fullName
        }
        return 'unknown'
    }


    //https://docs.google.com/document/d/12nMDl5KBM7R5tkHmQ5yQyGQkK1Q_jXhMLj0iT28ezPM/edit
    void writeLabels(List<? extends Analyte> analyteList) {
        def lines = collectPrintLines(analyteList)
        logger.info "${lines}"
        createFile(lines, udfProcessPrinter)
    }

    void writeLabels(Set<String> lines) {
        logger.info "${lines}"
        createFile(lines, udfProcessPrinter)
    }

    private static Set<String> collectPrintLines(List<? extends Analyte> analyteList) {
        Set<String> lines = []
        analyteList.each {
            if (it.printLabelText)
                lines << it.printLabelText
        }
        return lines
    }

    static String getStartString(String printer) {
        if (LCPlateSingleCellInternalDefaultProcessPrinter == printer)
            return HEADER_START_ECHO_BCODE8
        return HEADER_START
    }

    private static void createFile(Set<String> lines, String printer) {
        if (!lines ) {
            logger.info "nothing to print"
            return
        }

        String filename =  UUID.randomUUID().toString() + '.txt'
        def binding = ['printer':printer]
        def engine = new SimpleTemplateEngine()

        def template = engine.createTemplate(getStartString(printer)).make(binding)
        String headerStart = template.toString()

        StringBuilder builder = new StringBuilder()
        builder.append(headerStart + WINDOWS_NEWLINE)
        builder.append(HEADER_END + WINDOWS_NEWLINE)
        builder.append(WINDOWS_NEWLINE)
        lines.each { String line ->
            builder.append(line + WINDOWS_NEWLINE)
        }
        File f = new File(WATCH_FOLDER)
        String path = f.exists() && f.isDirectory() ? WATCH_FOLDER : WATCH_FOLDER_LOCAL

        filename = "${path}${File.separator}${filename}"
        new File(filename).withWriter { out ->
            out.write(builder.toString())
        }
        logger.info "Contents of ${filename} - \n${builder.toString()}"
    }

    BigDecimal getUdfProcessMinDnaConcNgUl(){
        return processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_MIN_DNA_CONCENTRATION_NG_UL.udf)
    }

    String getUdfProcessFlowcellBarcode(){
        return processNode.getUdfAsString(ClarityUdf.PROCESS_FLOWCELL_BARCODE.udf)
    }

    String getUdfProcessPrinter() {
        String printer = processNode.getUdfAsString(ClarityUdf.PROCESS_BARCODE_PRINTER.udf)
        if (!printer)
            return DEFAULT_PROCESS_PRINTER
        printer
    }

    String getUdfProcessQcTodo() {
        return processNode.getUdfAsString(ClarityUdf.PROCESS_QC_TODO.udf)
    }

    ProgramStatusNode getProgramStatusNode(){
        nodeManager.getProgramStatusNode(processNode.id)
    }

    void postErrorStatus(String message){
        if (!processNode?.id) {
            return //tests
        }
        message = "Executing process ${processNode.id}\n"+ message
        try{
            programStatusNode?.postErrorMsg(message)
        } catch(ignore){}
    }

    static void postErrorMessage(String message){
        throw new WebException(message,500)
    }

    void postError(String message) {
        programStatusNode?.postErrorMsg(message)
    }

    boolean getIsTestMode(){
        if(processNode.researcherNode.username in ['test_framework','Migration'])
            return true
        return false
    }

    void postInfoMessage(String message){
        message = "Executing process ${processNode.id}\n"+ message
        try{
            if(!isTestMode)
                programStatusNode?.postInfoMsg(message)
        }
        finally {
            logger.info message
        }
    }

    void postWarningMessage(String message){
        message = "Executing process ${processNode.id}\n"+ message
        try{
            if(!isTestMode)
                programStatusNode?.postWarningMsg(message)
        }
        finally{
            logger.warn message
        }
    }

    def getProcessTrigger(def action) {
        ProtocolNode protocolNode = stepsNode.protocolNode
        def triggers = protocolNode?.getEppTriggers()
        def currentTrigger = triggers.find { it.@'name' == action}
        if (!triggers || !currentTrigger) {
            def message = """
                ProtocolNode $protocolNode.id: cannot find the '$action' trigger.
                Please check Clarity Configuration. The action( -A) must have the same name as the trigger.
            """
            logger.warn(message)
            ProgramStatusNode programStatusNode = BeanUtil.nodeManager.getProgramStatusNode(processNode.id)
            programStatusNode?.postErrorMsg(message)
        }
        currentTrigger
    }

    def getAnalytesToPrintLabels(){
        return null
    }

    boolean getPrintLabelsTriggerUnused() {
        def printTrigger = getProcessTrigger(PRINT_LABELS_ACTION)
        return printTrigger.@'type' != 'MANUAL'
    }

    String getProcessLink(){
        //https://tempest.jgi-psf.org/api/v2/processes/<processId>
        String regex = "(https?:\\/\\/[^\\/]+)\\/[^-]+-([0-9]+)"
        Pattern p = Pattern.compile(regex)
        Matcher m = p.matcher(processNode.url)
        if(m.matches())
            return "${m.group(1)}/clarity/work-complete/${m.group(2)}"
        return ''
    }

    Long getResearcherContactId(){
        def contactId = researcher.contactId
        if(!contactId)
            throw new RuntimeException("Researcher ${researcher.researcherNode.id} does not have a contact id. Please update researcher information")
        return contactId
    }

    /*
    batch lookup for efficiency PPS-4781
    method implemented only for SampleAliquotCreationProcess
     */
    List<SampleNode> getBatchPmoSampleNodes() {
        return null
    }

    /*
    batch lookup for efficiency PPS-4781
    method implemented only for SampleAliquotCreationProcess
    */
    List<ArtifactNode> getBatchPmoSampleArtifactNodes() {
        return null
    }

    /*
    batch lookup for efficiency PPS-4781
    method implemented only for SampleAliquotCreationProcess
    */
    List<ContainerNode> getBatchPmoSampleContainerNodes() {
        return null
    }

    def getFreezerContainers(){
        throw new WebException([code:'ClarityProcess.getFreezerContainers().notImplemented', args:[this.toString()]], 422)
    }

    def getStatusCv() {
        throw new WebException([code:'ClarityProcess.getStatusCv().notImplemented', args:[this.toString()]], 422)
    }

    def getFailureModeCv() {
        throw new WebException([code:'ClarityProcess.getFailureModeCv().notImplemented', args:[this.toString()]], 422)
    }

    boolean getTestMode(){
        return false
    }
}
