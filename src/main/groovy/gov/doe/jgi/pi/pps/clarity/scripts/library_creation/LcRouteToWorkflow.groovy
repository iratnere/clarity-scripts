package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class LcRouteToWorkflow extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(LcRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        doRefresh()
        performLastStepActions()
    }

    void performLastStepActions() {
        ClarityProcess clarityProcess = process as LibraryCreationProcess
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.updateLibraryPoolUdfs()
        freezerService.freezerCheckoutInputAnalytes(clarityProcess)
        clarityProcess.lcAdapter.updateInputsAndSamples()
        if (clarityProcess.printLabelsTriggerUnused)
            clarityProcess.lcAdapter.createPrintLabelsFile()
        clarityProcess.lcAdapter.createPoolPrintLabelsFile()
        clarityProcess.lcAdapter.updateLcaUdfs()
        clarityProcess.lcAdapter.moveToNextWorkflow()
    }
}