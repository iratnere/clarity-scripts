package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation

class PoolingPacBioDualIndexes extends PoolingPacBioSingleIndexes  {
    PoolingPacBioDualIndexes(List<LibraryInformation> members) {
        super(members)
    }

    @Override
    def getIndexToCompare(LibraryInformation member) {
        return member.indexName
    }

    Map<String, List<LibraryInformation>> getIndexMembersMap(){
        Map<String, List<LibraryInformation>> indexMembers = [:].withDefault {[] as Set}
        members.each{ LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            String[] dualIndexSplit = indexToCompare.split("--")
            dualIndexSplit.each { String indexPart ->
                String uniqueIndexName = indexPart.substring(0, indexPart.indexOf("_"))
                indexMembers[uniqueIndexName] << li
            }
        }
        return indexMembers
    }

    @Override
    Map validate() {
        Map<String,List<String>> findings = [:].withDefault {[]}
        Map<String, List<LibraryInformation>> repSequenceToBeans = indexMembersMap?.findAll { it.value.size() > 1 }
        if(repSequenceToBeans)
            findings[ERROR] << "pool #$poolNumber has duplicate indexes: $repSequenceToBeans"
        return findings
    }
}
