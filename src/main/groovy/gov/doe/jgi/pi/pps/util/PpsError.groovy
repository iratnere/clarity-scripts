package gov.doe.jgi.pi.pps.util

import groovy.transform.Canonical
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Canonical
class PpsError {
    static final Logger log = LoggerFactory.getLogger(PpsError.class)
    String key
    String message

    PpsError(String key, String message) {
        this.key = tr(key)
        this.message = message
    }

    PpsError(String key, Exception ex) {
        log.error(ex.message, ex)
        this.key = tr(key)
        this.message = ex.message
    }

    private String tr(String key) {
        key.collect {
            Character.isUpperCase(it as Character) ? "-${it.toLowerCase()}" : it
        }.join()
    }
}
