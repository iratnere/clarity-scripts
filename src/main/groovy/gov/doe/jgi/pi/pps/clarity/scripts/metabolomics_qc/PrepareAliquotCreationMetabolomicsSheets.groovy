package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AliquotCreationSheet
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AliquotCreationSheetParams
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.LoggerFactory

class PrepareAliquotCreationMetabolomicsSheets extends ActionHandler {

    MetabolomicsQCProcess qcProcess

    static final logger = LoggerFactory.getLogger(PrepareAliquotCreationMetabolomicsSheets.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        qcProcess = (MetabolomicsQCProcess)process
        uploadAliquotWorksheet()
        uploadMetabolomicsWorksheet()
    }

    void uploadAliquotWorksheet() {
        String fileName = qcProcess.SCRIPT_GENERATED_ALIQUOT_SHEET
        ArtifactNode fileNode = qcProcess.getFileNode(fileName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        AliquotCreationSheetParams params = new AliquotCreationSheetParams(qcProcess)
        AliquotCreationSheet acSheet = new AliquotCreationSheet(params, qcProcess)
        ExcelWorkbook excelWorkbook = acSheet.aliquotCreationSheet
        excelWorkbook.store(process.nodeManager.nodeConfig, fileNode.id)
    }

    void uploadMetabolomicsWorksheet() {
        String fileName = qcProcess.SCRIPT_GENERATED_METABOLOMICS_SHEET
        ArtifactNode fileNode = qcProcess.getFileNode(fileName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        ExcelWorkbook excelWorkbook = populateMetabolomicsSheet()
        excelWorkbook.store(process.nodeManager.nodeConfig, fileNode.id)
    }

    ExcelWorkbook populateMetabolomicsSheet(){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(qcProcess.METABOLOMICS_SHEET_TEMPLATE_NAME, qcProcess.testMode)
        excelWorkbook.addSection(metabolomicsTableSection)
        return excelWorkbook
    }

    List<Analyte> getSortedOutputAnalytes(List<Analyte> inputAnalytes = qcProcess.sortInputAnalytes()) {
        return inputAnalytes.collect{ qcProcess.getOutputAnalytes(it) }.flatten()
    }

    Section getMetabolomicsTableSection(List<Analyte> analytes = sortedOutputAnalytes) {
        List<MetabolomicsTableBean> beans = []
        analytes.each { Analyte analyte ->
            MetabolomicsTableBean bean = new MetabolomicsTableBean()
            bean.populateBean(analyte)
            beans << bean
        }
        Section tableSection = tableSection
        tableSection.setData(beans)
        return tableSection
    }

    TableSection getTableSection(){
        return new TableSection(0, MetabolomicsTableBean.class, 'A1',null, true)
    }
}
