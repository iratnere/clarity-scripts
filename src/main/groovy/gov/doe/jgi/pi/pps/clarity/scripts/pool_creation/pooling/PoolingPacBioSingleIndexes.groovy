package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.BasicPoolingAlgorithm
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation

class PoolingPacBioSingleIndexes extends BasicPoolingAlgorithm {
    Integer degreeOfPooling = null

    PoolingPacBioSingleIndexes(List<LibraryInformation> members) {
        super(members)
    }

    @Override
    int getDop() {
        if(!degreeOfPooling)
            degreeOfPooling = members.first().dop
        return degreeOfPooling
    }

    @Override
    boolean getCanPool(){
        return false
    }
}
