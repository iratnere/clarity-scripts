package gov.doe.jgi.pi.pps.clarity.util

import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * Created by datjandra on 5/14/2015.
 */
final class ClarityStringUtils {

    private final static DateFormat CLARITY_DATE_FORMAT = new SimpleDateFormat('yyyy-MM-dd')

    private ClarityStringUtils() {}

    static String extendedTrim(String str) {
        return (str ? str.replaceAll("\u00a0", "\u0020").trim() : "")
    }

    static String toClarityDate(Date date) {
        return CLARITY_DATE_FORMAT.format(date)
    }

}
