package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive

enum ProcessType {
	SM_SAMPLE_RECEIPT('SM Sample Receipt'),
	SM_SHIP_OFFSITE('SM Ship Off-site'),
	SM_TRASH('SM Trash'),
	SM_ON_SITE('SM On Site'),
	SA_ADJUST_MASS('SA Adjust Mass'),
	SA_ADJUST_MOLARITY('SA Adjust Molarity'),
	SA_ADJUST_VOLUME('SA Adjust Volume'),

	SM_SAMPLE_QC('SM Sample QC',{ArtifactNodeInterface artifactNode -> new SampleQcHamiltonAnalyte(artifactNode)}),
	SM_SAMPLE_FRACTIONATION('SM Sample Fractionation',{ArtifactNodeInterface artifactNode -> new Analyte(artifactNode)}),
	SM_SAMPLE_METABOLOMICS_QC('SM Sample QC Metabolomics',{ArtifactNodeInterface artifactNode -> new SampleQcHamiltonAnalyte(artifactNode)}),
	SM_SOW_ITEM_QC('SM SOW Item QC'),
	SM_APPROVE_FOR_SHIPPING('SM Approve For Shipping'),
	AC_ALIQUOT_CREATION('AC Sample Aliquot Creation',{ArtifactNodeInterface artifactNode -> new ClaritySampleAliquot(artifactNode)}),
	LC_LIBRARY_CREATION('LC Library Creation',{ArtifactNodeInterface artifactNode -> new ClarityLibraryStock(artifactNode)}),
	LC_PLATE_TRANSFER('Plate To Tube Transfer',{ArtifactNodeInterface artifactNode -> new ClarityLibraryStock(artifactNode)}),
	LQ_LIBRARY_QPCR('LQ Library qPCR',{ArtifactNodeInterface artifactNode -> new Analyte(artifactNode)}),
	SIZE_SELECTION('Size Selection'),
	LP_POOL_CREATION('LP Pool Creation',{ArtifactNodeInterface artifactNode ->
		if (artifactNode.parentAnalytes.find{it.parentProcessType == LP_POOL_CREATION.value}) {
			return new ClarityPoolLibraryPool(artifactNode) //PPS-5052: to support mixed pools
		}
		new ClarityLibraryPool(artifactNode)}),
	PACBIO_LIBRARY_ANNEALING('PacBio Library Annealing',{ArtifactNodeInterface artifactNode -> new PacBioAnnealingComplex(artifactNode)}),
	PACBIO_LIBRARY_BINDING('PacBio Library Binding',{ArtifactNodeInterface artifactNode -> new PacBioBindingComplex(artifactNode)}),
	PACBIO_SEQUENCING_PLATE_CREATION('PacBio Sequencing Plate Creation',{ArtifactNodeInterface artifactNode -> new PacBioSequencingTemplate(artifactNode)}),
	PACBIO_SEQUENCING_COMPLETE('PacBio Sequencing Complete'),
	PACBIO_MAGBEAD_CLEANUP('PacBio MagBead Cleanup'),
	SQ_SEQUENCING('SQ Sequencing',{ArtifactNodeInterface artifactNode -> new ClarityPhysicalRunUnit(artifactNode)}),
	SQ_SEQUENCE_ANALYSIS('SQ Sequence Analysis'),
	REQUEUE_LIBRARY_CREATION('Requeue for Library Creation'),
	REQUEUE_QC('Requeue for Sample QC'),
	REQUEUE_LIBRARY_ANNEALING('Requeue for Library Annealing'),
	REQUEUE_LIBRARY_BINDING('Requeue for Library Binding'),
	ABANDON_WORK('Abandon Work'),
	PRINT_LABELS('Print Labels'),
	PLACE_ON_HOLD('Place on Hold'),
	PLACE_IN_NEEDS_ATTENTION('Place in Needs Attention'),
	RELEASE_FROM_HOLD('Release from Hold'),
	RELEASE_FROM_NEEDS_ATTENTION('Release from Needs Attention'),
	ABANDON_QUEUE('Abandon Queue'),
	ONBOARD_SEQUENCING_FILES('Onboard Sequencing File'),
	ONBOARD_LIBRARY_FILES('Onboard Libraries'),
	RECEIVE_SAMPLES('Receive Samples From Barcode Association')

	final String value
	final Closure<Analyte> outputAnalyteGenerator

	ProcessType(String value, Closure<Analyte> outputAnalyteGenerator = null) {
		this.value = value
		this.outputAnalyteGenerator = outputAnalyteGenerator
	}

    private static final EnumConverterCaseInsensitive<ProcessType> converter = new EnumConverterCaseInsensitive(ProcessType)

    static ProcessType toEnum(value) {
        return converter.toEnum(value)
    }

	String toString() {
		value
	}

}
