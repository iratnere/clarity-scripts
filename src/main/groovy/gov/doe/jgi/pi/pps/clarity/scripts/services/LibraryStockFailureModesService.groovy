package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.domain.LibraryStockFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LibraryStockFailureModesService {

    @Autowired
    DropDownService dropDownService

    static final String[] EXCLUDED_FAILURE_MODES = ['Abandoned Work']

    String[] getActiveFailureModes() {
        return LibraryStockFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }

    String[] getValidFailureModes() {
        return getActiveFailureModes()?.minus(EXCLUDED_FAILURE_MODES)
    }

    DropDownList getDropDownFailureModes() {
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(validFailureModes)
        return dropDownList
    }

    DropDownList getDropDownPassFail() {
        return dropDownService.dropDownPassFail
    }
}
