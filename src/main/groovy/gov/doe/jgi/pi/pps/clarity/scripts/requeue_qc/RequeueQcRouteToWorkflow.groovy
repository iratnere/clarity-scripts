package gov.doe.jgi.pi.pps.clarity.scripts.requeue_qc

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 8/19/15.
 */
class RequeueQcRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueQcRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        updateSampleUdfs()
        updateSampleStatuses()
        assignToNextWorkflow()
    }

    void updateSampleUdfs(List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>) {
        inputAnalytes.each { SampleAnalyte sampleAnalyte ->
            PmoSample pmoSample = (PmoSample) sampleAnalyte.claritySample
            pmoSample.udfQcTypeId = qcType.id
        }
    }

    QcTypeCv getQcType() {
        QcTypeCv qcType = QcTypeCv.findByQcType(process.udfProcessQcTodo)
        if (!qcType) {
            process.postErrorMessage("Cannot find qc type by the process ${ClarityUdf.PROCESS_QC_TODO.value} udf '${process.udfProcessQcTodo}'")
        }
        return qcType
    }

    void updateSampleStatuses(
            List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>,
            StatusService service = BeanUtil.getBean(StatusService.class)
    ) {
        def sampleQcCompleteMap = [:]
        inputAnalytes.each { SampleAnalyte sampleAnalyte ->
            PmoSample pmoSample = (PmoSample) sampleAnalyte.claritySample
            sampleQcCompleteMap[pmoSample.pmoSampleId] = SampleStatusCv.AWAITING_SAMPLE_QC
        }
        service.submitSampleStatus(sampleQcCompleteMap, process.researcherContactId)
    }

    void assignToNextWorkflow(List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>) {
        List<ArtifactNode> dnaSamples = []
        List<ArtifactNode> rnaSamples = []
        inputAnalytes.each { SampleAnalyte sampleAnalyte ->
            ClaritySample claritySample = sampleAnalyte.claritySample
            if (claritySample.isDnaProject) {
                dnaSamples << claritySample.sampleArtifactNode
            } else {
                rnaSamples << claritySample.sampleArtifactNode
            }
        }
        process.routeArtifactNodes(Stage.SAMPLE_QC_DNA,dnaSamples)
        process.routeArtifactNodes(Stage.SAMPLE_QC_RNA,rnaSamples)
    }

}
