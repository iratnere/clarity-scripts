package gov.doe.jgi.pi.pps.util

import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

import javax.sql.DataSource
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="clarity")
class ClaritySettings implements DisposableBean {
    String url
    String user
    String password
    String contactsUrl
    String pmoUrl
    String pmoWsUrl
    String dapSeqUri
    String sftpUrl
    String sftpUser
    String sftpPassword

    @Autowired
    DataSource clarityDataSource

    final static int THREADS = 10
    static ExecutorService executorService = Executors.newFixedThreadPool(THREADS)

    NodeConfig toNodeConfig() {
        NodeConfig nodeConfig = new NodeConfig()
        nodeConfig.executorService = executorService
        nodeConfig.geneusUrl = url
        nodeConfig.geneusUser = user
        nodeConfig.geneusPassword = password
        nodeConfig.clarityDataSource = clarityDataSource
        nodeConfig.sftpUrl = sftpUrl
        nodeConfig.sftpUser = sftpUser
        nodeConfig.sftpPassword = sftpPassword
        nodeConfig
    }

    @Override
    void destroy() throws Exception {
        executorService.shutdownNow()
    }

}
