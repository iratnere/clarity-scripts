package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.notification.OnboardingSequencingFileEmailNotification

/**
 * Created by lvishwas on 10/20/16.
 */
class FinishStep  extends ActionHandler {
    void execute(){
        OnboardHudsonAlphaSequencingFiles hudsonAlphaSequencingFiles = process as OnboardHudsonAlphaSequencingFiles
        sendEmailNotification(getAllAnalytes(hudsonAlphaSequencingFiles.getOnboardingWorkbook(OnboardHudsonAlphaSequencingFiles.SCRIPT_SEQUENCING_WORSHEET)))
    }

    List<Analyte> getAllAnalytes(ExcelWorkbook workbook){
        List<HudsonAlphaBean> beans = workbook.sections.values()[0].data
        boolean isPRU = beans.first().jgiPruLimsId? true: false
        List<String> analyteIds = beans.collect{isPRU? it.jgiPruLimsId : it.jgiLibraryLimsId}?.unique()
        return process.nodeManager.getArtifactNodes(analyteIds).collect{ AnalyteFactory.analyteInstance(it)}
    }

    List<EmailEvent> sendEmailNotification(List<Analyte> analytes){
        new ProcessUtility(this.process).sendEmailNotification(analytes, new OnboardingSequencingFileEmailNotification(), process.processLink)
    }
}
