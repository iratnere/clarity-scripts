package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AliquotCreationSheet
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AliquotCreationSheetParams
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AcPrepareAliquotCreationSheet extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(AcPrepareAliquotCreationSheet.class)
    AliquotCreationSheet acsCached

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        doTubesPlacement()
        SampleAliquotCreationProcess clarityProcess = process as SampleAliquotCreationProcess
        validateOutputs()
        uploadAliquotWorksheet()
        if (clarityProcess.outputPlateContainer)
            clarityProcess.outputPlateContainer.setUdf(ClarityUdf.CONTAINER_LABEL.udf, clarityProcess.sampleTubePlateLabel)
    }

    def validateOutputs() {
        SampleAliquotCreationProcess clarityProcess = process as SampleAliquotCreationProcess
        //validateLibraryCreationQueues
        clarityProcess.outputAnalytes*.libraryCreationQueue

        def outputPlateContainersSize = clarityProcess.outputPlateContainers.size()
        if (outputPlateContainersSize > 1) {
            clarityProcess.postErrorMessage("""
                Output plate does not pass custom validation.
                Number of output containers with value '$outputPlateContainersSize' exceeds maximum value 1.
                Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the output container.
            """)
        }
        if (outputPlateContainersSize) {
            validateOutputPlate()
        }
    }

    def validateOutputPlate(List<ClaritySampleAliquot> outputAnalytes = process.outputAnalytes as List<ClaritySampleAliquot>) {
        def libraryCreationQueue = [] as Set
        def dop = [] as Set
        def exomeCaptureProbeSet = [] as Set
        def itagPrimerSet = [] as Set
        def runMode = [] as Set
        def platformSet = [] as Set
        outputAnalytes.each { ClaritySampleAliquot claritySampleAliquot ->
            ScheduledSample scheduledSample = (ScheduledSample) claritySampleAliquot.claritySample
            LibraryCreationQueueCv lcQueue = claritySampleAliquot.libraryCreationQueue
            def dopUdf = scheduledSample.udfDegreeOfPooling
            def runModeUdf = scheduledSample.udfRunMode
            def platform = scheduledSample.platform
            def exomeCaptureProbeSetUdf = scheduledSample.udfExomeCaptureProbeSet
            def itagPrimerSetUdf = scheduledSample.udfItagPrimerSet

            libraryCreationQueue.add(lcQueue)
            dop.add(dopUdf)
            runMode.add(runModeUdf)
            platformSet.add(platform)

            if (exomeCaptureProbeSetUdf)
                exomeCaptureProbeSet.add(exomeCaptureProbeSetUdf)
            if (itagPrimerSetUdf)
                itagPrimerSet.add(itagPrimerSetUdf)
            logger.info "$scheduledSample.id: ${lcQueue?.libraryCreationQueue} / $dopUdf / $runModeUdf"
        }
        def errorMsg = "Output plate does not pass custom validation.$ClarityProcess.WINDOWS_NEWLINE Sample aliquots should have the same" << ' ' //StringBuilder.newInstance()
        def defaultSize = errorMsg.length()
        boolean isExomeOrItag = exomeCaptureProbeSet || itagPrimerSet
        if (libraryCreationQueue?.size() != 1)
            errorMsg << 'Library Creation Queue /'
        if (platformSet?.size() != 1)
            errorMsg << 'Platform /'
        if (exomeCaptureProbeSet && exomeCaptureProbeSet?.size() != 1)
            errorMsg << 'Exome Capture Probe Set /'
        if (itagPrimerSet && itagPrimerSet?.size() != 1)
            errorMsg << 'iTag Primer Set /'
        if (isExomeOrItag && dop?.size() != 1)
            errorMsg << 'Degree of Pooling /'
        if (isExomeOrItag && runMode?.size() != 1)
            errorMsg << 'Run Mode /'
        if (errorMsg.length() > defaultSize) {
            process.postErrorMessage(errorMsg as String)
        }
    }

    void uploadAliquotWorksheet(String fileName = ((SampleAliquotCreationProcess)process).SCRIPT_GENERATED_ALIQUOT_SHEET) {
        ArtifactNode fileNode = process.getFileNode(fileName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        ExcelWorkbook excelWorkbook = getAliquotCreationSheet().aliquotCreationSheet
        excelWorkbook.store(process.nodeManager.nodeConfig, fileNode.id)
    }

    AliquotCreationSheet getAliquotCreationSheet(List<Analyte> analytes = process.outputAnalytes) {
        AliquotCreationSheetParams params = new AliquotCreationSheetParams(process, analytes)
        if(!acsCached) {
            acsCached = new AliquotCreationSheet(params, process)
        }
        else
            acsCached.details = params
        return acsCached
    }
}
