package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ControlSampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This class is meant to perform pre-process validation for the Approve for shipping workflow
 * input must be samples
 * inputs must have status “Awaiting Shipping Approval”
 * PEU calculation must be successful and <=3 for all sow items of the selected samples/inputs
 * container type must be supported by library creation queue designated by sow item (uss.dt_library_creation_queue_cv has mass and volume values for the types of containers supported), except for Production RnD samples
 * Created by lvishwas on 1/17/15.
 */
class SampleReceiptPPV extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SampleReceiptPPV.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        String errors = checkInputSize()
        if (errors)
            process.postErrorMessage(errors)
        errors = checkIsControlSample()
        if (errors)
            process.postErrorMessage(errors)
    }

    String checkInputSize(List<Analyte> analytes = process.inputAnalytes){
        if(analytes.size() != 1)
            return "Only one control sample is expected as input to this process\n"
        return ""
    }

    String checkIsControlSample(Analyte analyte = process.inputAnalytes[0]){
        if (!(analyte instanceof ControlSampleAnalyte))
            return "This process requires a control sample as input. Please abort this process, empty ice-bucket, add control sample 'cs Sample Receipt' to ice bucket and restart the process.\n"
        return ""
    }
}
