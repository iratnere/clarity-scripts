package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96Iterator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.PlacementsNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import org.slf4j.LoggerFactory

class PlaceOutputs extends ActionHandler {
    static final logger = LoggerFactory.getLogger(PlaceOutputs.class)
    String getOutputContainer(PlacementsNode placementsNode = processNode.placementsNode){
        List<String> containerIds = placementsNode?.selectedContainerIds
        if (!containerIds) {
            process.postErrorMessage("No placement containers")
        } else if (containerIds.size() > 1) {
            process.postErrorMessage("More than one placement containers")
        }
        return containerIds[0]
    }

    String getOutputArtifactId(SampleAnalyte sampleAnalyte){
        List<String> outputArtifactIds = processNode.getOutputAnalyteIds(sampleAnalyte.id)
        if (!outputArtifactIds) {
            process.postErrorMessage("No outputs for input analyte ${sampleAnalyte.id}")
        }
        if (outputArtifactIds.size() > 1) {
            process.postErrorMessage("More than one output for input analyte ${sampleAnalyte.id}")
        }
        return outputArtifactIds[0]
    }

    OutputPlacement buildOutputPlacement(String containerId, WellAddress96 wellAddress, String outputArtifactId){
        ContainerLocation containerLocation = new ContainerLocation(containerId, wellAddress.clarityLocation)
        OutputPlacement outputPlacement = new OutputPlacement()
        outputPlacement.containerLocation = containerLocation
        outputPlacement.artifactId = outputArtifactId
        outputPlacement
    }

    List<OutputPlacement> getOutputPlacements(List<String> analyteIds, String containerId = outputContainer){
        List<OutputPlacement> outputPlacementList = []
        Iterator<WellAddress96> wellIterator = new WellAddress96Iterator(skipCorners:true, columnsFirst:true)
        analyteIds?.eachWithIndex { String analyteId, Integer index ->
            WellAddress96 wellAddress = wellIterator.next()
            if (!wellAddress) {
                process.postErrorMessage("Too many analytes for a plate")
            }
            outputPlacementList << buildOutputPlacement(containerId, wellAddress, analyteId)
        }
        return outputPlacementList
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        MetabolomicsQCProcess qcProcess = (MetabolomicsQCProcess) process
        OutputPlacements outputPlacements = new OutputPlacements()
        outputPlacements.addAll(getOutputPlacements(qcProcess.sortInputAnalytes(process.inputAnalytes).collect{getOutputArtifactId(it)}))
        processNode.placementsNode?.setOutputPlacements(outputPlacements)
        logger.debug "placements node after modification:\n${processNode.placementsNode?.nodeString}"
        processNode.placementsNode?.httpPost()
    }
}
