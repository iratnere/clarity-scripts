package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 *
 * Clarity DAP Seq LC Requirements
 * https://docs.google.com/document/d/1zc7V6mqTOIqsglkILLEKsc68uV604mmOso1IYFE3kuw/edit?ts=5da8f61f
 */
class LcPreProcessValidation extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(LcPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.validateInputAnalyteClass()
        clarityProcess.lcAdapter.validateInputsContainerType()
        clarityProcess.lcAdapter.validateBatch() //SingleCell and DAP-Seq
    }
}