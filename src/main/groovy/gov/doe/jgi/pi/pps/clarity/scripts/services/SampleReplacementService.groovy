package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.util.exception.WebException
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class SampleReplacementService {
    static final Logger logger = LoggerFactory.getLogger(SampleReplacementService.class)
    @Value("\${sampleReplaced.url}")
    private String sampleReplacedUrl

    int replaceSamples(def operatorId, Set<Long> sampleIds) {
        if (!sampleIds || operatorId == null)
            return
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'submitted-by'(operatorId.longValue())
            'sample-ids'(sampleIds)
        }
        return callSampleReplacementService(jsonRequest)
    }

    int callSampleReplacementService(def jsonRequest){
        int responseCode
        HTTPBuilder http = new HTTPBuilder(sampleReplacedUrl)
        http.request( Method.POST, ContentType.JSON ) { req ->
            body = jsonRequest.toPrettyString()
            response.success = { resp, reader ->
                logger.info "GlsSampleReplaced response - ${resp.statusLine}"
                responseCode = resp.statusLine.statusCode
            }
            response.failure = {resp, reader ->
                logger.error "GlsSampleReplaced response - ${resp.statusLine}"
                responseCode = resp.statusLine.statusCode
                throw new WebException(reader, resp.statusLine.statusCode)

            }
        }
        return responseCode
    }
}
