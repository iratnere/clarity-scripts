package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

class ProcessMetabolomicsSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ProcessMetabolomicsSheet.class)
    void execute() {
        logger.info "Starting ${this.class.simpleName} action...."
        String errors = ''
        errors += validateProcessUdfs()
        if(errors)
            process.postErrorMessage(errors)
        (process as MetabolomicsQCProcess).importDataFromMetabolomicsWorksheet()
    }

    String validateProcessUdfs() {
        String errors = ''
        MetabolomicsQCProcess qcProcess = (MetabolomicsQCProcess) process
        if(qcProcess.udfReplicatesFailurePercentAllowed == null)
            errors += "Process udf 'Replicates Failure Percent Allowed' is required.\n"
        if(qcProcess.udfMinimumIsotopeEnrichment == null)
            errors += "Process udf 'Minimum Isotope Enrichment (at%)' is required.\n"
        return errors
    }
}
