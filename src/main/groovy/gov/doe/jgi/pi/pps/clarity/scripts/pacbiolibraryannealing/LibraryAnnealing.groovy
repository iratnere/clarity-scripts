package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibraryannealing

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by lvishwas on 4/6/2015.
 */
class LibraryAnnealing extends ClarityProcess {

    static ProcessType processType = ProcessType.PACBIO_LIBRARY_ANNEALING

    LibraryAnnealing(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation': LaPreProcessValidation,
            'LibraryAnnealing':LaPacBioLibraryAnnealing,
            'RouteToNextWorkflow': LaRouteToWorkflow
        ]
    }
}
