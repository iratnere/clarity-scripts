package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 9/13/16.
 */
class SampleReceiptEmailDetails extends EmailDetails {
    SampleReceiptEmailDetails(Analyte analyte) {
        super(analyte)
    }

    @Override
    def getHashCode(){
        if(containerNode.containerTypeEnum == ContainerTypes.TUBE) {
            return new HashCodeBuilder(17, 37).
                    append(sampleContactId).
                    append(sequencingProjectManagerId).
                    toHashCode()
        }
        else{
            return new HashCodeBuilder(17, 37).
                    append(containerNode.id).
                    toHashCode()
        }
    }

    def getInfo() {
        return "$pmoSampleId,  $sampleLimsId,  $sampleName,  $barcode,  $containerName,  $plateLocation,  $sequencingProjectName, $sequencingProjectId"
    }
}
