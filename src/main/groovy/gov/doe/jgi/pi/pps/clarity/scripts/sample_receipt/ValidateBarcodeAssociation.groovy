package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel.SampleReceiptTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ValidateBarcodeAssociation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(ValidateBarcodeAssociation.class)
    List<Long> pmoSampleIds = []

    void execute(){
        String errors = ''
        errors = validateBeans()
        if(errors)
            process.postErrorMessage(errors)
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        logger.info "Reading sample status for samples $pmoSampleIds"
        errors = checkStatus(statusService.getSampleStatusBatch(pmoSampleIds))
        if(errors)
            process.postErrorMessage(errors)
    }

    String validateBeans(List<SampleReceiptTableBean> beans = process.barcodeTableBeans){
        logger.info "Validating beans"
        for(SampleReceiptTableBean bean : beans) {
            if(bean.jgiBarcode && process.updateBeanContainer(bean)) {
                String error = bean.validateBean()
                if (error)
                    return error
                bean.containerNode.contentsArtifactNodes.each { ArtifactNode artifactNode ->
                    SampleAnalyte sampleAnalyte = AnalyteFactory.analyteInstance(artifactNode)
                    error = checkIsSample(sampleAnalyte)
                    if (error)
                        return error
                    pmoSampleIds << sampleAnalyte.claritySample.pmoSample.pmoSampleId
                }
            }
        }
        return ''
    }

    String checkIsSample(SampleAnalyte sampleAnalyte){
        if((sampleAnalyte instanceof SampleAnalyte) && (sampleAnalyte?.claritySample instanceof PmoSample))
            return ''
        return  "Artifact ${sampleAnalyte.id} on Container Node ${sampleAnalyte.containerNode.id} is not a Sample\n"
    }

    String checkStatus(Map statusMap){
        String errors = ''
        //inputs must have status “Awaiting Shipping Approval”
        statusMap.each{
            def sampleStatus = it.value
            if(sampleStatus != SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value) {
                errors += "Invalid input ${it.key} with current status ${sampleStatus}. Expected status:  ${SampleStatusCv.AWAITING_SAMPLE_RECEIPT.value}\n"
            }
        }
        return errors
    }
}
