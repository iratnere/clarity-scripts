package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPoolTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity.scripts.services.PostProcessService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessParams
import gov.doe.jgi.pi.pps.clarity_node_manager.node.StepConfigurationNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 1/3/16.
 */
@Deprecated
class OutputPool {
    static final Logger logger = LoggerFactory.getLogger(OutputPool.class)
    LibraryCreationProcess process
    List<ClarityLibraryPool> clarityLibraryPools

    OutputPool(LibraryCreationProcess process) {
        this.process = process
    }

    ExcelWorkbook populateLibraryCreationSheet(String templateName,
                                               Section tableSectionPools,
                                               Section tableSectionLibraries){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(templateName, process.testMode)
        Section kvResultSection = process.getKeyValueResultSection()

        excelWorkbook.addSection(kvResultSection)
        excelWorkbook.addSection(tableSectionLibraries)
        excelWorkbook.addSection(tableSectionPools)

        return excelWorkbook
    }

    void  updateLibraryStockUdfs() {
        transferBeansDataToClarityLibraryStocks()
        process.updateNonWorksheetUdfs()
    }

    def transferBeansDataToClarityLibraryStocks() {
        String indexSet = process.resultsBean.indexContainerBarcode
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            Boolean passed = getLibraryStockStatus(clarityLibraryStock)

            LcPlateTableBean lcPlateTableBean = (LcPlateTableBean) process.getLcTableBean(clarityLibraryStock.id, passed)
            clarityLibraryStock.udfConcentrationNgUl = lcPlateTableBean.libraryConcentration
            clarityLibraryStock.udfActualTemplateSizeBp = lcPlateTableBean.libraryTemplateSize
            clarityLibraryStock.udfVolumeUl = lcPlateTableBean.libraryVolume
            clarityLibraryStock.udfIndexContainerBarcode = indexSet
            clarityLibraryStock.udfIndexName = lcPlateTableBean.libraryIndexName
            clarityLibraryStock.udfLibraryMolarityQcPm = lcPlateTableBean.libraryMolarity
            clarityLibraryStock.udfNumberPcrCycles = process.resultsBean.numberPcrCycles
        }
    }

    Boolean getLibraryStockStatus(ClarityLibraryStock clarityLibraryStock) {
        LcPlateTableBean libraryStockBean = (LcPlateTableBean) process.getLcTableBean(clarityLibraryStock.id)
        if (libraryStockBean.poolNumber) {
            LcPoolTableBean lcPoolTableBean = process.getLcPoolTableBean(libraryStockBean.poolNumber)
            return lcPoolTableBean?.passed
        }
        return null
    }

    def processLibraryPools() {
        clarityLibraryPools = createClarityLibraryPools()
        //lpContainers = clarityLibraryPools.collect {it.artifactNode.containerNode}.unique()
        if (!process.testMode) {
            LibraryNameReservationService libraryNameReservationService = BeanUtil.getBean(LibraryNameReservationService.class)
            libraryNameReservationService.assignLibraryNames(clarityLibraryPools.sort{ it.id })
        }
        transferBeansDataToClarityLibraryPools()
        clarityLibraryPools.each { it.updateLibraryCreationUdfs(process.researcher?.fullName) }
    }

    def transferBeansDataToClarityLibraryPools(List<ClarityLibraryPool> libraryPools = clarityLibraryPools,
                                               List<ClarityLibraryStock> libraryStocks = process.outputAnalytes
    ) {
        libraryPools.each { ClarityLibraryPool clarityLibraryPool ->
            def libraryStockBean = process.findLibraryStockBean(clarityLibraryPool)
            LcPoolTableBean lcPoolTableBean = process.getLcPoolTableBean(libraryStockBean.poolNumber)

            clarityLibraryPool.udfConcentrationNgUl = lcPoolTableBean.poolConcentration
            clarityLibraryPool.udfActualTemplateSizeBp = lcPoolTableBean.poolTemplateSize
            clarityLibraryPool.udfVolumeUl = lcPoolTableBean.poolVolume
            clarityLibraryPool.udfLibraryMolarityQcPm = lcPoolTableBean.poolMolarity

            clarityLibraryPool.udfLibraryQcResult = lcPoolTableBean.poolResult?.value
            clarityLibraryPool.systemQcFlag = lcPoolTableBean.poolResult?.value == Analyte.PASS
            clarityLibraryPool.udfLibraryQcFailureMode = lcPoolTableBean.failureMode?.value
            clarityLibraryPool.containerUdfLibraryQcResult = lcPoolTableBean.poolResult?.value
            clarityLibraryPool.containerUdfLibraryQcFailureMode = lcPoolTableBean.failureMode?.value

            clarityLibraryPool.poolMembers.each{
                it.udfLibraryQcFailureMode = lcPoolTableBean.failureMode?.value
                it.udfLibraryQcResult = lcPoolTableBean.poolResult?.value
                it.systemQcFlag = lcPoolTableBean.poolResult?.value == Analyte.PASS
            }
        }
        libraryStocks.findAll{ !it.udfLibraryQcResult }.each {
            it.udfLibraryQcFailureMode = ClarityLibraryPool.FAILURE_MODE_SAMPLE_PROBLEM
            it.udfLibraryQcResult = Analyte.FAIL
            it.systemQcFlag = false
        }

    }

    ProcessNode createPoolCreationProcessNode() {
        ProcessNode poolCreationProcessNode = createAndConfigureOutputPools()
        if (!poolCreationProcessNode) {
            process.postErrorMessage("LcProcessLibraryCreationSheet.createAndConfigureOutputPools.failed: poolCreationProcessNode($poolCreationProcessNode)")
        }
        return poolCreationProcessNode
    }

    List<ClarityLibraryPool> createClarityLibraryPools() {
        logger.info "Create library pools"
        ProcessNode poolCreationProcessNode = createPoolCreationProcessNode()
        return poolCreationProcessNode.outputAnalytes.collect{(ClarityLibraryPool) AnalyteFactory.analyteInstance(it) }
    }

    def createAndConfigureOutputPools() {
        PostProcessService taskGenerationService = BeanUtil.getBean(PostProcessService.class)
        Stage stage = Stage.AUTOMATIC_POOL_CREATION
        StepConfigurationNode stepConfigurationNode = StageUtility.getStepConfigurationNode(process.nodeManager, stage)
        ProcessParams processParams = new ProcessParams(stepConfigurationNode, poolNumberToPoolMemberArtifactNodes)
        processParams.processUdfMap = getProcessUdfsToDefaultValueMap(stepConfigurationNode)
        taskGenerationService.postPoolingProcess(process.nodeManager, stage, processParams)
    }

    static Map<UserDefinedField, Object> getProcessUdfsToDefaultValueMap(StepConfigurationNode stepConfigurationNode) {
        Map<UserDefinedField, Object> processUdfsMap = [:]
        StageUtility.getProcessUdfs(stepConfigurationNode)?.each { UserDefinedField udf ->
            processUdfsMap[udf] = ClarityUdf.DEFAULT_UDF_VALUE
        }
        processUdfsMap
    }

    Map<Integer,Collection<ArtifactNode>> getPoolNumberToPoolMemberArtifactNodes(){
        Map<Integer,Collection<ArtifactNode>> poolNumberPoolMembersMap = [:]
        process.libraryPoolBeans.findAll{it.poolNumber > 0}.each{ LcPoolTableBean poolTableBean ->
            Integer poolNumber = poolTableBean.poolNumber as Integer
            poolNumberPoolMembersMap[poolNumber] = process.libraryStockBeans.findAll{
                (it.poolNumber == poolNumber)
            }.collect {process.nodeManager.getArtifactNode(it.libraryLimsId)}
        }
        return poolNumberPoolMembersMap
    }

    void updateLibraryCreationAttemptUdfs() {
        List<ClarityLibraryPool> passedPools = clarityLibraryPools.findAll { it.udfLibraryQcResult == Analyte.PASS }
        process.outputAnalytes.each{ ClarityLibraryStock output ->
            ScheduledSample scheduledSample = (ScheduledSample)output.claritySample
            scheduledSample.incrementUdfLcAttempt()
            if (!passedPools) {
                scheduledSample.incrementUdfLcFailedAttempt()
            }
        }
    }

}
