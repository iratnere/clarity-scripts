package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 9/12/16.
 */
class SampleFailedReceiptEmailDetails extends EmailDetails{
    def comments

    SampleFailedReceiptEmailDetails(Analyte analyte){
        super(analyte)
        this.comments = analyte.claritySample.udfSampleReceiptNotes
    }

    @Override
    def getHashCode(){
        return new HashCodeBuilder(17, 37).
                append(sequencingProjectManagerId).
                toHashCode()
    }

    @Override
    def getInfo() {
        return "$pmoSampleId,  $containerName,  $comments"
    }
}
