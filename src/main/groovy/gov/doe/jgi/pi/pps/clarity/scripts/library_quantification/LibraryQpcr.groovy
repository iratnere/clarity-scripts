package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LibraryQpcr extends ClarityProcess {
    static final Logger logger = LoggerFactory.getLogger(LibraryQpcr.class)

    static ProcessType processType = ProcessType.LQ_LIBRARY_QPCR

    final static String QPCR_UPLOAD_FILE = 'Upload qPCR Worksheet'
    final static String QPCR_DOWNLOAD_FILE = 'Download qPCR Worksheet'

    boolean testMode = false
    private ExcelWorkbook qpcrWorkbook

	LibraryQpcr(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation': LqPreProcessValidation,
            'PrepareQpcrSheet': LqPrepareQpcrSheet,
            'ProcessQpcrSheet':LqProcessQpcrSheet,
            'RouteToNextWorkflow': LqRouteToNextWorkflow
        ]
    }

    ExcelWorkbook getQpcrWorkbook() {
        if (!qpcrWorkbook) {
            ArtifactNode resultFile = this.getFileNode(QPCR_UPLOAD_FILE)
            qpcrWorkbook = new ExcelWorkbook(resultFile.id, testMode)
            logger.info "workbook created from ${resultFile.id}"
        }
        return qpcrWorkbook
    }
}
