package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/14/16.
 */
class PhysicalRunUnitMigrator extends Migrator {
    static final logger = LoggerFactory.getLogger(PhysicalRunUnitMigrator.class)

    static Map<ClarityUdf, Object> defaultUdfs

    PhysicalRunUnitMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager, null)
    }

    static{
        defaultUdfs = [:].withDefault {[:]}
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_LIBRARY_CONVERSION_FACTOR] = 6.5
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_PHIX_SPIKE_IN] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.PARENT_CONTAINER_UDFS][ClarityUdf.CONTAINER_LIBRARY_QPCR_RESULT] = Analyte.PASS
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerTypes) {
        return Stage.SEQUENCING_ILLUMINA_HISEQ_RAPID
    }


    String getDestinationStage(Analyte analyte){
        if(analyte.claritySample.udfExternal == 'Y')
            return null
        ScheduledSample scheduledSample = (ScheduledSample) analyte.claritySample
        return scheduledSample.runModeCv.getSequencingWorkflowUri()
    }

    ContainerTypes getContainerType(Analyte parent){
        return ContainerTypes.FLOW_CELL
    }

    def createAnalytesInClarity(List<OnboardingBean> onboardingBeans, boolean startingPoint=false){
        if(startingPoint)
            prepareNodeManager()
        if(!parentMigrator)
            parentMigrator = getParentMigrator(onboardingBeans)
        List<Analyte> parents = parentMigrator?.createAnalytesInClarity(onboardingBeans)
        def unmergedBeans = getUnmergedBeans(onboardingBeans)
        def analytes = []
        int i=0
        if(unmergedBeans){
            parents.addAll(unmergedBeans.collect{parentMigrator.getAnalyte(it)})
            parents = parents?.findAll{it != null}
            parents.unique()
            List<ContainerNode> newContainers = getClarityContainers(clarityNodeManager, unmergedBeans.collect { it.flowcellBarcode }.unique())
            newContainers*.readOnly = false
            Map<ContainerNode, List<Analyte>> containerParents = [:].withDefault { [] }
            parents.each{it.artifactNode.httpRefresh()}
            setDefaultUdfs(parents, this.defaultUdfs[Migrator.PARENT_UDFS], this.defaultUdfs[Migrator.PARENT_CONTAINER_UDFS])
            clarityNodeManager.dirtyNodes.each {it.readOnly = false}
            clarityNodeManager.httpPutDirtyNodes()
            def workflowStage = getWorkflowStage(newContainers.first().containerTypeEnum)
            Map<String, ContainerNode> barcodeFlowcellMap = [:]
            parentMigrator.pruBeans.each{String flowcellLane, List<OnboardingBean> parentBeans->
                def unmergedPruBeans = parentBeans.findAll{getAnalyte(it)==null}
                if(unmergedPruBeans) {
                    List<Analyte> parentAnalytes = unmergedPruBeans.collect {
                        parentMigrator.getAnalyte(it)
                    }?.unique()
                    ContainerNode containerNode = barcodeFlowcellMap[parentBeans[0].flowcellBarcode]
                    if (!containerNode) {
                        containerNode = newContainers[i++]
                        barcodeFlowcellMap[parentBeans[0].flowcellBarcode] = containerNode
                        containerNode.name = parentBeans[0].flowcellBarcode
                    }
                    ContainerLocation outputLocation = new ContainerLocation(containerNode.id, unmergedPruBeans[0].laneNumber + '', '1')
                    containerParents[outputLocation].addAll(parentAnalytes.collect { it.artifactNode })
                }
            }
//            newContainers*.httpPut()
            List<ClarityProcess> processes = createChildrenInClarity(workflowStage, containerParents)
            analytes.addAll(processes?.collect{it.outputAnalytes}?.flatten())
            analytes*.setSystemQcFlag(true)
            newContainers = clarityNodeManager.batchRefreshGeneusNodes(newContainers)
            updateBeans(unmergedBeans, analytes)
        }
        logger.info "Done creating PRUs $analytes....."
        updateUdfs(onboardingBeans)
        def anaList = onboardingBeans.collect{getAnalyte(it)}
        routeParentsAndChildren(anaList.collect{it.parents}?.flatten(), anaList)
        clarityNodeManager.dirtyNodes.each {it.readOnly = false}
        clarityNodeManager.httpPutDirtyNodes()
        logger.info "Done updating PRUs $analytes"
        if(startingPoint)
            resetNodeManager()
        return analytes
    }

    void updateBeans(List<OnboardingBean> unmergedBeans, List<Analyte> analytes){
        unmergedBeans.each{ OnboardingBean bean ->
            Analyte analyte = analytes.find{it.containerName == bean.flowcellBarcode && it.containerLocation.row as int == bean.getLaneNumber()}
            bean.addAnalyte(analyte)
        }
    }

    List<ClarityProcess> createChildrenInClarity(Stage workflowStage, Map containerParents){
        def containerLocationAnalyteIdsMap = [:]
        List<ArtifactNode> parents = []
        containerParents.each{ ContainerLocation outputLocation, List<ArtifactNode> parentNodes ->
            routeAnalytes(workflowStage.uri, parentNodes?.collect { it.id }.unique())
            parentNodes.each{ ArtifactNode parent ->
                containerLocationAnalyteIdsMap[outputLocation] = parent
            }
            parents.addAll(parentNodes)
        }
        performPreProcessActions(parents)
        ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, workflowStage), containerLocationAnalyteIdsMap)
        processDetails.processUdfMap = getProcessUdfsMap(workflowStage)
        logger.info "posting Process ${workflowStage.value}"
        ProcessNode processNode = clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityOutputProcess(processDetails))
        logger.info "Process execution complete $processNode"
        refreshProcessNode(processNode)
        [ProcessFactory.processInstance(processNode)]
    }

    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans){
        checkExistingAnalytes(beans)
        return beans.findAll{!it.physicalRunUnit}
    }

    @Override
    void checkExistingAnalytes(List<OnboardingBean> beans){
        Map<String, ContainerNode> beanFlowcells = lookUpFlowcellsByBarcode(clarityNodeManager, beans.collect{it.flowcellBarcode})
        if(!beanFlowcells)
            return
        beans.each{ HudsonAlphaBean bean ->
            Analyte existingAnalyte = null
            ContainerNode flowCell = beanFlowcells[bean.flowcellBarcode]
            if(flowCell)
                existingAnalyte = getAnalyteByLocation(clarityNodeManager, new ContainerLocation(flowCell.id, "$bean.laneNumber", '1'))
            if(existingAnalyte)
                bean.addAnalyte(existingAnalyte)
        }
    }

    Analyte getAnalyte(OnboardingBean bean){
        return bean.physicalRunUnit
    }

    @Override
    Class getAnalyteType() {
        return ClarityPhysicalRunUnit.class
    }

    Migrator getParentMigrator(List<OnboardingBean> beans) {
        Map<String, List<OnboardingBean>> pruBeans = [:].withDefault { [] }
        beans.each { OnboardingBean bean ->
            pruBeans["${bean.flowcellBarcode}_${bean.laneNumber}"] << bean
        }
        Migrator migrator
        if(pruBeans.values().find{it.size() > 1})
            migrator = new LibraryPoolsMigrator(clarityNodeManager)
        else
            migrator = new LibraryStocksMigrator(clarityNodeManager)
        migrator.pruBeans = pruBeans
        migrator.validatePruBeans()
        return migrator
    }

    void performPreProcessActions(List<ArtifactNode> parents){
        Stage stage = Stage.AUTOMATIC_SEQUENCE_QC_REQUEUE_SEQUENCING
        routeAnalytes(stage.uri, parents.collect{it.id})
        ProcessParams processParams = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, stage), parents)
        String processId = clarityNodeManager.executeClarityInputProcess(processParams)
        logger.info "Posted requeue process $processId"
    }
}
