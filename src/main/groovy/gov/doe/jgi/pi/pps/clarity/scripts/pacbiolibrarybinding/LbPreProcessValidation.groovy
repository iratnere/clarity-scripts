package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/6/2015.
 */
class LbPreProcessValidation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LbPreProcessValidation.class)

    void execute(){

        logger.info "Starting ${this.class.name} action...."

        process.inputAnalytes.each { Analyte analyte ->
            if (analyte instanceof PacBioAnnealingComplex) {
                PacBioAnnealingComplex annealingComplex = analyte as PacBioAnnealingComplex
                if (annealingComplex.udfVolumeUl == null) {
                    errorMessage "Volume required for ${annealingComplex}."
                }

                if (annealingComplex.udfPacBioMolarityNm == null) {
                    errorMessage "PacBio molarity required for ${annealingComplex}."
                }
            } else {
                errorMessage "${analyte} is not an annealing complex."
            }
        }

    }
}
