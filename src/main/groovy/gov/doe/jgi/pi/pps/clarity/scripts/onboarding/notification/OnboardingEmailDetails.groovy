package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 10/23/16.
 */
class OnboardingEmailDetails extends EmailDetails{

    OnboardingEmailDetails(Analyte analyte) {
        super(analyte)
    }

    @Override
    def getHashCode() {
        return new HashCodeBuilder(17, 37).toHashCode()
    }

    @Override
    def getInfo() {
        return ''
    }
}
