package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.NodeManagerUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import org.slf4j.LoggerFactory

class MetabolomicsQCPPV extends ActionHandler {
    static final int MAX_INPUT_SIZE = 96-4
    static final logger = LoggerFactory.getLogger(MetabolomicsQCPPV.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        MetabolomicsQCProcess metabolomicsQC = (MetabolomicsQCProcess) process
        String errors = ''
        errors += checkInputSize(metabolomicsQC.inputAnalytes)
        metabolomicsQC.inputAnalytes.groupBy {it.claritySample.pmoSample.udfGroupName}.each{ String groupName, List<Analyte> groupSamples ->
            groupSamples.each { Analyte analyte ->
                errors += checkIsSIPOriginalSample(analyte)
            }
            errors += checkIsSIPGroupComplete(groupName, groupSamples.collect{it.claritySample.id})
        }
        if(errors)
            process.postErrorMessage(errors)
    }

    String checkIsSIPGroupComplete(String groupName, List<String> inputSampleLimsIds) {
        //All samples of a group should be QC’ed together
        NodeManagerUtility nodeManagerUtility = new NodeManagerUtility(nodeManager)
        List<String> pmoSampleLimsIds = nodeManagerUtility.lookUpSamplesBySipGroupName(groupName)
        List<ClaritySample> groupSamples = nodeManager.getSampleNodes(pmoSampleLimsIds).collect{ SampleFactory.sampleInstance(it)}
        List<PmoSample> pmoSamples = groupSamples.findAll{ it instanceof PmoSample}
        List<PmoSample> diffSamples = pmoSamples.findAll{!(it.id in inputSampleLimsIds) && (it.scheduledSamples[0].udfSowItemQcResult == Analyte.PASS)}
        if(diffSamples)
            return "All samples of the group $groupName should be QC'ed together. Please add ${diffSamples.collect{it.id}} to icebucket and restart the process.\n"
        return ""
    }

    String checkInputSize(List<Analyte> analytes) {
        //Input size must not exceed 92
        if(analytes.size() > MAX_INPUT_SIZE)
            return "Only $MAX_INPUT_SIZE inputs supported per process.\n"
        return ""
    }

    String checkIsSIPOriginalSample(Analyte analyte) {
        //Input analytes are all SIP Original root samples
        PmoSample pmoSample = analyte.claritySample
        List<ScheduledSample> scheduledSamples = pmoSample.scheduledSamples
        assert scheduledSamples && scheduledSamples.size() == 1
        if(!scheduledSamples.collect{it.udfSowItemType}.contains(SowItemTypeCv.SIP_SOURCE_SAMPLE.value))
            return "Only SIP Original samples supported as inputs for this process.\n"
        return ""
    }
}
