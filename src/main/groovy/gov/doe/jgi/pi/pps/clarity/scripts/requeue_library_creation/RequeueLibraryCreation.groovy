package gov.doe.jgi.pi.pps.clarity.scripts.requeue_library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class RequeueLibraryCreation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryCreation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.validateInputs()
        process.setCompleteStage()
    }

}
