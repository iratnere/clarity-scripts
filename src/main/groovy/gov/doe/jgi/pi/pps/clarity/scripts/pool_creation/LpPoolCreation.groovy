package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.ProcessEditService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 7/2/2015.
 */
class LpPoolCreation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LpPoolCreation.class)

    ProcessEditService processEditService

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        PoolCreation poolCreation = (PoolCreation)process
        def poolBeans = poolCreation.getPoolPrepMemberBeans()
        if(!processEditService)
            processEditService = BeanUtil.getBean(ProcessEditService.class)
        processEditService.editProcess(this.process, poolBeans[poolCreation.REPEAT_POOL_NUMBER], poolBeans[poolCreation.CONTINUE_POOL_NUMBER], Stage.POOL_CREATION)
        process.processNode.httpRefresh()

        poolBeans.remove(poolCreation.CONTINUE_POOL_NUMBER)
        poolBeans.remove(poolCreation.REPEAT_POOL_NUMBER)
        if(!process.testMode)
            nodeManager?.getPoolsNode(processNode.id).createPools(poolBeans)
        nodeManager.httpPutDirtyNodes()
    }
}