package gov.doe.jgi.pi.pps.clarity.scripts.services

import groovy.transform.Canonical

@Canonical
class KapaWellConfig {
    KapaWellConfig(String tagNameLocation, String tagSequence) {
        this.tagNameLocation = tagNameLocation
        this.tagSequence = tagSequence
    }
    String tagNameLocation
    String  tagSequence
    String getLocation() {
        String[] nameLoc = tagNameLocation.split(':', 2)
        nameLoc[1]
    }
}
