package gov.doe.jgi.pi.pps.clarity.model.process

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.PlacementsNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.LogTiming
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.util.logging.Slf4j

/**
 * Created by dscott on 12/10/2014.
 */
@Slf4j
class ActionHandler {

    private List<String> errorMessages = []

    void errorMessage(message) {
        if (message) {
            errorMessages << (String) message as String
        }
    }

    void requireProcessUdf(ClarityUdf clarityUdf) {
        if (!process.processNode.getUdfAsString(clarityUdf.udf)) {
            errorMessage "Process UDF [${clarityUdf}] is required for process type [${process.processNode.processType}]."
        }
    }

    void requireProcessUdfForStage(ClarityUdf clarityUdf, Stage stage) {
        if (!process.processNode.getUdfAsString(clarityUdf.udf)) {
            errorMessage "Process UDF [${clarityUdf}] is required for stage [${stage.name()}]."
        }
    }

    void checkErrorMessages() {
        if (errorMessages) {
            throw new WebException(errorMessages.join("\n"),422)
        }
    }

    ClarityProcess process

    String action
    String stepUrl
    List<String> fileIds
    //boolean putDirtyNodes = false
    boolean lastAction = false

    ActionHandler() {}

    ProcessNode getProcessNode() {
        process?.processNode
    }

    NodeManager getNodeManager() {
        processNode?.nodeManager
    }

    void execute() {
        throw new RuntimeException("${this.class.simpleName} is not implemented")
    }

    final void doExecute() {
        Long startTime = System.currentTimeMillis()
        execute()
        checkErrorMessages()
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'ActionHandler.doExecute',[action:this.class.simpleName])
    }

    protected void postFatalError(String errorMessage, Integer status) throws WebException {
        process.postErrorMessage(errorMessage)
        throw new WebException(errorMessage, status)
    }

    /**
     * testing/starting process step programmatically
     * placement for tubes did not happen -> see https://genologics.zendesk.com/requests/13792
     * need to do placement and refresh output artifact nodes -> error otherwise -> "analyte.containerNode.notFound"
     */
    //TODO need tubes placement
    void doTubesPlacement() {
        if (process.processNode.outputAnalytes[0].containerNode)
            return
        log.info 'doTubesPlacement()'
        automaticPlacementTube(process.processNode, BeanUtil.nodeManager)
        process.processNode.outputAnalytes.each {
            while(!it.containerNode)
                it.httpRefresh()
            log.info it.getNodeString()
        }
    }

    static void automaticPlacementTube(ProcessNode processNode, NodeManager clarityNodeManager) {
        String analytePlacement = '1:1'
        List outputAnalytes = processNode.outputAnalytes
        PlacementsNode placementsNode = processNode.placementsNode

        String containerId = placementsNode.selectedContainerIds[0] //only one  available selected container-> bug ->see https://genologics.zendesk.com/requests/13792
        def numberOfExtraContsNeeded = outputAnalytes.size() - 1
        def containerIds = [containerId]
        if (numberOfExtraContsNeeded > 0) {
            def containers = clarityNodeManager.createContainersBulk(ContainerTypes.TUBE, numberOfExtraContsNeeded)
            containers.each{ containerIds << it.id }
        }

        OutputPlacements outputPlacements = new OutputPlacements()
        outputAnalytes.each { outputAnalyte ->
            def containerIdCurrent = containerIds.pop()
            ContainerLocation containerLocation = new ContainerLocation(containerIdCurrent, analytePlacement)
            outputPlacements << new OutputPlacement(artifactId: outputAnalyte.id, containerLocation: containerLocation)
        }
        placementsNode.setOutputPlacements(outputPlacements).httpPost()
        log.info "Automatic Placement Script has completed successfully. ${outputAnalytes?.size()} samples have been transferred into containers."
    }

    /**
     * Duncan says: this call is to expensive
     * testing/advancing process step programmatically
     * tests can update some process udfs and upload a worksheet multiple times on the RECORD_DETAILS screens
     * need to refresh process node
     */

    void doRefreshProcessNode() {
        log.info 'doRefreshProcessNode() (disabled)'
        //ClarityWebTransaction.logger.info process.processNode.getNodeString()
        //processNode.httpRefresh()
        //ClarityWebTransaction.logger.info "httpRefresh(): DONE"
        //ClarityWebTransaction.logger.info process.processNode.getNodeString()
    }

    /**
     * testing/advancing process step programmatically
     * tests can update some process udfs and output udfs
     * need to refresh process node/outputs
     */
    void doRefresh() {
        log.info 'doRefresh() (disabled)'
        //process.refresh()
    }

    void performLastStepActions(){}
}
