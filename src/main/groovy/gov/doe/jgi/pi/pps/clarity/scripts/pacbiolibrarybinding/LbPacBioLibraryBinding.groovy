package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/6/2015.
 */
class LbPacBioLibraryBinding extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LbPacBioLibraryBinding.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."

        List<ArtifactNode> inputAnalytes = processNode.inputAnalytes
        for (ArtifactNode input : inputAnalytes) {
            List<ArtifactNode> outputArtifacts = processNode.getOutputAnalytes(input.id)
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(outputArtifacts.first())
            if (outputAnalyte.artifactNode.systemQcFlag == null) {
                process.postErrorMessage("QC flag required for $outputAnalyte")
            }

            PacBioBindingComplex bindingComplex = outputAnalyte as PacBioBindingComplex
            BigDecimal volumeUsed = bindingComplex.udfVolumeUsedUl
            if (volumeUsed == null) {
                process.postErrorMessage("Library Volume Used (uL) required for $bindingComplex")
            }

            if (bindingComplex.udfVolumeUl == null) {
                process.postErrorMessage("Volume (uL) required for $bindingComplex")
            }

            if (bindingComplex.udfPacBioMolarityNm == null) {
                process.postErrorMessage("PacBio molarity required for $bindingComplex")
            }
        }
        process.setCompleteStage()
    }
}
