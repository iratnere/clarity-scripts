package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/30/2015.
 */
class McPrepareMagbeadCleanupSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(McPrepareMagbeadCleanupSheet.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(MagbeadCleanup.MAGBEAD_TEMPLATE, false)
        excelWorkbook.addSection(getSections())
        ArtifactNode fileNode = process.getFileNode(MagbeadCleanup.DOWNLOAD_MAGBEAD_CLEANUP)
        excelWorkbook.store(processNode.nodeManager.nodeConfig, fileNode.id)
        logger.info "uploaded ${MagbeadCleanup.DOWNLOAD_MAGBEAD_CLEANUP} to $fileNode"
    }

    Section getSections(){
        List<MagBeadCleanupTableBean> tableBeans = new ArrayList<MagBeadCleanupTableBean>()
        process.inputAnalytes.eachWithIndex { Analyte inputAnalyte, Integer index ->
            MagBeadCleanupTableBean tableBean = new MagBeadCleanupTableBean([
                    number: index+1,
                    pacBioLibrary: inputAnalyte.id,
                    libraryName: inputAnalyte.name,
                    currentVolumeUl: inputAnalyte.udfVolumeUl,
                    currentConcentrationNgUl: inputAnalyte.udfConcentrationNgUl
            ])
            tableBeans.add(tableBean)
        }
        TableSection tableSection = new TableSection(0, MagBeadCleanupTableBean.class, 'A2')
        tableSection.setData(tableBeans)
        return tableSection
    }
}
