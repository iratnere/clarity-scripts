package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioSequencingTemplate
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing.beans.PbSequelLibraryBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by datjandra on 9/16/2015.
 */
class PacBioSequencingPlateCreation extends ClarityProcess {

    static ProcessType processType = ProcessType.PACBIO_SEQUENCING_PLATE_CREATION

    final static String RUN_DESIGN_SMARTLINK_7 = 'RunDesignSMRTLink7'
    boolean testMode = false

    PacBioSequencingPlateCreation(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':PbPreProcessValidation,
            'PrepareRunDesign':PbPrepareRunDesign,
            'ProcessRunDesign':PbProcessRunDesign,
            'RouteToNextWorkflow':PbRouteToWorkflow
        ]
    }

    String getSequencerName() {
        if (isPacBioSequel) {
            return processNode.getUdfAsString(ClarityUdf.PROCESS_SEQUEL_SEQUENCER.udf,'unknown')
        }
        return processNode.getUdfAsString(ClarityUdf.PROCESS_PACBIO_SEQUENCER.udf,'unknown')
    }

    String getSmartLinkUuid() {
        processNode.getUdfAsString(ClarityUdf.PROCESS_SMRT_LINK_UUID.udf)
    }

    String getDnaSequencingReagentKit() {
        processNode.getUdfAsString(ClarityUdf.PROCESS_DNA_SEQUENCING_REAGENT_KIT.udf,'')
    }

    String getDnaInternalControlComplexLot() {
        processNode.getUdfAsString(ClarityUdf.PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT.udf, '')
    }

    ContainerNode getOutputContainerNode(){
        return outputAnalytes.first().containerNode
    }

    List<PbSequelLibraryBean> getPbSequelLibraryBeans(String runName) {
        List<PbSequelLibraryBean> beans = []
        outputAnalytes.sort { it.artifactNode.location }.each {
            PacBioSequencingTemplate template = it as PacBioSequencingTemplate
            beans << getPbSequelLibraryBean(runName, template)
            if (template.isClarityLibraryPool) {
                beans.addAll(getPoolMembersBeans(template))
            }
        }
        return beans
    }

    PbSequelLibraryBean getPbSequelLibraryBean(String runName, PacBioSequencingTemplate template){
        PbSequelLibraryBean bean = new PbSequelLibraryBean(
                runName: runName,
                systemName: template.runMode,
                isCollection: Boolean.TRUE.toString().toUpperCase(),//template.isCollection,
                sampleWell: template.getWellLocationInPacBioFormat(),
                sampleName: template.sampleNameForCsv,
                sequencingMode: template.sequencingMode,
                movieTimeHrs: template.movieTimeHrs,
                insertSizeBp: template.insertSize,
                onPlateLoadingConcentrationPm: template.onPlateLoadingConcentrationPm,
                sizeSelection: template.sizeSelection,
                templatePrepKitBoxBarcode: template.dnaTemplatePrepKitBoxBarcode,
                bindingKitBoxBarcode: template.bindingKitBoxBarcodeAsExcelString, //from parent analyte (binding complex)
                sequencingKitBoxBarcode: dnaSequencingReagentKit,
                dnaControlComplexBoxBarcode: dnaInternalControlComplexLot,
                automationName: template.automationName,
                generateCcsData: template.generateCcsData,
                sampleIsBarcoded: template.sampleIsBarcoded,
                barcodeSet: template.isClarityLibraryPool ? smartLinkUuid : null,
                sameBarcodesOnBothEndsOfSequence: template.sameBarcodesOnBothEndsOfSequence,
                //barcodeName: template.barcodedSampleName
        )
        def errorMsg = bean.validate()
        if (errorMsg) {
            postErrorMessage(errorMsg as String)
        }
        bean
    }

    static List<PbSequelLibraryBean> getPoolMembersBeans(PacBioSequencingTemplate template) {
        return (template.parentLibrary as ClarityLibraryPool).poolMembers.collect{
            def indexName = (it as ClarityLibraryStock).udfIndexName
            new PbSequelLibraryBean(
                    isCollection: Boolean.FALSE.toString().toUpperCase(),
                    sampleName: template.sampleNameForCsv,
                    barcodeName: "$indexName",
                    bioSampleName: it.name,
            )
        }?.sort { it.barcodeName }
    }
}