package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import gov.doe.jgi.pi.pps.util.util.OnDemandCache
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 8/19/15.
 */
class AbandonWorkProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.ABANDON_WORK
    static final logger = LoggerFactory.getLogger(AbandonWorkProcess.class)

    boolean testMode = false
    OnDemandCache<Map<Analyte, List<Analyte>>> cachedDescendents = new OnDemandCache<>()


    AbandonWorkProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':AbandonWorkPreProcessValidation,
            'Process Abandon Work':AbandonWork,
            'Route to Next Workflow':AbandonWorkRouteToWorkflow
        ]
    }


    Map<Analyte, List<Analyte>> getInputAnalytesToDescendants() {
        return cachedDescendents.fetch {
            ArtifactNodeService artifactNodeService = BeanUtil.getBean(ArtifactNodeService.class)
            NodeManager nodeManager = BeanUtil.nodeManager
            Map<Analyte, List<Analyte>> analyteDescendantsMap = [:]
            inputAnalytes.each { Analyte sampleAnalyte ->
                List<Analyte> descendants = []
                analyteDescendantsMap[sampleAnalyte] = descendants
                List<String> descendantLimsIds = artifactNodeService.getDescendantList(sampleAnalyte.id)
                if (descendantLimsIds) {
                    nodeManager.getArtifactNodes(descendantLimsIds).each { ArtifactNode artifactNode ->
                        Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
                        descendants << analyte
                    }
                }
            }
            return analyteDescendantsMap
        }
    }


    void validateDecsendantStages(def inputAnalytes = inputAnalytes) {
        inputAnalytes.each { analyte ->
            List<Analyte> descendants = inputAnalytesToDescendants[analyte]
            descendants.each {
                if (it.isInProgressStages) {
                    def stageNames = it.artifactNode.stagesInProgress.collect{it.name}
                    postErrorMessage("""
Input analyte '$analyte.id($analyte.name)' has a descendant '$it.id($it.name)'
that is part of active/started processes: $stageNames.
Cannot abandon work.
                    """)
                }
            }
        }
    }

    void validateInputStages(def inputAnalytes = inputAnalytes) {
        inputAnalytes.each { Analyte analyte ->
            List<ArtifactWorkflowStage> stagesInProgress = analyte.artifactNode.stagesInProgress.findAll{it.getName() != Stage.ABANDON_WORK.value}
            if (stagesInProgress?.size() > 0) {
                postErrorMessage("""
Input analyte '$analyte.id($analyte.name)' is part of active/started processes: ${stagesInProgress.collect{it.name}}.
Cannot abandon work.
                    """)
            }
        }
    }

}
