package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit

interface SequencerType {

    //PPV
    void validateInputIsLibrary()
    void validateNumberInputs()
    void validateInputFlowcellType()
    //PrepareDilutionSheet
    String getTemplateName()
    void validateOutputContainerType()
    void updateWorkbookStyle(ExcelWorkbook excelWorkbook)
    int getActiveTabIndex()
    def getTableBean(ClarityPhysicalRunUnit outputAnalyte, List<FreezerContainer> freezerContainers)
    String getEndAddress()
    String getStartAddress()
    void uploadSampleSheet()
    //ProcessDilutionSheet
    boolean isValidateLane()
    //RouteToNextWorkflow
    void routeToNextWorkflow()
    List<Analyte> getRepeatOutputAnalytes()
}