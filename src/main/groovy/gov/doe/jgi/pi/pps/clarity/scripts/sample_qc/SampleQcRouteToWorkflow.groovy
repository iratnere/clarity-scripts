package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.RecordDetailsInfo
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.notification.SampleFailedQcEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleReplacementService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/26/15.
 */
class SampleQcRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SampleQcRouteToWorkflow.class)
    def failedTubes = []

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        performLastStepActions()
        failedTubes?.each { List<Analyte> analytes ->
            if(analytes)
                new ProcessUtility(this.process).sendEmailNotification(analytes, new SampleFailedQcEmailNotification())
        }
    }

    void performLastStepActions() {
        SampleQcProcess sampleQcProcess = (SampleQcProcess) process
        List<SampleQcAdapter> adapters = sampleQcProcess.sampleQcAdapters
        assert adapters
        adapters.each { SampleQcAdapter adapter ->
            RecordDetailsInfo recordDetailsInfo = new RecordDetailsInfo(sampleQcProcess)
            List<ArtifactNode> sampleArtifacts = adapter.processRecordDetails(recordDetailsInfo)
            if(sampleArtifacts)
                process.routeArtifactNodes(adapter.destinationStage, sampleArtifacts)
            List<Analyte> failedTubeSamples = adapter.getFailedTubes(recordDetailsInfo)
            if(failedTubeSamples) {
                failedTubes << failedTubeSamples
                SampleReplacementService sampleReplacementService = BeanUtil.getBean(SampleReplacementService.class)
                def responseCode = sampleReplacementService?.replaceSamples(process.researcherContactId, failedTubeSamples.collect {
                    it.claritySample.pmoSample.pmoSampleId
                } as Set)
                assert responseCode == 201
            }
        }
    }
}
