package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by lvishwas on 10/22/16.
 */
class OnboardingSequencingFileEmailNotification extends EmailNotification {
    static final String SUBJECT = 'Analytes imported into clarity'
    static final String CC_EMAIL_GROUP = 'jgi-its-onboard@lbl.gov'
    static final String TEMPLATE_FILE_NAME = 'onboarding_sequencing_files'

    @Override
    protected def getToList(List<EmailDetails> emailDetailsList) {
        //TODO: update to list to valid email ids
        return CC_EMAIL_GROUP
    }

    @Override
    protected def getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return SUBJECT
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return ''
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new OnboardingEmailDetails(it)}
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        return [contact:'Chris', date: new Date(), link: additionalInfo]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
