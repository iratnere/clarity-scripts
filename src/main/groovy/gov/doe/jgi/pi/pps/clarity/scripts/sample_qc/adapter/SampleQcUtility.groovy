package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter

import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.NodeManagerUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SampleQcUtility {
    static final Logger logger = LoggerFactory.getLogger(SampleQcUtility.class)
    List<Analyte> analytes
    Map<Long, JSONArray> sampleSowItems
    ScheduledSampleService scheduledSampleService

    static final String SIP_SAMPLE_SOW_TYPE_PREFIX = "SIP"
    static final String SIP_ORIGINAL_SAMPLE_SOW_TYPE = SowItemTypeCv.SIP_SOURCE_SAMPLE.value

    SampleQcUtility(List<Analyte> analytes, ScheduledSampleService scheduledSampleService = BeanUtil.getBean(ScheduledSampleService.class)) {
        this.analytes = analytes
        this.scheduledSampleService = scheduledSampleService
    }

    Map<String, List> getSowItemsForSamples() {
        if (!sampleSowItems) {
            List<Long> pmoSampleIds = analytes.collect {
                it.claritySample.pmoSample.pmoSampleId
            }
            logger.info "Retrieving SOW Item metadata for samples $pmoSampleIds"
            if (!scheduledSampleService)
                scheduledSampleService = BeanUtil.getBean(ScheduledSampleService.class)
            sampleSowItems = scheduledSampleService.getBatchSowItemMetadataForSample(pmoSampleIds)
        }
        sampleSowItems
    }

    boolean isSipSample(Long pmoSampleId) {
        List<String> sowItemTypes = getSowItemTypes(pmoSampleId)
        assert sowItemTypes
        if (sowItemTypes[0].startsWith(SIP_SAMPLE_SOW_TYPE_PREFIX)) {
            assert sowItemTypes.size() == 1
            return true
        }
        return false
    }

    boolean isSipOriginalSample(Long pmoSampleId) {
        List<String> sowItemTypes = getSowItemTypes(pmoSampleId)
        if (sowItemTypes.contains(SIP_ORIGINAL_SAMPLE_SOW_TYPE) && sowItemTypes.size() == 1)
            return true
        return false
    }

    Map<Long, Integer> getSampleQcTypeIds(Long pmoSampleId) {
        Map<Long, Integer> qcTypeIds = [:]
        Iterator iterator = sowItemsForSamples["${pmoSampleId}"]?.iterator()
        List<String> sowTerminalStatuses = [SowItemStatusCv.DELETED.value]//PPS-3814
        while (iterator?.hasNext()) {
            def sow = iterator.next()
            if (sow.'qc-type-id' && !sowTerminalStatuses.contains(sow.'current-status')) {
                qcTypeIds[sow.'sow-item-id'] = sow.'qc-type-id'
            }
        }
        logger.info "QC Types for sample ${pmoSampleId}: $qcTypeIds"
        return qcTypeIds
    }

    String getSMInstructions(Long pmoSampleId) {
        String smInstructions = ''
        Iterator iterator = sowItemsForSamples["${pmoSampleId}"]?.iterator()
        while (iterator?.hasNext()) {
            def sow = iterator.next()
            if (sow.'sm-instructions') {
                smInstructions += '[' + sow.'sm-instructions' + ']'
            }
        }
        logger.info "SM Instructions for sample ${pmoSampleId}: $smInstructions"
        return smInstructions
    }

    Map<Long, String> getSowItemsStatus(Long pmoSampleId) {
        Map<String, String> sowItemsStatus = [:]
        Iterator iterator = sowItemsForSamples["${pmoSampleId}"]?.iterator()
        while (iterator?.hasNext()) {
            def sow = iterator.next()
            if (sow.'current-status') {
                sowItemsStatus[sow.'sow-item-id'] = sow.'current-status'
            }
        }
        logger.info "SOW Item status for sample ${pmoSampleId}: $sowItemsStatus"
        return sowItemsStatus
    }

    boolean getIsITag(Long pmoSampleId) {
        Iterator iterator = sowItemsForSamples["${pmoSampleId}"]?.iterator()
        while (iterator?.hasNext()) {
            def sow = iterator.next()
            if (sow.'itag-primer-set') {
                return true
            }
        }
        return false
    }

    List<String> getSowItemTypes(Long pmoSampleId) {
        List<String> sowTypes = []
        Map<String, List> samplesSowItems = getSowItemsForSamples()
        List<JSONObject> sowItems = samplesSowItems["${pmoSampleId}"]
        List<String> sowTerminalStatuses = [SowItemStatusCv.DELETED.value]
        sowItems.each { JSONObject sow ->
            if (sow.'sow-item-type' && !sowTerminalStatuses.contains(sow.'current-status')) {
                sowTypes << sow.'sow-item-type'
            }
        }
//        Iterator iterator = sowItemsForSamples[pmoSampleId]?.iterator()
//        List<String> sowTerminalStatuses = [SowItemStatusCv.DELETED.value]
//        while(iterator?.hasNext()){
//            def sow = iterator.next()
//            if(sow.'sow-item-type' && !sowTerminalStatuses.contains(sow.'current-status')){
//                sowTypes << sow.'sow-item-type'
//            }
//        }
            logger.info "SOW Item types for sample ${pmoSampleId}: $sowTypes"
            return sowTypes
        }

        List<String> lookUpSamplesBySipGroupName(String groupName) {
            NodeManagerUtility nodeManagerUtility = new NodeManagerUtility(BeanUtil.nodeManager)
            List<String> sampleLimsIds = nodeManagerUtility.lookUpSamplesBySipGroupName(groupName)
            NodeManager nodeManager = nodeManagerUtility.clarityNodeManager
            List<ClaritySample> samples = nodeManager.getSampleNodes(sampleLimsIds).collect {
                SampleFactory.sampleInstance(it)
            }
            return samples.findAll { it instanceof PmoSample }?.collect { it.id }
        }

        String checkIfGroupComplete(String groupName, List<String> sampleLimsIds) {
            List<String> diff = sampleLimsIds.minus(analytes.collect { it.claritySample.id })
            if (!diff)
                return ""
            return "All samples of a SIP group $groupName should be QC'ed together. Please add samples $diff to the icebucket and restart the process."
        }
    }
