package gov.doe.jgi.pi.pps.clarity.dto

import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import net.sf.json.JSONObject

/**
 * Created by dscott on 11/18/2016.
 */
class RunModeDto {

    RunModeDto(){}

    RunModeDto(RunModeCv runModeCv) {
        this.runModeId = runModeCv.id
        this.runMode = runModeCv.runMode
        this.active = runModeCv.active == 'Y'
        this.readLengthBp = runModeCv.readLengthBp
        this.readTotal = runModeCv.readTotal
        this.workflowName = runModeCv.sequencingWorkflowName
        this.workflowUri = runModeCv.sequencingWorkflowUri
        this.sequencerModel = runModeCv.sequencerModel.sequencerModel
        this.platform = runModeCv.sequencerModel.platform
    }

    Long runModeId
    String runMode
    boolean active = true

    BigInteger readLengthBp
    BigInteger readTotal

    String workflowUri
    String workflowName

    String sequencerModel
    String platform

    JSONObject toJson() {
        JSONObject json = new JSONObject()
        json['run-mode-id'] = runModeId
        json['run-mode'] = runMode
        json['active'] = active
        json['read-total'] = readTotal
        json['read-length-bp'] = readLengthBp
        json['workflow-uri'] = workflowUri
        json['workflow-name'] = workflowName
        json['sequencer-model'] = sequencerModel
        json['platform'] = platform
        json
    }

    String toString() {
        runMode
    }
}
