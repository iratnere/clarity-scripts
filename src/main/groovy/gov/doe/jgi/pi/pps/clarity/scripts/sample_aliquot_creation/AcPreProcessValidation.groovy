package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AcPreProcessValidation extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(AcPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        validateInputAnalyteClass()
        validateParentSampleContainerUdf(process.batchPmoSampleContainerNodes)
    }

    void validateInputAnalyteClass(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { Analyte analyte ->
            analyte.validateIsScheduledSample()
        }
    }

    void validateParentSampleContainerUdf(List<ContainerNode> parentSampleContainerNodes) {
        parentSampleContainerNodes.each { ContainerNode containerNode ->
            String containerLocationUdf = containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION.udf)
            if (containerLocationUdf && containerLocationUdf != PmoSample.CONTAINER_LOCATION_ON_SITE) { //PPS-2142
                process.postErrorMessage("Parent sample container '${containerNode.id}' has an invalid ${ClarityUdf.CONTAINER_LOCATION.value} udf: '$containerLocationUdf'. Expected udf: '$PmoSample.CONTAINER_LOCATION_ON_SITE'.")
            }
        }
    }
}
