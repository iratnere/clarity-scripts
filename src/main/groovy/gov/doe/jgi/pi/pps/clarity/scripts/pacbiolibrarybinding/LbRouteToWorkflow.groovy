package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibrarybinding

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/6/2015.
 */
class LbRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LbRouteToWorkflow.class)

    void execute(){
        logger.info "Starting ${this.class.name} action...."

        Stage inputStage = process.inputStage
        if (inputStage.isPacBioSequel) {
            /*
                dNTP v3
                Column Conditioning Buffer
                Clean up Columns
                Qubit Assay Kit
            */
            requireProcessUdfForStage(ClarityUdf.PROCESS_DNTP_V3,inputStage)
            requireProcessUdfForStage(ClarityUdf.PROCESS_COLUMN_CONDITIONING_BUFFER,inputStage)
            requireProcessUdfForStage(ClarityUdf.PROCESS_CLEAN_UP_COLUMNS,inputStage)
            requireProcessUdfForStage(ClarityUdf.PROCESS_QUBIT_ASSAY_KIT,inputStage)
        }
        checkErrorMessages()

        List<ArtifactNode> inputAnalytes = processNode.inputAnalytes
        for (ArtifactNode input : inputAnalytes) {
            Analyte inputAnalyte = AnalyteFactory.analyteInstance(input)
            List<ArtifactNode> outputArtifacts = processNode.getOutputAnalytes(input.id)
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(outputArtifacts.first())

            PacBioBindingComplex bindingComplex = outputAnalyte as PacBioBindingComplex
            BigDecimal volumeUsed = bindingComplex.udfVolumeUsedUl
            if (volumeUsed == null) {
                process.postErrorMessage("Library Volume Used (uL) required for $bindingComplex")
            } else {
                inputAnalyte.setUdfVolumeUl(inputAnalyte.udfVolumeUl - volumeUsed)
            }

            if (bindingComplex.udfVolumeUl == null) {
                process.postErrorMessage("Volume (uL) required for $bindingComplex")
            }

            if (bindingComplex.udfPacBioMolarityNm == null) {
                process.postErrorMessage("PacBio molarity required for $bindingComplex")
            }
        }
        performLastStepActions()
        process.writeLabels(process.outputAnalytes)
    }

    void performLastStepActions(){
        List<Analyte> passedOutputs = []
        Set<ArtifactNodeInterface> failedOutputs = new HashSet<ArtifactNode>()
        process.outputAnalytes.each { Analyte output ->
            if (output.artifactNode.systemQcFlag?.booleanValue()) {
                passedOutputs << output
            } else if (!output.artifactNode.systemQcFlag?.booleanValue()) {
                failedOutputs.add(output.artifactNode)
            } else {
                process.postErrorMessage("QC flag required for $output")
            }
        }
        if (process.isPacBioSequel) {
            routeSequelOutputs(passedOutputs as List<PacBioBindingComplex>)
        } else {
            throw new WebException("no handler for ${this.process} input stage [${process.inputStage}]",422)
        }

        if (failedOutputs) {
            process.routeArtifactNodes(Stage.ABANDON_QUEUE, failedOutputs)
            StatusService statusService =
                    BeanUtil.getBean(StatusService.class)
            Map<Long, SowItemStatusCv> sowItemStatusMap = new HashMap<Long, SowItemStatusCv>()
            failedOutputs.each { ArtifactNode artifactNode ->
                List<ClaritySample> claritySamples = artifactNode.parentSampleNodes.collect{ SampleFactory.sampleInstance(it)}
                claritySamples.each { ClaritySample claritySample ->
                    if (claritySample instanceof ScheduledSample) {
                        sowItemStatusMap.put((claritySample as ScheduledSample).getSowItemId(), SowItemStatusCv.NEEDS_ATTENTION)
                    }
                }
            }
            statusService.submitSowItemStatus(sowItemStatusMap, process.researcherContactId)
        }
    }

    void routeSequelOutputs(List<PacBioBindingComplex> passedOutputs){
        Map<String, List<String>> uriToLimsIds = getUriToOutputIds(passedOutputs)
        uriToLimsIds.each { String uri, List<String> limsIds ->
            process.routeArtifactIdsToUri(uri, limsIds)
        }
    }

    static Map<String, List<String>> getUriToOutputIds(List<PacBioBindingComplex> passedOutputs){
        Map<String, List<String>> uriToOutputIds = [:].withDefault {[]}
        passedOutputs.each { PacBioBindingComplex bindingComplex ->
            String uri = bindingComplex.parentLibrary.runModeCv.sequencingWorkflowUri
            uriToOutputIds[uri] << bindingComplex.id
        }
        uriToOutputIds
    }
}
