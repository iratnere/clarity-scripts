package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcKeyValueResultsBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPoolTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.clarity.scripts.services.PostProcessService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LibraryCreationProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.LC_LIBRARY_CREATION

    static final Logger logger = LoggerFactory.getLogger(LibraryCreationProcess.class)

	static final SCRIPT_GENERATED_LIBRARY_CREATION_SHEET = 'Download Library Creation Worksheet'
	static final UPLOADED_LIBRARY_CREATION_SHEET = 'Upload Library Creation Worksheet'
    static final SCRIPT_GENERATED_ECHO_FILE = 'EchoPickFile'
	boolean testMode = false
    static final String START_ERROR_MSG = 'Please correct the errors below and upload spreadsheet again.'
    final static List PLATE_CORNERS = ['A1', 'A12', 'H1', 'H12']

    LibraryCreation lcAdapter
    ExcelWorkbook excelWorkbookCached
    def resultsBeanCached
    List libraryStockBeansCached
    List<LcPoolTableBean> libraryPoolBeansCached
    String excelFileName = UPLOADED_LIBRARY_CREATION_SHEET
    def sheetIndex = 0

    LibraryCreationProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation':LcPreProcessValidation,
                'Automatic Placement':LcAutomaticPlacement,
                'PrepareLibraryCreationSheet': LcPrepareLibraryCreationSheet,
                'ProcessLibraryCreationSheet': LcProcessLibraryCreationSheet,
                'Print Labels':LcPrintLabels,
                'Print Fragment Analyzer Labels': LcPrintFragmentAnalyzerLabels,
                'Route to Next Workflow':LcRouteToWorkflow
        ]
    }

    LibraryCreation initializeLcAdapter(Analyte inputAnalyte = inputAnalytes[0],
                                        boolean isOnPlate = selectedContainer.isPlate) {
        logger.info "Starting ${this.class.simpleName} initialize LibraryCreationAdapter...."
        if (inputAnalyte.isInternalSingleCell)
            return new LibraryCreationPlateSingleCellInternal(this)
        if (inputAnalyte.isDapSeq)
            return new LibraryCreationDapSeq(this)
        if (inputAnalyte.isExomeCapture)
            return new LibraryCreationExomeCapture(this)
        if (inputAnalyte.isItag)
            return new LibraryCreationItag(this)
        if (isOnPlate && inputAnalyte.isSmallRna)
            return new LibraryCreationPlateSmallRna(this)
        if (!isOnPlate && inputAnalyte.isSmallRna)
            return new LibraryCreationTubeIllumina(this)
        if (isOnPlate && inputAnalyte.isIllumina)
            return new LibraryCreationPlateIllumina(this)
        if (!isOnPlate && inputAnalyte.isIllumina)
            return new LibraryCreationTubeIllumina(this)
        if (!isOnPlate && inputAnalyte.isPacBio && !isMultiplexed(inputAnalyte))
            return new LibraryCreationTubePacBio(this)
        if (!isOnPlate && inputAnalyte.isPacBio && isMultiplexed(inputAnalyte))
            return new LibraryCreationTubePacBioMultiplexed(this)
        if (isOnPlate && inputAnalyte.isPacBio)
            return new LibraryCreationPlatePacBio(this)
        return postErrorMessage("Analyte($inputAnalyte.name) :unknown template type and/or platform")
    }

    boolean isMultiplexed(Analyte inputAnalyte){
        return getProtocolName(inputAnalyte) in [
                ClarityWorkflow.LC_PACBIO_MULTIPLEXED_2KB_AMPLICON_TUBES.value,
                ClarityWorkflow.LC_PACBIO_MULTIPLEXED_3KB_TUBES.value,
                ClarityWorkflow.LC_PACBIO_MULTIPLEXED_10KB_TUBES.value,
                ClarityWorkflow.LC_PACBIO_MULTIPLEXED_10KB_BLUE_PIPPIN_TUBES.value,
                ClarityWorkflow.LC_PACBIO_MULTIPLEXED_PRODUCTION_RND_TUBES.value
        ]
    }

    static DropDownList getDropDownIndexSet() {
        ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService)
        DropDownList indexSetDropDownList = artifactIndexService.getIndexSetDropDownList()
        //PPS-4733 - To allow custom values in drop down lists
        indexSetDropDownList.showErrorBox=false
        return indexSetDropDownList
    }

    ContainerNode getSelectedContainer() {
        NodeManager clarityNodeManager = BeanUtil.nodeManager
        PlacementsNode placementsNode = processNode.placementsNode
        String outputContainerId = placementsNode.selectedContainerIds[0] //first available output plate container
        return clarityNodeManager.getContainerNode(outputContainerId)
    }

    String getFreezerPath(List<FreezerContainer> freezerContainers = freezerLookupInputAnalytes()) {
        return freezerContainers?.collect { FreezerContainer fc -> "${fc.location}[${fc.checkInStatus}]" }?.join(';')
    }

    def freezerLookupInputAnalytes() {
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        return freezerService.freezerLookupInputAnalytes(this)
    }

    LcKeyValueResultsBean getResultsBean() {
        if (resultsBeanCached) {
            return resultsBeanCached
        }
        LcKeyValueResultsBean bean = (LcKeyValueResultsBean) getBeanList(LcKeyValueResultsBean.class.simpleName)
        def errorMsg = bean.validate()
        if(errorMsg)
            postErrorMessage(errorMsg as String)
        resultsBeanCached = bean
        return resultsBeanCached
    }

    List getLibraryStockBeans() {
        if (libraryStockBeansCached) {
            return libraryStockBeansCached
        }
        List beansList = (List) getBeanList(lcAdapter.getLcTableBeanClassName())
        lcAdapter.removeCornerBeans(beansList)
        libraryStockBeansCached = beansList
        return libraryStockBeansCached
    }

    ExcelWorkbook getExcelWorkbook() {
        if (excelWorkbookCached) {
            return excelWorkbookCached
        }
        def fileNode = getFileNode(excelFileName)
        excelWorkbookCached = new ExcelWorkbook(fileNode.id)
        excelWorkbookCached.load()
        excelWorkbookCached
    }

    def getBeanList(String beanName) {
        def beansList = getExcelWorkbook().sections.values()?.find {
            it.beanClass?.simpleName == beanName && it.parentSheetIndex == sheetIndex
        }?.getData()
        beansList
    }

    def getLcTableBean(limsId, Boolean passed = null) {
        def lcTableBean = libraryStockBeans.find { it.libraryLimsId == limsId }
        if (!lcTableBean) {
            //throw new WebException([code:'LcProcessLibraryCreationSheet.LcTableBean.missing',args:[limsId]], 422)
            postErrorMessage("Cannot find a library stock by the library stock limsid '$limsId'")
        }
        lcTableBean.passed = passed
        (lcTableBean instanceof LcTubeBean) ? lcTableBean.lcAdapter = lcAdapter : null
        def errorMsg = lcTableBean.validate()
        if(errorMsg)
            postErrorMessage(errorMsg as String)
        lcTableBean
    }

    LcPoolTableBean getLcPoolTableBean(poolNumber) {
        LcPoolTableBean lcPoolTableBean = libraryPoolBeans.find { it.poolNumber == poolNumber }
        if (!lcPoolTableBean) {
            postErrorMessage("Cannot find a library pool by the pool number '$poolNumber'")
            //throw new WebException([code:'LcProcessLibraryCreationSheet.transferBeansDataToClarityLibraryPools.failed',args:[clarityLibraryPool,lcPoolTableBean]], 422)
        }
        lcPoolTableBean
    }

    List<LcPoolTableBean> getLibraryPoolBeans() {
        if (libraryPoolBeansCached) {
            return libraryPoolBeansCached
        }
        List<LcPoolTableBean> beansList = (List<LcPoolTableBean>) getBeanList(LcPoolTableBean.class.simpleName)
        beansList.each {
            def errorMsg = it.validate()
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        if (!beansList.find{it.poolNumber > 0}) {
            postErrorMessage("""
$START_ERROR_MSG
Pool Info Section
Cannot find valid library pools
            """)
        }
        List<LcPlateTableBean> libraryStockBeans = getLibraryStockBeans()
        libraryStockBeans.each{
            def errorMsg = it.validateLibraryStockInPool()
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        beansList.each { LcPoolTableBean lcPoolTableBean ->
            if (lcPoolTableBean.passed) {
                List<LcPlateTableBean> poolMembers = libraryStockBeans.findAll {it.poolNumber == lcPoolTableBean.poolNumber}
                if (poolMembers.size() < 2) {
                    postErrorMessage("""
$START_ERROR_MSG
Plate Info Section
Cannot find valid pool members for the library pool number '${lcPoolTableBean.poolNumber as BigInteger}'
            """)
                }
            }
        }
        libraryPoolBeansCached = beansList
        return libraryPoolBeansCached
    }

    static List<ContainerNode> getContainerNodes(List<Analyte> analytes) {
        return analytes.collect { it.containerNode }.unique()
    }

    static ContainerTypes getContainerType(List<ContainerNode> containerNodes) {
        Set<ContainerTypes> containerTypes = containerNodes.collect {it.containerTypeEnum}.unique()
        return containerTypes.find{ true }
    }

    def validateOutputContainerType(
            List<ContainerNode> outputContainerNodes = getContainerNodes(outputAnalytes),
            List<ContainerNode> inputContainerNodes = getContainerNodes(inputAnalytes)
    ) {
        ContainerTypes inputContainerType = getContainerType(inputContainerNodes)
        ContainerTypes outputContainerType = getContainerType(outputContainerNodes)
        if (inputContainerType != outputContainerType) {
            postErrorMessage("""
                Process output container type '${outputContainerType?.value}'
                does not match input container type '${inputContainerType?.value}'.
                Please select '${inputContainerType?.value}' as the output container.
            """)
        }
    }

    def findLibraryStockBean(ClarityLibraryPool clarityLibraryPool){
        def libraryStockBean = libraryStockBeans.find{ it.libraryLimsId in clarityLibraryPool.poolMembers*.id}
        if (!libraryStockBean || libraryStockBean.poolNumber < 1) {
            postErrorMessage("LcProcessLibraryCreationSheet.transferBeansDataToClarityLibraryPools.failed: clarityLibraryPool($clarityLibraryPool), libraryStockBean($libraryStockBean)")
        }
        libraryStockBean
    }

    BigInteger getActualQueueId(String protocolName = getProtocolName()) {
        if (!protocolName)
            return null
        String queueName = protocolName.replaceFirst(ClaritySampleAliquot.LIBRARY_CREATION_WORKFLOW_PREFIX, "")
        LibraryCreationQueueCv lcQueue = LibraryCreationQueueCv.findByLibraryCreationQueue(queueName)
        if (lcQueue)
            return lcQueue.id as BigInteger
        return null //exception ? no queue found for name [{0}]
    }

    String getProtocolName(Analyte inputAnalyte = inputAnalytes[0]) {
        return inputAnalyte.artifactNode?.getProtocolNodeInProgress(processNode)?.name
    }

    def updateNonWorksheetUdfs() {
        outputAnalytes.each { (it as ClarityLibraryStock).updateLibraryCreationUdfs(actualQueueId, researcher?.fullName) }
    }

    Section getKeyValueResultSection() {
        Section kvSection = new KeyValueSection(0, LcKeyValueResultsBean.class, 'A2','B20')
        LcKeyValueResultsBean bean = lcKeyValueResultsBean
        kvSection.setData(bean)
        return kvSection
    }

    LcKeyValueResultsBean getLcKeyValueResultsBean(
            List<Analyte> inputAnalytes = inputAnalytes,
            List<ClarityLibraryStock> outputAnalytes = outputAnalytes
    ) {
        LibraryStockFailureModesService libraryStockFailureModesService = BeanUtil.getBean(LibraryStockFailureModesService.class)
        Analyte inputAnalyte = inputAnalytes[0]
        ScheduledSample claritySample = (ScheduledSample)inputAnalyte.claritySample
        LcKeyValueResultsBean bean = new LcKeyValueResultsBean()
        bean.materialCategory = claritySample.sequencingProject.udfMaterialCategory
        bean.inputPlateBarcode = inputAnalyte.containerNode.name
        bean.outputPlateBarcode = outputAnalytes[0].containerNode.name
        bean.plateName = outputAnalytes[0].getContainerUdfLabel()
        bean.numberSamples = inputAnalytes.size()
        bean.libraryCreationQueue = inputAnalyte.libraryCreationQueue?.libraryCreationQueue
        bean.lcInstructions = claritySample.udfLcInstructions
        bean.smNotes = claritySample.udfNotes
        bean.claimant = researcher?.fullName
        bean.targetTemplateSize = claritySample.udfTargetTemplateSizeBp
        bean.itagPrimerSet = claritySample.udfItagPrimerSet
        bean.dop = claritySample.udfDegreeOfPooling
        bean.freezerPath = getFreezerPath()
        bean.captureProbeSet = claritySample.udfExomeCaptureProbeSet
        bean.targetAliquotMass = inputAnalyte.containerUdfFinalAliquotMassNg
        bean.targetAliquotVolume = inputAnalyte.containerUdfFinalAliquotVolumeUl
        bean.failureMode = libraryStockFailureModesService.dropDownFailureModes
        bean.plateResult = libraryStockFailureModesService.dropDownPassFail
        bean.libraryIndexSet = dropDownIndexSet
        bean
    }

    void processCompleteFailure(def failedLibraryStock) {
        if (!failedLibraryStock) {
            return
        }
        List<ArtifactNode> firstTryDnaSamples = []
        List<ArtifactNode> firstTryRnaSamples = []
        List<ArtifactNode> secondTrySamples = []
        failedLibraryStock.each { ClarityLibraryStock clarityLibraryStock ->
            ClaritySample claritySample = clarityLibraryStock.claritySample
            BigDecimal lcAttempt = claritySample.udfLcAttempt
            if ( !lcAttempt || lcAttempt < 2 ) {
                postErrorMessage("Clarity Sample($claritySample.id): invalid udf LC Attempt '$lcAttempt'")
            }
            if (lcAttempt == 2.0) { //first try
                if (claritySample.isDnaProject) {
                    firstTryDnaSamples << claritySample.sampleArtifactNode
                } else {
                    firstTryRnaSamples << claritySample.sampleArtifactNode
                }
            }
            if (lcAttempt > 2) { //second try
                secondTrySamples << claritySample.sampleArtifactNode
            }
        }
        PostProcessService taskGenerationService = BeanUtil.getBean(PostProcessService.class)
        if (firstTryDnaSamples) {
            taskGenerationService.postInputProcess(this, Stage.AUTOMATIC_REQUEUE_LIBRARY_CREATION,firstTryDnaSamples)
        }
        if (firstTryRnaSamples) {
            taskGenerationService.postInputProcess(this, Stage.AUTOMATIC_REQUEUE_LIBRARY_CREATION,firstTryRnaSamples)
        }
        routeArtifactNodes(Stage.ABANDON_WORK,secondTrySamples)
        routeArtifactNodes(Stage.REQUEUE_LIBRARY_CREATION,secondTrySamples)
        routeArtifactIds(Stage.ABANDON_QUEUE,failedLibraryStock*.id)
        //PPS-3121: submit ABANDON RoutingRequests because of the DB triggers
        //first move to Stage.ABANDON_QUEUE then move to Stage.ALIQUOT_CREATION_DNA
        processRoutingRequests()

        routeArtifactNodes(Stage.ALIQUOT_CREATION_DNA,firstTryDnaSamples)
        routeArtifactNodes(Stage.ALIQUOT_CREATION_RNA,firstTryRnaSamples)
        processRoutingRequests()
    }

    static def getIndexContainerBarcodes(List<ClarityLibraryStock> libraryStocks) {
        def indexContainerBarcodes = [] as Set<String>
        libraryStocks.each{
            if (it.udfIndexContainerBarcode)
                indexContainerBarcodes.add(it.udfIndexContainerBarcode)
        }
        return indexContainerBarcodes as List<String>
    }

    static def getIndexNames(List<ClarityLibraryStock> libraryStocks) {
        def indexNames = [] as Set<String>
        libraryStocks.each{
            if (it.udfIndexName && it.udfIndexName != ArtifactIndexService.INDEX_NAME_N_A)
                indexNames.add(it.udfIndexName)
        }
        return indexNames as List<String>
    }

    String getSmNotes(Analyte clarityLibraryStock) {
        if (clarityLibraryStock.isOnPlate)
            return resultsBean.smNotes
        def lcTubeBean = getLcTableBean(clarityLibraryStock.id)
        return lcTubeBean.smNotes
    }

    def updateScheduledSampleNotes(List<Analyte> outputs = outputAnalytes) {
        outputs.each { Analyte clarityLibraryStock ->
            ScheduledSample scheduledSample = (ScheduledSample) clarityLibraryStock.claritySample
            scheduledSample.udfNotes = getSmNotes(clarityLibraryStock)
        }
    }
}