package gov.doe.jgi.pi.pps.clarity.scripts.print

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 3/25/16.
 */
class PrintBarcode extends ActionHandler {

    @Override
    void execute() {
        List<Analyte> analytes = process.sampleAnalyteBeans*.analyte
        process.writeLabels(analytes)
    }
}
