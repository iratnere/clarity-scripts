package gov.doe.jgi.pi.pps.clarity.dto

import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField

/**
 * Created by dscott on 5/23/2016.
 */
class GeneusNodeDto implements Comparable<GeneusNodeDto> {

    String limsid
    String name
    String supportNotes

    String newName

    boolean limsidSelected = false
    boolean nameSelected = false

    boolean getNameUpdate() {
        newName && newName != name
    }

    private Map<String,UserDefinedField> udfs
    private Map<String,UserDefinedField> newUdfs

    String errorMessage

    LinkedList<String> getUdfNames() {
        LinkedList<String> udfNameList = null
        if (udfs) {
            udfNameList = new LinkedList<String>(udfs.keySet())
        }
        udfNameList
    }

    LinkedList<String> getNewUdfNames() {
        LinkedList<String> udfNameList = null
        if (newUdfs) {
            udfNameList = new LinkedList<String>(newUdfs.keySet())
        }
        udfNameList
    }

    List<UserDefinedField> getUdfs() {
        udfs?.values()?.toList()
    }

    List<UserDefinedField> getNewUdfs() {
        newUdfs?.values()?.toList()
    }

    private static Map udfsToMap(Collection<UserDefinedField> udfCollection) {
        Map udfMap = [:]
        udfCollection?.sort{it.name?.trim()?.toLowerCase()}?.each{ UserDefinedField udf -> udfMap[udf.name] = udf}
        udfMap
    }

    void setUdfs(Collection<UserDefinedField> udfCollection) {
        this.udfs = udfsToMap(udfCollection)
    }

    void setNewUdfs(Collection<UserDefinedField> udfCollection) {
        this.newUdfs = udfsToMap(udfCollection)
    }

    UserDefinedField getUdf(udfName) {
        udfs?.get(udfName?.toString())
    }

    UserDefinedField getNewUdf(udfName) {
        newUdfs?.get(udfName?.toString())
    }

    @Override
    int compareTo(GeneusNodeDto o) {
        return this.limsid.compareTo(o.limsid)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        GeneusNodeDto oDto = (GeneusNodeDto) o
        if (limsid != oDto.limsid) return false
        return true
    }

    int hashCode() {
        return limsid.hashCode()
    }

}
