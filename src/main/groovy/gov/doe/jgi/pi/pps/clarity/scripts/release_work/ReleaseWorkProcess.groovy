package gov.doe.jgi.pi.pps.clarity.scripts.release_work

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorklowStageStatus

/**
 * Created by tlpaley on 2/24/16.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 *
 * Clarity process “Release from Hold”/“Release from Needs Attention”.
 * The process has analytes as inputs and produces no outputs.
 * The process must:
 * PPV: do the original queues still exist or are they archived
 * Move analytes back to their original queues
 * Set corresponding sow items status to “In Progress”
 */
class ReleaseWorkProcess extends ClarityProcess {

    static List<ProcessType> processTypes = [ProcessType.RELEASE_FROM_HOLD, ProcessType.RELEASE_FROM_NEEDS_ATTENTION]

    ReleaseWorkProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation': ReleaseWorkPreProcessValidation,
                'Mark Protocol as Complete': ReleaseWorkRecordDetailsProcess,
                'Route to Next Workflow': ReleaseWorkRouteToWorkflow
        ]
    }

    /*
example http://frow.jgi-psf.org:8080/api/v2/artifacts/SCO173476A6PA1
workflowStages:
<?xml version="1.0" encoding="UTF-8"?>
<workflow-stages>
   <workflow-stage status="QUEUED" name="AC Sample Aliquot Creation" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/251/stages/351" />
   <workflow-stage status="REMOVED" name="AC Sample Aliquot Creation" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/251/stages/351" />
   <workflow-stage status="SKIPPED" name="Requeue for Library Creation" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/451/stages/604" />
   <workflow-stage status="SKIPPED" name="Abandon Work" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/452/stages/617" />
   <workflow-stage status="COMPLETE" name="On Hold" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/580/stages/756" />
   <workflow-stage status="COMPLETE" name="On Hold" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/580/stages/756" />
   <workflow-stage status="REMOVED" name="Requeue for Library Creation" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/451/stages/604" />
   <workflow-stage status="REMOVED" name="Abandon Work" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/452/stages/617" />
   <workflow-stage status="QUEUED" name="Release from Hold" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/608/stages/814" />
</workflow-stages>

@return
lastRemovedStages:
    <workflow-stage status="REMOVED" name="Requeue for Library Creation" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/451/stages/604" />
    <workflow-stage status="REMOVED" name="Abandon Work" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/452/stages/617" />
     */
    static List<ArtifactWorkflowStage> getLastRemovedStages(Analyte analyte) {
        List<ArtifactWorkflowStage> lastRemovedStages = []
        analyte.artifactNode.workflowStages.reverse().find{ ArtifactWorkflowStage stage ->
            if (ArtifactWorklowStageStatus.REMOVED == stage.status) { lastRemovedStages << stage }
            ArtifactWorklowStageStatus.COMPLETE == stage.status
        }
        return lastRemovedStages
    }

}
