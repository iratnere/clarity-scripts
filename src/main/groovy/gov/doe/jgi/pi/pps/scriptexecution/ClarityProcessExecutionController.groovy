package gov.doe.jgi.pi.pps.scriptexecution

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ClarityProcessExecutionController {

	@Autowired
	ProcessExecutionService processExecutionService

	@PostMapping("/register-process-execution")
	ResponseEntity<ScriptResponseModel> process(@RequestBody ScriptParams scriptParams) {
		ScriptResponseModel responseModel = processExecutionService.processScriptParams(scriptParams)
		new ResponseEntity<ScriptResponseModel>(responseModel, HttpStatus.OK)
	}

}
