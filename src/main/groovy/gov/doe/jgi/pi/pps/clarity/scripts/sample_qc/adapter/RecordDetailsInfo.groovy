package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.SampleQcProcess

class RecordDetailsInfo {
    TableSection glsStarletUseSection
    List<KeyValueSection> keyValueSections
    def researcherContactId
    BigDecimal replicatesFailurePercentAllowed
    BigDecimal controlsFailurePercentAllowed

    RecordDetailsInfo(){}

    RecordDetailsInfo(SampleQcProcess sampleQcProcess) {
        this.glsStarletUseSection = sampleQcProcess.glsStarletTableSection
        this.keyValueSections = sampleQcProcess.keyValueSections
        this.researcherContactId = sampleQcProcess.researcherContactId
        this.replicatesFailurePercentAllowed = sampleQcProcess.udfReplicatesFailurePercentAllowed
        this.controlsFailurePercentAllowed = sampleQcProcess.udfControlsFailurePercentAllowed
    }
}
