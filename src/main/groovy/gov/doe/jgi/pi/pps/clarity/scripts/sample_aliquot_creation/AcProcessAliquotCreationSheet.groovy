package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AcKeyValuePlateResultsBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.AliquotCreationService
import gov.doe.jgi.pi.pps.clarity.scripts.services.KapaWellConfig
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AcProcessAliquotCreationSheet extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(AcProcessAliquotCreationSheet.class)

    @Override
    void execute() {
        logger.info "Starting ${this.class.simpleName} action...."
        //doRefreshProcessNode()

        logger.info "Sample Aliquots Plate Container: ${process.outputPlateContainer?.id}"
        //
        ContainerNode outputPlateContainer = (process as SampleAliquotCreationProcess).outputPlateContainer
        if (outputPlateContainer) {
            String kapaPlateBarcode = processNode.getUdfAsString(ClarityUdf.PROCESS_KAPA_SPIKE_IN_PLATE_BARCODE.udf)
            if (kapaPlateBarcode)
                processKapaBarcode(kapaPlateBarcode)
        }
        transferBeansDataToClaritySampleAliquots()
        process.setCompleteStage()
    }

    void processKapaBarcode(String kapaPlateBarcode) {
        String[] arr = kapaPlateBarcode.split('_')
        if (arr.length != 3)
            throw new IllegalArgumentException("'Kapa Spike-in Plate Barcode' UDF is invalid: '$kapaPlateBarcode'.")
        String kapaLotId = arr[0]
        String kapaBatchId = arr[1]
        if (!kapaBatchId)
            throw new IllegalArgumentException("'Kapa Spike-in Plate Barcode' UDF is invalid: empty 'Kapa Batch Id' part in '$kapaPlateBarcode'.")
        BigDecimal kapaTagMassNg
        try {
            kapaTagMassNg = arr[2] as BigDecimal
        } catch (NumberFormatException) {
            throw new IllegalArgumentException("'Kapa Spike-in Plate Barcode' UDF is invalid: invalid 'Kapa Tag Mass (ng)' part in '$kapaPlateBarcode'.")
        }
        logger.info "$kapaLotId, $kapaBatchId, $kapaTagMassNg"
        AliquotCreationService aliquotCreationService = gov.doe.jgi.pi.pps.util.util.BeanUtil.getBean(AliquotCreationService.class)
        Map<String, KapaWellConfig> kapaPlateMap = aliquotCreationService.retrieveKapaPlateConfiguration(kapaLotId)
        if (!kapaPlateMap)
            throw new IllegalArgumentException("Kapa Plate configuration is not defined for Kapa Plate Barcode: '$kapaPlateBarcode'.")
        process.outputAnalytes.each { Analyte aliquot ->
            aliquot.artifactNodeInterface.with {
                setUdf(ClarityUdf.ANALYTE_KAPA_LOT_ID.udf, kapaLotId)
                setUdf(ClarityUdf.ANALYTE_KAPA_BATCH_ID.udf, kapaBatchId)
                String wellLocation = location
                assert wellLocation
                KapaWellConfig kapaWellConfig = kapaPlateMap.get(wellLocation)
                if (!kapaWellConfig)
                    throw new IllegalArgumentException("Kapa Plate configuration is not defined for Kapa Plate Barcode, well location: ['$kapaPlateBarcode', $wellLocation].")
                String kapaTagName = kapaWellConfig.tagNameLocation.split(':', 2)[0]
                setUdf(ClarityUdf.ANALYTE_KAPA_TAG_NAME.udf, kapaTagName)
                String kapaTagSequence = kapaWellConfig.tagSequence
                setUdf(ClarityUdf.ANALYTE_KAPA_TAG_SEQUENCE.udf, kapaTagSequence)
                setUdf(ClarityUdf.ANALYTE_KAPA_TAG_MASS_NG.udf, kapaTagMassNg)
            }
        }
    }

    void transferBeansDataToClaritySampleAliquots(List<ClaritySampleAliquot> outputAnalytes = process.outputAnalytes as List<ClaritySampleAliquot>) {
        ContainerNode outputPlateContainer = (process as SampleAliquotCreationProcess).outputPlateContainer

        if (outputPlateContainer) {
            AcKeyValuePlateResultsBean plateResultsBean = (process as SampleAliquotCreationProcess).getResultsBean()
            ClaritySampleAliquot claritySampleAliquot = (ClaritySampleAliquot) outputAnalytes[0]
            claritySampleAliquot.containerUdfLabResult = plateResultsBean.aliquotStatus.value
            outputPlateContainer.name = outputPlateContainer.id
            claritySampleAliquot.containerUdfFinalAliquotMassNg = plateResultsBean.targetAliquotMassNg
            claritySampleAliquot.containerUdfFinalAliquotVolumeUl = plateResultsBean.totalVolumeUl
        }
        outputAnalytes.each { ClaritySampleAliquot sampleAliquot ->
            SampleAnalyteTableBean bean = (process as SampleAliquotCreationProcess).getSampleAnalyteTableBean(sampleAliquot.id)

            sampleAliquot.setSystemQcFlag(bean.passed)
            sampleAliquot.udfLabResult = bean.aliquotStatus.value
            sampleAliquot.udfFailureMode = bean.failureMode?.value
            sampleAliquot.udfNotes = bean.smNotes
            sampleAliquot.udfVolumeUl = bean.finalAliquotVolumeUl
            sampleAliquot.udfVolumeUsedUl = bean.adjustedAliquotVolumeUl
            sampleAliquot.udfConcentrationNgUl = bean.passed ? (bean.finalAliquotMassNg / bean.finalAliquotVolumeUl) : 0

            if (!outputPlateContainer) {
                if (bean.destinationBarcode) {
                    sampleAliquot.containerNode.name = bean.destinationBarcode
                }
                sampleAliquot.containerUdfFinalAliquotMassNg = bean.finalAliquotMassNg
                sampleAliquot.containerUdfFinalAliquotVolumeUl = bean.finalAliquotVolumeUl
                sampleAliquot.containerUdfLabResult = bean.aliquotStatus.value
            }
        }
    }

}