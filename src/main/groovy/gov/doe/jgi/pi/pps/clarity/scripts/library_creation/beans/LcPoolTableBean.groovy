package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryPoolFailureModesService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LcPoolTableBean {
	@FieldMapping(header='Library Pool Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolConcentration
	@FieldMapping(header='Pool Number', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolNumber
	@FieldMapping(header='Avg Library Pool Template Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolTemplateSize
	@FieldMapping(header='Library Pool Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolVolume
	@FieldMapping(header='Library Pool Molarity (pM)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal poolMolarity

    //Please Select,Pass,Fail
	@FieldMapping(header='PASS or FAIL?', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList poolResult
	//Sample Problem,Instrument Error,Operator Error,Informatics Error,Facility Problem,Protocol Deviation,Reagent Consumable Issue
	@FieldMapping(header='Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList failureMode

    LibraryPoolFailureModesService libraryPoolFailureModesService
	Boolean passed

	def validate() {
        def errorMsg = StringBuilder.newInstance()
        if (!poolNumber) {
           return null
        }
        String labResult = poolResult?.value
        if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
            errorMsg << "invalid PASS or FAIL? '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
            return buildErrorMessage(errorMsg)
        }
        passed = labResult == Analyte.PASS
        if (!passed && !failureMode?.value) {
            errorMsg << "unspecified Failure Mode $ClarityProcess.WINDOWS_NEWLINE"
            return buildErrorMessage(errorMsg)
        }
        if (!passed)
            return null
        //validate passed pools
        if (failureMode?.value) {
            errorMsg << "incorrect failure mode '$failureMode.value' or status '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!poolVolume) {
            errorMsg << "unspecified Library Pool Volume (ul) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!poolConcentration) {
            errorMsg << "unspecified Library Pool Concentration (ng/ul) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!poolTemplateSize) {
            errorMsg << "unspecified Avg Library Pool Template Size (bp) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!poolMolarity) {
            errorMsg << "unspecified Library Pool Molarity (pM) $ClarityProcess.WINDOWS_NEWLINE"
        }
        return buildErrorMessage(errorMsg)
    }

    def buildErrorMessage(def errorMsg) {
        def startErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Pool Info Section $ClarityProcess.WINDOWS_NEWLINE"
        startErrorMsg << "Library Pool Number($poolNumber):$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

    void populateRequiredFields(ClarityProcess process, int poolPassCount){
        if(!libraryPoolFailureModesService){
            libraryPoolFailureModesService = BeanUtil.getBean(LibraryPoolFailureModesService.class)
        }
        poolResult = libraryPoolFailureModesService.dropDownPassFail
        failureMode = libraryPoolFailureModesService.dropDownFailureModes
        if(poolPassCount > 0)
            poolResult.value = Analyte.PASS
        else{
            poolResult.value = Analyte.FAIL
            failureMode.value = failureMode.controlledVocabulary[0]
        }
        poolVolume = 10
        poolConcentration = 10
        poolTemplateSize = 10
        poolMolarity = 1
    }

    boolean getRequiresIndex(){
        return false
    }

    void setIndexName(String indexName){
        return
    }
}
