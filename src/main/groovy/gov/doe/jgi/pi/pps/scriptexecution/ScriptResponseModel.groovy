package gov.doe.jgi.pi.pps.scriptexecution


import org.springframework.stereotype.Component

@Component
class ScriptResponseModel {
    ScriptResponseData responseData
    Boolean lastAction

}
