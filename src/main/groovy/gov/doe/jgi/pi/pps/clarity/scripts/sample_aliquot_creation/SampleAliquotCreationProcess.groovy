package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.AcKeyValuePlateResultsBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.aliquoting.SampleAnalyteTableBean
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleAliquotFailureModesService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

class SampleAliquotCreationProcess extends ClarityProcess {
    static final Logger logger = LoggerFactory.getLogger(SampleAliquotCreationProcess.class)
    static ProcessType processType = ProcessType.AC_ALIQUOT_CREATION

	static final String SCRIPT_GENERATED_ALIQUOT_SHEET = 'Download Aliquot Creation Worksheet'
	static final String UPLOADED_ALIQUOT_SHEET = 'Upload Aliquot Creation Worksheet'
    static final String TEMPLATE_NAME = 'resources/templates/excel/SampleAliquotStarlet'
    String excelFileName = UPLOADED_ALIQUOT_SHEET

    boolean testMode = false
    FreezerService freezerService
    SampleAliquotFailureModesService sampleAliquotFailureModesService
    ExcelWorkbook excelWorkbookCached
    List sampleAnalyteBeansCached
    AcKeyValuePlateResultsBean resultsBeanCached
    LibraryCreationQueueCv plateLcQueueCached

    SampleAliquotCreationProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation':AcPreProcessValidation,
                'PrepareAliquotCreationSheet':AcPrepareAliquotCreationSheet,
                'ProcessAliquotCreationSheet':AcProcessAliquotCreationSheet,
                'Print Labels':AcPrintLabels,
                'RouteToNextWorkflow':AcRouteToWorkflow
        ]


    }

    ContainerNode getOutputPlateContainer() {
        ContainerNode outputContainerNode = outputAnalytes[0].containerNode
        if (outputContainerNode?.isNinetySixWellPlate)
            return outputContainerNode
        return null
    }

    List<SampleAnalyteTableBean> getSampleAnalyteBeans() {
        if (sampleAnalyteBeansCached) {
            return sampleAnalyteBeansCached
        }
        List<SampleAnalyteTableBean> beansList = (List<SampleAnalyteTableBean>) getBeanList(SampleAnalyteTableBean.class.simpleName)
        beansList.each {
            def errorMsg = it.validate()
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        ContainerNode outputContainerNode = outputAnalytes[0].containerNode
        if (outputContainerNode.isNinetySixWellPlate) {
            validateDataTab(beansList, outputContainerNode)
        }
        sampleAnalyteBeansCached = beansList
        return sampleAnalyteBeansCached
    }

    static void validateDataTab(List<SampleAnalyteTableBean> beansList, ContainerNode outputContainerNode) {
        def passBarcodes = [] as Set
        beansList.each { SampleAnalyteTableBean bean ->
            if (bean.passed) {
                passBarcodes << bean.destinationBarcode
            }
        }
        //all passed aliquots should have the same destination barcode
        if (passBarcodes && passBarcodes.size() != 1) {
            postErrorMessage("""
                    Please correct the errors below and upload spreadsheet again.
                    Data Tab: more than one destination barcode found for passed aliquots on a plate
                    """)
        }
        def barcode = passBarcodes.find{true} as String
        if (barcode && !barcode.equalsIgnoreCase(outputContainerNode.id)) {
            postErrorMessage("""
                    Please correct the errors below and upload spreadsheet again.
                    Data Tab: '$barcode' destination barcode is not equal to container LIMS ID '$outputContainerNode.id'
                    """)
        }
    }

    LibraryCreationQueueCv getPlateLcQueue() {
        if (!outputPlateContainer) {
            return null
        }
        if (plateLcQueueCached) {
            return plateLcQueueCached
        }
        plateLcQueueCached = outputAnalytes[0].libraryCreationQueue
        plateLcQueueCached
    }

    ExcelWorkbook getExcelWorkbook() {
        if (excelWorkbookCached) {
            return excelWorkbookCached
        }
        def fileNode = getFileNode(excelFileName)
        excelWorkbookCached = new ExcelWorkbook(fileNode.id)
        excelWorkbookCached.load()
        excelWorkbookCached
    }

    def getBeanList(String beanName) {
        def beansList = getExcelWorkbook().sections.values()?.find {
            it.beanClass.simpleName == beanName
        }?.getData()
        beansList
    }

    SampleAnalyteTableBean getSampleAnalyteTableBean(limsId) {
        def tableBean = sampleAnalyteBeans.find { it.aliquotLimsId == limsId }
        if (!tableBean) {
            throw new WebException([code:'SampleAliquotCreationProcess.SampleAnalyteTableBean.missing', args:[limsId]], 422)
        }
        tableBean
    }

    AcKeyValuePlateResultsBean getResultsBean() {
        if (resultsBeanCached) {
            return resultsBeanCached
        }
        AcKeyValuePlateResultsBean bean = (AcKeyValuePlateResultsBean) getBeanList(AcKeyValuePlateResultsBean.class.simpleName)
        def errorMsg = bean.validate()
        if(errorMsg)
            postErrorMessage(errorMsg as String)
        resultsBeanCached = bean
        return resultsBeanCached
    }

    List<ContainerNode> getOutputPlateContainers() {
        List<ContainerNode> containerNodes = outputAnalytes.collect{it.containerNode}.unique()
        return containerNodes.findAll { it.isNinetySixWellPlate }
    }

    String getSampleTubePlateLabel() {
        def sampleTubePlateLabel
        //only one output plate is expected
        List<ContainerNode> inputPlates = inputAnalytes.collect { it.containerNode }.unique()?.findAll{ it.isNinetySixWellPlate }
        def inputTubes = inputAnalytes.collect { it.containerNode }.unique()?.find{ ContainerTypes.TUBE.value == it.containerType }
        if (inputPlates?.size() == 1 && !inputTubes) {
            sampleTubePlateLabel = inputPlates[0].getUdfAsString(ClarityUdf.CONTAINER_LABEL.udf)
        } else {
            //The re-array aliquot plate "plate map": <YY-Mmm-DD>_RA_<counter>
            sampleTubePlateLabel = buildSampleTubePlateLabel()
        }
        logger.info "${outputPlateContainer}: setting '${ClarityUdf.CONTAINER_LABEL.value}' udf to '${sampleTubePlateLabel}'"
        sampleTubePlateLabel
    }

    static String buildSampleTubePlateLabel(int maxCounter = 1000, Date processDate = new Date()) {
        ArtifactNodeService artifactNodeService = BeanUtil.getBean(ArtifactNodeService.class)
        def counter = (1..maxCounter).find {
            List<String> containerLimsIds = artifactNodeService.collectPlateContainerLimsIds(it, processDate)
            if (!containerLimsIds)
                return it
            return null
        }
        if (!counter) {
            String date
            try {
                date = new SimpleDateFormat('yy-MMM-dd').format(processDate)
            } catch (Throwable t) {
            }
            postErrorMessage("""Cannot find an empty re-array label: 
process date ${date}, 
max counter $maxCounter
""")
        }
        //The re-array aliquot plate "plate map": <YY-Mmm-DD>_RA_<counter>
        String formatDate = new SimpleDateFormat('yy-MMM-dd').format(processDate)
        return "${formatDate}_RA_$counter"
    }

    //batch lookup for efficiency PPS-4781
    @Override
    List<SampleNode> getBatchPmoSampleNodes() {
        List<ScheduledSample> scheduledSamples = inputAnalytes*.claritySample as List<ScheduledSample>
        return nodeManager.getSampleNodes(scheduledSamples*.pmoSampleLimsId)
    }
    //batch lookup for efficiency PPS-4781
    @Override
    List<ArtifactNode> getBatchPmoSampleArtifactNodes() {
        return nodeManager.getArtifactNodes(batchPmoSampleNodes*.artifactId)
    }
    //batch lookup for efficiency PPS-4781
    @Override
    List<ContainerNode> getBatchPmoSampleContainerNodes() { //batch lookup for efficiency PPS-4781
        return nodeManager.getContainerNodes(batchPmoSampleArtifactNodes*.containerId)
    }

    def getFreezerContainers(){
        if(!freezerService) {
            freezerService = BeanUtil.getBean(FreezerService.class)
        }
        return freezerService.freezerLookupInputAnalytes(this)
    }

    def getStatusCv() {
        if(!sampleAliquotFailureModesService) {
            sampleAliquotFailureModesService = BeanUtil.getBean(SampleAliquotFailureModesService.class)
        }
        return sampleAliquotFailureModesService.dropDownPassFail
    }

    def getFailureModeCv() {
        if(!sampleAliquotFailureModesService) {
            sampleAliquotFailureModesService = BeanUtil.getBean(SampleAliquotFailureModesService.class)
        }
        return sampleAliquotFailureModesService.dropDownFailureModes
    }
}
