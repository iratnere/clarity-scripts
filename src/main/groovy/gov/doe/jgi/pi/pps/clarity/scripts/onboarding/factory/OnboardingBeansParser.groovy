package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemPurposeCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingDetailsBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.Migrator
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.json.JsonBuilder
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 1/30/17.
 */
class OnboardingBeansParser {
    static final logger = LoggerFactory.getLogger(OnboardingBeansParser.class)

    final static int ONBOARDING_SOW_ITEM_NUMBER = 329
    final static String ONBOARDING_SOW_RUN_MODE = 'Illumina X10 2 X 150'
    final static String LIB_CREATION_SITE_NOT_JGI = 'Not JGI'
    final static Integer LIB_CREATION_SPECS_NOT_JGI = 72
    static final String ONBOARDING_DETAILS='Onboarding Details'
    ScheduledSampleService scheduledSampleService
    Map sampleSowItemMap = [:].withDefault {[]}

    List<String> validate(List<OnboardingBean> onboardingBeans){
        List<String> errors = []
        Map<String,Set<String>> libName2SampleName = [:].withDefault {[] as Set}
        Map<ContainerNode, Set<ClaritySample>> containerSamples = [:].withDefault {[] as Set}
        List<Long> sampleIds = []
        onboardingBeans.each{
            libName2SampleName[it.getLabLibraryName()] << it.sampleName
            containerSamples[it.pmoSample.sampleAnalyte.containerNode] << it.pmoSample
            if (it.checkStatus)
                sampleIds << it.pmoSample.pmoSampleId
        }
        if(sampleIds){
            StatusService statusService = BeanUtil.getBean(StatusService.class)
            def sampleStatus = statusService.getSampleStatusBatch(sampleIds)
            sampleStatus.each{
                if(it.value != SampleStatusCv.AWAITING_SHIPPING_APPROVAL.value)
                    errors << "Expecting sample status ${SampleStatusCv.AWAITING_SHIPPING_APPROVAL.value}. Sample ${it.key} has status ${it.value}"
            }
        }
        libName2SampleName?.each{String libName, Set<String> sampleNames ->
            if(sampleNames.size() > 1)
                errors << "Library name $libName is used for more than 1 samples $sampleNames."
        }
        containerSamples?.each{ ContainerNode containerNode, Set<ClaritySample> samples ->
            if(containerNode.contentsArtifactNodes.size() != samples.size())
                errors << "While onboarding sample plates either ALL or NONE of the content samples are on boarded. Please include all the samples on the container ${containerNode.id}."
        }
        return errors
    }

    void prepareBeansForOnboarding(List<OnboardingBean> onboardingBeans){
        logger.info "Preparing beans for onboarding....."
        scheduledSampleService = BeanUtil.getBean(ScheduledSampleService.class)
        Long submittedBy = onboardingBeans.first().contactId
        Map sampleBeans = onboardingBeans.groupBy{it.pmoSampleId}
        def sampleSows = scheduledSampleService.getBatchSowItemMetadataForSample(sampleBeans.keySet() as List)
        Set<Long> sowIds = [] as Set
        sampleBeans.each{Long pmoSampleId, List<OnboardingBean> beans ->
            logger.info "Processing sample $pmoSampleId...."
            List<Long> sampleSowIds = []
            def mergedBeans = beans.findAll{it.isMergedBean()}
            def mergedSows = mergedBeans.collect{it.getSowId()}
            sampleSows[pmoSampleId].each{
                if(it.'sow-item-type' == SowItemTypeCv.ONBOARDING.value && !mergedSows.contains(it.'sow-item-id'))
                    sampleSowIds << it.'sow-item-id'
            }
            assignSowsToSampleBeans(beans-mergedBeans, sampleSowIds, submittedBy)
            sowIds.addAll(beans.collect{it.getSowId()})
        }
        updateSowStatus(sowIds, submittedBy)
        logger.info "Beans ready for onboarding. "
    }

    void assignSowsToSampleBeans(List<OnboardingBean> sampleBeans, List<Long> sowIds, Long submittedBy){
        if(!sampleBeans)
            return
        def libBeans = sampleBeans.groupBy{it.getLabLibraryName()}
        NodeManager clarityNodeManager = BeanUtil.nodeManager
        List<Analyte> available = []
        List<ScheduledSample> onboardingSS = sampleBeans[0].pmoSample.getScheduledSamples(clarityNodeManager)?.findAll{it.udfSowItemType == SowItemTypeCv.ONBOARDING.value}
        Map<String, Analyte> ssDescendants = lookupLastDescendant(clarityNodeManager, onboardingSS?.collect{it.sampleAnalyte.id}, ClarityLibraryStock.class)
        onboardingSS?.each{ ScheduledSample ss ->
            Analyte descendant = ssDescendants[ss.sampleAnalyte.id]
            if(!descendant)
                descendant = ss.sampleAnalyte
            List<OnboardingBean> ssBeans = sampleBeans.findAll{it.getSowId() == ss.sowItemId}
            if(!ssBeans){
                available << descendant
            }
            else {
                ssBeans.each { it.addAnalyte(descendant) }
            }
            sowIds.remove(sowIds.indexOf(ss.sowItemId as Integer))
        }

        libBeans?.each { String libName, List<OnboardingBean> beans ->
            if (!beans[0].getSowId()){
                int availIndex = 0
                while (available && availIndex < available.size()) {
                    Analyte avaiAnalyte = available[availIndex++]
                    if (beans[0].isCompatible(avaiAnalyte)) {
                        logger.info "Using $avaiAnalyte for $libName"
                        beans.each { it.addAnalyte(avaiAnalyte) }
                        available.remove(avaiAnalyte)
                        break
                    }
                }
            }
            if(!beans[0].getSowId() && sowIds){
                beans?.each{
                    it.sowId = sowIds[0]
                }
                logger.info "Using SOW Item ${sowIds[0]} for $libName"
                sowIds.remove(0)
            }
            if(!beans[0].getSowId()) {
                def sampleId = beans[0].pmoSampleId as Integer
                def sowId = sampleSowItemMap[sampleId][0]
                if(!sowId) {
                    addSowItem(beans[0], submittedBy)
                    sowId = sampleSowItemMap[sampleId][0]
                }
                beans?.each{it.sowId = sowId}
                logger.info "Using SOW Item ${sowId} for $libName"
                sampleSowItemMap[sampleId].remove(0)
            }
        }
    }

    Map<String, Analyte> lookupLastDescendant(NodeManager clarityNodeManager, List<String> ssArtifactIds, Class descendantType){
        if(!ssArtifactIds)
            return null
        Map<String,List<String>> sowDescendantIds = scheduledSampleService.getAllDescendantIds(ssArtifactIds)
        Map<String,List<Analyte>> sowDescendants = [:]
        Map<String, ArtifactNode> artifactNodes = clarityNodeManager.getArtifactNodesMap(sowDescendantIds.values().flatten())
        sowDescendantIds.each{String ssAnalyteId, List<String> desIds ->
            int i = 0
            while(true){
                Analyte analyte = AnalyteFactory.analyteInstance(artifactNodes[desIds[i++]])
                if(!analyte)
                    break
                sowDescendants[ssAnalyteId] = analyte
                if(analyte.class == descendantType)
                    break
            }
        }
        return sowDescendants
    }

    def addSowItem(OnboardingBean bean, Long submittedBy){
        if(!sampleSowItemMap[bean.pmoSampleId as int]) {
            def jsonRequest = new JsonBuilder()
            if(bean.libraryCreationSite == LIB_CREATION_SITE_NOT_JGI) {
                jsonRequest {
                    'created-by-cid'(submittedBy)
                    'sequencing-project-id'(bean.jgiProjectId)
                    'sow-number'(ONBOARDING_SOW_ITEM_NUMBER)
                    'sample-id'(bean.pmoSampleId)
                    'sow-item-purpose'(SowItemPurposeCv.ANTICIPATED_PLANNED_WORK.value)
                    'run-mode'(bean.sowRunMode ? bean.sowRunMode : ONBOARDING_SOW_RUN_MODE)
                    if(bean.dop)
                        'degree-of-pooling'(bean.dop)
                    'library-creation-specs-id'(LIB_CREATION_SPECS_NOT_JGI)
                }
            }
            else{
                jsonRequest {
                    'created-by-cid'(submittedBy)
                    'sequencing-project-id'(bean.jgiProjectId)
                    'sow-number'(ONBOARDING_SOW_ITEM_NUMBER)
                    'sample-id'(bean.pmoSampleId)
                    'sow-item-purpose'(SowItemPurposeCv.ANTICIPATED_PLANNED_WORK.value)
                    if(bean.dop)
                        'degree-of-pooling'(bean.dop)
                    'run-mode'(bean.sowRunMode ? bean.sowRunMode : ONBOARDING_SOW_RUN_MODE)
                }
            }
            scheduledSampleService.addSowItem(jsonRequest.toPrettyString())?.each{int sampleId, int sowId ->
                sampleSowItemMap[sampleId] << sowId
            }
        }
        return sampleSowItemMap[bean.pmoSampleId as Integer][0]
    }

    Section prepareOnboardingDetails(List<OnboardingBean> onboardingBeans, int sheetIndex){
        Section detailsSection = new TableSection(sheetIndex,OnboardingDetailsBean.class, 'A2')
        List<OnboardingDetailsBean> detailBeans = []
        onboardingBeans?.each{ OnboardingBean bean ->
            bean.analytes.each{ Analyte analyte ->
                if(analyte.class.name == analyte.parentAnalyte?.class?.name)
                    detailBeans << prepareDetailsBean(analyte.parentAnalyte)
                detailBeans << prepareDetailsBean(analyte)
            }
        }
        detailsSection.setData(detailBeans)
        return detailsSection
    }

    int getOnboardingDetailsSheetIndex(ExcelWorkbook onboardingWorkbook) {
        int sheetIndex = onboardingWorkbook.getSheetIndex(ONBOARDING_DETAILS)
        if(sheetIndex < 0) {
            onboardingWorkbook.workbook.createSheet(ONBOARDING_DETAILS)
            sheetIndex = onboardingWorkbook.getSheetIndex(ONBOARDING_DETAILS)
            OneDSection tableNameSection = new OneDSection(sheetIndex, CellTypeEnum.STRING, 'A1', 'A1')
            tableNameSection.setData(['Onboarded Analytes'])
            onboardingWorkbook.addSection(tableNameSection)
            OneDSection tableHeaderSection = new OneDSection(sheetIndex, CellTypeEnum.STRING, 'A2', 'C2')
            tableHeaderSection.setData(['Process Lims Id','Process Type','Analyte Lims Id'])
            onboardingWorkbook.addSection(tableHeaderSection)
        }
        return sheetIndex
    }

    OnboardingDetailsBean prepareDetailsBean(Analyte analyte){
        OnboardingDetailsBean detailsBean = new OnboardingDetailsBean()
        if(analyte.artifactNode.parentProcessId){
            detailsBean.processLimsId = analyte.artifactNode.parentProcessId
            detailsBean.processType = analyte.parentProcessType.value
        }
        detailsBean.analyteLimsId = analyte.id
        return detailsBean
    }

    void updateSowStatus(Set<Long> sowIds, Long contactId){
        if(!sowIds)
            return
        logger.info "Changing SOW Item status of SOW Items $sowIds to ${SowItemStatusCv.AWAITING_SAMPLE_QC_REVIEW.value} "
        Map<Long, SowItemStatusCv> sampleStatusCvMap = [:]
        sowIds.each{sampleStatusCvMap[it] = SowItemStatusCv.AWAITING_SAMPLE_QC_REVIEW}
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        statusService?.submitSowItemStatus(sampleStatusCvMap,contactId)
    }

    List<String> lookUpLibraries(List<OnboardingBean> beans, NodeManager nodeManager) {
        List<String> errors = []
        Map<String, List<OnboardingBean>> beansByLibraryName = beans.groupBy { it.getLabLibraryName()}
        Map<String, Analyte> libraries = Migrator.lookupLibrariesByCollaboratorName(nodeManager, beansByLibraryName.values().collect{it.first()})
        if(libraries) {
            beansByLibraryName.each { String collabLibName, List<OnboardingBean> lbeans ->
                Analyte libraryStock = libraries[collabLibName]
                if(libraryStock)
                    lbeans.each {
                        String error = processLibrary(it, libraryStock)
                        if(error)
                            errors << error
                    }
            }
        }
        return errors
    }

    List<String> lookUpPools(List<OnboardingBean> beans, NodeManager nodeManager) {
        List<String> errors = []
        Map<String, List<OnboardingBean>> beansByPoolName = beans.groupBy {it.collaboratorPoolName}
        Map<String, Analyte> libraries = Migrator.lookupPoolsByCollaboratorName(nodeManager, beansByPoolName.values().collect{it.first()})
        if(libraries) {
            beansByPoolName.each { String collabLibName, List<OnboardingBean> lbeans ->
                Analyte pool = libraries[collabLibName]
                if(pool)
                    lbeans.each {
                        String error = processLibrary(it, pool)
                        if(error)
                            errors << error
                    }
            }
        }
        return errors
    }

    String processLibrary(OnboardingBean bean, Analyte library) {
        List<PmoSample> pmoSamples = library.claritySamples.collect{it.pmoSample}
        List<String> collaboratorSampleNames = pmoSamples.collect { it.udfCollaboratorSampleName}
        String analyteCollabName = (library instanceof ClarityLibraryStock)?bean.getLabLibraryName():bean.collaboratorPoolName
        if (!collaboratorSampleNames.contains(bean.sampleName))
            return "Analyte with Collaborator name $analyteCollabName already exists in clarity. Check $bean"
        bean.addAnalyte(library)
        return ""
    }

    List<String> lookUpSamples(List<OnboardingBean> beans, NodeManager nodeManager) {
        List<String> errors = []
        Map<String, List<OnboardingBean>> beansBySampleName = beans.groupBy {it.sampleName}
        Map<String, ClaritySample> samples = Migrator.lookupPmoSamplesByCollaboratorName(nodeManager, beansBySampleName.keySet())
        if(samples) {
            beansBySampleName.each { String sampleName, List<OnboardingBean> sbeans ->
                ClaritySample sample = samples[sampleName]
                if(!sample)
                    errors << "Sample ${sampleName} does not exist in clarity"
                else
                    sbeans.each { processSample(it, sample) }
            }
        }
        return errors
    }

    void processSample(OnboardingBean bean, ClaritySample sample) {
        PmoSample pmoSample = sample.pmoSample
        if(pmoSample.udfExternal != OnboardingAdapter.EXTERNAL_UDF_VALUE)
            bean.checkStatus = true
        bean.addAnalyte(sample?.sampleAnalyte)
        bean.pmoSample?.udfExternal = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.isExternal = true
    }
}
