package gov.doe.jgi.pi.pps.clarity.welladdress

import org.apache.commons.lang.NotImplementedException

/**
 * Created by dscott on 6/20/2014.
 */
class WellAddress96Iterator implements Iterator<gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96> {

    boolean skipCorners = false
    boolean columnsFirst = false

    private Integer colIndex = 0
    private Integer rowIndex = 0
    private gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96 nextAddress
    private boolean initialized = false

    private void initialize() {
        if (!initialized) {
            if (skipCorners) {
                setNextAddress()
            } else {
                nextAddress = new gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96(colIndex: colIndex, rowIndex: rowIndex)
            }
            initialized = true
        }
    }

    private void setNextAddress() {
        nextAddress = null
        while (nextAddress == null && increment()) {
            if (!skipCorners || !onCorner()) {
                nextAddress = new gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96(colIndex:colIndex,rowIndex:rowIndex)
            }
        }
    }

    private boolean increment() {
        if (columnsFirst) {
            rowIndex++
            if (rowIndex > gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxRowIndex) {
                rowIndex = 0
                colIndex++
            }
            return colIndex <= gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxColIndex
        } else {
            colIndex++
            if (colIndex > gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxColIndex) {
                colIndex = 0
                rowIndex++
            }
            return rowIndex <= gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxRowIndex
        }
    }

    private boolean onCorner() {
        return  (gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.minRowIndex == rowIndex || gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxRowIndex == rowIndex) &&
                (gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.minColIndex == colIndex || gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96.maxColIndex == colIndex)
    }

    synchronized boolean hasNext() {
        initialize()
        return nextAddress != null
    }

    synchronized gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96 next() {
        initialize()
        gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96 address = nextAddress
        if (address != null) {
            setNextAddress()
        }
        return address
    }

    void remove() {
        throw new NotImplementedException()
    }

}
