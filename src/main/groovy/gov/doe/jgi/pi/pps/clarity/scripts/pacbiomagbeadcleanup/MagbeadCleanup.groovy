package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by datjandra on 10/30/2015.
 */
class MagbeadCleanup extends ClarityProcess {

    static ProcessType processType = ProcessType.PACBIO_MAGBEAD_CLEANUP

    final static String UPLOAD_MAGBEAD_CLEANUP = 'Upload MagBeadCleanup.xls'
    final static String DOWNLOAD_MAGBEAD_CLEANUP = 'Download MagBeadCleanup.xls'
    final static String MAGBEAD_TEMPLATE = 'resources/templates/excel/MagBeadCleanup'
    boolean testMode = false

    MagbeadCleanup(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':McPreProcessValidation,
            'PrepareMagbeadCleanupSheet':McPrepareMagbeadCleanupSheet,
            'ProcessMagbeadCleanupSheet':McProcessMagbeadCleanupSheet,
            'Route to Next Workflow':McRouteToWorkflow
        ]
    }
}
