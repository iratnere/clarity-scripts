package gov.doe.jgi.pi.pps.clarity.scripts.abandon.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

class AbandonWorkEmailNotification extends EmailNotification{
    static final String SUBJECT = 'has been abandoned from Seq Project'
    static final String CC_EMAIL_GROUP = 'jgi-its-abandon-segment@lbl.gov' //PPS-4849 - update mailing lists
    static final String TEMPLATE_FILE_NAME = 'abandon_segment'

    @Override
    String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return "Sow Item ${emailDetailsList.first().sowItemId} ${SUBJECT} ${emailDetailsList.first().sequencingProjectName}"
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new AbandonWorkEmailDetails(it)}
    }

    @Override
    def getToList(List<EmailDetails> emailDetailsList){
        def to = emailDetailsList.first().sequencingProjectManagerId
        return to
    }

    @Override
    def getBinding(List<EmailDetails> emailDetailsList) {
        return [contact: emailDetailsList.first().sampleContactName, samples: getSampleList(emailDetailsList), abandonDate: new Date()]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}