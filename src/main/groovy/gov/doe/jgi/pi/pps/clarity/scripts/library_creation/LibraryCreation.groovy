package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.PlacementsNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 4/1/15.
 */
abstract class LibraryCreation {

    static final Logger logger = LoggerFactory.getLogger(LibraryCreation.class)

    private final static Stage DEFAULT_STAGE = Stage.LIBRARY_QPCR
    LibraryCreationProcess process

    OutputType output

    void validateOutputContainers() {
        output.validate()
    }
    void updateOutputContainers() {
        output.update()
    }
    void createPrintLabelsFile() {
        output.print()
    }
    void removeCornerBeans(List beansList) {
        output.removeCorners(beansList)
    }

    abstract ExcelWorkbook populateLibraryCreationSheet()
    abstract Section getTableSectionLibraries()
    abstract void updateLibraryStockUdfs()
    abstract void moveToNextWorkflow()

    void  updateInputsAndSamples() {
        process.inputAnalytes.each { it.udfVolumeUl = BigDecimal.ZERO }
        process.updateScheduledSampleNotes()
    }

    void  updateLcaUdfs() {
        process.outputAnalytes.each{ output ->
            (output as ClarityLibraryStock).updateLibraryCreationLcaUdf()
        }
    }
    void updateLibraryPoolUdfs() {}
    void createPoolPrintLabelsFile() {}

    Stage getDefaultStage() { DEFAULT_STAGE }
    abstract String getLcTableBeanClassName()
    abstract String getTemplate()

    void automaticPlacement() {
        PlacementsNode placementsNode = process.processNode.placementsNode
        String containerId = placementsNode.selectedContainerIds[0] //first available output plate container
        process.validateOutputContainerType(
                [BeanUtil.nodeManager.getContainerNode(containerId)]
        )
        OutputPlacements outputPlacements = new OutputPlacements()
        //For destination container, iterate through its corresponding rows and add their placement to the placement node
        process.outputAnalytes.each { Analyte outputAnalyte ->
            Analyte inputAnalyte = outputAnalyte.parentAnalyte
            String analytePlacement = inputAnalyte.containerLocation.rowColumn
            ContainerLocation containerLocation = new ContainerLocation(containerId, analytePlacement)
            outputPlacements << new OutputPlacement(artifactId: outputAnalyte.id, containerLocation: containerLocation)
        }
        placementsNode.setOutputPlacements(outputPlacements).httpPost()
        logger.info("""
Automatic Placement Script has completed successfully.
${process.outputAnalytes?.size()} samples have been transferred into ${containerId} container.
        """)
    }

    void validateInputAnalyteClass(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { analyte ->
            if (!(analyte instanceof ClaritySampleAliquot)) {
                process.postErrorMessage("""
Input Analyte '$analyte.id' has an invalid analyte class: '${analyte.class?.simpleName}'. 
Expected class: 'ClaritySampleAliquot'.
                """)
            }
        }
    }

    void validateInputsContainerType(List<ContainerNode> containerNodes = process.getContainerNodes(process.inputAnalytes)) {
        Set<ContainerTypes> containerTypes = containerNodes.collect {it.containerTypeEnum}.unique()
        if (containerTypes.size() != 1) {
            process.postErrorMessage("""
Inputs can only be in containers of the same type.
Please select only one ${ContainerTypes.WELL_PLATE_96.value} or ${ContainerTypes.TUBE.value}s as containers for inputs.
            """)
        }
        ContainerTypes containerType = containerTypes.find{ true }
        if ( containerType != ContainerTypes.TUBE && containerNodes.size() != 1) {
            process.postErrorMessage("""
Process can only run on a single ${ContainerTypes.WELL_PLATE_96.value}.
Please select a single ${ContainerTypes.WELL_PLATE_96.value} as the input.
            """)
        }
    }

    void validateBatch() {}
    void uploadExtraFiles() {}

    int getTemplateIndex() {
        return 0
    }

    void validateProcessUdfs() {}
}