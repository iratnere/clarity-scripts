package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.SampleQcProcess
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel.GLSStartletUseTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import net.sf.json.JSONArray
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

abstract class SampleQcAdapter {
    static final Logger logger = LoggerFactory.getLogger(SampleQcAdapter.class)
    static final int MAX_INPUT_SIZE = 384-16
    final static List<String> SOW_ITEM_STATUS = [SowItemStatusCv.ABANDONED.value, SowItemStatusCv.COMPLETE.value, SowItemStatusCv.DELETED.value,
                                                 SowItemStatusCv.IN_PROGRESS.value, SowItemStatusCv.AWAITING_QA_QC_ANALYSIS.value, SowItemStatusCv.AWAITING_SAMPLE_QC_REVIEW.value]
    List<String> errors = []
    List<Analyte> failedTubeSamples = null
    SampleQcUtility sampleQcUtility
    List<Analyte> analytes

    Map<Long, JSONArray> getSampleSowItems(){
        return sampleQcUtility.sampleSowItems
    }

    ScheduledSampleService getScheduledSampleService(){
        sampleQcUtility.scheduledSampleService
    }

    SampleQcAdapter(SampleQcUtility sampleQcUtility, List<Analyte> analytes) {
        this.analytes = analytes
        this.sampleQcUtility = sampleQcUtility
    }

    protected String checkInputSize(List<Analyte> inputAnalytes) {
        if(inputAnalytes.size() > MAX_INPUT_SIZE) {
            errors << "Too many input samples. Expecting $MAX_INPUT_SIZE or less Sample(s)."
            return errors.last()
        }
        return ""
    }

    protected List<String> validateSampleStatus() {
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        return checkSampleStatus(statusService.getSampleStatusBatch(analytes.collect{it.claritySample.pmoSample.pmoSampleId}))
    }

    protected List<String> checkSampleStatus(Map statusMap) {
        List<String> err = []
        statusMap.each {
            def sampleStatus = it.value
            if (sampleStatus != SampleStatusCv.AWAITING_SAMPLE_QC.value)
                err << "Invalid input ${it.key} with current status ${sampleStatus}. Expected status:  ${SampleStatusCv.AWAITING_SAMPLE_QC.value}."
        }
        errors.addAll(err)
        return err
    }

    protected String checkIsSample(Analyte analyte) {
        if(analyte instanceof SampleAnalyte && analyte.claritySample instanceof PmoSample)
            return ""
        errors << "Invalid input $analyte. Expecting Root samples as input."
        return errors.last()
    }

    void transferDataFromKeyValueSection(List<KeyValueSection> sections){
        sections.each { Section section ->
            section.data?.updateContainerUdfs()
        }
    }

    void transferDataFromTableToUdfs(TableSection section){
        section.data.each{ GLSStartletUseTableBean bean ->
            String currentQcType = bean.reqdQCType.value
            def qcTypeId = QcTypeCv.findByQcType(currentQcType)?.id
            //Cumulative QC Type Id = cumulative-qc-type-id =cumulative-qc-type-id  OR current-qc-type-id where current-qc-type-id is the corresponding id from uss.dt_sample.sample_qc_type_cv based on “Current QC Type” udf
            if(qcTypeId)
                bean.sample?.udfCumulativeQcTypeId = (bean.sample?.udfCumulativeQcTypeId? bean.sample?.udfCumulativeQcTypeId : 0) | qcTypeId
            //Sample QC Date = current date
            bean.sample?.udfSampleQcDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date())
            bean?.updateSampleUdfs(BeanUtil.nodeManager)
        }
    }

    List<Analyte> getFailedTubes(RecordDetailsInfo recordDetailsInfo){
        if(failedTubeSamples == null)
            processRecordDetails(recordDetailsInfo)
        return failedTubeSamples
    }

    List<ArtifactNode> createScheduledSamples(Map<ClaritySample, List<String>> sampleSowItemIds, Long researcherContactId){
        if(!sampleSowItemIds)
            return
        //create scheduled samples for each sample PLUSS sow items
        //set scheduled sample udfs to the uss.dt_sow_item ones
        logger.info "Creating Scheduled sample(s) for sow-items ${sampleSowItemIds.values().flatten()}"
        Map<String,String> sowItemScheduledSampleIdMap = scheduledSampleService?.createScheduledSamplesInClarity(sampleSowItemIds.values().flatten(), researcherContactId)
        logger.info "${sowItemScheduledSampleIdMap}"
        def scheduledSampleLimsIdSampleNodeMap = BeanUtil.nodeManager.getSampleNodesMap(sowItemScheduledSampleIdMap.values())
        logger.info "Moving Scheduled samples to ${Stage.SOW_ITEM_QC.value}"
        //assign scheduled samples to “Sow Item QC” workflow
        return scheduledSampleLimsIdSampleNodeMap?.values()?.collect{it.artifactNode}
    }

    Map<Long, String> getSowItemsStatus(Long pmoSampleId){
        sampleQcUtility.getSowItemsStatus(pmoSampleId)
    }

    abstract String validateAnalytes()

    abstract List<ArtifactNode> processRecordDetails(RecordDetailsInfo recordDetailsInfo)

    abstract Stage getDestinationStage()

    abstract List<String> checkProcessUdfs(SampleQcProcess sampleQcProcess)
}
