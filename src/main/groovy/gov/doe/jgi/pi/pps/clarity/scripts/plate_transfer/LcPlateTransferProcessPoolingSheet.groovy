package gov.doe.jgi.pi.pps.clarity.scripts.plate_transfer

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.plate_transfer.beans.PlateTransferTableBean
import org.slf4j.LoggerFactory

/**
 * /**
 * This class corresponds to PacBio Plate To Tube Transfer process
 * This process reads and validates the pooling sheet
 */
class LcPlateTransferProcessPoolingSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(LcPlateTransferProcessPoolingSheet.class)

    @Override
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        transferBeansDataToClarityLibraryStocks()
        process.setCompleteStage()
    }

    void transferBeansDataToClarityLibraryStocks() {
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            PlateTransferTableBean bean = (PlateTransferTableBean) process.getPoolCreationTableBean(clarityLibraryStock.parentAnalyte.id)
            clarityLibraryStock.udfLibraryQcResult = bean.poolLabProcessResult?.value
            clarityLibraryStock.systemQcFlag = bean.poolLabProcessResult?.value == Analyte.PASS
            clarityLibraryStock.udfLibraryQcFailureMode = bean.poolLabProcessFailureMode?.value
            clarityLibraryStock.udfLibraryMolarityPm = bean.poolConcentrationpM
            clarityLibraryStock.udfVolumeUl = ClarityLibraryPool.DEFAULT_VOLUME_UL
        }
    }

}