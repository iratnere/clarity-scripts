package gov.doe.jgi.pi.pps.clarity.config

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive
import gov.doe.jgi.pi.pps.util.util.OnDemandCache

enum SequencingPlatform {
	PACBIO('PacBio'),
    ILLUMINA('Illumina')

    final String value

    SequencingPlatform(String value) {
		this.value = value
	}

	String toString() {
		return value
	}

	private static final EnumConverterCaseInsensitive<SequencingPlatform> converter = new EnumConverterCaseInsensitive(SequencingPlatform)

	static SequencingPlatform toEnum(value) {
		converter.toEnum(value)
	}

	private static final OnDemandCache<Map> cachedSeqPlatformSeqQueue = new OnDemandCache<>()

}
