package gov.doe.jgi.pi.pps.clarity.scripts.sequence_analysis

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 8/18/2015.
 */
class SqAnalysisExitRecordDetails extends ActionHandler {
    static final logger = LoggerFactory.getLogger(SqAnalysisExitRecordDetails.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.inputAnalytes.each{ analyte ->
            analyte.artifactNode.httpRefresh()
            validateInputAnalyte(analyte as ClarityPhysicalRunUnit)
        }
        process.setCompleteStage()
    }

    void validateInputAnalyte(ClarityPhysicalRunUnit physicalRunUnit) {
        Boolean systemQcFlag = physicalRunUnit.systemQcFlag
        //if (systemQcFlag == null) -> prevented by API
        String failureMode = physicalRunUnit.sequencingFailureMode
        if (systemQcFlag && failureMode) {
            process.postErrorMessage("""
$physicalRunUnit / $physicalRunUnit.name:
The '$failureMode' Failure Mode is set to the passed analyte.
""")
        }
        if (!systemQcFlag && !failureMode) {
            process.postErrorMessage("""
$physicalRunUnit / $physicalRunUnit.name:
The Failure Mode udf is not set.
""")
        }
    }
}
