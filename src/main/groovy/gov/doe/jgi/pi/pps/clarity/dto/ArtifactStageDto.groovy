package gov.doe.jgi.pi.pps.clarity.dto

import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage

/**
 * Created by dscott on 3/4/2016.
 */
class ArtifactStageDto implements Comparable<ArtifactStageDto> {

    String uri
    String name
    String status
    boolean active = false

    ArtifactStageDto() {}

    ArtifactStageDto(ArtifactWorkflowStage artifactWorkflowStage) {
        this.uri = artifactWorkflowStage.workflowNode.url
        this.name = artifactWorkflowStage.workflowNode.name
        this.status = artifactWorkflowStage.statusString
        this.active = artifactWorkflowStage.status.active
    }

    boolean getIsQueued() {
        return 'QUEUED' == status
    }

    boolean getIsInProgress() {
        return 'IN_PROGRESS' == status
    }

    @Override
    int compareTo(ArtifactStageDto o) {
        if (this.name && o?.name) {
            return this.name.trim().compareToIgnoreCase(o.name.trim())
        }
        if (this.uri && o?.uri) {
            return this.uri.compareTo(o.uri)
        }
        if (!o) {
            return -1
        }
        return 0
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof ArtifactStageDto)) return false
        ArtifactStageDto that = (ArtifactStageDto) o
        return uri == that.uri
    }

    int hashCode() {
        return uri.hashCode()
    }

    String toString() {
        "${name} (${status})"
    }
}
