package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.text.SimpleDateFormat

@Service
class RqcPacBioService {

    static final Logger logger = LoggerFactory.getLogger(RqcPacBioService.class)
    final static String RESPONSE_CODE = 'response-code'
    final static String RESPONSE_BODY = 'response-body'

    @Value("\${rqcPacBioReady.url}")
    private String rqcPacBioReadyUrl

    void callPacBioSequencingCompleteService(def process) {
        def contactId = process.researcher?.contactId
        def libraryNames = getLibraryNames(process)
        def jsonSubmission = getJsonSubmission(contactId, libraryNames)
        HTTPBuilder http = new HTTPBuilder(rqcPacBioReadyUrl)
        http.request(Method.POST, ContentType.JSON) { req ->
            body = jsonSubmission.toPrettyString()
            response.success = { HttpResponseDecorator resp, respJson ->
                JSONObject jsonResponse = buildResponseJson(jsonSubmission, resp, respJson, uri)
                submitToCouchdb(jsonResponse)
            }

            response.failure = { HttpResponseDecorator resp, respJson ->
                def json = JsonUtil.toJson(respJson)
                logger.error("Rqc service response: status [${resp.status}], body [${json?.toString(2)}]")
                def errorMsg
                if (resp.status >= 500)
                    errorMsg = 'cannot reach RQC website'
                else if (resp.status == 422) {
                    errorMsg = json?.'errors'?.collect { it.'message' }?.join('\n')
                }
                errorMsg ?: 'unexpected response from RQC website'
                process.postErrorMessage(errorMsg)
            }
        }
    }

    def getLibraryNames(def process){
        List<PacBioBindingComplex> inputAnalytes = process.inputAnalytes
        def libraryNames = []
        inputAnalytes.each { PacBioBindingComplex bindingComplex ->
            Analyte parentAnalyte = bindingComplex.parentAnalyte.parentAnalyte //return ClarityLibraryStock or ClarityLibraryPool
            if (parentAnalyte instanceof ClarityLibraryStock){
                libraryNames.add(parentAnalyte.name)
            }
            if (parentAnalyte instanceof ClarityLibraryPool){
                libraryNames.addAll((parentAnalyte as ClarityLibraryPool).poolMembers*.name)
            }
        }
        libraryNames
    }

    JSONObject buildResponseJson(submissionBody, HttpResponseDecorator resp, respJson, uri) {
        Date notificationTime = new Date() //resp.headers['Date']
        JSONObject responseJson = new JSONObject()
        responseJson['success'] = resp.success
        String date
        try {
            date = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss z').format(notificationTime)
        } catch (Throwable t) {
        }
        responseJson['notification-time'] = date//notificationTime.format('yyyy-MM-dd HH:mm:ss z')
        responseJson['submission-url'] = uri //resp.context['http.request'].URI
        responseJson['submission-content-type'] = resp.contentType
        responseJson['submission-method'] = resp.context['http.request']?.method
        responseJson['submission-body'] = submissionBody
        responseJson[RESPONSE_CODE] = resp.status
        responseJson[RESPONSE_BODY] = """
            Rqc service response ${resp.status}:
            ${JsonUtil.toJson(respJson)}
        """
        logger.info "Complete PacBio Sequencing service response: ${responseJson}"
        responseJson
    }

    def getJsonSubmission(def contactId, List<String> libraries) {
        def jsonSubmission = new JsonBuilder()
        jsonSubmission {
            'submitted-by-cid'(contactId)
            'sequencing-complete-library-names'(libraries.collect { "$it" })
        }
        logger.info "PacBio Complete Submission ${jsonSubmission.toPrettyString()}"
        jsonSubmission
    }

    def submitToCouchdb(JSONObject couchDbRecord) {
        /* TODO fix this - where is CouchDbEntity parameter to insert?
        CouchDb couchDb = ClarityCouchdbService.CouchDatabase.PROCESS_SCRIPT.couchDb
        couchDbService.insert
        */
    }
}
