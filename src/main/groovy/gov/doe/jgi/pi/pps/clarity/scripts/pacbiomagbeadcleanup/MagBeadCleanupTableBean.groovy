package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 11/2/2015.
 */
class MagBeadCleanupTableBean {

    MagBeadCleanupTableBean() {}

    @FieldMapping(header = '#', cellType = CellTypeEnum.NUMBER)
    public BigDecimal number

    @FieldMapping(header = 'PacBio Library', cellType = CellTypeEnum.STRING)
    public String pacBioLibrary

    @FieldMapping(header = 'Library Name', cellType = CellTypeEnum.STRING)
    public String libraryName

    @FieldMapping(header = 'Current Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal currentVolumeUl

    @FieldMapping(header = 'Current Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal currentConcentrationNgUl

    @FieldMapping(header = 'Current Molarity (pm)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal currentMolarityPm

    @FieldMapping(header = 'New Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal newVolumeUl

    @FieldMapping(header = 'New Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal newConcentrationNgUl

    void populateRequiredFields(){
        newVolumeUl = 100
        newConcentrationNgUl = 10
    }
}
