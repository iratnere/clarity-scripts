package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class LcProcessLibraryCreationSheet extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(LcProcessLibraryCreationSheet.class)

    @Override
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ClarityProcess clarityProcess = process as LibraryCreationProcess

        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.validateProcessUdfs()
        clarityProcess.lcAdapter.updateLibraryStockUdfs()
        process.setCompleteStage()
    }

}