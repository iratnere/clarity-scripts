package gov.doe.jgi.pi.pps.clarity.model.researcher

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ResearcherNode

/**
 * Created by tlpaley on 2/24/16.
 */
class LabUser extends Researcher {

    LabUser(ResearcherNode researcherNode) {
        super(researcherNode)
    }

}
