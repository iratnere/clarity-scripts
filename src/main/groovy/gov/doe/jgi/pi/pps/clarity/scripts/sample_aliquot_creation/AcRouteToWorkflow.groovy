package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification.SampleAliquotEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AcRouteToWorkflow extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(AcRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        doRefresh()

        logger.info "Sample Aliquots Plate Container: ${(process as SampleAliquotCreationProcess).outputPlateContainer?.id}"
        updateSamplesUdfs()
        if (process.printLabelsTriggerUnused)
            process.writeLabels([process.outputAnalytes[0]]) //need to print one output plate label
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        freezerService.freezerCheckoutInputAnalytes(process)

        groupOutputByDestinationStage()?.each { String stageUri, List<String> limsIds ->
            process.routeArtifactIdsToUri(stageUri, limsIds)
        }
        process.routeArtifactNodes(Stage.ABANDON_QUEUE, aliquotsByLabResult(Analyte.FAIL)*.artifactNode)

        sendEmailNotification()
    }

    List<ClaritySampleAliquot> aliquotsByLabResult(labResult, List<ClaritySampleAliquot> outputAnalytes = process.outputAnalytes as List<ClaritySampleAliquot>) {
        ContainerNode outputPlateContainer = (process as SampleAliquotCreationProcess).outputPlateContainer
        boolean plateResult = outputPlateContainer?.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf) == labResult
        if (outputPlateContainer && plateResult) {
            return outputAnalytes.findAll{ !it.isCustomAliquot }
        }
        if (outputPlateContainer && !plateResult) {
            return null
        }
        return outputAnalytes.findAll{ it.udfLabResult == labResult && (!it.isCustomAliquot) }
    }

    def updateSamplesUdfs(List<ClaritySampleAliquot> outputAnalytes = process.outputAnalytes as List<ClaritySampleAliquot>) {
        process.batchPmoSampleNodes //batch lookup for efficiency PPS-4781
        outputAnalytes.each { ClaritySampleAliquot sampleAliquot ->
            //SampleAnalyteTableBean bean = (SampleAnalyteTableBean) process.getSampleAnalyteTableBean(sampleAliquot.id)
            SampleAnalyte sampleAnalyte = sampleAliquot.parentAnalyte
            PmoSample rootSample = sampleAliquot.parentPmoSamples.find{ true }
            ScheduledSample scheduledSample = (ScheduledSample) sampleAnalyte.claritySample

            scheduledSample.udfNotes = sampleAliquot.udfNotes
            if (rootSample.udfVolumeUl != null && sampleAliquot.udfVolumeUsedUl)
                rootSample.udfVolumeUl = rootSample.udfVolumeUl - sampleAliquot.udfVolumeUsedUl
        }
    }

    Map<String, List<String>> groupOutputByDestinationStage(){
        List<ClaritySampleAliquot> passedAliquots = aliquotsByLabResult(Analyte.PASS)
        if (!passedAliquots)
            return null
        Map<String, List<String>> outputsByStage = [:].withDefault{[]}
        passedAliquots.each{sampleAliquot ->
            String uri = getWorkflowStageUri(sampleAliquot.lcWorkflowName)
            outputsByStage[uri] << sampleAliquot.id
        }
        return outputsByStage
    }

    List<EmailEvent> sendEmailNotification(List<Analyte> failedAliquots = process.outputAnalytes.findAll{ it.systemQcFlag == Boolean.FALSE }) {
        if (!failedAliquots || failedAliquots.isEmpty())
            return null
        List<EmailEvent> emailEvents = new ProcessUtility(process).sendEmailNotification(failedAliquots, new SampleAliquotEmailNotification())
        //emailEvents.addAll(process.sendNotificationEmails(failedAliquots, EmailNotification.Purpose.SA_ABANDON_SAMPLE_ALIQUOT, process.testMode))
        emailEvents
    }

    String getWorkflowStageUri(String workflowName){
        WorkflowNode workflowNode = BeanUtil.nodeManager.getWorkflowNodeByName(workflowName)
        if(!workflowNode)
            process.postErrorMessage("WorkflowNode.notFound: $workflowName")
        return workflowNode.stages[0].uri
    }

}
