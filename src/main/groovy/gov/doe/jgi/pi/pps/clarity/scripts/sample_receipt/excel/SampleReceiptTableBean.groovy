package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.util.ClarityStringUtils
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode

/**
 * Created by datjandra on 3/17/2015.
 */
class SampleReceiptTableBean {
    @FieldMapping(header="Container 2D Barcode", cellType= CellTypeEnum.STRING)
    public String containerBarcode

    @FieldMapping(header="JGI Barcode", cellType= CellTypeEnum.STRING)
    public String jgiBarcode

    @FieldMapping(header="Receipt Status", cellType= CellTypeEnum.DROPDOWN)
    public DropDownList receiptStatus

    @FieldMapping(header="Missing Dry Ice", cellType= CellTypeEnum.DROPDOWN)
    public DropDownList dryIce

    @FieldMapping(header="Receipt Comments", cellType= CellTypeEnum.STRING)
    public String receiptComments

    @FieldMapping(header="Operator Id", cellType= CellTypeEnum.NUMBER)
    public BigDecimal operatorId

    private ContainerNode containerNode
    private String smInstructionsText = ''
    private Integer qcTypeId = 0

    List<String> getPassFailList(){
        return Analyte.PASS_FAIL_LIST.collect{it.toLowerCase()}
    }

    String getReceiptStatusString(){
        return receiptStatus.value.equalsIgnoreCase(Analyte.PASS)? Analyte.PASS: Analyte.FAIL
    }

    String validateBean(){
        String errors =""
        if(operatorId == null)
            errors += "Operator Id field is required.\n"
        if(!dryIce )
        if(receiptComments && ClarityStringUtils.extendedTrim(receiptComments)?.length() > 4000)
            errors += '"Receipt Comments" + "Dry Ice" comments must be less than 4000 characters.\n'
        if(!receiptStatus || !(receiptStatus.value?.toLowerCase() in passFailList))
            errors += "samples in the input file must have valid, \"Receipt status (Pass/Fail)\".\n"
        containerBarcode = ClarityStringUtils.extendedTrim(containerBarcode)
        if(!containerNode.isPlate && !containerBarcode)
            errors += 'samples in the input file must have "2D Sample Barcode" if they are tubes.\n'
        return errors
    }

    String getReceiptNotes(){
        String receiptNotes = receiptComments
        String dryIceValue = dryIce.value
        if (dryIceValue.equals('Y')) {
            receiptNotes = (receiptNotes?receiptNotes:'')+" Shipped with no dry ice"
        } else if (dryIceValue.equals('N')) {
            receiptNotes = (receiptNotes?receiptNotes:'')+" Dry Ice Intact"
        }
        return receiptNotes
    }

    void updateSampleUdfs(PmoSample pmoSample, String date){
        pmoSample.udfSampleReceiptNotes = receiptNotes
        pmoSample.udfSampleReceiptMissingDryIce = dryIce.value
        pmoSample.udfSampleReceiptDate = date
        pmoSample.udfSampleReceiptResult = receiptStatusString
    }

    void updatePerContainerUdfs(PmoSample pmoSample, String date){
        if (!containerNode.isPlate)
                containerNode.name = containerBarcode
        pmoSample.udfContainerLocation = passed? PmoSample.CONTAINER_LOCATION_ON_SITE: PmoSample.CONTAINER_LOCATION_NONE
        pmoSample.udfContainerLocationDate = date
        containerNode.contentsArtifactNodes.each{ ArtifactNode artifactNode ->
            ClaritySample sample = SampleFactory.sampleInstance(artifactNode.sampleNode)
            sample.requiredQcType = qcTypeId
            sample.udfSmInstructions = smInstructionsText
        }
    }

    boolean getPassed(){
        return receiptStatusString == Analyte.PASS
    }

    void appendInstructions(String instructions){
        if(!instructions) {
            return
        }
        if (!this.smInstructionsText.contains(instructions)) {
            this.smInstructionsText += instructions + System.getProperty('line.separator') //append instructions + newline
        }
    }

    void updateQcType(Integer sowQcTypeId) {
        qcTypeId = qcTypeId | sowQcTypeId
    }

    @Override
    public String toString() {
        return "SampleReceiptTableBean{" +
                "containerBarcode='" + containerBarcode + '\'' +
                ", jgiBarcode='" + jgiBarcode + '\'' +
                ", receiptStatus=" + receiptStatusString +
                ", dryIce=" + dryIce?.value +
                ", receiptComments='" + receiptComments + '\'' +
                ", operatorId=" + operatorId +
                '}';
    }
}
