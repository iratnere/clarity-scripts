package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.SowQcFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ClaritySampleAnalyteComparator
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.GenericSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SIPSowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SowQcAdapter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/26/15.
 */
class SowItemQc extends ClarityProcess {
    static final Logger logger = LoggerFactory.getLogger(SowItemQc.class)
    static ProcessType processType = ProcessType.SM_SOW_ITEM_QC

    static final SCRIPT_GENERATED_SOW_QC_WORKSHEET = 'Download SowItemQC Worksheet'
    static final UPLOADED_SOW_QC_WORKSHEET = 'Upload SowItemQC Worksheet'
    boolean testMode = false
    List<SampleAnalyte> tubeScheduledSamples
    Map<String, Map<PmoSample,List<ScheduledSample>>> plateSamples
    Map<Integer, List<Section>> sowQcSections = null

    static final String[] getActiveSowQCFailureModes() {
        return SowQcFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }
    static final DropDownList getDropDownSowQCFailureModes() {
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(activeSowQCFailureModes)
        return dropDownList
    }

    static DropDownList getDropDownPassFail(){
        return new DropDownList(controlledVocabulary: Analyte.PASS_FAIL_LIST)
    }

    SowItemQc(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation': SowItemQcPpv,
            'PrepareSowItemQcSheet':PrepareSowItemQcSheet,
            'ProcessSowItemQcSheet':ProcessSowItemQcSheet,
            'RouteToNextWorkflow':SowItemQcRouteToWorkflow,
        ]
    }

    void sortInputAnalytes(){
        def inputAnalytes = this.inputAnalytes.sort{a,b->a.containerId <=> b.containerId}
        Collections.sort(inputAnalytes, new ClaritySampleAnalyteComparator())
        segregateAnalytes(inputAnalytes)
    }

    void segregateAnalytes(List<Analyte> inputAnalytes){
        tubeScheduledSamples = []
        plateSamples = [:]
        int tubeIndex=0
        inputAnalytes.eachWithIndex{ SampleAnalyte sampleAnalyte, index ->
            if(sampleAnalyte.containerNode.containerTypeEnum == ContainerTypes.TUBE)
                tubeScheduledSamples[tubeIndex++] = sampleAnalyte
            else{
                ClaritySample rootSample = sampleAnalyte.claritySample.pmoSample
                String sampleContainerId = rootSample.sampleAnalyte.containerId
                if(!plateSamples[sampleContainerId])
                    plateSamples[sampleContainerId] = [:].withDefault {[]}
                plateSamples[sampleContainerId][rootSample] << sampleAnalyte.claritySample
            }
        }
    }

    Map<Integer, List<Section>> getSowQcSections(){
        if(!sowQcSections){
            def fileNode = processNode.outputResultFiles?.find { it.name.contains(UPLOADED_SOW_QC_WORKSHEET)}
            SowQCWorksheet worksheet = new SowQCWorksheet(fileNode.id, this)
            sowQcSections = worksheet?.readWorksheet(this.testMode)
        }
        sowQcSections
    }

    void cacheRootSamples(){
        nodeManager.getSampleNodes(inputAnalytes.collect{it.claritySample.pmoSampleLimsId})
    }

    List<SowQcAdapter> getSowQcAdapters(List<Analyte> analytes = inputAnalytes) {
        List<Analyte> sipSamples = []
        List<Analyte> genericSamples = []
        analytes.each { Analyte analyte ->
            ScheduledSample scheduledSample = (ScheduledSample) analyte.claritySample
            if (scheduledSample.isSIPOriginalScheduledSample) {
                logger.info "Found SIP Metagenomics original scheduled sample ${analyte.id}"
                sipSamples << analyte
            } else {
                logger.info "Found non-SIP Metagenomics scheduled sample ${analyte.id}"
                genericSamples << analyte
            }
        }
        List<SowQcAdapter> adapters = []
        if (genericSamples) {
            adapters << new GenericSowQcAdapter(genericSamples)
        }
        if (sipSamples) {
            adapters << new SIPSowQcAdapter(sipSamples)
        }
        logger.info "Made adapters $adapters for analytes $analytes"
        return adapters
    }

    BigDecimal getUdfReplicatesFailurePercentAllowed() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_REPLICATES_FAILURE_PERCENT_ALLOWED.udf)
    }

    BigDecimal getUdfControlsFailurePercentAllowed() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_CONTROLS_FAILURE_PERCENT_ALLOWED.udf)
    }
}
