package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleReplacementService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

class RouteToFractionation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RouteToFractionation.class)
    List<Analyte> passedSamples = []
    List<Analyte> failedSamples = []
    StatusService statusService
    MetabolomicsQCProcess qcProcess

    void execute() {
        logger.info "Starting ${this.class.simpleName} action...."
        performLastStepActions()
        process.processRoutingRequests()
    }

    void performLastStepActions() {
        qcProcess = (MetabolomicsQCProcess)process
        statusService = BeanUtil.getBean(StatusService.class)
        copyFieldsFromOutputs()
        updateSampleAndSOWStatus()
        if(passedSamples)
            process.routeAnalytes(Stage.SAMPLE_FRACTIONATION, passedSamples)
        if(failedSamples) {
            SampleReplacementService sampleReplacementService = BeanUtil.getBean(SampleReplacementService.class)
            def responseCode = sampleReplacementService?.replaceSamples(process.researcherContactId, failedSamples.collect {
                it.claritySample.pmoSample.pmoSampleId
            } as Set)
            assert responseCode == 201
        }
    }

    void updateSampleAndSOWStatus() {
        Map statusCompleteMap = [:]
        Map finalStatusMap = [:]
        Map<Long, SowItemStatusCv> inProgressSows = [:]
        Map<Long, SowItemStatusCv> naSows = [:]
        List<Long> completeSows = []
        process.inputAnalytes.each { Analyte analyte ->
            PmoSample pmoSample = analyte.claritySample.pmoSample
            ScheduledSample scheduledSample = analyte.claritySample.pmoSample.scheduledSamples[0]
            statusCompleteMap[pmoSample.pmoSampleId] = SampleStatusCv.SAMPLE_QC_COMPLETE
            inProgressSows[scheduledSample.sowItemId] = SowItemStatusCv.IN_PROGRESS
            if(pmoSample.passedQc) {
                finalStatusMap[pmoSample.pmoSampleId] = SampleStatusCv.AVAILABLE_FOR_USE
                completeSows << scheduledSample.sowItemId //PPS-5177: Complete SOWs only if sample passed Metabolomics QC
                passedSamples << analyte
            }
            else {
                finalStatusMap[pmoSample.pmoSampleId] = SampleStatusCv.ABANDONED
                naSows[scheduledSample.sowItemId] = SowItemStatusCv.NEEDS_ATTENTION //PPS-5177: Change SOW to Needs Attention if sample failed Metabolomics QC
                failedSamples << analyte
            }
        }
        statusService.submitSampleStatus(statusCompleteMap, process.researcherContactId)
        statusService.submitSampleStatus(finalStatusMap, process.researcherContactId)
        statusService.submitSowItemStatus(inProgressSows, process.researcherContactId)
        if(completeSows)
            statusService.completeSowItems(completeSows, process.researcherContactId)
        if(naSows)
            statusService.submitSowItemStatus(naSows, process.researcherContactId)
    }

    void copyFieldsFromOutputs(){
        qcProcess.outputAnalytes.each { SampleQcHamiltonAnalyte analyte ->
            PmoSample pmoSample = analyte.claritySample.pmoSample
            pmoSample.sampleNode.readOnly = false
            pmoSample.udfMetabolomicsSampleId = analyte.udfMetabolomicsSampleId
            pmoSample.udfSampleQCResult = analyte.udfSampleQCResult
            pmoSample.udfSampleQCFailureMode = pmoSample.passedQc ? '' : analyte.udfSampleQCFailureMode
            pmoSample.udfIsotopeEnrichment = analyte.udfIsotopeEnrichment
        }
    }
}
