package gov.doe.jgi.pi.pps.clarity.scripts.print.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class SampleTableBean {
	@FieldMapping(header = 'Sample Name', cellType = CellTypeEnum.STRING)
	public String sampleName
	@FieldMapping(header = 'LIMS ID (Sample)', cellType = CellTypeEnum.STRING)
	public String sampleLimsId

    Analyte analyte

	def validate(NodeManager nodeManager = BeanUtil.nodeManager) {
        def errorMsg = "Please correct the errors below and upload spreadsheet again.$ClarityProcess.WINDOWS_NEWLINE Sample LIMS ID($sampleLimsId):"<<' '
        ArtifactNode artifactNode = nodeManager.getArtifactNode(sampleLimsId)
        if (!artifactNode) {
            return  errorMsg << "Can't find artifact node by lims id $sampleLimsId"
        }
        analyte = AnalyteFactory.analyteInstance(artifactNode)
        if (!analyte) {
            return  errorMsg << "Can't find analyte by artifact node $artifactNode"
        }
        return null
	}

}
