package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.PoolCreation

/**
 * Created by lvishwas on 1/6/15.
 */
class PoolCreationTableBean {
    @FieldMapping(header='#', cellType = CellTypeEnum.NUMBER)
    public def poolNumber
    @FieldMapping(header='qPCR\'d Sample Name', cellType = CellTypeEnum.STRING)
    public def sampleName
    @FieldMapping(header='qPCR\'d Sample LIMS ID', cellType = CellTypeEnum.STRING)
    public def sampleLimsId
    @FieldMapping(header='Conc (pM)', cellType = CellTypeEnum.NUMBER)
    public def concentration
    @FieldMapping(header='Index ID', cellType = CellTypeEnum.STRING)
    public def indexName
    @FieldMapping(header='DILUTE?', cellType = CellTypeEnum.STRING)
    public def dilute
    @FieldMapping(header='ul of Undiluted or 1:10 Library', cellType = CellTypeEnum.FORMULA)
    public def ulOfUndiluted
    @FieldMapping(header='Target Pool Concentration pM', cellType = CellTypeEnum.NUMBER)
    public def targetPoolConc
    @FieldMapping(header='Source Labware', cellType = CellTypeEnum.FORMULA)
    public def srcLabware
    @FieldMapping(header='Source Position', cellType = CellTypeEnum.STRING)
    public String srcPosition
    @FieldMapping(header='Destination Labware', cellType = CellTypeEnum.FORMULA)
    public def destLabware
    @FieldMapping(header='Destination Position', cellType = CellTypeEnum.STRING)
    public String destPosition
    @FieldMapping(header='Notification', cellType = CellTypeEnum.STRING)
    public def notification
    @FieldMapping(header='Pool Name', cellType = CellTypeEnum.STRING)
    public def poolName
    @FieldMapping(header='Pool Size', cellType = CellTypeEnum.NUMBER)
    public def poolSize
    @FieldMapping(header='Run Mode', cellType = CellTypeEnum.STRING)
    public def runMode
    @FieldMapping(header='Pool Lab Process Result', cellType = CellTypeEnum.DROPDOWN)
    public def poolLabProcessResult
    @FieldMapping(header='Pool Lab Process Failure Mode', cellType = CellTypeEnum.DROPDOWN)
    public def poolLabProcessFailureMode
    @FieldMapping(header='Pool Concentration pM', cellType = CellTypeEnum.NUMBER)
    public def poolConcentrationpM
    @FieldMapping(header='PacBio Pool Concentration ng/ul', cellType = CellTypeEnum.NUMBER)
    public def pacBioPoolConcentration
    @FieldMapping(header='Actual Template Size (bp)', cellType = CellTypeEnum.NUMBER)
    public def actualTemplateSizeBp
    @FieldMapping(header='Source Container Barcode', cellType = CellTypeEnum.STRING)
    public def sourceContainerBarcode
    @FieldMapping(header='Source Container Type', cellType = CellTypeEnum.STRING)
    public def sourceContainerType
    //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
    @FieldMapping(header='Library Percentage with SOF', cellType = CellTypeEnum.NUMBER)
    public def libraryPercentage
    @FieldMapping(header='Library Actual Percentage with SOF', cellType = CellTypeEnum.NUMBER)
    public def lpActualWithSof

    def analyte

    String validateBean(){
        String errors = ''
        if(runMode.contains(PlatformTypeCv.PACBIO.value)) {
            //PPS-4497
            if(!pacBioPoolConcentration)
                errors += "PacBio Pool Concentration is a required field for the analyte ${sampleName}.\n"
            if(!ulOfUndiluted)
                errors += "ul of Undiluted or 1:10 Library is a required field for the analyte ${sampleName}.\n"
        }
        else {
            if (!poolConcentrationpM)
                errors += "Pool Concentration pM is a required field for the analyte ${sampleName}.\n"
        }
        if(!poolLabProcessResult?.value)
            errors += "Pool Lab Process Result is a required field for the analyte ${sampleName}.\n"
        if(poolLabProcessResult?.value == Analyte.FAIL && !poolLabProcessFailureMode?.value)
            errors += "Pool Lab Process Failure Mode is a required field for the analyte ${sampleName}.\n"
        if(runMode.contains(SequencerModelCv.NOVASEQ)) {
            if (!lpActualWithSof)
                errors += "Library Actual Percentage with SOF is a required field for the analyte ${sampleName}.\n"
        }
        return errors
    }

    void populateBean(Analyte libraryStock, Analyte pool){
        sampleName = libraryStock.name
        sampleLimsId = libraryStock.id
        String molarityStr = libraryStock.udfLibraryMolarityPm
        if(molarityStr)
            concentration = (molarityStr as Double).round(6)
        sourceContainerBarcode = libraryStock.containerId
        sourceContainerType = libraryStock.containerType
        poolName = pool.name
        poolSize = pool.poolMembers.size()
        runMode = libraryStock.udfRunMode
        poolLabProcessResult = PoolCreation.dropDownPassFail
        poolLabProcessFailureMode = PoolCreation.dropDownFailureModes
        actualTemplateSizeBp = libraryStock.udfActualTemplateSizeBp
        analyte = libraryStock
    }

    void populateRequiredFields(int passCount){
        if(runMode.contains(PlatformTypeCv.PACBIO.value)) {
            pacBioPoolConcentration = 10
            ulOfUndiluted = 2
        }
        else{
            poolConcentrationpM = 10
        }
        poolLabProcessResult = PoolCreation.dropDownPassFail
        poolLabProcessFailureMode = PoolCreation.dropDownFailureModes
        if(passCount > 0)
            poolLabProcessResult.value = Analyte.PASS
         else{
            poolLabProcessResult.value = Analyte.FAIL
            poolLabProcessFailureMode.value = poolLabProcessFailureMode.controlledVocabulary[0]
        }
        dilute = 'DILUTE'
        targetPoolConc = 10
        RunModeCv runModeCv = RunModeCv.findByRunMode(runMode)
        if(runModeCv.sequencerModel.isIlluminaNovaSeq) {
            lpActualWithSof = 0.2
        }
    }

    String getKey(){
        StringBuilder sb = new StringBuilder()
        String delimiter = ' '
        if(runMode.contains(PlatformTypeCv.PACBIO.value)) {
            sb.append('PacBio Pool Concentration ng/ul - ').append(pacBioPoolConcentration).append(delimiter)
        }
        else{
            sb.append('Pool Concentration pM - ').append(poolConcentrationpM).append(delimiter)
        }
        sb.append('Pool number - ').append(destPosition).append(delimiter)
        sb.append('Pool size - ').append(poolSize).append(delimiter)
        sb.append('Run mode - ').append(runMode).append(delimiter)
        sb.append('Pool Lab process result - ').append(poolLabProcessResult.value).append(delimiter)
        sb.append(poolLabProcessFailureMode?.value)
        return sb.toString()
    }
}
