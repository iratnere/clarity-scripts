package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.GeneusNode
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 1/31/17.
 * Required: Sample Name, Library Name, Library Index, DOP, Run Mode, Molarity
 */
class OnboardingBean {
    @FieldMapping(header = 'JGI Project ID', cellType = CellTypeEnum.NUMBER)
    public def jgiProjectId
    @FieldMapping(header = 'JGI Project Name', cellType = CellTypeEnum.STRING)
    public def jgiProjectName
    @FieldMapping(header = 'JGI Sample ID', cellType = CellTypeEnum.NUMBER)
    public def pmoSampleId
    @FieldMapping(header = 'Genus', cellType = CellTypeEnum.STRING)
    public def genus
    @FieldMapping(header = 'Species', cellType = CellTypeEnum.STRING)
    public def species
    @FieldMapping(header = 'Strain', cellType = CellTypeEnum.STRING)
    public def strain
    @FieldMapping(header = 'sample name', cellType = CellTypeEnum.STRING)
    public def sampleName
    @FieldMapping(header = 'SOW ID', cellType = CellTypeEnum.NUMBER)
    public def sowId
    @FieldMapping(header = 'SOW run mode', cellType = CellTypeEnum.STRING)
    public def sowRunMode
    @FieldMapping(header = 'Degree of Pooling', cellType = CellTypeEnum.NUMBER)
    public def dop
    @FieldMapping(header = 'library protocol', cellType = CellTypeEnum.STRING)
    public def libraryProtocol
    @FieldMapping(header = 'Lab Library Name', cellType = CellTypeEnum.STRING)
    public def labLibraryName
    @FieldMapping(header = 'JGI Lib Name', cellType = CellTypeEnum.STRING)
    public def jgiLibraryStockName
    @FieldMapping(header = 'Aliquot amount (ng)', cellType = CellTypeEnum.NUMBER)
    public def aliquotAmount
    @FieldMapping(header = 'Aliquot Volume', cellType = CellTypeEnum.NUMBER)
    public def aliquotVolume
    @FieldMapping(header = 'PCR Cycles', cellType = CellTypeEnum.NUMBER)
    public def pcrCycles
    @FieldMapping(header = 'Index Name / Barcode', cellType = CellTypeEnum.STRING)
    public def indexName
    @FieldMapping(header = 'Library Fragment Size w/ adaptors (bp)', cellType = CellTypeEnum.NUMBER)
    public def libFragmentSize
    @FieldMapping(header = 'Library Stock volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def libVolume
    @FieldMapping(header = 'Library Molarity (pM)', cellType = CellTypeEnum.NUMBER)
    public def libMolarityPm
    @FieldMapping(header = 'Library concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public def libConcentration
    @FieldMapping(header = 'JGI Library lims id', cellType = CellTypeEnum.STRING)
    public def jgiLibraryLimsId
    @FieldMapping(header = 'Library Creation Site', cellType = CellTypeEnum.STRING)
    public def libraryCreationSite
    @FieldMapping(header = 'Pool Number', cellType = CellTypeEnum.NUMBER)
    public def poolNumber
    @FieldMapping(header = 'index sequence', cellType = CellTypeEnum.STRING)
    public def indexSequence

    PmoSample pmoSample
    ScheduledSample scheduledSample
    Analyte sampleAliquot
    Analyte libraryStock
    Analyte libraryPool
    Long contactId
    PoolDetailsBean poolBean = null

    boolean isExternal = false
    boolean checkStatus = false
    Map<Class, Map> udfMaps = [:].withDefault { [:] }

    int hashCode() {
        new HashCodeBuilder(17, 37).
                append(sampleName).append(labLibraryName).toHashCode()
    }

    boolean equals(HudsonAlphaBean bean) {
        bean.hashCode() == this.hashCode()
    }

    void setPmoSample(PmoSample pmoSample) {
        this.pmoSample = pmoSample
        this.pmoSampleId = pmoSample.pmoSampleId
        this.jgiProjectId = pmoSample.sequencingProject?.sequencingProjectId
        this.jgiProjectName = pmoSample.sequencingProject?.udfSequencingProjectName
        this.pmoSample.sampleNode?.readOnly = false
        updateUdfs(PmoSample.class)
    }

    void setScheduledSample(ScheduledSample scheduledSample) {
        this.scheduledSample = scheduledSample
        this.scheduledSample.sampleNode.readOnly = false
        sowId = scheduledSample.sowItemId
        this.scheduledSample.sampleNode.readOnly = false
    }

    void addAnalyte(Analyte analyte) {
        while (!(analyte?.claritySample instanceof PmoSample)) {
            if (!analyte)
                return
            analyte?.artifactNode?.readOnly = false
            switch (analyte.class) {
                case SampleAnalyte:
                    if (scheduledSample)
                        return
                    ClaritySample sample = analyte.claritySample
                    sample.sampleNode?.readOnly = false
                    setScheduledSample(sample)
                    analyte = sample.pmoSample?.sampleAnalyte
                    break
                case ClaritySampleAliquot:
                    if (sampleAliquot)
                        return
                    sampleAliquot = analyte
                    analyte = analyte.singleParent
                    break
                case ClarityLibraryStock:
                    updateLibraryStock(analyte)
                    analyte = analyte.singleParent
                    if (analyte instanceof ClarityLibraryStock) {
                        analyte = analyte.singleParent
                    }
                    break
                case ClarityLibraryPool:
                    if (libraryPool)
                        return
                    libraryPool = analyte
                    if (poolBean)
                        poolBean.jgiPoolName = analyte.name
                    analyte = analyte.parents?.find { it.udfCollaboratorLibraryName == getLabLibraryName() }
                    break
                default:
                    analyte = analyte.singleParent
            }
        }
        setPmoSample(analyte.claritySample)
    }

    List<Analyte> getAnalytes() {
        List<Analyte> analyteList = [pmoSample.sampleAnalyte, scheduledSample.sampleAnalyte, sampleAliquot, libraryStock]
        if (libraryPool)
            analyteList << libraryPool
        return analyteList
    }

    GeneusNode getAnalyteForType(Class analyteClass) {
        switch (analyteClass) {
            case PmoSample:
                return pmoSample.sampleNode
            case ScheduledSample:
                return scheduledSample.sampleNode
            case ClaritySampleAliquot:
                return sampleAliquot.artifactNode
            case ClarityLibraryStock:
                return libraryStock.artifactNode
            case ClarityLibraryPool:
                return libraryPool.artifactNode
            default:
                return null
        }
    }

    void updateUdfs(Class analyteClass) {
        GeneusNode analyte = getAnalyteForType(analyteClass)
        if (!analyte)
            return
        udfMaps[analyteClass].each { ClarityUdf udf, Object value ->
            analyte.setUdf(udf.udf, value)
        }
    }

    String toString() {
        StringBuilder sb = new StringBuilder()
        sb.append("Sample Name : $sampleName")
        sb.append("Lab Library Name : $labLibraryName")
        if(collaboratorPoolName)
            sb.append("Collaborator pool name: $collaboratorPoolName")
        return sb.toString()
    }

    String getLabLibraryName() {
        return labLibraryName
    }

    void updateLibraryStock(Analyte analyte) {
        libraryStock = analyte
        jgiLibraryStockName = analyte?.name
        jgiLibraryLimsId = analyte?.id
        updateUdfs(ClarityLibraryStock.class)
    }

    boolean isMergedBean() {
        return poolNumber >= 0 ? this.libraryPool != null : this.libraryStock != null
    }

    Long getSowId() {
        return sowId as Long
    }

    boolean isCompatible(Analyte analyte) {
        if (analyte.claritySample.udfSowItemType != SowItemTypeCv.ONBOARDING.value)
            return false
        def parentLibraries = analyte.ancestorsForProcessTypes([ProcessType.LC_LIBRARY_CREATION, ProcessType.LP_POOL_CREATION])
        if (!parentLibraries)
            return true
        if (parentLibraries.size() == 1) {
            if (parentLibraries[0] instanceof ClarityLibraryStock) {
                String labLibName = parentLibraries[0].udfCollaboratorLibraryName
                if (!labLibName || labLibName == getLabLibraryName())
                    return true
                return false
            } else
                parentLibraries = parentLibraries[0].poolMembers
        }
        if (parentLibraries.find { it.udfCollaboratorLibraryName == getLabLibraryName() })
            return true
        return false
    }

    void clear() {
        pmoSample = null
        scheduledSample = null
        sowId = null
        sampleAliquot = null
        libraryStock = null
        jgiLibraryStockName = null
        jgiProjectName = null
        jgiProjectId = null
        jgiLibraryLimsId = null
        checkStatus = false
    }

    String getPoolKey() {
        if (!poolNumber)
            return ""
        return "Pool #${poolNumber}"
    }

    String getCollaboratorPoolName() {
        return poolBean.collaboratorPoolName
    }
}