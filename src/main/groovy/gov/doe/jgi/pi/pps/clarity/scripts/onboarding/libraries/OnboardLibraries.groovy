package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.libraries

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryPoolsAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingLibraryStocksAdapter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by lvishwas on 10/20/16.
 */
class OnboardLibraries extends ClarityProcess {

    static ProcessType processType = ProcessType.ONBOARD_LIBRARY_FILES

    boolean testMode = false
    static final String USER_LIBRARY_WORSHEET = 'Upload Onboard Library File'
    static final String SCRIPT_LIBRARY_WORSHEET = 'Download Onboard Library File'
    static final String ONBOARDING_LIBRARIES_SHEET_NAME = 'Onboarding Libraries'
    static final String POOL_DETAILS_TAB = "Pool Details"

    OnboardLibraries(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'ProcessLibraryFile':ProcessLibrariesFile,
                'FinishStep': FinishOnboardingStep
        ]
    }

    OnboardingAdapter getOnboardingAdapterInstance(ExcelWorkbook workbook) {
        int sheetIndex = workbook.getSheetIndex(ONBOARDING_LIBRARIES_SHEET_NAME)
        if(sheetIndex < 0)
            throw new RuntimeException("Could not find sheet by name $ONBOARDING_LIBRARIES_SHEET_NAME. Please check the file format.")
        Section section = workbook.sections.values().find {it.parentSheetIndex == sheetIndex}
        if(section.beanClass == OnboardingBean.class) {
            OnboardingBean poolBean = section.data.find { OnboardingBean bean ->
                bean.poolNumber >= 1
            }
            if(poolBean) {
                if(section.data.find { OnboardingBean bean ->
                    !bean.poolNumber || bean.poolNumber <= 0})
                    throw new RuntimeException("ALL or NONE of the rows should have pool numbers.")
                return new OnboardingLibraryPoolsAdapter(workbook, researcherContactId)
            }
            else
                return new OnboardingLibraryStocksAdapter(workbook, researcherContactId)
        }
    }
}
