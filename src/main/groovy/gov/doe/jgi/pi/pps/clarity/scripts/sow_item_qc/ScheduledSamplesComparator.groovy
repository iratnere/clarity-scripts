package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample

/**
 * Created by lvishwas on 6/22/2015.
 */
class ScheduledSamplesComparator implements Comparator<SampleAnalyte> {
    static final String PACBIO_PLATFORM = 'PacBio'
    static final String MICROBIAL_SCIENTIFIC_PROGRAM = 'Microbial'
    private final static List<String> predefinedOrder =
            Collections.unmodifiableList(['Fragment', 'WgsFragment PacBio', 'LMP'])

    @Override
    int compare(SampleAnalyte sa1, SampleAnalyte sa2) {
        ScheduledSample ss1 = sa1.claritySample
        ScheduledSample ss2 = sa2.claritySample
        int result = (ss1.sampleAnalyte.isOnPlate && ss2.sampleAnalyte.isOnPlate)?(ss1.sampleArtifactNode.containerId <=> ss2.sampleArtifactNode.containerId):0
        if(result == 0) {
            result = (ss1.pmoSample.pmoSampleId <=> ss2.pmoSample.pmoSampleId)
            if (result == 0) {
                String ss1Index = ''
                String ss1Platform = ss1.platform == PACBIO_PLATFORM ? PACBIO_PLATFORM : ''
                if (ss1.udfScientificProgram == MICROBIAL_SCIENTIFIC_PROGRAM) {
                    ss1Index = "${ss1.udfSowItemType} ${ss1Platform}".trim()
                } else {
                    ss1Index = "${ss1.udfSowItemType}"
                }

                String ss2Index = ''
                String ss2Platform = ss1.platform == PACBIO_PLATFORM ? PACBIO_PLATFORM : ''
                if (ss1.udfScientificProgram == MICROBIAL_SCIENTIFIC_PROGRAM) {
                    ss2Index = "${ss2.udfSowItemType} ${ss2Platform}".trim()
                } else {
                    ss2Index = "${ss2.udfSowItemType}"
                }

                result = predefinedOrder.indexOf(ss1Index) - predefinedOrder.indexOf(ss2Index)
                if (result == 0) {
                    if (ss1.udfTargetInsertSizeKb == ss2.udfTargetInsertSizeKb)
                        result = 0
                    else if (ss1.udfTargetInsertSizeKb < ss2.udfTargetInsertSizeKb)
                        result = 1
                    else
                        result = -1
                }
                return result
            }
            return result
        }
        return result
    }
}
