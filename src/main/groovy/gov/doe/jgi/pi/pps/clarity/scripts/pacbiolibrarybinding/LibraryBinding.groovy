package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by lvishwas on 4/6/2015.
 */
class LibraryBinding extends ClarityProcess {

    static ProcessType processType = ProcessType.PACBIO_LIBRARY_BINDING

    LibraryBinding(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':LbPreProcessValidation,
            'LibraryBinding':LbPacBioLibraryBinding,
            'RouteToNextWorkflow':LbRouteToWorkflow
        ]
    }
}
