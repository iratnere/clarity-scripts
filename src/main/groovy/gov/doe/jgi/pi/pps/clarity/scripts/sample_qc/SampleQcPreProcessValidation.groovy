package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Action Handler run at the beginning of the workflow execution to validate the input samples
 * Created by tlpaley on 2/26/15.
 */
class SampleQcPreProcessValidation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SampleQcPreProcessValidation.class)

    @Override
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        //PPS-2692 to prevent node-manager from being closed by finalize method if request times out (suspected source of error),
        // get reference to node-manager early in request.
        checkNodeManager()
        SampleQcProcess sampleQcProcess = (SampleQcProcess) process
        List<SampleQcAdapter> adapters = sampleQcProcess.sampleQcAdapters
        assert adapters
        String errors
        adapters?.each{ SampleQcAdapter adapter ->
            errors = adapter.validateAnalytes()
        }
        if(errors)
            process.postErrorMessage(errors)
    }

    void checkContainerContents(Map<ContainerNode, Set<String>> containerAnalyteIds){
        containerAnalyteIds?.each{container, analyteIds ->
            if(!container){
                process.postErrorMessage("Analytes ${analyteIds} have no Container. \n")
            }
            //all samples from the input containers must be placed on the qc plate
            if(container.contentsIds?.toSet()?.size() > analyteIds?.size()){
                process.postErrorMessage("All samples from the input containers must be placed on the qc plate.")
            }
        }
    }

    void checkNodeManager(){
        NodeManager nodeManager = BeanUtil.nodeManager
        if (nodeManager.getIsClosed()) {
            throw new RuntimeException("${nodeManager} is closed")
        }
    }

    void checkIsSample(Analyte analyte){
        if(analyte instanceof SampleAnalyte && analyte.claritySample instanceof PmoSample)
            return
        process.postErrorMessage("Invalid input $analyte. Expecting Root samples as input")
    }

    void checkSampleStatus(Map statusMap) {
        statusMap.each {
            def sampleStatus = it.value
            if (sampleStatus != SampleStatusCv.AWAITING_SAMPLE_QC.value) {
                process.postErrorMessage("Invalid input ${it.key} with current status ${sampleStatus}. Expected status:  ${SampleStatusCv.AWAITING_SAMPLE_QC.value}")
            }
        }
    }
}
