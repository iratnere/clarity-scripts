package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel.SampleReceiptTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification.SampleFailedReceiptEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification.SampleReceiptEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleReplacementService
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessParams
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.DateFormat
import java.text.SimpleDateFormat

class ReceiveSamples extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(ReceiveSamples.class)
    Map<Long, SampleStatusCv> sampleReceivedStatusMap = [:]
    Map<Long, SampleStatusCv> sampleStatusCvMap = [:]
    Map<Long, Set<SampleAnalyte>> failedSamples = [:].withDefault {[] as Set}
    List<SampleAnalyte> passedSamples = []
    List<Long> pmoSampleIds = []
    def sowItemData

    void execute(){
        performLastStepActions()
        sendEmail(passedSamples, new SampleReceiptEmailNotification())
        processFailedSamples()
        process.nodeManager.httpPutDirtyNodes()
    }

    void performLastStepActions(){
        def current = getDateString(new Date())
        logger.info "Current date $current"
        List<SampleReceiptTableBean> beans = process.barcodeTableBeans
        Map<Long, List<ArtifactNode>> processInputs = [:].withDefault {[]}
        beans.each{ SampleReceiptTableBean bean ->
            if(bean.jgiBarcode) {
                process.updateBeanContainer(bean)
                ArtifactNode artifact
                bean.containerNode.contentsArtifactNodes.each { ArtifactNode artifactNode ->
                    processInputs[bean.operatorId as Long] << artifactNode
                    artifact = artifactNode
                }
                pmoSampleIds << SampleFactory.sampleInstance(artifact.sampleNode).pmoSample.pmoSampleId
            }
        }
        beans.each { SampleReceiptTableBean bean ->
            if(bean.jgiBarcode) {
                processBean(bean, current)
            }
        }
        commitStatus()
        List<String> processes = postSampleReceiptProcesses(processInputs)
        logger.info "Posted Sample Receipt processes $processes"
    }

    void commitStatus(){
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        logger.info "Writing sample status to USS: $sampleReceivedStatusMap"
        statusService.submitSampleStatus(sampleReceivedStatusMap, process.researcherContactId)
        logger.info "Writing sample status to USS: $sampleStatusCvMap"
        statusService.submitSampleStatus(sampleStatusCvMap, process.researcherContactId)
    }

    List<String> postSampleReceiptProcesses(Map processInputs){
        List<String> processIds = []
        ExcelWorkbook workbook = process.workbook
        processInputs.each{Long operatorId, List<ArtifactNode> inputs->
            logger.info "Posting sample receipt process for inputs : $inputs"
            ProcessParams processParams = new ProcessParams(StageUtility.getStepConfigurationNode(process.nodeManager, Stage.SAMPLE_RECEIPT), inputs)
            String processId = process.nodeManager.executeClarityInputProcess(processParams)
            processIds << processId
            if(workbook) {
                String artifactId = process.getBarcodeFileArtifactId(process.nodeManager.getProcessNode(processId))
                logger.info "Uploading barcode association excel file to $artifactId"
                workbook.store(process.nodeConfig, artifactId, false)
            }
        }
        logger.info "All sample receipt processes $processIds"
        return processIds
    }

    String getDateString(Date date){
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        DateFormat df = new SimpleDateFormat(format)
        return df.format(date)
    }

    void processBean(SampleReceiptTableBean bean, String currentDate){
        PmoSample pmoSample
        bean.containerNode.readOnly = false
        bean.containerNode.contentsArtifactNodes.each { ArtifactNode artifactNode ->
            SampleAnalyte sampleAnalyte = AnalyteFactory.analyteInstance(artifactNode)
            pmoSample = sampleAnalyte.claritySample
            bean.updateSampleUdfs(pmoSample, currentDate)
            updateStatusMap(bean, pmoSample)
            sampleAnalyte.artifactNode.readOnly = false
            pmoSample.sampleNode.readOnly = false
            RoutingRequest routingRequest = updateRoutingRequests(bean, sampleAnalyte)
            if(routingRequest)
                process.routingRequests << routingRequest
            updateBeanWithSowData(bean, pmoSample.pmoSampleId)
        }
        bean.updatePerContainerUdfs(pmoSample, currentDate)
    }

    void updateBeanWithSowData(SampleReceiptTableBean bean, Long pmoSampleId){
        if(!pmoSampleIds.contains(pmoSampleId))
            return
        logger.info "Sow item details: ${sowItems[pmoSampleId]}"
        sowItems[pmoSampleId]?.each { sow ->
            bean.appendInstructions(sow.'sm-instructions')
            Integer sowQcTypeId = sow.'qc-type-id' as Integer
            if (!sowQcTypeId && sowQcTypeId != 0) {
                process.postErrorMessage("QC Type ID not defined for sow-item:\n${sow.toString(2)}")
            }
            bean.updateQcType(sowQcTypeId)
        }
        logger.info "Final SM instructions : ${bean.smInstructionsText}"
        logger.info "Calculated QC Type Id: $bean.qcTypeId"
    }

    void processFailedSamples(Map<BigDecimal,Set<Analyte>> operatorIdSamples = failedSamples) {
        if(!operatorIdSamples)
            return
        SampleReplacementService sampleReplacementService = BeanUtil.getBean(SampleReplacementService.class)
        for(Map.Entry entry : operatorIdSamples){
            if(!entry.value)
                continue
            def sampleIds = entry.value.collect{it.claritySample.pmoSample.pmoSampleId} as Set
            logger.info "Calling sample replacement service for failed samples $sampleIds"
            def responseCode = sampleReplacementService?.replaceSamples(entry.key, sampleIds)
            assert responseCode
            logger.info "Sending email notification for failed samples $sampleIds"
            sendEmail(entry.value as List, new SampleFailedReceiptEmailNotification())
        }
    }

    def updateRoutingRequests(SampleReceiptTableBean bean, SampleAnalyte sampleAnalyte, boolean testMode = false){
        PmoSample pmoSample = sampleAnalyte.claritySample
        boolean stopAtReceipt = pmoSample.udfStopAtReceipt
        if(bean.passed){
            if(stopAtReceipt != null && !stopAtReceipt.booleanValue()) {
                SequencingProject sequencingProject = pmoSample.sequencingProject
                String materialCategory = sequencingProject.udfMaterialCategory
                Stage stage = materialCategory == MaterialCategoryCv.DNA.materialCategory ? Stage.SAMPLE_QC_DNA : Stage.SAMPLE_QC_RNA
                logger.info "Route $sampleAnalyte to $stage"
                return makeRoutingRequest(sampleAnalyte, stage, testMode)
            }
        }
        else{
            logger.info "Unassigning failed samples from all active queues"
            def routingRequests = sampleAnalyte.unassignFromActiveStagesRequests
            if(routingRequests)
                process.routingRequests.addAll(routingRequests)
            return null
        }
    }

    RoutingRequest makeRoutingRequest(SampleAnalyte sampleAnalyte, Stage stage, boolean testMode){
        testMode?new RoutingRequest(routingUri: stage.value, artifactId: "sampleAnalyte.id", action: Routing.Action.assign) :
                new RoutingRequest(routingUri: stage.uri, artifactId: sampleAnalyte.id, action: Routing.Action.assign)
    }

    void updateStatusMap(SampleReceiptTableBean bean, PmoSample pmoSample){
        sampleReceivedStatusMap[pmoSample.pmoSampleId] = SampleStatusCv.SAMPLE_RECEIVED
        if(bean.passed){
            passedSamples << pmoSample.sampleAnalyte
            if(pmoSample.udfStopAtReceipt.booleanValue())
                sampleStatusCvMap[pmoSample.pmoSampleId] = SampleStatusCv.AVAILABLE_FOR_USE
            else
                sampleStatusCvMap[pmoSample.pmoSampleId] = SampleStatusCv.AWAITING_SAMPLE_QC
        }
        else {
            sampleStatusCvMap[pmoSample.pmoSampleId] = SampleStatusCv.ABANDONED
            failedSamples[bean.operatorId] << pmoSample.sampleAnalyte
        }
    }

    void sendEmail(List samples, EmailNotification emailNotification) {
        new ProcessUtility(this.process).sendEmailNotification(samples, emailNotification)
    }

    JSONObject getSowItems() {
        if(!sowItemData) {
            logger.info "Retrieving sow item data for $pmoSampleIds"
            ScheduledSampleService service = BeanUtil.getBean(ScheduledSampleService.class)
            sowItemData = service.getBatchSowItemMetadataForSample(pmoSampleIds)
            logger.info "Received sow item data $sowItemData"
        }
        return sowItemData
    }
}
