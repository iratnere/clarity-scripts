package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

class ProcessRecordDetails extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ProcessRecordDetails.class)
    MetabolomicsQCProcess qcProcess

    void execute() {
        logger.info "Starting ${this.class.simpleName} action...."
        qcProcess = (MetabolomicsQCProcess)process
        String errors = ""
        if(!isDataImported)
            qcProcess.importDataFromMetabolomicsWorksheet()
        else {
            qcProcess.outputAnalytes.each { SampleQcHamiltonAnalyte analyte ->
                if(!analyte.passedQc && !analyte.udfSampleQCFailureMode)
                    errors += "Sample QC Failure Mode is required for ${analyte.parentAnalyte.name}\n"
            }
            if(errors)
                qcProcess.postErrorMessage(errors)
            qcProcess.updateGroupQcResult()
        }
        qcProcess.setCompleteStage()
        String warnMessage = ""
        qcProcess.outputAnalytes.groupBy {it.claritySample.pmoSample.udfGroupName}.each { String groupName, List<SampleQcHamiltonAnalyte> groupAnalytes ->
            warnMessage += getWarning(groupName, groupAnalytes)
        }
        if(warnMessage)
            qcProcess.postWarningMessage(warnMessage)
    }

    String getWarning(String groupName, List<SampleQcHamiltonAnalyte> groupAnalytes){
        if(!groupAnalytes.find{it.passedQc})
            return "All samples in group $groupName [${groupAnalytes.collect {it.claritySample?.id}}] failed QC (Group Failure). \n"
        return ''
    }

    boolean getIsDataImported(List<Analyte> analytes = qcProcess.outputAnalytes) {
        Analyte analyte = analytes.find { SampleQcHamiltonAnalyte analyte ->
            !analyte.udfMetabolomicsSampleId
        }
        return analyte == null
    }
}
