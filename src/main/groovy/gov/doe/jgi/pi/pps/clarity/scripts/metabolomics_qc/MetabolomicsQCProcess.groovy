package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ClaritySampleAnalyteComparator
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.SIPUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleQcHamiltonAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleAliquotFailureModesService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

class MetabolomicsQCProcess extends ClarityProcess {
    static final logger = LoggerFactory.getLogger(MetabolomicsQCProcess.class)

    static ProcessType processType = ProcessType.SM_SAMPLE_METABOLOMICS_QC
    static final String METABOLOMICS_SHEET_TEMPLATE_NAME = 'resources/templates/excel/MetabolomicsSampleQC'
    static final String SCRIPT_GENERATED_ALIQUOT_SHEET = 'Download Aliquot Creation Worksheet'
    static final String SCRIPT_GENERATED_METABOLOMICS_SHEET = 'Download Metabolomics Worksheet'
    static final String UPLOADED_METABOLOMICS_SHEET = 'Upload Metabolomics Worksheet'
    protected final static Comparator COMPARATOR = new ClaritySampleAnalyteComparator()
    boolean testMode = false
    LibraryCreationQueueCv plateLcQueueCached
    ExcelWorkbook metabolomicsSheetCached

    MetabolomicsQCProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation': MetabolomicsQCPPV,
                'PlaceOutputs': PlaceOutputs,
                'PrepareWorksheets': PrepareAliquotCreationMetabolomicsSheets,
                'PrintLabels': PrintLabels,
                'ProcessWorksheets' : ProcessMetabolomicsSheet,
                'ProcessRecordDetails': ProcessRecordDetails,
                'RouteToFractionation': RouteToFractionation
        ]
        this.lastActionKey = 'RouteToFractionation'
    }

    List<Analyte> sortInputAnalytes (List<Analyte> analytes = inputAnalytes) {
        analytes.sort{a,b -> COMPARATOR.compare(a.claritySample.sampleAnalyte, b.claritySample.sampleAnalyte)}
        return analytes
    }

    ContainerNode getOutputPlateContainer() {
        ContainerNode outputContainerNode = outputAnalytes[0].containerNode
        if (outputContainerNode?.isNinetySixWellPlate)
            return outputContainerNode
        return null
    }

    LibraryCreationQueueCv getPlateLcQueue() {
        if (!outputPlateContainer) {
            return null
        }
        if (plateLcQueueCached) {
            return plateLcQueueCached
        }
        plateLcQueueCached = outputAnalytes[0].libraryCreationQueue
        plateLcQueueCached
    }

    BigDecimal getUdfReplicatesFailurePercentAllowed() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_REPLICATES_FAILURE_PERCENT_ALLOWED.udf)
    }

    BigDecimal getUdfMinimumIsotopeEnrichment() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_MINIMUM_ISOTOPE_ENRICHMENT.udf)
    }

    TableSection getMetabolomicsTableSection(){
        if(!metabolomicsSheetCached) {
            def fileNode = processNode.outputResultFiles?.find { it.name.contains(UPLOADED_METABOLOMICS_SHEET)}
            metabolomicsSheetCached = new ExcelWorkbook(fileNode.id)
            metabolomicsSheetCached.load()
        }
        return metabolomicsSheetCached.sections.values().find {it instanceof TableSection}
    }

    def getFreezerContainers(){
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        return freezerService.freezerLookupInputAnalytes(this)
    }

    def getStatusCv() {
        SampleAliquotFailureModesService sampleAliquotFailureModesService = BeanUtil.getBean(SampleAliquotFailureModesService.class)
        return sampleAliquotFailureModesService.dropDownPassFail
    }

    def getFailureModeCv() {
        SampleAliquotFailureModesService sampleAliquotFailureModesService = BeanUtil.getBean(SampleAliquotFailureModesService.class)
        return sampleAliquotFailureModesService.dropDownFailureModes
    }

    void importDataFromMetabolomicsWorksheet(Map<Long, Analyte> analytes = pmoSampleOutput, TableSection tableSection = metabolomicsTableSection, BigDecimal minimumIsotopeEnrichment = udfMinimumIsotopeEnrichment, BigDecimal replicatesFailurePercentAllowed = udfReplicatesFailurePercentAllowed) {
        tableSection.data.each { MetabolomicsTableBean bean ->
            Analyte analyte = analytes[bean.sampleId as Long]
            bean.updateUdfs(analyte, minimumIsotopeEnrichment)
        }
        updateGroupQcResult(analytes.values() as List, replicatesFailurePercentAllowed)
        outputAnalytes.each{it.artifactNode.httpPut()}
    }

    void updateGroupQcResult(List<Analyte> analytes = outputAnalytes, BigDecimal replicatesFailurePercentAllowed = udfReplicatesFailurePercentAllowed) {
        analytes.groupBy {it.claritySample?.pmoSample?.udfGroupName}.each { String groupName, List<SampleQcHamiltonAnalyte> groupSamples ->
            SIPUtility.updateGroupQcResult(groupSamples, replicatesFailurePercentAllowed, null)
            groupSamples.each { SampleQcHamiltonAnalyte analyte ->
                analyte.systemQcFlag = analyte.passedQc
                if(analyte.passedQc)
                    analyte.udfSampleQCFailureMode = ''
            }
        }
    }

    Map<Long, Analyte> getPmoSampleOutput(List<Analyte> analytes = outputAnalytes) {
        Map<Long, Analyte> pmoSampleOutput = [:]
        analytes.each{ Analyte analyte ->
            pmoSampleOutput[analyte.claritySample.pmoSample.pmoSampleId] = analyte
        }
        return pmoSampleOutput
    }
}
