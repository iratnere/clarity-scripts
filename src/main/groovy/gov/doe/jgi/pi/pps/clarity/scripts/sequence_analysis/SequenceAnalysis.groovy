package gov.doe.jgi.pi.pps.clarity.scripts.sequence_analysis

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by datjandra on 6/4/2015.
 */
class SequenceAnalysis extends ClarityProcess {

    static ProcessType processType = ProcessType.SQ_SEQUENCE_ANALYSIS

    boolean testMode = false

    SequenceAnalysis(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'EnterRecordDetails': SqAnalysisEnterRecordDetails,
                'ExitRecordDetails': SqAnalysisExitRecordDetails,
                'RouteToNextWorkflow': SqAnalysisRouteToWorkflow
        ]
    }
}
