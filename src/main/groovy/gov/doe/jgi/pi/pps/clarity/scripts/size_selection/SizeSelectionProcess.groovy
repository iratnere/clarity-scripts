package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by tlpaley on 3/25/16.
 */
class SizeSelectionProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.SIZE_SELECTION

    SizeSelectionProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':SizeSelectionPreProcessValidation,
            'Process Size Selection':SizeSelectionRecordDetails,
            'Route to Next Workflow':SizeSelectionRouteToWorkflow
        ]
    }

}