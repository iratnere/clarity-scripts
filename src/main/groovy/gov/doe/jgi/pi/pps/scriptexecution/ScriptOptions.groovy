package gov.doe.jgi.pi.pps.scriptexecution

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import groovy.transform.Canonical

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Canonical
@JsonDeserialize(using = CustomScriptOptionsDeserializer.class)
class ScriptOptions {

    @NotNull
    @Size(min = 6)
    String processId

    @NotNull
    @Size(min = 10)
    String action

    @NotNull
    @Size(min = 20)
    String stepUrl

    @NotNull
    @Size(min = 6)
    String user


    def fileIds

}
