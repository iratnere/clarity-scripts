package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AbandonWork extends ActionHandler {
    static final logger = LoggerFactory.getLogger(AbandonWork.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.setCompleteStage()
    }
    
}
