package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.SampleQcFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ClaritySampleAnalyteComparator
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.GenericSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SIPMetagenomicsSampleQC
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcUtility
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.LogTiming
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by dscott on 2/25/2015.
 */
class SampleQcProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.SM_SAMPLE_QC
    static final Logger logger = LoggerFactory.getLogger(SampleQcProcess.class)

    static final String UPLOAD_SAMPLE_QC_WORSHEET = 'Download SampleQC Worksheet'
    static final String DOWNLOAD_SAMPLE_QC_WORSHEET = 'Upload SampleQC Worksheet'
    static final String SAMPLE_QC_WORKSHEET_TEMPLATE = 'resources/templates/excel/SampleQCWorksheet.xls'
    boolean testMode=false
    List<Section> sampleQcSections = null
    SampleQcUtility sampleQcUtilityCached

    static final String[] getActiveQcTypes() {
        return QcTypeCv.findAllWhere(active: "Y")?.collect{ it.qcType }
    }

    static final String[] getActiveSampleQCFailureModes() {
        return SampleQcFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }

    static final DropDownList getDropDownQcTypes() {
        return new DropDownList(controlledVocabulary: activeQcTypes)
    }

    static final DropDownList getDropDownPassFail(){
        return new DropDownList(controlledVocabulary: Analyte.PASS_FAIL_LIST)
    }

    static final DropDownList getDropDownSampleQCFailureModes(){
        return new DropDownList(controlledVocabulary: activeSampleQCFailureModes)
    }

    static final DropDownList getDropDownHMWgDNA() {
        String[] controlledVocab = ['Y', 'N', 'Marginal','Unable to proceed']
        return new DropDownList(controlledVocabulary: controlledVocab)

    }

    SampleQcProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation':SampleQcPreProcessValidation,
                'PlaceOutputs': SampleQcPlaceOutputs,
                'ValidatePlacement':SampleQcValidatePlacements,
                'PrepareSampleQcSheet':PrepareSampleQcSheet,
                'ProcessSampleQcSheet': ProcessSampleQcSheet,
                'RouteToNextWorkflow': SampleQcRouteToWorkflow
        ]
    }

    SampleQcUtility getSampleQcUtility(){
        if(!sampleQcUtilityCached)
            sampleQcUtilityCached = new SampleQcUtility(inputAnalytes)
        return sampleQcUtilityCached
    }

    List<Section> getSampleQcWorksheetSections(){
        Long startTime = System.currentTimeMillis()
        if(!sampleQcSections){
            SampleQcWorksheet worksheet = new SampleQcWorksheet(this)
            ExcelWorkbook workbook = worksheet?.readSampleQcWorksheet()
            sampleQcSections = workbook.sections.values() as List
        }
        Long endTime = System.currentTimeMillis()
        LogTiming.logMethodCall(startTime,endTime,'SampleQcProcess.getSampleQcWorksheetSections')
        return sampleQcSections
    }

    List<SampleAnalyte> getSortedInputAnalytes(List<SampleAnalyte> sampleAnalytes = inputAnalytes){
        Collections.sort(sampleAnalytes, new ClaritySampleAnalyteComparator())
        return sampleAnalytes
    }

    //OPI-363 - if 1 or more sow items have ITag Primer set, then sample is considered as iTag
    boolean getIsITag(Long pmoSampleId){
        return sampleQcUtility.getIsITag(pmoSampleId)
    }

    boolean getIsSIPOriginalSample(Long pmoSampleId) {
        return sampleQcUtility.isSipOriginalSample(pmoSampleId)
    }

    List<SampleQcAdapter> getSampleQcAdapters(List<Analyte> analytes = inputAnalytes){
        List<Analyte> sipSamples = []
        List<Analyte> genericSamples = []
        analytes.each{ Analyte analyte ->
            if(sampleQcUtility.isSipSample(analyte.claritySample.pmoSample.pmoSampleId)){
                if(sampleQcUtility.isSipOriginalSample(analyte.claritySample.pmoSample.pmoSampleId)) {
                    logger.info "Found SIP Metagenomics original sample ${analyte.id}"
                    sipSamples << analyte
                }
                else {
                    logger.info "Found SIP Metagenomics fraction sample ${analyte.id}"
                    postErrorMessage("Invalid sample ${analyte.id}. Expecting SIP Original samples only")
                }
            }
            else
                genericSamples << analyte
        }
        List<SampleQcAdapter> adapters = []
        if(genericSamples) {
            adapters << new GenericSampleQC(sampleQcUtility, genericSamples)
        }
        if(sipSamples) {
            adapters << new SIPMetagenomicsSampleQC(sampleQcUtility, sipSamples)
        }
        logger.info "Made adapters $adapters for analytes $analytes"
        return adapters
    }

    ScheduledSampleService getScheduledSampleService(){
        return sampleQcUtility.scheduledSampleService
    }

    Map<Long, Integer> getSampleQcTypeIds(Long pmoSampleId){
        sampleQcUtility.getSampleQcTypeIds(pmoSampleId)
    }

    String getSMInstructions(Long pmoSampleId){
        sampleQcUtility.getSMInstructions(pmoSampleId)
    }

    List<String> getSowItemTypes(Long pmoSampleId){
        sampleQcUtility.getSowItemTypes(pmoSampleId)
    }

    TableSection getGlsStarletTableSection(){
        return sampleQcWorksheetSections.find{it instanceof TableSection}
    }

    List<KeyValueSection> getKeyValueSections(){
        return sampleQcWorksheetSections.findAll{it instanceof KeyValueSection}
    }

    BigDecimal getUdfReplicatesFailurePercentAllowed() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_REPLICATES_FAILURE_PERCENT_ALLOWED.udf)
    }

    BigDecimal getUdfControlsFailurePercentAllowed() {
        processNode.getUdfAsBigDecimal(ClarityUdf.PROCESS_CONTROLS_FAILURE_PERCENT_ALLOWED.udf)
    }
}
