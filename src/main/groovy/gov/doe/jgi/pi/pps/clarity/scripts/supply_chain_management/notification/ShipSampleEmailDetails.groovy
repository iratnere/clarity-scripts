package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.project.SequencingProject
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 9/9/16.
 */
class ShipSampleEmailDetails extends EmailDetails {
    def sampleFormat

    ShipSampleEmailDetails(Analyte analyte) {
        super(analyte)
        ClaritySample sample = analyte.claritySample
        SequencingProject sqProject = sample.sequencingProject
        if (!sqProject)
            throw new RuntimeException("Sequencing Project not found for sample id ${sample.id}")

        sampleFormat = sample.udfSampleFormat
    }

    @Override
    def getHashCode(){
        if(containerNode.containerTypeEnum == ContainerTypes.TUBE) {
            return new HashCodeBuilder(17, 37).
                    append(sampleContactId).
                    append(sequencingProjectManagerId).
                    toHashCode()
        }
        else{
            return new HashCodeBuilder(17, 37).
                    append(containerNode.id).
                    toHashCode()
        }
    }

    def getInfo() {
        return "$pmoSampleId,  $sampleLimsId,  $sampleName,  $barcode,  $containerName,  $plateLocation,  $sequencingProjectName,  $sampleFormat"
    }
}

