package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryBindingProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.REQUEUE_LIBRARY_BINDING
    static final logger = LoggerFactory.getLogger(RequeueLibraryBindingProcess.class)

    boolean testMode = false

    RequeueLibraryBindingProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':RequeueLibraryBindingPreProcessValidation,
            'Process Requeue':RequeueLibraryBinding,
            'Route to Next Workflow':RequeueLibraryBindingRouteToWorkflow
        ]
    }
}
