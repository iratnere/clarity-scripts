package gov.doe.jgi.pi.pps.clarity.cv

import gov.doe.jgi.pi.pps.util.util.EnumConverterCaseInsensitive

enum SowItemStatusCv {

	ABANDONED('Abandoned'),
	AWAITING_COLLABORATOR_METADATA('Awaiting Collaborator Metadata'),
	AWAITING_ELIGIBLE_SAMPLE('Awaiting Eligible Sample'),
	AWAITING_QA_QC_ANALYSIS('Awaiting QA/QC Analysis'),
	AWAITING_SAMPLE_QC_REVIEW('Awaiting Sample QC Review'),
	COMPLETE('Complete'),
	CREATED('Created'),
	DELETED('Deleted'),
	IN_PROGRESS('In Progress'),
	NEEDS_ATTENTION('Needs Attention'),
	ON_HOLD('On Hold'),
	SAMPLE_REVIEW('Sample Review')
	
	final String value

	private static final EnumConverterCaseInsensitive<SowItemStatusCv> converter = new EnumConverterCaseInsensitive(SowItemStatusCv)

	private SowItemStatusCv(String value) {
		this.value = value
	}

	String toString() {
		return value
	}
			
	static SowItemStatusCv toEnum(value) {
		return converter.toEnum(value)
	}
}
	