package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

/**
 * Created by lvishwas on 10/14/16.
 */
class SampleAliquotsMigrator extends Migrator{

    static Map<ClarityUdf, Object> defaultUdfs

    SampleAliquotsMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager, new ScheduledSamplesMigrator(clarityNodeManager))
    }

    static{
        defaultUdfs = [:].withDefault {[:]}
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_SOW_QC_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_LAB_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_FINAL_ALIQUOT_MASS_NG] = 100
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_FINAL_ALIQUOT_VOLUME_UL] = 50
        defaultUdfs[Migrator.SELF_CONTAINER_UDFS][ClarityUdf.CONTAINER_LAB_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.SELF_CONTAINER_UDFS][ClarityUdf.CONTAINER_FINAL_ALIQUOT_MASS_NG] = 100
        defaultUdfs[Migrator.SELF_CONTAINER_UDFS][ClarityUdf.CONTAINER_FINAL_ALIQUOT_VOLUME_UL] = 50
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerType) {
        return Stage.ALIQUOT_CREATION_DNA
    }

    @Override
    void checkExistingAnalytes(List<OnboardingBean> beans) {
        beans.each { OnboardingBean bean ->
            Analyte existingAnalyte = lookupSAByPmoSampleId(clarityNodeManager, bean.pmoSample.pmoSample.pmoSampleId.toString(), bean.getSowId())
            if (existingAnalyte)
                bean.addAnalyte(existingAnalyte)
        }
    }

    String getDestinationStage(Analyte analyte){
        LibraryCreationQueueCv queue = analyte.claritySample.libraryCreationQueue
        if(queue){
            String workflowName = ClaritySampleAliquot.LIBRARY_CREATION_WORKFLOW_PREFIX + queue?.libraryCreationQueue
            WorkflowNode workflowNode = clarityNodeManager.getWorkflowNodeByName(workflowName)
            return workflowNode.stages?.first().uri
        }
        return null
    }

    Map getDefaultUdfs(){
        this.defaultUdfs
    }

    void updateUdfs(List<OnboardingBean> beans){
        if(!beans)
            return
        def defaultUdfs = this.defaultUdfs
        if(defaultUdfs)
            setDefaultUdfs(beans.collect{getAnalyte(it)}, this.defaultUdfs[SELF_UDFS], this.defaultUdfs[SELF_CONTAINER_UDFS])
        beans.each{
            it.updateUdfs(getAnalyteType())
            ClaritySampleAliquot sampleAliquot = AnalyteFactory.analyteInstance(it.getAnalyteForType(analyteType))
            sampleAliquot.udfVolumeUl = it.pmoSample.udfVolumeUl
            sampleAliquot.udfConcentrationNgUl = it.pmoSample.udfConcentration
        }

    }

    @Override
    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans) {
        if(beans.find{!it.getSowId()}) {
            checkExistingAnalytes(beans)
        }
        return beans.findAll{!it.sampleAliquot}
    }

    ContainerTypes getContainerType(Analyte parent){
        return parent.containerNode.containerTypeEnum
    }

    Analyte getAnalyte(OnboardingBean bean){
        return bean.sampleAliquot
    }

    @Override
    Class getAnalyteType() {
        return ClaritySampleAliquot.class
    }
}
