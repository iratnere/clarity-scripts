package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management.notification.ShipSampleEmailNotification
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 1/17/15.
 */
class RouteToSampleReceipt extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(RouteToSampleReceipt.class)

    void execute(){
        logger.info "Starting ${this.class.name} action...."
        performLastStepActions()
        sendEmailNotification()
    }

    Map<Long, SampleStatusCv> getSampleStatusMap(List<Analyte> inputAnalytes = process.inputAnalytes){
        Map<Long, SampleStatusCv> sampleStatusMap = [:]
        inputAnalytes.each { SampleAnalyte analyte ->
            //2. Update sample status to "Awaiting Sample Receipt"
            def sampleId = analyte.claritySample?.pmoSampleId
            sampleStatusMap[sampleId] = SampleStatusCv.AWAITING_SAMPLE_RECEIPT
        }
        return sampleStatusMap
    }

    void performLastStepActions(){
        updateSamplesStatus(sampleStatusMap)
        process.moveInputsToStage(Stage.SAMPLE_RECEIPT)
    }

    void updateSamplesStatus(Map<Long, SampleStatusCv> sampleStatusMap){
        StatusService service = BeanUtil.getBean(StatusService.class)
        service.submitSampleStatus(sampleStatusMap, process.researcherContactId)
    }

    void sendEmailNotification(List<Analyte> inputAnalytes = process.inputAnalytes){
        //3.Send notification email , group by sample contact ( one email per sample contact)
        //Changes to input analytes move to RouteToSampleReceipt
        //Notification emails are to be sent grouping by the sample contact
        //PPS-3164 moving email notification from ApproveForShipping
        new ProcessUtility(this.process).sendEmailNotification(inputAnalytes, new ShipSampleEmailNotification())
    }
}
