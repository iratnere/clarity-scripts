package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import groovy.transform.ToString

/**
 * Created by datjandra on 5/21/2015.
 */
@ToString(includeNames=true, includeFields=true)
class MiSeqReagentSheetHeaderBean {

    @FieldMapping(header='Investigator Name', cellType = CellTypeEnum.STRING)
    public String investigatorName

    @FieldMapping(header='Project Name', cellType = CellTypeEnum.STRING)
    public String projectName

    @FieldMapping(header='Experiment Name', cellType = CellTypeEnum.STRING)
    public String experimentName

    @FieldMapping(header='Date', cellType = CellTypeEnum.STRING)
    public String date

}
