package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc


import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SowQcAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.notification.SowFailedQcEmailNotification
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/26/15.
 */
class SowItemQcRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SowItemQcRouteToWorkflow.class)

    List<Analyte> failedScheduledSamples = []

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        performLastStepActions()
        if(failedScheduledSamples)
            sendNotificationEmails(failedScheduledSamples)
    }

    void sendNotificationEmails(List<Analyte> failedAnalytes){
        if(!failedAnalytes)
            return
        new ProcessUtility(this.process).sendEmailNotification(failedAnalytes, new SowFailedQcEmailNotification())
    }

    void performLastStepActions(Map<Integer, List<Section>> sections = ((SowItemQc)process).sowQcSections){
        sections.each {Integer sheetIndex, List<Section> sheetSections ->
            updateUdfs(sheetIndex, sheetSections)
        }
        SowItemQc sowItemQc = (SowItemQc)process
        List<SowQcAdapter> adapters = sowItemQc.sowQcAdapters
        assert adapters
        RecordDetailsInfo recordDetailsInfo = new RecordDetailsInfo(sowItemQc)
        adapters.each { SowQcAdapter adapter ->
            process.routingRequests.addAll(adapter.getRoutingRequests(recordDetailsInfo))
            failedScheduledSamples.addAll(adapter.getFailedScheduledSamples(recordDetailsInfo))
        }
    }

    void updateUdfs(int sheetIndex, List<Section> sheetSections){
        if(!process.tubeScheduledSamples && !process.plateSamples)
            process.sortInputAnalytes()
        if(sheetIndex == 0)
            updateTubeScheduledSampleUdfs(sheetSections.first().data)
        else{
            String sheetName = sheetSections.first().parentSheet.sheetName
            Map<PmoSample,List<ScheduledSample>> sampleScheduledSamples = process.plateSamples[sheetName]
            int scheduledSamplesPerSample = sampleScheduledSamples.values().first().size()
            int i=0
            while(i < scheduledSamplesPerSample){
                List<ScheduledSample> scheduledSamples = sampleScheduledSamples.values().collect{it[i]}
                updatePlateScheduledSampleUdfs(scheduledSamples, [sheetSections[0], sheetSections[1], sheetSections[7+3*i], sheetSections[8+3*i]])
                i++
            }
        }
    }

    void updatePlateScheduledSampleUdfs(List<ScheduledSample> scheduledSamples, List<Section> sections){
        def plateSowQCBean = sections[0].data
        def lastKVBean = sections[2].data
        scheduledSamples.each{ ScheduledSample scheduledSample ->
            plateSowQCBean.updateUdfs(scheduledSample)
            lastKVBean.updateUdfs(scheduledSample)
        }
    }

    void updateTubeScheduledSampleUdfs(def beans){
        beans.each{bean ->
            bean.updateUdfs(process.tubeScheduledSamples.find{it.claritySample.sowItemId == bean.sowId})
        }
    }
}
