package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.FileNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.ActionType
import gov.doe.jgi.pi.pps.clarity_node_manager.node.actions.NextAction
import gov.doe.jgi.pi.pps.util.exception.WebException

/**
 * Created by datjandra on 9/16/2015.
 */
class PbProcessRunDesign extends ActionHandler {

    void setNextActions(){
        Collection<NextAction> artifactActions = processNode.outputAnalytes.collect{ ArtifactNode outputArtifact ->
            outputArtifact.nextAction(ActionType.REPEAT)
        }
        process.setArtifactActions(artifactActions)
    }

    void execute() {
        if (process.isPacBioSequel) {
            String runName = PbPrepareRunDesign.runName((process as PacBioSequencingPlateCreation).sequencerName, extractRunNumberFromContainerName())
            validateSequelRunSheet(runName)
        } else {
            throw new WebException("no handler for input stage ${process.inputStage} of process ${process}",422)
        }
        setNextActions()
    }

    FileNode getFileNode(String placeHolder) {
        FileNode fileNode = process.getFileNode(placeHolder)?.fileNode
        if (!fileNode) {
            process.postErrorMessage("${placeHolder} was not attached")
        }
        fileNode
    }

    void validateSequelRunSheet(String runName) {
        PacBioSequencingPlateCreation clarityProcess = process as PacBioSequencingPlateCreation
        String generatedCsvString = PbPrepareRunDesign.createLibraryContents(clarityProcess.getPbSequelLibraryBeans(runName))
        String downloadedCsvString = "${getFileNode(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7).download()}".toString()
        if (generatedCsvString != downloadedCsvString) {
            process.postErrorMessage("Attached combined run design file is not consistent with latest UDF values: please generate the file again.")
        }
    }

    String extractRunNumberFromContainerName() {
        ContainerNode containerNode = (process as PacBioSequencingPlateCreation).outputContainerNode
        String runNumber = PbPrepareRunDesign.containerNameToRunNumber(containerNode.name)
        runNumber
    }
}
