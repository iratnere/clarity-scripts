package gov.doe.jgi.pi.pps.clarity.model.sample

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.util.exception.BundleAwareException
import gov.doe.jgi.pi.pps.util.util.BeanUtil

/**
 * Created by duncanscott on 4/7/15.
 */
class PmoSample extends ClaritySample {

    static final String CONTAINER_LOCATION_ON_SITE = 'On Site'
    static final String CONTAINER_LOCATION_NONE = 'None'

    PmoSample(SampleNode sampleNode) {
        super(sampleNode)
    }

    PmoSample getPmoSample() {
        return this
    }

    public BigDecimal getMass() {
        return (udfVolumeUl != null && udfConcentration != null) ? udfVolumeUl * udfConcentration : null
    }

    List<ScheduledSample> getScheduledSamples(NodeManager nodeManager = BeanUtil.nodeManager) {
        //SampleIdIterator(NodeConfig nodeConfig, Collection<SampleParameterValue> parameters)
        SampleParameterValue sampleLimsIdUdf = SampleParameter.UDF.setValue(sampleNode.id).setUdf(ClarityUdf.SAMPLE_LIMSID.value)
        SampleIdIterator sampleIdIterator = new SampleIdIterator(sampleNode.nodeManager.nodeConfig, [sampleLimsIdUdf])
        List<String> sampleIds = []
        while (sampleIdIterator.hasNext()) {
            sampleIds << sampleIdIterator.next()
        }
        List<ScheduledSample> scheduledSamples = []
        SampleFactory.getSamples(sampleIds, nodeManager).values().each { ClaritySample claritySample ->
            if (claritySample instanceof ScheduledSample) {
                scheduledSamples << (ScheduledSample) claritySample
            } else {
                throw new BundleAwareException([code:'sample.scheduledSampleExpected', args:[claritySample]])
            }
        }
        return scheduledSamples
    }

    Long getPmoSampleId(){
        String sampleName = sampleNode.name
        if (sampleName.isLong()) {
            return sampleName.toLong()
        }
        return null
    }

    String toString() {
        "${PmoSample.class.simpleName}[limsid:${id},pmoId:${pmoSampleId}]"
    }

    String getUdfSampleFormat(){
        sampleNode.getUdfAsString(ClarityUdf.SAMPLE_FORMAT.udf)
    }

    String getPmoSampleIdAsString(){
        pmoSampleId?.toString()
    }

    public BigDecimal getUdfCollaboratorVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_COLLABORATOR_VOLUME_UL.udf)
    }

    public void setUdfCollaboratorVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_COLLABORATOR_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfConcentration() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_CONCENTRATION_NG_UL.udf)
    }

    public void setUdfConcentration(BigDecimal concentration) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_CONCENTRATION_NG_UL.udf, concentration)
    }

    public BigDecimal getUdfInitialVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_INITIAL_VOLUME_UL.udf)
    }

    public void setUdfInitialVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_INITIAL_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfGelDiluteVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_GEL_DILUTE_VOLUME_UL.udf)
    }

    public void setUdfGelDiluteVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_GEL_DILUTE_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfPurityVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_PURITY_VOLUME_UL.udf)
    }

    public void setUdfPurityVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_PURITY_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfQualityVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_QUALITY_VOLUME_UL.udf)
    }

    public void setUdfQualityVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QUALITY_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfQuantityVolumeUl() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_QUANTITY_VOLUME_UL.udf)
    }

    public void setUdfQualityScore(String qualityScore) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QUALITY_SCORE.udf, qualityScore)
    }

    public String getUdfQualityScore() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_QUALITY_SCORE.udf)
    }

    public void setUdfQuantityVolumeUl(BigDecimal volume) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QUANTITY_VOLUME_UL.udf, volume)
    }

    public BigDecimal getUdfAbsorbanceA260A230() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ABSORBANCE_260_230.udf)
    }

    public void setUdfAbsorbanceA260A230(BigDecimal a260a230) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_ABSORBANCE_260_230.udf, a260a230)
    }

    public BigDecimal getUdfAbsorbanceA260A280() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ABSORBANCE_260_280.udf)
    }

    public void setUdfAbsorbanceA260A280(BigDecimal a260a280) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_ABSORBANCE_260_280.udf, a260a280)
    }

    public String getUdfCollaboratorSampleName() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_COLLABORATOR_SAMPLE_NAME.udf)
    }

    public void setUdfCollaboratorSampleName(String concentration) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_COLLABORATOR_SAMPLE_NAME.udf, concentration)
    }

    public BigDecimal getUdfCollaboratorConcentration() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_COLLABORATOR_CONCENTRATION_NGUL.udf)
    }

    public void setUdfCollaboratorConcentration(BigDecimal concentration) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_COLLABORATOR_CONCENTRATION_NGUL.udf, concentration)
    }

    String getUdfBiosafetyMaterialCategory() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_BIOSAFETY_MATERIAL_CATEGORY.udf)
    }

    void setUdfBiosafetyMaterialCategory(String biosafetyMaterialCategory) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_BIOSAFETY_MATERIAL_CATEGORY.udf, biosafetyMaterialCategory)
    }

    BigDecimal getUdfEstimatedGenomeSizeMb() {
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ESTIMATED_GENOME_SIZE_MB.udf)
    }

    void setUdfEstimatedGenomeSizeMb(BigDecimal genomeSizeMb) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_ESTIMATED_GENOME_SIZE_MB.udf, genomeSizeMb)
    }

    String getUdfSampleReceiptNotes() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_RECEIPT_NOTES.udf)
    }

    void setUdfSampleReceiptNotes(String sampleReceiptNotes) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_RECEIPT_NOTES.udf, sampleReceiptNotes)
    }

    String getUdfSampleHMWgDNAYN() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_HMW_GDNA_Y_N.udf)
    }

    void setUdfSampleHMWgDNAYN(String sampleHMWgDNAYN) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_HMW_GDNA_Y_N.udf, sampleHMWgDNAYN)
    }

    String getUdfSampleReceiptDate() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_RECEIPT_DATE.udf)
    }

    void setUdfSampleReceiptDate(def sampleReceiptDate) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_RECEIPT_DATE.udf, sampleReceiptDate)
    }

    String getUdfSampleReceiptMissingDryIce() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_RECEIPT_MISSING_DRY_ICE.udf)
    }

    void setUdfSampleReceiptMissingDryIce(String dryIce) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_RECEIPT_MISSING_DRY_ICE.udf, dryIce)
    }

    String getUdfSampleReceiptResult() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_RECEIPT_RESULT.udf)
    }

    void setUdfSampleReceiptResult(String result) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_RECEIPT_RESULT.udf, result)
    }

    String getUdfSampleQcInsrument() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_QC_INSTRUMENT.udf)
    }

    void setUdfSampleQcInsrument(String sampleQcInstrument) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QC_INSTRUMENT.udf, sampleQcInstrument)
    }

    Boolean getUdfStopAtReceipt() {
        return sampleNode.getUdfAsBoolean(ClarityUdf.SAMPLE_STOP_AT_RECEIPT.udf)
    }

    void setUdfStopAtReceipt(Boolean stopAtReceipt) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_STOP_AT_RECEIPT.udf, stopAtReceipt)
    }

    String getUdfSampleQCResult() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_QC_RESULT.udf)
    }

    void setUdfSampleQCResult(String result) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QC_RESULT.udf, result)
    }

    String getUdfSampleQCFailureMode() {
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_QC_FAILURE_MODE.udf)
    }

    void setUdfSampleQCFailureMode(String result) {
        sampleNode.setUdf(ClarityUdf.SAMPLE_QC_FAILURE_MODE.udf, result)
    }

    void setUdfSampleQcDate(def sampleQcDate){
        sampleNode.setUdf(ClarityUdf.SAMPLE_QC_DATE.udf, sampleQcDate)
    }

    Date getUdfSampleQcDate(){
        sampleNode.getUdfAsDate(ClarityUdf.SAMPLE_QC_DATE.udf)
    }

    BigInteger getUdfCumulativeQcTypeId(){
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_CUMULATIVE_QC_TYPE_ID.udf)
    }

    void setUdfCumulativeQcTypeId(String cumulativeQcType){
        sampleNode.setUdf(ClarityUdf.SAMPLE_CUMULATIVE_QC_TYPE_ID.udf, cumulativeQcType)
    }

    String getUdfCurrentQcType(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_CURRENT_QC_TYPE.udf)
    }

    void setUdfCurrentQcType(String currentQcType){
        sampleNode.setUdf(ClarityUdf.SAMPLE_CURRENT_QC_TYPE.udf, currentQcType)
    }

    BigDecimal getUdfRRnaRatio(){
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_RRNA_RATIO.udf)
    }

    void setUdfRRnaRatio(BigDecimal rRnaRatio){
        sampleNode.setUdf(ClarityUdf.SAMPLE_RRNA_RATIO.udf, rRnaRatio)
    }

    //PPS-4399 - Udf "Qc Type Id" can only have whole numbers 0,1,2,.... as values. Hence changing the return type to BigInteger from BigDecimal
    BigInteger getUdfQcTypeId(){
        return sampleNode.getUdfAsBigInteger(ClarityUdf.SAMPLE_QC_TYPE_ID.udf)
    }

    void setUdfQcTypeId(BigDecimal qcTypeId){
        sampleNode.setUdf(ClarityUdf.SAMPLE_QC_TYPE_ID.udf, qcTypeId as BigInteger)
    }

    String getUdfContainerSampleQcResult(){
        return sampleNode.artifactNode.containerNode.getUdfAsString(ClarityUdf.CONTAINER_SAMPLE_QC_RESULT.udf)
    }

    void setUdfContainerSampleQcResult(String sampleQcStatus){
        sampleNode.artifactNode.containerNode.setUdf(ClarityUdf.CONTAINER_SAMPLE_QC_RESULT.udf, sampleQcStatus)
    }

    String getUdfContainerLocation(){
        return sampleNode.artifactNode.containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION.udf)
    }

    void setUdfContainerLocation(String location){
        sampleNode.artifactNode.containerNode.setUdf(ClarityUdf.CONTAINER_LOCATION.udf, location)
    }

    String getUdfContainerLocationDate(){
        return sampleNode.artifactNode.containerNode.getUdfAsString(ClarityUdf.CONTAINER_LOCATION_DATE.udf)
    }

    void setUdfContainerLocationDate(String sampleQcStatus){
        sampleNode.artifactNode.containerNode.setUdf(ClarityUdf.CONTAINER_LOCATION_DATE.udf, sampleQcStatus)
    }

    String getUdfSampleQcNotes(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_QC_NOTES.udf)
    }

    String setUdfSampleQcNotes(String sampleQcNotes){
        return sampleNode.setUdf(ClarityUdf.SAMPLE_QC_NOTES.udf, sampleQcNotes)
    }

    String getUdfGroupName(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_GROUP_NAME.udf)
    }

    void setUdfGroupName(String sipGroupName){
        sampleNode.setUdf(ClarityUdf.SAMPLE_GROUP_NAME.udf, sipGroupName)
    }

    BigDecimal getUdfIsotopeEnrichment(){
        return sampleNode.getUdfAsBigDecimal(ClarityUdf.SAMPLE_ISOTOPE_ENRICHMENT.udf)
    }

    void setUdfIsotopeEnrichment(BigDecimal isotopeEnrichment){
        sampleNode.setUdf(ClarityUdf.SAMPLE_ISOTOPE_ENRICHMENT.udf, isotopeEnrichment)
    }

    void setUdfIsotopeLabel(String isotopeLabel){
        sampleNode.setUdf(ClarityUdf.SAMPLE_ISOTOPE_LABEL.udf, isotopeLabel)
    }

    String getUdfIsotopeLabel(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_ISOTOPE_LABEL.udf)
    }

    void setUdfMetabolomicsSampleId(String isotopeLabel){
        sampleNode.setUdf(ClarityUdf.SAMPLE_METABOLOMICS_SAMPLE_ID.udf, isotopeLabel)
    }

    String getUdfMetabolomicsSampleId(){
        return sampleNode.getUdfAsString(ClarityUdf.SAMPLE_METABOLOMICS_SAMPLE_ID.udf)
    }

    String getSMInstructionsFromScheduledSamples(NodeManager nodeManager){
        Set<String> uniqueSmInstructions = getScheduledSamples(nodeManager).collect{it.udfSmInstructions?.trim()}
        return uniqueSmInstructions?'[' + uniqueSmInstructions?.join('][') + ']':''
    }

    QcTypeCv calculateRequiredQcType(List<Integer> sowItemsQcTypeIds){
        //Required Qc Type = ((“all sow items qc type id”  | “cumulative qc type id” ) ^ “cumulative qc type id”) | (sample udf “QC Type Id”? sample udf “QC Type Id”:0)
        int sowItemsQcTypeId = 0
        sowItemsQcTypeIds?.each { qcTypeId  ->
            if (qcTypeId) {
                sowItemsQcTypeId |= qcTypeId as int
            }
        }
        int cumulativeQcTypeId = udfCumulativeQcTypeId?:0
        Long idValue  = (((sowItemsQcTypeId|cumulativeQcTypeId)^cumulativeQcTypeId) | (udfQcTypeId?udfQcTypeId:0)) as Long
        return QcTypeCv.get(idValue)
    }

    boolean getIsControlSample() {
        return udfIsotopeLabel == 'Unlabeled'
    }

    String getPmoSampleArtifactLimsId() {
        this.sampleAnalyte.id
    }

    void passOnGroupFailure() {
        if(udfSampleQCResult == Analyte.PASS) {
            udfSampleQCResult = Analyte.FAIL
            udfContainerSampleQcResult = Analyte.FAIL
            udfSampleQCFailureMode = GROUP_FAILURE
        }
    }

    boolean getPassedQc() {
        return udfSampleQCResult == Analyte.PASS
    }
}

