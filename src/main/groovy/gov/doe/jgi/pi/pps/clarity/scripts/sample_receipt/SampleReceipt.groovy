package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.excel.SampleReceiptTableBean
import gov.doe.jgi.pi.pps.clarity.util.ClarityStringUtils
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.SampleIdIterator
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameter
import gov.doe.jgi.pi.pps.clarity_node_manager.iterator.parameter.SampleParameterValue
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/22/2018.
 */
class SampleReceipt extends ClarityProcess {

    static ProcessType processType = ProcessType.RECEIVE_SAMPLES
    static final Logger logger = LoggerFactory.getLogger(SampleReceipt.class)

    public static final String BARCODE_ASSOCIATION_FILE = 'BarcodeAssociation.xls'
    List<SampleReceiptTableBean> beans
    ExcelWorkbook workbook

    SampleReceipt(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':SampleReceiptPPV,
            'ValidateBarcodeAssociation':ValidateBarcodeAssociation,
            'RouteReceivedSamples':ReceiveSamples
        ]
    }

    String getBarcodeFileArtifactId(ProcessNode processNode = this.processNode){
        logger.info "Extracting artifact id for $BARCODE_ASSOCIATION_FILE."
        def fileNode = processNode.outputResultFiles?.find { it.name.contains(BARCODE_ASSOCIATION_FILE)}
        if(!fileNode)
            throw new RuntimeException("Barcode file not found")
        logger.info "$BARCODE_ASSOCIATION_FILE file artifact id: ${fileNode.id}"
        return fileNode.id
    }

    ExcelWorkbook getBarcodeAssociation(String artifactId=barcodeFileArtifactId){
        if(!workbook){
            workbook = new ExcelWorkbook(artifactId)
        }
        return workbook
    }

    List<SampleReceiptTableBean> getBarcodeTableBeans(String artifactId=barcodeFileArtifactId){
        if(!beans) {
            ExcelWorkbook workbook = barcodeAssociation
            TableSection section = tableSection
            logger.info "Reading section: $section"
            workbook.addSection(section)
            workbook.load()
            beans = section.data
            logger.info "Found ${beans.size()} rows in the section $section"
        }
        return beans
    }

    static Section getTableSection(){
        return new TableSection(0, SampleReceiptTableBean.class, 'A1', null, true)
    }

    ContainerNode findContainerByPmoSampleId(String pmoSampleId, NodeManager nodeManager) {
        Collection<SampleParameterValue> parameterValues = [
                SampleParameter.NAME.setValue(pmoSampleId)
        ]
        logger.info "Looking up container for Pmo sample: $pmoSampleId"
        SampleIdIterator sampleIdIterator = new SampleIdIterator(nodeManager.nodeConfig, parameterValues)
        if (sampleIdIterator.hasNext()) {
            SampleNode sampleNode = nodeManager.getSampleNode(sampleIdIterator.next())
            ContainerNode containerNode = sampleNode.artifactNode.containerNode
            if (containerNode.contentsIds.size() == 1) {
                return containerNode
            }
            logger.info "PmoSample $pmoSampleId is not in tube."
        }
        return null
    }

    boolean updateBeanContainer(SampleReceiptTableBean bean){
        if(bean.containerNode)
            return true
        if(!bean.jgiBarcode)
            return false
        bean.jgiBarcode = ClarityStringUtils.extendedTrim(bean.jgiBarcode)
        String errors = ''
        ContainerNode containerNode = findContainerByPmoSampleId(bean.jgiBarcode, nodeManager)
        if (!containerNode) {
            logger.info "${bean.jgiBarcode} is not PMO sample id."
            try {
                //Sample Receipt for samples on a Plate
                containerNode = nodeManager.getContainerNode(bean.jgiBarcode)
            } catch (Exception e) {
                logger.info "${bean.jgiBarcode} is not Container Lims Id."
            }
        }
        if (!containerNode)
            containerNode = getContainerNodeByName(bean.jgiBarcode)
        if (!containerNode) {
            //Container not found in Clarity
            errors += "Sample not found by container limsid or name for JGI barcode ${bean.jgiBarcode}\n"
        }
        if(errors)
            postErrorMessage(errors)
        bean.containerNode = containerNode
        return true
    }

    ContainerNode getContainerNodeByName(String containerName){
        logger.info "Looking up container for container name : $containerName"
        List<ContainerNode> containerNodes = nodeManager.getContainerNodesByName(containerName)
        if (containerNodes) {
            if (containerNodes.size() == 1) {
                logger.info "Found container $containerName"
                return containerNodes[0]
            } else {
                //Sample Receipt already run on this Sample container
                postErrorMessage("Container not uniquely defined by name [$containerName].\n")
            }
        }
    }
}
