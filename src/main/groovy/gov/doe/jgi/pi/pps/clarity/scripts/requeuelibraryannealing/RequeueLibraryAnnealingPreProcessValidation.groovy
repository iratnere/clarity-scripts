package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibraryannealing

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryAnnealingPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryAnnealingPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        Integer cleanupCount = 0
        Boolean magBeadCleanup = Boolean.FALSE
        process.inputAnalytes.each { Analyte analyte ->
            if (!(analyte instanceof ClarityLibraryStock) && !(analyte instanceof ClarityLibraryPool)) {
                process.postErrorMessage("$analyte must be a Library stock or a Library pool")
            }

            if (analyte.containerNode.isPlate) {
                process.postErrorMessage("$analyte must be on a tube")
            }

            if (!analyte.sequencerModelCv.isPacBio) {
                process.postErrorMessage("Inputs must be PacBio library stocks")
            }

            if (analyte.udfMagBeadCleanupDate) {
                magBeadCleanup = Boolean.TRUE
                cleanupCount++
            }
        }

        if (magBeadCleanup) {
            process.postWarningMessage("${ProcessType.PACBIO_MAGBEAD_CLEANUP.value} was run on $cleanupCount inputs")
        }
    }
}
