package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.SeqTableBean

class NextSeq implements SequencerType{

    Sequencing process

    NextSeq(Sequencing process) {
        this.process = process
    }

    @Override
    void validateInputIsLibrary() {
        process.inputAnalytes*.validateIsLibraryInput() //Input must be library stock, pool, pool of pools
    }

    @Override
    void validateNumberInputs() {
        if (process.inputAnalytes.size() > 1)
            process.postErrorMessage("only one input is accepted for ${process.sequencerModelCv.sequencerModel}")
    }

    @Override
    void validateInputFlowcellType() {}

    @Override
    String getTemplateName() {
        return 'resources/templates/excel/ClarityNextSeq.xls'
    }

    @Override
    void validateOutputContainerType() {

    }

    @Override
    void updateWorkbookStyle(ExcelWorkbook excelWorkbook) {}

    @Override
    int getActiveTabIndex() {
        return 0
    }

    @Override
    def getTableBean(ClarityPhysicalRunUnit outputAnalyte, List<FreezerContainer> freezerContainers) {
        Analyte inputAnalyte = outputAnalyte.parentAnalyte
        SeqTableBean tableBean = new SeqTableBean()
        tableBean.freezerPath = process.getFreezerLocation(freezerContainers, inputAnalyte)
        tableBean.libraryStockName = inputAnalyte.name
        tableBean.containerBarcode = inputAnalyte.containerName
        tableBean.runType = inputAnalyte.udfRunMode
        tableBean.materialType = outputAnalyte.claritySample.sequencingProject.udfMaterialCategory
        tableBean.concentrationPm = inputAnalyte.udfLibraryMolarityPm
        tableBean
    }

    @Override
    String getEndAddress() {
        return null
    }

    @Override
    String getStartAddress() {
        return 'A1'
    }

    @Override
    void uploadSampleSheet() {}

    @Override
    boolean isValidateLane() {
        false
    }

    @Override
    void routeToNextWorkflow() {}

    @Override
    List<Analyte> getRepeatOutputAnalytes() {
        return process.failedQcAnalytes
    }

    def getDefaultPhixSpikeIn(){
        return 1
    }

    def getDefaultLibConvFactor(){
        return 1.6
    }

    void setDefaultDnaWa01Ul(def bean){
//     =IF(conc pM>=1000,20/qPCR conc nM,((1300*(1-(0.01*spike-in %)))*lib conv factor)/conc pM)
        if(bean.concentrationPm >= 1000)
            bean.dnaWa01Ul = 20/(bean.concentrationPm/1000)
        else
            bean.dnaWa01Ul = ((1300*(1-(0.01*bean.phixSpikeIn)))*bean.libraryConvFactor)/bean.concentrationPm
    }
}
