package gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class FractionalLanePooling implements gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling{
    int startPoolNumber
    List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members
    List<Analyte> sagPools

    static final Logger logger = LoggerFactory.getLogger(FractionalLanePooling.class)

    FractionalLanePooling(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members){
        this.members = members
        sagPools = members.findAll { it.isSAGPool }
    }

    @Override
    void makePools() {
        logger.debug("Starting PoolingNovaSeq makePools")
        def poolNumberToSum = [:]
        members.sort{-it.libraryPercentage}.eachWithIndex{ gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation candidateBean, index ->
            if (index == 0) {
                candidateBean.poolNumber = startPoolNumber
                poolNumberToSum[startPoolNumber] = candidateBean.libraryPercentage as BigDecimal
            }
            else {
                int candidateNumber = findPoolNumber(members, poolNumberToSum, candidateBean) as int
                candidateBean.poolNumber = candidateNumber
                poolNumberToSum[candidateNumber] = getSum(members, candidateNumber)
            }
        }
        startPoolNumber = members.collect{ it.poolNumber }.max() + 1
        logger.debug("PoolingNovaSeq makePools completed: nextPoolNumber $startPoolNumber")
    }

    static def findPoolNumber(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members, def poolNumberToSum, gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation candidateBean) {
        BigInteger pool = poolNumberToSum.find{number,sum ->
            isPoolMember(members, number, candidateBean)
        }?.key
        if (pool != null) {
            return pool
        }
        return (poolNumberToSum.keySet().max() + 1)
    }

    static boolean isPoolMember(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members, def poolNumber, gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation candidateBean) {
        BigDecimal threshold = members[0].threshold ? members[0].threshold : gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling.DEFAULT_THRESHOLD
        BigDecimal libraryPercentage = candidateBean.libraryPercentage as BigDecimal
        return ((getSum(members, poolNumber) + libraryPercentage) <= threshold
                && isDistinctIndex(members, poolNumber, candidateBean.indexSequences)
                    && !has2SagPools(members, poolNumber, candidateBean)
        )
    }

    static BigDecimal getSum(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members, def poolNumber) {
        def sum = members.findAll{ it.poolNumber == poolNumber }*.libraryPercentage.sum()
        return sum ? sum : 0
    }

    static boolean isDistinctIndex(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members, def poolNumber, def indexSequences) {
        def indexes = []
        members.findAll { it.poolNumber == poolNumber }.each { gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation member ->
            member.indexSequences.each { String sequence ->
                indexes.addAll(ArtifactIndexService.splitSequences(sequence))
            }
        }
        return !(indexSequences.intersect(indexes))
    }

    static boolean has2SagPools(List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> members, int poolNumber, gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation candidateBean) {
        //PPS-5253 - 2 SAG Pools should not be pooled together while pooling Internal single cell pools with NovaSeq
        if(!candidateBean.isSAGPool)
            return false
        List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation> existingSagPools = members.findAll { it.poolNumber == poolNumber && it.isSAGPool }
        if(existingSagPools)
            return true
        return false
    }



    @Override
    int completePoolPrepBeans(int poolNumber) {
        startPoolNumber = poolNumber
        makePools()
        return startPoolNumber
    }

    Map<String, List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation>> getIndexMembersMap(){
        Map<String, List<gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation>> indexMembers = [:].withDefault {[]}
        members.each{ gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation li ->
            def indexToCompare = getIndexToCompare(li)
            if(indexToCompare instanceof String)
                indexToCompare = [indexToCompare]
            indexToCompare.each { String index ->
                ArtifactIndexService.splitSequences(index).each {
                    indexMembers[it] << li
                }
            }
        }
        return indexMembers
    }
}