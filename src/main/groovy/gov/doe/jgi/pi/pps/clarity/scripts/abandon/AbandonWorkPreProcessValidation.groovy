package gov.doe.jgi.pi.pps.clarity.scripts.abandon

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AbandonWorkPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(AbandonWorkPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        validateInputClass()
    }

    void validateInputClass(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { analyte ->
            analyte.validateIsScheduledSample()
        }
    }

}
