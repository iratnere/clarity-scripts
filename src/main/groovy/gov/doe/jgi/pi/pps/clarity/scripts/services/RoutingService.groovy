package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest

interface RoutingService {
    boolean submitRoutingRequests(Collection<RoutingRequest> routingRequests)
}