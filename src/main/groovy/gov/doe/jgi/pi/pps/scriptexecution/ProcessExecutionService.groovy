package gov.doe.jgi.pi.pps.scriptexecution

interface ProcessExecutionService {
    ScriptResponseModel processScriptParams(ScriptParams scriptParams)
}