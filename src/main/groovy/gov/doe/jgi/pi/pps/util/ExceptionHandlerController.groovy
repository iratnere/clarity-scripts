package gov.doe.jgi.pi.pps.util

import gov.doe.jgi.pi.pps.clarity_node_manager.util.ClarityNotAvailabeException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.TypeMismatchException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.reactive.function.client.WebClientResponseException

import javax.servlet.http.HttpServletRequest
import java.lang.reflect.UndeclaredThrowableException

@ControllerAdvice
class ExceptionHandlerController {

    static final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class)

    @ExceptionHandler
    ResponseEntity<String> handle(final HttpServletRequest request, PpsException ex) {
        String method = ((HttpServletRequest)request).method
        String message = ex.message
        if (method == 'GET') {
            String url = (((HttpServletRequest) request).requestURI).trim()
            try {
                Collection<PpsError> errors = ex.errors
                PpsError error = errors[0]
                message = error.message
            } catch (Exception e) {
                //fall to a default
            }
            log.error("error accessing $url:  $message", ex)
            return new ResponseEntity<String>(message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        new ResponseEntity<String>(ex.message, HttpStatus.UNPROCESSABLE_ENTITY)
    }

    //WebClientResponseException$NotFound

    @ExceptionHandler
    ResponseEntity<String> handle(WebClientResponseException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, ex.statusCode)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(ClarityEntityNotFoundException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler
    ResponseEntity<Object> handle(TypeMismatchException ex) {
        String msg = ex.cause?.message ?: ex.message
        log.error(msg, ex)
        def matcher = msg =~ /: (.+)$/
        if (matcher) {
            def spId = matcher[0][1]
            msg = "Invalid sequencing project id [$spId]"
        }
        new ResponseEntity<String>(msg, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(PlussEntityNotFoundException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(ClarityNotAvailabeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.SERVICE_UNAVAILABLE)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(UndeclaredThrowableException ex) {
        Throwable throwable = ex.cause
        if (!throwable)
            throw ex
        String message = "${throwable.cause}" ?: "'$throwable.message'"
        log.error(message, throwable)
        new ResponseEntity<String>(message, HttpStatus.SERVICE_UNAVAILABLE)
    }

}
