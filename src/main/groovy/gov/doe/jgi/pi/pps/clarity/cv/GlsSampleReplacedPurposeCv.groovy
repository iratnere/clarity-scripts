package gov.doe.jgi.pi.pps.clarity.cv

/**
 * Created by datjandra on 4/21/2015.
 */
enum GlsSampleReplacedPurposeCv {
    SHIP_SAMPLE,
    SUBMIT_STERILITY_DOCUMENT,
    SAMPLE_FAILED_IN_RECEIPT,
    SAMPLE_FAILED_IN_QC,
    SUBMIT_SAMPLE_METADATA,
    SAMPLE_FAILED_IN_STERILITY_DOCUMENT_REVIEW
}
