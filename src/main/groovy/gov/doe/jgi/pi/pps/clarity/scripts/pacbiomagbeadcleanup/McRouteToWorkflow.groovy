package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactNodeService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/30/2015.
 */
class McRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(McRouteToWorkflow.class)
    List data

    void performLastStepActions(){
        Map<String, Analyte> mappedLibraryStocks = new HashMap<String, Analyte>()
        process.inputAnalytes.each { Analyte inputAnalyte ->
            mappedLibraryStocks.put(inputAnalyte.id, inputAnalyte)
        }
        data.each { MagBeadCleanupTableBean tableBean ->
            BigDecimal newVolumeUl = tableBean.newVolumeUl
            if (newVolumeUl == null)
                process.postErrorMessage('new volume on the sheet must be filled for all libraries')
            BigDecimal newConcentrationNgUl = tableBean.newConcentrationNgUl
            if (newConcentrationNgUl == null)
                process.postErrorMessage('new concentration on the sheet must be filled for all libraries')
            Analyte library = mappedLibraryStocks.get(tableBean.pacBioLibrary)
            logger.info "Updating $library volume to $newVolumeUl"
            library.setUdfVolumeUl(newVolumeUl)
            logger.info "Updating $library concentration to $newConcentrationNgUl"
            library.setUdfConcentrationNgUl(newConcentrationNgUl)
        }
        Set<String> repeatIds = new HashSet<String>()
        List<ArtifactNodeInterface> inputArtifacts = new ArrayList<ArtifactNodeInterface>()
        ArtifactNodeService artifactNodeService = BeanUtil.getBean(ArtifactNodeService.class)
        process.inputAnalytes.each { Analyte inputAnalyte ->
            repeatIds.add(inputAnalyte.id)
            inputArtifacts.add(inputAnalyte.artifactNode)
            List<String> descendants = artifactNodeService.getDescendantList(inputAnalyte.id)
            descendants.each {
                Analyte childAnalyte = AnalyteFactory.analyteInstance(processNode.nodeManager.getArtifactNode(it))
                if (childAnalyte instanceof PacBioAnnealingComplex || childAnalyte instanceof PacBioBindingComplex)
                    process.removeAnalyteFromActiveWorkflows(childAnalyte)
                 else
                    logger.warn "no handler for analyte class ${childAnalyte.class.simpleName}, skip remove from active workflows"
            }
            Date dateRun = processNode.dateRunAsDate ?: new Date()
            inputAnalyte.setUdfMagBeadCleanupDate(dateRun)
        }
        if (process.isPacBio) {
            if (process.isPacBioSequel) {
                process.completeRepeat(repeatIds)
                process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING,process.inputAnalytes)
            } else {
                throw new WebException("no handler for process pacbio input stage [${process.inputStage}]",422)
            }
        } else {
            throw new WebException("no handler for process input stage [${process.inputStage}]",422)
        }
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ArtifactNode resultFile = process.getFileNode(MagbeadCleanup.UPLOAD_MAGBEAD_CLEANUP)
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(resultFile.id, process.testMode)
        Section tableSection = new TableSection(0, MagBeadCleanupTableBean.class, 'A2')
        excelWorkbook.addSection(tableSection)
        excelWorkbook.load()
        data = excelWorkbook.sections.values()[0].getData()
        performLastStepActions()
    }
}
