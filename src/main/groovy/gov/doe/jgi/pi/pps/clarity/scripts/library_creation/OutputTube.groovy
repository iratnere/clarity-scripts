package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.Index
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.apache.commons.lang3.NotImplementedException

/**
 * Created by tlpaley on 1/3/16.
 */
class OutputTube implements OutputType {

    final static String TEMPLATE_NAME = 'resources/templates/excel/LcTube'
    final static String TABLE_CLASS_NAME = LcTubeBean.class.simpleName
    LibraryCreationProcess process

    OutputTube(LibraryCreationProcess process) {
        this.process = process
    }

    @Override
    void validate() {
        process.validateOutputContainerType()
    }

    @Override
    void update() {
    }

    @Override
    void print() {
        process.writeLabels(process.outputAnalytes.sort{it.name})
    }

    @Override
    void removeCorners(List beansList) {
    }

    @Override
    void transferBeansDataToClarityLibraryStocks() {
        throw new NotImplementedException("method transferBeansDataToClarityLibraryStocks() not implemented for ${this.class.simpleName}")
    }

    @Override
    void moveOutputToNextWorkflow(List<ClarityLibraryStock> analytes) {
        List<Analyte> passedLibraryStocks = []
        List<Analyte> failedLibraryStocks = []
        analytes.each { ClarityLibraryStock clarityLibraryStock ->
            def libraryQcResult = clarityLibraryStock.udfLibraryQcResult
            if (!libraryQcResult) {
                new WebException("Library Stock($clarityLibraryStock.id): invalid Library QC Result '$libraryQcResult'", 422)
            }
            if (libraryQcResult == Analyte.PASS) {
                passedLibraryStocks << clarityLibraryStock
            }
            if (libraryQcResult == Analyte.FAIL) {
                failedLibraryStocks << clarityLibraryStock
            }
        }
        process.routeAnalytes(process.lcAdapter.defaultStage, passedLibraryStocks)
        process.processCompleteFailure(failedLibraryStocks)
    }

    ExcelWorkbook populateLibraryCreationSheet(Section tableSectionLibraries){
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(TEMPLATE_NAME, process.testMode)
        excelWorkbook.addSection(tableSectionLibraries)
        return excelWorkbook
    }

    @Override
    void updateLibraryIndexUdf(List<ClarityLibraryStock> libraryStocks){
        List indexContainerBarcodes = process.getIndexContainerBarcodes(libraryStocks)
        ArtifactIndexService artifactIndexService = gov.doe.jgi.pi.pps.util.util.BeanUtil.getBean(ArtifactIndexService.class)
        Map<String, List<Index>> barcodeToIndexes = artifactIndexService.getIndexes(indexContainerBarcodes, false)
        libraryStocks.each{ libraryStock ->
            Index index = findIndex(libraryStock.udfIndexContainerBarcode, barcodeToIndexes)
            libraryStock.udfIndexName = index?.indexName
        }
        validateLibraryIndexUdfBatch(libraryStocks)
    }

    static final def BARCODE_REG_EX = /([0-9]*)([A-Z][0-9]*)-([0-9]*)-([0-9]*)/

    static final def getBarcodeRegExGroup(String indexContainerBarcode){
        def group = (indexContainerBarcode =~ BARCODE_REG_EX)
        if (!group || !group[0])
            return null
        return group[0]
    }

    static final def getBarcodeKey(String indexContainerBarcode){
        return indexContainerBarcode
    }

    static final def getIndexSet(String indexContainerBarcode){
        def group = getBarcodeRegExGroup(indexContainerBarcode)//[01A01-04182018-02, 01, A01, 04182018, 02]
        return group[1]
    }

    static final def getLocation(String indexContainerBarcode){
        def group = getBarcodeRegExGroup(indexContainerBarcode)//[01A01-04182018-02, 01, A01, 04182018, 02]
        def location = group[2] //A01 or H11
        def match = (location =~ /([A-Z])([0-9]*)/)
        if (match && match[0])
            return "${match[0][1]}:${match[0][2]}".replace('0','')
        return null
    }

    static Index findIndex(String indexContainerBarcode, Map<String, List<Index>> barcodeToIndexes){
        if (!barcodeToIndexes || !indexContainerBarcode)
            return null
        Index index = barcodeToIndexes[indexContainerBarcode]?.find{it.indexSet == indexContainerBarcode}
        if (index)
            return index
        def indexSet = getIndexSet(indexContainerBarcode)
        return barcodeToIndexes[indexContainerBarcode]?.find{it.indexSet == indexSet}
    }

    static def validateLibraryIndexUdfBatch(List<ClarityLibraryStock> libraryStocks) {
        def errorMsg = StringBuilder.newInstance()
        libraryStocks.each {
            if (it.udfLibraryQcResult == Analyte.PASS && !it.udfIndexName){
                errorMsg << "Library Stock ($it.name): Invalid ${ClarityUdf.ANALYTE_INDEX_CONTAINER_BARCODE.udf} '$it.udfIndexContainerBarcode'"
                errorMsg << "/${ClarityUdf.ANALYTE_INDEX_NAME.udf} '$it.udfIndexName'"
                errorMsg << ClarityProcess.WINDOWS_NEWLINE
            }
        }
        if (errorMsg?.length()) {
            throw new IllegalArgumentException(errorMsg as String)
        }
    }

}
