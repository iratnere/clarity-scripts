package gov.doe.jgi.pi.pps.scriptexecution

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

//https://www.baeldung.com/jackson-object-mapper-tutorial
class CustomScriptOptionsDeserializer extends StdDeserializer<ScriptOptions> {
    public CustomScriptOptionsDeserializer() {
        this(null);
    }

    public CustomScriptOptionsDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ScriptOptions deserialize(JsonParser parser, DeserializationContext deserializer) {
        ScriptOptions scriptOptions = new ScriptOptions();
        ObjectCodec codec = parser.getCodec();
        JsonNode node = codec.readTree(parser);

        // try catch block
        JsonNode pNode = node.get("P");
        scriptOptions.with {
            processId = node.get("P").asText();
            action = node.get("A").asText();
            stepUrl = node.get("S").asText();
            user = node.get("U").asText();
            fileIds = node.get("L");
        }
        scriptOptions
    }

}
