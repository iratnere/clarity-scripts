package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96Iterator
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.PlacementsNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacement
import gov.doe.jgi.pi.pps.clarity_node_manager.node.placements.OutputPlacements
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Action handler meant to place sorted input samples on the Sample QC output plate
 * Sorting rules:
 * first by source containers; all tubes at the top followed by each plate
 * within each plate, sort the wells in column order: A1, B1,C1...H1, A2, B2,

 * Created by tlpaley on 2/26/15.
 */
class SampleQcPlaceOutputs extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SampleQcPlaceOutputs.class)
    List<String> outputPlates = null
    Map<Analyte, String> inputOutput = null
    int index = -1
    static final int MAX_PER_PLATE = 92

    String getEmptyOutputPlate(){
        index++
        if(outputPlates?.size() == 7)
            process.postErrorMessage("Cannot create more output plates.")
        if(!outputPlates){
            outputPlates = []
            PlacementsNode placementsNode = processNode.placementsNode
            List<String> containerIds = placementsNode?.selectedContainerIds
            if(containerIds) {
                outputPlates << containerIds[0]
            }
            logger.info "Using default plate ${outputPlates[index]} to place outputs."
        }
        if(!outputPlates || index == outputPlates.size()) {
            ContainerNode newPlate = nodeManager.createContainer(ContainerTypes.WELL_PLATE_96)
            outputPlates << newPlate.id
            logger.info "Default plate not empty. Created new container ${outputPlates[index]} to place outputs."
        }
        return outputPlates[index]
    }

    String getOutputArtifactId(SampleAnalyte sampleAnalyte){
        if(inputOutput)
            return inputOutput[sampleAnalyte]
        List<String> outputArtifactIds = processNode.getOutputAnalyteIds(sampleAnalyte.id)
        if (!outputArtifactIds) {
            process.postErrorMessage("No outputs for input analyte ${sampleAnalyte.id}")
        }
        if (outputArtifactIds.size() > 1) {
            process.postErrorMessage("More than one output for input analyte ${sampleAnalyte.id}")
        }
        return outputArtifactIds[0]
    }

    List<OutputPlacement> buildOutputPlacements(String containerId, List<String> outputArtifactIds){
        List<OutputPlacement> ops = []
        Iterator<WellAddress96> wellIterator = new WellAddress96Iterator(skipCorners:true, columnsFirst:true)
        outputArtifactIds.each{String outputArtifactId ->
            WellAddress96 wellAddress = wellIterator.next()
            ContainerLocation containerLocation = new ContainerLocation(containerId, wellAddress.clarityLocation)
            OutputPlacement outputPlacement = new OutputPlacement()
            outputPlacement.containerLocation = containerLocation
            outputPlacement.artifactId = outputArtifactId
            logger.info "Placing $outputArtifactId at location $containerLocation"
            ops << outputPlacement
        }
        return ops
    }

    OutputPlacements getOutputPlacements(List<SampleAnalyte> analytes = process.sortedInputAnalytes) {
        OutputPlacements outputPlacements = new OutputPlacements()
        List<String> batch = []
        int i = 0
        while(i < analytes.size()){
            int plateSize = analytes[i].containerNode.contentsIds.size()
            if((batch.size() + plateSize) <= MAX_PER_PLATE){
                for(int j=0;j<plateSize;j++){
                    batch << getOutputArtifactId(analytes[i])
                    i++
                }
            }
            else{
                logger.info "Placing $batch on 1 output plate."
                outputPlacements.addAll(buildOutputPlacements(emptyOutputPlate, batch))
                batch.clear()
            }
        }
        if(batch) {
            logger.info "Placing $batch on 1 output plate."
            outputPlacements.addAll(buildOutputPlacements(emptyOutputPlate, batch))
        }
        return outputPlacements
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        inputOutput = null
        processNode.placementsNode?.setOutputPlacements(outputPlacements)
        logger.debug "placements node after modification:\n${processNode.placementsNode?.nodeString}"
        processNode.placementsNode?.httpPost()
    }
}
