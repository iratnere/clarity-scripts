package gov.doe.jgi.pi.pps.util
/**
 * Created by iratnere on 11/7/14.
 */
class ClarityEntityNotFoundException extends RuntimeException {
    ClarityEntityNotFoundException(String message) {
        super(message)
    }
}
