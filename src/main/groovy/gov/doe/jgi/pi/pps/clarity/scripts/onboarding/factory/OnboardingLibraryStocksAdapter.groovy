package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.PlatformTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.LibraryStocksMigrator
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.Migrator
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

class OnboardingLibraryStocksAdapter implements OnboardingAdapter {
    static final logger = LoggerFactory.getLogger(OnboardingLibraryStocksAdapter.class)
    ExcelWorkbook onboardingWorkbook
    List<OnboardingBean> onboardingBeansCached
    Long researcherContactId
    @Delegate
    OnboardingBeansParser parser
    List<String> runModeCvs

    NodeManager getNodeManager() {
        return BeanUtil.nodeManager
    }

    OnboardingLibraryStocksAdapter(ExcelWorkbook onboardingWorkbook, Long researcherId) {
        this.onboardingWorkbook = onboardingWorkbook
        this.researcherContactId = researcherId
        parser = new OnboardingBeansParser()
    }

    List<OnboardingBean> getOnboardingBeans(){
        if(!onboardingBeansCached) {
            Section section = onboardingWorkbook.sections.values().find { it.beanClass == OnboardingBean.class }
            onboardingBeansCached = section.data
        }
        parser = new OnboardingBeansParser()
        return onboardingBeansCached
    }

    @Override
    List<String> validateOnboardingData() {
        List<String> errors = []
        if(onboardingBeans.find {!it.labLibraryName})
            errors << "'Lab Library Name' is required for all libraries."
        if(!errors) {
            errors = checkMissingFields(onboardingBeans)
            errors.addAll(validateBeansWithUSS(onboardingBeans))
            if(!errors)
                errors = validateBeansWithClarity(onboardingBeans)
        }
        if(!errors)
            errors.addAll(validate(onboardingBeans))
        if(!errors) {
            onboardingBeans.each { OnboardingBean bean ->
                if(bean.libraryStock) {
                    def libActiveQueues = bean.libraryStock?.activeWorkflows
                    if (!libActiveQueues?.intersect(WORKFLOWS_FOR_VALIDATION))
                        errors << "Library stock not queued in any of the workflows $WORKFLOWS_FOR_VALIDATION. Check ${bean.labLibraryName}."
                }
            }
        }
        return errors
    }

    @Override
    void mergeDataToClarity(NodeManager nodeManager = BeanUtil.nodeManager) {
        onboardingBeans.each {
            it.contactId = researcherContactId
            prepareUdfsMap(it)
        }
        prepareBeansForOnboarding(onboardingBeans)
        Migrator migrator = new LibraryStocksMigrator(nodeManager)
        migrator.createAnalytesInClarity(onboardingBeans, true)
        Migrator.submitRoutingRequests()
        nodeManager.httpPutDirtyNodes()
        ScheduledSampleService service = BeanUtil.getBean(ScheduledSampleService.class)
        service.editSow(researcherContactId, onboardingBeans.collect{it.scheduledSample})
    }

    @Override
    void archiveOnboardingResult(String artifactId, NodeManager nodeManager = BeanUtil.nodeManager){
        int sheetIndex = getOnboardingDetailsSheetIndex(onboardingWorkbook)
        onboardingWorkbook.addSection(prepareOnboardingDetails(onboardingBeans, sheetIndex))
        onboardingWorkbook.store(nodeManager.nodeConfig, artifactId, false)
    }

    @Override
    List<Analyte> getAnalytes() {
        onboardingBeans.collect{it.analytes}.flatten()
    }

    @Override
    String getAction() {
        return "Onboarding Libraries"
    }

    List<String> checkMissingFields(List<OnboardingBean> beans) {
        List<String> errors = []
        for(OnboardingBean bean : beans) {
            if (!bean.sampleName)
                errors << "'sample name' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.sowRunMode)
                errors << "'SOW run mode' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.dop)
                errors << "'Degree of Pooling' is required for all libraries and it must be >= 1. Check ${bean.labLibraryName}."
            if (!bean.aliquotAmount)
                errors << "'Aliquot amount (ng)' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.aliquotVolume)
                errors << "'Aliquot Volume' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.pcrCycles)
                errors << "'PCR Cycles' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libFragmentSize)
                errors << "'Library Fragment Size w/ adaptors (bp)' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libVolume)
                errors << "'Library Stock volume (ul)' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libMolarityPm)
                errors << "'Library Molarity (pM)' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libConcentration)
                errors << "'Library concentration (ng/ul)' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libraryCreationSite)
                errors << "'Library Creation Site' is required for all libraries. Check ${bean.labLibraryName}."
            if (!bean.libraryProtocol)
                errors << "'library protocol' is required for all libraries. Check ${bean.labLibraryName}."
        }
        return errors
    }

    List<String> validateBeansWithUSS(List<OnboardingBean> beans, ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService.class)) {
        List<String> errors = []
        List<String> indexNames = []
        for(OnboardingBean bean : beans) {
            if(!isRunModeValid(bean.sowRunMode))
                errors << "'SOW run mode' is not valid. Check ${bean.labLibraryName}."
            if(!isPacBio(bean.sowRunMode) && !bean.indexName)
                errors << "Index name field is required. Check ${bean.labLibraryName}."
            if(bean.indexName)
                indexNames << bean.indexName
        }
        if(indexNames) {
            Map<String, String> indexSequences = artifactIndexService.getSequences(indexNames)
            for(OnboardingBean bean : beans) {
                String sequence = indexSequences[bean.indexName]
                if(!sequence)
                    errors << "The index name ${bean.indexName} is not registered with clarity. Check ${bean.labLibraryName}."
            }
        }
        return errors
    }

    List<String> validateBeansWithClarity(List<OnboardingBean> beans, NodeManager nodeManager = this.nodeManager) {
        List<String> errors = lookUpLibraries(beans, nodeManager)
        List<OnboardingBean> unMergedBeans = beans.findAll { !it.pmoSample }
        errors.addAll(lookUpSamples(unMergedBeans, nodeManager))
        if(!errors)
            nodeManager.httpPutNodesBatch(beans.collect{it.pmoSample.sampleNode})
        return errors
    }

    @Override
    void prepareUdfsMap(OnboardingBean bean) {
        bean.udfMaps[PmoSample.class][ClarityUdf.SAMPLE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_EXTERNAL] = OnboardingAdapter.EXTERNAL_UDF_VALUE
            //PPS-3652 Do not set external udf on library stock
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_DEGREE_OF_POOLING] = bean.dop
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_RUN_MODE] = bean.sowRunMode
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_VOLUME_UL] = bean.aliquotVolume
        if (bean.aliquotVolume > 0)
            bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = bean.aliquotAmount / bean.aliquotVolume
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_NUMBER_PCR_CYCLES] = bean.pcrCycles
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP] = bean.libFragmentSize
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_VOLUME_UL] = bean.libVolume
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM] = bean.libMolarityPm
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM] = bean.libMolarityPm
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = bean.libConcentration
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_NOTES] = "Library protocol: ${bean.libraryProtocol}"
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME] = bean.labLibraryName
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_INDEX_NAME] = bean.indexName?:''
    }

    boolean isRunModeValid(String runMode) {
        if(!runModeCvs) {
            runModeCvs = RunModeCv.findAllByActive("Y")?.collect {it.runMode}
        }
        return runModeCvs.contains(runMode)
    }

    boolean isPacBio(String runMode) {
        return runMode.contains(PlatformTypeCv.PACBIO.value)
    }
}
