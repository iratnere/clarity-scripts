package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteSorter
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class LcPrepareLibraryCreationSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LcPrepareLibraryCreationSheet.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        doTubesPlacement()
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()

        clarityProcess.lcAdapter.validateOutputContainers()
        //assign library names to the library stock analytes, sort libraries by ITS sample id or container location
        LibraryNameReservationService libraryNameReservationService = BeanUtil.getBean(LibraryNameReservationService.class)
        libraryNameReservationService.assignLibraryNames(AnalyteSorter.sort(clarityProcess.outputAnalytes))
        clarityProcess.lcAdapter.updateOutputContainers()
        //generate library creation worksheet
        uploadLibraryCreationWorksheet()
    }

    void uploadLibraryCreationWorksheet() {
        ArtifactNode fileNode = process.getFileNode(LibraryCreationProcess.SCRIPT_GENERATED_LIBRARY_CREATION_SHEET)
        logger.info "Uploading script generated file ${fileNode.id}..."
        ExcelWorkbook libraryCreationWorkbook = (process as LibraryCreationProcess).lcAdapter.populateLibraryCreationSheet()
        libraryCreationWorkbook?.store(process.nodeManager.nodeConfig, fileNode.id)
    }
}