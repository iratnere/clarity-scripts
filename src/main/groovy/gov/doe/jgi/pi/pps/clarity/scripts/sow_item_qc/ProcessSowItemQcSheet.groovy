package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SowQcAdapter
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/26/15.
 */
class ProcessSowItemQcSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(ProcessSowItemQcSheet.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        SowItemQc sowQcProcess = (SowItemQc) process
        List<SowQcAdapter> adapters = sowQcProcess.sowQcAdapters
        assert adapters
        adapters.each { SowQcAdapter adapter ->
            adapter.checkProcessUdfs(sowQcProcess)
        }
        sowQcProcess.sowQcSections
        sowQcProcess.setCompleteStage()
    }
}