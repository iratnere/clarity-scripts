package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import groovy.json.JsonBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.SQLException

@Service
class ScheduledSampleService {

    Logger logger = LoggerFactory.getLogger(ScheduledSampleService.class)

    @Value("\${scheduledSample.url}")
    private String scheduledSampleUrl

    @Value("\${sowItems.url}")
    private String sowItemsUrl

    @Value("\${addSowItem.url}")
    private String addSowItemUrl

    @Value("\${sowItemsEdit.url}")
    private String sowItemsEditUrl

    @Autowired
    JdbcTemplate jdbcTemplate

    @Autowired
    DataSource dataSource

    def createScheduledSamplesInClarity(List<Long> sowItemIds, Long contactId) {
        def sowItemSampleMap = null
        if (!sowItemIds)
            return sowItemSampleMap
        logger.info "${scheduledSampleUrl}"
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'sow-item-ids'(sowItemIds)
            'submitted-by'(contactId)
        }
        logger.info "${jsonRequest.toPrettyString()}"
        HTTPBuilder http = new HTTPBuilder(scheduledSampleUrl)
        http.request(Method.POST, ContentType.JSON) { req ->
            body = jsonRequest.toPrettyString()
            response.success = { resp, reader ->
                sowItemSampleMap = reader
            }
            response.failure = { resp ->
                throw new RuntimeException("${resp.status} : ${resp.statusLine.reasonPhrase}")
            }
        }
        return sowItemSampleMap
    }

    def getSowItemMetadataForSample(Long pmoSampleId, Boolean returnPeu = false, Boolean validateQueue = false) {
        getBatchSowItemMetadataForSample([pmoSampleId], returnPeu, validateQueue)
    }

    JSONObject getBatchSowItemMetadataForSample(Collection sampleIds, Boolean returnPeu = false, Boolean validateQueue = false) {
        def jsonRequest = new JsonBuilder()
        jsonRequest {
            'sample-ids'(sampleIds)
            'return-peu'(returnPeu)
            'validate-queue'(validateQueue)
        }

        JSONObject sowItemMap = new JSONObject()
        HTTPBuilder http = new HTTPBuilder(sowItemsUrl)
        http.request(Method.POST, ContentType.JSON) { req ->
            body = jsonRequest.toPrettyString()
            response.success = { resp, reader ->
                logger.info "web-service=sow-items status=success resp=${resp}:\n${reader}"
                JSONArray samples = JSONArray.fromObject(reader as List)//slurper.parse(reader)//new JSONArray(reader)
                samples?.each { def sample ->
                    String sampleIdString = sample?.'sample-id' as String
                    if (!sampleIdString?.isLong()) {
                        return
                    }
//                    JSONArray sowItemList = new JSONArray()
//                    sample.'sow-items'?.each { sowItem ->
//                        if (sowItem) {
//                            sowItemList.add(new JSONObject(sowItem))
//                        }
//                    }
//                    sowItemMap[sampleIdString.toLong()] = sowItemList
                    if (sample.'sow-items')
                        sowItemMap[sampleIdString.toLong()] = sample.'sow-items'
                }
            }
            response.failure = { resp, reader ->
                logger.error "web-service=sow-items status=failure resp=${resp}:\n${reader}"
                throw new RuntimeException("Errors: ${resp.status} : ${reader}")
            }
        }
        logger.info "service=${ScheduledSampleService.class.simpleName} returning:\n${sowItemMap.toString(2)}"
        sowItemMap
    }

    def getAllDescendantIds(List<String> scheduledSampleAnalyteIds) {
        String query = """select art2.luid as ss, art.luid as artLimsId
from CLARITYLIMS.ARTIFACT_ANCESTOR_MAP artmap, claritylims.artifact art, claritylims.artifact art2,
claritylims.artifact_sample_map amap
where artmap.ANCESTORARTIFACTID = art2.artifactid
and amap.ARTIFACTID=art2.artifactid
and artmap.artifactid=art.artifactid
and art.artifacttypeid=2
and art2.artifacttypeid=2
and art2.luid in ('${scheduledSampleAnalyteIds.join("','")}')
order by artLimsId"""
        jdbcTemplate = new JdbcTemplate(dataSource)
        return jdbcTemplate.query(query, new ResultSetExtractor<Object>() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                def obj = [:].withDefault { [] }
                while (rs.next()) {
                    obj[rs.getString('ss')] << rs.getString('artLimsId')
                }
                return obj
            }
        })
    }

    Map addSowItem(def submissionJson) {
        Map sampleSow = [:]
        JSONArray samples = JSONArray.fromObject(postToSowService(addSowItemUrl, submissionJson))
        samples?.each { def sample ->
            sampleSow[sample?.'sample-id'] = sample?.'sow-item-id'
        }
        return sampleSow
    }

    void editSow(Long submittedBy, List<ScheduledSample> scheduledSamples) {
        def ssGroups = scheduledSamples.groupBy { it.udfDegreeOfPooling + it.udfRunMode }
        ssGroups.values().each { List<ScheduledSample> ssg ->
            def jsonRequest = new JsonBuilder()
            jsonRequest {
                "submitted-by"(submittedBy)
                "sow-items-edit" {
                    "sow-item-ids"(ssg.collect { it.sowItemId })
                    'degree-of-pooling'(ssg[0].udfDegreeOfPooling)
                    'run-mode'(ssg[0].udfRunMode)
                }
            }
            postToSowService(sowItemsEditUrl, jsonRequest.toPrettyString())
        }
    }

    def postToSowService(String url, String jsonSubmission) {
        HTTPBuilder http = new HTTPBuilder(url)
        http.request(Method.POST, ContentType.JSON) { req ->
            body = jsonSubmission
            response.success = { resp, reader ->
                return reader
            }
            response.failure = { resp, reader ->
                throw new RuntimeException("Errors: ${resp.status} : ${reader}")
            }
        }
    }
}
