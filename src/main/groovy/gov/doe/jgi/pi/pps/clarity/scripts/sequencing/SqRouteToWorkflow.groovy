package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.PostProcessService
import gov.doe.jgi.pi.pps.util.util.BeanUtil

/**
 * Created by datjandra on 4/23/2015.
 * Clarity Illumina MiSeq, NextSeq, HiSeq Support Requirements
 * https://docs.google.com/document/d/15g6IcLuFzb-1Q8LtFCdfukaT2f2l0m32j9b8yo3_51Q/edit#
 * Clarity Illumina NovaSeq Support Requirements
 * https://docs.google.com/document/d/1CmDCgNkQwyHQRqk4Gx3LgeWzNYiPuJ6X9X6BKPffq9w/edit
 * Clarity Illumina HiSeq 2000, 1T Requirements
 * https://docs.google.com/document/d/1DhaLc-e3zebpmqOYUSMp19CcsvrP5QPlB5iFlsZNgAs/edit#heading=h.7bnr80mxwz1b
 */
class SqRouteToWorkflow extends ActionHandler {

    SequencerType sequencerType

    void execute() {
        sequencerType = process.sequencerType
        updateInputUdfs()
        List<Analyte> repeatOutputAnalytes = sequencerType.repeatOutputAnalytes
        setRepeatActions(repeatOutputAnalytes*.id as Set<String>)
        postRequeueProcesses(repeatOutputAnalytes*.parentAnalyte)
        sequencerType.routeToNextWorkflow()
    }

    void updateInputUdfs(){
        process.outputAnalytes.each{
            ClarityPhysicalRunUnit pru = it as ClarityPhysicalRunUnit
            Analyte inputAnalyte = pru.parentAnalyte
            def tableBean = process.findTableBean(pru, sequencerType.validateLane)
            BigDecimal volume = inputAnalyte.udfVolumeUl - (tableBean.dnaWa01Ul) as BigDecimal
            inputAnalyte.setUdfVolumeUl(volume)
        }
    }

    def postRequeueProcesses(List<Analyte> analytes){
        if (!analytes)
            return
        PostProcessService postProcessService = BeanUtil.getBean(PostProcessService.class)
        def processId = postProcessService.postInputProcess(process, Stage.AUTOMATIC_REQUEUE_SEQUENCING, analytes*.artifactNode)
        return processId
    }

    def setRepeatActions(Set<String> repeatIds) {
        if (!repeatIds)
            return
        process.completeRepeat(repeatIds)
    }

}