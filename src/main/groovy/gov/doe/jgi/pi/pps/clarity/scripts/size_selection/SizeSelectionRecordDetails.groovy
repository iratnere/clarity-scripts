package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess

/**
 * Created by tlpaley on 3/25/16.
 */
class SizeSelectionRecordDetails extends ActionHandler {

    @Override
    void execute() {
        process.inputAnalytes.each{ validateUdfs(it) } //PPV-> analyte instanceof ClarityLibraryPool
        process.setCompleteStage()
    }

    def validateUdfs(ClarityLibraryPool analyte) {
        def errorMsg = StringBuilder.newInstance()
        if (analyte.udfVolumeUl == null) {
            errorMsg << "$ClarityUdf.ANALYTE_VOLUME_UL is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfLibraryMolarityQcPm == null) {
            errorMsg << "$ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.value is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfConcentrationNgUl == null) {
            errorMsg << "$ClarityUdf.ANALYTE_CONCENTRATION_NG_UL is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (analyte.udfActualTemplateSizeBp == null) {
            errorMsg << "$ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP is required" << ClarityProcess.WINDOWS_NEWLINE
        }
        if (errorMsg?.length()) {
            def fullErrorMsg = "Please correct the errors below " << "Analyte($analyte.name):$ClarityProcess.WINDOWS_NEWLINE" << errorMsg
            process.postErrorMessage(fullErrorMsg as String)
        }
    }
}
