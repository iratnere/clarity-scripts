package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/14/16.
 */
class LibraryStocksMigrator extends Migrator {
    static final logger = LoggerFactory.getLogger(LibraryStocksMigrator.class)
    static Map defaultUdfs
    Map<String, List<OnboardingBean>> pruBeans
    Map<String, List> libraryBeans = [:].withDefault {[]}

    LibraryStocksMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager, new SampleAliquotsMigrator(clarityNodeManager))
    }

    static{
        defaultUdfs = [:].withDefault {[:]}
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_VOLUME_UL] = 25
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_LIBRARY_QC_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP] = 300
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_NUMBER_PCR_CYCLES] = 5
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = 1.3
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM] = 1.6
        defaultUdfs[Migrator.SELF_CONTAINER_UDFS][ClarityUdf.CONTAINER_LIBRARY_QC_RESULT] = Analyte.PASS
    }

    @Override
    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans) {
        if(beans.find{!it.getSowId()}) {
            checkExistingAnalytes(beans)
        }
        return beans.findAll{!it.libraryStock}
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerType) {
        return Stage.LC_NOT_JGI
    }

    @Override
    void checkExistingAnalytes(List<OnboardingBean> beans) {
        lookupLibrariesByCollaboratorName(clarityNodeManager, beans)
    }

    String getDestinationStage(Analyte analyte){
        ScheduledSample scheduledSample = (ScheduledSample) analyte.claritySample
        return scheduledSample.runModeCv.targetWorkflowForLibrary.uri
    }

    void updateUdfs(List<OnboardingBean> beans){
        if(!beans)
            return
        def analytes = beans.collect{getAnalyte(it)}
        def defaultUdfs = this.defaultUdfs
        if(defaultUdfs)
            setDefaultUdfs(analytes, this.defaultUdfs[Migrator.SELF_UDFS], this.defaultUdfs[Migrator.SELF_CONTAINER_UDFS])
        def unnamedLibraries = []
        beans.each{ OnboardingBean bean ->
            Analyte analyte = getAnalyte(bean)
            if(analyte.name.startsWith('ls '))
                unnamedLibraries << analyte
            ClarityLibraryStock ls = (ClarityLibraryStock) analyte
            ScheduledSample scheduledSample = ls.claritySample
            if((!scheduledSample.sampleAnalyte.isPacBio) && (scheduledSample.pmoSample.udfExternal != 'Y'))
                ls.udfIndexName = bean.indexName
            ls.udfDegreeOfPooling = scheduledSample.udfDegreeOfPooling
            ls.udfRunMode = scheduledSample.udfRunMode
            ls.udfLibraryCreationQueueId = scheduledSample.udfLibraryCreationQueueId
            ls.udfActualLibraryCreationQueueId = scheduledSample.udfLibraryCreationQueueId
            ls.udfLibraryCreationSpecsId = scheduledSample.udfLibraryCreationSpecsId
            ls.containerUdfLabel = scheduledSample.containerUdfLabel
            bean.updateUdfs(getAnalyteType())
            bean.jgiLibraryStockName = bean.libraryStock.name
        }
        if(unnamedLibraries) {
            LibraryNameReservationService libraryNameReservationService = BeanUtil.getBean(LibraryNameReservationService.class)
            libraryNameReservationService.assignLibraryNames(unnamedLibraries)
        }
    }

    Map getDefaultUdfs(){
        this.defaultUdfs
    }

    ContainerTypes getContainerType(Analyte parent){
        return parent.containerNode.containerTypeEnum
    }

    Analyte getAnalyte(OnboardingBean bean){
        return bean.libraryStock
    }

    Class getAnalyteType(){
        return ClarityLibraryStock.class
    }

    void validatePruBeans(){
        List<String> errors = []
        //Check for duplicate indexes within a pool
        pruBeans.each{String wellLocation, List<HudsonAlphaBean> beans ->
            errors.addAll(validatePoolIndexes(beans[0].poolKey, beans.collect{it.indexSequence}))
        }
        if(errors)
            throw new IllegalArgumentException(errors.join("\n"))
    }

    static List<String> validatePoolIndexes (String poolId, Collection<String> indexes) {
        List<Integer> seqLengths = []
        List<Boolean> seqTypes = []
        List<String> sequences = []
        indexes.each { String indexSequence ->
            def splitIndexes = ArtifactIndexService.splitSequences(indexSequence)
            seqLengths << splitIndexes[0].length()
            sequences.addAll(splitIndexes)
            seqTypes << ArtifactIndexService.isDualIndexSequence(indexSequence)
        }
        List<String> errors = []
        if(seqLengths.unique().size() > 1)
            errors << "Invalid pool $poolId, all indexes are not of same length. \n$indexes"
        if(seqTypes.unique().size() > 1)
            errors << "Invalid pool ${poolId}, combines single and dual indexes together. \n$indexes"
        if(sequences.size() > sequences.unique().size())
            errors << "Invalid pool $poolId, has duplicate indexes. \n$indexes."
        return errors
    }

    List<Analyte> performAdditionalActions(List<Analyte> analytes, Long contactId){
        if(!analytes)
            return analytes
        def pacBioPlateAnalytes = analytes.findAll{
            it.containerType == ContainerTypes.WELL_PLATE_96.value && it.isPacBio
        }
        if(!pacBioPlateAnalytes)
            return analytes
        analytes = analytes - pacBioPlateAnalytes
        Stage stage = Stage.PLATE_TO_TUBE_TRANSFER
        routeAnalytes(stage.uri, pacBioPlateAnalytes.collect{it.id})
        List<ContainerNode> newTubes = clarityNodeManager.createContainersBulk(ContainerTypes.TUBE, pacBioPlateAnalytes.size())
        Map<ContainerLocation, ArtifactNode> containerParents = [:].withDefault {[]}
        pacBioPlateAnalytes.eachWithIndex{ Analyte analyte, int index->
            ContainerLocation outputLocation = new ContainerLocation(newTubes[index].id, '1:1')
            containerParents[outputLocation] = analyte.artifactNode
        }
        ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, stage), containerParents)
        ClarityProcess process = ProcessFactory.processInstance(clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityOutputProcess(processDetails)))
        analytes.addAll(process.outputAnalytes)
        process.outputAnalytes.each{ Analyte analyte ->
            Analyte parent = analyte.singleParent
            analyte.name = parent.name
            cloneNodeUdfs(parent.artifactNode, analyte.artifactNode)
            analyte.systemQcFlag = true
            analyte.artifactNode.readOnly = false
            analyte.artifactNode.httpPut()
        }
        routeParentsAndChildren(pacBioPlateAnalytes, process.outputAnalytes)
        return analytes
    }

    static private void cloneNodeUdfs(def source, def dest, def configuredUdfNames = null) {
        //dest.httpRefresh()
        source?.node?.'udf:field'?.@'name'?.each { udfName ->
            if (configuredUdfNames && !(udfName in configuredUdfNames)) {
                logger.warn "Clarity udf $udfName not found in $configuredUdfNames."
                return
            }
            def udf = ClarityUdf.toEnum(udfName as String)?.udf
            if (!udf) {
                logger.warn "Clarity udf $udfName not found"
                return
            }
            def value = source.getUdfAsString(udf)
            dest.setUdf(udf, value)
            logger.info " $udf : $value"
        }
    }
}
