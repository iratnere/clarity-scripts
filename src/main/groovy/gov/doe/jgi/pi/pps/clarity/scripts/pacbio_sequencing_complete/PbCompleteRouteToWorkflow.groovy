package gov.doe.jgi.pi.pps.clarity.scripts.pacbio_sequencing_complete

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.RqcPacBioService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PbCompleteRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbCompleteRouteToWorkflow.class)

    final static def SUCCESS_CODE_200 = 200

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        passLibraries()
        removeAnalytesFromWorkflows()
    }

    void removeAnalytesFromWorkflows(List<PacBioBindingComplex> inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each{ PacBioBindingComplex bindingComplex ->
            process.removeAnalyteFromActiveWorkflows(bindingComplex)
            PacBioAnnealingComplex annealingComplex = (PacBioAnnealingComplex) bindingComplex.parentAnalyte
            process.removeAnalyteFromActiveWorkflows(annealingComplex)
            Analyte library = annealingComplex.parentAnalyte //could be ClarityLibraryStock or ClarityLibraryPool
            process.removeAnalyteFromActiveWorkflows(library)
        }
    }

    void passLibraries() {
        RqcPacBioService rqcService = BeanUtil.getBean(RqcPacBioService.class)
        rqcService?.callPacBioSequencingCompleteService(process)
    }

}
