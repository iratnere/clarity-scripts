package gov.doe.jgi.pi.pps.clarity.scripts.metabolomics_qc

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class PrintLabels extends ActionHandler {
    static final logger = LoggerFactory.getLogger(PrintLabels.class)
    
    void execute() {
        logger.info "${this.class.name} $action"
        process.writeLabels([process.outputAnalytes[0]]) //need to print one output plate label
    }

}
