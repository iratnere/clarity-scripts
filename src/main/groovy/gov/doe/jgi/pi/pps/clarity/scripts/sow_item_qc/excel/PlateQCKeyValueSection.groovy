package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager

/**
 * Created by lvishwas on 6/16/2015.
 */
class PlateQCKeyValueSection {
    @FieldMapping(header = 'QC Date', cellType = CellTypeEnum.DATE)
    public def qcDate
    @FieldMapping(header = 'Material Type', cellType = CellTypeEnum.STRING)
    public def materialType
    @FieldMapping(header = 'Plate Barcode', cellType = CellTypeEnum.STRING)
    public def plateBarcode
    @FieldMapping(header = 'Plate LIMSID', cellType = CellTypeEnum.STRING)
    public def plateLimsId
    @FieldMapping(header = 'Plate Name', cellType = CellTypeEnum.STRING)
    public def plateName
    @FieldMapping(header = '# Samples', cellType = CellTypeEnum.NUMBER)
    public def samplesCount
    @FieldMapping(header = 'SM Instructions from PM', cellType = CellTypeEnum.STRING)
    public def smInstructionsFromPM
    @FieldMapping(header = 'SM Notes', cellType = CellTypeEnum.STRING)
    public def smNotes

    String validate(String sheetName){
        return ''
    }

    void copyDataFromRootSample(PmoSample rootSample){
        qcDate = rootSample?.udfSampleQcDate
        materialType = rootSample?.sequencingProject?.udfMaterialCategory
        plateBarcode = rootSample?.containerName
        plateLimsId = rootSample?.containerLimsId
        plateName = rootSample?.containerUdfLabel
        smNotes = rootSample?.udfNotes
    }

    void populateBean(int sampleCount, ScheduledSample scheduledSample, NodeManager nodeManager){
        PmoSample rootSample = scheduledSample.pmoSample
        copyDataFromRootSample(rootSample)
        smInstructionsFromPM = rootSample.getSMInstructionsFromScheduledSamples(nodeManager)
        samplesCount = sampleCount

    }

    void updateUdfs(ScheduledSample scheduledSample){
        scheduledSample.udfNotes = smNotes
    }
}
