package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.FractionsTableBean

class ProcessTransferSheet extends ActionHandler {

    void execute() {
        transferBeansDataToOutputs()
        process.setCompleteStage()
    }

    void transferBeansDataToOutputs(List<Analyte> outputAnalytes = process.outputAnalytes as List<Analyte>) {
        outputAnalytes.each { Analyte outputAnalyte ->
            FractionsTableBean bean = (process as SampleFractionationProcess).getFractionsBean(outputAnalyte.id)

            outputAnalyte.setSystemQcFlag(bean.passed)
            outputAnalyte.udfVolumeUl = bean.transferredVolumeUl
            outputAnalyte.udfDestinationBarcode = bean.destinationBarcode
        }
    }
}
