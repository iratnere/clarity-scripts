package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter.SampleQcAdapter
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Action Handler to read, validate and process Sample QC Worksheet
 * Created by tlpaley on 2/26/15.
 */
class ProcessSampleQcSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(ProcessSampleQcSheet.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        SampleQcProcess sampleQcProcess = (SampleQcProcess) process
        List<SampleQcAdapter> adapters = sampleQcProcess.sampleQcAdapters
        assert adapters
        adapters.each { SampleQcAdapter adapter ->
            List<String> errors = adapter.checkProcessUdfs(sampleQcProcess)
            if(errors)
                sampleQcProcess.postErrorMessage(errors.join("\n"))
        }
        sampleQcProcess.sampleQcWorksheetSections
        sampleQcProcess.setCompleteStage()
    }

}