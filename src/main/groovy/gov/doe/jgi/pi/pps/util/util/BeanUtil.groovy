package gov.doe.jgi.pi.pps.util.util

import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service


@Service
class BeanUtil implements ApplicationContextAware {
    private static ApplicationContext context

    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext
    }

    static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass)
    }

    static getRequestCache() {
        return getBean(RequestCash.class).data
    }

    static getNodeManager() {
        return getBean(NodeManager.class)
    }
}
