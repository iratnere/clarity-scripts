package gov.doe.jgi.pi.pps.clarity.scripts.sequence_analysis

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 8/18/2015.
 */
class SqAnalysisEnterRecordDetails extends ActionHandler {
    static final logger = LoggerFactory.getLogger(SqAnalysisEnterRecordDetails.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
    }
}
