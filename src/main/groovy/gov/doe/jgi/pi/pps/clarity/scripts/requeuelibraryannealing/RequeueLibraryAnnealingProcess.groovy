package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibraryannealing

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryAnnealingProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.REQUEUE_LIBRARY_ANNEALING

    boolean testMode = false

    RequeueLibraryAnnealingProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
            'PreProcessValidation':RequeueLibraryAnnealingPreProcessValidation,
            'Process Requeue':RequeueLibraryAnnealing,
            'Route to Next Workflow':RequeueLibraryAnnealingRouteToWorkflow
        ]
    }
}
