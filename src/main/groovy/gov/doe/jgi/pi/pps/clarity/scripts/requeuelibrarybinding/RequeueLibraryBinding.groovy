package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryBinding extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryBinding.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.setCompleteStage()
    }

}
