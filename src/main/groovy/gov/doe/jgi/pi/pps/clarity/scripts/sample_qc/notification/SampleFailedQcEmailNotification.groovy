package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by lvishwas on 9/19/16.
 */
class SampleFailedQcEmailNotification extends EmailNotification{
    static final String SUBJECT = 'Sample failed in QC'
    static final String CC_EMAIL_GROUP = 'jgi-its-ship-sample@lbl.gov'//PPS-4849 - update mailing lists
    //PPS-4491 - until now email body was same for sample receipt failure and qc failure. Now they are different, hence new template file created
    static final String TEMPLATE_FILE_NAME = 'sample_failed_qc'

    @Override
    protected getToList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return SUBJECT
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new SampleFailedQcEmailDetails(it)}
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        return [contact:emailDetailsList.first().sequencingProjectManagerName, samples: getSampleList(emailDetailsList)]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
