package gov.doe.jgi.pi.pps.clarity.scripts.plate_transfer

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class LcPlateTransferRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(LcPlateTransferRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.writeLabels(process.outputAnalytes)
        moveToNextWorkflow()
    }

    void moveToNextWorkflow(List<ClarityLibraryStock> outputAnalytes = process.outputAnalytes) {
        List<ClarityLibraryStock> failedStocks = []
        List<ClarityLibraryStock> passedPacBioSequel = []

        outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            String libraryQcResult = clarityLibraryStock.udfLibraryQcResult
            if (!(libraryQcResult in [Analyte.PASS, Analyte.FAIL])) {
                new WebException("Library Stock($clarityLibraryStock.id): invalid Library QC Result '${libraryQcResult}'", 422)
            }

            if (clarityLibraryStock.udfLibraryQcResult == Analyte.FAIL) {
                failedStocks << clarityLibraryStock
                return
            }

            SequencerModelCv sequencerModelCv = clarityLibraryStock.sequencerModelCv
            if (!sequencerModelCv.isPacBio) {
                throw new WebException("invalid platform [${sequencerModelCv.platform}] for ${clarityLibraryStock} with run mode ${clarityLibraryStock.udfRunMode}, expected PacBio",422)
            }
            if (sequencerModelCv.isSequel) {
                passedPacBioSequel << clarityLibraryStock
            } else {
                throw new WebException("no handler for sequencer model [${sequencerModelCv.sequencerModel}] for ${clarityLibraryStock} with run mode ${clarityLibraryStock.udfRunMode}",422)
            }
        }

        process.routeAnalytes(Stage.ABANDON_QUEUE, failedStocks)
        process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING,passedPacBioSequel)
    }

}
