package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/30/2015.
 */
class McProcessMagbeadCleanupSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(McProcessMagbeadCleanupSheet.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ArtifactNode resultFile = process.getFileNode(MagbeadCleanup.UPLOAD_MAGBEAD_CLEANUP)
        if (!resultFile) {
            process.postErrorMessage("${MagbeadCleanup.UPLOAD_MAGBEAD_CLEANUP} was not attached")
        } else {
            try {
                ExcelWorkbook workbook = new ExcelWorkbook(resultFile.id, false)
            } catch (Exception e) {
                process.postErrorMessage("${MagbeadCleanup.UPLOAD_MAGBEAD_CLEANUP} was not attached")
            }
        }
        process.setCompleteStage()
    }
}
