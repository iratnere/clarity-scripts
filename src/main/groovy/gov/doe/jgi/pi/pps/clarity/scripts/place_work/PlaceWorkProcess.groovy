package gov.doe.jgi.pi.pps.clarity.scripts.place_work

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ClarityAttachmentUtil
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.place_work.beans.LimsIdTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by tlpaley on 8/19/15.
 * * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 */
class PlaceWorkProcess extends ClarityProcess {

    static List<ProcessType> processTypes = [ProcessType.PLACE_IN_NEEDS_ATTENTION, ProcessType.PLACE_ON_HOLD]

    static final String UPLOADED_LIMS_ID_SHEET = 'Upload LIMS ID Worksheet'
    static final String SCRIPT_GENERATED_LIMS_ID_SHEET = 'Download LIMS ID Worksheet'
    static final String TEMPLATE_NAME = 'resources/templates/excel/OnHoldNeedsAttention'

    ExcelWorkbook excelWorkbookCached
    List limsIdBeansCached

    PlaceWorkProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'Prepare Place on Hold Worksheet': PrepareLimsIdSheet,
                'Process Place on Hold Worksheet': ProcessLimsIdSheet,

                'Prepare Place in Needs Attention Worksheet': PrepareLimsIdSheet,
                'Process Place in Needs Attention Worksheet': ProcessLimsIdSheet,

                'Route to Next Workflow': PlaceWorkRouteToWorkflow
        ]
    }

    List<LimsIdTableBean> getLimsIdBeans() {
        if (limsIdBeansCached) {
            return limsIdBeansCached
        }
        List<LimsIdTableBean> beansList = (List<LimsIdTableBean>) getBeanList(LimsIdTableBean.class.simpleName)
        beansList.each {
            def errorMsg = it.validate()
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        limsIdBeansCached = beansList
        return limsIdBeansCached
    }

    ExcelWorkbook getExcelWorkbook() {
        if (excelWorkbookCached) {
            return excelWorkbookCached
        }
        def fileNode = getFileNode(UPLOADED_LIMS_ID_SHEET)
        def excelWorkbook = ClarityAttachmentUtil.downloadExcelWorkbook(fileNode.id)
        Section section = new TableSection(0, LimsIdTableBean.class, 'A1')
        excelWorkbook.addSection(section)
        excelWorkbook.load()
        excelWorkbookCached = excelWorkbook
        excelWorkbookCached
    }

    def getBeanList(String beanName) {
        def beansList = getExcelWorkbook().sections.values()?.find {
            it.beanClass.simpleName == beanName
        }?.getData()
        beansList
    }

    List<Analyte> getAnalytes(){
        List<Analyte> analytes = []
        List<LimsIdTableBean> beans = getLimsIdBeans()
        beans.each{ LimsIdTableBean bean ->
            analytes.addAll(bean.analytes)
        }
        analytes.unique()
    }

}
