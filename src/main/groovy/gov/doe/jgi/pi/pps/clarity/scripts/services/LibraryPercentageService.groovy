package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.Libraries
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.json.JsonUtil
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.Method
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class LibraryPercentageService {

    static final Logger logger = LoggerFactory.getLogger(LibraryPercentageService.class)
    @Value("\${laneFraction.url}")
    String laneFractionsSubmissionUrl

    def laneFractionsResponseJson(List libraryLimsIds){
        def responseJson = callHttp(laneFractionsSubmissionUrl, Method.POST, libraryLimsIds)
        responseJson
    }

    JSONObject getLaneFractionsJson(List libraryLimsIds) {
        Libraries submission = new Libraries()
        submission.libraries = libraryLimsIds.collect{ new LaneFraction(libraryLimsId: it) }
        submission.toJson()
    }


//http://claritydev1.jgi-psf.org/pps-1/lane-fraction
    def callHttp(String url, def method, List libraryLimsIds){
        NodeManager nodeManager = BeanUtil.nodeManager
        def json = getLaneFractionsJson(libraryLimsIds)
        def responseJson = null
        logger.info "http request URL: ${url}"
        logger.info "submission: ${json}"
        HTTPBuilder http = new HTTPBuilder(url)
        http.request( method, ContentType.JSON ) { req ->
            json ? body = json.toString() : null
            response.success = { HttpResponseDecorator resp, respJson ->
                logger.info "response json: ${respJson}"
                responseJson = respJson
            }
            response.failure = { HttpResponseDecorator resp, respJson ->
                def errorMsg = StringBuilder.newInstance()
                errorMsg << "$url service response status: [${resp.status}]" << ClarityProcess.WINDOWS_NEWLINE
                def respError = JsonUtil.toJson(respJson)
                logger.error("$url service response: status [${resp.status}], body [${respError}]")
                if (resp.status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
                    respError.'errors'.each{
                        if (it.'index' != null) {
                            def index = it.'index' as int
                            def libraryLimsId = libraryLimsIds[index]
                            def libraryName = nodeManager.getArtifactNode(libraryLimsId)?.name
                            errorMsg << "$libraryName/$libraryLimsId: $it.message" << ClarityProcess.WINDOWS_NEWLINE
                        }
                    }
                }
                throw new WebException(errorMsg.toString(), resp.status)
            }
        }
        responseJson
    }

}
