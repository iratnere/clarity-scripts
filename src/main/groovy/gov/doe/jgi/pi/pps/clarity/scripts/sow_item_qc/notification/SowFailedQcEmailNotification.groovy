package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * Created by lvishwas on 9/17/16.
 */
class SowFailedQcEmailNotification extends EmailNotification{
    static final String SUBJECT = '***ALERT*** SOW Item(s) Need Attention - SOW Item QC Failure '
    static final String CC_EMAIL_GROUP = 'jgi-its-sow-qc-fail@lbl.gov' //PPS-4849 - update mailing lists
    static final String TEMPLATE_FILE_NAME = 'sow_failed_qc'

    @Override
    protected getToList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        DateFormat formatter = new SimpleDateFormat('EEE MMM dd HH:mm:ss zzz yyyy', Locale.US)
        return "${SUBJECT}${formatter.format(new Date())}"
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new SowQcFailedEmailDetails(it)}
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        //PPS-4285 - sorting info by Sample contact id - 3) order information by sample contact so I have all the samples from a sample contact together instead of interspersed throughout the notification..
        emailDetailsList.sort{a,b ->
            a.sampleContactId <=> b.sampleContactId
        }
        //PPS-4285 - 2) List the SPIDs & Sample IDs at the top so I can quickly copy & paste these IDs into ITS to create new SOW times, if necessary, and/or send out the links to fill out metadata for replacement samples.
        def sampleIds = "${emailDetailsList.collect{it.pmoSampleId}.join(',')}"
        def spIds = "${emailDetailsList.collect{it.sequencingProjectId}.join(',')}"
        return [contact:emailDetailsList.first().sequencingProjectManagerName, sampleIds:sampleIds, spIds: spIds, samples: getSampleList(emailDetailsList), abandonDate: new Date()]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
