package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by tlpaley on 8/9/15.
 */
class SampleAliquotAbandonEmailDetails extends EmailDetails {
    def sowItemId
    def sowItemType
    def defaultLibraryQueue
    def failureMode
    def comments

    SampleAliquotAbandonEmailDetails(ClaritySampleAliquot analyte){
        super(analyte)
        ScheduledSample scheduledSample = analyte.claritySample
        sowItemId = scheduledSample.sowItemId
        sowItemType = scheduledSample.udfSowItemType
        defaultLibraryQueue = scheduledSample.libraryCreationQueue?.libraryCreationQueue
        failureMode = analyte.udfFailureMode
        comments = scheduledSample.udfNotes
    }

    @Override
    def getHashCode(){
        return new HashCodeBuilder(17, 37).
                append(sequencingProjectManagerId).
                toHashCode()
    }

    /*
Proposal ID; Sequencing Project ID; Sequencing Project Name; SP Product; SP PI Name;
Sample Contact(s); PM Name; PMO Sample ID; Sample Name; Sample Mass; LIMS Sample ID;
SOW Item ID; SOW Item Type; Default library queue; Failure Mode; Comments
     */
    def getInfo(){
        def details = []
        details << proposalId
        details << sequencingProjectId
        details << sequencingProjectName
        details << sequencingProductName
        details << sequencingProjectPIName

        details << sampleContactName
        details << sequencingProjectManagerName
        details << pmoSampleId
        details << sampleName
        details << sampleMass
        details << sampleLimsId

        details << sowItemId
        details << sowItemType
        details << defaultLibraryQueue
        details << failureMode
        details << comments

        return details.join('; ')
    }
}
