package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules
/**
 * Created by datjandra on 7/6/2015.
 */
class ConjunctionCondition extends Condition {

    private Collection<Condition> conjunction

    ConjunctionCondition(Collection<Condition> conditions) {
        conjunction = new ArrayList<Condition>()
        conditions.each { Condition condition ->
            conjunction.add(condition)
        }
    }

    @Override
    boolean evaluate(ObjectWithDynamicAttributes percept) {
        for (Condition condition : conjunction) {
            if (!condition.evaluate(percept)) {
                return false
            }
        }
        return true
    }

    @Override
    public String toString() {
        return conjunction?.toString()
    }
}

