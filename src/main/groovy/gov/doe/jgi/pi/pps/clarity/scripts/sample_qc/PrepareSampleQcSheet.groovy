package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc


import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Action Handler to create and populate Sample QC Worksheet
 * Created by tlpaley on 2/26/15.
 */
class PrepareSampleQcSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PrepareSampleQcSheet.class)



    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ExcelWorkbook workbook = generateSampleQCWorksheet()
        def fileNode = processNode.outputResultFiles.find { it.name.contains(SampleQcProcess.UPLOAD_SAMPLE_QC_WORSHEET)}
        logger.info "Writing Sample QC Worksheet to ${fileNode.id}"
        workbook?.store(process.nodeConfig, fileNode.id)
    }

    ExcelWorkbook generateSampleQCWorksheet(ClarityProcess process = this.process){
        SampleQcWorksheet worksheet = new SampleQcWorksheet(process)
        return worksheet?.makeSampleQcWorksheet()
    }
}
