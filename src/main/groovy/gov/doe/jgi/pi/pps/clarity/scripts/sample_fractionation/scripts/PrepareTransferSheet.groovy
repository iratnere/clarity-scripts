package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.FractionsTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.slf4j.LoggerFactory

class PrepareTransferSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(PrepareTransferSheet.class)

    void execute() {
        uploadTransferWorksheet()
    }

    void uploadTransferWorksheet(String fileName = SampleFractionationProcess.SCRIPT_GENERATED_TRANSFER_SHEET) {
        ArtifactNode fileNode = process.getFileNode(fileName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        ExcelWorkbook excelWorkbook = populateTransferSheet()
        excelWorkbook.store(process.nodeManager.nodeConfig, fileNode.id)
    }

    def populateTransferSheet = { ->
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(SampleFractionationProcess.TEMPLATE_NAME,
                (process as SampleFractionationProcess).testMode)
        List<FractionsTableBean> beans = getFractionsBeans(process.outputAnalytes)
        Section tableSection = getTableSection(beans)
        excelWorkbook.addSection(tableSection)
        return excelWorkbook
    }

    static int compare(Analyte a, Analyte b) {
        int result = a.containerName <=> b.containerName
        if (result == 0) {
            // on plate we need to order by columns: A1,B1,...H1, A2,B2,...,H2, A3,B3...
            def aSourceLocation = a.artifactNodeInterface?.location?.minus(':')
            def bSourceLocation = b.artifactNodeInterface?.location?.minus(':')
            def aRow = aSourceLocation?.getAt(0)
            def aCol = aSourceLocation?.drop(1)
            def bRow = bSourceLocation?.getAt(0)
            def bCol = bSourceLocation?.drop(1)
            result = (aCol?.toBigInteger() <=> bCol?.toBigInteger())
            if (result == 0) {
                return aRow <=> bRow
            }
            return result
        }
        return result
    }

    def getTableSection = { List<FractionsTableBean> beans ->
        Section tableSection = getDefaultTableSection()
        beans.sort { a, b -> compare(a.sourceAnalyte, b.sourceAnalyte) }
        tableSection.setData(beans)
        return tableSection
    }

    static getDefaultTableSection = { ->
        return new TableSection(0, FractionsTableBean.class, 'A3', null, true)
    }

    static getDropDownPassFail = { Boolean sourceStatus ->
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(Analyte.PASS_FAIL_LIST)
        if (sourceStatus != null)
            dropDownList.value = sourceStatus ? Analyte.PASS : Analyte.FAIL
        return dropDownList
    }

    static getSourceStatus = { BigDecimal concentration, BigDecimal minDnaConcentration ->
        if (concentration && concentration >= minDnaConcentration) {
            return true
        }
        return false
    }

    def getFractionsBeans = { List<Analyte> outputAnalytes ->
        outputAnalytes.collect { Analyte fraction -> //output analyte
            boolean sourceStatus = getSourceStatus(fraction.udfConcentrationNgUl, process.udfProcessMinDnaConcNgUl)
            FractionsTableBean bean = new FractionsTableBean()
            bean.sourceAnalyte = fraction
            bean.sampleLimsId = fraction.id
            bean.sourceBarcode = fraction.containerName
            bean.sourceLabware = ContainerTypes.WELL_PLATE_96.value
            bean.sourceLocation = fraction.containerLocation.wellLocation
            bean.dnaConcentrationNgUl = fraction.udfConcentrationNgUl
            bean.density = fraction.udfFractionDensity
            bean.sourceStatus = getDropDownPassFail(sourceStatus)
            fraction.systemQcFlag = sourceStatus
            bean.transferredVolumeUl = SampleFractionationProcess.DEFAULT_VOLUME_UL
            bean.destinationLabware = ContainerTypes.TUBE.value
            bean
        }
    }
}
