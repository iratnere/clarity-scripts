package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.NodeManagerUtility
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.SIPUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.RecordDetailsInfo
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.SowItemQc
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.util.util.BeanUtil

class SIPSowQcAdapter extends SowQcAdapter{
    SIPSowQcAdapter(List<Analyte> inputAnalytes) {
        super(inputAnalytes)
    }

    @Override
    String validateInputs() {
        String errors = ''
        errors += checkForUnQCedInputs()
        errors += verifyCompleteness()
        return errors
    }

    String verifyCompleteness() {
        String errors = ''
        analytes.groupBy { ((ScheduledSample)it.claritySample).SIPGroupName}.each{ String groupName, List<Analyte> groupSS ->
            List<String> inputSampleLimsIds = groupSS.collect{((ScheduledSample)it.claritySample).pmoSampleLimsId}
            List<String> groupSampleLimsIds = lookUpSamplesBySipGroupName(groupName)
            if(!inputSampleLimsIds.containsAll(groupSampleLimsIds))
                errors += "All scheduled samples of a SIP group $groupName should be QC'ed together. \n"
        }
        return errors
    }

    List<String> lookUpSamplesBySipGroupName(String groupName) {
        NodeManager nodeManager = BeanUtil.nodeManager
        NodeManagerUtility nodeManagerUtility = new NodeManagerUtility(nodeManager)
        List<String> sampleLimsIds = nodeManagerUtility.lookUpSamplesBySipGroupName(groupName)
        List<ClaritySample> samples = nodeManager.getSampleNodes(sampleLimsIds).collect{ SampleFactory.sampleInstance(it)}
        return samples.findAll{it instanceof PmoSample}?.collect{it.id}
    }

    @Override
    String checkProcessUdfs(SowItemQc sowQcProcess) {
        String errors = ''
        if(sowQcProcess.udfControlsFailurePercentAllowed == null)
            errors += "Process udf 'Controls Failure Percent Allowed' is required.\n"
        if(sowQcProcess.udfReplicatesFailurePercentAllowed == null)
            errors += "Process udf 'Replicates Failure Percent Allowed' is required.\n"
        return errors
    }

    //PPS-5067: update failed original sample status after sow qc
    void updateStatusForFailedSamples(RecordDetailsInfo recordDetailsInfo) {
        List<Analyte> failedAnalytes = analytes.findAll { !(it.claritySample as ScheduledSample).passedQc }
        if(failedAnalytes) {
            Map<Long, SampleStatusCv> completeMap = [:]
            Map<Long, SampleStatusCv> statusMap = [:]
            failedAnalytes.each { Analyte analyte ->
                PmoSample pmoSample = analyte.claritySample.pmoSample
                completeMap[pmoSample.pmoSampleId] = SampleStatusCv.SAMPLE_QC_COMPLETE
                statusMap[pmoSample.pmoSampleId] = pmoSample.passedQc? SampleStatusCv.AVAILABLE_FOR_USE: SampleStatusCv.ABANDONED
            }
            StatusService service = BeanUtil.getBean(StatusService.class)
            service.submitSampleStatus(completeMap, recordDetailsInfo.researcherId)
            service.submitSampleStatus(statusMap, recordDetailsInfo.researcherId)
        }
    }

    void updateFailedSOWstatus(List<ScheduledSample> failedSS, RecordDetailsInfo recordDetailsInfo) {
        Map<Long, SowItemStatusCv> inProgressSows = [:], needsAttentionSows = [:]
        failedSS.each { ScheduledSample scheduledSample ->
            inProgressSows[scheduledSample.sowItemId] = SowItemStatusCv.IN_PROGRESS
            needsAttentionSows[scheduledSample.sowItemId] = SowItemStatusCv.NEEDS_ATTENTION
        }
        StatusService service = BeanUtil.getBean(StatusService.class)
        service.submitSowItemStatus(inProgressSows, recordDetailsInfo.researcherId)
        service.submitSowItemStatus(needsAttentionSows, recordDetailsInfo.researcherId)
    }

    @Override
    void updateStatusAndRouting(RecordDetailsInfo recordDetailsInfo) {
        analytes.groupBy {it.claritySample.pmoSample.udfGroupName}.each { String groupName, List<Analyte> ssAnalyte ->
            SIPUtility.updateGroupQcResult(ssAnalyte.collect{it.claritySample}, recordDetailsInfo.replicatesFailurePercentAllowed, recordDetailsInfo.controlsFailurePercentAllowed)
        }
        List<ScheduledSample> failedSS = []
        analytes.each { SampleAnalyte sampleAnalyte ->
            ScheduledSample scheduledSample = (ScheduledSample)sampleAnalyte.claritySample
            String sowQcResult = scheduledSample.udfSowItemQcResult
            routingRequests.addAll(buildRoutingRequests(sampleAnalyte, sowQcResult))
            if(!scheduledSample.passedQc)
                failedSS << (ScheduledSample)sampleAnalyte.claritySample
        }
        updateStatusForFailedSamples(recordDetailsInfo)
        //PPS-5151 SIP Metabolomics QC - update SOW status to "In Progress" for all SOWs
        if(failedSS)
            updateFailedSOWstatus(failedSS, recordDetailsInfo)
    }

    List<RoutingRequest> buildRoutingRequests(Analyte sampleAnalyte, String sowQcResult){
        if(sowQcResult == Analyte.PASS){
            ScheduledSample scheduledSample = sampleAnalyte.claritySample
            return RoutingRequest.generateRequestsToRouteArtifactIdsToUri(Stage.SAMPLE_QC_METABOLOMICS.uri, [scheduledSample.pmoSampleArtifactLimsId], Routing.Action.assign)
        }
        else if(sowQcResult == Analyte.FAIL){
            List<RoutingRequest> routingRequests = []
            failedScheduledSamples << sampleAnalyte
            sampleAnalyte.activeWorkflows.each { ClarityWorkflow activeWorkflow ->
                if(activeWorkflow != ClarityWorkflow.SOW_ITEM_QC) {
                    routingRequests << new RoutingRequest(routingUri: activeWorkflow.uri, artifactId: sampleAnalyte.id, action: Routing.Action.unassign)
                }
            }
            return routingRequests
        }
        return []
    }
}
