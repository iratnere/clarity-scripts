package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryPoolFailureModeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.util.ClarityStringUtils
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.clarity_node_manager.util.OnDemandCache
import gov.doe.jgi.pi.pps.util.exception.WebException
import groovy.transform.PackageScope

/**
 * Created by dscott on 4/8/2014.
 */
class ClarityLibraryPool extends Analyte {

    static final String[] getActiveLibraryPoolFailureModes() {
        return LibraryPoolFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }
    static final String FAILURE_MODE_SAMPLE_PROBLEM = 'Sample Problem'//getValidLibraryPoolFailureModes().find {it.equalsIgnoreCase('Sample Problem')}
    static final String[] EXCLUDED_FAILURE_MODES = ['Abandoned Work']
    static final String[] getValidLibraryPoolFailureModes() {
        return getActiveLibraryPoolFailureModes()?.minus(EXCLUDED_FAILURE_MODES)
    }
    
    static final BigDecimal DEFAULT_VOLUME_UL = 10

    protected OnDemandCache<List<Analyte>> cachedPoolMembers = new OnDemandCache<List<Analyte>>()

    @PackageScope
    ClarityLibraryPool(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    Set<String> getSampleIds() {
        Set<String> ids = []
        artifactNodeInterface.sampleIds?.each{ids << it}
        poolMembers?.each { Analyte poolMember ->
            ids.addAll(poolMember.sampleIds)
        }
        ids
    }

    List<Analyte> getPoolMembers() {
        return cachedPoolMembers.fetch {
            artifactNodeInterface.parentProcessNode.getInputAnalytes(this.id)?.collect{ AnalyteFactory.analyteInstance(it) }
        }
    }

    BigDecimal getUdfActualTemplateSizeBp() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf)
    }

    void setUdfActualTemplateSizeBp(BigDecimal actualTemplateSizeBp) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, actualTemplateSizeBp)
    }

    BigDecimal getUdfLibraryMolarityQcPm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.udf)
    }

    void setUdfLibraryMolarityQcPm(BigDecimal libraryMolarityQcPm) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.udf, libraryMolarityQcPm)
    }

    String getUdfLibraryQcResult() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QC_RESULT.udf)
    }

    void setUdfLibraryQcResult(String libraryQcResult) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QC_RESULT.udf, libraryQcResult)
    }

    String getUdfLibraryQcFailureMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QC_FAILURE_MODE.udf)
    }

    void setUdfLibraryQcFailureMode(String libraryQcFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QC_FAILURE_MODE.udf, libraryQcFailureMode)
    }

    String getUdfLibraryCreator() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_CREATOR.udf)
    }

    void setUdfLibraryCreator(String libraryCreator) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CREATOR.udf, libraryCreator)
    }

    BigDecimal getUdfDegreeOfPooling() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_DEGREE_POOLING.udf)
    }

    void setUdfDegreeOfPooling(BigDecimal degreeOfPooling) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_DEGREE_POOLING.udf, degreeOfPooling)
    }

    //3_0_0
    void setUdfMagBeadCleanupDate(Date cleanupDate) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_MAGBEAD_CLEANUP_DATE.udf, ClarityStringUtils.toClarityDate(cleanupDate))
    }

    Date getUdfMagBeadCleanupDate() {
        return artifactNodeInterface.getUdfAsDate(ClarityUdf.ANALYTE_MAGBEAD_CLEANUP_DATE.udf)
    }

    @Override
    String getPrintLabelText() {
        return new LabelBean(
                pos_1_1: containerName,
                pos_1_2: name,
                pos_1_3: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerName,
                pos_2_2: name
        ) as String
    }

    String getUdfLibraryQpcrFailureMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_FAILURE_MODE.udf)
    }

    void setUdfLibraryQpcrFailureMode(String failureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QPCR_FAILURE_MODE.udf, failureMode)
    }

    String getUdfLibraryQpcrResult() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf)
    }

    void setUdfLibraryQpcrResult(String qpcrResult) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf, qpcrResult)
    }

    BigDecimal getUdfLibraryMolarityPm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM.udf)
    }

    void setUdfLibraryMolarityPm(BigDecimal libraryMolarityPm) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM.udf, libraryMolarityPm)
    }

    def updateLibraryCreationUdfs(String researcherFullName) {
        setUdfLibraryCreator(researcherFullName)
        setUdfRunMode(claritySample.getUdfRunMode())
        setUdfDegreeOfPooling(claritySample.getUdfDegreeOfPooling())
    }

    void setUdfRunMode(String runMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_RUN_MODE.udf, runMode)
    }

    //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
    BigDecimal getUdfLibraryPercentageWithSOF() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_PERCENTAGE_SOF.udf)
    }

    void setUdfLibraryPercentageWithSOF(BigDecimal libraryPercentage) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_PERCENTAGE_SOF.udf, libraryPercentage)
    }

    @Override
    BigDecimal getUdfLpActualWithSof() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LP_ACTUAL_WITH_SOF.udf)
    }

    @Override
    void setUdfLpActualWithSof(BigDecimal value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LP_ACTUAL_WITH_SOF.udf, value)
    }

    @Override
    RunModeCv getRunModeCv() {
        String runModeName = udfRunMode
        if (!runModeName) {
            throw new WebException("${this}: run mode not defined", 422)
        }
        RunModeCv runModeCv = RunModeCv.findByRunMode(runModeName)
        if (!runModeCv?.id) {
            throw new WebException("${this}: run mode not found for the value [${runModeName}]", 422)
        }
        return runModeCv
    }

    @Override
    void setNewRunMode(String runMode) {
        setUdfRunMode(runMode)
        poolMembers.each { Analyte poolMember ->
            if (this.is(poolMember)) {
                return
            }
            poolMember.setNewRunMode(runMode)
        }
    }

    @Override
    BigInteger getReadLengthBp() {
        return runModeCv.readLengthBp
    }

    BigInteger getReadTotal() {
        return runModeCv.readTotal
    }
    @Override
    SequencerModelCv getSequencerModelCv() {
        return runModeCv.sequencerModel
    }

    String getSequencer() {
        return sequencerModelCv?.sequencerModel
    }

    String getPlatform() {
        return sequencerModelCv?.platform
    }

    Integer getMaxLcAttempt() {
        Integer maxLcAttempt = Integer.MIN_VALUE
        for (Analyte member : poolMembers) {
            if (member instanceof ClarityLibraryStock) {
                ClarityLibraryStock libraryStock = member as ClarityLibraryStock
                maxLcAttempt = Math.max(maxLcAttempt, libraryStock.maxLcAttempt)
            }
        }
        return maxLcAttempt
    }

    void validateVolumeUl(SequencerModelCv sequencerModelCv = runModeCv?.sequencerModel) {
        if (sequencerModelCv.isIlluminaNovaSeq && !udfVolumeUl) {
            throw getWebException(ClarityUdf.ANALYTE_VOLUME_UL.value)
        }
        if (isPacBio && !udfVolumeUl) {
            throw getWebException(ClarityUdf.ANALYTE_VOLUME_UL.value)
        }
    }

    void validateActualTemplateSizeBp(SequencerModelCv sequencerModelCv = runModeCv?.sequencerModel) {
        if (isPacBio && !udfActualTemplateSizeBp) {
            throw getWebException(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.value)
        }
    }

    WebException getWebException(String udfName){
        return new WebException("""
Library pool($id, $name, $runModeCv.runMode)
The '${udfName}' udf is not set to the '$sequencerModelCv.platform $sequencerModelCv.sequencerModel' analyte
""",
                422
        )
    }

    @Override
    def validateIsLibraryInput() {
    }

    @Override
    boolean getIsPacBioSequel() {
        return sequencerModelCv.isSequel
    }

    @Override
    boolean getIsPacBio() {
        return sequencerModelCv.isPacBio
    }

    @Override
    String getSmrtbellTemplatePrepKit() {
        ClarityLibraryStock libraryStock = poolMembers[0] as ClarityLibraryStock
        if (libraryStock.udfSmrtbellTemplatePrepKit)
            return libraryStock.udfSmrtbellTemplatePrepKit
        return libraryStock.udfPacBioSmrtbellTemplatePrepKit
    }

    List<String> getLibraryStockIds(){
        return artifactNodeInterface.parentAnalyteIds
    }

    boolean getIsSAG() {
        if(poolMembers.find{it.isSAG})
            return true
        return false
    }

}
