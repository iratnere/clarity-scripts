package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This class is meant to perform pre-process validation for the Approve for shipping workflow
 * input must be samples
 * inputs must have status “Awaiting Shipping Approval”
 * PEU calculation must be successful and <=3 for all sow items of the selected samples/inputs
 * container type must be supported by library creation queue designated by sow item (uss.dt_library_creation_queue_cv has mass and volume values for the types of containers supported), except for Production RnD samples
 * Created by lvishwas on 1/17/15.
 */
class ApproveForShippingPPV extends ActionHandler {

    static final Logger logger = LoggerFactory.getLogger(ApproveForShippingPPV.class)
    
    String errorMsg = ''

    void execute(){
        logger.info "Starting ${this.class.name} action...."
        def sampleIds = []
        process.inputAnalytes?.each{ SampleAnalyte inputSample ->
            checkIsSample(inputSample)
            sampleIds << inputSample.claritySample.pmoSample.pmoSampleId
        }
        if(errorMsg)
            process.postErrorMessage(errorMsg)
        StatusService service
        if(!service)
            service = BeanUtil.getBean(StatusService.class)
        logger.info "retrieving sample status for samples $sampleIds"
        checkSampleStatus(service.getSampleStatusBatch(sampleIds))
        if(errorMsg)
            process.postErrorMessage(errorMsg)
        verifyPEU(sampleIds)
        if(errorMsg)
            process.postErrorMessage(errorMsg)
    }

    void verifyPEU(def sampleIds = process.inputAnalytes.collect{it.claritySample.pmoSampleId}){
        //PEU calculation must be successful and <=3 for all sow items of the selected samples/inputs
        //container type must be supported by library creation queue designated by sow item
        ScheduledSampleService service = BeanUtil.getBean(ScheduledSampleService.class)
        try{
            //PPS-5090: Supply chain management need ti validate queue vs sample container.
            service.getBatchSowItemMetadataForSample(sampleIds,true,false)
        }catch(Exception e){
            errorMsg += e.message + "\n"
        }
        if(errorMsg){
            process.postErrorMessage(errorMsg)
        }
    }

    void checkIsSample(SampleAnalyte analyte){
        //input must be samples
        if ((analyte instanceof SampleAnalyte) && (analyte?.claritySample instanceof PmoSample)){
            return
        }
        errorMsg += "Invalid input ${analyte}. Expecting samples as input"
        errorMsg += "\n"
    }

    void checkSampleStatus(Map statusMap) {
        //inputs must have status “Awaiting Shipping Approval”
        statusMap.each{
            def sampleStatus = it.value
            if(sampleStatus != SampleStatusCv.AWAITING_SHIPPING_APPROVAL.value) {
                errorMsg += "Invalid input ${it.key} with current status ${sampleStatus}. Expected status:  ${SampleStatusCv.AWAITING_SHIPPING_APPROVAL.value}"
                errorMsg += "\n"
            }
        }
    }
}
