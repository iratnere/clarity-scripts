package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 9/13/16.
 */
class SampleFailedQcEmailDetails extends EmailDetails{
    def comments
    def sampleQcFailureMode
    //PPS-4491
    def proposalPI

    SampleFailedQcEmailDetails(Analyte analyte) {
        super(analyte)
        PmoSample pmoSample = analyte.claritySample as PmoSample
        sampleQcFailureMode = pmoSample.udfSampleQCFailureMode
        comments = pmoSample.udfSampleQcNotes
        //PPS-4491
        proposalPI = EmailDetails.formatName(pmoSample.sequencingProject.udfSequencingProjectPI)
    }

    @Override
    def getHashCode(){
        return new HashCodeBuilder(17, 37).
                append(sequencingProjectManagerId).
                toHashCode()
    }

    /*
    PPS-4491
SP ID, SP Name, Proposal ID, Proposal PI, Seq Proj PI, Sample Contact, Product Name, Plate Name, Sample Name, PMO Sample ID, GLS Sample, Sample Mass, Sample QC Failure Mode, Sample QC Comments
     */
    @Override
    def getInfo() {
        def details = []
        details << sequencingProjectId
        details << sequencingProjectName
        details << proposalId
        //PPS-4491
        details << proposalPI
        details << sequencingProjectPIName
        details << sampleContactName
        details << sequencingProductName
        details << containerName
        details << sampleName

        details << pmoSampleId
        details << sampleLimsId
        details << sampleMass
        details << sampleQcFailureMode
        details << comments

        return details.join(', ')
    }
}
