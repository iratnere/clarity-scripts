package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter.SowQcAdapter

/**
 * Created by tlpaley on 2/26/15.
 */
class SowItemQcPpv extends ActionHandler {

    String checkIsScheduledSample(Analyte analyte){
        if(!(analyte instanceof SampleAnalyte) || !(analyte?.claritySample instanceof ScheduledSample))
            return "Invalid input ${analyte}. Expecting Scheduled Samples as input.\n"
        return ''
    }

    void execute() {
        String errors = ''
        process.inputAnalytes.each{ Analyte analyte ->
            errors += checkIsScheduledSample(analyte)
        }
            if(errors)
            process.postErrorMessage(errors)
        SowItemQc sowQcProcess = (SowItemQc) process
        sowQcProcess.cacheRootSamples()
        List<SowQcAdapter> adapters = sowQcProcess.sowQcAdapters
        assert adapters
        adapters.each { SowQcAdapter adapter ->
            errors += adapter.validateInputs()
            }
        if(errors)
            process.postErrorMessage(errors)
    }
}