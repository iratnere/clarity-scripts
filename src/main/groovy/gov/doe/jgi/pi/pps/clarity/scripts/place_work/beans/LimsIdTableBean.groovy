package gov.doe.jgi.pi.pps.clarity.scripts.place_work.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class LimsIdTableBean {
	@FieldMapping(header = 'Scheduled Sample Lims Id', cellType = CellTypeEnum.STRING)
	public String sampleLimsId
    @FieldMapping(header = 'Container Lims Id', cellType = CellTypeEnum.STRING)
    public String containerLimsId
    @FieldMapping(header = 'Analyte Lims Id', cellType = CellTypeEnum.STRING)
    public String artifactLimsId

    List<Analyte> analytes = []

    def validate(NodeManager nodeManager = BeanUtil.nodeManager) {
        def errorMsg = StringBuilder.newInstance()
        if (containerLimsId) {
            ContainerNode containerNode = nodeManager.getContainerNode(containerLimsId)
            if (!containerNode) {
                errorMsg << "$ClarityProcess.WINDOWS_NEWLINE Container LIMS ID($containerLimsId): Can't find container node by lims id $containerLimsId"
            } else
                analytes = containerNode.contentsArtifactNodes.collect{ AnalyteFactory.analyteInstance(it)}
        }
        if (sampleLimsId) {
            SampleNode sampleNode = nodeManager.getSampleNode(sampleLimsId)
            if (!sampleNode) {
                errorMsg << "$ClarityProcess.WINDOWS_NEWLINE Sample LIMS ID($sampleLimsId): Can't find sample node by lims id $sampleLimsId"
            } else
                analytes << AnalyteFactory.analyteInstance(sampleNode.artifactNode)
        }
        if (artifactLimsId) {
            ArtifactNode artifactNode = nodeManager.getArtifactNode(artifactLimsId)
            if (!artifactNode) {
                return  errorMsg << "$ClarityProcess.WINDOWS_NEWLINE Analyte LIMS ID($artifactLimsId): Can't find artifact node by lims id $artifactLimsId"
            } else
                analytes << AnalyteFactory.analyteInstance(artifactNode)
        }
        return buildErrorMessage(errorMsg)
    }

    static def buildErrorMessage(def errorMsg) {
        def startErrorMsg = "Please correct the errors below and upload spreadsheet again."//<<' '
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

}
