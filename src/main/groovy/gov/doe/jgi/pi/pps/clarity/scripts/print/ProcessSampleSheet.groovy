package gov.doe.jgi.pi.pps.clarity.scripts.print

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 3/25/16.
 */
class ProcessSampleSheet extends ActionHandler {

    @Override
    void execute() {
        process.getSampleAnalyteBeans()
        //process.setCompleteStage() //not needed -> control samples are configured as a "single step control" sample
    }
}
