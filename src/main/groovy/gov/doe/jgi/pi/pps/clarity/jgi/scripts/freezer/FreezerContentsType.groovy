package gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 3/7/11
 * Time: 11:21 AM
 *
 * Enum corresponds to controlled vocabulary defined in table freezers.dt_contents_type_cv.
 *
 */
enum FreezerContentsType {

	CONTAINER ('container', 'Indeterminate Contents'),
	BEAD_PREP_454 ('454_BEAD_PREP', '454 Bead Prep', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.F454),
	WORKING_ALIQUOT_454 ('454_WORKING_ALIQ', '454 Working Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.F454),
	LIBRARY_STOCK_454 ('454_LIB_STOCK', '454 Library Stock', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.F454),
	LIBRARY_STOCK_ILLUMINA ('ILLUMINA_LIBRARY_STOCK', 'Illumina Library Stock', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.ILLUMINA),
	PCR_STOCK_ILLUMINA ('ILLUMINA_PCR_STOCK', 'Illumina PCR Stock', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.ILLUMINA),
	WORKING_ALIQUOT_ILLUMINA ('ILLUMINA_WORKING_ALIQUOT', 'Illumina Working Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.ILLUMINA),
	SAMPLE ('SAMPLE', 'Sample Stock', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.CLONING_TECHNOLOGY),
	SAMPLE_ALIQUOT ('SAMPLE_ALIQUOT', 'Sample Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.CLONING_TECHNOLOGY),
	VENONAT_QPCR('venonat_qpcr', 'Venonat qPCR'),
	VENONAT_POOLING('venonat_pooling','Venonat Pooling'),
	VENONAT_SEQUENCING('venonat_sequencing','Venonat Sequencing'),
	VENONAT_SAMPLE_STOCK('venonat_sample_stock', 'Venonat Sample Stock', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	VENONAT_SAMPLE_ALIQUOT('venonat_sample_aliquot','Venonat Sample Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	VENONAT_CONTAINER('venonat_container', 'Venonat Indeterminate Container'),
	VENONAT_ILLUMINA_WORKING_ALIQUOT('venonat_illumina_working_aliquot', 'Venonat Illumina Working Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.ILLUMINA),
	RNA_SAMPLE('RNA_sample','RNA Sample', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	DNA_SAMPLE('DNA_sample','DNA Sample', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	RNA_SAMPLE_ALIQUOT('RNA_sample_aliquot','RNA Sample Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	DNA_SAMPLE_ALIQUOT('DNA_sample_aliquot','DNA Sample Aliquot', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.SAMPLE_MANAGEMENT),
	QPCR('qPCR','qPCR', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.QPCR),
	POOLING('pooling','Pooling', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.POOLING),
	CLUSTER_GENERATION('cluster_generation','Cluster Generation', gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose.CLUSTER_GENERATION)
	
	public final databaseName;
    public final freezerPurpose
    public final limsName
	/**
	 * The name string supplied to the constructor should match the database field container_type in table freezers.dt_contents_type_cv.
	 * @param databaseName
	 * @return
	 */
    FreezerContentsType(String databaseName, String limsName, gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerPurpose freezerPurpose = null) {
		this.databaseName = databaseName
        this.freezerPurpose = freezerPurpose
        this.limsName = limsName
	}

	@Override
	String toString() {
		return databaseName;
	}

	/**
	 * Method allows conversion of the string contents_type in table freezers.dt_container_location or freezers.dt_holder to the corresponding
	 * enum value.
	 * @param databaseName
	 * @return
	 */
	static FreezerContentsType databaseNameToEnum(String databaseName) {
		for (FreezerContentsType contentsType : FreezerContentsType.values()) {
			if (databaseName == contentsType.databaseName) {
				return contentsType;
			}
		}
        return null;
	}

	/**
	 * Method allows conversion of the string selected in LIMS UI (e.g. that of the Freezer Check-In process) to the corresponding
	 * enum value.
	 * @param limsName
	 * @return
	 */
	static FreezerContentsType limsNameToEnum(String limsName) {
		for (FreezerContentsType contentsType : FreezerContentsType.values()) {
			if (limsName == contentsType.limsName) {
				return contentsType;
			}
		}
        return null;
	}
    

}