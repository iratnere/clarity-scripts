package gov.doe.jgi.pi.pps.clarity.scripts.pacbio_sequencing_complete

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 9/16/2015.
 */
class PbCompletePreProcessValidation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbCompletePreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.inputAnalytes.each { Analyte inputAnalyte ->
            if (!inputAnalyte instanceof PacBioBindingComplex) {
                postFatalError("$inputAnalyte is not a PacBio binding complex", HttpStatus.SC_BAD_REQUEST)
            }
        }
    }
}
