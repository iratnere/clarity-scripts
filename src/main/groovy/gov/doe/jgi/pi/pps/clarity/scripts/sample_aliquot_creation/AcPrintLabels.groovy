package gov.doe.jgi.pi.pps.clarity.scripts.sample_aliquot_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class AcPrintLabels extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(AcPrintLabels.class)
    
    void execute() {
        logger.info "${this.class.name} $action"
        process.writeLabels([process.outputAnalytes[0]]) //need to print one output plate label
    }

}
