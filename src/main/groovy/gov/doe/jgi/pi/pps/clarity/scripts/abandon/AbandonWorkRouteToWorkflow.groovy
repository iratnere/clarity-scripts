package gov.doe.jgi.pi.pps.clarity.scripts.abandon


import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailEvent
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.ProcessUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.abandon.email_notification.AbandonWorkEmailNotification
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 8/19/15.
 */
class AbandonWorkRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(AbandonWorkRouteToWorkflow.class)
    void performLastStepActions(){
        removeDescendantsFromWorkflows()
        removeInputsFromWorkflows()
        moveToWorkflow()
        updatePlussStatus()
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        (process as AbandonWorkProcess).validateInputStages()
        (process as AbandonWorkProcess).validateDecsendantStages()
        performLastStepActions()
        sendEmailNotification()
    }

    List<EmailEvent> sendEmailNotification(List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>) {
        List<EmailEvent> emailEvents = []
        emailEvents.addAll(new ProcessUtility(process).sendEmailNotification(inputAnalytes, new AbandonWorkEmailNotification()))
        emailEvents
    }

    void updatePlussStatus(List<SampleAnalyte> inputAnalytes = process.inputAnalytes as List<SampleAnalyte>,
                           StatusService service = BeanUtil.getBean(StatusService.class)
    ) {
        Map<Long, SowItemStatusCv> sowItemStatusMap = [:]
        inputAnalytes.each { SampleAnalyte analyte ->
            Long sowItemId = (analyte.claritySample as ScheduledSample)?.sowItemId
            sowItemStatusMap[sowItemId] = SowItemStatusCv.NEEDS_ATTENTION
        }
        service.submitSowItemStatus(sowItemStatusMap, process.researcherContactId)
    }

    void moveToWorkflow(inputAnalytes = process.inputAnalytes as List<SampleAnalyte>) {
        process.routeArtifactNodes(Stage.ABANDON_QUEUE, inputAnalytes*.artifactNode)
    }

    void removeInputsFromWorkflows(List<Analyte> inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each{ Analyte analyte ->
            analyte.artifactNode.activeStages.findAll{ it.name != Stage.ABANDON_WORK.value}.collect { ArtifactWorkflowStage artifactWorkflowStage ->
                ClarityWorkflow.toEnum(artifactWorkflowStage.workflowNode.name)
            }.unique().each { clarityWorkflow ->
                process.removeAnalyteFromWorkflow(analyte, clarityWorkflow as ClarityWorkflow)
            }
        }
    }

    void removeDescendantsFromWorkflows() {
        ((AbandonWorkProcess) process).inputAnalytesToDescendants.each { Analyte analyte, List<Analyte> descendants ->
            descendants.each { Analyte descendant ->
                process.removeAnalyteFromActiveWorkflows(descendant)
            }
        }
    }
}