package gov.doe.jgi.pi.pps.clarity.scripts.requeue_qc

import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class RequeueQcPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueQcPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        validateInputs()
    }

    def validateInputs(def inputAnalytes = process.inputAnalytes,
                       StatusService service = BeanUtil.getBean(StatusService.class)
    ) {
        inputAnalytes.each { Analyte analyte ->
            validateIsPmoSample(analyte)
            PmoSample pmoSample = (PmoSample) analyte.claritySample
            validateSampleStatus(
                    pmoSample,
                    [SampleStatusCv.AVAILABLE_FOR_USE.value, SampleStatusCv.NEEDS_ATTENTION.value],
                    process,
                    service
            )
        }
    }

    void validateIsPmoSample(Analyte analyte) {
        if (!analyte.isPmoSample) {
            process.postErrorMessage("Invalid input analyte '$analyte.id'. Expecting Sample as input.")
        }
    }

    void validateSampleStatus(
            PmoSample pmoSample,
            List<String> expectedStatuses,
            ClarityProcess clarityProcess,
            StatusService service = BeanUtil.getBean(StatusService.class)
    ) {
        logger.info "retrieving sample status for ${pmoSample.id} named ${pmoSample.name}"
        String sampleStatus = service.getSampleStatus(pmoSample.pmoSampleId)
        if(!(sampleStatus in expectedStatuses)){
            clarityProcess.postErrorMessage("""
Invalid input ${pmoSample.id} named ${pmoSample.name}
with current status ${sampleStatus}.
Expected: ${expectedStatuses}
            """)
        }
    }

}
