package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/24/16.
 */
class SizeSelectionRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(SizeSelectionRouteToWorkflow.class)

    void execute() {
        logger.info "${this.class} execute()"
        performLastStepActions()
    }

    void performLastStepActions(){
        process.routeArtifactNodes(Stage.LIBRARY_QPCR, process.inputAnalytes*.artifactNode)
    }

}