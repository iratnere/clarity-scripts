package gov.doe.jgi.pi.pps.scriptexecution

import gov.doe.jgi.pi.pps.util.PpsException
import groovy.transform.Canonical

import javax.annotation.PostConstruct
import javax.validation.Valid
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory
import javax.validation.constraints.NotNull

@Canonical
class ScriptParams {

    static Validator validator
    def constraintViolations

    @PostConstruct
    static void createValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
    }

    @NotNull
    @Valid
    ScriptOptions options
    List<String> arguments

    void validate() {
        if (!validator)
            createValidator()
        constraintViolations = validator.validate(this)
        if (constraintViolations) {
            def errors = constraintViolations.collect {
                String message = it.message
                if (!message)
                    message = "may not be $it.invalidValue"
                "[$it.propertyPath] $message"
            }.sort()
//            errors.each {
//                println it
//            }
            throw new PpsException(errors.join('\n'))
        }
    }
}
