package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel

import gov.doe.jgi.pi.pps.clarity.domain.QcTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.SampleQcProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import groovy.util.logging.Slf4j

import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * Bean corresponding to thr Table in the GLS & Startlet Use sheet
 * Created by lvishwas on 5/1/2015.
 */
@Slf4j
class GLSStartletUseTableBean {
    @FieldMapping(header = 'Input Sample #', cellType = CellTypeEnum.NUMBER)
    public def sampleNumber
    @FieldMapping(header = 'ITS Sample ID', cellType = CellTypeEnum.STRING)
    public def pmoSampleId
    @FieldMapping(header = 'Sample Lims ID', cellType = CellTypeEnum.STRING)
    public def sampleLimsId
    @FieldMapping(header = 'Source Barcode', cellType = CellTypeEnum.STRING)
    public def sampleBarcode
    @FieldMapping(header = 'Source Location', cellType = CellTypeEnum.STRING)
    public def sampleLocation
    @FieldMapping(header = 'Source Labware', cellType = CellTypeEnum.STRING)
    public def sampleLabware
    @FieldMapping(header = 'Sequencing Product', cellType = CellTypeEnum.STRING)
    public def seqProduct
    @FieldMapping(header = 'Organism', cellType = CellTypeEnum.STRING)
    public def seqProject
    @FieldMapping(header = 'Date', cellType = CellTypeEnum.STRING)
    public def date
    @FieldMapping(header = 'Comments', cellType = CellTypeEnum.STRING)
    public def comments
    @FieldMapping(header = 'SM Instructions', cellType = CellTypeEnum.STRING)
    public def smInstructions
    @FieldMapping(header = 'SM Notes', cellType = CellTypeEnum.STRING)
    public def smNotes
    @FieldMapping(header = 'Required QC Type', cellType = CellTypeEnum.DROPDOWN)
    public def reqdQCType
    @FieldMapping(header = 'Collaborator Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def collaboratorVolume
    @FieldMapping(header = 'Collaborator Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public def collaboratorConcentration
    @FieldMapping(header = 'Initial Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def initialVolume // Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populateOutputPlateSection()
    @FieldMapping(header = 'Vol for Quantity (ul)', cellType = CellTypeEnum.NUMBER)
    public def volForQuantity
    @FieldMapping(header = 'Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public def concentration// Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populateOutputPlateSection()
    @FieldMapping(header = 'Vol for Quality (ul)', cellType = CellTypeEnum.NUMBER)
    public def volForQuality
    @FieldMapping(header = 'Gel Diluent Vol (ul)', cellType = CellTypeEnum.NUMBER)
    public def gelDiluentVol
    @FieldMapping(header = 'Vol for Purity (ul)', cellType = CellTypeEnum.NUMBER)
    public def volForPurity
    @FieldMapping(header = 'A260/A280', cellType = CellTypeEnum.NUMBER)
    public def a260a280
    @FieldMapping(header = 'A260/A230', cellType = CellTypeEnum.NUMBER)
    public def a260a230
    @FieldMapping(header = 'HMW gDNA (Y/N/Marginal/Unable to proceed)', cellType = CellTypeEnum.DROPDOWN)
    public def hMWgDNAYN
    @FieldMapping(header = 'Quality Score', cellType = CellTypeEnum.STRING)
    public def qualityScore
    @FieldMapping(header = 'rRNA Ratio', cellType = CellTypeEnum.NUMBER)
    public def rRNARatio
    @FieldMapping(header = 'Available Volume (ul)', cellType = CellTypeEnum.FORMULA)
    public def availableVolume
    @FieldMapping(header = 'Available Mass (ng)', cellType = CellTypeEnum.FORMULA)
    public def availableMass// Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populateOutputPlateSection()
    @FieldMapping(header = 'QC Status', cellType = CellTypeEnum.DROPDOWN)
    public def qcStatus// Please do not change the variable name, this variable name is being used to get the field by reflection in SampleQCWorksheet.populatePlateSection()
    @FieldMapping(header = 'Failure Mode', cellType = CellTypeEnum.DROPDOWN)
    public def failureMode
    @FieldMapping(header = 'QC Comments', cellType = CellTypeEnum.STRING)
    public def qcComments
    @FieldMapping(header = 'Freezer Path', cellType = CellTypeEnum.STRING)
    public def freezerPath
    @FieldMapping(header = 'Freezer Operator', cellType = CellTypeEnum.STRING)
    public def freezerOperator
    @FieldMapping(header = 'Freezer Status', cellType = CellTypeEnum.STRING)
    public def freezerStatus

    private PmoSample sample

    PmoSample getPmoSample() {
        this.sample
    }
    void setPmoSample(PmoSample sample) {
        this.sample = sample
        log.info "set ${this.class.simpleName} sample to ${sample.id}"
        log.info "${sample} XML:\n${sample.sampleNode.nodeString}"
    }

    DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")

    String validateBean(sampleQcTypeIds, def scheduledSamples) {
        String errors = ''
        if (!reqdQCType || !reqdQCType.value) {
            errors += "Required QC Type column not set for the sample ${sample}.\n"
        }
        def cumulativeQcTypeId = sample.udfCumulativeQcTypeId ? sample.udfCumulativeQcTypeId : 0
        def qcTypeIds = sampleQcTypeIds
        qcTypeIds.each { Long sowItemId, Integer qcTypeId ->
            def scheduledSample = scheduledSamples.find{it.sowItemId == sowItemId}
            if(!scheduledSample || !scheduledSample.udfSowItemQcResult){
               if(!validateQCType(qcTypeId, cumulativeQcTypeId, QcTypeCv.findByQcType(reqdQCType.value)?.id)){
                   errors += "All sow items associated with the sample ${sample} that have not gone through sow item qc cannot be satisfied.\n"
               }
            }
        }
        if (!availableVolume) {
            errors += "Available volume column not set for the sample ${sample}.\n"
        }
        if (!qcStatus || !qcStatus.value) {
            errors += "QC Status column not set for the sample ${sample}.\n"
        }
        if (qcStatus?.value == Analyte.FAIL && !failureMode.value) {
            errors += "Failure Mode column not set for the sample ${sample}.\n"
        }
        return errors
    }

    boolean validateQCType(def a, def b, def c){
        if(c == null)
            return true
        if(a == 0)
            return true
        b = b|c
        if((((a^b)|b)^b) == 0)
            return true
        return false
    }

    void updateSampleUdfs(NodeManager nodeManager) {
        String currentQcType = reqdQCType.value
        def qcTypeId = QcTypeCv.findByQcType(currentQcType)?.id
        //Cumulative QC Type Id = cumulative-qc-type-id =cumulative-qc-type-id  OR current-qc-type-id where current-qc-type-id is the corresponding id from uss.dt_sample.sample_qc_type_cv based on “Current QC Type” udf
        if(qcTypeId)
            sample?.udfCumulativeQcTypeId = (sample?.udfCumulativeQcTypeId? sample?.udfCumulativeQcTypeId : 0) | qcTypeId
        //Sample QC Date = current date
        sample?.udfSampleQcDate = simpleDateFormat.format(new Date())
        sample.udfNotes = smNotes
        sample.getScheduledSamples(nodeManager)?.each{it.udfNotes = smNotes}
        //Current QC Type = bean.Required QC Type
        sample.udfCurrentQcType = reqdQCType?.value
        //Initial Volume (ul) = Initial Volume (ul)
        sample.udfInitialVolumeUl = initialVolume
        //Quantity Volume (ul) = Vol for Quantity (ul)
        sample.udfQuantityVolumeUl = volForQuantity
        //Concentration (ng/ul) = Concentration (ng/ul)
        sample.udfConcentration = concentration
        //Quality Volume (ul) = Vol for Quality (ul)
        sample.udfQualityVolumeUl = volForQuality
        //Gel Diluent Volume (ul) = Gel Diluent Vol (ul)
        sample.udfGelDiluteVolumeUl = gelDiluentVol
        //Purity Volume (ul) = Vol for Purity (ul)
        sample.udfPurityVolumeUl = volForPurity
        //Absorbance 260/230 = A260/A230
        sample.udfAbsorbanceA260A230 = a260a230
        //Absorbance 260/280 = A260/A280
        sample.udfAbsorbanceA260A280 = a260a280
        //HMW gDNA Eval = HMW gDNA (Y/N)
        sample.udfSampleHMWgDNAYN = hMWgDNAYN.value
        //Quality Score = Quality Score
        sample.udfQualityScore = qualityScore
        //rRNA Ratio = rRNA Ratio
        sample.udfRRnaRatio = rRNARatio
        //Volume (ul) = Available Volume (ul)
        sample.udfVolumeUl = availableVolume
        //Sample QC Result = QC Status
        sample.udfSampleQCResult = qcStatus.value
        SampleAnalyte analyte = sample.sampleAnalyte
        if (!analyte.isOnPlate)
            sample.udfContainerSampleQcResult = qcStatus.value
        if (qcStatus?.value == Analyte.FAIL)
            sample.udfSampleQCFailureMode = failureMode.value
        sample.udfSampleQcNotes = qcComments
    }

    void populateBean(int index, SampleAnalyte analyte, List<Integer> sowItemQcTypeIds, Map<String, FreezerContainer> freezerContainerMap) {
        boolean isOnPlate = analyte.containerNode?.containerTypeEnum == ContainerTypes.WELL_PLATE_96
        ClaritySample sample = analyte.claritySample
        //Counter
        sampleNumber = index
        //ITS Sample ID  <-- sample udf “PMO Sample Id”
        pmoSampleId = sample?.pmoSampleIdAsString
        //Sample Lims ID <-- sample lims id
        sampleLimsId = sample?.id
        //Source Barcode <-- container lims id if plate; container name if tube
        sampleBarcode = analyte?.containerName
        //Source Location<-- well location if plate
        sampleLocation = isOnPlate ? analyte?.containerLocation?.rowColumn?.replace(":", '') : ''
        //Source Labware <-- “Tube” or “96 well plate”
        sampleLabware = analyte?.containerNode?.containerTypeEnum?.value
        //Sequencing Product <-- project udf “Sequencing Product Name”
        seqProduct = sample?.sequencingProject?.udfSequencingProductName
        //Organism <-- project udf “Sequencing Project Name”
        seqProject = sample?.sequencingProject?.udfSequencingProjectName
        //Date <-- sample udf “Sample Receipt Date”
        date = sample?.udfSampleReceiptDate?:''
        //Comments <-- sample udf “Sample Receipt Notes”
        comments = sample?.udfSampleReceiptNotes?:''
        //SM Instructions <-- sample udf “SM Instructions”
        smInstructions = sample?.udfSmInstructions?:''
        smNotes = sample?.udfNotes?:''
        //Required QC Type <-- ((“all sow items qc type id”  | “cumulative qc type id” ) ^ “cumulative qc type id”) | (sample udf “QC Type Id”? sample udf “QC Type Id”:0)
        reqdQCType = SampleQcProcess.dropDownQcTypes
        QcTypeCv qcTypeCv = ((PmoSample) analyte.claritySample).calculateRequiredQcType(sowItemQcTypeIds)
        reqdQCType.value = qcTypeCv ? qcTypeCv.qcType : ''
        //Collaborator Volume (ul) <-- sample udf “Collaborator Volume (ul)”
        collaboratorVolume = sample?.udfCollaboratorVolumeUl?:''
        //Collaborator Concentration(ng/ul) <-- sample udf “Collaborator Concentration (ng/ul)”
        collaboratorConcentration = sample?.udfCollaboratorConcentration?:''
        //Initial Volume (ul) <-- sample udf Initial Volume (ul)
        initialVolume = sample?.udfInitialVolumeUl?:0
        //Vol for Quantity (ul) <-- sample udf Quantity Volume (ul)
        volForQuantity = sample?.udfQuantityVolumeUl?:0

        //Vol for Quantity (ul) <-- sample udf Quantity Volume (ul)
        volForQuality = sample?.udfQualityVolumeUl?:0
        //Gel Diluent Vol (ul) <-- sample udf Gel Diluent Volume (ul)
        gelDiluentVol = sample?.udfGelDiluteVolumeUl?:0
        //Vol for Purity (ul) <-- sample udf Purity Volume (ul)
        volForPurity = sample?.udfPurityVolumeUl?:0
        //A260/A230 <-- sample udf Absorbance 260/230a260a230 = sample?.udfAbsorbanceA260A230
        //A260/A280 <-- sample udf Absorbance 260/280
        a260a280 = sample?.udfAbsorbanceA260A280?:''
        a260a230 = sample?.udfAbsorbanceA260A230?:''
        //Quality Score <-- sample udf Quality Score
        qualityScore = sample?.udfQualityScore?:''
        //rRNA Ratio <-- sample udf rRNA Ratio
        rRNARatio = sample?.udfRRnaRatio?:''
        //QC Status <-- Pass/Fail
        qcStatus = SampleQcProcess.dropDownPassFail
        //Failure Mode <-- uss.dt_sample_qc_failure_mode_cv
        failureMode = SampleQcProcess.dropDownSampleQCFailureModes
        //HMW gDNA (Y/N) <-- Y/N/Marginal
        hMWgDNAYN = SampleQcProcess.dropDownHMWgDNA
        //HMW gDNA (Y/N) <-- sample udf HMW gDNA Eval
        hMWgDNAYN.value = sample?.udfSampleHMWgDNAYN?:''
        //QC Comments <-- sample udf Sample QC Notes
        qcComments = sample?.udfSampleQcNotes?:''
        FreezerContainer container = freezerContainerMap[analyte.containerName]
        freezerPath = container?.location
        freezerOperator = container?.operatorFullName
        freezerStatus = container?.checkInStatus
    }

    void populateRequiredFields(int passCount){
        initialVolume = 1000
        volForQuality = 10
        volForPurity = 10
        volForQuantity = 10
        concentration = 1
        gelDiluentVol = 10
        a260a230 = 1
        a260a280 = 1
        def value = reqdQCType.value
        reqdQCType = new DropDownList()
        reqdQCType.value = value
        hMWgDNAYN = new DropDownList()
        hMWgDNAYN.value = SampleQcProcess.dropDownHMWgDNA.controlledVocabulary[0]
        qualityScore = 1
        rRNARatio = 1
        qcStatus = new DropDownList()
        failureMode = new DropDownList()
        if(passCount > 0)
            qcStatus.value = Analyte.PASS
        else{
            qcStatus.value = Analyte.FAIL
            failureMode.value = SampleQcProcess.dropDownSampleQCFailureModes.controlledVocabulary[0]
        }
        availableVolume = initialVolume - (volForPurity + volForQuality + volForQuantity)
    }
}
