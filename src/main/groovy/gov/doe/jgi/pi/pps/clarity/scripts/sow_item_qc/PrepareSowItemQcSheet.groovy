package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/26/15.
 */
class PrepareSowItemQcSheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PrepareSowItemQcSheet.class)
    static final String SOW_ITEM_QC_WORKSHEET_TEMPLATE = 'resources/templates/excel/SowQCWorksheet.xls'

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.cacheRootSamples()
        ExcelWorkbook workbook = generateSowQCWorksheet()
        def fileNode = processNode.outputResultFiles.find { it.name.contains(SowItemQc.SCRIPT_GENERATED_SOW_QC_WORKSHEET)}
        logger.info "Writing SOW QC Worksheet to ${fileNode.id}"
        workbook?.store(process.nodeConfig, fileNode.id)
    }

    ExcelWorkbook generateSowQCWorksheet(){
        process.sortInputAnalytes()
        SowQCWorksheet worksheet = new SowQCWorksheet(SOW_ITEM_QC_WORKSHEET_TEMPLATE, process)
        return worksheet?.populateWorksheet()
    }
}
