package gov.doe.jgi.pi.pps.clarity.scripts.requeue_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by tlpaley on 8/19/15.
 * See requirement 1 in
 * https://docs.google.com/document/d/1jEhFJ0qHecvtafFTJ9fLLSYQE-jg29xXbtbEIoZcffY/edit
 */
class RequeueQcProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.REQUEUE_QC

    boolean testMode = false

    RequeueQcProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation':RequeueQcPreProcessValidation,
                'Process Requeue':RequeueQc,
                'Route to Next Workflow':RequeueQcRouteToWorkflow
        ]
    }

}
