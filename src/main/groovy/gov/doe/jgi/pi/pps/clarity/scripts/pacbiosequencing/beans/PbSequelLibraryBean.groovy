package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing.beans

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class PbSequelLibraryBean {
	@FieldMapping(header = 'Experiment Name', cellType = CellTypeEnum.STRING)
	public String experimentName
	@FieldMapping(header = 'Experiment Id', cellType = CellTypeEnum.STRING)
	public String experimentId
	@FieldMapping(header = 'Experiment Description', cellType = CellTypeEnum.STRING)
	public String experimentDescription
	@FieldMapping(header = 'Run Name', cellType = CellTypeEnum.STRING)
	public String runName
	@FieldMapping(header = 'System Name', cellType = CellTypeEnum.STRING)
	public String systemName
	@FieldMapping(header = 'Run Comments', cellType = CellTypeEnum.STRING)
	public String runComments
	@FieldMapping(header = 'Is Collection', cellType = CellTypeEnum.STRING)
	public String isCollection
	@FieldMapping(header = 'Sample Well', cellType = CellTypeEnum.STRING)
	public String sampleWell
	@FieldMapping(header = 'Sample Name', cellType = CellTypeEnum.STRING)
	public String sampleName
	@FieldMapping(header = 'Sequencing Mode', cellType = CellTypeEnum.STRING)
	public String sequencingMode
	@FieldMapping(header = 'Movie Time per SMRT Cell (hours)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal movieTimeHrs
	@FieldMapping(header = 'Sample Comment', cellType = CellTypeEnum.STRING)
	public String sampleComment
	@FieldMapping(header = 'Insert Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigInteger insertSizeBp
	@FieldMapping(header = 'On-Plate Loading Concentration (pM)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal onPlateLoadingConcentrationPm
	@FieldMapping(header = 'Use Predictive Loading', cellType = CellTypeEnum.STRING)
	public String usePredictiveLoading
	@FieldMapping(header = 'Loading Target (P1 + P2)', cellType = CellTypeEnum.STRING)
	public String loadingTarget
	@FieldMapping(header = 'Maximum Loading Time (hours)', cellType = CellTypeEnum.STRING)
	public String maximumLoadingTimeHrs
	@FieldMapping(header = 'Size Selection', cellType = CellTypeEnum.STRING)
	public String sizeSelection
	@FieldMapping(header = 'Template Prep Kit Box Barcode', cellType = CellTypeEnum.STRING)
	public String templatePrepKitBoxBarcode
	@FieldMapping(header = 'Binding Kit Box Barcode', cellType = CellTypeEnum.STRING)
	public String bindingKitBoxBarcode
	@FieldMapping(header = 'Sequencing Kit Box Barcode', cellType = CellTypeEnum.STRING)
	public String sequencingKitBoxBarcode
	@FieldMapping(header = 'DNA Control Complex Box Barcode', cellType = CellTypeEnum.STRING)
	public String dnaControlComplexBoxBarcode
	@FieldMapping(header = 'Automation Name', cellType = CellTypeEnum.STRING)
	public String automationName
	@FieldMapping(header = 'Automation Parameters', cellType = CellTypeEnum.STRING)
	public String automationParameters
	@FieldMapping(header = 'Generate CCS Data', cellType = CellTypeEnum.STRING)
	public String generateCcsData
	@FieldMapping(header = 'Sample is Barcoded', cellType = CellTypeEnum.STRING)
	public String sampleIsBarcoded
	@FieldMapping(header = 'Barcode Set', cellType = CellTypeEnum.STRING)
	public String barcodeSet
	@FieldMapping(header = 'Same Barcodes on Both Ends of Sequence', cellType = CellTypeEnum.STRING)
	public String sameBarcodesOnBothEndsOfSequence
	@FieldMapping(header = 'Barcode Name', cellType = CellTypeEnum.STRING)
	public String barcodeName
	@FieldMapping(header = 'Bio Sample Name', cellType = CellTypeEnum.STRING)
	public String bioSampleName
	@FieldMapping(header = 'Pipeline Id', cellType = CellTypeEnum.STRING)
	public String pipelineId
	@FieldMapping(header = 'Analysis Name', cellType = CellTypeEnum.STRING)
	public String analysisName
	@FieldMapping(header = 'Entry Points', cellType = CellTypeEnum.STRING)
	public String entryPoints
	@FieldMapping(header = 'Task Options', cellType = CellTypeEnum.STRING)
	public String taskOptions

	static final String[] TRUE_FALSE = [Boolean.TRUE.toString().toUpperCase(), Boolean.FALSE.toString().toUpperCase()]
	static final String[] SEQ_MODELS = [SequencerModelTypeCv.SEQUEL.value, SequencerModelTypeCv.SEQUEL_II.value]
	String newLine = ClarityProcess.WINDOWS_NEWLINE

	def validate() {
		def errorMsg = StringBuilder.newInstance()
		if ( !(systemName in SEQ_MODELS)) {
			errorMsg << "invalid System Name value <$systemName>, "
			errorMsg << "expected values $SEQ_MODELS $newLine"
		}
		if (!(isCollection in TRUE_FALSE)) {
			errorMsg << "invalid Is Collection value <$isCollection>, "
			errorMsg << "expected values $TRUE_FALSE $newLine"
		}
		def udfName = ClarityUdf.ANALYTE_ACQUISITION_TIME.value
		if (!movieTimeHrs || movieTimeHrs < 0.1) {
			errorMsg << "invalid movieTimeHrs value <$movieTimeHrs>, "
			errorMsg << "<$udfName> value should be >= 6 $newLine"
		}
		if (movieTimeHrs && systemName == SequencerModelTypeCv.SEQUEL.value && movieTimeHrs > 20) {
			errorMsg << "invalid movieTimeHrs value <$movieTimeHrs>, "
			errorMsg << "<$udfName> value should be <= 1200 $newLine"
		}
		if (movieTimeHrs && systemName == SequencerModelTypeCv.SEQUEL_II.value && movieTimeHrs > 30) {
			errorMsg << "invalid movieTimeHrs value <$movieTimeHrs>, "
			errorMsg << "$udfName value should be <= 1800 $newLine"
		}
		if (!insertSizeBp || insertSizeBp < 10) {
			udfName = ClarityUdf.ANALYTE_PACBIO_INSERT_SIZE.value
			errorMsg << "invalid <$udfName> value <$insertSizeBp>, "
			errorMsg << "<$udfName> value should be >= 10 $newLine"
		}
		if (onPlateLoadingConcentrationPm == null) {
			udfName = ClarityUdf.ANALYTE_PACBIO_LOADING_CONCENTRATION_NM.value
			errorMsg << "invalid <$udfName> value <$onPlateLoadingConcentrationPm> $newLine"
		}
		if (sequencingKitBoxBarcode && !sequencingKitBoxBarcode.isNumber()) {
			udfName = ClarityUdf.PROCESS_DNA_SEQUENCING_REAGENT_KIT.value
			errorMsg << "invalid <$udfName> value <$sequencingKitBoxBarcode> $newLine"
		}
		if (dnaControlComplexBoxBarcode && !dnaControlComplexBoxBarcode.isNumber()) {
			udfName = ClarityUdf.PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT.value
			errorMsg << "invalid <$udfName> value <$dnaControlComplexBoxBarcode> $newLine"
		}
		if (!(sampleIsBarcoded in TRUE_FALSE)) {
			errorMsg << "invalid Sample is Barcoded value <$sampleIsBarcoded>, "
			errorMsg << "expected values $TRUE_FALSE $newLine"
		}
		if (errorMsg) {
			def startMsg = "Please correct the <$sampleName $sampleWell> value(s):$newLine"
			errorMsg.insert(0, startMsg)
		}
		errorMsg
	}
}
