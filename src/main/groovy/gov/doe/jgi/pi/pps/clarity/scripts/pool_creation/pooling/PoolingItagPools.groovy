package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.BasicPoolingAlgorithm
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation

class PoolingItagPools extends BasicPoolingAlgorithm {

    PoolingItagPools(List<LibraryInformation> members) {
        super(members)
    }

    @Override
    int getDop() {
        return 2
    }

    @Override
    def getIndexToCompare(LibraryInformation member){
        return member.indexName
    }
}
