package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Clarity Illumina MiSeq, NextSeq, HiSeq Support Requirements
 * https://docs.google.com/document/d/15g6IcLuFzb-1Q8LtFCdfukaT2f2l0m32j9b8yo3_51Q/edit#
 * Clarity Illumina NovaSeq Support Requirements
 * https://docs.google.com/document/d/1CmDCgNkQwyHQRqk4Gx3LgeWzNYiPuJ6X9X6BKPffq9w/edit
 * Clarity Illumina HiSeq 2000, 1T Requirements
 * https://docs.google.com/document/d/1DhaLc-e3zebpmqOYUSMp19CcsvrP5QPlB5iFlsZNgAs/edit#heading=h.7bnr80mxwz1b
 */

class Sequencing extends ClarityProcess {

    static ProcessType processType = ProcessType.SQ_SEQUENCING

    final static String DILUTION_UPLOAD_FILE = 'Upload Dilution Worksheet'
    final static String DILUTION_DOWNLOAD_FILE = 'Download Dilution Worksheet'
    final static String PREFIX_SQ_SEQUENCING = 'SQ Sequencing'

    boolean testMode = false
    ExcelWorkbook excelWorkbookCached
    SequencerModelCv sequencerModelCvCached

	Sequencing(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation':SqPreProcessValidation,
                'PrepareDilutionSheet':SqPrepareDilutionSheet,
                'ProcessDilutionSheet':SqProcessDilutionSheet,
                'RouteToNextWorkflow':SqRouteToWorkflow
        ]
    }

    SequencerType getSequencerType() {
        if (isNovaSeq) {
            return new NovaSeq(this)
        }
        if (isNextSeq) {
            return new NextSeq(this)
        }
        if (isHiSeqRapid) {
            return new HiSeqRapid(this)
        }
        if (isMiSeq) {
            return new MiSeq(this)
        }
        //PPS-5150: inactivate discontinued run modes
//        if (isHiSeq1Tb) {
//            return new HiSeq1Tb(this)
//        }
        postErrorMessage("Unsupported sequencer ${sequencerModelCv.sequencerModel}")
        return null
    }

    RunModeCv getRunModeCv() {
        return inputAnalytes[0].runModeCv // PPV: all inputs have the same run mode
    }

    SequencerModelCv getSequencerModelCv() {
        if (sequencerModelCvCached != null) {
            return sequencerModelCvCached
        }
        sequencerModelCvCached = runModeCv.sequencerModel
        return sequencerModelCvCached
    }

    Boolean getIsNovaSeq() {
        return sequencerModelCv.isIlluminaNovaSeq
    }

    //PPS-5150: inactivate discontinued run modes
//    Boolean getIsHiSeq1Tb() {
//        return sequencerModelCv.isIlluminaHiSeq1Tb
//    }

    Boolean getIsMiSeq() {
        return sequencerModelCv.isIlluminaMiSeq
    }

    Boolean getIsNextSeq() {
        return sequencerModelCv.isNextSeq
    }

    Boolean getIsHiSeqRapid() {
        return sequencerModelCv.isIlluminaHiSeqRapid
    }

    ExcelWorkbook getUploadedExcelWorkbook() {
        if (excelWorkbookCached)
            return excelWorkbookCached
        ArtifactNode resultFile = getFileNode(DILUTION_UPLOAD_FILE)
        excelWorkbookCached = new ExcelWorkbook(resultFile.id, testMode)
        excelWorkbookCached.load()
        excelWorkbookCached
    }

    List getTableBeans() {
        List data = uploadedExcelWorkbook.sections.values()[0].getData() as List
        List tableBeans = []
        data.each { tableBean ->
            if (!tableBean.libraryStockName) {
                return
            }
            validateTableBean(tableBean)
            tableBeans << tableBean
        }
        tableBeans
    }

    static def validateTableBean(def tableBean) {
        boolean isNovaSeq = tableBean.runType.contains(SequencerModelCv.NOVASEQ)
        def errorMsg = 'Uploaded Dilution worksheet:'
        if (tableBean.phixSpikeIn == null) {
            postErrorMessage("$errorMsg Field 'PhiX Spike in %' must be set.")
        }
        if (tableBean.libraryConvFactor == null && !isNovaSeq) {
            postErrorMessage("$errorMsg Field 'Library conv. factor' must be set.")
        }
        if (tableBean.dnaWa01Ul == null) {
            postErrorMessage("$errorMsg DNA WA01 (µL) is invalid, check that Conc (pM) is populated.")
        }
    }

    static def getFreezerLocation(List<FreezerContainer> freezerContainers, Analyte inputAnalyte) {
        return freezerContainers.find{it.barcode == inputAnalyte.containerName}?.location
    }

    def findTableBean(ClarityPhysicalRunUnit pru, boolean validateLane) {
        def tableBean = tableBeans.find{
            (validateLane?(pru.lane == it.lane):true) && pru.parentAnalyte.name == it.libraryStockName
        }
        if (!tableBean) {
            postErrorMessage("""
Uploaded Dilution worksheet:
Cannot find an associated record for the '$pru.name' analyte.
""")
        }
        String error = tableBean?.validateBean()
        if(error)
            throw new RuntimeException(error)
        tableBean
    }

    List<Analyte> getFailedQcAnalytes() {
        return outputAnalytes.findAll{ !it.systemQcFlag }
    }

}
