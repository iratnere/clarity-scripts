package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel

import gov.doe.jgi.pi.pps.clarity.cv.SowItemTypeCv
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.SowItemQc
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes

/**
 * Created by lvishwas on 6/16/2015.
 */
class TubeSowQCTableBean {
    @FieldMapping(header = 'ITS Sample ID', cellType = CellTypeEnum.NUMBER)
    public def itsSampleId
    @FieldMapping(header = 'Sample Lims ID', cellType = CellTypeEnum.STRING)
    public def sampleLimsId
    @FieldMapping(header = 'Source Barcode', cellType = CellTypeEnum.STRING)
    public def sampleBarcode
    @FieldMapping(header = 'Concentration (ng/ul)', cellType = CellTypeEnum.NUMBER)
    public def concentrationNgUl
    @FieldMapping(header = 'Initial Sample  Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def initSampleVolume
    @FieldMapping(header = 'Initial  Sample Mass (ng)', cellType = CellTypeEnum.FORMULA)
    public def initSampleMass
    @FieldMapping(header = 'Available Volume (ul)', cellType = CellTypeEnum.FORMULA)
    public def availableVolume
    @FieldMapping(header = 'Available Mass (ng)', cellType = CellTypeEnum.FORMULA)
    public def availableMass
    @FieldMapping(header = 'Library Queue', cellType = CellTypeEnum.STRING)
    public def libraryQueue
    @FieldMapping(header = 'HMW gDNA', cellType = CellTypeEnum.STRING)
    public def hmwGDNA_YN
    @FieldMapping(header = 'Quality Score', cellType = CellTypeEnum.STRING)
    public def qualityScore
    @FieldMapping(header = 'rRNA Ratio', cellType = CellTypeEnum.STRING)
    public def rRnaRatio
    @FieldMapping(header = 'QC Comments', cellType = CellTypeEnum.STRING)
    public def qcComments
    @FieldMapping(header = 'Target Aliquot Mass (ng)', cellType = CellTypeEnum.NUMBER, required = true)
    public def targetAliquotMass
    @FieldMapping(header = 'Target Max Volume (ul)', cellType = CellTypeEnum.NUMBER)
    public def targetMaxVolume
    @FieldMapping(header = 'Target Aliquot Volume (ul)', cellType = CellTypeEnum.FORMULA)
    public def targetAliquotVolume
    @FieldMapping(header = 'Adjusted Aliquot Volume (ul)', cellType = CellTypeEnum.FORMULA, required = true)
    public def adjustedAliquotVolume
    @FieldMapping(header = 'SOW QC Status', cellType = CellTypeEnum.DROPDOWN)
    public def sowQCStatus
    @FieldMapping(header = 'Failure Mode', cellType = CellTypeEnum.DROPDOWN)
    public def failureMode
    @FieldMapping(header = 'SM QC instruction from PM', cellType = CellTypeEnum.STRING)
    public def smQCInstructionFromPM
    @FieldMapping(header = 'SM Notes', cellType = CellTypeEnum.STRING)
    public def smNotes
    @FieldMapping(header = 'Cumulative Used Volume (ul) - DO NOT EDIT', cellType = CellTypeEnum.FORMULA)
    public def cumulativeUsedVolume
    @FieldMapping(header = 'SOW Item ID', cellType = CellTypeEnum.NUMBER)
    public def sowItemId
    @FieldMapping(header = 'Group Name', cellType = CellTypeEnum.STRING)
    public def groupName
    @FieldMapping(header = 'Isotope Label', cellType = CellTypeEnum.STRING)
    public def isotopeLabel

    static final String DEFAULT_SOW_QC_FAILURE_MODE = 'Sample Failed QC'

    def validate(){
        def errors = ''
            if (targetAliquotMass == null)
                errors += "Target Aliquot Mass (ng) is a required field for ${sampleLimsId}\n"
        if(!sowQCStatus || !sowQCStatus.value)
            errors += "SOW QC Status is a required field for ${sampleLimsId}\n"
        if(sowQCStatus?.value == Analyte.FAIL && !failureMode?.value)
            errors += "Failure Mode is required if the SOW QC status is \"Fail\" for ${sampleLimsId}\n"
        if(adjustedAliquotVolume == null || parseToBigDecimalError(adjustedAliquotVolume))
            errors += "Adjusted Aliquot Volume (ul) is required field for ${sampleLimsId}\n"
        return errors
    }

    void copyDataFromRootSample(PmoSample rootSample){
        //ITS Sample ID <--- sample udf “PMO Sample Id”
        this.itsSampleId = rootSample?.pmoSampleId
        //Sample Lims Id <--- sample lims id
        sampleLimsId = rootSample?.id
        //Source Barcode <--- sample container name
        sampleBarcode = rootSample?.containerName
        //Concentration (ng/ul) <--- sample udf “Concentration (ng/ul)
        concentrationNgUl = rootSample?.udfConcentration
        //Initial Sample  Volume (ul) <--- sample udf “Volume (ul)”
        initSampleVolume = rootSample?.udfVolumeUl
        //HMW gDNA (Y/N/Marginal) <--- sample udf “HMW gDNA Eval”
        hmwGDNA_YN = rootSample?.udfSampleHMWgDNAYN
        //Quality Score <--- sample udf “Quality Score”
        qualityScore = rootSample?.udfQualityScore
        //rRNA Ratio <--- sample udf “rRNA Ratio”
        rRnaRatio = rootSample?.udfRRnaRatio
        //QC Comments <--- sample udf “Sample QC Notes”
        qcComments = rootSample?.udfSampleQcNotes
        smNotes = rootSample?.udfNotes
        groupName = rootSample?.udfGroupName?:''
        isotopeLabel = rootSample?.udfIsotopeLabel?:''
    }

    void copyDataFromScheduledSample(ScheduledSample scheduledSample, PmoSample rootSample){
        //Library Queue <--- scheduled sample udf “Library Creation Queue”
            libraryQueue = scheduledSample?.libraryCreationQueue?.libraryCreationQueue
        LibraryCreationQueueCv libraryQueue = LibraryCreationQueueCv.findByLibraryCreationQueue(libraryQueue)
        boolean sampleOnPlate = scheduledSample.sampleArtifactNode.containerNode.containerTypeEnum == ContainerTypes.WELL_PLATE_96
            //Target Aliquot Mass (ng) <--- '' if sow item type ==”Custom Aliquot”
            if(scheduledSample?.udfSowItemType == SowItemTypeCv.CUSTOM_ALIQUOT.value)
                targetAliquotMass = null
            //Target Aliquot Mass (ng) <--- '' if sow item type ==”BLR and Receipt”
            else if(scheduledSample?.udfSowItemType == SowItemTypeCv.BLR_AND_RECEIPT.value)
                targetAliquotMass = null
            else
            //Target Aliquot Mass (ng) <--- tube “Mass (ng) req. per attempt” for corresponding library queue from uss.dt_library_creation_queue_cv
                targetAliquotMass = sampleOnPlate?libraryQueue?.plateTargetMassLibTrialNg:libraryQueue?.tubeTargetMassLibTrialNg
            targetMaxVolume = sampleOnPlate?libraryQueue?.plateTargetVolumeLibTrialUl:libraryQueue?.tubeTargetVolumeLibTrialUl
        //SOW QC Status <--- Pass/Fail
        String sampleQCResult = rootSample?.udfContainerSampleQcResult
        if(sampleQCResult == Analyte.FAIL){
            sowQCStatus.value = sampleQCResult
            //Failure Mode <--- uss.dt_sow_item_failure_mode_cv
            failureMode.value = DEFAULT_SOW_QC_FAILURE_MODE
        }
        if(sampleQCResult == Analyte.PASS && rootSample?.sampleArtifactNode?.containerNode?.containerTypeEnum == ContainerTypes.WELL_PLATE_96){
            sowQCStatus.value = sampleQCResult
        }
        //ITS Sample ID <--- sample udf “PMO Sample Id”
        smQCInstructionFromPM = scheduledSample?.udfSmInstructions
        //ITS Sample ID <--- sample udf “PMO Sample Id”
        sowItemId = scheduledSample?.sowItemId
    }

    void prepareDropDowns(){
        sowQCStatus = SowItemQc.dropDownPassFail
        failureMode = SowItemQc.dropDownSowQCFailureModes
    }

    void populateBean(SampleAnalyte analyte){
        prepareDropDowns()
        ScheduledSample scheduledSample = analyte.claritySample
        PmoSample rootSample = scheduledSample?.pmoSample
        copyDataFromRootSample(rootSample)
        copyDataFromScheduledSample(scheduledSample, rootSample)
    }

    void updateUdfs(SampleAnalyte analyte){
        analyte.claritySample.udfSowItemQcResult = sowQCStatus.value
        if(sowQCStatus?.value == Analyte.FAIL)
            analyte.claritySample.udfSowItemQcFailureMode = failureMode.value
        analyte.claritySample.udfTargetAliquotMassNg = targetAliquotMass
        analyte.claritySample.udfNotes = smNotes
    }

    static def parseToBigDecimalError(def object) {
        try{
            object as BigDecimal
        } catch (e) {
            return e
        }
        return null
    }

    Long getSowId(){
        return (sowItemId as BigDecimal).longValue()
    }
}
