package gov.doe.jgi.pi.pps.util.util

import gov.doe.jgi.pi.pps.util.PpsError
import gov.doe.jgi.pi.pps.util.PpsException
import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope

import java.util.concurrent.ConcurrentHashMap

@Component
@RequestScope
class RequestCash {
    Map<String, Object> data = new ConcurrentHashMap<>()
    def couchdbResponse
    def couchdbQueries
    def dapSeqJson
    Integer statusCode
    List<PpsError> errorMessages = []


    void addErrorMessage(String s) {
        errorMessages << new PpsError("", s)
    }

    void exit(Integer status) {
        throw new PpsException(errorMessages)
    }
}
