package gov.doe.jgi.pi.pps.clarity.welladdress

import org.apache.commons.lang.NotImplementedException

/**
 * Created by dscott on 6/20/2014.
 */
class WellAddress384Iterator implements Iterator<gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384> {

    boolean skipCorners = false
    boolean columnsFirst = false

    private Integer colIndex = 0
    private Integer rowIndex = 0
    private gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384 nextAddress
    private boolean initialized = false

    private void initialize() {
        if (!initialized) {
            if (skipCorners) {
                setNextAddress()
            } else {
                nextAddress = new gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384(colIndex: colIndex, rowIndex: rowIndex)
            }
            initialized = true
        }
    }

    private void setNextAddress() {
        nextAddress = null
        while (nextAddress == null && increment()) {
            if (!skipCorners || !onCorner()) {
                nextAddress = new gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384(colIndex:colIndex,rowIndex:rowIndex)
            }
        }
    }

    private boolean increment() {
        if (columnsFirst) {
            rowIndex++
            if (rowIndex > gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxRowIndex) {
                rowIndex = 0
                colIndex++
            }
            return colIndex <= gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxColIndex
        } else {
            colIndex++
            if (colIndex > gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxColIndex) {
                colIndex = 0
                rowIndex++
            }
            return rowIndex <= gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxRowIndex
        }
    }

    private boolean onCorner() {
        return  (gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.minRowIndex == rowIndex || gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxRowIndex == rowIndex) &&
                (gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.minColIndex == colIndex || gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384.maxColIndex == colIndex)
    }

    synchronized boolean hasNext() {
        initialize()
        return nextAddress != null
    }

    synchronized gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384 next() {
        initialize()
        gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress384 address = nextAddress
        if (address != null) {
            setNextAddress()
        }
        return address
    }

    void remove() {
        throw new NotImplementedException()
    }

}
