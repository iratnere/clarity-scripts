package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.process.ProcessFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryNameReservationService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/14/16.
 */
class LibraryPoolsMigrator extends LibraryStocksMigrator {
    static final logger = LoggerFactory.getLogger(LibraryPoolsMigrator.class)
    static final BigDecimal DEFAULT_VOLUME_UL = 10
    static final BigDecimal DEFAULT_VOLUME_USED_UL = 10
    static Map<ClarityUdf, Object> defaultUdfs

    LibraryPoolsMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager)
        parentMigrator = new LibraryStocksMigrator(clarityNodeManager)
    }

    static{
        defaultUdfs = [:].withDefault {[:]}
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_LIBRARY_QC_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.ANALYTE_VOLUME_USED_UL] = DEFAULT_VOLUME_USED_UL
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM] = 1.7
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.ANALYTE_CONCENTRATION_NG_UL] = 1.3
        defaultUdfs[Migrator.PARENT_CONTAINER_UDFS][ClarityUdf.CONTAINER_LIBRARY_QC_RESULT] = Analyte.PASS
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerTypes) {
        return Stage.POOL_CREATION
    }

    @Override
    void checkExistingAnalytes(List<OnboardingBean> beans) {
        pruBeans.each{String flowCellLane, List<OnboardingBean> laneBeans ->
            String key = laneBeans.collect{it.getLabLibraryName()}.join('_')
            libraryBeans[key] << laneBeans?.unique()
        }
        libraryBeans.each{ String memberNames, List<List<OnboardingBean>> memberBeans ->
            Analyte pool = lookUpPoolForLibraries(clarityNodeManager, memberBeans[0])
            if(pool)
                memberBeans.each { mbeans ->
                    mbeans.each { it.addAnalyte(pool) }
                }
        }
    }

    String getDestinationStage(Analyte analyte){
        if(analyte.isPacBio) {
            return Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING.uri
        }
        ScheduledSample scheduledSample = (ScheduledSample) analyte.claritySample
        return scheduledSample.runModeCv.getSequencingWorkflowUri()
    }

    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans){
        checkExistingAnalytes(beans)
        return beans.findAll{!it.libraryPool}
    }

    void updateUdfs(List<OnboardingBean> beans){
        if(!beans)
            return
        List<Analyte> analytes = beans.collect{getAnalyte(it)}
        analytes = analytes.findAll{it != null}?.unique()
        def unnamedPools = analytes.findAll{it.name.startsWith('Pool #')}
        if(unnamedPools){
            //ClarityProcess process = ProcessFactory.processInstance(analytes[0].artifactNode.parentProcessNode)
            LibraryNameReservationService libraryNameReservationService = BeanUtil.getBean(LibraryNameReservationService.class)
            libraryNameReservationService.assignLibraryNames(unnamedPools)
        }
        def defaultUdfs = this.defaultUdfs
        if(defaultUdfs)
            setDefaultUdfs(analytes, this.defaultUdfs[Migrator.SELF_UDFS], this.defaultUdfs[Migrator.SELF_CONTAINER_UDFS])
        analytes.each{ Analyte analyte ->
            ClarityLibraryPool pool = null
            if(analyte instanceof ClarityLibraryPool) {
                pool = (ClarityLibraryPool) analyte
                def dop = pool.poolMembers.size()
                pool.udfVolumeUl = pool.poolMembers.size() * DEFAULT_VOLUME_UL
                pool.udfDegreeOfPooling = dop
                def ls = pool.poolMembers.first()
                pool.udfRunMode = ls.claritySample.udfRunMode
                if (ls.udfVolumeUl > 0)
                    ls.udfVolumeUl = ls.udfVolumeUl - DEFAULT_VOLUME_USED_UL
            }
        }
        beans.each{
            it.updateUdfs(getAnalyteType())
        }
    }

    ContainerTypes getContainerType(Analyte parent){
        return ContainerTypes.TUBE
    }

    Analyte getAnalyte(OnboardingBean bean){
        Analyte analyte = bean.libraryPool
        if(!analyte)
            return bean.libraryStock
        return analyte
    }

    Class getAnalyteType(){
        return ClarityLibraryPool.class
    }

    def createAnalytesInClarity(List<OnboardingBean> onboardingBeans, boolean startingPoint = false){
        if(startingPoint)
            prepareNodeManager()
        List<Analyte> parents = parentMigrator?.createAnalytesInClarity(onboardingBeans)
        def unmergedBeans = getUnmergedBeans(onboardingBeans)
        def analytes = []
        int i=0
        if(unmergedBeans){
            parents.addAll(unmergedBeans.collect{parentMigrator.getAnalyte(it)})
            parents.unique()
            Map<ContainerNode, List<Analyte>> containerParents = [:].withDefault { [] }
            setDefaultUdfs(parents, this.defaultUdfs[Migrator.PARENT_UDFS], this.defaultUdfs[Migrator.PARENT_CONTAINER_UDFS])
            clarityNodeManager.dirtyNodes.each {it.readOnly = false}
            clarityNodeManager.httpPutDirtyNodes()
            def workflowStage = getWorkflowStage(ContainerTypes.TUBE)
            List<Analyte> ls = []
            libraryBeans.each{String libraryNames, List<List<OnboardingBean>> parentBeansList->
                List<OnboardingBean> parentBeans = parentBeansList[0]
                List<Analyte> parentAnalytes = parentBeans.collect{ it.analytes.find{it.class == parentMigrator.analyteType} }
                if(parentAnalytes?.unique()?.size() > 1)
                    containerParents[++i].addAll(parentAnalytes.collect{it.artifactNode})
                else
                    ls << parentAnalytes[0]
            }
            List<ClarityProcess> processes = createChildrenInClarity(workflowStage, containerParents)
            analytes.addAll(processes?.collect{it.outputAnalytes}?.flatten())
            analytes*.setSystemQcFlag(true)
            analytes.addAll(ls)
        }
        logger.info "Done creating pools $analytes...."
        updateBeans(unmergedBeans, analytes)
        updateUdfs(onboardingBeans)
        def anaList = onboardingBeans.collect{getAnalyte(it)}?.findAll{it != null}
        routeParentsAndChildren(anaList.collect{it.parents}?.flatten(), anaList)
        anaList.each {it.artifactNode.readOnly = false}
        clarityNodeManager.httpPutNodesBatch(anaList.collect{it.artifactNode})
        logger.info "Done updating pools $analytes...."
        if(startingPoint)
            resetNodeManager()
        return analytes.unique()
    }

    void updateBeans(List<OnboardingBean> unmergedBeans, List<Analyte> analytes){
        Map map = [:]
        analytes.each{ analyte ->
            def parents = analyte.parents.sort{a,b -> a.id <=> b.id}
            map[parents.collect{it.id}.join("_")] = analyte
        }
        pruBeans.each{String flowCellLane, List<OnboardingBean> laneBeans ->
            def libs = laneBeans.collect{parentMigrator.getAnalyte(it)}.sort{a,b -> a.id <=> b.id}
            def ls = libs.collect{it.id}.join("_")
            Analyte pool = map[ls]

            if(pool)
                laneBeans.each {it.addAnalyte(pool)}
        }
    }

    @Override
    List<ClarityProcess> createChildrenInClarity(Stage workflowStage, Map containerParents) {
        def containerLocationAnalyteIdsMap = [:]
        List<ArtifactNode> inputs = []
        List<ClarityProcess> processes = []
        containerParents.each{Integer poolNumber, List<ArtifactNode> parentNodes ->
            if(parentNodes.intersect(inputs)){
                routeAnalytes(workflowStage.uri, containerLocationAnalyteIdsMap?.values()?.flatten()?.collect { it.id }.unique())
                ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, workflowStage), containerLocationAnalyteIdsMap)
                processDetails.processUdfMap = getProcessUdfsMap(workflowStage)
                logger.info "posting Process ${workflowStage.value}"
                ProcessNode processNode = clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityPoolingProcess(processDetails))
                processes << ProcessFactory.processInstance(processNode)
                logger.info "Process execution complete $processNode"
                refreshProcessNode(processNode)
                containerLocationAnalyteIdsMap.clear()
                inputs.clear()
            }
            containerLocationAnalyteIdsMap[poolNumber] = parentNodes
            inputs.addAll(parentNodes)
            performPreProcessActions(parentNodes)
        }
        routeAnalytes(workflowStage.uri, containerLocationAnalyteIdsMap?.values()?.flatten()?.collect { it.id }?.unique() as Collection<String>)
        ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, workflowStage), containerLocationAnalyteIdsMap)
        processDetails.processUdfMap = getProcessUdfsMap(workflowStage)
        logger.info "posting Process ${workflowStage.value}"
        ProcessNode processNode = clarityNodeManager.getProcessNode(clarityNodeManager.executeClarityPoolingProcess(processDetails))
        processes << ProcessFactory.processInstance(processNode)
        logger.info "Process execution complete $processNode"
        refreshProcessNode(processNode)
        return processes
    }

    void performPreProcessActions(List<ArtifactNode> parents){
        Stage stage = Stage.AUTOMATIC_SEQUENCE_QC_REQUEUE_POOL_CREATION
        routeAnalytes(stage.uri, parents.collect{it.id})
        ProcessParams processParams = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, stage), parents)
        String processId = clarityNodeManager.executeClarityInputProcess(processParams)
        logger.info "Posted requeue process $processId"
    }
}