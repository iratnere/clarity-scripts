package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibraryannealing

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/6/2015.
 */
class LaPreProcessValidation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LaPreProcessValidation.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        Integer cleanupCount = 0
        Boolean magBeadCleanup = Boolean.FALSE
        process.inputAnalytes.each { Analyte analyte ->
            if (analyte.containerNode.isPlate) {
                process.postErrorMessage("$analyte must be a PacBio library on a tube")
            }
            if(!(analyte instanceof ClarityLibraryStock)&&!(analyte instanceof ClarityLibraryPool)){
                process.postErrorMessage("Input $analyte not supported. Expecting Library Stock or Library Pool as input.")
            }
            if (!analyte.sequencerModelCv.isPacBio) {
                process.postErrorMessage("Inputs must be PacBio library stocks")
            }
            if (analyte.udfVolumeUl == null) {
                process.postErrorMessage("Volume required for $analyte")
            }
            if (analyte.udfConcentrationNgUl == null) {
                process.postErrorMessage("Concentration required for $analyte")
            }
            if (analyte.udfMagBeadCleanupDate) {
                magBeadCleanup = Boolean.TRUE
                cleanupCount++
            }
        }
        if (magBeadCleanup) {
            process.postWarningMessage("${ProcessType.PACBIO_MAGBEAD_CLEANUP.value} was run on $cleanupCount inputs")
        }
    }
}