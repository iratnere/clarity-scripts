package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction

class PoolingFactory {
    static final int MAX_POOL_SIZE_COLOR_BALANCING = 20

    static Pooling getPoolingForDataCreation(String groupKey, List<LibraryInformation> members, Map<String, LaneFraction> nameToFraction = null){
        if(groupKey.contains(LibraryInformation.FRACTIONAL_LANE))
            return new PoolingIlluminaSingleIndexes(members)
        if(groupKey.contains(LibraryInformation.DUAL_INDEX) && members[0].dop > MAX_POOL_SIZE_COLOR_BALANCING)
            return new PoolingIlluminaDualIndexes(members)
        return new PoolingIlluminaSingleIndexes(members)
    }

    static Pooling getPoolingTechnique(String groupKey, List<LibraryInformation> members, Map<String, LaneFraction> nameToFraction = null){
        if(groupKey.contains(LibraryInformation.INTERNAL_SINGLE_CELL))
            return new PoolingInternalSingleCell(members)
        if(groupKey.contains(LibraryInformation.I_TAG))
            return new PoolingItagPools(members)
        if(groupKey.contains(LibraryInformation.EXOME_CAPTURE) || groupKey.contains(LibraryInformation.POOL_BY_DOP))
            return new PoolingPoolsByDOP(members)
        if(groupKey.contains(LibraryInformation.FRACTIONAL_LANE))
            if(groupKey.contains(LibraryInformation.PACBIO))
                return new PoolingPacBioDualIndexes(members)
            else
                return new NovaSeqFractionalLanePooling(members, nameToFraction)
        if(groupKey.contains(LibraryInformation.DUAL_INDEX)) {
            if (groupKey.contains(LibraryInformation.PACBIO))
                return new PoolingPacBioDualIndexes(members)
            else if(members[0].dop <= MAX_POOL_SIZE_COLOR_BALANCING)
                return new PoolingIlluminaDualIndexes(members)
        }
        if(groupKey.contains(LibraryInformation.PACBIO))
            return new PoolingPacBioSingleIndexes(members)
        return new PoolingIlluminaSingleIndexes(members)
    }
}
