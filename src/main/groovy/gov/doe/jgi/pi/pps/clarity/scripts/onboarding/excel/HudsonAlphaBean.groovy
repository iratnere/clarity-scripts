package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity_node_manager.node.GeneusNode
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 * Created by lvishwas on 10/16/16.
 */
class HudsonAlphaBean extends OnboardingBean{
    @FieldMapping(header = 'scientific program', cellType = CellTypeEnum.STRING)
    public def scientificProgram
    @FieldMapping(header = 'user program', cellType = CellTypeEnum.STRING)
    public def userProgram
    @FieldMapping(header = 'PI', cellType = CellTypeEnum.STRING)
    public def pi
    @FieldMapping(header = 'proposal ID', cellType = CellTypeEnum.NUMBER)
    public def proposalId
    @FieldMapping(header = 'Sample contact', cellType = CellTypeEnum.STRING)
    public def sampleContact
    @FieldMapping(header = 'Isolate Name/ID', cellType = CellTypeEnum.STRING)
    public def isolateNameID
    @FieldMapping(header = 'NCBI TAX ID', cellType = CellTypeEnum.NUMBER)
    public def ncbiTaxId
    @FieldMapping(header = 'Reference Genome', cellType = CellTypeEnum.STRING)
    public def referenceGenome
    @FieldMapping(header = 'Sample Isolation Method', cellType = CellTypeEnum.STRING)
    public def sampleIsolationMethod
    @FieldMapping(header = 'Collection Year', cellType = CellTypeEnum.NUMBER)
    public def collectionYear
    @FieldMapping(header = 'Collection Month', cellType = CellTypeEnum.STRING)
    public def collectionMonth
    @FieldMapping(header = 'Collection Day', cellType = CellTypeEnum.NUMBER)
    public def collectionDay
    @FieldMapping(header = 'Sample Isolated From', cellType = CellTypeEnum.STRING)
    public def sampleIsolationFrom
    @FieldMapping(header = 'instrument name', cellType = CellTypeEnum.STRING)
    public def instrumentName
    @FieldMapping(header = 'instrument type', cellType = CellTypeEnum.STRING)
    public def instrumentType
    @FieldMapping(header = 'physical run name', cellType = CellTypeEnum.STRING)
    public def physicalRunName
    @FieldMapping(header = 'platform name', cellType = CellTypeEnum.STRING)
    public def platformName
    @FieldMapping(header = 'flowcell barcode', cellType = CellTypeEnum.STRING)
    public def flowcellBarcode
    @FieldMapping(header = 'bases mask', cellType = CellTypeEnum.STRING)
    public def basesMask
    @FieldMapping(header = 'index type', cellType = CellTypeEnum.STRING)
    public def indexType
    @FieldMapping(header = 'lane number', cellType = CellTypeEnum.NUMBER)
    public def laneNumber
    @FieldMapping(header = 'fastq file name', cellType = CellTypeEnum.STRING)
    public def fastqFileName
    @FieldMapping(header = 'md5sum', cellType = CellTypeEnum.STRING)
    public def md5Sum
    @FieldMapping(header = 'sample id', cellType = CellTypeEnum.STRING)
    public def sampleId
    @FieldMapping(header = 'library name', cellType = CellTypeEnum.STRING)
    public def collaboratorLibraryName
    @FieldMapping(header = 'JGI PRU lims id', cellType = CellTypeEnum.STRING)
    public def jgiPruLimsId

    Analyte physicalRunUnit

    public Integer getLaneNumber(){
        return this.laneNumber as Integer
    }

    int hashCode(){
        new HashCodeBuilder(17, 37).
                append(sampleName).append(collaboratorLibraryName).append(flowcellBarcode).
                append(laneNumber).toHashCode()
    }

    String toString(){
        return """Sample Name: $sampleName,
        Collaborator Library Name: $collaboratorLibraryName,
        Flowcell Barcode : $flowcellBarcode,
        Location : ${getLaneNumber()}:1"""
    }

    void addAnalyte(Analyte analyte){
        if(!analyte)
            return
        analyte?.artifactNode?.readOnly = false
        while(analyte) {
            switch (analyte.class) {
                case ClarityPhysicalRunUnit:
                    if(physicalRunUnit)
                        return
                    physicalRunUnit = analyte
                    jgiPruLimsId = analyte.id
                    analyte = analyte.singleParent
                    break
                default:
                    super.addAnalyte(analyte)
                    return
            }
        }
    }

    List<Analyte> getAnalytes(){
        def analytes = super.analytes
        analytes << physicalRunUnit
        return analytes
    }

    GeneusNode getAnalyteForType(Class analyteClass){
        switch(analyteClass){
            case ClarityLibraryPool:
                return libraryPool?.artifactNode
            case ClarityPhysicalRunUnit:
                return physicalRunUnit?.artifactNode
            default:
                return super.getAnalyteForType(analyteClass)
        }
    }

    @Override
    String getLabLibraryName(){
        return collaboratorLibraryName
    }

    void setLabLibraryName(String labLibraryName){
        collaboratorLibraryName = labLibraryName
    }

    boolean isMergedBean(){
        this.physicalRunUnit != null
    }

    void clear(){
        super.clear()
        libraryPool = null
        physicalRunUnit=null
        jgiPruLimsId=null
    }

    String getPoolKey() {
        return "$flowcellBarcode:${getLaneNumber()}:1"
    }
}
