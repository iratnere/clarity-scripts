package gov.doe.jgi.pi.pps.clarity.scripts.requeue_library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 8/19/15.
 */
class  RequeueLibraryCreationRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryCreationRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        assignToNextWorkflow()
    }

    void assignToNextWorkflow(List<SampleAnalyte> inputAnalytes = process.inputAnalytes) {
        List<ArtifactNode> dnaSamples = []
        List<ArtifactNode> rnaSamples = []
        inputAnalytes.each { SampleAnalyte sampleAnalyte ->
            ClaritySample claritySample = sampleAnalyte.claritySample
            if (claritySample.isDnaProject) {
                dnaSamples << claritySample.sampleArtifactNode
            } else {
                rnaSamples << claritySample.sampleArtifactNode
            }
            process.removeAnalyteFromWorkflow(sampleAnalyte, ClarityWorkflow.ABANDON_WORK)
        }
        process.routeArtifactNodes(Stage.ALIQUOT_CREATION_DNA,dnaSamples)
        process.routeArtifactNodes(Stage.ALIQUOT_CREATION_RNA,rnaSamples)
    }

}
