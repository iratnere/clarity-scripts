package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SampleStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.SampleService
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/14/16.
 */
class SamplesMigrator extends Migrator {
    static final logger = LoggerFactory.getLogger(SamplesMigrator.class)

    SampleService sampleService

    SamplesMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager, null)
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerTypes) {
        return null
    }

    @Override
    def createAnalytesInClarity(List<OnboardingBean> onboardingBeans, boolean startingPoint = false){
        if(startingPoint)
            prepareNodeManager()
        List<OnboardingBean> unmergedBeans = getUnmergedBeans(onboardingBeans)
        def rootSamples = []
        assert !unmergedBeans
//        unmergedBeans?.each{ OnboardingBean bean ->
//            PmoSample rootSample = rootSamples.find{it.name == bean.pmoSampleId+''}
//            updateBean(bean, rootSample.sampleAnalyte)
//        }
        (onboardingBeans-unmergedBeans)?.each{
            rootSamples.addAll(it.pmoSample)
        }
        updateUdfs(onboardingBeans)
        clarityNodeManager.dirtyNodes.each {it.readOnly = false}
        clarityNodeManager.httpPutDirtyNodes()
        logger.info "Done updating root samples $rootSamples"
        routeParentsAndChildren(null, onboardingBeans.collect{it.pmoSample.sampleAnalyte})
        if(startingPoint)
            resetNodeManager()
        return rootSamples.collect{it.sampleAnalyte}
    }

    String getDestinationStage(Analyte analyte){
        return Stage.APPROVE_FOR_SHIPPING.uri
    }

    ContainerTypes getContainerType(Analyte parent){
        return null
    }

    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans){
        if(beans.find{!it.getSowId()}) {
            checkExistingAnalytes(beans)
        }
        return beans.findAll{!it.pmoSample}
    }

    void checkExistingAnalytes(List<OnboardingBean> beans){
        beans.each{ OnboardingBean bean ->
            Analyte existingAnalyte = lookupPmoSampleByCollaboratorName(clarityNodeManager, bean.sampleName)?.sampleAnalyte
            if(existingAnalyte) {
                bean.addAnalyte(existingAnalyte)
            }
        }
    }

    Analyte getAnalyte(OnboardingBean bean){
        return bean.pmoSample.sampleAnalyte
    }

    @Override
    Class getAnalyteType() {
        return PmoSample.class
    }

    List<Analyte> performAdditionalActions(List<Analyte> analytes, Long contactId){
        if(!analytes)
            return analytes
        Map<Long, SampleStatusCv> sampleStatusCvMap = [:]
        analytes.each{sampleStatusCvMap[it.claritySample.pmoSample.pmoSampleId] = SampleStatusCv.AVAILABLE_FOR_USE}
        StatusService statusService = BeanUtil.getBean(StatusService.class)
        statusService?.submitSampleStatus(sampleStatusCvMap,contactId)
        return analytes
    }
}
