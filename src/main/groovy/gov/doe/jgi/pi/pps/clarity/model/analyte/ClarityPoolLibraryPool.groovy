package gov.doe.jgi.pi.pps.clarity.model.analyte


import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface

/**
 * Created by dscott on 4/8/2014.
 */
class ClarityPoolLibraryPool extends ClarityLibraryPool {

    protected ClarityPoolLibraryPool(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

}


