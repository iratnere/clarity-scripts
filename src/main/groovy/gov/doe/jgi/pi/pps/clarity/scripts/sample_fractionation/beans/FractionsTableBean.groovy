package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts.SampleFractionationProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class FractionsTableBean {
	@FieldMapping(header = 'Sample Lims ID', cellType = CellTypeEnum.STRING)
	public String sampleLimsId
	@FieldMapping(header = 'Source Barcode', cellType = CellTypeEnum.STRING)
	public String sourceBarcode
	@FieldMapping(header = 'Source Labware', cellType = CellTypeEnum.STRING)
	public String sourceLabware
	@FieldMapping(header = 'Source Location', cellType = CellTypeEnum.STRING)
	public String sourceLocation
	@FieldMapping(header = 'Destination Barcode', cellType = CellTypeEnum.STRING)
	public String destinationBarcode
	@FieldMapping(header = 'Destination Labware', cellType = CellTypeEnum.STRING)
	public String destinationLabware
	@FieldMapping(header='DNA Concentration (ng/uL)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal dnaConcentrationNgUl
	@FieldMapping(header = 'Density (g/mL)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal density
    @FieldMapping(header='Transferred Volume (uL)', cellType = CellTypeEnum.NUMBER)
    public BigDecimal transferredVolumeUl
	@FieldMapping(header='Source Status', cellType = CellTypeEnum.DROPDOWN)
	public def sourceStatus

    Analyte sourceAnalyte // same as fraction, same as output analyte
	Boolean passed

	def validate(List<String> validBarcodes) {
        def errorMsg = "Please correct the errors below and upload spreadsheet again." << ClarityProcess.WINDOWS_NEWLINE
		errorMsg << "Sample Lims Id($sampleLimsId): "
        String labResult = sourceStatus?.value
        if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
            return  errorMsg << "invalid Source Status '$labResult'"
        }
        passed = labResult == Analyte.PASS

		if (passed && !destinationBarcode) {
           return  errorMsg << 'unspecified Destination Barcode' << ClarityProcess.WINDOWS_NEWLINE
		}
		if (passed && (destinationBarcode in validBarcodes)) {
			return  errorMsg << "Destination Barcode<$destinationBarcode> is not unique" << ClarityProcess.WINDOWS_NEWLINE
		}
		if (destinationBarcode)
			validBarcodes << destinationBarcode
        if (passed && (!transferredVolumeUl || SampleFractionationProcess.parseToBigDecimalError(transferredVolumeUl) || transferredVolumeUl <= 0)) {//PPS-5171: Sample Fractionation Tube Transfer Sheet allows negative Volumes
			return errorMsg << "invalid Transferred Volume(ul) $transferredVolumeUl" << ClarityProcess.WINDOWS_NEWLINE
        }
       return null
	}
}