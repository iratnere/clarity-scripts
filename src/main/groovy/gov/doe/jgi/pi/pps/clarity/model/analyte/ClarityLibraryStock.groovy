package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryStockFailureModeCv
import gov.doe.jgi.pi.pps.clarity.domain.RunModeCv
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.util.ClarityStringUtils
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException
import groovy.transform.PackageScope

/**
 * Created by dscott on 4/8/2014.
 */
class ClarityLibraryStock extends Analyte {

    static final String WINDOWS_NEWLINE = '\r\n'

    static final String[] getActiveLibraryStockFailureModes() {
        return LibraryStockFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }
    static final String[] EXCLUDED_FAILURE_MODES = ['Abandoned Work']
    static final String[] getValidLibraryStockFailureModes() {
        return getActiveLibraryStockFailureModes()?.minus(EXCLUDED_FAILURE_MODES)
    }

    @PackageScope
    ClarityLibraryStock(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    BigDecimal getUdfActualTemplateSizeBp() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf)
    }

    void setUdfActualTemplateSizeBp(BigDecimal actualTemplateSizeBp) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_ACTUAL_TEMPLATE_SIZE_BP.udf, actualTemplateSizeBp)
    }

    BigDecimal getUdfActualInsertSizeKb() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_ACTUAL_INSERT_SIZE_KB.udf)
    }

    void setUdfActualInsertSizeKb(BigDecimal value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_ACTUAL_INSERT_SIZE_KB.udf, value)
    }

    String getUdfIndexName() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_INDEX_NAME.udf)
    }

    void setUdfIndexName(String indexName) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_INDEX_NAME.udf, indexName)
    }

    String getUdfIndexContainerBarcode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_INDEX_CONTAINER_BARCODE.udf)
    }

    void setUdfIndexContainerBarcode(String indexContainerBarcode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_INDEX_CONTAINER_BARCODE.udf, indexContainerBarcode)
    }

    BigDecimal getUdfLibraryMolarityQcPm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.udf)
    }

    void setUdfLibraryMolarityQcPm(BigDecimal libraryMolarityQcPm) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_QC_PM.udf, libraryMolarityQcPm)
    }

    //PPS-4708 - Pool fraction needs to be carried over to the pool creation worksheet
    BigDecimal getUdfLibraryPercentageWithSOF() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_PERCENTAGE_SOF.udf)
    }

    void setUdfLibraryPercentageWithSOF(BigDecimal libraryPercentage) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_PERCENTAGE_SOF.udf, libraryPercentage)
    }

    @Override
    BigDecimal getUdfLpActualWithSof() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LP_ACTUAL_WITH_SOF.udf)
    }

    @Override
    void setUdfLpActualWithSof(BigDecimal value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LP_ACTUAL_WITH_SOF.udf, value)
    }

    BigDecimal getUdfLibraryMolarityPm() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM.udf)
    }

    void setUdfLibraryMolarityPm(BigDecimal libraryMolarityPm) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_MOLARITY_PM.udf, libraryMolarityPm)
    }

    BigDecimal getUdfNumberPcrCycles() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_NUMBER_PCR_CYCLES.udf)
    }

    void setUdfNumberPcrCycles(BigDecimal numberPcrCycles) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_NUMBER_PCR_CYCLES.udf, numberPcrCycles)
    }

    BigDecimal getUdfDegreeOfPooling() {
        return artifactNodeInterface.getUdfAsBigDecimal(ClarityUdf.ANALYTE_DEGREE_POOLING.udf)
    }

    void setUdfDegreeOfPooling(BigDecimal degreeOfPooling) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_DEGREE_POOLING.udf, degreeOfPooling)
    }

    String getUdfLibraryQcResult() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QC_RESULT.udf)
    }

    boolean getValidQcResult() {
        return udfLibraryQcResult in Analyte.PASS_FAIL_LIST && containerUdfLibraryQcResult in Analyte.PASS_FAIL_LIST
    }

    void setUdfLibraryQcResult(String libraryQcResult) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QC_RESULT.udf, libraryQcResult)
    }

    String getUdfLibraryQcFailureMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QC_FAILURE_MODE.udf)
    }

    void setUdfLibraryQcFailureMode(String libraryQcFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QC_FAILURE_MODE.udf, libraryQcFailureMode)
    }

    BigInteger getUdfLibraryCreationQueueId() {
        return artifactNodeInterface.getUdfAsBigInteger(ClarityUdf.ANALYTE_LIBRARY_CREATION_QUEUE_ID.udf)
    }

    void setUdfLibraryCreationQueueId(BigInteger libraryCreationQueueId) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CREATION_QUEUE_ID.udf, libraryCreationQueueId)
    }

    BigInteger getUdfActualLibraryCreationQueueId() {
        return artifactNodeInterface.getUdfAsBigInteger(ClarityUdf.ANALYTE_ACTUAL_LIBRARY_CREATION_QUEUE_ID.udf)
    }

    void setUdfActualLibraryCreationQueueId(BigInteger queueId) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_ACTUAL_LIBRARY_CREATION_QUEUE_ID.udf, queueId)
    }

    String getActualLibraryCreationQueue() {
        return LibraryCreationQueueCv.get(udfActualLibraryCreationQueueId)?.libraryCreationQueue
    }

    BigInteger getUdfLibraryCreationSpecsId() {
        return artifactNodeInterface.getUdfAsBigInteger(ClarityUdf.ANALYTE_LIBRARY_CREATION_SPECS_ID.udf)
    }

    void setUdfLibraryCreationSpecsId(BigInteger libraryCreationSpecsId) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CREATION_SPECS_ID.udf, libraryCreationSpecsId)
    }

    String getUdfLibraryCreator() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_CREATOR.udf)
    }

    void setUdfLibraryCreator(String libraryCreator) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_CREATOR.udf, libraryCreator)
    }

    @Override
    RunModeCv getRunModeCv() {
        String runModeName = udfRunMode
        if (!runModeName) {
            throw new WebException("${this}: run mode not defined", 422)
        }
        RunModeCv runModeCv = RunModeCv.findByRunMode(runModeName)
        if (!runModeCv?.id) {
            throw new WebException("${this}: run mode not found for the value [${runModeName}]", 422)
        }
        return runModeCv
    }

    void setUdfRunMode(String runMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_RUN_MODE.udf, runMode)
    }

    def updateLibraryCreationUdfs(BigInteger actualQueueId, String researcherFullName) {
        setUdfDegreeOfPooling(claritySample.getUdfDegreeOfPooling() as BigDecimal)
        setUdfActualLibraryCreationQueueId(actualQueueId)
        def queueId = parentAnalyte.libraryCreationQueue?.id
        setUdfLibraryCreationQueueId(queueId as BigInteger)
        setUdfLibraryCreationSpecsId(claritySample.udfLibraryCreationSpecsId)
        setUdfLibraryCreator(researcherFullName)
        setUdfRunMode(claritySample.getUdfRunMode())
    }
    @Override
    BigInteger getReadLengthBp() {
        return runModeCv.readLengthBp
    }

    BigInteger getReadTotal() {
        return runModeCv.readTotal
    }
    @Override
    SequencerModelCv getSequencerModelCv() {
        return runModeCv.sequencerModel
    }

    String getSequencer() {
        return sequencerModelCv?.sequencerModel
    }

    String getPlatform() {
        return sequencerModelCv?.platform
    }

    def updateLibraryCreationLcaUdf() {
        claritySample.incrementUdfLcAttempt()
        if (udfLibraryQcResult == Analyte.FAIL) {
            claritySample.incrementUdfLcFailedAttempt()
        }
    }

    private final static String[] PREFIX = ['LS', 'NLS', 'NLS', 'CLS']
    @Override
    String getPrintLabelText() {
        if (isExomeCapture) {
            return PREFIX.collect{ plateLabelBean(it).toString() }.join(WINDOWS_NEWLINE)
        }
        if (isInternalSingleCell) {
            return "$containerId,$containerId"
        }
        if (isOnPlate) {
            return plateLabelBean() as String
        }
        return new LabelBean(
                pos_1_1: containerName,
                pos_1_2: name,
                pos_1_3: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerName,
                pos_2_2: name
        ) as String
    }

    LabelBean plateLabelBean(String prefix = '') {
        return new LabelBean(
                pos_1_1: "$prefix$containerId",
                pos_1_2: containerUdfLabel,
                pos_1_4: claritySample.sequencingProject.udfMaterialCategory,
                pos_1_5: containerId
        )
    }

    String getUdfLibraryQpcrFailureMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_FAILURE_MODE.udf)
    }

    void setUdfLibraryQpcrFailureMode(String qpcrFailureMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QPCR_FAILURE_MODE.udf, qpcrFailureMode)
    }

    String getUdfLibraryQpcrResult() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf)
    }

    void setUdfLibraryQpcrResult(String qpcrResult) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LIBRARY_QPCR_RESULT.udf, qpcrResult)
    }

    String getUdfSmrtbellTemplatePrepKit() {
        ProcessNode parentProcessNode = artifactNodeInterface.parentProcessNode
        if (parentProcessNode.processType == ProcessType.LC_PLATE_TRANSFER.value) {
            return parentAnalyte.artifactNodeInterface.parentProcessNode.getUdfAsString(ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT.udf)
        }
        return parentProcessNode.getUdfAsString(ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT.udf)
    }

    String getUdfPacBioSmrtbellTemplatePrepKit() {
        ProcessNode parentProcessNode = artifactNodeInterface.parentProcessNode
        if (parentProcessNode.processType == ProcessType.LC_PLATE_TRANSFER.value) {
            return parentAnalyte.artifactNodeInterface.parentProcessNode.getUdfAsString(ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT.udf)
        }
        return parentProcessNode.getUdfAsString(ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT.udf)
    }

    String getSmrtbellTemplatePrepKit() {
        if (udfSmrtbellTemplatePrepKit)
            return udfSmrtbellTemplatePrepKit
        return udfPacBioSmrtbellTemplatePrepKit
    }

    void setUdfMagBeadCleanupDate(Date cleanupDate) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_MAGBEAD_CLEANUP_DATE.udf, ClarityStringUtils.toClarityDate(cleanupDate))
    }

    Date getUdfMagBeadCleanupDate() {
        return artifactNodeInterface.getUdfAsDate(ClarityUdf.ANALYTE_MAGBEAD_CLEANUP_DATE.udf)
    }

    String getUdfCollaboratorLibraryName() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME.udf)
    }

    void setUdfCollaboratorLibraryName(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME.udf, value)
    }

    Integer getMaxLcAttempt() {
        Integer maxLcAttempt = Integer.MIN_VALUE
        if (claritySample instanceof PmoSample) {
            PmoSample pmoSample = claritySample as PmoSample
            for (ScheduledSample scheduledSample : pmoSample.getScheduledSamples()) {
                BigDecimal lcAttempt = scheduledSample.getUdfLcAttempt()
                if (lcAttempt != null) {
                    maxLcAttempt = Math.max(maxLcAttempt, lcAttempt.intValue())
                }
            }
        } else if (claritySample instanceof ScheduledSample) {
            ScheduledSample scheduledSample = claritySample as ScheduledSample
            BigDecimal lcAttempt = scheduledSample.getUdfLcAttempt()
            if (lcAttempt != null) {
                maxLcAttempt = Math.max(maxLcAttempt, lcAttempt.intValue())
            }
        }
        return maxLcAttempt
    }

    void setNewRunMode(String runMode) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_RUN_MODE.udf,runMode)
        parentAnalytes?.each { Analyte pAnalyte ->
            if (pAnalyte instanceof  ClarityLibraryStock) {
                pAnalyte.setNewRunMode(runMode)
            }
        }
    }

    @Override
    def validateIsLibraryInput() {
    }

    @Override
    boolean getIsPacBioSequel() {
        return sequencerModelCv.isSequel
    }

    @Override
    boolean getIsPacBio() {
        return sequencerModelCv.isPacBio
    }

    List<String> getLibraryStockIds(){
        return [id]
    }

    boolean getIsSAG() {
        return isInternalSingleCell && claritySample.sequencingProject.udfSequencingProductName == 'Microbial Minimal Draft, Single Cell'
    }
}
