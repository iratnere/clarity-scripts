package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.KeyValueSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.MiSeqReagentSheetHeaderBean
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.MiSeqSampleSheetBean
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.MiSeqSampleSheetReadsBean
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.SeqTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

class MiSeq implements SequencerType {
    Logger logger = LoggerFactory.getLogger(MiSeq.class)

    final static String SAMPLE_SHEET_TEMPLATE = 'resources/templates/excel/MiSeqSampleSheet.xls'
    final static String DUAL_SAMPLE_SHEET_TEMPLATE = 'resources/templates/excel/MiSeqDualIndexSampleSheet.xls'
    final static String SAMPLE_SHEET_FILE = 'Sample Worksheet'
    Sequencing process
    Boolean isDualIndex
    ArtifactIndexService artifactIndexService

    MiSeq(Sequencing process) {
        this.process = process
        artifactIndexService = gov.doe.jgi.pi.pps.util.util.BeanUtil.getBean(ArtifactIndexService.class)
    }

    @Override
    void validateInputIsLibrary() {
        process.inputAnalytes*.validateIsLibraryInput() //Input must be library stock, pool, pool of pools
    }

    @Override
    void validateNumberInputs() {
        if (process.inputAnalytes.size() > 1)
            process.postErrorMessage("only one input is accepted for ${process.sequencerModelCv.sequencerModel}")
    }

    @Override
    void validateInputFlowcellType() {}

    @Override
    String getTemplateName() {
        return 'resources/templates/excel/ClarityMiSeq.xls'
    }

    @Override
    void validateOutputContainerType() {}

    @Override
    void updateWorkbookStyle(ExcelWorkbook excelWorkbook) {}

    @Override
    int getActiveTabIndex() {
        return 0
    }

    @Override
    def getTableBean(ClarityPhysicalRunUnit outputAnalyte, List<FreezerContainer> freezerContainers) {
        Analyte inputAnalyte = outputAnalyte.parentAnalyte
        SeqTableBean tableBean = new SeqTableBean()
        tableBean.freezerPath = process.getFreezerLocation(freezerContainers, inputAnalyte)
        tableBean.libraryStockName = inputAnalyte.name
        tableBean.containerBarcode = inputAnalyte.containerName
        tableBean.runType = inputAnalyte.udfRunMode
        tableBean.materialType = outputAnalyte.claritySample.sequencingProject.udfMaterialCategory
        tableBean.concentrationPm = inputAnalyte.udfLibraryMolarityPm
        tableBean
    }

    @Override
    String getEndAddress() {
        return null
    }

    @Override
    String getStartAddress() {
        return 'A1'
    }

    @Override
    void uploadSampleSheet() {
        ExcelWorkbook excelWorkbook = getSampleSheetWorkbook()
        def reagentFileNode = process.getFileNode(SAMPLE_SHEET_FILE)
        def responseNode = excelWorkbook.store(process.nodeManager.nodeConfig, reagentFileNode.id)
        logger.info "uploaded $SAMPLE_SHEET_FILE to $responseNode"
    }

    private traverse(Set<ClarityLibraryStock> libraryStocks, ClarityLibraryPool pool, Set<ArtifactNode> visited) {
        if (visited.contains(pool)) {
            return
        }
        visited.add(pool)

        pool.poolMembers.each { Analyte child ->
            if (child.artifactNode.isControlAnalyte) {
                return
            } else if (child instanceof ClarityLibraryStock) {
                libraryStocks.add(child as ClarityLibraryStock)
            } else if (child instanceof ClarityLibraryPool) {
                traverse(libraryStocks, child, visited)
            }
        }
    }

    private Set<ClarityLibraryStock> getLibraryStocks() {
        Set<ArtifactNode> visited = new HashSet<ArtifactNode>()
        Set<ClarityLibraryStock> libraryStocks = new LinkedHashSet<ClarityLibraryStock>()
        List<ArtifactNode> inputAnalytes = process.processNode.inputAnalytes
        for (ArtifactNode artifactNode : inputAnalytes) {
            Analyte analyte = AnalyteFactory.analyteInstance(artifactNode)
            if (analyte.artifactNode.isControlAnalyte) {
                continue
            } else if (analyte instanceof ClarityLibraryStock) {
                libraryStocks.add(analyte as ClarityLibraryStock)
            } else if (analyte instanceof ClarityLibraryPool) {
                traverse(libraryStocks, analyte as ClarityLibraryPool, visited)
            }
        }
        return libraryStocks
    }

    BigDecimal getReadLengthBp() {
        ScheduledSample scheduledSample = process.inputAnalytes[0].claritySample as ScheduledSample
        return scheduledSample.readLengthBp + 1
    }

    BigDecimal getReadTotal() {
        ScheduledSample scheduledSample = process.inputAnalytes[0].claritySample as ScheduledSample
        return scheduledSample.readTotal
    }

    static List<MiSeqSampleSheetBean> getMiSeqSampleSheetBeans(Map<ClarityLibraryStock, String> libraryToIndexSequence) {
        List<MiSeqSampleSheetBean> miSeqData = new ArrayList<MiSeqSampleSheetBean>()
        libraryToIndexSequence.each{ ClarityLibraryStock libraryStock, String indexSequence ->
            MiSeqSampleSheetBean dataRow = new MiSeqSampleSheetBean()
            dataRow.sampleId = libraryStock.name
            dataRow.setIndexSequence(indexSequence)
            miSeqData.add(dataRow)
        }
        return miSeqData
    }

    MiSeqReagentSheetHeaderBean getHeader() {
        Analyte inputAnalyte = process.inputAnalytes[0]
        MiSeqReagentSheetHeaderBean header = new MiSeqReagentSheetHeaderBean()
        header.investigatorName = process.processNode.technicianCanonicalName
        header.projectName = inputAnalyte.artifactNode.sampleNode.projectNode.pmoManagerName
        header.experimentName = inputAnalyte.artifactNode.name
        header.date = new SimpleDateFormat('MM/dd/yyyy').format(new Date())
        return header
    }

    KeyValueSection getHeaderSection() {
        KeyValueSection headerSection = (isDualIndex ?
                new KeyValueSection(0, MiSeqReagentSheetHeaderBean.class, 'A2', 'B11') :
                new KeyValueSection(0, MiSeqReagentSheetHeaderBean.class, 'A2', 'B10'))
        headerSection.setData(header)
        return headerSection
    }

    TableSection getRunModeSection() {
        List<MiSeqSampleSheetReadsBean> reads
        if (readTotal == 2) {
            reads = [
                    new MiSeqSampleSheetReadsBean([read: readLengthBp]),
                    new MiSeqSampleSheetReadsBean([read: readLengthBp])
            ]
        } else {
            reads = [
                    new MiSeqSampleSheetReadsBean([read: readLengthBp])
            ]
        }
        TableSection runModeSection = (isDualIndex ?
                new TableSection(0, MiSeqSampleSheetReadsBean.class, 'A12', 'A14') :
                new TableSection(0, MiSeqSampleSheetReadsBean.class, 'A11', 'A13'))
        runModeSection.setData(reads)
        return runModeSection
    }

    Section getTableSection(miSeqData) {
        Section tableSection = (isDualIndex ?
                new TableSection(0, MiSeqSampleSheetBean.class, 'A20') :
                new TableSection(0, MiSeqSampleSheetBean.class, 'A17'))
        tableSection.setData(miSeqData)
        return tableSection
    }

    static boolean isDualIndex(def indexSequences) {
        //ppv miseq -> inputAnalytes().size() == 1: LS(inputLibraryStocks.size() == 1) or pool of pools (inputLibraryStocks.size() > 1)
        //assert: cannot mix dual and single libraries in a pool
        return indexSequences.any{ArtifactIndexService.isDualIndexSequence(it as String)}
    }

    ExcelWorkbook getSampleSheetWorkbook() {
        Map<ClarityLibraryStock, String> libraryToIndexSequence = [:]
        Set<ClarityLibraryStock> libraries = libraryStocks
        Map<String, String> indexNameToSequence = artifactIndexService.getSequences(libraries*.udfIndexName)
        libraries.each{
            libraryToIndexSequence[it] = indexNameToSequence[it.udfIndexName]
        }
        isDualIndex = isDualIndex(libraryToIndexSequence?.values())
        List<MiSeqSampleSheetBean> miSeqData = getMiSeqSampleSheetBeans(libraryToIndexSequence)
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(sampleTemplateName, process.testMode)
        excelWorkbook.addSection(getRunModeSection())
        excelWorkbook.addSection(getHeaderSection())
        excelWorkbook.addSection(getTableSection(miSeqData))
        excelWorkbook
    }

    String getSampleTemplateName() {
        return isDualIndex ? DUAL_SAMPLE_SHEET_TEMPLATE : SAMPLE_SHEET_TEMPLATE
    }

    @Override
    boolean isValidateLane() {
        false
    }

    @Override
    void routeToNextWorkflow() {}

    @Override
    List<Analyte> getRepeatOutputAnalytes() {
        return process.failedQcAnalytes
    }

    def getDefaultPhixSpikeIn(){
        return 7
    }

    def getDefaultLibConvFactor(){
        return 8.5
    }

    void setDefaultDnaWa01Ul(def bean){
//        =IF(AND(G2>=4000,run type="Illumina MiSeq 2 X 300"),((4*15)/I2),IF(G2>=2000,50/I2,(((660*(1-(0.01*F2)))*H2)/G2)))
        if(bean.concentrationPm >= 4000 && bean.runType == "Illumina MiSeq 2 X 300")
            bean.dnaWa01Ul = (4*15)/(bean.concentrationPm/1000)
        else if(bean.concentrationPm >= 2000)
            bean.dnaWa01Ul = 50/(bean.concentrationPm/1000)
        else
            bean.dnaWa01Ul = ((660*(1-(0.01*bean.phixSpikeIn)))*bean.libraryConvFactor)/bean.concentrationPm
    }
}
