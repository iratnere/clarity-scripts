package gov.doe.jgi.pi.pps.clarity.scripts.plate_transfer.beans

import gov.doe.jgi.pi.pps.clarity.domain.LibraryStockFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService

/**
 * Created by lvishwas on 1/6/15.
 */
class PlateTransferTableBean {
    @FieldMapping(header='#', cellType = CellTypeEnum.NUMBER)
    public def poolNumber
    @FieldMapping(header='qPCR\'d Sample Name', cellType = CellTypeEnum.STRING)
    public def sampleName
    @FieldMapping(header='qPCR\'d Sample LIMS ID', cellType = CellTypeEnum.STRING)
    public def sampleLimsId
    @FieldMapping(header='Conc (pM)', cellType = CellTypeEnum.NUMBER)
    public def concentration
    @FieldMapping(header='Index ID', cellType = CellTypeEnum.STRING)
    public def indexName
    @FieldMapping(header='DILUTE?', cellType = CellTypeEnum.STRING)
    public def dilute
    @FieldMapping(header='Library Working Concentration for Pooling', cellType = CellTypeEnum.FORMULA)
    public def libraryWorkingConc
    @FieldMapping(header='ul of Undiluted or 1:10 Library', cellType = CellTypeEnum.FORMULA)
    public def ulOfUndiluted
    @FieldMapping(header='Vol TE  to Bring Library up to 10ul', cellType = CellTypeEnum.FORMULA)
    public def volTEToBringLibraryUpTo10ul
    @FieldMapping(header='Target Pool Concentration pM', cellType = CellTypeEnum.NUMBER)
    public def targetPoolConc
    @FieldMapping(header='Source Labware', cellType = CellTypeEnum.FORMULA)
    public def srcLabware
    @FieldMapping(header='Source Position', cellType = CellTypeEnum.STRING)
    public String srcPosition
    @FieldMapping(header='Destination Labware', cellType = CellTypeEnum.FORMULA)
    public def destLabware
    @FieldMapping(header='Destination Position', cellType = CellTypeEnum.STRING)
    public String destPosition
    @FieldMapping(header='Notification', cellType = CellTypeEnum.STRING)
    public def notification
    @FieldMapping(header='Pool Name', cellType = CellTypeEnum.STRING)
    public def poolName
    @FieldMapping(header='Pool Size', cellType = CellTypeEnum.NUMBER)
    public def poolSize
    @FieldMapping(header='Run Mode', cellType = CellTypeEnum.STRING)
    public def runMode
    @FieldMapping(header='Pool Lab Process Result', cellType = CellTypeEnum.DROPDOWN)
    public def poolLabProcessResult
    @FieldMapping(header='Pool Lab Process Failure Mode', cellType = CellTypeEnum.DROPDOWN)
    public def poolLabProcessFailureMode
    @FieldMapping(header='Pool Concentration pM', cellType = CellTypeEnum.NUMBER)
    public def poolConcentrationpM
    @FieldMapping(header='Actual Template Size (bp)', cellType = CellTypeEnum.NUMBER)
    public def actualTemplateSizeBp
    @FieldMapping(header='Source Container Barcode', cellType = CellTypeEnum.STRING)
    public def sourceContainerBarcode
    @FieldMapping(header='Source Container Type', cellType = CellTypeEnum.STRING)
    public def sourceContainerType

    def analyte

    String validateBean(){
        String errors = ''
        if(!poolConcentrationpM)
            errors += "Pool Concentration pM is a required field for the analyte ${sampleLimsId}.\n"
        if(!poolLabProcessResult?.value)
            errors += "Pool Lab Process Result is a required field for the analyte ${sampleLimsId}.\n"
        if(poolLabProcessResult?.value == Analyte.FAIL && !poolLabProcessFailureMode?.value)
            errors += "Pool Lab Process Failure Mode is a required field for the analyte ${sampleLimsId}.\n"
        return errors
    }

    void populateRequiredFields(int passCount){
        poolConcentrationpM = 10
        String passFail = Analyte.PASS
        String failureMode = ""
        String[] failureModes = LibraryStockFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }.minus(LibraryStockFailureModesService.EXCLUDED_FAILURE_MODES)
        if(passCount <= 0){
            passFail = Analyte.FAIL
            failureMode = failureModes[0]
        }
        poolLabProcessFailureMode = new DropDownList(controlledVocabulary: failureModes, value: failureMode)
        poolLabProcessResult = new DropDownList(controlledVocabulary: Analyte.PASS_FAIL_LIST, value: passFail)
        dilute = 'DILUTE'
        targetPoolConc = 10
    }
}
