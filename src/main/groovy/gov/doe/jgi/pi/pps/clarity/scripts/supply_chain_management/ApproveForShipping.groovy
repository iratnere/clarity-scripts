package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteSorter
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * send notification email, group by sample contact and SP Manager (one email for each sample contact and SP Manager pair). See requirements for “approve for shipping” event.
 * print barcodes for the input containers. See requirements for samples.
 * update the USS sample status of each input sample to “Awaiting Sample Receipt”
 * Created by lvishwas on 1/17/15.
 */
class ApproveForShipping extends ActionHandler{

    void execute(){
        def inputs = AnalyteSorter.sort(process.inputAnalytes)
        //1.  print barcodes for the input containers PPS-3126
        process.writeLabels(inputs)
        process.setCompleteStage()
    }
}
