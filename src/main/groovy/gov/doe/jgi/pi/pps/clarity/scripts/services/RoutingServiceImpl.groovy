package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.clarity_node_manager.util.LogTiming
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Service
import org.springframework.web.context.WebApplicationContext

@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
class RoutingServiceImpl implements RoutingService {

    boolean testMode = false
    Logger logger = LoggerFactory.getLogger(RoutingServiceImpl.class.name)

    @Autowired
    NodeManager nodeManager

    @Override
    boolean submitRoutingRequests(Collection<RoutingRequest> routingRequests) {
        boolean somethingRouted = false
        if (testMode || !routingRequests) {
            return somethingRouted
        }
        Long startTime = System.currentTimeMillis()
        //ClarityWebTransaction webTransaction = ClarityWebTransaction.requireCurrentTransaction()
        //boolean routingError = false
        //try {
//            if (!webTransaction.webTransactionRecorder) {
//                webTransaction.setWebTransactionRecorder(new ClarityWebTransactionSuccessRecorder())
//            } else if (webTransaction.webTransactionRecorder instanceof CouchdbWebTransactionRecorder) {
//                ((CouchdbWebTransactionRecorder) webTransaction.webTransactionRecorder).enabled = true
//                if (((CouchdbWebTransactionRecorder) webTransaction.webTransactionRecorder).couchDb.name != ClarityCouchdbService.CouchDatabase.WEB_TRANSACTION.couchDb.name) {
//                    logger.warn "${webTransaction}: routings will be logged to ${((CouchdbWebTransactionRecorder) webTransaction.webTransactionRecorder).couchDb.name}"
//                }
//            } else {
//                //throw new RuntimeException('couchdb web-transaction recorder must be set to record routings')
//                logger.error "${webTransaction}: no web transaction recorder to log routings"
//            }

            //NodeManager nodeManager = BeanUtil.nodeManager
            Map<String,List<RoutingRequest>> batches = [:].withDefault{[]}
            routingRequests.each{ RoutingRequest routingRequest ->
                ArtifactNode artifactNode = nodeManager.getArtifactNode(routingRequest.artifactId)
                String containerId = artifactNode.containerId
                batches[containerId] << routingRequest
            }
            Map<String,Routing> routingAssignments = [:].withDefault{new Routing()}
            Map<String,Routing> routingUnAssignments = [:].withDefault{new Routing()}
            batches.values().each { List<RoutingRequest> containerBatch ->
                containerBatch.each { RoutingRequest routingRequest ->
                    if (!routingRequest.action) {
                        throw new RuntimeException('routing action not defined')
                    }
                    Routing routing = routingRequest.action == Routing.Action.unassign ? routingUnAssignments[routingRequest.routingUri] : routingAssignments[routingRequest.routingUri]
                    routing.uri = routingRequest.routingUri
                    routing.action = routingRequest.action
                    if (!routing.artifactUris) {
                        routing.artifactUris = []
                    }
                    routing.artifactUris << nodeManager.nodeConfig.artifactsUrl + routingRequest.artifactId
                }
            }
            List<Routing> routings = []
            if (routingAssignments) {
                routings.addAll(routingAssignments.values().flatten())
            }
            if (routingUnAssignments) {
                routings.addAll(routingUnAssignments.values().flatten())
            }
            somethingRouted = Routing.route(nodeManager.nodeConfig,routings)
            Long endTime = System.currentTimeMillis()
            LogTiming.logMethodCall(startTime,endTime,'RoutingService.submitRoutingRequests',['batch-size':routingRequests.size()])
//        } catch (routingException) {
//            routingError = true
//            webTransaction.logger.error "${webTransaction}", GrailsUtil.sanitizeRootCause(routingException)
//            LoggerFactory.getLogger('errors.routing').error "routing error", routingException
//            //clarityProcess.postError(routingErrorMessage)
//        } finally {
//            webTransaction.jsonResponse.'routing-error' = routingError
//        }
        return somethingRouted
    }


}
