package gov.doe.jgi.pi.pps.util.util

class EnumConverterCaseInsensitive<K> {

    private final OnDemandCache<Map<String, K>> cachedConversionMap = new OnDemandCache<>()
    private K enumType

    EnumConverterCaseInsensitive(K enumType) {
        this.enumType = enumType
    }

    private final Map<String, K> getConversionMap() {
        return cachedConversionMap.fetch {
            Map<String, K> cMap = [:]
            enumType.values().each { K enumValue ->
                String strVal = enumValue.toString().trim().toLowerCase()
                if (cMap.containsKey(strVal)) {
                    throw new RuntimeException("duplicate case-insensitive enum value [${strVal}]")
                }
                cMap[strVal] = enumValue
            }
            cMap
        }
    }

    final K toEnum(Object value) {
        String trimValue = value?.toString()?.trim()
        K matchingType = null
        if (trimValue) {
            matchingType = conversionMap[trimValue.toLowerCase()]
        }
        return matchingType
    }

}
