package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by datjandra on 3/27/2015.
 */
class SampleReceiptEmailNotification extends EmailNotification {
    static final String SUBJECT = 'Your sample(s) has been received by the JGI'
    static final String CC_EMAIL_GROUP = 'jgi-its-ship-sample@lbl.gov' //PPS-4849 - update mailing lists
    static final String TEMPLATE_FILE_NAME = 'sample_receipt'

    @Override
    def getToList(List<EmailDetails> emailDetailsList){
        def to = emailDetailsList.findAll{it.sampleContactId != null}?emailDetailsList.findAll{it.sampleContactId != null}.collect{it.sampleContactId}?.unique()?.join(',') + ', ':''
        to += emailDetailsList.findAll{it.sequencingProjectContactId != null}?emailDetailsList.findAll{it.sequencingProjectContactId != null}.collect{it.sequencingProjectContactId}?.unique()?.join(',') + ',':''
        to += "PI"
        return to
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return SUBJECT
    }

    @Override
    def getCcList(List<EmailDetails> emailDetailsList){
        def cc = emailDetailsList.findAll{it.sequencingProjectManagerId != null}?.collect{it.sequencingProjectManagerId}?.unique()?.join(',')
        if(cc)
            cc += ','
        cc += CC_EMAIL_GROUP
        return cc
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        if(emailDetailsList.collect{it.sequencingProjectManagerId}.unique().size() == 1)
            return emailDetailsList.first().sequencingProjectManagerId
        return ''
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new SampleReceiptEmailDetails(it)}
    }

    @Override
    def getBinding(List<EmailDetails> emailDetailsList) {
        return [piLastName:emailDetailsList.first().sampleContactName, samples: getSampleList(emailDetailsList)]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
