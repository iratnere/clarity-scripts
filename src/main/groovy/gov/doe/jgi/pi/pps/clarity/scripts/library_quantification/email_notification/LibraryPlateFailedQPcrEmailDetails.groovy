package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import org.apache.commons.lang.builder.HashCodeBuilder

class LibraryPlateFailedQPcrEmailDetails extends EmailDetails{
    int sowItemId
    String sowItemType
    String defaultLibraryQueue
    String failureMode
    String failureComments

    LibraryPlateFailedQPcrEmailDetails(Analyte analyte) {
        super(analyte)
        ClaritySample scheduledSample = (ScheduledSample) analyte.claritySample
        sowItemId = scheduledSample.sowItemId
        sowItemType = scheduledSample.udfSowItemType
        failureMode = analyte.udfLibraryQpcrFailureMode
        defaultLibraryQueue = scheduledSample.libraryCreationQueue.libraryCreationQueue
        failureComments = analyte.udfNotes
    }

    @Override
    def getHashCode() {
        return new HashCodeBuilder(17, 37).
                append(containerNode.id).
                append(sequencingProjectManagerId).
                toHashCode()
    }

    @Override
    def getInfo() {
        return "$sequencingProjectId;  $sequencingProjectName;  $sequencingProjectPIName; $sequencingProjectManagerName; $pmoSampleId; $sampleLimsId; $sowItemId; $sowItemType; $defaultLibraryQueue; $failureMode; $failureComments"
    }
}
