package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans.NovaSeqTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException

class NovaSeq implements SequencerType{

    Sequencing process
    private def activeTabIndexTransient

    NovaSeq(Sequencing process) {
        this.process = process
    }

    @Override
    void validateInputIsLibrary() {
        process.inputAnalytes*.validateIsLibraryInput() //Input must be library stock, pool, pool of pools
    }

    @Override
    void validateNumberInputs() {}

    @Override
    void validateInputFlowcellType() { //PPV: all inputs have the same run mode
        String flowcellType = process.sequencerModelCv.flowcellType as String
        if (!flowcellType) { //flowcellTypes == [""] -> error
            process.postErrorMessage("""
The flowcell type is not set for selected inputs.
Inputs: ${process.inputAnalytes*.name}
Sequencer Model: ${process.sequencerModelCv.sequencerModel}
""")
        }
    }

    @Override
    String getTemplateName() {
        return 'resources/templates/excel/ClarityNovaSeq.xls'
    }

    @Override
    void validateOutputContainerType() {
        String flowcellType = process.sequencerModelCv.flowcellType as String
        if (outputContainerType != flowcellType) {
            process.postErrorMessage("""
Process output container type '$outputContainerType'
does not match the inputs flowcell type from sequencer model: '$process.sequencerModelCv.sequencerModel'.
Please select '$flowcellType' as the output container.
""")
        }
    }

    String getOutputContainerType() {
        return process.outputAnalytes[0].containerNode.containerType
    }

    @Override
    void updateWorkbookStyle(ExcelWorkbook excelWorkbook) {
        activeTabIndexTransient = excelWorkbook.getSheetIndex(outputContainerType)
        excelWorkbook.workbook.setActiveSheet(activeTabIndex)
        excelWorkbook.workbook.setSelectedTab(activeTabIndex)
        //setSheetHidden is not working in Windows PPS-3856
    }

    @Override
    int getActiveTabIndex() {
        if (activeTabIndexTransient != null)
            return  activeTabIndexTransient
        //Not used
        throw new WebException("Invalid output container type: $outputContainerType", 500)
    }

    @Override
    def getTableBean(ClarityPhysicalRunUnit outputAnalyte, List<FreezerContainer> freezerContainers) {
        Analyte inputAnalyte = outputAnalyte.parentAnalyte
        NovaSeqTableBean tableBean = new NovaSeqTableBean()
        tableBean.populateBean(inputAnalyte, outputAnalyte)
        tableBean.freezerPath = process.getFreezerLocation(freezerContainers, inputAnalyte)
        tableBean
    }

    @Override
    String getEndAddress() {
        return outputContainerType == ContainerTypes.FLOW_CELL_S4.value ? 'P6' : 'P4'
    }

    @Override
    String getStartAddress() {
        return 'A2'
    }

    @Override
    void uploadSampleSheet() {}

    @Override
    boolean isValidateLane() {
        true
    }

    @Override
    void routeToNextWorkflow() {}

    @Override
    List<Analyte> getRepeatOutputAnalytes() {
        return process.failedQcAnalytes
    }

    static def getDefaultPhixSpikeIn(){
        return 1
    }

    static def getDefaultLibConvFactor(){
        return 1250
    }

    static void setDefaultDnaWa01Ul(NovaSeqTableBean bean){
//        =((150)-((150*(0.01*spike-in %)*conc pM)/conc pM))/2
        bean.dnaWa01Ul =((150)-((150*(0.01*bean.phixSpikeIn)*bean.concpMfromqPCR)/bean.concpMfromqPCR))/2
    }
}