package gov.doe.jgi.pi.pps.clarity.scripts.release_work

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 3/25/16.
 */
class ReleaseWorkRecordDetailsProcess extends ActionHandler {

    @Override
    void execute() {
        process.setCompleteStage()
    }
}
