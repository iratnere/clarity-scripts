package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.libraries

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.PoolDetailsBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 10/20/16.
 */
class ProcessLibrariesFile extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ProcessLibrariesFile.class)
    //CouchDbService couchDbService

    void execute(){
        OnboardingAdapter adapter = (process as OnboardLibraries).getOnboardingAdapterInstance(onboardingWorkbook)
        List<String> errors = adapter.validateOnboardingData()
        if(errors)
            process.postErrorMessage(errors.join("\n"))
        adapter.mergeDataToClarity()
        def destFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardLibraries.SCRIPT_LIBRARY_WORSHEET)}?.id
        adapter.archiveOnboardingResult(destFileNode)
        //recordUpdates(adapter)
        logger.info "Onboarding completed successfully!"
    }

    ExcelWorkbook getOnboardingWorkbook() {
        def srcFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardLibraries.USER_LIBRARY_WORSHEET)}
        if(srcFileNode?.fileNode?.contentUri?.endsWith('xlsx'))
            throw new IllegalFormatException("Only Excel 97-2004 Workbook (.xls) format supported")
        ExcelWorkbook onboardLibraryWorkbook = new ExcelWorkbook(srcFileNode?.id, process.testMode)
        Section section = getOnboardingBeanTableSection(onboardLibraryWorkbook)
        onboardLibraryWorkbook.addSection(section)
        section = getPoolDetailsBeanTableSection(onboardLibraryWorkbook)
        if(section)
            onboardLibraryWorkbook.addSection(section)
        onboardLibraryWorkbook.load()
        return onboardLibraryWorkbook
    }

    TableSection getOnboardingBeanTableSection(ExcelWorkbook onboardLibraryWorkbook) {
        int sheetIndex = onboardLibraryWorkbook.getSheetIndex(OnboardLibraries.ONBOARDING_LIBRARIES_SHEET_NAME)
        if(sheetIndex < 0)
            throw new RuntimeException("Please attach the onboarding worksheet with righ format.")
        return new TableSection(sheetIndex,OnboardingBean.class,'A2')
    }

    TableSection getPoolDetailsBeanTableSection(ExcelWorkbook onboardLibraryWorkbook) {
        int sheetIndex = onboardLibraryWorkbook.getSheetIndex(OnboardLibraries.POOL_DETAILS_TAB)
        if(sheetIndex >= 0) {
            return new TableSection(sheetIndex, PoolDetailsBean.class, 'A2')
        }
        return null
    }

//    void recordUpdates(OnboardingAdapter adapter){
//        ClarityWebTransaction webTransaction = ClarityWebTransaction.currentTransaction
//        couchDbService = webTransaction.requireApplicationBean(CouchDbService.class)
//        if (!webTransaction.webTransactionRecorder) {
//            webTransaction.setWebTransactionRecorder(new ClarityWebTransactionSuccessRecorder())
//        } else if (webTransaction.webTransactionRecorder instanceof CouchdbWebTransactionRecorder) {
//            ((CouchdbWebTransactionRecorder) webTransaction.webTransactionRecorder).enabled = true
//        }
//        ClarityCouchdbService.CouchDatabase couchdb = ClarityCouchdbService.CouchDatabase.WEB_TRANSACTION
//        URL url = new URL(couchDbService.couchDbUrl(couchdb.name))
//        String submissionUri = "http://${url.host}:${url.port}/${couchdb.name}/${webTransaction.transactionId}"
//        NodeConfig nodeConfig = process.nodeConfig
//        String researcherId = new ResearcherUriIterator(nodeConfig,[ResearcherParameter.USER_NAME.setValue(nodeConfig.geneusUser)]).collect {
//            it.split('/')[-1]
//        }?.last()
//        Researcher researcher = process.researcher
//        webTransaction.submittedBy = researcher.contactId
//        webTransaction.jsonResponse.'researcher-contact-id' = researcher.contactId
//        webTransaction.jsonResponse.'researcher-username' = nodeConfig.geneusUser
//        webTransaction.jsonResponse.'researcher-url' = researcher.researcherNode.url
//        webTransaction.jsonResponse.'researcher-id' = researcherId
//        webTransaction.jsonResponse.'researcher-name' = researcher.fullName
//        webTransaction.jsonResponse.'analytes' = adapter.analytes?.collect{it.toString()}?.join(',')
//        webTransaction.jsonResponse.'action'= adapter.action
//        webTransaction.jsonResponse.'processId'= process.toString()
//    }
}
