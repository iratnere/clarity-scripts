package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.Pooling
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.PoolCreation

class ActionBean {
    @FieldMapping(header='Action', cellType = CellTypeEnum.STRING)
    public def poolingPrepAction
    @FieldMapping(header = 'Threshold (%)', cellType = CellTypeEnum.NUMBER)
    public def threshold

    def validate() {
        def errorMsg = StringBuilder.newInstance()
        String result = poolingPrepAction
        if (!result || !(result in PoolCreation.PROCESS_ACTIONS)) {
            errorMsg << "invalid Action '$result' $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!threshold) {
            errorMsg << 'unspecified Threshold (%)' << ClarityProcess.WINDOWS_NEWLINE
        }
        return buildErrorMessage(errorMsg)
    }

    static def buildErrorMessage(def errorMsg) {
        def startErrorMsg = "Please correct the errors below and upload spreadsheet again.$ClarityProcess.WINDOWS_NEWLINE" << "Action Section:$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

    void populateRequiredFields(){
        poolingPrepAction = PoolCreation.PROCESS_ACTION_DONE
        threshold = Pooling.DEFAULT_THRESHOLD
    }

}