package gov.doe.jgi.pi.pps.clarity.model.sample

import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationSpecsCv
import gov.doe.jgi.pi.pps.clarity_node_manager.node.SampleNodeInterface

class CustomAliquotScheduledSample extends ScheduledSample {

    CustomAliquotScheduledSample(SampleNodeInterface sampleNode) {
        super(sampleNode)
    }
    @Override
    List<LibraryCreationQueueCv> getLibraryCreationQueuesFromSpecsId() {
        return null
    }
    @Override
    LibraryCreationQueueCv getLibraryCreationQueue() {
        return null
    }
    @Override
    LibraryCreationSpecsCv getLibraryCreationSpecs() {
        return null
    }
}
