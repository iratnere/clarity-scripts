package gov.doe.jgi.pi.pps.clarity.scripts.pacbio_sequencing_complete

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 9/16/2015.
 */
class PbSequencingComplete extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbSequencingComplete.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.setCompleteStage()
    }
}
