package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.StageUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.ScheduledSampleService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.GeneusNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessParams
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

/**
 * Created by lvishwas on 10/14/16.
 */
class ScheduledSamplesMigrator extends Migrator {
    static final logger = LoggerFactory.getLogger(ScheduledSamplesMigrator.class)

    ScheduledSampleService scheduledSampleService
    static Map<Integer, Map<ClarityUdf, Object>> defaultUdfs

    ScheduledSamplesMigrator(NodeManager clarityNodeManager) {
        super(clarityNodeManager, new SamplesMigrator(clarityNodeManager))
        scheduledSampleService = BeanUtil.getBean(ScheduledSampleService.class)
    }

    static{
        defaultUdfs = [:].withDefault {[:]}
        String currentDate = new SimpleDateFormat(GeneusNode.defaultDateFormatText).format(new Date())
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_RECEIPT_NOTES] = ''
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_RECEIPT_DATE] = currentDate
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_RECEIPT_MISSING_DRY_ICE] = 'N'
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_RECEIPT_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_SM_INSTRUCTIONS] = ''
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_REQUIRED_QC_TYPE] = 'Quantity+Quality'
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QC_INSTRUMENT] = 'Manual'
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_VOLUME_UL] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_INITIAL_VOLUME_UL] = 1000
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_CONCENTRATION_NG_UL] = 1.2
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_GEL_DILUTE_VOLUME_UL] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_PURITY_VOLUME_UL] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QUALITY_VOLUME_UL] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QUANTITY_VOLUME_UL] = 10
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QC_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QC_NOTES] = ''
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_CURRENT_QC_TYPE] = 'Quantity+Quality'
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_QC_DATE] = currentDate
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_HMW_GDNA_Y_N] = 'Marginal'
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_ABSORBANCE_260_230] = 1
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_ABSORBANCE_260_280] = 1
        defaultUdfs[Migrator.PARENT_UDFS][ClarityUdf.SAMPLE_RRNA_RATIO] = 1
        defaultUdfs[Migrator.PARENT_CONTAINER_UDFS][ClarityUdf.CONTAINER_LOCATION] = 'On Site'
        defaultUdfs[Migrator.PARENT_CONTAINER_UDFS][ClarityUdf.CONTAINER_LOCATION_DATE] = currentDate
        defaultUdfs[Migrator.PARENT_CONTAINER_UDFS][ClarityUdf.CONTAINER_SAMPLE_QC_RESULT] = Analyte.PASS
        defaultUdfs[Migrator.SELF_UDFS][ClarityUdf.SAMPLE_LC_ATTEMPT] =1
    }

    @Override
    Stage getWorkflowStage(ContainerTypes containerType) {
        return null
    }

    @Override
    def createAnalytesInClarity(List<OnboardingBean> onboardingBeans, boolean startingPoint=false){
        if(startingPoint)
            prepareNodeManager()
        List<Analyte> sampleAnalytes = parentMigrator.createAnalytesInClarity(onboardingBeans)
        parentMigrator.performAdditionalActions(sampleAnalytes, onboardingBeans.first().contactId)
        List<OnboardingBean> unmergedBeans = getUnmergedBeans(onboardingBeans)
        if(unmergedBeans) {
            sampleAnalytes.addAll(unmergedBeans.collect{it.pmoSample.sampleAnalyte})
            sampleAnalytes.unique()
        }
        setDefaultUdfs(sampleAnalytes, this.defaultUdfs[Migrator.PARENT_UDFS], this.defaultUdfs[Migrator.PARENT_CONTAINER_UDFS])
        Map<Long, String> samples = scheduledSampleService.createScheduledSamplesInClarity(unmergedBeans.collect{it.getSowId()}.unique(), Migrator.DEFAULT_CONTACT_ID)
        logger.info "Done creating Scheduled samples $samples..."
        List<ScheduledSample> scheduledSamples = []
        if(samples)
            scheduledSamples.addAll(clarityNodeManager.getSampleNodesMap(samples?.values()).values()?.collect{ SampleFactory.sampleInstance(it)})
        unmergedBeans.each{ OnboardingBean bean ->
            bean.scheduledSample = scheduledSamples.find{it.sowItemId == bean.getSowId()}
        }
        updateUdfs(onboardingBeans)
        clarityNodeManager.dirtyNodes.each {it.readOnly = false}
        clarityNodeManager.httpPutDirtyNodes()
        def allSS = onboardingBeans.collect{it.scheduledSample}
        logger.info "Done updating scheduled samples $allSS..."
        routeParentsAndChildren(allSS.collect{it.pmoSample.sampleAnalyte}, allSS.collect{it.sampleAnalyte})
        if(startingPoint)
            resetNodeManager()
        return clarityNodeManager.getArtifactNodes(scheduledSamples.collect { it.sampleNode.artifactId }).collect { AnalyteFactory.analyteInstance(it)}
    }

    String getDestinationStage(Analyte analyte){
        return Stage.SOW_ITEM_QC.uri
    }

    Map getDefaultUdfs(){
        this.defaultUdfs
    }

    ContainerTypes getContainerType(Analyte parent){
        return parent.containerNode.containerTypeEnum
    }

    List<OnboardingBean> getUnmergedBeans(List<OnboardingBean> beans){
        if(beans.find{!it.getSowId()}){
            checkExistingAnalytes(beans)
        }
        return beans.findAll{!it.scheduledSample}
    }

    void checkExistingAnalytes(List<OnboardingBean> beans){
        beans.each{ OnboardingBean bean ->
            Analyte existingAnalyte = lookupSampleByPmoSampleId(clarityNodeManager, bean.pmoSampleId, false)?.sampleAnalyte
            if(existingAnalyte)
                bean.addAnalyte(existingAnalyte)
        }
    }

    Analyte getAnalyte(OnboardingBean bean){
        return bean.scheduledSample.sampleAnalyte
    }

    @Override
    Class getAnalyteType() {
        return ScheduledSample.class
    }

    List<Analyte> performAdditionalActions(List<Analyte> analytes, Long contactId){
        if(!analytes)
            return analytes
        Stage stage = Stage.SOW_ITEM_QC
        routeAnalytes(stage.uri, analytes.collect{it.id})
        ProcessParams processDetails = new ProcessParams(StageUtility.getStepConfigurationNode(clarityNodeManager, stage), analytes.collect{it.artifactNode})
        clarityNodeManager.executeClarityInputProcess(processDetails)
        return analytes
    }

}
