package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlateTableBean

/**
 * Created by tlpaley on 4/1/15.
 */
class LibraryCreationPlateSmallRna extends LibraryCreation {

    final static String TEMPLATE_NAME = 'resources/templates/excel/LcPlateSmallRna'

    LibraryCreationPlateSmallRna(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputPlate(process)
    }

    @Override
    void updateLibraryStockUdfs() {
        output.transferBeansDataToClarityLibraryStocks()
        output.updateLibraryIndexUdf(process.outputAnalytes as List<ClarityLibraryStock>)
        process.updateNonWorksheetUdfs()
    }

    @Override
    Section getTableSectionLibraries(){
        Section tableSection = new TableSection(0,LcPlateTableBean.class,'A26', 'I122')
        def tableBeansArray = []
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlateTableBean bean = new LcPlateTableBean()
            bean.well = clarityLibraryStock.artifactNode.location?.replaceAll(':','')
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryName = clarityLibraryStock.name
            bean.aliquotMass = clarityLibraryStock.parentAnalyte?.massNg
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    String getLcTableBeanClassName() {
        output.TABLE_CLASS_NAME
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return output.populateLibraryCreationSheet(TEMPLATE_NAME, getTableSectionLibraries())
    }

    @Override
    void moveToNextWorkflow() {
        output.moveOutputToNextWorkflow(process.outputAnalytes)
    }

    String getTemplate(){
        return TEMPLATE_NAME
    }

    int getTemplateIndex() {
        return 16
    }
}
