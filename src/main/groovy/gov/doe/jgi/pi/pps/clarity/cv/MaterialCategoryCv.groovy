package gov.doe.jgi.pi.pps.clarity.cv

/**
 * Created by datjandra on 3/26/2015.
 */
enum MaterialCategoryCv {
    DNA('DNA'),
    RNA('RNA')

    String materialCategory

    MaterialCategoryCv(String materialCategory) {
        this.materialCategory = materialCategory
    }
}