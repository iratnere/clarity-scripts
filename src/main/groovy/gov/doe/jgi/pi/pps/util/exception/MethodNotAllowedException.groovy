package gov.doe.jgi.pi.pps.util.exception

class MethodNotAllowedException extends WebException {
	
	MethodNotAllowedException() {
		super([code: 'http.method.not.allowed'], 405)
	}
}
