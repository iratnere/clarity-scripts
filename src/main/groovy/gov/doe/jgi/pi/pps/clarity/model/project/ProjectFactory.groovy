package gov.doe.jgi.pi.pps.clarity.model.project

import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProjectNode

class ProjectFactory {

    static final SequencingProject projectInstance(ProjectNode projectNode) {
        if (!projectNode) {
            return null
        }
        if (!projectNode.cache) {
            projectNode.cache = new SequencingProject(projectNode)
        }
        return  (SequencingProject) projectNode.cache
    }

}
