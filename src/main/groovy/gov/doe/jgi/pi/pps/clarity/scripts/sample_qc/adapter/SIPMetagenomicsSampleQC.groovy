package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.adapter

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.SIPUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.SampleQcProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode

class SIPMetagenomicsSampleQC extends SampleQcAdapter {
    List<String> errors = []

    SIPMetagenomicsSampleQC(SampleQcUtility sampleQcUtility, List<Analyte> analytes) {
        super(sampleQcUtility, analytes)
    }

    String checkIfGroupComplete(String groupName, List<String> sampleLimsIds = lookUpSamplesBySipGroupName(groupName)) {
        if(!sampleLimsIds) {
            errors << "No sample found with SIP group name $groupName"
            return errors.last()
        }
        String error = sampleQcUtility.checkIfGroupComplete(groupName, sampleLimsIds)
        if(error)
            errors << error
        return error
    }

    List<String> lookUpSamplesBySipGroupName(String groupName) {
        return sampleQcUtility.lookUpSamplesBySipGroupName(groupName)
    }

    @Override
    String validateAnalytes() {
        checkInputSize(analytes)
        Set<String> groupNames = [] as Set
        analytes.each { Analyte analyte ->
            checkIsSample(analyte)
            groupNames << analyte.claritySample.pmoSample.udfGroupName
        }
        groupNames.each{String groupName ->
            checkIfGroupComplete(groupName)
        }
        validateSampleStatus()
        return errors ? errors.join("\n") : ""
    }

    @Override
    List<ArtifactNode> processRecordDetails(RecordDetailsInfo recordDetailsInfo) {
        failedTubeSamples = []
        Map<ClaritySample, List<String>> sampleSowItemIds = [:].withDefault {[]}
        transferDataFromTableToUdfs(recordDetailsInfo.glsStarletUseSection)
        transferDataFromKeyValueSection(recordDetailsInfo.keyValueSections)
        analytes.groupBy {it.claritySample.pmoSample.udfGroupName}.each { String groupName, List<Analyte> groupSamples ->
            groupSamples.each { Analyte sampleAnalyte ->
                getSowItemsStatus(sampleAnalyte.claritySample.pmoSample.pmoSampleId)?.each{sowItemId, sampleStatus ->
                    if(!(sampleStatus in SOW_ITEM_STATUS))
                        sampleSowItemIds[sampleAnalyte.claritySample] << sowItemId
                }
            }
            String sampleQcResult = SIPUtility.updateGroupQcResult(groupSamples.collect{it.claritySample.pmoSample}, recordDetailsInfo.replicatesFailurePercentAllowed, recordDetailsInfo.controlsFailurePercentAllowed)
            if(sampleQcResult == Analyte.FAIL)
                failedTubeSamples.addAll(groupSamples)
            else
                failedTubeSamples.addAll(groupSamples.findAll{!it.claritySample.pmoSample.passedQc})
        }
        return createScheduledSamples(sampleSowItemIds, recordDetailsInfo.researcherContactId)
    }

    @Override
    Stage getDestinationStage() {
        return Stage.SOW_ITEM_QC
    }

    @Override
    List<String> checkProcessUdfs(SampleQcProcess sampleQcProcess) {
        List<String> errors = []
        if(sampleQcProcess.udfControlsFailurePercentAllowed == null)
            errors << "Process udf 'Controls Failure Percent Allowed' is required."
        if(sampleQcProcess.udfReplicatesFailurePercentAllowed == null)
            errors << "Process udf 'Replicates Failure Percent Allowed' is required."
        return errors
    }
}
