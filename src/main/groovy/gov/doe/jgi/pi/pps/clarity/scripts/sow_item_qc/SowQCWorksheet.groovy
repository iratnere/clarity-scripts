package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.LastKeyValueSection
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.PlateQCKeyValueSection
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.excel.TubeSowQCTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.apache.poi.ss.util.CellReference

/**
 * Created by lvishwas on 6/16/2015.
 */
class SowQCWorksheet {
    ExcelWorkbook sowQCWorksheet
    ClarityProcess process

    SowQCWorksheet(String templateName, ClarityProcess process){
        sowQCWorksheet = new ExcelWorkbook(templateName, process.testMode)
        this.process = process
    }

    TubeSOWQCSheet getTubeSOWQCSheetInstance(){
        return new TubeSOWQCSheet(process.tubeScheduledSamples)
    }

    DynamicPerPlateSheet getDynamicPerPlateSheetInstance(Map<ClaritySample,List<ScheduledSample>> sampleScheduledSamples, int sheetIndex){
        return new DynamicPerPlateSheet(sampleScheduledSamples, sheetIndex)
    }

    PlateSowQCTemplate getPlateSowQCTemplate() {
        return new PlateSowQCTemplate(sowQCWorksheet.getSheetIndex('PlateSowQcTemplate'))
    }

    ExcelWorkbook populateWorksheet() {
        if (process.tubeScheduledSamples){
            TubeSOWQCSheet tubeSheet = new TubeSOWQCSheet(process.tubeScheduledSamples)
            sowQCWorksheet.addSections(tubeSheet.allSections)
        }
        int sheetIndex = 1

        List<DynamicPerPlateSheet> sheetList = []
        process.plateSamples.each{String containerLimsId, Map<ClaritySample,List<ScheduledSample>> sampleScheduledSamples ->
            DynamicPerPlateSheet sheet = new DynamicPerPlateSheet(sampleScheduledSamples, sheetIndex)
            sowQCWorksheet.addSheet(containerLimsId, sheet.sheetIndex)
            sheetList << sheet
            sheetIndex++
        }
        PlateSowQCTemplate srcSheet = new PlateSowQCTemplate(sowQCWorksheet.getSheetIndex('PlateSowQcTemplate'))
        sheetList.each{ DynamicPerPlateSheet sheet ->
            sheet.createSheet(srcSheet)
            sowQCWorksheet.addSections(sheet.sheetSections)
        }
        return sowQCWorksheet
    }

    Map<Integer, List<Section>> readWorksheet(boolean testMode){
        sowQCWorksheet.load()
        String errors = validateSections(sowQCWorksheet.sections.values() as List)
        if(errors){
            process.postErrorMessage("SOW QC Worksheet validation failed.\n ${errors}")
        }
        return sowQCWorksheet.sections.values().groupBy {it.parentSheetIndex}

    }

    String validateSections(List<Section> sections){
        String errors = ''
        sections.each{ Section section ->
            if(section instanceof TableSection){
                section.data?.each{
                    errors += it.validate()
                }
            }
            if(section instanceof KeyValueSection){
                if(section.startCell.formatAsString() != PlateSowQCTemplate.KEY_VALUE_SECTION_1_STARTCELL){
                    errors += section.data?.validate(section.parentSheet.sheetName)
                }
            }
        }
        return errors
    }

    class TubeSOWQCSheet{
        static final int TUBES_SOW_QC_SHEET = 0
        static final String TUBES_SOW_QC_TABLE_STARTCELL = 'A3'
        Section tableSection
        List<SampleAnalyte> tubeSampleAnalytes

        TubeSOWQCSheet(List<SampleAnalyte> sampleAnalytes){
            tubeSampleAnalytes = sampleAnalytes
            tableSection = new TableSection(TUBES_SOW_QC_SHEET, TubeSowQCTableBean.class, TUBES_SOW_QC_TABLE_STARTCELL, null, true)
        }

        List<Section> getAllSections(){
            List<TubeSowQCTableBean> tableBeans = []
            tubeSampleAnalytes.eachWithIndex { SampleAnalyte analyte, Integer index ->
                TubeSowQCTableBean bean = new TubeSowQCTableBean()
                bean.populateBean(analyte)
                tableBeans << bean
            }
            tableSection.data = tableBeans
            return [tableSection]
        }

    }

    class PlateSowQCTemplate{
        static final String KEY_VALUE_SECTION_1_SECTION_NAME = 'Plate SOW QC'
        static final String KEY_VALUE_SECTION_1_STARTCELL = 'A2'
        static final String KEY_VALUE_SECTION_1_ENDCELL = 'B9'

        static final String CONC_PLATE_SECTION_NAME = 'Conc (ng/uL)'
        static final String CONC_PLATE_STARTCELL = 'A13'
        static final String CONC_PLATE_ENDCELL = 'M21'

        static final String INIT_SAMPLE_VOLUME_PLATE_SECTION_NAME = 'Initial Sample Volume (ul)'
        static final String INIT_SAMPLE_VOLUME_PLATE_SECTION_STARTCELL = 'A24'
        static final String INIT_SAMPLE_VOLUME_PLATE_SECTION_ENDCELL = 'M32'

        static final String CUMULATIVE_VOLUME_PLATE_SECTION_NAME = 'Cumulative Volume (ul)'
        static final String CUMULATIVE_VOLUME_PLATE_SECTION_STARTCELL = 'A37'
        static final String CUMULATIVE_VOLUME_PLATE_SECTION_ENDCELL = 'M21'

        static final String AVAILABLE_VOLUME_PLATE_SECTION_NAME = 'Available Volume (ul)'
        static final String AVAILABLE_VOLUME_PLATE_SECTION_STARTCELL = 'A50'
        static final String AVAILABLE_VOLUME_PLATE_SECTION_ENDCELL = 'M58'

        static final String KEY_VALUE_SECTION_2_SECTION_NAME = 'Library Creation Queue'
        static final String KEY_VALUE_SECTION_2_STARTCELL = 'A62'
        static final String KEY_VALUE_SECTION_2_ENDCELL = 'B66'

        static final String ALIQUOT_VOLUME_PLATE_SECTION_NAME = 'Aliquot Volume (ul)'
        static final String ALIQUOT_VOLUME_PLATE_SECTION_STARTCELL = 'A67'
        static final String ALIQUOT_VOLUME_PLATE_SECTION_ENDCELL = 'M75'

        static final String SOW_ITEM_ID_PLATE_SECTION_NAME = 'SOW Item ID (GLS Use Only)'
        static final String SOW_ITEM_ID_PLATE_SECTION_STARTCELL = 'O67'
        static final String SOW_ITEM_ID_PLATE_SECTION_ENDCELL = 'AA75'

        static final String ONE_D_SECTION_HEADER_SECTION_NAME = 'Percentage Aliquots Passed Header'
        static final String ONE_D_SECTION_STARTCELL = 'A48'
        static final String ONE_D_SECTION_VALUE_SECTION_NAME = 'Percentage Aliquots Passed Value'
        static final String ONE_D_SECTION_ENDCELL = 'B48'

        Map<String, Section> commonSections = [:]
        Map<String, Section> repeatedSections = [:]

        PlateSowQCTemplate(int sheetIndex){
            commonSections[KEY_VALUE_SECTION_1_SECTION_NAME] = new KeyValueSection(sheetIndex, PlateQCKeyValueSection.class, KEY_VALUE_SECTION_1_STARTCELL, KEY_VALUE_SECTION_1_ENDCELL)
            commonSections[CONC_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.STRING, CONC_PLATE_STARTCELL, CONC_PLATE_ENDCELL)
            commonSections[INIT_SAMPLE_VOLUME_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.STRING, INIT_SAMPLE_VOLUME_PLATE_SECTION_STARTCELL, INIT_SAMPLE_VOLUME_PLATE_SECTION_ENDCELL)
            commonSections[CUMULATIVE_VOLUME_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, CUMULATIVE_VOLUME_PLATE_SECTION_STARTCELL, CUMULATIVE_VOLUME_PLATE_SECTION_ENDCELL)
            commonSections[ONE_D_SECTION_HEADER_SECTION_NAME] = new OneDSection(sheetIndex, CellTypeEnum.STRING, ONE_D_SECTION_STARTCELL, ONE_D_SECTION_STARTCELL)
            commonSections[ONE_D_SECTION_VALUE_SECTION_NAME] = new OneDSection(sheetIndex, CellTypeEnum.FORMULA, ONE_D_SECTION_ENDCELL, ONE_D_SECTION_ENDCELL)
            commonSections[AVAILABLE_VOLUME_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, AVAILABLE_VOLUME_PLATE_SECTION_STARTCELL, AVAILABLE_VOLUME_PLATE_SECTION_ENDCELL)
            repeatedSections[KEY_VALUE_SECTION_2_SECTION_NAME] = new KeyValueSection(sheetIndex, LastKeyValueSection.class, KEY_VALUE_SECTION_2_STARTCELL, KEY_VALUE_SECTION_2_ENDCELL)
            repeatedSections[ALIQUOT_VOLUME_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, ALIQUOT_VOLUME_PLATE_SECTION_STARTCELL, ALIQUOT_VOLUME_PLATE_SECTION_ENDCELL)
            repeatedSections[SOW_ITEM_ID_PLATE_SECTION_NAME] = new PlateSection(sheetIndex, CellTypeEnum.STRING, SOW_ITEM_ID_PLATE_SECTION_STARTCELL, SOW_ITEM_ID_PLATE_SECTION_ENDCELL)
            commonSections?.each{ String name, Section section ->
                section?.workbook = sowQCWorksheet
            }
            repeatedSections?.each{ String name, Section section ->
                section?.workbook = SowQCWorksheet.this.sowQCWorksheet
            }
        }

        Map<String, Section> getCommonSrcSections(){
            return commonSections
        }

        Map<String, Section> getRepeatedSrcSections(){
            return repeatedSections
        }
    }

    class DynamicPerPlateSheet{
        Map<PmoSample, List<ScheduledSample>> rootSampleScheduledSamples
        int sheetIndex
        PlateSowQCTemplate srcTemplateSheet
        List<Section> sheetSections = []

        static final int KEY_VALUE_SECTION_2_ROW_OFFSET = 12
        static final int KEY_VALUE_SECTION_2_COL_OFFSET = 0
        static final int ALIQUOT_VOLUME_PLATE_SECTION_ROW_OFFSET = 5
        static final int ALIQUOT_VOLUME_PLATE_SECTION_COL_OFFSET = 0
        static final int SOW_ITEM_ID_PLATE_SECTION_ROW_OFFSET = 0
        static final int SOW_ITEM_ID_PLATE_SECTION_COL_OFFSET = 14

        DynamicPerPlateSheet(Map<PmoSample, List<ScheduledSample>> rootSampleScheduledSamples, int sheetIndex){
            this.rootSampleScheduledSamples = rootSampleScheduledSamples
            this.sheetIndex = sheetIndex
        }

        void createSheet(PlateSowQCTemplate srcSheet){
            srcTemplateSheet = srcSheet
            Section srcSection = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.KEY_VALUE_SECTION_1_SECTION_NAME]
            sheetSections << new KeyValueSection(srcSection, sheetIndex, srcSection.beanClass, srcSection.startCell.formatAsString())//index - 0
            populatePlateSowQCKeyValueSection(sheetSections.last())

            Section srcConcSection = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.CONC_PLATE_SECTION_NAME]
            sheetSections << new PlateSection(srcConcSection, sheetIndex, srcConcSection.cellType, srcConcSection.startCell.formatAsString())//index - 1
            Section concentrationSection = sheetSections.last()

            Section srcInitVolSection = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.INIT_SAMPLE_VOLUME_PLATE_SECTION_NAME]
            sheetSections << new PlateSection(srcInitVolSection, sheetIndex, srcInitVolSection.cellType, srcInitVolSection.startCell.formatAsString())//index - 2
            Section srcCumuVolSection = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.CUMULATIVE_VOLUME_PLATE_SECTION_NAME]
            sheetSections << new PlateSection(srcCumuVolSection, sheetIndex, srcCumuVolSection.cellType, srcCumuVolSection.startCell.formatAsString())//index - 3
            Section srcAvailVolSection = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.AVAILABLE_VOLUME_PLATE_SECTION_NAME]
            sheetSections << new PlateSection(srcAvailVolSection, sheetIndex, srcAvailVolSection.cellType, srcAvailVolSection.startCell.formatAsString())//index - 4

            Section src1DSection1 = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.ONE_D_SECTION_HEADER_SECTION_NAME]
            sheetSections << new OneDSection(src1DSection1, sheetIndex, src1DSection1.cellType, src1DSection1.startCell.formatAsString())//index - 5
            sheetSections.last().data = ['Percentage Aliquots Passed (%)']

            Section src1DSection2 = srcTemplateSheet.commonSrcSections[PlateSowQCTemplate.ONE_D_SECTION_VALUE_SECTION_NAME]
            sheetSections << new OneDSection(src1DSection2, sheetIndex, src1DSection2.cellType, src1DSection2.startCell.formatAsString())//index - 6
            sheetSections[0].workbook = sowQCWorksheet
            String sampleCountCell = getCellAddressWithOffset(srcSection, 'samplesCount', 0, 1)
            String start = getCellAddressWithOffset(srcAvailVolSection, 'A1')
            String end = getCellAddressWithOffset(srcAvailVolSection, 'H12')
            String formula = "IF(${sampleCountCell}>0,(${sampleCountCell}-COUNTIF(${start}:${end},\"${Analyte.FAIL}\"))*100/${sampleCountCell},\"\")"
            sheetSections.last().data = [formula]

            Section srcLastKeyValueSection = srcTemplateSheet.repeatedSrcSections[PlateSowQCTemplate.KEY_VALUE_SECTION_2_SECTION_NAME]
            Section srcAliquotVolSection = srcTemplateSheet.repeatedSrcSections[PlateSowQCTemplate.ALIQUOT_VOLUME_PLATE_SECTION_NAME]
            Section srcSowIdSection = srcTemplateSheet.repeatedSrcSections[PlateSowQCTemplate.SOW_ITEM_ID_PLATE_SECTION_NAME]

            CellReference startCell = srcAvailVolSection.startCell
            List<ScheduledSample> scheduledSamples = rootSampleScheduledSamples.values()?.first()
            int repeatCount = scheduledSamples?.size()
            int negativeColOffset = 0
            while(repeatCount){
                ScheduledSample scheduledSample = scheduledSamples[scheduledSamples?.size() - repeatCount]
                startCell = new CellReference(startCell.row + KEY_VALUE_SECTION_2_ROW_OFFSET, startCell.col + KEY_VALUE_SECTION_2_COL_OFFSET + negativeColOffset)
                sheetSections << new KeyValueSection(srcLastKeyValueSection, sheetIndex, srcLastKeyValueSection.beanClass, startCell.formatAsString())
                Section targetAliquotMassSection = sheetSections.last()
                populate2ndKeyValueSection(sheetSections.last(), scheduledSample)

                startCell = new CellReference(startCell.row + ALIQUOT_VOLUME_PLATE_SECTION_ROW_OFFSET, startCell.col + ALIQUOT_VOLUME_PLATE_SECTION_COL_OFFSET)
                Section volSection = new PlateSection(srcAliquotVolSection, sheetIndex, srcAliquotVolSection.cellType, startCell.formatAsString())
                sheetSections << volSection

                startCell = new CellReference(startCell.row + SOW_ITEM_ID_PLATE_SECTION_ROW_OFFSET, startCell.col + SOW_ITEM_ID_PLATE_SECTION_COL_OFFSET)
                Section sowSection = new PlateSection(srcSowIdSection, sheetIndex, srcSowIdSection.cellType, startCell.formatAsString())
                sheetSections << sowSection
                targetAliquotMassSection.workbook = sowQCWorksheet
                String targetAliquotMassCell = getCellAddressWithOffset(targetAliquotMassSection,'targetAliquotMass',0,1)

                populateRepeatedSections(targetAliquotMassCell, [volSection, sowSection], concentrationSection, scheduledSample)
                repeatCount--
                negativeColOffset = -14
            }
            populateSamplePlateSections(sheetSections[1..4])
        }

        void populatePlateSowQCKeyValueSection(Section section){
            PlateQCKeyValueSection bean = new PlateQCKeyValueSection()
            ScheduledSample scheduledSample = rootSampleScheduledSamples.values()?.flatten()?.first()
            int samplesCount = rootSampleScheduledSamples.keySet()?.size()
            bean.populateBean(samplesCount, scheduledSample, process.nodeManager)
            section.data = bean
        }

        List<String> getFailedQcPlateLocations() {
            return rootSampleScheduledSamples?.keySet()?.collect { PmoSample rootSample ->
                String plateLocation = rootSample.sampleAnalyte.containerLocation?.rowColumn?.replaceAll(":","")
                rootSample.udfSampleQCResult == Analyte.FAIL ? plateLocation : null
            }
        }

        void populateSamplePlateSections(List<Section> plateSections){
            Map concentrationMap = [:], initSampleVolume = [:], cumulativeVolume = [:], availableVolume = [:]
            rootSampleScheduledSamples?.keySet().each{ PmoSample rootSample ->
                String plateLocation = rootSample.sampleAnalyte.containerLocation?.rowColumn?.replaceAll(":","")
                concentrationMap[plateLocation] = rootSample.udfConcentration
                initSampleVolume[plateLocation] = rootSample.udfVolumeUl
                cumulativeVolume[plateLocation] = cumulativeVolumeFormula(rootSample)
                availableVolume[plateLocation] = availableVolumeFormula(rootSample)
            }
            plateSections[0].data = concentrationMap
            (plateSections[0] as PlateSection).failedQcPlateLocations = failedQcPlateLocations
            plateSections[1].data = initSampleVolume
            plateSections[2].data = cumulativeVolume
            plateSections[3].data = availableVolume
        }

        String cumulativeVolumeFormula(PmoSample rootSample){
            //IF($B$64="PASS", IF($B$69="Fail",0,$B$69))
            int repeatSectionsIndex = 7
            int kvSectionIndexOffset = 0
            int plateSectionIndexOffset = 1
            int nextGroupIndexOffset = 3
            List<String> formulas = []
            List<ScheduledSample> scheduledSamples = rootSampleScheduledSamples[rootSample]

            scheduledSamples.each{ ScheduledSample scheduledSample ->
                KeyValueSection kvSection = sheetSections[repeatSectionsIndex+kvSectionIndexOffset]
                PlateSection pSection = sheetSections[repeatSectionsIndex+plateSectionIndexOffset]
                String sowItemStatusCellRef = getCellAddressWithOffset(kvSection, 'sowItemStatus', 0, 1)
                String aliquotVolCellRef = getCellAddressWithOffset(pSection, rootSample.sampleAnalyte.containerLocation.rowColumn?.replaceAll(":",""))
                formulas << "IF(${sowItemStatusCellRef}=\"Pass\", IF(ISNUMBER(${aliquotVolCellRef}),${aliquotVolCellRef},0),0)"
                repeatSectionsIndex+=nextGroupIndexOffset
            }
            if(formulas)
                return formulas.join('+')
            return ''
        }

        String availableVolumeFormula(PmoSample rootSample){
            //IF(OR(C25=0,ISBLANK(C25)),"",IF(C25="Fail","Fail",IF(C25-C38<0,"Fail",C25-C38)))
            PlateSection initVolSection = sheetSections[2]
            PlateSection cumuVolSection = sheetSections[3]
            String plateLocation = rootSample.sampleAnalyte.containerLocation.rowColumn?.replaceAll(":","")
            String initVolCellRef = getCellAddressWithOffset(initVolSection, plateLocation)
            String cumuVolCellRef = getCellAddressWithOffset(cumuVolSection, plateLocation)
            //String formula = "!F=IF(${volCell}=0,\"\",IF(${volCell}=\"Fail\",\"Fail\",IF(${volCell}-${cumulativeVolCell}<0,\"Fail\",${volCell}-${cumulativeVolCell})))"
            String formula = "IF(${initVolCellRef}=0,\"\",IF(${initVolCellRef}=\"Fail\",\"Fail\",IF(${initVolCellRef}-${cumuVolCellRef}<0,\"Fail\",${initVolCellRef}-${cumuVolCellRef})))"
            return formula
        }

        void populate2ndKeyValueSection(Section section, ScheduledSample scheduledSample){
            LastKeyValueSection bean = new LastKeyValueSection()
            bean.populateBean(scheduledSample)
            section.data = bean
        }

        void populateRepeatedSections(String targetAliquotMassCell, List<Section> sections, Section concSection, ScheduledSample scheduledSample){
            Map volMap = [:], sowMap = [:]
            Section volSection = sections[0]
            Section sowSection = sections[1]
            List<ArtifactNode> plateScheSamples = scheduledSample.sampleAnalyte.containerNode.contentsArtifactNodes
            concSection.workbook = sowQCWorksheet
            plateScheSamples.each{ArtifactNode artifactNode ->
                SampleAnalyte sampleAnalyte = AnalyteFactory.analyteInstance(artifactNode)
                String plateLocation = sampleAnalyte.containerLocation.rowColumn?.replaceAll(":","")
                String concentrationCell = getCellAddressWithOffset(concSection, plateLocation)
                volMap[plateLocation] = "IF(OR(${targetAliquotMassCell}=\"\",${concentrationCell}=\"\",${targetAliquotMassCell}=0,${concentrationCell}=0),\"\",IF(${concentrationCell}=\"Fail\",\"Fail\",IF(${targetAliquotMassCell}/${concentrationCell}<2.4,2.4,${targetAliquotMassCell}/${concentrationCell})))"
                sowMap[plateLocation] = sampleAnalyte.claritySample.sowItemId
            }
            volSection.data = volMap
            (volSection as PlateSection).failedQcPlateLocations = failedQcPlateLocations
            sowSection.data = sowMap
        }

        String getCellAddressWithOffset(Section section, String fieldName, int rowOffset=0, int colOffset=0){
            CellReference ref = section.getCellReference(fieldName)
            return ref?(new CellReference(ref.row+rowOffset, ref.col+colOffset)).formatAsString():''
        }
    }
}
