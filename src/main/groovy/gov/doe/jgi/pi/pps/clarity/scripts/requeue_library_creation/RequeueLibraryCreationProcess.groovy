package gov.doe.jgi.pi.pps.clarity.scripts.requeue_library_creation

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 8/19/15.
 */
class RequeueLibraryCreationProcess extends ClarityProcess {
    static final logger = LoggerFactory.getLogger(RequeueLibraryCreationProcess.class)

    static ProcessType processType = ProcessType.REQUEUE_LIBRARY_CREATION

    boolean testMode = false

    RequeueLibraryCreationProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'Process Requeue':RequeueLibraryCreation,
                'Route to Next Workflow': RequeueLibraryCreationRouteToWorkflow
        ]
    }

    def validateInputs() {
        logger.info "Starting ${this.class.name} validateInputs"
        inputAnalytes.each { analyte ->
            analyte.validateIsScheduledSample()
        }
    }

}
