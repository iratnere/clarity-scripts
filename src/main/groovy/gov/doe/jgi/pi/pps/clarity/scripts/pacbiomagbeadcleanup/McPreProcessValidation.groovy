package gov.doe.jgi.pi.pps.clarity.scripts.pacbiomagbeadcleanup

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactNodeService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/30/2015.
 */
class McPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(McPreProcessValidation.class)

    void checkInputType(Analyte analyte){
        if (!analyte.sequencerModelCv.isPacBio)
            process.postErrorMessage("Check input $analyte. Inputs must be PacBio library stocks")
        if(!(analyte instanceof ClarityLibraryStock) && !(analyte instanceof ClarityLibraryPool))
            process.postErrorMessage("Invalid input analyte type ($analyte). Expecting Library stocks or Library pools.")
    }

    void checkIfRequeue(Analyte analyte){
        if (analyte.inProgressWorkflow([ClarityWorkflow.REQUEUE_LIBRARY_ANNEALING]))
            process.postErrorMessage("Cannot start MagBead Cleanup on a analyte $analyte if the it is part of an active 'Requeue for Annealing' process.")
    }

    void checkAnnealingComplex(PacBioAnnealingComplex annealingComplex, Analyte analyte){
        List bindingWorkflows = [ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_BINDING]
        if (annealingComplex.inActiveWorkflow(bindingWorkflows) || annealingComplex.inProgressWorkflow(bindingWorkflows))
            process.postErrorMessage("Cannot start MagBead Cleanup on a $analyte if the corresponding $annealingComplex is in the Binding queue or part of an active binding process.")
        if (annealingComplex.inProgressWorkflow([ClarityWorkflow.REQUEUE_LIBRARY_BINDING]))
            process.postErrorMessage("Cannot start MagBead Cleanup on a $analyte if the corresponding $annealingComplex is part of an active 'Requeue for Binding' process.")
    }

    void checkBindingComplex(PacBioBindingComplex bindingComplex, Analyte analyte){
        if (bindingComplex.inProgressWorkflow([
                ClarityWorkflow.PACBIO_SEQUEL_SEQUENCING_PREP,
                ClarityWorkflow.PACBIO_SEQUEL_II_SEQUENCING_PREP,
                ClarityWorkflow.PACBIO_SEQUEL_SEQUENCING_COMPLETE
        ]))
            process.postErrorMessage("Cannot start MagBead Cleanup on a $analyte if the corresponding $bindingComplex is part of an active sequencing process.")

    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        ArtifactNodeService artifactNodeService =
                BeanUtil.getBean(ArtifactNodeService.class)
        process.inputAnalytes.each { Analyte inputAnalyte ->
            checkInputType(inputAnalyte)
            checkIfRequeue(inputAnalyte)
            List<String> descendants = artifactNodeService.getDescendantList(inputAnalyte.id)
            descendants.each {
                Analyte childAnalyte = AnalyteFactory.analyteInstance(processNode.nodeManager.getArtifactNode(it))
                if (childAnalyte instanceof PacBioAnnealingComplex)
                    checkAnnealingComplex(childAnalyte as PacBioAnnealingComplex, inputAnalyte)
                else if (childAnalyte instanceof PacBioBindingComplex)
                    checkBindingComplex(childAnalyte as PacBioBindingComplex, inputAnalyte)
            }
        }
    }
}
