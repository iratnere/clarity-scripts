package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.model.analyte.*
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.OnboardingBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.Migrator
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.migration.PhysicalRunUnitMigrator
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerLocation
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.LoggerFactory

class HudsonAlphaIlluminaOnboardingAdapter implements OnboardingAdapter {
    static final logger = LoggerFactory.getLogger(HudsonAlphaIlluminaOnboardingAdapter.class)
    ExcelWorkbook onboardingWorkbook
    List<HudsonAlphaBean> allOnboardingBeansCached
    List<HudsonAlphaBean> uniqueOnboardingBeansCached
    Long researcherContactId
    @Delegate
    OnboardingBeansParser parser
    static final String UNDETERMINED = "Undetermined"

    NodeManager getNodeManager() {
        return BeanUtil.nodeManager
    }

    HudsonAlphaIlluminaOnboardingAdapter(ExcelWorkbook onboardingWorkbook, Long researcherId) {
        this.onboardingWorkbook = onboardingWorkbook
        this.researcherContactId = researcherId
        parser = new OnboardingBeansParser()
    }

    List<HudsonAlphaBean> getUniqueOnboardingBeans() {
        if(!uniqueOnboardingBeansCached) {
            uniqueOnboardingBeansCached = allOnboardingBeans?.unique()
            uniqueOnboardingBeansCached?.removeAll(uniqueOnboardingBeansCached.findAll{it.indexSequence == UNDETERMINED})
        }
        return uniqueOnboardingBeansCached
    }

    List<HudsonAlphaBean> getAllOnboardingBeans(){
        if(!allOnboardingBeansCached) {
            Section section = onboardingWorkbook.sections.values().find { it.beanClass == HudsonAlphaBean.class }
            allOnboardingBeansCached = section?.data
        }
        return allOnboardingBeansCached
    }

    @Override
    List<String> validateOnboardingData() {
        List<String> errors = validateAllBeans()
        if (!errors) {
            errors = checkMissingFields(uniqueOnboardingBeans)
            errors.addAll(validateBeansWithUSS(uniqueOnboardingBeans))
            if(!errors)
                errors = validateBeansWithClarity(uniqueOnboardingBeans)
        }
        if(!errors)
            errors.addAll(validate(uniqueOnboardingBeans))

        return errors
    }

    List<String> validateAllBeans(List<HudsonAlphaBean> beans = allOnboardingBeans) {
        List<String> errors = []
        beans.each { HudsonAlphaBean bean ->
            if (!bean.flowcellBarcode)
                errors << "'flowcell barcode' is required for all rows."
            if (!bean.laneNumber)
                errors << "'lane number' is required for all rows."
            if (!bean.fastqFileName)
                errors << "'fastq file name' cannot be blank."
            if (!bean.indexSequence)
                errors << "'index sequence' cannot be blank."
            if (!bean.fastqFileName?.contains(UNDETERMINED) && bean.indexSequence != UNDETERMINED) {
                if (!bean.sampleName || !bean.collaboratorLibraryName)
                    errors << "Sample Name and Library Name fields are required for flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}"
            }
        }
        return errors.unique()
    }

    List<String> checkMissingFields(List<HudsonAlphaBean> beans) {
        List<String> errors = []
        for (HudsonAlphaBean bean : beans) {
            if (!bean.collaboratorLibraryName)
                errors << "'library name' is required. Check flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}."
            if (!bean.sampleName)
                errors << "'sample name' is required. Check flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}."
            if (!bean.libraryCreationSite)
                errors << "'Library Creation Site' is required. Check flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}."
            if (!bean.indexSequence)
                errors << "'index sequence' is required. Check flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}."
        }
        return errors
    }

    List<String> validateBeansWithClarity(List<HudsonAlphaBean> beans, NodeManager nodeManager = this.nodeManager) {
        List<String> errors = lookUpFlowcells(beans)
        List<HudsonAlphaBean> mergedBeans = beans.findAll { it.libraryStock }
        mergedBeans.each {
            String error = processLibrary(it, it.libraryStock)
            if(error)
                errors << error
        }
        errors.addAll(lookUpLibraries(beans - mergedBeans,nodeManager))
        List<HudsonAlphaBean> unMergedBeans = beans.findAll { !it.pmoSample }
        errors.addAll(lookUpSamples(unMergedBeans, nodeManager))
        if(!errors)
            nodeManager.httpPutNodesBatch(beans.collect{it.pmoSample.sampleNode})
        return errors
    }

    List<String> validateBeansWithUSS(List<HudsonAlphaBean> beans, ArtifactIndexService artifactIndexService = BeanUtil.getBean(ArtifactIndexService.class)) {
        List<String> errors = []
        Map<String,String> indexSeqName = artifactIndexService.getIndexForSequences(beans.collect { it.indexSequence })
        beans.each { HudsonAlphaBean bean ->
            bean.indexName = indexSeqName[bean.indexSequence]
            if(!bean.indexName)
                errors << "The index sequence ${bean.indexSequence} is not registered with clarity. Check flowcell ${bean.flowcellBarcode} lane ${bean.getLaneNumber()}."
            else
                bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_INDEX_NAME] = bean.indexName
        }
        return errors
    }

    List<String> lookUpFlowcells(Collection<HudsonAlphaBean> beans, NodeManager nodeManager = this.nodeManager) {
        List<String> errors = []
        Map<String, List<HudsonAlphaBean>> beansByBarcode = beans.groupBy {it.flowcellBarcode}
        Map<String, ContainerNode> flowcells = Migrator.lookUpFlowcellsByBarcode(nodeManager, beansByBarcode.keySet())
        if(flowcells) {
            beansByBarcode.each { String flowcellBarcode, List<HudsonAlphaBean> fbeans ->
                ContainerNode flowcell = flowcells[flowcellBarcode]
                if(flowcell)
                    fbeans.each {
                        String error = processFlowcell(it, flowcell)
                        if(error)
                            errors << error
                    }
            }
        }
        return errors
    }

    String processFlowcell(HudsonAlphaBean bean, ContainerNode flowcell, NodeManager nodeManager = this.nodeManager) {
        ContainerLocation location = new ContainerLocation(flowcell.id, "${bean.getLaneNumber()}", '1')
        Analyte analyte = Migrator.getAnalyteByLocation(nodeManager, location)
        if (analyte) {
            if (!analyte.claritySamples.collect { it.pmoSample.udfCollaboratorSampleName }.contains(bean.sampleName))
                return "Lane number ${bean.getLaneNumber()} of flowcell $bean.flowcellBarcode already in use."
            bean.addAnalyte(analyte)
        }
        return ""
    }

    @Override
    void prepareUdfsMap(OnboardingBean bean) {
        bean.udfMaps[PmoSample.class][ClarityUdf.SAMPLE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ScheduledSample.class][ClarityUdf.SAMPLE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ClaritySampleAliquot.class][ClarityUdf.ANALYTE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ClarityLibraryPool.class][ClarityUdf.ANALYTE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ClarityPhysicalRunUnit.class][ClarityUdf.ANALYTE_EXTERNAL] = EXTERNAL_UDF_VALUE
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_COLLABORATOR_LIBRARY_NAME] = bean.getLabLibraryName()
        bean.udfMaps[ClarityLibraryStock.class][ClarityUdf.ANALYTE_INDEX_NAME] = bean.indexName?:''
    }

    @Override
    String getAction() {
        return "Onboarding sequencing files"
    }

    @Override
    void mergeDataToClarity(NodeManager nodeManager = this.nodeManager) {
        allOnboardingBeans.each {
            it.contactId = researcherContactId
            prepareUdfsMap(it)
        }
        prepareBeansForOnboarding(uniqueOnboardingBeans)
        Migrator migrator = new PhysicalRunUnitMigrator(nodeManager)
        migrator.parentMigrator = migrator.getParentMigrator(uniqueOnboardingBeans)
        migrator.createAnalytesInClarity(uniqueOnboardingBeans, true)
        uniqueOnboardingBeans.each{ HudsonAlphaBean bean ->
            List<HudsonAlphaBean> duplicateBeans = allOnboardingBeans.findAll{it.equals(bean)}
            duplicateBeans.each{ HudsonAlphaBean dBean ->
                dBean.addAnalyte(bean.physicalRunUnit)
            }
        }
        Migrator.submitRoutingRequests()
        nodeManager.httpPutDirtyNodes()
        logger.info "Created analytes: ${uniqueOnboardingBeans.collect{it.analytes}?.join(',')}"
    }

    @Override
    void archiveOnboardingResult(String artifactId, NodeManager nodeManager = this.nodeManager){
        int sheetIndex = getOnboardingDetailsSheetIndex(onboardingWorkbook)
        onboardingWorkbook.addSection(prepareOnboardingDetails(uniqueOnboardingBeans, sheetIndex))
        onboardingWorkbook.store(nodeManager.nodeConfig, artifactId, false)
    }

    @Override
    List<Analyte> getAnalytes() {
        return uniqueOnboardingBeans.collect{it.analytes}.flatten()
    }
}