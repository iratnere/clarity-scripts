package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.scripts

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans.FractionsTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.exception.WebException

class SampleFractionationProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.SM_SAMPLE_FRACTIONATION

	static final String SCRIPT_GENERATED_TRANSFER_SHEET = 'Download Transfer Worksheet'
	static final String UPLOADED_TRANSFER_SHEET = 'Upload Transfer Worksheet'
    static final String TEMPLATE_NAME = 'resources/templates/excel/FractionsTransfer'
    static final BigDecimal DEFAULT_VOLUME_UL = 15.00
    String excelFileName = UPLOADED_TRANSFER_SHEET

    boolean testMode = false

    ExcelWorkbook excelWorkbookCached
    List fractionsBeansCached

    SampleFractionationProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                //'PreProcessValidation':SfPreProcessValidation,
                'ProcessDensityFiles': ProcessDensityFiles,
                'PrepareTransferSheet': PrepareTransferSheet,
                'ProcessTransferSheet': ProcessTransferSheet,
                'RouteToNextWorkflow': RouteToWorkflow
        ]
    }

    List<FractionsTableBean> getFractionsBeans() {
        if (fractionsBeansCached) {
            return fractionsBeansCached
        }
        List<FractionsTableBean> beansList = (List<FractionsTableBean>) getBeanList(
                FractionsTableBean.class.simpleName,
                getExcelWorkbook()
        )
        validateBeans(beansList)
        fractionsBeansCached = beansList
        return fractionsBeansCached
    }

    static void validateBeans(List<FractionsTableBean> beansList){
        List<String> validBarcodes = []
        beansList.each{
            def errorMsg = it.validate(validBarcodes)
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        def passedSize = beansList.findAll{it.passed}?.size()
        if (passedSize && passedSize > 192)
            postErrorMessage("""
Invalid number of passed fraction: $passedSize.
The instrument cannot support more than 192 tubes.`
""")
    }

    FractionsTableBean getFractionsBean(limsId) {
        def tableBean = fractionsBeans.find{ it.sampleLimsId == limsId }
        if (!tableBean) {
            throw new WebException([code:'SampleFractionationProcess.FractionsTableBean.missing', args:[limsId]], 422)
        }
        tableBean
    }

    ExcelWorkbook getExcelWorkbook() {
        if (excelWorkbookCached) {
            return excelWorkbookCached
        }
        def fileNode = getFileNode(excelFileName)
        excelWorkbookCached = new ExcelWorkbook(fileNode.id)
        excelWorkbookCached.load()
        excelWorkbookCached
    }

    static def getBeanList(String beanName, ExcelWorkbook workbook) {
        def beansList = workbook.sections.values()?.find {
            it.beanClass.simpleName == beanName
        }?.getData()
        beansList
    }

    static def parseToBigDecimalError(def object) {
        try{
            object as BigDecimal
        } catch (e) {
            return e
        }
        return null
    }
}