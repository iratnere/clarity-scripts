package gov.doe.jgi.pi.pps.clarity.scripts.abandon.email_notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import org.apache.commons.lang.builder.HashCodeBuilder

class AbandonWorkEmailDetails extends EmailDetails{
    def sowItemId
    def sowItemType
    def targetTemplateSizeBp
    def targetInsertSizeKb
    def notes

    AbandonWorkEmailDetails(Analyte analyte) {
        super(analyte)
        ClaritySample scheduledSample = (ScheduledSample) analyte.claritySample
        sowItemId = scheduledSample.sowItemId
        sowItemType = scheduledSample.udfSowItemType
        targetTemplateSizeBp = scheduledSample.udfTargetTemplateSizeBp
        targetInsertSizeKb = scheduledSample.udfTargetInsertSizeKb
        notes = scheduledSample.udfNotes
    }

    @Override
    def getHashCode() {
        return new HashCodeBuilder(17, 37).
                append(sowItemId).
                toHashCode()
    }

    @Override
    def getInfo(){
        return """
Sequencing Project ID               $sequencingProjectId,
Sequencing Project Name             $sequencingProjectName,
PI Name                             $sequencingProjectPIName,
PM Name                             $sequencingProjectManagerName,
PMO Sample ID                       $pmoSampleId,
LIMS Sample ID                      $sampleLimsId,
SOW Item ID                         $sowItemId,
SOW Item Type                       $sowItemType,
SOW Item Target Template size       $targetTemplateSizeBp,
SOW Item Target Insert size         $targetInsertSizeKb,
Notes                               $notes,
Sample mass                         $sampleMass
"""
    }
}
