package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.CsvBuilder
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.FileUploadUtil
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.SequenceService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.lang.reflect.Field

/**
 * Created by datjandra on 9/16/2015.
 */
class PbPrepareRunDesign extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbPrepareRunDesign.class)

    void execute() {
        PacBioSequencingPlateCreation clarityProcess  = process as PacBioSequencingPlateCreation
        try {
            String pacBioRunNumber = generatePacBioRunNumber()
            updateOutputContainerName(pacBioRunNumber)
            if (process.isPacBioSequel) {
                String runName = runName(clarityProcess.sequencerName, pacBioRunNumber)
                def prefix = runName + '_' + new java.text.SimpleDateFormat('MMddyyyy').format(new Date()) + '_'
                String smartLink7 = createLibraryContents(clarityProcess.getPbSequelLibraryBeans(runName))
                uploadSequelLibraryFile(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7, smartLink7, prefix)
            } else {
                throw new WebException("no handler for input stage ${process.inputStage} of process ${process}", 422)
            }
        } catch (Exception e) {
            String date = "${new java.text.SimpleDateFormat('MMddyyyy').format(new Date())}"
            def prefix = 'Error_' + date + 'x_'
            uploadSequelLibraryFile(PacBioSequencingPlateCreation.RUN_DESIGN_SMARTLINK_7, e.message, prefix)
            throw e
        }
    }

    static String runName(pacBioSequencer, runId) {
        "${pacBioSequencer}_Run${runId}"
    }

    static String generatePacBioRunNumber() {
        SequenceService sequenceService = BeanUtil.getBean(SequenceService.class)
        return sequenceService.nextPacBioRunNumber()?.toString()
    }

    def updateOutputContainerName(String pacBioRunNumber) {
        ContainerNode containerNode = (process as PacBioSequencingPlateCreation).outputContainerNode
        containerNode.readOnly = false
        containerNode.name = runNumberToContainerName(pacBioRunNumber)
    }

    static String runNumberToContainerName(String pacBioRunNumber) {
        "pbr${pacBioRunNumber}"
    }

    static String containerNameToRunNumber(String containerName) {
        containerName - /pbr/
    }

    void uploadSequelLibraryFile(String placeholderName, String fileContents, String fileName) {
        NodeConfig nodeConfig = BeanUtil.nodeManager.nodeConfig
        ArtifactNode fileNode = process.getFileNode(placeholderName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        Node node = FileUploadUtil.uploadTextFile(
                fileNode.id,
                fileContents,
                nodeConfig,
                '.csv',
                (process as PacBioSequencingPlateCreation).testMode,
                fileName
        )
        logger.info "Uploaded Sequel Library File $fileName to $node"
    }

    //called by ProcessRunDesign as well
    static String createLibraryContents(List beans) {
        def beanClass = beans[0].class
        def beanHeaders = getBeanHeaders(beanClass)
        CsvBuilder builder = new CsvBuilder(beanHeaders.size(), false)
        builder.append(beanHeaders)
        beans.each{
            builder.append(createFileRow(it))
        }
        builder.done()
        return builder.toString()
    }

    static def getBeanHeaders(def beanClass) {
        return beanClass.declaredFields
                .findAll { field -> (field as Field).isAnnotationPresent(FieldMapping.class) }
                .collect { field -> (field as Field).getAnnotation(FieldMapping.class).header()?.trim() }
    }

    static Map<String, ClarityLibraryStock> getIndexNameToLibraryMap(ClarityLibraryPool clarityLibraryPool){
        Map<String, ClarityLibraryStock> indexNameToLibrary = [:]
        clarityLibraryPool.poolMembers.each{
            ClarityLibraryStock libraryStock  = it as ClarityLibraryStock
            indexNameToLibrary[libraryStock.udfIndexName] = libraryStock
        }
        indexNameToLibrary
    }

    static def createFileRow(def bean) {
        return bean.class.declaredFields
                .findAll { field -> (field as Field).isAnnotationPresent(FieldMapping.class) }
                .collect { field -> bean."${(field as Field).name}"?.toString()?.trim() }
    }
}