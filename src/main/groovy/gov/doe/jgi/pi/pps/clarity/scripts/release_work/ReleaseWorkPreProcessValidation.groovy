package gov.doe.jgi.pi.pps.clarity.scripts.release_work

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/24/16.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 *
 * Clarity process “Release from Hold”/“Release from Needs Attention”.
 * PPV: do the original queues still exist or are they archived
 */
class ReleaseWorkPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ReleaseWorkPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        //PPV: do the original queues still exist or are they archived
        validateMoveToWorkflows()
    }

    void validateMoveToWorkflows(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { Analyte analyte ->
            process.getLastRemovedStages(analyte).each{ validateStage(it, analyte) }
        }
    }

    void validateStage(ArtifactWorkflowStage stage, Analyte analyte) {
        if (!stage.workflowNode.isActive) {
            process.postErrorMessage("""
Cannot move an analyte to the 'ARCHIVED' workflow:
analyte $analyte.name/$analyte,
workflow ${stage.workflowNode.name}/${stage.workflowNode.workflowUri}.
Please file a JIRA ticket.
            """)
        }
    }

}
