package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import org.apache.commons.lang3.builder.HashCodeBuilder

/**
 * Created by lvishwas on 9/17/16.
 */
class SowQcFailedEmailDetails extends EmailDetails{
    def sowItemId
    def sowItemType
    def defaultLibraryQueue
    def failureMode
    def sampleQCResult
    def sowQCResult
    def concentration
    def volume
    def sampleQCComments
    def SowItemQCComments

    SowQcFailedEmailDetails(Analyte analyte){
        super(analyte)
        ClaritySample claritySample = analyte.claritySample
        sowItemId = claritySample.sowItemId
        sowItemType = claritySample.udfSowItemType
        defaultLibraryQueue = claritySample.libraryCreationQueue?claritySample.libraryCreationQueue.libraryCreationQueue:''
        failureMode = claritySample.udfSowItemQcFailureMode
        sampleQCResult = claritySample.pmoSample.udfSampleQCResult
        sowQCResult = claritySample.udfSowItemQcResult
        concentration = claritySample.pmoSample.udfConcentration
        volume = claritySample.pmoSample.udfVolumeUl
        sampleQCComments = claritySample.pmoSample.udfSampleQcNotes
        SowItemQCComments = claritySample.udfNotes
    }

    @Override
    def getHashCode(){
        return new HashCodeBuilder(17, 37).
                append(sequencingProjectManagerId).
                toHashCode()
    }

    @Override
    def getInfo() {
        //PPS-4285 - 1) the sample contact name - list under the PI
        return """
Sequencing Project ID   $sequencingProjectId
Sequencing Project Name $sequencingProjectName
PI Name                 $sequencingProjectPIName
Sample Contact Name     $sampleContactName
PM Name                 $sequencingProjectManagerName
PMO Sample ID           $pmoSampleId
LIMS Sample ID          $sampleLimsId
SOW Item ID             $sowItemId
SOW Item Type           $sowItemType
Default library queue   $defaultLibraryQueue
Failure Mode            $failureMode
sample QC result        $sampleQCResult
SOW QC result           $sowQCResult
Concentration           $concentration
Volume                  $volume
Sample QC comments      $sampleQCComments
SOW item QC comments    $SowItemQCComments
"""
    }
}
