package gov.doe.jgi.pi.pps.clarity.util

class IdGenerator {

    private static long lastTime
    private static int increment = 0

    static synchronized String nextId() {
        long time = System.currentTimeMillis()
        if (time == lastTime) {
            increment++
        } else {
            lastTime = time
            increment = 0
        }
        "${Long.toString(time).reverse()}-${increment}"
    }

}
