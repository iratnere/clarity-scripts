package gov.doe.jgi.pi.pps.clarity.scripts.services

import gov.doe.jgi.pi.pps.clarity.domain.SampleAliquotFailureModeCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SampleAliquotFailureModesService {

    @Autowired
    DropDownService dropDownService

    String[] getActiveFailureModes() {
        return SampleAliquotFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }

    DropDownList getDropDownFailureModes() {
        DropDownList dropDownList = new DropDownList()
        dropDownList.setControlledVocabulary(activeFailureModes)
        return dropDownList
    }

    DropDownList getDropDownPassFail() {
        return dropDownService.dropDownPassFail
    }
}
