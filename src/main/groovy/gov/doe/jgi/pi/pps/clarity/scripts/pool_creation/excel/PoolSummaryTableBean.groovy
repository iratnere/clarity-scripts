package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class PoolSummaryTableBean {
    @FieldMapping(header='NovaSeq Pool #', cellType = CellTypeEnum.NUMBER)
    public def poolNumber

}
