package gov.doe.jgi.pi.pps.clarity.scripts.print

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ClarityAttachmentUtil
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.print.beans.SampleTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by tlpaley on 3/25/16.
 */
class PrintLabelsProcess extends ClarityProcess{

    static ProcessType processType = ProcessType.PRINT_LABELS

    static final String UPLOADED_SAMPLE_SHEET = 'Upload Sample Worksheet'

    ExcelWorkbook excelWorkbookCached
    List sampleAnalyteBeansCached

    public PrintLabelsProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'Process Sample Worksheet': ProcessSampleSheet,
                'Print Barcode': PrintBarcode
        ]
    }

    List<SampleTableBean> getSampleAnalyteBeans() {
        if (sampleAnalyteBeansCached) {
            return sampleAnalyteBeansCached
        }
        List<SampleTableBean> beansList = (List<SampleTableBean>) getBeanList(SampleTableBean.class.simpleName)
        beansList.each {
            def errorMsg = it.validate()
            if(errorMsg)
                postErrorMessage(errorMsg as String)
        }
        sampleAnalyteBeansCached = beansList
        return sampleAnalyteBeansCached
    }

    ExcelWorkbook getExcelWorkbook() {
        if (excelWorkbookCached) {
            return excelWorkbookCached
        }
        def fileNode = getFileNode(UPLOADED_SAMPLE_SHEET)
        def excelWorkbook = ClarityAttachmentUtil.downloadExcelWorkbook(fileNode.id)
        Section section = new TableSection(0, SampleTableBean.class, 'A1')
        excelWorkbook.addSection(section)
        excelWorkbook.load()
        excelWorkbookCached = excelWorkbook
        excelWorkbookCached
    }

    def getBeanList(String beanName) {
        def beansList = getExcelWorkbook().sections.values()?.find {
            it.beanClass.simpleName == beanName
        }?.getData()
        beansList
    }

}