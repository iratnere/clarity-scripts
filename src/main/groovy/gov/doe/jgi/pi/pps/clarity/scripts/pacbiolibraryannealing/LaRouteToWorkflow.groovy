package gov.doe.jgi.pi.pps.clarity.scripts.pacbiolibraryannealing

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.AnalyteFactory
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.util.exception.WebException
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by lvishwas on 4/6/2015.
 */
class  LaRouteToWorkflow extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LaRouteToWorkflow.class)
    void execute(){
        logger.info "Starting ${this.class.name} action...."
        List<ArtifactNode> inputAnalytes = processNode.inputAnalytes
        for (ArtifactNode input : inputAnalytes) {
            Analyte inputAnalyte = AnalyteFactory.analyteInstance(input)
            List<ArtifactNode> outputArtifacts = processNode.getOutputAnalytes(input.id)
            Analyte outputAnalyte = AnalyteFactory.analyteInstance(outputArtifacts.first())

            PacBioAnnealingComplex annealingComplex = outputAnalyte as PacBioAnnealingComplex
            BigDecimal volumeUsed = annealingComplex.udfVolumeUsedUl
            if (volumeUsed == null) {
                process.postErrorMessage("Library Volume Used (uL) required for $annealingComplex")
            } else {
                inputAnalyte.setUdfVolumeUl(inputAnalyte.udfVolumeUl - volumeUsed)
            }

            if (annealingComplex.udfVolumeUl == null) {
                process.postErrorMessage("Volume (uL) required for $annealingComplex")
            }

            if (annealingComplex.udfPacBioMolarityNm == null) {
                process.postErrorMessage("PacBio molarity required for $annealingComplex")
            }
        }
        performLastStepActions()
        process.writeLabels(process.outputAnalytes)
    }

    void performLastStepActions(){
        List<Analyte> passedOutputAnalytes = []
        Set<ArtifactNodeInterface> failedOutputs = new HashSet<ArtifactNodeInterface>()
        process.outputAnalytes.each { Analyte output ->
            if (output.artifactNode.systemQcFlag?.booleanValue()) {
                passedOutputAnalytes << output
            } else if (!output.artifactNode.systemQcFlag?.booleanValue()) {
                failedOutputs.add(output.artifactNode)
            } else {
                process.postErrorMessage("QC flag required for $output")
            }
        }

        if (process.isPacBioSequel) {
            process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_BINDING,passedOutputAnalytes)
        } else {
            throw new WebException("no handler for ${this.process} input stage [${process.inputStage}]",422)
        }

        if (failedOutputs) {
            process.routeArtifactNodes( Stage.ABANDON_QUEUE,  failedOutputs )
            StatusService statusService =
                    BeanUtil.getBean(StatusService.class)
            Map<Long, SowItemStatusCv> sowItemStatusMap = new HashMap<Long, SowItemStatusCv>()
            failedOutputs.each { ArtifactNodeInterface artifactNode ->
                List<ClaritySample> claritySamples = artifactNode.parentSampleNodes.collect{ SampleFactory.sampleInstance(it)}
                claritySamples.each { ClaritySample claritySample ->
                    if (claritySample instanceof ScheduledSample) {
                        sowItemStatusMap.put((claritySample as ScheduledSample).getSowItemId(), SowItemStatusCv.NEEDS_ATTENTION)
                    }
                }
            }
            statusService.submitSowItemStatus(sowItemStatusMap, process.researcherContactId)
        }
    }
}
