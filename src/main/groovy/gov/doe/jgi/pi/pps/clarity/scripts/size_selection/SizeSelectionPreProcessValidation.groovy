package gov.doe.jgi.pi.pps.clarity.scripts.size_selection

import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 */
class SizeSelectionPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(SizeSelectionPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        validateInputAnalyteClass()
    }

    void validateInputAnalyteClass(def inputAnalytes = process.inputAnalytes) {
        inputAnalytes.each { analyte ->
            if (!(analyte instanceof ClarityLibraryPool)) {
                process.postErrorMessage("Input Analyte '$analyte.id' has an invalid analyte class: '${analyte.class?.simpleName}'. Expected class: 'ClarityLibraryPool'.")
            }
        }
    }

}
