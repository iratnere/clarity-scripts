package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by datjandra on 6/2/2015.
 */
class MiSeqSampleSheetReadsBean {
    @FieldMapping(header="[Reads]", cellType= CellTypeEnum.NUMBER)
    public Integer read
}
