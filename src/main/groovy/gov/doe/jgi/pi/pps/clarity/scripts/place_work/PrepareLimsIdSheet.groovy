package gov.doe.jgi.pi.pps.clarity.scripts.place_work

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.place_work.beans.LimsIdTableBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 12/5/14.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 */
class PrepareLimsIdSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(PrepareLimsIdSheet.class)
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        uploadLimsIdWorksheet()
    }

    void uploadLimsIdWorksheet(String fileName = PlaceWorkProcess.SCRIPT_GENERATED_LIMS_ID_SHEET) {
        ArtifactNode fileNode = process.getFileNode(fileName)
        logger.info "Uploading script generated file ${fileNode.id}..."
        ExcelWorkbook excelWorkbook = new ExcelWorkbook(PlaceWorkProcess.TEMPLATE_NAME)
        excelWorkbook.store(process.nodeManager.nodeConfig, fileNode.id, false)
    }

    static Section getTableSection() {
        Section tableSection = new TableSection(0,LimsIdTableBean.class,'A1',null,true)
        return tableSection
    }

}
