package gov.doe.jgi.pi.pps.clarity.scripts.sequence_analysis

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.services.PostProcessService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.apache.http.HttpStatus
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 6/4/2015.
 */
class SqAnalysisRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(SqAnalysisRouteToWorkflow.class)

    def getSeqParents(){
        Map<String,List<Analyte>> seqParents = new HashMap<String,List<Analyte>>().withDefault {
            new ArrayList<ArtifactNode>()
        }

        for (ClarityPhysicalRunUnit inputAnalyte : process.inputAnalytes) {
            if (inputAnalyte.artifactNode.isControlAnalyte) {
                continue
            }

            Boolean inputPassed = inputAnalyte.systemQcFlag
            if (inputPassed == null) {
                postFatalError("QC flag must be selected", HttpStatus.SC_BAD_REQUEST)
            }

            List<Analyte> parents = inputAnalyte.getParents()//artifactNode.parentAnalytes
            for (Analyte parent : parents) {

                if (!inputPassed.booleanValue()) {
                    parent.artifactNode.setUdf(ClarityUdf.ANALYTE_SEQUENCING_FAILURE_MODE.udf, inputAnalyte.sequencingFailureMode)
                    seqParents[parent.runModeCv.getSequencingWorkflowUri()].add(parent)
                    //seqParents["SQ Cluster Generation ${sequencerModel.platform} ${sequencerModel.sequencerModel}"].add(parent)
                }
            }
        }
        return seqParents
    }

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        performLastStepActions()
    }

    void performLastStepActions(){
        def seqParentsMap = seqParents
        List<Analyte> autoRequeueAnalytes = []
        seqParentsMap.each { String workflowUri, List<Analyte> parents ->
            process.routeArtifactIdsToUri(workflowUri, parents*.id)
            autoRequeueAnalytes.addAll(parents)
        }
        PostProcessService taskGenerationService = BeanUtil.getBean(PostProcessService.class)
        taskGenerationService.postInputProcess(process, Stage.AUTOMATIC_REQUEUE_SEQUENCE_ANALYSIS, autoRequeueAnalytes*.artifactNode)

    }
}
