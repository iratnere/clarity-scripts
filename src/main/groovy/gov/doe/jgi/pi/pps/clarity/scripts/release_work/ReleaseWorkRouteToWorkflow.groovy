package gov.doe.jgi.pi.pps.clarity.scripts.release_work

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 2/24/16.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 *
 * Clarity process “Release from Hold”/“Release from Needs Attention”.
 * Move analytes back to their original queues
 * Set corresponding sow items status to “In Progress”
 */
class ReleaseWorkRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ReleaseWorkRouteToWorkflow.class)

    void execute() {
        logger.info "${this.class} execute()"
        assignToNextWorkflow()
    }

    void assignToNextWorkflow(){
        groupInputsByDestinationStage().each{ String stageUri, List<ArtifactNode> artifactNodes ->
            process.routeArtifactIdsToUri(stageUri,artifactNodes*.id)
        }
    }

    Map<String, List<ArtifactNode>> groupInputsByDestinationStage(List<Analyte> analytes = process.inputAnalytes){
        Map<String, List<ArtifactNode>> outputsByStage = [:].withDefault{[]}
        analytes.each{
            process.getLastRemovedStages(it).each{ ArtifactWorkflowStage stage ->
                outputsByStage[stage.uri] << it.artifactNode
            }
        }
        return outputsByStage
    }

}