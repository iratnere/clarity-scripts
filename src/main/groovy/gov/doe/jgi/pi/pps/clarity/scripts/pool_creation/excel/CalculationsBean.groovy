package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

class CalculationsBean {
    @FieldMapping(header = 'Library Name', cellType = CellTypeEnum.STRING)
    public def libraryName
    @FieldMapping(header = 'Sow Item Id', cellType = CellTypeEnum.STRING)
    public def sowItemId
    @FieldMapping(header = 'Number of Reads (Bp)', cellType = CellTypeEnum.NUMBER)
    public def numberOfReadsBp
    @FieldMapping(header = 'Total Number of Reads', cellType = CellTypeEnum.NUMBER)
    public def totalNumberOfReads
    @FieldMapping(header='Sequencing Optimization Factor', cellType = CellTypeEnum.NUMBER)
    public def sof
    @FieldMapping(header='Library Percentage with SOF (%)', cellType = CellTypeEnum.NUMBER)
    public def libraryPercentage

}
