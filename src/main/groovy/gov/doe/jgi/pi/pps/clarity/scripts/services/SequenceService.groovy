package gov.doe.jgi.pi.pps.clarity.scripts.services


import groovy.sql.Sql
import groovy.transform.PackageScope
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class SequenceService {

    @Autowired
    DataSource dataSource

    //for testing
    @PackageScope
    BigInteger currentPacBioRunNumber() {
        Sql sql = new Sql(dataSource)
        def row = sql.firstRow("select 1 as yep from dual")
        return row?.'yep'?.toBigInteger()
    }

    BigInteger nextPacBioRunNumber() {
        Sql sql = new Sql(dataSource)
        def row = sql.firstRow("select uss.pac_bio_run_number_seq.NEXTVAL from dual")
        return row?.'nextval'?.toBigInteger()
    }
}
