package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.scripts.services.ArtifactIndexService

/**
 * Created by datjandra on 5/21/2015.
 */
class MiSeqSampleSheetBean {

    private final static String DEFAULT_GENOME_FOLDER = '/house/sdm/prod/illumina/staging/Sample_Sheets'

    @FieldMapping(header="Sample_ID", cellType= CellTypeEnum.STRING)
    public String sampleId

    @FieldMapping(header="Sample_Name", cellType= CellTypeEnum.STRING)
    public String sampleName

    @FieldMapping(header="index", cellType= CellTypeEnum.STRING)
    public String index

    @FieldMapping(header="index2", cellType= CellTypeEnum.STRING)
    public String indexTwo

    @FieldMapping(header="GenomeFolder", cellType= CellTypeEnum.STRING)
    public String genomeFolder = DEFAULT_GENOME_FOLDER

    def setIndexSequence(String indexSequence){
        if (!indexSequence)
            return
        String[] indexes = indexSequence?.split(ArtifactIndexService.INDEX_CONCAT_DELIMITER)
        index = indexes[0]
        if (indexes?.size() > 1) {
            indexTwo = indexes[1]
        }
    }
}
