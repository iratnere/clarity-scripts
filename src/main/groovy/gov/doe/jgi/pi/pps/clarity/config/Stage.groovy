package gov.doe.jgi.pi.pps.clarity.config

import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.StageNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.StepConfigurationNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.artifact.ArtifactWorkflowStage
import groovy.util.logging.Slf4j

/*
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<stg:stage xmlns:stg="http://genologics.com/ri/stage" index="3" name="10.52 PCA Plate Complete" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/151/stages/408">
    <workflow uri="http://frow.jgi-psf.org:8080/api/v2/configuration/workflows/151"/>
    <protocol uri="http://frow.jgi-psf.org:8080/api/v2/configuration/protocols/101"/>
    <step uri="http://frow.jgi-psf.org:8080/api/v2/configuration/protocols/101/steps/201"/>
</stg:stage>
 */

@Slf4j
enum Stage {

	APPROVE_FOR_SHIPPING('SM Approve For Shipping', ClarityWorkflow.SUPPLY_CHAIN_MANAGEMENT),
	RECEIVE_SAMPLES('Receive Samples From Barcode Association', ClarityWorkflow.RECEIVE_SAMPLES),
	SAMPLE_RECEIPT('SM Sample Receipt', ClarityWorkflow.SAMPLE_RECEIPT),
    SAMPLE_QC_DNA('SM Sample QC', ClarityWorkflow.SAMPLE_QC_DNA),
    SAMPLE_QC_RNA('SM Sample QC', ClarityWorkflow.SAMPLE_QC_RNA),
    SOW_ITEM_QC('SM SOW Item QC', ClarityWorkflow.SOW_ITEM_QC),
	SAMPLE_QC_METABOLOMICS('SM Sample QC Metabolomics', ClarityWorkflow.SAMPLE_QC_METABOLOMICS),
	SAMPLE_FRACTIONATION('SM Sample Fractionation', ClarityWorkflow.SAMPLE_FRACTIONATION),
	ALIQUOT_CREATION_DNA('AC Sample Aliquot Creation', ClarityWorkflow.ALIQUOT_CREATION_DNA),
    ALIQUOT_CREATION_RNA('AC Sample Aliquot Creation', ClarityWorkflow.ALIQUOT_CREATION_RNA),

	LC_NOT_JGI('LC Library Creation', ClarityWorkflow.LC_NOT_JGI),

    PLATE_TO_TUBE_TRANSFER('Plate To Tube Transfer',  ClarityWorkflow.PLATE_TO_TUBE_TRANSFER),
	LIBRARY_QPCR('LQ Library qPCR', ClarityWorkflow.LIBRARY_QUANTIFICATION),
	POOL_CREATION('LP Pool Creation', ClarityWorkflow.POOL_CREATION),
	SIZE_SELECTION('Size Selection', ClarityWorkflow.SIZE_SELECTION),

	PACBIO_SEQUEL_LIBRARY_ANNEALING('PacBio Library Annealing', ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_ANNEALING),
	PACBIO_SEQUEL_SEQUENCING_PREP('PacBio Sequencing Plate Creation', ClarityWorkflow.PACBIO_SEQUEL_SEQUENCING_PREP),
	PACBIO_SEQUEL_II_SEQUENCING_PREP('PacBio Sequencing Plate Creation', ClarityWorkflow.PACBIO_SEQUEL_II_SEQUENCING_PREP),
	PACBIO_SEQUEL_LIBRARY_BINDING('PacBio Library Binding', ClarityWorkflow.PACBIO_SEQUEL_LIBRARY_BINDING),
	PACBIO_SEQUEL_SEQUENCING_COMPLETE('PacBio Sequencing Complete', ClarityWorkflow.PACBIO_SEQUEL_SEQUENCING_COMPLETE),
	PACBIO_SEQUEL_MAGBEAD_CLEANUP('PacBio MagBead Cleanup', ClarityWorkflow.PACBIO_SEQUEL_MAGBEAD_CLEANUP),

	SEQUENCING_ILLUMINA_MISEQ('SQ Sequencing', ClarityWorkflow.SEQUENCING_ILLUMINA_MISEQ),
	SEQUENCING_ILLUMINA_HISEQ_RAPID('SQ Sequencing', ClarityWorkflow.SEQUENCING_ILLUMINA_HISEQ_RAPID),
	SEQUENCING_ILLUMINA_NOVASEQ('SQ Sequencing', ClarityWorkflow.SEQUENCING_ILLUMINA_NOVASEQ),
//	CLUSTERGEN_ILLUMINA_HISEQ_1TB('SQ Sequencing', ClarityWorkflow.CLUSTERGEN_ILLUMINA_HISEQ_1TB),//PPS-5150: inactivate discontinued run modes
//	SQ_SEQUENCE_ANALYSIS('SQ Sequence Analysis', ClarityWorkflow.SEQUENCING_ILLUMINA_HISEQ_1TB),//PPS-5150: inactivate discontinued run modes

	ABANDON_QUEUE('Abandon Queue', ClarityWorkflow.ABANDON_QUEUE),
	ABANDON_WORK('Abandon Work', ClarityWorkflow.ABANDON_WORK),
	ON_HOLD('On Hold', ClarityWorkflow.ON_HOLD_WORKFLOW),
	RELEASE_HOLD('Release from Hold', ClarityWorkflow.RELEASE_HOLD_WORKFLOW),
	NEEDS_ATTENTION('Needs Attention', ClarityWorkflow.NEEDS_ATTENTION_WORKFLOW),
	RELEASE_NEEDS_ATTENTION('Release from Needs Attention', ClarityWorkflow.RELEASE_NEEDS_ATTENTION_WORKFLOW),
	PLACE_ON_HOLD('Place on Hold', ClarityWorkflow.PLACE_ON_HOLD),

	REQUEUE_QC('Requeue for Sample QC', ClarityWorkflow.REQUEUE_FOR_QC),
	REQUEUE_LIBRARY_CREATION('Requeue for Library Creation', ClarityWorkflow.REQUEUE_LIBRARY_CREATION),
	REQUEUE_LIBRARY_ANNEALING('Requeue for Library Annealing', ClarityWorkflow.REQUEUE_LIBRARY_ANNEALING),
	REQUEUE_LIBRARY_BINDING('Requeue for Library Binding', ClarityWorkflow.REQUEUE_LIBRARY_BINDING),

	AUTOMATIC_REQUEUE_LIBRARY_CREATION('Automatic Requeue for Library Creation', ClarityWorkflow.AUTOMATIC_REQUEUE_LIBRARY_CREATION),
	AUTOMATIC_REQUEUE_SEQUENCING('Automatic Requeue for Sequencing', ClarityWorkflow.AUTOMATIC_REQUEUE_SEQUENCING),
	AUTOMATIC_REQUEUE_SEQUENCE_ANALYSIS('Automatic Requeue for Sequencing Analysis', ClarityWorkflow.AUTOMATIC_REQUEUE_SEQUENCE_ANALYSIS),
	AUTOMATIC_REQUEUE_QPCR('Automatic Requeue for qPCR', ClarityWorkflow.AUTOMATIC_REQUEUE_QPCR),
	AUTOMATIC_POOL_CREATION('LP Pool Creation', ClarityWorkflow.AUTOMATIC_POOL_CREATION_WORKFLOW),
	AUTOMATIC_ROUTE_TO_WORKFLOW('Automatic Route to Workflow', ClarityWorkflow.AUTOMATIC_ROUTE_TO_WORKFLOW),
	AUTOMATIC_SOWITEM_REQUEUE_FOR_POOL_CREATION('Automatic SowItem Requeue for Pool Creation', ClarityWorkflow.AUTOMATIC_SOWITEM_REQUEUE_FOR_POOL_CREATION),
	AUTOMATIC_SOWITEM_REQUEUE_SEQUENCING('Automatic SowItem Requeue for Sequencing', ClarityWorkflow.AUTOMATIC_SOWITEM_REQUEUE_FOR_SEQUENCING),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_LIBRARY_CREATION('Automatic Sequence QC Requeue for Library Creation', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_LIBRARY_CREATION),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_SEQUENCING('Automatic Sequence QC Requeue for Sequencing', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_SEQUENCING),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_POOL_CREATION('Automatic Sequence QC Requeue for Pool Creation', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_POOL_CREATION),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_QPCR('Automatic Sequence QC Requeue for qPCR', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_QPCR),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_PACBIO_ANNEALING('Automatic Sequence QC Requeue for PacBio Library Annealing', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_ANNEALING),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_PACBIO_BINDING('Automatic Sequence QC Requeue for PacBio Library Binding', ClarityWorkflow.AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_BINDING),
	AUTOMATIC_SOW_ITEM_QC('SM SOW Item QC', ClarityWorkflow.AUTOMATIC_SOW_ITEM_QC_WORKFLOW),

	SHIP_OFFSITE('SM Ship Off-site', ClarityWorkflow.SHIP_OFFSITE),
    TRASH('SM Trash', ClarityWorkflow.TRASH),
    ON_SITE('SM On Site', ClarityWorkflow.ON_SITE),
    ADJUST_MASS('Adjust Mass', ClarityWorkflow.ADJUST_MASS),
    ADJUST_MOLARITY('Adjust Molarity', ClarityWorkflow.ADJUST_MOLARITY),
    ADJUST_VOLUME('Adjust Volume', ClarityWorkflow.ADJUST_VOLUME)


    final String value
	final ClarityWorkflow workflow

	private Stage(String value, ClarityWorkflow workflow) {
		this.value = value
		this.workflow = workflow
	}

	gov.doe.jgi.pi.pps.clarity_node_manager.node.workflow.Stage getWorkflowStage() {
		workflow.workflowNode?.stages?.find {it.name == value }
	}

	static Stage artifactWorkflowStageToStage(ArtifactWorkflowStage artifactWorkflowStage) {
		String workflowName = artifactWorkflowStage?.workflowNode?.name
        ClarityWorkflow clarityWorkflow = ClarityWorkflow.toEnum(workflowName)
		if (!clarityWorkflow) {
			log.warn "no ClarityWorkflow enum configured for name [${workflowName}]"
			return null
		}
        Stage stage = clarityWorkflow.stages.find{it.uri == artifactWorkflowStage.uri}
		if (!stage) {
			log.warn "no Stage enum configured for workflow [${clarityWorkflow}] and name [${artifactWorkflowStage}] (URI=${artifactWorkflowStage.uri})"
			return null
		}
		stage
	}

	String getUri() {
		workflowStage?.uri
	}

	Integer getId() {
		workflowStage?.id
	}

	Boolean getIsPacBioSequel() {
		workflow.isPacBioSequel
	}

	Boolean getIsPacBio() {
		workflow.isPacBio
	}

    String toString() {
        value
    }

	Integer protocolId(NodeManager nodeManager) {
		stageNode(nodeManager)?.protocolId
	}

	Integer stepId(NodeManager nodeManager) {
		stageNode(nodeManager)?.stepId
	}

	StageNode stageNode(NodeManager nodeManager) {
		nodeManager.getStageNode(workflow.id,id)
	}

	StepConfigurationNode stepConfigurationNode(NodeManager nodeManager){
		return nodeManager.getStepConfigurationNode(protocolId(nodeManager), stepId(nodeManager))
	}

}

