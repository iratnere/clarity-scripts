package gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.rules

import gov.doe.jgi.pi.pps.clarity.cv.SequencerModelTypeCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryPool
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.beans.AttributeValues
import gov.doe.jgi.pi.pps.clarity.scripts.library_quantification.util_rules.*
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerClass

/**
 * Created by datjandra on 7/6/2015.
 */
class LibraryQuantificationRoutingRules extends SimpleRulesAgent {

    private final static Set<Rule> ROUTING_RULES = initRoutingRules()

    LibraryQuantificationRoutingRules() {
        super(ROUTING_RULES)
    }

    private static Set<Rule> initRoutingRules() {
        final FindSequencingStageAction findSequencingStageAction = new FindSequencingStageAction()
        final PoolCreationStageAction findPoolingStageAction = new PoolCreationStageAction()
        final AbandonStageAction abandonStageAction = new AbandonStageAction()
        final LibraryQpcrStageAction libraryQpcrStageAction = new LibraryQpcrStageAction()
        final InvalidStageAction invalidStageAction = new InvalidStageAction()
        final AbandonAndRequeueStageAction abandonAndRequeueStageAction = new AbandonAndRequeueStageAction()
        final AbandonLibrariesOnlyAction abandonLibrariesOnlyAction = new AbandonLibrariesOnlyAction()
        Set<Rule> routingRules = new HashSet<Rule>()

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new EqualCondition(AttributeKeys.DOP, 184),
                        new EqualCondition(AttributeKeys.ITAG, Boolean.TRUE)
                ]), findPoolingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new GreaterThanCondition(AttributeKeys.DOP, 12),
                        new EqualCondition(AttributeKeys.EXOME, Boolean.TRUE)
                ]), findPoolingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new GreaterThanCondition(AttributeKeys.DOP, 0),
                        new EqualCondition(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.TRUE),
                        new EqualCondition(AttributeKeys.SEQUENCER_MODEL, SequencerModelTypeCv.NOVASEQ.value)
                ]), findPoolingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_CLASS, ClarityLibraryPool.class),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new GreaterThanCondition(AttributeKeys.DOP, 0),
                        new EqualCondition(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.TRUE),
                        new NegationCondition(new EqualCondition(AttributeKeys.SEQUENCER_MODEL, SequencerModelTypeCv.NOVASEQ.value))
                ]), findSequencingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new GreaterThanCondition(AttributeKeys.DOP, 1),
                        new EqualCondition(AttributeKeys.EXOME, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.ITAG, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
                ]), findPoolingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new EqualCondition(AttributeKeys.DOP, 92),
                        new EqualCondition(AttributeKeys.ITAG, Boolean.TRUE)
                ]), findSequencingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new LessThanCondition(AttributeKeys.DOP, 13),
                        new EqualCondition(AttributeKeys.EXOME, Boolean.TRUE)
                ]), findSequencingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new EqualCondition(AttributeKeys.DOP, 1),
                        new EqualCondition(AttributeKeys.EXOME, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.ITAG, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.SINGLE_CELL_INTERNAL, Boolean.FALSE)
                ]), findSequencingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 2),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 1),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonLibrariesOnlyAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new GreaterThanCondition(AttributeKeys.LC_ATTEMPT, 2),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonAndRequeueStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.TRUE)
                ]), abandonLibrariesOnlyAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.TUBE.value),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE)
                ]), libraryQpcrStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.PASS),
                        new GreaterThanCondition(AttributeKeys.DOP, 0)
                ]), findPoolingStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.TRUE),
                ]), abandonLibrariesOnlyAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 1)
                ]), abandonLibrariesOnlyAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 2)
                ]), abandonStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.FAIL),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE),
                        new GreaterThanCondition(AttributeKeys.LC_ATTEMPT, 2)
                ]), abandonAndRequeueStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.DONE),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE)
                ]), invalidStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK),
                        new EqualCondition(AttributeKeys.ANALYTE_STATUS, AttributeValues.REQUEUE),
                        new AnyCondition(AttributeKeys.ANALYTE_STATUS_LIST, AttributeValues.REQUEUE)
                ]), libraryQpcrStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK),
                        new NegationCondition(
                            new AnyCondition(AttributeKeys.ANALYTE_STATUS_LIST, AttributeValues.REQUEUE)
                        ),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 2),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK),
                        new NegationCondition(
                                new AnyCondition(AttributeKeys.ANALYTE_STATUS_LIST, AttributeValues.REQUEUE)
                        ),
                        new EqualCondition(AttributeKeys.LC_ATTEMPT, 1),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonLibrariesOnlyAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK),
                        new NegationCondition(
                            new AnyCondition(AttributeKeys.ANALYTE_STATUS_LIST, AttributeValues.REQUEUE)
                        ),
                        new GreaterThanCondition(AttributeKeys.LC_ATTEMPT, 2),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.FALSE)
                ]), abandonAndRequeueStageAction
        ))

        routingRules.add(new Rule(
                new ConjunctionCondition([
                        new EqualCondition(AttributeKeys.CONTAINER_CLASS, ContainerClass.PLATE.value),
                        new EqualCondition(AttributeKeys.PLATE_STATUS, AttributeValues.REWORK),
                        new NegationCondition(
                                new AnyCondition(AttributeKeys.ANALYTE_STATUS_LIST, AttributeValues.REQUEUE)
                        ),
                        new EqualCondition(AttributeKeys.EXTERNAL, Boolean.TRUE)
                ]), abandonLibrariesOnlyAction
        ))

        return Collections.unmodifiableSet(routingRules)
    }
}
