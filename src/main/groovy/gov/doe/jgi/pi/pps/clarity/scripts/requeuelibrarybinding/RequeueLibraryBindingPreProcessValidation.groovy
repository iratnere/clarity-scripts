package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibrarybinding

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.apache.http.HttpStatus
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryBindingPreProcessValidation extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryBindingPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        process.inputAnalytes.each { Analyte analyte ->
            if (!(analyte instanceof PacBioAnnealingComplex)) {
                postFatalError("$analyte must be an annealing complex", HttpStatus.SC_BAD_REQUEST)
            }
        }
    }
}
