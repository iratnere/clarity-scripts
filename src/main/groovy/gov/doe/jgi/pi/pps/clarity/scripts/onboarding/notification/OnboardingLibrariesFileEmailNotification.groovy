package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails


/**
 * Created by lvishwas on 10/22/16.
 */
class OnboardingLibrariesFileEmailNotification extends OnboardingSequencingFileEmailNotification {
    static final String CC_EMAIL_GROUP = 'jgi-its-onboard@lbl.gov' //PPS-4849 - update mailing lists

    @Override
    protected def getToList(List<EmailDetails> emailDetailsList) {
        //TODO: update to list to valid email ids
        return CC_EMAIL_GROUP
    }

    @Override
    protected def getCcList(List<EmailDetails> emailDetailsList) {
        return CC_EMAIL_GROUP
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        return [contact:'Chris', date: new Date(), link: additionalInfo]
    }
}
