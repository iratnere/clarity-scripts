package gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.apache.commons.lang.RandomStringUtils

//import grails.converters.JSON

/**
 * Created by tlpaley on 5/3/17.
 */
@JsonSerialize(using = SequencingProjectSubmissionSerializer.class)
class SequencingProjectSubmission {

    SequencingProjectSubmission() {
        //registerSubmission()
    }
    Long strategyId
    Long sequencingProductId
    def submittedBy

//    static def registerSubmission() {
//        JSON.registerObjectMarshaller (SequencingProjectSubmission) { SequencingProjectSubmission submission ->
//                def output = [:]
//            output['existing-jgi-taxonomy-id'] = 1492
//            output['sequencing-project-name'] = "SP ${RandomStringUtils.randomAlphanumeric(4)}${new Date().time}"
//            output['expected-number-of-samples'] = 1
//
//            output['final-deliverable-project-id'] = 1091346
//            output['sequencing-product-id'] = submission.sequencingProductId
//            output['sequencing-strategy-id'] = submission.strategyId
//
//            output['sequencing-project-manager-cid'] = submission.submittedBy
//            output['sequencing-project-comments-and-notes'] = "sequencing-project-comments-and-notes"
//            output['eligible-for-public-release'] = "Y"
//            output['embargo-days'] = 180
//            output['sequencing-project-contact-cid'] = submission.submittedBy
//            output['sample-contact-cid'] = submission.submittedBy
//            output['auto-schedule-sow-items'] = "Y"
//
//            return output
//        }
//    }
    class SequencingProjectSubmissionSerializer extends StdSerializer< SequencingProjectSubmission> {
        protected SequencingProjectSubmissionSerializer() {
            super(null)
        }
        protected SequencingProjectSubmissionSerializer(Class<SequencingProjectSubmission> t) {
            super(t)
        }

        @Override
        void serialize(SequencingProjectSubmission submission, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartObject();
            //jgen.writeStringField("sample-id", "${submission.sampleId}" )
            jgen.writeNumberField('existing-jgi-taxonomy-id', 1492)
            jgen.writeStringField('sequencing-project-name', "SP ${RandomStringUtils.randomAlphanumeric(4)}${new Date().time}")
            jgen.writeNumberField('expected-number-of-samples', 1)

            jgen.writeNumberField('final-deliverable-project-id', 1091346)
            jgen.writeNumberField('sequencing-product-id', submission.sequencingProductId)
            jgen.writeNumberField('sequencing-strategy-id', submission.strategyId)

            jgen.writeStringField('sequencing-project-manager-cid', "${submission.submittedBy}")
            jgen.writeStringField('sequencing-project-comments-and-notes', "sequencing-project-comments-and-notes")
            jgen.writeStringField('eligible-for-public-release', "Y")
            jgen.writeNumberField('embargo-days', 180)
            jgen.writeStringField('sequencing-project-contact-cid', "${submission.submittedBy}")
            jgen.writeStringField('sample-contact-cid', "${submission.submittedBy}")
            jgen.writeStringField('auto-schedule-sow-items', "Y")
            jgen.writeEndObject();
        }
    }
}
