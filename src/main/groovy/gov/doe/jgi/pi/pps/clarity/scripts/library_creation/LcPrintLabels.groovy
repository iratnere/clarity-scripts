package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 12/5/14.
 */
class LcPrintLabels extends ActionHandler {
    
    void execute() {
        LibraryCreationProcess clarityProcess = process as LibraryCreationProcess
        clarityProcess.lcAdapter = clarityProcess.initializeLcAdapter()
        clarityProcess.lcAdapter.createPrintLabelsFile()
    }
}