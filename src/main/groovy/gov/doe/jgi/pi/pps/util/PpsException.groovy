package gov.doe.jgi.pi.pps.util

import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by iratnere on 11/7/14.
 */
class PpsException extends RuntimeException {
    final static Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class)

    PpsException(String message) {
        this([new PpsError("", message)])
    }


    def errors = []

    String toJsonStr() {
        JSONObject.fromObject([errors: errors]).toString(3)
    }

    PpsException(Throwable ex) {
        this(ex.message)
    }

    PpsException(def errors) {
        super(JSONObject.fromObject([errors: errors]).toString(3))
        this.errors = errors
        log.error(message, this)
    }

}
