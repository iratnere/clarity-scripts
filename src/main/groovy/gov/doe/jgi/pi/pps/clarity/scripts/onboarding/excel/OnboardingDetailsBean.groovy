package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping

/**
 * Created by lvishwas on 3/16/17.
 */
class OnboardingDetailsBean {
    @FieldMapping(header = 'Process Lims Id', cellType = CellTypeEnum.STRING)
    public def processLimsId
    @FieldMapping(header = 'Process Type', cellType = CellTypeEnum.STRING)
    public def processType
    @FieldMapping(header = 'Analyte Lims Id', cellType = CellTypeEnum.STRING)
    public def analyteLimsId

}
