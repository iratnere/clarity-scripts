package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.excel.HudsonAlphaBean
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.HudsonAlphaIlluminaOnboardingAdapter
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 * Created by lvishwas on 10/20/16.
 */
class OnboardHudsonAlphaSequencingFiles extends ClarityProcess {

    static ProcessType processType = ProcessType.ONBOARD_SEQUENCING_FILES

    boolean testMode = false
    static final String USER_SEQUENCING_WORSHEET = 'Upload Onboard Sequencing File'
    static final String SCRIPT_SEQUENCING_WORSHEET = 'Download Onboard Sequencing File'
    static final int HUDSON_ALPHA_SHEET_INDEX = 0


    OnboardHudsonAlphaSequencingFiles(ProcessNode processNode) {
        super(processNode)
        actionHandlers = ['ProcessSequencingFile': ProcessHudsonAlphaSequencingFile,
                          'FinishStep': FinishStep
        ]
    }

    OnboardingAdapter getOnboardingAdapterInstance(ExcelWorkbook workbook) {
        Section section = workbook.sections.values().find {it.parentSheetIndex == HUDSON_ALPHA_SHEET_INDEX}
        if(section.beanClass == HudsonAlphaBean.class)
            return new HudsonAlphaIlluminaOnboardingAdapter(workbook, researcherContactId)
        throw new RuntimeException("Incorrect file format.")
    }

    ExcelWorkbook getOnboardingWorkbook(String fileName) {
        def srcFileNode = processNode.outputResultFiles?.find { it.name.contains(fileName)}
        if(srcFileNode?.fileNode?.contentUri?.endsWith('xlsx'))
            throw new IllegalFormatException("Only Excel 97-2004 Workbook (.xls) format supported")
        ExcelWorkbook onboardLibraryWorkbook = new ExcelWorkbook(srcFileNode?.id, testMode)
        onboardLibraryWorkbook.addSection(onboardingBeanTableSection)
        onboardLibraryWorkbook.load()
        return onboardLibraryWorkbook
    }

    TableSection getOnboardingBeanTableSection() {
        return new TableSection(HUDSON_ALPHA_SHEET_INDEX, HudsonAlphaBean.class,'A2')
    }
}
