package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.*
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel.GLSStartletUseTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.sample_qc.excel.SMWorkspaceSampleQCPassFailKeyValueBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.util.CellReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SampleQcWorksheet {
    static final Logger logger = LoggerFactory.getLogger(SampleQcWorksheet.class)
    SampleQcProcess qcProcess
    List<Analyte> sortedInputs
    Map<ContainerNode, Map<ContainerNode, List<Analyte>>> outputPlates = [:].withDefault{[:].withDefault {[]}}
    Map<String, FreezerContainer> freezerContainerMap = [:]
    ExcelWorkbook sampleQcWorkbook
    int inputIndex = 0

    SampleQcWorksheet(SampleQcProcess process){
        qcProcess = process
    }

    void init(){
        sortedInputs = qcProcess.sortedInputAnalytes
        sortedInputs.each { Analyte analyte ->
            Analyte outputAnalyte = qcProcess.getOutputAnalytes(analyte)[0]
            if(!outputAnalyte.containerNode)
                outputAnalyte.artifactNode.httpRefresh();''
            outputPlates[outputAnalyte.containerNode][analyte.containerNode] << analyte
        }
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        List<FreezerContainer> freezerContainers = freezerService.freezerCheckoutInputAnalytes(qcProcess)
        freezerContainers?.each{ FreezerContainer container ->
            freezerContainerMap[container.barcode] = container
        }
    }

    ExcelWorkbook makeSampleQcWorksheet(){
        init()
        sampleQcWorkbook = new ExcelWorkbook(qcProcess.SAMPLE_QC_WORKSHEET_TEMPLATE, qcProcess.testMode)
        createSheets()
        createAndPopulateSections()
        return sampleQcWorkbook
    }

    void createSheets(){
        List<ContainerNode> containers = outputPlates.keySet().sort{a,b -> a.id<=>b.id} as List
        int i = 0
        sampleQcWorkbook.workbook.setSheetOrder(GlsStarletUse.GLS_STARLET_SHEET_NAME, i)
        SMWorkspaceModel model = new SMWorkspaceModel()
        containers.each { ContainerNode container ->
            Sheet sheet = sampleQcWorkbook.workbook.cloneSheet(model.sheetIndex)
            sampleQcWorkbook.workbook.setSheetName(sampleQcWorkbook.workbook.getSheetIndex(sheet), container.id)
            sampleQcWorkbook.workbook.setSheetOrder(container.id, ++i)
        }
    }

    void createAndPopulateSections(){
        TableSection tableSection = createAndPopulateTableSection()
        sampleQcWorkbook.addSection(tableSection)
        getSMWorkspaceSheets(tableSection)
    }

    Section createAndPopulateTableSection(){
        return new GlsStarletUse().tableSection
    }

    ExcelWorkbook readSampleQcWorksheet(){
        loadSampleQcWorksheet()
        Set<String> outputPlateIds = [] as Set
        Set<String> inputPlateIds = [] as Set
        qcProcess.outputAnalytes.each { Analyte output ->
            outputPlateIds << output.containerId
            if(output.parentAnalyte.containerNode.containerTypeEnum == ContainerTypes.WELL_PLATE_96)
                inputPlateIds << output.parentAnalyte.containerId
        }
        int totalSectionsCount = 1 + outputPlateIds.size() * 3 + inputPlateIds.size() * 2
        if(sampleQcWorkbook.sections.size() < totalSectionsCount)
            throw new IllegalFormatException("The attached Sample QC WOrksheet template does not match the current template. Please Abort the process and restart!")
        List<Section> neededSections = []
        Map<Integer, List<Section>> sections = sampleQcWorkbook.sections.values().groupBy {it.parentSheetIndex}
        sections.each { int sheetIndex, List<Section> sheetSections ->
            List sens = sheetSections.findAll{!(it.sectionName in ['Initial Volume (ul)', 'Concentration (ng/ul)', 'Available Mass (ng)'])}
            if(sens.size() == 1 && sens[0] instanceof TableSection){
                sens[0].data.each{ GLSStartletUseTableBean bean ->
                    String sampleLimsId = bean.sampleLimsId
                    bean.sample = qcProcess.inputAnalytes.find{it.claritySample.id == sampleLimsId}?.claritySample
                }
                neededSections << sens[0]
            }
            List<KeyValueSection> kvs = sens.findAll{it instanceof KeyValueSection}
            kvs?.each { KeyValueSection kv ->
                SMWorkspaceSampleQCPassFailKeyValueBean bean = (SMWorkspaceSampleQCPassFailKeyValueBean) kv.data
                bean.firstSampleOnPlate = qcProcess.inputAnalytes.find { it.containerName == bean.plateBarcode }
                neededSections << kv
            }
        }
        String errors = validateSections(neededSections)
        if(errors){
            qcProcess.postErrorMessage("Sample QC Worksheet validation failed. ${errors}")
        }
        sampleQcWorkbook.sections.clear()
        sampleQcWorkbook.addSections(neededSections)
        return sampleQcWorkbook
    }

    /**
     * check that all sow items associated with the input samples that have not gone through sow item qc (do not have already have the “Sow Item QC Result” udf set) can be satisfied (uss.dt_sow_item.qc_type_id - sample “Cumulative QC Type Id” <=0); if not OPI-337
     * all samples have QC Status filled out
     *
     * @param sections
     * @return
     */
    String validateSections(List<Section> sections){
        String loadErrors = ''
        Map<String, Set<String>> containerSmInstructions = [:].withDefault {[] as Set}
        Map<String, Set<String>> containerSmNotes = [:].withDefault {[] as Set}

        sections?.each{ Section section ->
            if(section instanceof TableSection){
                section.data?.each{ GLSStartletUseTableBean bean ->
                    loadErrors += bean.validateBean(qcProcess.getSampleQcTypeIds(bean.sample.pmoSampleId), bean.sample.getScheduledSamples(qcProcess.nodeManager))
                    if(bean.smInstructions)
                        containerSmInstructions[bean.sampleBarcode] << bean.smInstructions
                    if(bean.smNotes)
                        containerSmNotes[bean.sampleBarcode] << bean.smNotes
                }
                containerSmInstructions.each{String barcode, Set<String> instructions ->
                    if(instructions.size() > 1){
                        loadErrors += "All Samples on plate ${barcode} do not have the same SM Instructions.\n"
                    }
                }
                containerSmNotes.each{String barcode, Set<String> notes ->
                    if(notes.size() > 1){
                        loadErrors += "All Samples on plate ${barcode} do not have the same SM Notes.\n"
                    }
                }
            }
            if(section instanceof KeyValueSection){
                loadErrors += section.data?.validateBean()
            }
        }
        return loadErrors
    }

    ExcelWorkbook loadSampleQcWorksheet(){
        def fileNode = qcProcess.processNode.outputResultFiles?.find { it.name.contains(qcProcess.DOWNLOAD_SAMPLE_QC_WORSHEET)}
        sampleQcWorkbook = new ExcelWorkbook(fileNode.id, qcProcess.testMode)
        sampleQcWorkbook.load()
        return sampleQcWorkbook
    }

    void getSMWorkspaceSheets(TableSection tableSection){
        outputPlates.each { ContainerNode outputPlate, Map containerAnalytes ->
            int sheetIndex = sampleQcWorkbook.getSheetIndex(outputPlate.id)
            SMWorkspaceModel model = new SMWorkspaceModel(sheetIndex)
            def opSections = populateOutputPlateSections(model, outputPlate, tableSection)
            sampleQcWorkbook.addSections(opSections)
            int j = 0
            List<Section> dynamicSections = model.getDynamicSections(outputPlate)
            if(dynamicSections) {
                containerAnalytes.each { ContainerNode inputPlate, List<Analyte> inputAnalytes ->
                    if(inputPlate.containerTypeEnum == ContainerTypes.WELL_PLATE_96) {
                        KeyValueSection kvs = dynamicSections[j++]
                        PlateSection ps = dynamicSections[j++]
                        sampleQcWorkbook.addSection(kvs)
                        sampleQcWorkbook.addSection(ps)
                        populateDynamicKeyValueSection(kvs, ps, inputPlate)
                        populateDynamicPlateSection(ps, inputAnalytes, GlsStarletUse.GLS_STARLET_SHEET_NAME, tableSection)
                    }
                }
            }
        }
    }

    void populateDynamicPlateSection(Section section, List<Analyte> inputAnalytes, String firstSheetName, Section tableSection){
        Map<String, String> locationFormulaMap = [:]
        inputAnalytes?.each{ Analyte analyte ->
            String qcStatusCellRef = getCellAddressWithOffset(tableSection,'qcStatus',sortedInputs.indexOf(analyte)+1,0)
            String sheetCellRef = "'${firstSheetName}'!${qcStatusCellRef}"
            String formula = "IF(${sheetCellRef} = \"\",\"\",${sheetCellRef})"
            locationFormulaMap[analyte?.containerLocation?.wellLocation] = formula
        }
        section?.setData(locationFormulaMap)
    }

    void populateDynamicKeyValueSection(KeyValueSection kvSection, PlateSection plateSection, ContainerNode inputPlate){
        kvSection.data = new SMWorkspaceSampleQCPassFailKeyValueBean()
        int numberOfSamples = inputPlate.contentsIds.size()
        kvSection.data.populateBean(inputPlate)
        if(plateSection)
            kvSection.data.passesSamplesCount = "COUNTIF(${plateSection.startCell.formatAsString()}:${plateSection.lastCell.formatAsString()},\"Pass\")"
        String passedSampleCountRef = getCellAddressWithOffset(kvSection,'passesSamplesCount', 0, 1)
        String passPercentageRef = getCellAddressWithOffset(kvSection,'plateQCPassPercentage',0,1)
        kvSection.data.plateQCResult = "IF((${passedSampleCountRef}*100/${numberOfSamples}) >= ${passPercentageRef},\"Pass\",\"Fail\")"
    }

    List<PlateSection> populateOutputPlateSections(SMWorkspaceModel model, ContainerNode outputPlate, TableSection tableSection){
        List<String> fieldNames = ['initialVolume', 'concentration', 'availableMass']
        List<PlateSection> outputPlateSections = [model.initVolModelSection, model.concentrationModelSection, model.availableMassModelSection]
        int offset = 0
        while(offset < outputPlate.contentsIds.size() && offset+inputIndex < sortedInputs.size()) {
            fieldNames.eachWithIndex{String fieldName, int i ->
                addFormulaToPlateSection(tableSection, outputPlateSections[i], fieldName, offset+inputIndex)
            }
            offset++
        }
        inputIndex += offset
        return outputPlateSections
    }

    void addFormulaToPlateSection(TableSection tableSection, PlateSection targetSection, String fieldName, int index){
        Analyte inputAnalyte = sortedInputs[index]
        String plateLocation = qcProcess.getOutputAnalytes(inputAnalyte)[0].containerLocation.wellLocation
        String address = getCellAddressWithOffset(tableSection, fieldName, index+1, 0)
        targetSection.data[plateLocation] = "'${GlsStarletUse.GLS_STARLET_SHEET_NAME}'!${address}"
    }

    class GlsStarletUse {
        static final String GLS_STARLET_SHEET_NAME = "GLS & Starlet Use"
        static final int GLS_AND_STARLET_USE_SHEET = 0
        static final String GLS_STARLET_TABLE_START_CELL = "A4"

        Section getTableSection(){
            Section section = new TableSection(GLS_AND_STARLET_USE_SHEET, GLSStartletUseTableBean.class, GLS_STARLET_TABLE_START_CELL, null, true)
            section.data = glsStarletTableBeans
            return section
        }

        List<GLSStartletUseTableBean> getGlsStarletTableBeans() {
            List<Analyte> analytes = sortedInputs
            List<GLSStartletUseTableBean> beanList = []
            analytes.eachWithIndex{ Analyte analyte, int index ->
                PmoSample sample = analyte.claritySample
                GLSStartletUseTableBean bean = new GLSStartletUseTableBean()
                bean.populateBean(index+1, analyte, qcProcess.getSampleQcTypeIds(sample.pmoSampleId)?.values() as List,freezerContainerMap)
                //Concentration (ng/ul) <-- sample udf Concentration (ng/ul)
                //OPI-363 - PPS-3142
                bean.concentration = qcProcess.getIsITag(sample.pmoSampleId)? 2.6 : (sample?.udfConcentration?:0)
                beanList << bean
            }
            return beanList
        }
    }

    class SMWorkspaceModel{
        final static String SM_WORKSPACE_MODEL_SHEET_NAME = "SM Workspace Model"
        final static String PLATE_QC_MODEL_SHEET_NAME = "Plate QC Model"

        final static String INIT_VOL_PLATE_SECTION_START_CELL = 'A3'
        final static String INIT_VOL_PLATE_SECTION_END_CELL = 'M11'

        final static String CONCENTRATION_PLATE_SECTION_START_CELL = 'A14'
        final static String CONCENTRATION_PLATE_SECTION_END_CELL = 'M22'

        final static String MASS_PLATE_SECTION_START_CELL = 'A25'
        final static String MASS_PLATE_SECTION_END_CELL = 'M33'

        final static int DYNAMIC_KEY_VALUE_SECTION_START_CELL_ROW_OFFSET = 6
        final static int DYNAMIC_PLATE_SECTION_ROW_OFFSET = 8

        Integer sheetIndex = null

        SMWorkspaceModel(int sheetNo = -1){
            if(sheetNo < 0)
                sheetIndex = SMWorkspaceModelSheetIndex
            else
                sheetIndex = sheetNo
        }

        int getSMWorkspaceModelSheetIndex(){
            sampleQcWorkbook.getSheetIndex(SM_WORKSPACE_MODEL_SHEET_NAME)
        }

        int getPlateQCModelSheetIndex(){
            sampleQcWorkbook.getSheetIndex(PLATE_QC_MODEL_SHEET_NAME)
        }

        Section getInitVolModelSection(){
            Section section = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, INIT_VOL_PLATE_SECTION_START_CELL, INIT_VOL_PLATE_SECTION_END_CELL)
            section.data = [:]
            return section
        }

        Section getConcentrationModelSection(){
            Section section = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, CONCENTRATION_PLATE_SECTION_START_CELL, CONCENTRATION_PLATE_SECTION_END_CELL)
            section.data = [:]
            return section
        }

        Section getAvailableMassModelSection() {
            Section section = new PlateSection(sheetIndex, CellTypeEnum.FORMULA, MASS_PLATE_SECTION_START_CELL, MASS_PLATE_SECTION_END_CELL)
            section.data = [:]
            return section
        }

        Section getSourceKeyValueSection(){
            KeyValueSection kvs = new KeyValueSection(plateQCModelSheetIndex, SMWorkspaceSampleQCPassFailKeyValueBean.class, 'A2', 'B8')
            kvs.workbook = sampleQcWorkbook
            return kvs
        }

        Section getSourcePlateSection(){
            PlateSection ps = new PlateSection(plateQCModelSheetIndex, CellTypeEnum.FORMULA, 'A10')
            ps.workbook = sampleQcWorkbook
            return ps
        }

        Section getDynamicKeyValueSection(PlateSection previousSection){
            int row = previousSection.lastCell.row
            CellReference startCellReference = new CellReference(row+DYNAMIC_KEY_VALUE_SECTION_START_CELL_ROW_OFFSET, 0)
            return new KeyValueSection(sourceKeyValueSection, sheetIndex, SMWorkspaceSampleQCPassFailKeyValueBean.class, startCellReference.formatAsString())
        }

        Section getDynamicPlateSection(KeyValueSection previousSection){
            int row = previousSection.startCell.row
            CellReference startCellReference = new CellReference(row+DYNAMIC_PLATE_SECTION_ROW_OFFSET, 0)
            Section section = new PlateSection(sourcePlateSection, sheetIndex, CellTypeEnum.FORMULA, startCellReference.formatAsString())
            section.data = [:]
            return section
        }

        List<Section> getDynamicSections(ContainerNode outputPlate){
            int dynamicSectionsCount = outputPlates[outputPlate].findAll{it.key.containerTypeEnum == ContainerTypes.WELL_PLATE_96}.size()
            List<Section> dynamicSections = []
            Section lastSection = availableMassModelSection
            for(int i=0; i < dynamicSectionsCount; i++) {
                lastSection = getDynamicKeyValueSection(lastSection)
                dynamicSections << lastSection
                lastSection = getDynamicPlateSection(lastSection)
                dynamicSections << lastSection
            }
            return dynamicSections
        }
    }

    static String getCellAddressWithOffset(Section section, String fieldName, int rowOffset=0, int colOffset=0){
        CellReference ref = section.getCellReference(fieldName)
        return ref?(new CellReference(ref.row+rowOffset, ref.col+colOffset)).formatAsString():''
    }
}