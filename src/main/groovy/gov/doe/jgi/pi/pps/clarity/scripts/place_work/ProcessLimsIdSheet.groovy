package gov.doe.jgi.pi.pps.clarity.scripts.place_work

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import org.slf4j.LoggerFactory

/**
 * Created by tlpaley on 3/25/16.
 * https://docs.google.com/document/d/10hqH-LWcX6QNp9DUpsCshserr_H5agWJMJzHRGEIsEw/edit
 */
class ProcessLimsIdSheet extends ActionHandler {
    static final logger = LoggerFactory.getLogger(ProcessLimsIdSheet.class)

    @Override
    void execute() {
        logger.info "Starting ${this.class.name} action...."
        logger.info "Worksheet analytes $process.analytes"
        process.analytes.each{
            //Only derived and scheduled samples can be put “On Hold”/”Needs Attention”.
            it.validateIsDerivedSample()
            it.validateIsInQueuedStages()
        }
        //process.setCompleteStage() //not needed -> control samples are configured as a "single step control" sample
    }
}
