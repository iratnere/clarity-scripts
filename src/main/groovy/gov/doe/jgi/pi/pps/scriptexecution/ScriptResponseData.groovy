package gov.doe.jgi.pi.pps.scriptexecution

import gov.doe.jgi.pi.pps.clarity.model.researcher.Researcher
import groovy.transform.Canonical

@Canonical
class ScriptResponseData {
    Researcher researcher
    String actionHandler
    String processClass
    Boolean lastAction
}
