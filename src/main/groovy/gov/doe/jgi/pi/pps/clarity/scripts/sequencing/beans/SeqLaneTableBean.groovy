package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import groovy.transform.ToString

/**
 * Auto-generated on Thu Apr 30 14:44:45 PDT 2015
 */
@ToString(includeNames=true, includeFields=true)
class SeqLaneTableBean{

    @FieldMapping(header="Library Stock Name", cellType= CellTypeEnum.STRING)
    public String libraryStockName

    @FieldMapping(header="Container Barcode", cellType= CellTypeEnum.STRING)
    public String containerBarcode

    @FieldMapping(header="Freezer Path", cellType= CellTypeEnum.STRING)
    public String freezerPath

    @FieldMapping(header="Run Type", cellType= CellTypeEnum.STRING)
    public String runType

    @FieldMapping(header="Material Type", cellType= CellTypeEnum.STRING)
    public String materialType

    @FieldMapping(header="PhiX Spike in %", cellType= CellTypeEnum.NUMBER)
    public def phixSpikeIn

    @FieldMapping(header="Conc (pM) from qPCR", cellType= CellTypeEnum.NUMBER)
    public BigDecimal concentrationPm

    @FieldMapping(header="Library conv. factor", cellType= CellTypeEnum.FORMULA)
    public def libraryConvFactor

    @FieldMapping(header="DNA WA01 (μL)", cellType= CellTypeEnum.FORMULA)
    public def dnaWa01Ul

    @FieldMapping(header="Lane", cellType= CellTypeEnum.STRING, isRowHeader = true)
    public String lane

    String validateBean(){
        return ""
    }

    String setLaneString(){
        lane = "${lane as BigInteger}".toString()
    }

    void updatePhysicalRunUnit(ClarityPhysicalRunUnit physicalRunUnit){
        physicalRunUnit.setUdfPhixSpikeIn(phixSpikeIn)
        physicalRunUnit.setUdfLibraryConversionFactor(libraryConvFactor)
    }
}
