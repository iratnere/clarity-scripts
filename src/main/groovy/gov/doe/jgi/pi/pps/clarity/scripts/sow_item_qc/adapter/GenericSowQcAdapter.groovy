package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter

import gov.doe.jgi.pi.pps.clarity.config.ClarityWorkflow
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.cv.MaterialCategoryCv
import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.SampleAnalyte
import gov.doe.jgi.pi.pps.clarity.model.sample.SampleFactory
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.RecordDetailsInfo
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.SowItemQc
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ContainerNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.Routing
import gov.doe.jgi.pi.pps.util.util.BeanUtil

class GenericSowQcAdapter extends SowQcAdapter {

    Map<ContainerNode,String> containerAutoScheduleSowItems = [:]

    GenericSowQcAdapter(List<Analyte> inputAnalytes) {
        super(inputAnalytes)
    }

    @Override
    String validateInputs() {
        return checkForUnQCedInputs()
    }

    @Override
    String checkProcessUdfs(SowItemQc sowQcProcess) {
        return ''
    }

    @Override
    void updateStatusAndRouting(RecordDetailsInfo recordDetailsInfo) {
        analytes.each { SampleAnalyte sampleAnalyte ->
            String sowQcResult = sampleAnalyte.claritySample.udfSowItemQcResult
            routingRequests.addAll(buildRoutingRequests(sampleAnalyte, sowQcResult, sampleAnalyte.claritySample.sequencingProject.udfMaterialCategory))
            updateStatusMap(sampleAnalyte, sowQcResult)
        }
        if(sampleStatusMap) {
            StatusService service = BeanUtil.getBean(StatusService.class)
            service.submitSowItemStatus(sampleStatusMap, recordDetailsInfo.researcherId)
        }
    }

    Stage getStage(String materialCategory) {
        if(materialCategory == MaterialCategoryCv.DNA.materialCategory)
            return Stage.ALIQUOT_CREATION_DNA
        if(materialCategory == MaterialCategoryCv.RNA.materialCategory)
            return Stage.ALIQUOT_CREATION_RNA
        return null
    }

    List<RoutingRequest> buildRoutingRequests(Analyte sampleAnalyte, String sowQcResult, String materialCategory){
        if(sowQcResult == Analyte.PASS){
            //PPS-3793 stop all sow items on plate from being scheduled if any SP on plate autoschedule=N
            boolean autoScheduleSowItems = getAutoScheduleSowItems(sampleAnalyte.containerNode)
            if(autoScheduleSowItems)
                return RoutingRequest.generateRequestsToRouteArtifactIdsToUri(getStage(materialCategory)?.uri, [sampleAnalyte.id], Routing.Action.assign)
            else{
                List<RoutingRequest> routingRequests = []
                sampleAnalyte.activeWorkflows?.each { ClarityWorkflow activeWorkflow ->
                    if(activeWorkflow != ClarityWorkflow.SOW_ITEM_QC)
                        routingRequests << new RoutingRequest(routingUri: activeWorkflow.uri, artifactId: sampleAnalyte.id, action: Routing.Action.unassign)
                }
                return routingRequests
            }
        }
        else if(sowQcResult == Analyte.FAIL){
            failedScheduledSamples << sampleAnalyte
            List<RoutingRequest> routingRequests = []
            sampleAnalyte.activeWorkflows?.each { ClarityWorkflow activeWorkflow ->
                if(activeWorkflow != ClarityWorkflow.SOW_ITEM_QC) {
                    routingRequests << new RoutingRequest(routingUri: activeWorkflow.uri, artifactId: sampleAnalyte.id, action: Routing.Action.unassign)
                }
            }
            return routingRequests
        }
        return []
    }

    Map updateStatusMap(Analyte sampleAnalyte, String sowQcResult){
        ScheduledSample scheduledSample = sampleAnalyte.claritySample
        if(sowQcResult == Analyte.PASS){
            boolean autoScheduleSowItems = getAutoScheduleSowItems(sampleAnalyte.containerNode)
            if(!autoScheduleSowItems) {
                sampleStatusMap[scheduledSample.sowItemId] = SowItemStatusCv.AWAITING_SAMPLE_QC_REVIEW
            }
        }
        else if(sowQcResult == Analyte.FAIL){
            sampleStatusMap[scheduledSample.sowItemId] = SowItemStatusCv.NEEDS_ATTENTION
        }
        sampleStatusMap
    }

    boolean getAutoScheduleSowItems(ContainerNode containerNode){
        def autoScheduleSowItems = containerAutoScheduleSowItems[containerNode]
        if(autoScheduleSowItems != null)
            return autoScheduleSowItems.toBoolean()
        boolean result = true
        containerNode.contentsArtifactNodes.each{
            result = SampleFactory.sampleInstance(it.sampleNode).sequencingProject.udfAutoScheduleSowItems.toBoolean() && result
        }
        containerAutoScheduleSowItems[containerNode] = result.toString()
        return result
    }
}
