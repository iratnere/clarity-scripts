package gov.doe.jgi.pi.pps.clarity.scripts.requeuelibraryannealing

import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.domain.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.util.NodeManagerUtility
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioAnnealingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 10/9/2015.
 */
class RequeueLibraryAnnealingRouteToWorkflow extends ActionHandler {
    static final logger = LoggerFactory.getLogger(RequeueLibraryAnnealingRouteToWorkflow.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        assignToNextWorkflow()
    }

    void assignToNextWorkflow(){
        List<Analyte> pacBioSequel = []
        NodeManagerUtility nmu = new NodeManagerUtility(process.nodeManager)
        process.inputAnalytes.each { Analyte analyte ->
            //analyte instanceof ClarityLibraryStock or ClarityLibraryPool, sequencerModel.isPacBio
            //please see RequeueLibraryAnnealingPreProcessValidation
            SequencerModelCv sequencerModel = analyte.sequencerModelCv
            if (sequencerModel.isSequel) {
                pacBioSequel << analyte
            } else {
                throw new WebException("no handler for sequencer model [${sequencerModel.sequencerModel}] for ${analyte} with run mode ${analyte.udfRunMode}",422)
            }
            nmu.getAnnealingComplexes(analyte)?.each { PacBioAnnealingComplex annealingComplex ->
                process.removeAnalyteFromActiveWorkflows(annealingComplex)
                logger.info("Removed $annealingComplex from all workflows")
            }
            nmu.getBindingComplexes(analyte)?.each { PacBioBindingComplex bindingComplex ->
                process.removeAnalyteFromActiveWorkflows(bindingComplex)
                logger.info "Removed $bindingComplex from all workflows"
            }
        }
        process.routeAnalytes(Stage.PACBIO_SEQUEL_LIBRARY_ANNEALING,pacBioSequel)
    }
}
