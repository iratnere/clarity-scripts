package gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.DropDownList
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.LibraryCreationProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class TubeBean {
	@FieldMapping(header='#', cellType = CellTypeEnum.NUMBER)
	public BigDecimal counter
	@FieldMapping(header = 'Analyte Name', cellType = CellTypeEnum.STRING)
	public String aliquotName
	@FieldMapping(header = 'Label', cellType = CellTypeEnum.STRING)
	public String aliquotContainerName
	@FieldMapping(header = 'Freezer Path', cellType = CellTypeEnum.STRING)
	public String aliquotFreezerPath
	@FieldMapping(header='Concentration (ng/uL)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal aliquotConcentration
	@FieldMapping(header='Available Volume (ul)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal aliquotVolume
	@FieldMapping(header='Available Mass (ng)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal aliquotMass
	@FieldMapping(header = 'Library', cellType = CellTypeEnum.STRING)
	public String libraryName
    @FieldMapping(header = 'Library LIMS ID', cellType = CellTypeEnum.STRING)
    public String libraryLimsId
    @FieldMapping(header = 'Queue', cellType = CellTypeEnum.STRING)
	public String libraryCreationQueue
	@FieldMapping(header = 'LC Instructions', cellType = CellTypeEnum.STRING)
	public String lcInstructions
	@FieldMapping(header='Degree of pooling', cellType = CellTypeEnum.NUMBER)
	public BigDecimal dop
	@FieldMapping(header='Library QC Result', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryQcResult
	@FieldMapping(header='Library QC Failure Mode', cellType = CellTypeEnum.DROPDOWN)
	public DropDownList libraryQcFailureMode
	@FieldMapping(header='Library Volume (uL)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryVolume
	@FieldMapping(header='Library Concentration (ng/uL)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryConcentration
	@FieldMapping(header='Library Actual Template Size (bp)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryActualTemplateSize
	@FieldMapping(header='Library Molarity QC (pm)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryMolarityQc
	@FieldMapping(header='Library Actual Insert Size (kb)', cellType = CellTypeEnum.NUMBER)
	public BigDecimal libraryActualInsertSize
	@FieldMapping(header='Number of PCR Cycles', cellType = CellTypeEnum.NUMBER)
	public BigDecimal numberPcrCycles
	@FieldMapping(header = 'SM Notes', cellType = CellTypeEnum.STRING)
	public String smNotes
	@FieldMapping(header = 'Index Name', cellType = CellTypeEnum.STRING)
	public String libraryIndexName
	@FieldMapping(header = 'LC Notes', cellType = CellTypeEnum.STRING)
	public String lcNotes

    Boolean passed
    static final String LMP_QUEUE = 'LMP'

	def validate() {
		def errorMsg = StringBuilder.newInstance()
		String labResult = libraryQcResult?.value
		if (!labResult || !(labResult in Analyte.PASS_FAIL_LIST)) {
			errorMsg << "invalid Library QC Result '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
			return buildErrorMessage(errorMsg)
		}
		passed = labResult == Analyte.PASS
		if (!passed && !libraryQcFailureMode?.value) {
			errorMsg << "unspecified Library QC Failure Mode $ClarityProcess.WINDOWS_NEWLINE"
            return buildErrorMessage(errorMsg)
		}
        if (!passed)
            return null
        //validate passed libraries
		if (libraryQcFailureMode?.value) {
			errorMsg << "incorrect failure mode '$libraryQcFailureMode.value' or status '$labResult' $ClarityProcess.WINDOWS_NEWLINE"
		}
        if (!libraryVolume) {
            errorMsg << "unspecified Library Volume (uL) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!libraryConcentration) {
            errorMsg << "unspecified Library Concentration (ng/uL) $ClarityProcess.WINDOWS_NEWLINE"
        }
        if (!libraryActualTemplateSize) {
            errorMsg << "unspecified Library Actual Template Size (bp) $ClarityProcess.WINDOWS_NEWLINE"
        }
		errorMsg << validateTubeBean()
		return buildErrorMessage(errorMsg)
	}

	String validateTubeBean(){
        return ''
    }

    def buildErrorMessage(def errorMsg) {
        def startErrorMsg = LibraryCreationProcess.START_ERROR_MSG << ClarityProcess.WINDOWS_NEWLINE << "Library Stock ($libraryName):$ClarityProcess.WINDOWS_NEWLINE"
        if (errorMsg?.length()) {
            return startErrorMsg << errorMsg
        }
        return null
    }

	void populateRequiredFields(ClarityProcess process, int passCount){
		LibraryStockFailureModesService libraryStockFailureModesService = BeanUtil.getBean(LibraryStockFailureModesService.class)
		libraryQcResult = libraryStockFailureModesService.dropDownPassFail
		libraryQcFailureMode = libraryStockFailureModesService.dropDownFailureModes
		if(passCount > 0)
			libraryQcResult.value = Analyte.PASS
		else{
			libraryQcResult.value = Analyte.FAIL
			libraryQcFailureMode.value = libraryQcFailureMode.controlledVocabulary[0]
		}
		libraryVolume = 10
		libraryConcentration = 10
		libraryActualTemplateSize = 10
        Analyte analyte = process.outputAnalytes.find{it.id == libraryLimsId}
		libraryCreationQueue = analyte.claritySample.libraryCreationQueue.libraryCreationQueue
        populateBeanRequiredFields()
	}

    void populateBeanRequiredFields(){
    }

    boolean getRequiresIndex(){
        return !libraryIndexName
    }

	void setIndexName(String indexName){
		libraryIndexName = indexName
	}
}
