package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.config.Stage
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcPlatePacBioTableBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.util.util.BeanUtil

/**
 * Created by tlpaley on 4/1/15.
 */
class LibraryCreationPlatePacBio extends LibraryCreation {

    final static String TEMPLATE_NAME = 'resources/templates/excel/LcPlatePacBio'
    private final static String TABLE_CLASS_NAME = LcPlatePacBioTableBean.class.simpleName

    LibraryCreationPlatePacBio(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputPlate(process)
    }

    @Override
    Stage getDefaultStage() {
        Stage.PLATE_TO_TUBE_TRANSFER
    }

    @Override
    String getLcTableBeanClassName() {
        TABLE_CLASS_NAME
    }

    @Override
    void updateLibraryStockUdfs() {
        transferBeansDataToClarityLibraryStocks()
        process.updateNonWorksheetUdfs()
    }

    void transferBeansDataToClarityLibraryStocks() {
        process.outputAnalytes[0].containerUdfNotes = process.resultsBean.lcPlateNotes
        process.outputAnalytes[0].containerUdfLibraryQcResult = process.resultsBean.plateResult?.value
        process.outputAnalytes[0].containerUdfLibraryQcFailureMode = process.resultsBean.failureMode?.value
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlatePacBioTableBean lcPlateTableBean = (LcPlatePacBioTableBean) process.getLcTableBean(clarityLibraryStock.id)
            clarityLibraryStock.udfConcentrationNgUl = lcPlateTableBean.libraryConcentration
            clarityLibraryStock.udfActualTemplateSizeBp = lcPlateTableBean.libraryTemplateSize
            clarityLibraryStock.udfVolumeUl = lcPlateTableBean.libraryVolume
            clarityLibraryStock.udfIndexName = lcPlateTableBean.libraryIndexName
            clarityLibraryStock.udfLibraryMolarityQcPm = lcPlateTableBean.libraryMolarity
            clarityLibraryStock.udfNumberPcrCycles = process.resultsBean.numberPcrCycles
            clarityLibraryStock.udfNotes = lcPlateTableBean.lcNotes
            clarityLibraryStock.udfLibraryQcResult = lcPlateTableBean.libraryQcResult?.value
            clarityLibraryStock.systemQcFlag = lcPlateTableBean.libraryQcResult?.value == Analyte.PASS
            clarityLibraryStock.udfLibraryQcFailureMode = lcPlateTableBean.libraryFailureMode?.value
        }
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return output.populateLibraryCreationSheet(TEMPLATE_NAME, getTableSectionLibraries())
    }

    @Override
    Section getTableSectionLibraries(){
        LibraryStockFailureModesService libraryStockFailureModesService = BeanUtil.getBean(LibraryStockFailureModesService.class)
        Section tableSection = new TableSection(0, LcPlatePacBioTableBean.class, 'A26', 'L122')
        def tableBeansArray = []
        process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
            LcPlatePacBioTableBean bean = new LcPlatePacBioTableBean()
            bean.well = clarityLibraryStock.artifactNode.location?.replaceAll(':','')
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryName = clarityLibraryStock.name
            bean.aliquotMass = clarityLibraryStock.parentAnalyte?.massNg
            bean.libraryQcResult = libraryStockFailureModesService.dropDownPassFail
            bean.libraryFailureMode = libraryStockFailureModesService.dropDownFailureModes
            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    void moveToNextWorkflow() {
        output.moveOutputToNextWorkflow(process.outputAnalytes)
    }

    @Override
    void updateLcaUdfs() {
        def libraryQcResult = process.outputAnalytes[0].containerUdfLibraryQcResult
        process.outputAnalytes.each{ ClarityLibraryStock output ->
            ScheduledSample scheduledSample = (ScheduledSample)output.claritySample
            scheduledSample.incrementUdfLcAttempt()
            if (libraryQcResult == Analyte.FAIL) {
                scheduledSample.incrementUdfLcFailedAttempt()
            }
        }
    }

    String getTemplate(){
        return TEMPLATE_NAME
    }

    @Override
    void validateProcessUdfs() {
        def processUdfSmrtbellTemplatePrepKit = (process.outputAnalytes[0] as ClarityLibraryStock).udfSmrtbellTemplatePrepKit
        def processUdfPacBioSmrtbellTemplatePrepKit = (process.outputAnalytes[0] as ClarityLibraryStock).udfPacBioSmrtbellTemplatePrepKit
        if (!processUdfSmrtbellTemplatePrepKit && !processUdfPacBioSmrtbellTemplatePrepKit)
            process.postErrorMessage("""
Unable to Complete Work 
Please specify the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf.
""")
        if (processUdfSmrtbellTemplatePrepKit && processUdfPacBioSmrtbellTemplatePrepKit)
            process.postErrorMessage("""
Unable to Complete Work 
Both udfs were specified the $ClarityUdf.PROCESS_SMRTBELL_TEMPLATE_PREP_KIT or $ClarityUdf.PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT udf.
""")
    }

}
