package gov.doe.jgi.pi.pps.clarity.scripts.pacbio_sequencing_complete

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode

/**
 *  This process is meant to notify that the PacBio Sequencing is done for a set of PacBio libraries
 *  This process is run on the PacBio binding complexes
 *  This process produces no analyte outputs
 *  Requirements doc https://docs.google.com/document/d/1ZMNLq302W-oUqC-w8cqT7l7B3V-If4LVxcbM68P3vvQ/edit
 *
 */

class PacBioSequencingCompleteProcess extends ClarityProcess {

    static ProcessType processType = ProcessType.PACBIO_SEQUENCING_COMPLETE

    boolean testMode = false

    PacBioSequencingCompleteProcess(ProcessNode processNode) {
        super(processNode)
        actionHandlers = [
                'PreProcessValidation': PbCompletePreProcessValidation,
                'Process Sequencing Complete': PbSequencingComplete,
                'Route to Next Workflow': PbCompleteRouteToWorkflow
        ]
    }
}
