package gov.doe.jgi.pi.pps.clarity.scripts.sequencing.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityPhysicalRunUnit
import groovy.transform.ToString

/**
 * Auto-generated on Thu Apr 30 14:44:45 PDT 2015
 */
@ToString(includeNames=true, includeFields=true)
class NovaSeqTableBean {

    @FieldMapping(header="Library Stock Name", cellType= CellTypeEnum.STRING)
    public String libraryStockName

    @FieldMapping(header="Container Barcode", cellType= CellTypeEnum.STRING)
    public String containerBarcode

    @FieldMapping(header="Freezer Path", cellType= CellTypeEnum.STRING)
    public String freezerPath

    @FieldMapping(header="Run Type", cellType= CellTypeEnum.STRING)
    public String runType

    @FieldMapping(header="Material Type", cellType= CellTypeEnum.STRING)
    public String materialType

    @FieldMapping(header="PhiX Spike in %", cellType= CellTypeEnum.NUMBER)
    public BigDecimal phiXSpikein

    @FieldMapping(header="Conc (pM) from qPCR", cellType= CellTypeEnum.NUMBER)
    public BigDecimal concpMfromqPCR

    @FieldMapping(header="Loading Conc (pM)", cellType= CellTypeEnum.NUMBER)
    public def libraryConvFactor

    @FieldMapping(header="DNA WA01 (μL)", cellType= CellTypeEnum.FORMULA)
    public def dnaWa01Ul

    @FieldMapping(header="ExAmp Master Mix", cellType= CellTypeEnum.NUMBER)
    public def exAmpMasterMix

    @FieldMapping(header="Loading vol of Final diln (uL)", cellType= CellTypeEnum.NUMBER)
    public BigDecimal loadingVolOfFinaldilnuL

    @FieldMapping(header="Lane", cellType= CellTypeEnum.NUMBER, isRowHeader = true)
    public def lane

    public static final String D_NA_WA01_L_ = 'dnaWa01Ul'

    String validateBean(){
        String libStockName = libraryStockName
        if (!libStockName) {
            return
        }

        if (!hasProperty(D_NA_WA01_L_)) {
            return 'Field "DNA WA01 (μL)" not found.'
        }

        if (phixSpikeIn == null) {
            return 'Field "PhiX Spike in %" must be set.'
        }

        if (dnaWa01Ul == null) {
            return 'DNA WA01 (µL) is invalid, check that Conc (pM) is populated'
        }

        if(libraryConvFactor == null)
            return 'Loading Conc (pM) is a required field'
    }

    def getPhixSpikeIn(){
        return phiXSpikein
    }

    void populateBean(Analyte input, ClarityPhysicalRunUnit output){
        lane = output.lane as Integer
        libraryStockName = input.name
        containerBarcode = input.containerName
        runType = input.udfRunMode
        materialType = output.claritySample.sequencingProject.udfMaterialCategory
        concpMfromqPCR = input.udfLibraryMolarityPm
    }

    String getLane(){
        return "${lane as BigInteger}".toString()
    }

    String setLaneString(){
        lane = "${lane as BigInteger}".toString()
    }

    void updatePhysicalRunUnit(ClarityPhysicalRunUnit physicalRunUnit){
        physicalRunUnit.setUdfPhixSpikeIn(phixSpikeIn)
        physicalRunUnit.setUdfLibraryConversionFactor(libraryConvFactor/1000)
    }

}
