package gov.doe.jgi.pi.pps.clarity.scripts.sample_fractionation.beans

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.CellTypeEnum
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.FieldMapping
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class DensityTableBean {
	@FieldMapping(header = 'Plate barcode', cellType = CellTypeEnum.STRING)
	public String plateBarcode
	@FieldMapping(header = 'Sample barcode', cellType = CellTypeEnum.STRING)
	public String sampleBarcode
	@FieldMapping(header = 'Well Pos', cellType = CellTypeEnum.STRING)
	public String wellPos
	@FieldMapping(header = 'Fraction #', cellType = CellTypeEnum.NUMBER)
	public BigDecimal fractionNumber
	@FieldMapping(header = 'Density', cellType = CellTypeEnum.FORMULA_NOT_IMPLEMENTED)
	public def density

	static final def newline = ClarityProcess.WINDOWS_NEWLINE

	def validate(List<String> inputBarcodes, Set<String> enteredBarcodes) {
		StringBuffer beanErrorMsg = new StringBuffer()
		if (density && !sampleBarcode) {
			beanErrorMsg << "unspecified Sample Barcode.$newline"
		}
		if (sampleBarcode && !(sampleBarcode in inputBarcodes)) {
			beanErrorMsg << "invalid Sample Barcode $sampleBarcode.$newline"
			beanErrorMsg << "Sample Barcode name should be in $inputBarcodes.$newline"
		}
		if (sampleBarcode && sampleBarcode in enteredBarcodes) {
			beanErrorMsg << "invalid Sample Barcode $sampleBarcode.$newline"
			beanErrorMsg << "Sample Barcode name is already used by an existing density file.$newline"
		}
		if (sampleBarcode && !density || sampleBarcode && density < 0) {
			beanErrorMsg << "invalid Density $density.$newline"
			beanErrorMsg << "Density value should be greater than zero.$newline"
		}
		if (beanErrorMsg) {
			return "Well Pos $wellPos: " << beanErrorMsg
		} else
			return null
	}

	static def validateEnteredBarcodes(List<String> inputBarcodes, Set<String> enteredBarcodes){
		List notUsed = inputBarcodes.findAll{ !(it in enteredBarcodes) }
		if (notUsed) {
			def filesErrorMsg = "Please correct the errors below and upload file(s) again." << newline
			filesErrorMsg << "Cannot find the following Sample Barcodes in density files:" << newline
			filesErrorMsg << notUsed << newline
			return filesErrorMsg
		}
		return null
	}
}