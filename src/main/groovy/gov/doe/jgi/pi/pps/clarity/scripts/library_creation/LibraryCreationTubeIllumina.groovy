package gov.doe.jgi.pi.pps.clarity.scripts.library_creation

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.ExcelWorkbook
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.Section
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.excel_framework.TableSection
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.freezer.FreezerContainer
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClarityLibraryStock
import gov.doe.jgi.pi.pps.clarity.model.analyte.ClaritySampleAliquot
import gov.doe.jgi.pi.pps.clarity.model.sample.ClaritySample
import gov.doe.jgi.pi.pps.clarity.scripts.library_creation.beans.LcTubeBean
import gov.doe.jgi.pi.pps.clarity.scripts.services.FreezerService
import gov.doe.jgi.pi.pps.clarity.scripts.services.LibraryStockFailureModesService
import gov.doe.jgi.pi.pps.util.util.BeanUtil

/**
 * Created by tlpaley on 4/1/15.
 */
class LibraryCreationTubeIllumina extends LibraryCreation {

    LibraryCreationTubeIllumina(LibraryCreationProcess process) {
        this.process = process
        this.output = new OutputTube(process)
    }

    @Override
    String getLcTableBeanClassName() {
        (output as OutputTube).TABLE_CLASS_NAME
    }

    @Override
    void updateLibraryStockUdfs() {
        transferBeansDataToClarityLibraryStocks()
        output.updateLibraryIndexUdf(process.outputAnalytes as List<ClarityLibraryStock>)
        process.updateNonWorksheetUdfs()
    }

    void transferBeansDataToClarityLibraryStocks() {
         process.outputAnalytes.each { ClarityLibraryStock clarityLibraryStock ->
             LcTubeBean lcTubeBean = (LcTubeBean) process.getLcTableBean(clarityLibraryStock.id)
             clarityLibraryStock.udfConcentrationNgUl = lcTubeBean.libraryConcentration
             clarityLibraryStock.udfActualTemplateSizeBp = lcTubeBean.libraryActualTemplateSize
             clarityLibraryStock.udfVolumeUl = lcTubeBean.libraryVolume
             clarityLibraryStock.udfIndexContainerBarcode = lcTubeBean.libraryIndexName
             clarityLibraryStock.udfActualInsertSizeKb = lcTubeBean.libraryActualInsertSize
             clarityLibraryStock.udfLibraryMolarityQcPm = lcTubeBean.libraryMolarityQc
             clarityLibraryStock.udfNumberPcrCycles = lcTubeBean.numberPcrCycles
             clarityLibraryStock.udfLibraryQcResult = lcTubeBean.libraryQcResult?.value
             clarityLibraryStock.systemQcFlag = lcTubeBean.libraryQcResult?.value == Analyte.PASS
             clarityLibraryStock.udfLibraryQcFailureMode = lcTubeBean.libraryQcFailureMode?.value
             clarityLibraryStock.udfNotes = lcTubeBean.lcNotes

             clarityLibraryStock.containerUdfLibraryQcResult = lcTubeBean.libraryQcResult?.value
             clarityLibraryStock.containerUdfLibraryQcFailureMode = lcTubeBean.libraryQcFailureMode?.value
         }
    }

    @Override
    Section getTableSectionLibraries() {
        LibraryStockFailureModesService libraryStockFailureModesService = BeanUtil.getBean(LibraryStockFailureModesService.class)
        FreezerService freezerService = BeanUtil.getBean(FreezerService.class)
        Section tableSection = new TableSection(0, LcTubeBean.class, 'A1', null, true)
        def tableBeansArray = []
        List<FreezerContainer> freezerContainers = freezerService.freezerLookupInputAnalytes(process)
        process.outputAnalytes.sort{it.name}.eachWithIndex { ClarityLibraryStock clarityLibraryStock, index ->
            ClaritySampleAliquot claritySampleAliquot = clarityLibraryStock.parentAnalyte
            def containerName = claritySampleAliquot.containerName
            ClaritySample claritySample = claritySampleAliquot.claritySample

            LcTubeBean bean = new LcTubeBean()
            bean.aliquotConcentration = claritySampleAliquot.udfConcentrationNgUl
            bean.aliquotVolume = claritySampleAliquot.udfVolumeUl
            bean.aliquotMass = claritySampleAliquot.massNg
            bean.aliquotContainerName = containerName // Label was "aq $containerName"
            bean.aliquotFreezerPath = process.getFreezerPath(freezerContainers.findAll{it.barcode == containerName})
            bean.aliquotName = claritySampleAliquot.name
            bean.counter = index + 1
            bean.libraryName = clarityLibraryStock.name
            bean.libraryLimsId = clarityLibraryStock.id
            bean.libraryCreationQueue = claritySampleAliquot.libraryCreationQueue?.libraryCreationQueue
            bean.dop = claritySample.udfDegreeOfPooling
            bean.lcInstructions = claritySample.udfLcInstructions
            bean.smNotes = claritySample.udfNotes
            bean.libraryQcResult = libraryStockFailureModesService.dropDownPassFail
            bean.libraryQcFailureMode = libraryStockFailureModesService.dropDownFailureModes

            tableBeansArray << bean
        }
        tableSection.setData(tableBeansArray)
        return tableSection
    }

    @Override
    ExcelWorkbook populateLibraryCreationSheet(){
        return (output as OutputTube).populateLibraryCreationSheet(getTableSectionLibraries())
    }

    @Override
    void moveToNextWorkflow() {
        output.moveOutputToNextWorkflow(process.outputAnalytes as List<ClarityLibraryStock>)
    }

    String getTemplate(){
        return OutputTube.TEMPLATE_NAME
    }

    int getTemplateIndex() {
        return 1
    }
}
