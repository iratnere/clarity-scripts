package gov.doe.jgi.pi.pps.clarity.scripts.pacbiosequencing

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioBindingComplex
import gov.doe.jgi.pi.pps.clarity.model.analyte.PacBioSequencingTemplate
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.util.exception.WebException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by datjandra on 9/16/2015.
 */
class PbPreProcessValidation extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(PbPreProcessValidation.class)

    void execute() {
        logger.info "Starting ${this.class.name} action...."
        if (!process.isPacBioSequel) {
            throw new WebException("no handler for input stage ${process.inputStage} of process ${process}",422)
        }
        process.inputAnalytes.each { Analyte inputAnalyte ->
            if (!inputAnalyte instanceof PacBioBindingComplex) {
                process.postErrorMessage("$inputAnalyte is not a PacBio binding complex")
            }

            PacBioBindingComplex bindingComplex = inputAnalyte as PacBioBindingComplex
            if (bindingComplex.udfVolumeUl == null) {
                process.postErrorMessage("Volume required for $bindingComplex")
            }

            if (bindingComplex.udfPacBioMolarityNm == null) {
                process.postErrorMessage("PacBio molarity required for $bindingComplex")
            }
        }

        process.outputAnalytes.each { Analyte outputAnalyte ->
            PacBioSequencingTemplate sequencingTemplate = outputAnalyte as PacBioSequencingTemplate
            sequencingTemplate.setAcqusitionTime(sequencingTemplate.getAcquisitionTime())
        }
    }
}
