package gov.doe.jgi.pi.pps.clarity.scripts.sample_qc

import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96
import gov.doe.jgi.pi.pps.clarity.welladdress.WellAddress96Iterator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SampleQcValidatePlacements  extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(SampleQcValidatePlacements.class)

    void execute(){
        logger.info "Starting ${this.class.name} action...."
        validatePlacement()
    }

    void validatePlacement(List<Analyte> analytes = process.sortedInputAnalytes){
        String currentContainerId = null
        Iterator<WellAddress96> wellIterator = null
        Map<String,Integer> map = [:].withDefault {0}
        analytes.each{ Analyte analyte ->
            Analyte outputAnalyte = process.getOutputAnalytes(analyte).first()
            map[outputAnalyte.containerId]++
            logger.info "Validating placement for $outputAnalyte"
            if(currentContainerId != outputAnalyte.containerId){
                if(currentContainerId > outputAnalyte.containerId)
                    process.postErrorMessage("Output placement not as per placement rules. Please follow the described rules to correct the placement.\nTubes first sorted by PMO Sample Ids, followed by plates sorted by container name and by column within each plate")
                wellIterator = new WellAddress96Iterator(skipCorners:true, columnsFirst:true)
            }
            currentContainerId = outputAnalyte.containerId
            String error = validateLocation(outputAnalyte, wellIterator.next())
            if(error)
                process.postErrorMessage(error)
        }
        String error = validateContainers(map)
        if(error)
            process.postErrorMessage(error)
    }

    String validateContainers(Map<String,List<Analyte>> map) {
        if(map.size() > 7)
            return "Please do not create more than 7 output plates"
        String error = ""
        List<String> containers = map.keySet().sort{a,b -> a<=>b} as List
        for(int i=0;i<containers.size()-1;i++) {
            if((map[containers[i]]+map[containers[i+1]]) <= 92) {
                error += "Analytes from plate ${containers[i]} and ${containers[i + 1]} can be combined together into a single plate.\n"
                i++
            }
        }
        return error
    }

    String validateLocation(Analyte outputAnalyte, WellAddress wellAddress){
        if(outputAnalyte.containerLocation.wellLocation != wellAddress.wellName)
            return "$outputAnalyte expected at location ${wellAddress.wellName}.\nOutput placement not as per placement rules. Please follow the described rules to correct the placement.\nTubes first sorted by PMO Sample Ids, followed by plates sorted by container name and by column within each plate"
        return ""
    }
}
