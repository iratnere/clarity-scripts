package gov.doe.jgi.pi.pps.clarity.scripts.sample_receipt.notification

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailDetails
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.email_notification.EmailNotification
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte

/**
 * Created by lvishwas on 9/19/16.
 */
class SampleFailedReceiptEmailNotification extends EmailNotification{
    static final String SUBJECT = 'Sample failed in receipt'
    static final String TEMPLATE_FILE_NAME = 'sample_failed'

    @Override
    protected getToList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected getCcList(List<EmailDetails> emailDetailsList) {
        return ''
    }

    @Override
    protected String getSubjectLine(List<EmailDetails> emailDetailsList) {
        return SUBJECT
    }

    @Override
    protected getFromList(List<EmailDetails> emailDetailsList) {
        return emailDetailsList.first().sequencingProjectManagerId
    }

    @Override
    protected List<EmailDetails> buildEmailDetails(List<Analyte> analytes) {
        return analytes.collect{new SampleFailedReceiptEmailDetails(it)}
    }

    @Override
    protected getBinding(List<EmailDetails> emailDetailsList) {
        String endPointUrl = getDashboardIndexUrl()
        return [contact:emailDetailsList.first().sequencingProjectManagerName, link:endPointUrl, samples: getSampleList(emailDetailsList)]
    }

    @Override
    protected String getTemplateFileName() {
        return TEMPLATE_FILE_NAME
    }
}
