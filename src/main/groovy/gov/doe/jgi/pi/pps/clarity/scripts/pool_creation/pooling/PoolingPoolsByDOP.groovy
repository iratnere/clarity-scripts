package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.BasicPoolingAlgorithm
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation

class PoolingPoolsByDOP extends BasicPoolingAlgorithm {
    int dop

    PoolingPoolsByDOP(List<LibraryInformation> members) {
        super(members)
    }

    @Override
    boolean getCanPool(){
        return false
    }

    @Override
    def getIndexToCompare(LibraryInformation member){
        return member.indexSequences //PPS-5253: Users can pool pools by DOP
    }
}