package gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.adapter

import gov.doe.jgi.pi.pps.clarity.cv.SowItemStatusCv
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.sample.PmoSample
import gov.doe.jgi.pi.pps.clarity.model.sample.ScheduledSample
import gov.doe.jgi.pi.pps.clarity.scripts.services.StatusService
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.RecordDetailsInfo
import gov.doe.jgi.pi.pps.clarity.scripts.sow_item_qc.SowItemQc
import gov.doe.jgi.pi.pps.clarity.util.RoutingRequest
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.util.util.BeanUtil

abstract class SowQcAdapter {
    static final List<String> IGNORE_SOW_STATUS = [SowItemStatusCv.ABANDONED.value, SowItemStatusCv.COMPLETE.value, SowItemStatusCv.DELETED.value]
    List<Analyte> analytes
    Map<Long,String> sampleStatusMap = [:]
    List<Analyte> failedScheduledSamples = []
    List<RoutingRequest> routingRequests = []

    SowQcAdapter(List<Analyte> inputAnalytes) {
        analytes = inputAnalytes
    }

    /**
     * This method is supposed to identify is any of the non QC'ed scheduled samples with
     * status other than Abandoned, Complete, Deleted are missing from the inputs
     * @param inputScheduledSamples
     * @param unQCedScheduledSamples
     * @param sowStatus
     * @return
     */
    String checkForUnQCedInputs(NodeManager nodeManager = BeanUtil.nodeManager) {
        List<ScheduledSample> inputScheduledSamples = []
        Map<PmoSample, List<ScheduledSample>> unQCedScheduledSamples = [:]
        List<Long> unQCedSowIds = []
        analytes.each { Analyte analyte ->
            inputScheduledSamples << analyte.claritySample
            PmoSample rootSample = analyte.claritySample.pmoSample
            List<ScheduledSample> unQCedSS = rootSample.getScheduledSamples(nodeManager).findAll {
                !it.udfSowItemQcResult
            }
            unQCedScheduledSamples[rootSample] = unQCedSS
            unQCedSowIds.addAll(unQCedSS.collect { it.sowItemId })
        }
        Map<Long, String> sowStatus = getSOWStatus(unQCedSowIds)
        List<String> errors = []
        for (PmoSample pmoSample : unQCedScheduledSamples.keySet()) {
            unQCedScheduledSamples[pmoSample].removeAll(inputScheduledSamples)
            List<ScheduledSample> missingSS = unQCedScheduledSamples[pmoSample]
            if (!missingSS)
                continue
            missingSS = missingSS.findAll {!(sowStatus[it.sowItemId] in IGNORE_SOW_STATUS)}
            if (missingSS)
                errors << "Please select all the non qc-ed sow items for sample ${pmoSample.id}. Please add scheduled samples ${missingSS.collect{it.id}} to the process.\n"
        }
        return errors ? errors.join("\n") : ''
    }

    Map<Long, String> getSOWStatus(List<Long> sowItemIds) {
        if(!sowItemIds)
            return [:]
        StatusService service = BeanUtil.getBean(StatusService.class)
        return service.getSowItemStatusBatch(sowItemIds)
    }

    List<RoutingRequest> getRoutingRequests(RecordDetailsInfo recordDetailsInfo) {
        if(!routingRequests)
            updateStatusAndRouting(recordDetailsInfo)
        return routingRequests
    }

    List<Analyte> getFailedScheduledSamples(RecordDetailsInfo recordDetailsInfo) {
        if(!failedScheduledSamples)
            updateStatusAndRouting(recordDetailsInfo)
        return failedScheduledSamples
    }

    abstract String validateInputs()

    abstract String checkProcessUdfs(SowItemQc sowQcProcess)

    abstract void updateStatusAndRouting(RecordDetailsInfo recordDetailsInfo)
}
