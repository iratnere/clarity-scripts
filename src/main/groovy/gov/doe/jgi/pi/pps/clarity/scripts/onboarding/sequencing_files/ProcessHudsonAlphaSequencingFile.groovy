package gov.doe.jgi.pi.pps.clarity.scripts.onboarding.sequencing_files


import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity.scripts.onboarding.factory.OnboardingAdapter

/**
 * Created by lvishwas on 10/20/16.
 */
class ProcessHudsonAlphaSequencingFile  extends ActionHandler {

    void execute(){
        OnboardHudsonAlphaSequencingFiles hudsonAlphaSequencingFilesProcess = process as OnboardHudsonAlphaSequencingFiles
        OnboardingAdapter adapter = hudsonAlphaSequencingFilesProcess.getOnboardingAdapterInstance(hudsonAlphaSequencingFilesProcess.getOnboardingWorkbook(OnboardHudsonAlphaSequencingFiles.USER_SEQUENCING_WORSHEET))
        List<String> errors = adapter.validateOnboardingData()
        if(errors)
            process.postErrorMessage(errors.join("\n"))
        adapter.mergeDataToClarity()
        def destFileNode = process.processNode.outputResultFiles?.find { it.name.contains(OnboardHudsonAlphaSequencingFiles.SCRIPT_SEQUENCING_WORSHEET)}?.id
        adapter.archiveOnboardingResult(destFileNode)
        //recordUpdates(adapter)
    }

//    void recordUpdates(OnboardingAdapter adapter){
//        ClarityWebTransaction webTransaction = ClarityWebTransaction.currentTransaction
//        couchDbService = webTransaction.requireApplicationBean(CouchDbService.class)
//        if (!webTransaction.webTransactionRecorder) {
//            webTransaction.setWebTransactionRecorder(new ClarityWebTransactionSuccessRecorder())
//        } else if (webTransaction.webTransactionRecorder instanceof CouchdbWebTransactionRecorder) {
//            ((CouchdbWebTransactionRecorder) webTransaction.webTransactionRecorder).enabled = true
//        }
//        ClarityCouchdbService.CouchDatabase couchdb = ClarityCouchdbService.CouchDatabase.WEB_TRANSACTION
//        URL url = new URL(couchDbService.couchDbUrl(couchdb.name))
//        String submissionUri = "http://${url.host}:${url.port}/${couchdb.name}/${webTransaction.transactionId}"
//        NodeConfig nodeConfig = process.nodeConfig
//        String researcherId = new ResearcherUriIterator(nodeConfig,[ResearcherParameter.USER_NAME.setValue(nodeConfig.geneusUser)]).collect {
//            it.split('/')[-1]
//        }?.last()
//        Researcher researcher = process.researcher
//        webTransaction.submittedBy = researcher.contactId
//        webTransaction.jsonResponse.'researcher-contact-id' = researcher.contactId
//        webTransaction.jsonResponse.'researcher-username' = nodeConfig.geneusUser
//        webTransaction.jsonResponse.'researcher-url' = researcher.researcherNode.url
//        webTransaction.jsonResponse.'researcher-id' = researcherId
//        webTransaction.jsonResponse.'researcher-name' = researcher.fullName
//        webTransaction.jsonResponse.'analytes' = adapter.analytes?.collect{it.toString()}?.join(',')
//        webTransaction.jsonResponse.'action'= "Onboarding sequencing files"
//        webTransaction.jsonResponse.'processId'= adapter.action
//    }
}
