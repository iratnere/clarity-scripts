package gov.doe.jgi.pi.pps.clarity.model.analyte

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.domain.LibraryCreationQueueCv
import gov.doe.jgi.pi.pps.clarity.domain.SampleAliquotFailureModeCv
import gov.doe.jgi.pi.pps.clarity.model.sample.CustomAliquotScheduledSample
import gov.doe.jgi.pi.pps.clarity.util.LabelBean
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNodeInterface
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.pi.pps.util.exception.WebException
import groovy.transform.PackageScope

/**
 * Created by dscott on 4/8/2014.
 */
class ClaritySampleAliquot extends Analyte {

    static final String[] getActiveSampleAliquotFailureModes() {
        return SampleAliquotFailureModeCv.findAllWhere(active: "Y")?.collect{ it.failureMode }
    }
    static final String[] EXCLUDED_FAILURE_MODES = ['Abandoned Work']
    static final String[] getValidFailureModes() {
        return getActiveSampleAliquotFailureModes()?.minus(EXCLUDED_FAILURE_MODES)
    }

    static final String LIBRARY_CREATION_WORKFLOW_PREFIX = 'LC '

    @PackageScope
    ClaritySampleAliquot(ArtifactNodeInterface artifactNodeInterface) {
        super(artifactNodeInterface)
    }

    SampleAnalyte getParentAnalyte() {
        ArtifactNodeInterface parentArtifactNode = artifactNodeInterface.singleParentAnalyte
        return (SampleAnalyte) AnalyteFactory.analyteInstance(parentArtifactNode)
    }

    String getContainerUdfLabResult() {
        return containerNode.getUdfAsString(ClarityUdf.CONTAINER_LAB_RESULT.udf)
    }

    void setContainerUdfLabResult(String value) {
        containerNode.setUdf(ClarityUdf.CONTAINER_LAB_RESULT.udf, value)
    }

    String getUdfFailureMode() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_FAILURE_MODE.udf)
    }

    void setUdfFailureMode(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_FAILURE_MODE.udf, value)
    }

    String getUdfLabResult() {
        return artifactNodeInterface.getUdfAsString(ClarityUdf.ANALYTE_LAB_RESULT.udf)
    }

    void setUdfLabResult(String value) {
        artifactNodeInterface.setUdf(ClarityUdf.ANALYTE_LAB_RESULT.udf, value)
    }

    @Override
    String getPrintLabelText() {
        if (isOnPlate) {
            def targetTemplateSize = claritySample.udfTargetTemplateSizeBp
            return new LabelBean(
                    pos_1_1: containerId,
                    pos_1_2: containerUdfLabel,
                    pos_1_4: "${targetTemplateSize != null ? targetTemplateSize : ''} ${claritySample.sequencingProject.udfMaterialCategory}",
                    pos_1_5: containerId
            ) as String
        }
        return null
    }
    /*
    	def (prefix, suffix) = plateMap
		def tfs = targetFragmentSize
		String tfsMaterial = "${tfs != null ? tfs : ''} ${materialType}".trim()
		return "${artifactNode.containerNode.id},${prefix},${suffix},${tfsMaterial},${artifactNode.containerNode.id},,,"
     */

    @Override
    LibraryCreationQueueCv getLibraryCreationQueue() {
        if (isCustomAliquot) {
            return null
        }
        boolean isPlate = artifactNodeInterface.containerNode.isPlate
        List<LibraryCreationQueueCv> queues = claritySample.libraryCreationQueuesFromSpecsId
        LibraryCreationQueueCv queue = queues.find{
            (isPlate && it.plateTargetMassLibTrialNg) || (!isPlate && it.tubeTargetMassLibTrialNg)
        }
        if (!queue) {
            def errorMsg = this.createQueueErrorMsg()
            throw new WebException([code: errorMsg, args: [this]], 422)
        }
        return queue
    }

    String createQueueErrorMsg() {
        boolean isPlate = artifactNodeInterface.containerNode.isPlate
        if (isPlate) {
            return "Cannot place sample <${claritySample.name}> on ${artifactNodeInterface.containerNode.containerType}. Please place sample in ${ContainerTypes.TUBE.value}. "
        } else {
            return "Cannot place sample <${claritySample.name}> in ${artifactNodeInterface.containerNode.containerType}. Please place sample on ${ContainerTypes.WELL_PLATE_96.value}. "
        }
    }

    boolean getIsCustomAliquot() {
        claritySample instanceof CustomAliquotScheduledSample
    }

    String getLcWorkflowName() {
        return LIBRARY_CREATION_WORKFLOW_PREFIX + libraryCreationQueue?.libraryCreationQueue
    }
}
