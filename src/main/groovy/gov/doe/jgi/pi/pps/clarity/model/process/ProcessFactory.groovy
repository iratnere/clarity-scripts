package gov.doe.jgi.pi.pps.clarity.model.process

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.services.ProcessRegistrationService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import gov.doe.jgi.pi.pps.util.util.BeanUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.lang.reflect.Constructor

class ProcessFactory {

    static Logger logger = LoggerFactory.getLogger(ProcessFactory.class.name)

    private static getProcessRegistrationService() {
        ProcessRegistrationService processRegistrationService = BeanUtil.getBean(ProcessRegistrationService.class)
        processRegistrationService
    }

    private static ClarityProcess generateClarityProcess(ProcessNode processNode) {
        ProcessType processType = ProcessType.toEnum(processNode?.processType)
        if (processType) {
            Constructor<ClarityProcess> geneusProcessConstructor = processConstructor(processType)
            if (geneusProcessConstructor) {
                return geneusProcessConstructor.newInstance(processNode)
            }
        }
        return new ClarityProcess(processNode)
    }

    private static Constructor<ClarityProcess> processConstructor(ProcessType processType) {
        processRegistrationService.processConstructor(processType)
    }

    static ClarityProcess processInstance(ProcessNode processNode) {
        if (!processNode) {
            return null
        }
        if (!processNode.cache ) {
            processNode.cache = generateClarityProcess(processNode)
        }
        (ClarityProcess) processNode.cache
    }



}
