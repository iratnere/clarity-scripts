package gov.doe.jgi.pi.pps.clarity.scripts.sequencing

import gov.doe.jgi.pi.pps.clarity.config.ClarityUdf
import gov.doe.jgi.pi.pps.clarity.model.analyte.Analyte
import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler

/**
 * Created by tlpaley on 3/2/15.
 * Clarity Illumina MiSeq, NextSeq, HiSeq Support Requirements
 * https://docs.google.com/document/d/15g6IcLuFzb-1Q8LtFCdfukaT2f2l0m32j9b8yo3_51Q/edit#
 * Clarity Illumina NovaSeq Support Requirements
 * https://docs.google.com/document/d/1CmDCgNkQwyHQRqk4Gx3LgeWzNYiPuJ6X9X6BKPffq9w/edit
 * Clarity Illumina HiSeq 2000, 1T Requirements
 * https://docs.google.com/document/d/1DhaLc-e3zebpmqOYUSMp19CcsvrP5QPlB5iFlsZNgAs/edit#heading=h.7bnr80mxwz1b
 */
class SqPreProcessValidation extends ActionHandler {

    SequencerType sequencerType

    void execute() {
        validateInputRunModes()
        sequencerType = (process as Sequencing).sequencerType
        sequencerType.validateInputIsLibrary()

        sequencerType.validateNumberInputs()
        sequencerType.validateInputFlowcellType()
        validateInputVolume()
    }

    void validateInputRunModes() {
        def runModeCvs = process.inputAnalytes.collect{it.runModeCv}.unique()
        if (runModeCvs.size() > 1)
            process.postErrorMessage("All selected inputs must have the same '${ClarityUdf.ANALYTE_RUN_MODE.value}' udf.")
    }

    void validateInputVolume() {
        process.inputAnalytes.each{ validateUdfVolumeUl(it) } //Cannot be null
    }

    def validateUdfVolumeUl(Analyte analyte) {
        if (analyte.udfVolumeUl == null)
            process.postErrorMessage("$this: Invalid input '${ClarityUdf.ANALYTE_VOLUME_UL.value}' udf.")
    }

}
