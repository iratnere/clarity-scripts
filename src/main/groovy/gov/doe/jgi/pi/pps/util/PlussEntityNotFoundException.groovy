package gov.doe.jgi.pi.pps.util

class PlussEntityNotFoundException extends RuntimeException {
    PlussEntityNotFoundException(String message) {
        super(message)
    }
}