package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.pooling

import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.FractionalLanePooling
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.pooling.LibraryInformation
import gov.doe.jgi.pi.pps.clarity.jgi.scripts.rest.LaneFraction
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.ActionBean
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.CalculationsBean
import gov.doe.jgi.pi.pps.clarity.scripts.pool_creation.excel.PoolSummaryTableBean
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet

class NovaSeqFractionalLanePooling extends FractionalLanePooling{

    static final def REGULAR_CELL_ROW = 3 // == row 3 cell 11
    static final def REGULAR_CELL = 11

    Map<String, LaneFraction> libraryNameToFraction

    NovaSeqFractionalLanePooling(List<LibraryInformation> members, Map<String, LaneFraction> libNameToFraction) {
        super(members)
        libraryNameToFraction = libNameToFraction
    }

    @Override
    String getMessage() {
        return ""
    }

    @Override
    CellStyle getCellStyle(Sheet sheet) {
        def row = sheet.getRow(REGULAR_CELL_ROW)
        def cell = row.getCell(REGULAR_CELL)
        CellStyle cellStyle = cell.getCellStyle()
        return cellStyle
    }

    @Override
    List getActionBeans() {
        ActionBean bean = new ActionBean()
        bean.threshold = members.first().threshold
        return [bean]
    }

    @Override
    List getSummaryBeans() {
        def novaSeqPoolNumbers = members.collect{ it.poolNumber }.unique()
        return novaSeqPoolNumbers.collect{ new PoolSummaryTableBean(poolNumber: it) }
    }

    @Override
    List getCalculationBeans() {
        List<CalculationsBean> beans = members.collect{
            LaneFraction laneFraction = libraryNameToFraction[it.analyte.name]
            new CalculationsBean(
                    libraryName: it.analyte.name,
                    sowItemId: laneFraction.sowItemId,
                    numberOfReadsBp: laneFraction.numberOfReadsBp,
                    totalNumberOfReads: laneFraction.totalNumberOfReads,
                    sof: laneFraction.sequencingOptimizationFactor,
                    libraryPercentage: (laneFraction.laneFraction * 100)
            )
        }
        return beans
    }

    @Override
    Map validate() {
        Map<String,List<String>> findings = [:].withDefault {[]}
        Map<String, List<LibraryInformation>> repSequenceToBeans = indexMembersMap?.findAll { it.value.size() > 1 }
        if(repSequenceToBeans) {
            def details = getErrorMessage(repSequenceToBeans)
            findings[ERROR] << "pool #$poolNumber has duplicate indexes: ${ClarityProcess.WINDOWS_NEWLINE}$details"
        }
        //PPS-4699 Since NovaSeq pools are large (over 92 libraries) the probability of not respecting the dual color index rules is minimal and does not need to be enforced
        return findings
    }

    static String getErrorMessage(Map<String, List<LibraryInformation>> sequenceToBeans) {
        def errorMessage = '' << ''
        sequenceToBeans.each{ sequence, beans ->
            errorMessage.append("$sequence:${beans*.toErrorMessage()}${ClarityProcess.WINDOWS_NEWLINE}")
        }
        return errorMessage
    }

    @Override
    int getPoolNumber() {
        members.first().poolNumber
    }

    @Override
    def getIndexToCompare(LibraryInformation member) {
        return member.indexSequences //PPS-5253 - pooling Internal single cell pools with NovaSeq
    }
}
