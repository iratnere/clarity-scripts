package gov.doe.jgi.pi.pps.clarity.scripts.pool_creation

import gov.doe.jgi.pi.pps.clarity.model.process.ActionHandler
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This class is meant to auto pool libraries according to the uploaded pooling prep worksheet
 * for requirements refer 4c (i-v) of the Clarity Library Pooling Requirements document
 * https://docs.google.com/a/lbl.gov/document/d/1itCb6lXyP_IM0Fl-o-8tMDdsVq-WgdmekOmN53JMfg4/edit#
 *
 * Created of the ticket PPS-1769
 * Created by lvishwas on 12/18/2014.
 */
class LpProcessPoolPrepWorksheet extends ActionHandler {
    static final Logger logger = LoggerFactory.getLogger(LpProcessPoolPrepWorksheet.class)

    void execute(){
        logger.info "Starting ${this.class.simpleName} action..."
        PoolCreation poolCreation = process as PoolCreation
        if (poolCreation.actionBean?.poolingPrepAction == PoolCreation.PROCESS_ACTION_REPEAT) {
            poolCreation.threshold = poolCreation.actionBean?.threshold
            ArtifactNode fileNodeToUpload = fileNode
            poolCreation.uploadPoolingPrepSheet(poolCreation.sortedBeans, fileNodeToUpload)
            poolCreation.postErrorMessage("""
You have selected to '$PoolCreation.PROCESS_ACTION_REPEAT'.
Please download the '$fileNodeToUpload.name' worksheet.
""")
        }
        poolCreation.getPoolPrepMemberBeans(true)
    }

    ArtifactNode getFileNode() {
        ArtifactNode fileNode
        PoolCreation.FILE_NAMES.find { String name ->
            fileNode = process.processNode.outputResultFiles.find{ it.name.contains(name) && !it.fileNode }
        }
        if (fileNode && !fileNode.fileNode) {
            return fileNode
        }
        process.postErrorMessage("""
Cannot find an empty node to upload the PoolingPrep file.
Please select 'Done' or 'Abort' the process.
""")
        return null
    }
}
