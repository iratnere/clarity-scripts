package gov.doe.jgi.pi.pps.clarity.scripts.supply_chain_management

import gov.doe.jgi.pi.pps.clarity.model.analyte.ProcessType
import gov.doe.jgi.pi.pps.clarity.model.process.ClarityProcess
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.springframework.context.annotation.ComponentScan

@ComponentScan
class SmApproveForShipping extends ClarityProcess {

	static ProcessType processType = ProcessType.SM_APPROVE_FOR_SHIPPING

	SmApproveForShipping(ProcessNode processNode) {
		super(processNode)
		actionHandlers = [
			'ApproveForShippingPPV': ApproveForShippingPPV,
			'ApproveForShipping':ApproveForShipping,
			'RouteToNextStep': RouteToSampleReceipt
		]
	}

//	@PostConstruct
//	void register() {
//		assert processRegistrationService
//		processRegistrationService.registerProcessClass(SmApproveForShipping)
//	}
}
