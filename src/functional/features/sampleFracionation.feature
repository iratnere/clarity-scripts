Feature: sample Fracionation Process

  Background:


  Scenario: ProcessDensityFiles action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-911723",
    "A": "ProcessDensityFiles",
    "S": "http://localhost:9080/api/v2/processes/24-911723",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "tatiana paley",
            "username": "tpaley",
            "account-locked": false,
            "lims-id": "284",
            "last-name": "paley",
            "first-name": "tatiana",
            "last-first-name": "paley, tatiana",
            "contact-id": 5179
        },
        "action-handler": "ProcessDensityFiles",
        "process-class": "SampleFractionationProcess",
        "last-action": false
    },
    "last-action": false
}
    """



  Scenario: PrepareTransferSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964015",
    "A": "PrepareTransferSheet",
    "S": "http://localhost:9080/api/v2/processes/24-964015",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "PrepareTransferSheet",
        "process-class": "SampleFractionationProcess",
        "last-action": false
    },
    "last-action": false
}
    """


  Scenario: ProcessTransferSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-911723",
    "A": "ProcessTransferSheet",
    "S": "http://localhost:9080/api/v2/processes/24-911723",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
"""
{
    "response-data": {
        "researcher": {
            "full-name": "tatiana paley",
            "username": "tpaley",
            "account-locked": false,
            "lims-id": "284",
            "last-name": "paley",
            "first-name": "tatiana",
            "last-first-name": "paley, tatiana",
            "contact-id": 5179
        },
        "action-handler": "ProcessTransferSheet",
        "process-class": "SampleFractionationProcess",
        "last-action": false
    },
    "last-action": false
}
"""

  Scenario: RouteToNextWorkflow action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-911723",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-911723",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 500
    #message:The sample 229021 is not associated with a Metagenome SIP Source sequencing project

  Scenario: RouteToNextWorkflow action script fails
    When the client requests POST /register-process-execution with JSON:
    """
 {
  "options": {
    "P": "24-1069862",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1069862",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
 }
    """
    Then the status is 500
    #The FD name provided: 273128_4 is not unique across existing Final Deliverable Projects


