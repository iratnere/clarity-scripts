Feature: Library Creation

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067356",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1067356",
    "U": false,
    "L": "92-4369058"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LcPreProcessValidation",
        "process-class": "LibraryCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

    Scenario: Automatic Placement action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070059",
    "A": "Automatic Placement",
    "S": "http://localhost:9080/api/v2/processes/24-1070059",
    "U": false,
    "L": "92-4377767"
  },
  "arguments": []
}
"""
    Then the status is 500
      #Placements cannot be assigned once a step is completed.

  Scenario: PrepareLibraryCreationSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067356",
    "A": "PrepareLibraryCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1067356",
    "U": false,
    "L": "92-4369058"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LcPrepareLibraryCreationSheet",
        "process-class": "LibraryCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: LcProcessLibraryCreationSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1069819",
    "A": "PrepareLibraryCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1069819",
    "U": igor,
    "L": "92-4377327"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LcPrepareLibraryCreationSheet",
        "process-class": "LibraryCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: Print Labels action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1054636",
    "A": "Print Labels",
    "S": "http://localhost:9080/api/v2/processes/24-1054636",
    "U": false,
    "L": "92-4331916"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "Clarity Migration",
            "username": "Migration",
            "account-locked": false,
            "lims-id": "104",
            "last-name": "Migration",
            "first-name": "Clarity",
            "last-first-name": "Migration, Clarity",
            "contact-id": 2633
        },
        "action-handler": "AcPrintLabels",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: Print Fragment Analyzer Labels action script success
    #takes 80 secs
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-989556",
    "A": "Print Fragment Analyzer Labels",
    "S": "http://localhost:9080/api/v2/processes/24-989556",
    "U": false,
    "L": "92-4209411"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "tatiana paley",
            "username": "tpaley",
            "account-locked": false,
            "lims-id": "284",
            "last-name": "paley",
            "first-name": "tatiana",
            "last-first-name": "paley, tatiana",
            "contact-id": 5179
        },
        "action-handler": "LcPrintFragmentAnalyzerLabels",
        "process-class": "LibraryCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: Route to Next Workflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1054626",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-1054626",
    "U": false,
    "L": "92-4331831"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "LcRouteToWorkflow",
        "process-class": "LibraryCreationProcess",
        "last-action": true
    },
    "last-action": true
}
    """

