Feature: Sequencing

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070062",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1070062",
    "U": false,
    "L": "92-4377859"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SqPreProcessValidation",
        "process-class": "Sequencing",
        "last-action": false
    },
    "last-action": false
}
    """
    Scenario: PrepareDilutionSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070062",
    "A": "PrepareDilutionSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070062",
    "U": false,
    "L": "92-4377859"
  },
  "arguments": []
}

"""
      Then the status is 200
      And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SqPrepareDilutionSheet",
        "process-class": "Sequencing",
        "last-action": false
    },
    "last-action": false
}
    """
  @ignore
  Scenario: ProcessDilutionSheet action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
  """
{
  "options": {
    "P": "24-1070062",
    "A": "ProcessDilutionSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070062",
    "U": false,
    "L": "92-4377859"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SqProcessDilutionSheet",
        "process-class": "Sequencing",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: RouteToNextWorkflow action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
  """
{
  "options": {
    "P": "24-1070062",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1070062",
    "U": false,
    "L": "92-4377859"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SqRouteToWorkflow",
        "process-class": "Sequencing",
        "last-action": true
    },
    "last-action": true
}
    """
