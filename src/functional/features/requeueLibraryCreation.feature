Feature: Requeue Library Creation

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-209762",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-209762",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "yuko yoshinaga",
            "username": "yuko",
            "account-locked": false,
            "lims-id": "204",
            "last-name": "yoshinaga",
            "first-name": "yuko",
            "last-first-name": "yoshinaga, yuko",
            "contact-id": 6565
        },
        "action-handler": "AbandonWorkPreProcessValidation",
        "process-class": "AbandonWorkProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: Route to Next Workflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-209762",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-209762",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "yuko yoshinaga",
            "username": "yuko",
            "account-locked": false,
            "lims-id": "204",
            "last-name": "yoshinaga",
            "first-name": "yuko",
            "last-first-name": "yoshinaga, yuko",
            "contact-id": 6565
        },
        "action-handler": "AbandonWorkRouteToWorkflow",
        "process-class": "AbandonWorkProcess",
        "last-action": true
    },
    "last-action": true
}
    """
