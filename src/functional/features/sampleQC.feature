Feature: Sample QC

  Background:


  @ignore
  Scenario: PreProcessValidation action script success
    #Invalid input 272563 with current status Available For Use. Expected status:  Awaiting Sample QC
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SampleQcPreProcessValidation",
        "process-class": "SampleQcProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: PlaceOutputs action script success
    #Placements cannot be assigned once a step is complete
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "PlaceOutputs",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SampleQcPlaceOutputs",
        "process-class": "SampleQcProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: PrepareSampleQcSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "PrepareSampleQcSheet",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": "92-4386565"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "PrepareSampleQcSheet",
        "process-class": "SampleQcProcess",
        "last-action": false
    },
    "last-action": false
}
    """
    @ignore
    Scenario: CalculateHMWgDNA action script success
      #ActionHandler is not implemented
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "CalculateHMWgDNA",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """

    """
  @ignore
  Scenario: ProcessSampleQcSheet action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "ProcessSampleQcSheet",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": "92-4386565"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    """

  Scenario: ProcessSampleQcSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073115",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/steps/24-1073115",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SampleQcRouteToWorkflow",
        "process-class": "SampleQcProcess",
        "last-action": true
    },
    "last-action": true
  }
  """


