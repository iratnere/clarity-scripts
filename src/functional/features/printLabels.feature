Feature: Print Labels

  Background:


  Scenario: Process Sample Worksheet action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-910867",
    "A": "Process Sample Worksheet",
    "S": "http://localhost:9080/api/v2/processes/24-910867",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 503
    #com.jcraft.jsch.SftpException: No such file



  Scenario: Print Barcode action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-910867",
    "A": "Print Barcode",
    "S": "http://localhost:9080/api/v2/processes/24-910867",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 503
    #com.jcraft.jsch.SftpException: No such file

