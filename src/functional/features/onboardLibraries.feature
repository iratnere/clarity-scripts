Feature: Onboard Libraries

  Background:


  Scenario: ProcessLibraryFile action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1052966",
    "A": "ProcessLibraryFile",
    "S": "http://localhost:9080/api/v2/processes/24-1052966",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #Library stock not queued in any of the workflows [Library Quantification Workflow, PacBio Sequel Library Annealing]

  Scenario: FinishStep action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1052966",
    "A": "FinishStep",
    "S": "http://localhost:9080/api/v2/processes/24-1052966",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "FinishOnboardingStep",
        "process-class": "OnboardLibraries",
        "last-action": false
    },
    "last-action": false
}
    """
