Feature: Onboard Sequencing Files

  Background:


  Scenario: ProcessSequencingFile action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1052970",
    "A": "ProcessSequencingFile",
    "S": "http://localhost:9080/api/v2/processes/24-1052970",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "ProcessHudsonAlphaSequencingFile",
        "process-class": "OnboardHudsonAlphaSequencingFiles",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: FinishStep action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1052970",
    "A": "FinishStep",
    "S": "http://localhost:9080/api/v2/processes/24-1052970",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "FinishStep",
        "process-class": "OnboardHudsonAlphaSequencingFiles",
        "last-action": false
    },
    "last-action": false
}
    """
