Feature: Supply chain management
  Background:


  Scenario: supply management action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-994552",
    "A": "ApproveForShippingPPV",
    "S": "http://localhost:9080/api/v2/processes/24-994552",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
  "response-data": {
    "researcher": {
      "full-name": "Selenium Webdriver",
      "username": "web",
      "last-name": "Webdriver",
      "first-name": "Selenium",
      "last-first-name": "Webdriver, Selenium",
      "account-locked": false,
      "lims-id": "9954",
      "contact-id": 3234
    },
    "action-handler": "ApproveForShippingPPV",
    "process-class": "SmApproveForShipping",
    "last-action": false
  },
  "last-action": false
}
    """
