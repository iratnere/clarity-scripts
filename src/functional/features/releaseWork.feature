Feature: Release Work

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1069868",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1069868",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ReleaseWorkPreProcessValidation",
        "process-class": "ReleaseWorkProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: Mark Protocol as Complete action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1069868",
    "A": "Mark Protocol as Complete",
    "S": "http://localhost:9080/api/v2/processes/24-1069868",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ReleaseWorkRecordDetailsProcess",
        "process-class": "ReleaseWorkProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: Route to Next Workflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1069868",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-1069868",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ReleaseWorkRouteToWorkflow",
        "process-class": "ReleaseWorkProcess",
        "last-action": true
    },
    "last-action": true
}
    """
