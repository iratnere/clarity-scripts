Feature: PacBio Library Annealing

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073122",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1073122",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LaPreProcessValidation",
        "process-class": "LibraryAnnealing",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: LibraryAnnealing action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073122",
    "A": "LibraryAnnealing",
    "S": "http://localhost:9080/api/v2/processes/24-1073122",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500

  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073122",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1073122",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #can't re-run due to use of cachedInputStage in ClarityProcess

