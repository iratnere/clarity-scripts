Feature: PacBio Library Binding

  Background:

  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073123",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1073123",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "contact-id": 149,
            "lims-id": "220",
            "account-locked": false,
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor"
        },
        "action-handler": "LbPreProcessValidation",
        "process-class": "LibraryBinding",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: LibraryBinding action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073123",
    "A": "LibraryBinding",
    "S": "http://localhost:9080/api/v2/processes/24-1073123",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]

  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073123",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1073123",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #can't re-run due to use of cachedInputStage in ClarityProcess

