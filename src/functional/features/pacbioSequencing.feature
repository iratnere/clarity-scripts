Feature: Pacbio Sequencing

  Background:


  @ignore
  Scenario: PreProcessValidation action script success
    #no handler for input stage null of process script:PacBioSequencingPlateCreation
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073124",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1073124",
    "U": false,
    "L": "92-4386673"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "contact-id": 149,
            "lims-id": "220",
            "account-locked": false,
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor"
        },
        "action-handler": "LbPreProcessValidation",
        "process-class": "LibraryBinding",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: PrepareRunDesign action script success
    #no handler for input stage null of process script:PacBioSequencingPlateCreation
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073124",
    "A": "PrepareRunDesign",
    "S": "http://localhost:9080/api/v2/processes/24-1073124",
    "U": "APIUser",
    "L": "92-4386673"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    """

  @ignore
  Scenario: ProcessRunDesign action script success
    #no handler for input stage null of process script:PacBioSequencingPlateCreation
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073124",
    "A": "ProcessRunDesign",
    "S": "http://localhost:9080/api/v2/processes/24-1073124",
    "U": "APIUser",
    "L": "92-4386673"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    """

  @ignore
  Scenario: RouteToNextWorkflow action script success
    #can't re-run due to use of cachedInputStage in ClarityProcess
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073124",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1073124",
    "U": false,
    "L": "92-4386673"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    """

