Feature: Library QCPR

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070060",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1070060",
    "U": false,
    "L": "92-4377796"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LqPreProcessValidation",
        "process-class": "LibraryQpcr",
        "last-action": false
    },
    "last-action": false
}
    """
    Scenario: PrepareQpcrSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070060",
    "A": "PrepareQpcrSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070060",
    "U": false,
    "L": "92-4377796"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LqPrepareQpcrSheet",
        "process-class": "LibraryQpcr",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: ProcessQpcrSheet action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070060",
    "A": "ProcessQpcrSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070060",
    "U": false,
    "L": "92-4377796"
  },
  "arguments": []
}
  """
    Then the status is 200
    And the response should be JSON:
  """
  """
  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070060",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1070060",
    "U": false,
    "L": "92-4377796"
  },
  "arguments": []
}
  """
    Then the status is 200
    And the response should be JSON:
  """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LqRouteToNextWorkflow",
        "process-class": "LibraryQpcr",
        "last-action": true
    },
    "last-action": true
}
  """
