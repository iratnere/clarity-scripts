Feature: Sample Aliquot Creation

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067354",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4369054"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "AcPreProcessValidation",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: PrepareAliquotCreationSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067354",
    "A": "PrepareAliquotCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4369054"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
 {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "AcPrepareAliquotCreationSheet",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: ProcessAliquotCreationSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067354",
    "A": "ProcessAliquotCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4369054"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "AcProcessAliquotCreationSheet",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: Print Labels action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067354",
    "A": "Print Labels",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4369054"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "AcPrintLabels",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067354",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4369054"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "AcRouteToWorkflow",
        "process-class": "SampleAliquotCreationProcess",
        "last-action": true
    },
    "last-action": true
}
    """


