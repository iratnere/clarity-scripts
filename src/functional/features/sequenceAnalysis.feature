Feature: Sequence Analysis

  Background:


  Scenario: ProcessDensityFiles action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-903259",
    "A": "EnterRecordDetails",
    "S": "http://localhost:9080/api/v2/processes/24-903259",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
  """
  {
    "response-data": {
        "researcher": {
            "full-name": "mojgan amirebrahimi",
            "username": "mojgan",
            "account-locked": false,
            "lims-id": "266",
            "last-name": "amirebrahimi",
            "first-name": "mojgan",
            "last-first-name": "amirebrahimi, mojgan",
            "contact-id": 2643
        },
        "action-handler": "SqAnalysisEnterRecordDetails",
        "process-class": "SequenceAnalysis",
        "last-action": false
    },
    "last-action": false
}
  """



  Scenario: ExitRecordDetails action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-903259",
    "A": "ExitRecordDetails",
    "S": "http://localhost:9080/api/v2/processes/24-903259",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]

  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-903259",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-903259",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "mojgan amirebrahimi",
            "username": "mojgan",
            "account-locked": false,
            "lims-id": "266",
            "last-name": "amirebrahimi",
            "first-name": "mojgan",
            "last-first-name": "amirebrahimi, mojgan",
            "contact-id": 2643
        },
        "action-handler": "SqAnalysisRouteToWorkflow",
        "process-class": "SequenceAnalysis",
        "last-action": true
    },
    "last-action": true
}
    """
