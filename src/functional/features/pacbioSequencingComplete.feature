Feature: PacBio Sequencing Complete

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073125",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1073125",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "PbCompletePreProcessValidation",
        "process-class": "PacBioSequencingCompleteProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: Process Sequencing Complet action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073125",
    "A": "Process Sequencing Complete",
    "S": "http://localhost:9080/api/v2/processes/24-1073125",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    """

  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073125",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-1073125",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "PbCompleteRouteToWorkflow",
        "process-class": "PacBioSequencingCompleteProcess",
        "last-action": true
    },
    "last-action": true
}
    """

