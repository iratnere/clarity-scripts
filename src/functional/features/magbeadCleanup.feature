Feature: Magbead Cleanup

  Background:


  Scenario: PreProcessValidation on Hold Worksheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-908818",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/steps/24-908818",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "mi yan",
            "username": "mi",
            "account-locked": false,
            "lims-id": "213",
            "last-name": "yan",
            "first-name": "mi",
            "last-first-name": "yan, mi",
            "contact-id": 25437
        },
        "action-handler": "McPreProcessValidation",
        "process-class": "MagbeadCleanup",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: PrepareMagbeadCleanupSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-908818",
    "A": "PrepareMagbeadCleanupSheet",
    "S": "http://localhost:9080/api/v2/steps/24-908818",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "mi yan",
            "username": "mi",
            "account-locked": false,
            "lims-id": "213",
            "last-name": "yan",
            "first-name": "mi",
            "last-first-name": "yan, mi",
            "contact-id": 25437
        },
        "action-handler": "McPrepareMagbeadCleanupSheet",
        "process-class": "MagbeadCleanup",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: ProcessMagbeadCleanupSheet action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-908818",
    "A": "ProcessMagbeadCleanupSheet",
    "S": "http://localhost:9080/api/v2/steps/24-908818",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 500
    #Upload MagBeadCleanup.xls was not attached

  Scenario: Route to Next Workflow action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-977715",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/steps/24-977715",
    "U": false,
    "L": false
  },
  "arguments": []
}

"""
    Then the status is 500
    #com.jcraft.jsch.SftpException: No such file
