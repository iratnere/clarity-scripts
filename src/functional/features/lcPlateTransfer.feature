Feature: LC Plate Transfer

  Background:


  Scenario: PreparePoolingSheet on Hold Worksheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-977715",
    "A": "PreparePoolingSheet",
    "S": "http://localhost:9080/api/v2/steps/24-977715",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Clarity Migration",
            "username": "Migration",
            "account-locked": false,
            "lims-id": "104",
            "last-name": "Migration",
            "first-name": "Clarity",
            "last-first-name": "Migration, Clarity",
            "contact-id": 2633
        },
        "action-handler": "LcPlateTransferPreparePoolingSheet",
        "process-class": "LcPlateTransferProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: ProcessPoolingSheet action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-977715",
    "A": "ProcessPoolingSheet",
    "S": "http://localhost:9080/api/v2/steps/24-977715",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 500
    #artifact [92-4178722] does not have an attached file


  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-977715",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/steps/24-977715",
    "U": false,
    "L": false
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Clarity Migration",
            "username": "Migration",
            "account-locked": false,
            "lims-id": "104",
            "last-name": "Migration",
            "first-name": "Clarity",
            "last-first-name": "Migration, Clarity",
            "contact-id": 2633
        },
        "action-handler": "LcPlateTransferRouteToWorkflow",
        "process-class": "LcPlateTransferProcess",
        "last-action": true
    },
    "last-action": true
}
    """
