Feature: sample leReceit

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073087",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1073087",
    "U": "APIUser",
    "L": "92-4386286"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SampleReceiptPPV",
        "process-class": "SampleReceipt",
        "last-action": false
    },
    "last-action": false
}
    """
    @ignore
    Scenario: ValidateBarcodeAssociation action script success
      #Invalid input 272559 with current status Awaiting Sample QC. Expected status:  Awaiting Sample Receipt
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073087",
    "A": "ValidateBarcodeAssociation",
    "S": "http://localhost:9080/api/v2/processes/24-1073087",
    "U": "APIUser",
    "L": "92-4386286"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ValidateBarcodeAssociation",
        "process-class": "SampleReceipt",
        "last-action": false
    },
    "last-action": false
}
    """

  @ignore
  Scenario: RouteReceivedSamples action script success
    #the samples are not queued for this step
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1073087",
    "A": "RouteReceivedSamples",
    "S": "http://localhost:9080/api/v2/processes/24-1073087",
    "U": "APIUser",
    "L": "92-4386286"
  },
  "arguments": []
}
  """
    Then the status is 200
    And the response should be JSON:
  """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ReceiveSamples",
        "process-class": "SampleReceipt",
        "last-action": true
    },
    "last-action": true
}
  """
