Feature: Approve For Shipping
  Background:


  Scenario: ApproveForShippingPPV action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067355",
    "A": "ApproveForShippingPPV",
    "S": "http://localhost:9080/api/v2/processes/24-1067355",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "lims-id": "220",
            "account-locked": false,
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ApproveForShippingPPV",
        "process-class": "SmApproveForShipping",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: ApproveForShipping action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067355",
    "A": "ApproveForShipping",
    "S": "http://localhost:9080/api/v2/processes/24-1067355",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "last-name": "rattner",
            "first-name": "igor",
            "lims-id": "220",
            "account-locked": false,
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "ApproveForShipping",
        "process-class": "SmApproveForShipping",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: RouteToNextStep action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1067353",
    "A": "RouteToNextStep",
    "S": "http://localhost:9080/api/v2/processes/24-1067353",
    "U": "APIUser",
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "RouteToSampleReceipt",
        "process-class": "SmApproveForShipping",
        "last-action": true
    },
    "last-action": true
}
    """
