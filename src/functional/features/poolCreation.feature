Feature: Pool Creation

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": false
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LpPreProcessValidation",
        "process-class": "PoolCreation",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: PreparePoolingPrepSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "PreparePoolingPrepSheet",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LpPreparePoolingPrepSheet",
        "process-class": "PoolCreation",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: ProcessPoolingPrepSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "ProcessPoolingPrepSheet",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": "92-4377798"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LpProcessPoolPrepWorksheet",
        "process-class": "PoolCreation",
        "last-action": false
    },
    "last-action": false
}
    """
  @ignore
  Scenario: PoolInputs action script success
    #Pools cannot be updated for this step because it is beyond the pooling status.
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "PoolInputs",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": "92-4377798"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """

    """

  Scenario: PreparePoolCreationSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "PreparePoolCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": false
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LpPreparePoolCreationSheet",
        "process-class": "PoolCreation",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: PrintLabels action script success
    When the client requests POST /register-process-execution with JSON:
 """
  {
  "options": {
    "P": "122-1070061",
    "A": "PrintLabels",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": "APIUser",
    "L": "92-4377798"
  },
  "arguments": []
}
  """
  Then the status is 200
  And the response should be JSON:
  """
  {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "PrintLabels",
        "process-class": "PoolCreation",
        "last-action": false
    },
    "last-action": false
}
  """
  @ignore
  Scenario: ProcessPoolCreationSheet action script success
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "ProcessPoolCreationSheet",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": "92-4377798"
  },
  "arguments": []
}
  """
    Then the status is 200
    And the response should be JSON:
  """
  """
  Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "122-1070061",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/122-1070061",
    "U": false,
    "L": false
  },
  "arguments": []
}
  """
    Then the status is 200
    And the response should be JSON:
  """
{
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "LpRouteToWorkflow",
        "process-class": "PoolCreation",
        "last-action": true
    },
    "last-action": true
}
  """
