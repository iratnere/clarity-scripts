Feature: Size Selection

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-959558",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-959558",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
  """
  {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "SizeSelectionPreProcessValidation",
        "process-class": "SizeSelectionProcess",
        "last-action": false
    },
    "last-action": false
}
  """



  Scenario: Process Size Selection action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-959558",
    "A": "Process Size Selection",
    "S": "http://localhost:9080/api/v2/processes/24-959558",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]

  Scenario: Route to Next Workflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-959558",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-959558",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "SizeSelectionRouteToWorkflow",
        "process-class": "SizeSelectionProcess",
        "last-action": true
    },
    "last-action": true
}
    """
