Feature: Metabolomics QC Process

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #All samples of the group Group 12/28/2019 11:24:43.751 should be QC'ed together



  Scenario: PlaceOutputs action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "PlaceOutputs",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #Placements cannot be assigned once a step is completed


  Scenario: PrepareWorksheets action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "PrepareWorksheets",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "PrepareAliquotCreationMetabolomicsSheets",
        "process-class": "MetabolomicsQCProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: PrintLabels action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "PrintLabels",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "PrintLabels",
        "process-class": "MetabolomicsQCProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: ProcessWorksheets action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "ProcessWorksheets",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "ProcessMetabolomicsSheet",
        "process-class": "MetabolomicsQCProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: ProcessRecordDetails action script fails
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "ProcessRecordDetails",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 500
    #You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps]

  Scenario: RouteToFractionation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-964014",
    "A": "RouteToFractionation",
    "S": "http://localhost:9080/api/v2/processes/24-964014",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "Lakshmi Vishwas",
            "username": "lv",
            "account-locked": false,
            "lims-id": "354",
            "last-name": "Vishwas",
            "first-name": "Lakshmi",
            "last-first-name": "Vishwas, Lakshmi",
            "contact-id": 17828
        },
        "action-handler": "RouteToFractionation",
        "process-class": "MetabolomicsQCProcess",
        "last-action": false
    },
    "last-action": false
}
    """


