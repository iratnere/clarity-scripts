Feature: Sow Item QC

  Background:


  Scenario: PreProcessValidation action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070057",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1070057",
    "U": false,
    "L": "92-4377697"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SowItemQcPpv",
        "process-class": "SowItemQc",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: PrepareSowItemQcSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070057",
    "A": "PrepareSowItemQcSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070057",
    "U": false,
    "L": "92-4377697"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "PrepareSowItemQcSheet",
        "process-class": "SowItemQc",
        "last-action": false
    },
    "last-action": false
}
    """
  @ignore
  Scenario: ProcessSowItemQcSheet action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070057",
    "A": "ProcessSowItemQcSheet",
    "S": "http://localhost:9080/api/v2/processes/24-1070057",
    "U": false,
    "L": "92-4377697"
  },
  "arguments": []
}
"""
    Then the status is 500
    And the response should be JSON:
    """
    {
    "timestamp": 1589584032834,
    "status": 500,
    "error": "Internal Server Error",
    "message": "http call on https://clarity-dev01.jgi.doe.gov/api/v2/steps/24-1070057/actions failed with response code 400: message=[Bad Request] content=[<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<exc:exception xmlns:exc=\"http://genologics.com/ri/exception\">\n    <message>You cannot change the step actions because one or more of them are not in [Record Details, Assign Next Steps].</message>\n</exc:exception>\n]",
    "path": "/register-process-execution"
    }
    """

    Scenario: RouteToNextWorkflow action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1070057",
    "A": "RouteToNextWorkflow",
    "S": "http://localhost:9080/api/v2/processes/24-1067354",
    "U": false,
    "L": "92-4377697"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "SowItemQcRouteToWorkflow",
        "process-class": "SowItemQc",
        "last-action": true
    },
    "last-action": true
}
    """


