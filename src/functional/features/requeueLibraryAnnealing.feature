Feature: Requeue Library Annealing

  Background:


  Scenario: PreProcessValidation action script success
  When the client requests POST /register-process-execution with JSON:
 """
 {
  "options": {
    "P": "24-1069864",
    "A": "PreProcessValidation",
    "S": "http://localhost:9080/api/v2/processes/24-1069864",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
"""
    Then the status is 200
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "RequeueLibraryAnnealingPreProcessValidation",
        "process-class": "RequeueLibraryAnnealingProcess",
        "last-action": false
    },
    "last-action": false
}
    """

  Scenario: Process Requeue action script success
    When the client requests POST /register-process-execution with JSON:
 """
{
  "options": {
    "P": "24-1069864",
    "A": "Process Requeue",
    "S": "http://localhost:9080/api/v2/processes/24-1069864",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}

"""
    Then the status is 200
    And the response should be JSON:
    """
   {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "RequeueLibraryAnnealing",
        "process-class": "RequeueLibraryAnnealingProcess",
        "last-action": false
    },
    "last-action": false
}
    """
  Scenario: Route to Next Workflow action script success
    When the client requests POST /register-process-execution with JSON:
    """
    {
  "options": {
    "P": "24-1069864",
    "A": "Route to Next Workflow",
    "S": "http://localhost:9080/api/v2/processes/24-1069864",
    "U": false,
    "L": "{compoundOutputFileLuid0}"
  },
  "arguments": []
}
    """
    And the response should be JSON:
    """
    {
    "response-data": {
        "researcher": {
            "full-name": "igor rattner",
            "username": "igor",
            "account-locked": false,
            "lims-id": "220",
            "last-name": "rattner",
            "first-name": "igor",
            "last-first-name": "rattner, igor",
            "contact-id": 149
        },
        "action-handler": "RequeueLibraryAnnealingRouteToWorkflow",
        "process-class": "RequeueLibraryAnnealingProcess",
        "last-action": true
    },
    "last-action": true
}
    """
