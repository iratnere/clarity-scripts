import support.Http
import wslite.http.HTTPClientException
import wslite.json.JSONObject

import static cucumber.api.groovy.EN.*
import static net.sf.json.JSONObject.fromObject
import static support.Http.*
//
//this.metaClass.mixin(cucumber.api.groovy.Hooks)
//this.metaClass.mixin(cucumber.api.groovy.EN)

client = Http.primeRestClient()
//
//When(~'^the client requests PUT /([^\\/]+)/(\\d+) with JSON:$') { String base, int id, String json ->
//    try {
//        response = putContent(client, base, id, new JSONObject(json))
//    } catch (HTTPClientException rce) {
//        response = rce.response
//    }
//}

Then(~'^the status is (\\d+)$') { int status ->
    assert response.statusCode == status
}

And(~'^the exception message is "([^"]*)"$') { String errorMessage ->
    assert response.contentAsString == errorMessage
}

And(~'^the response should be JSON:$') { json ->
    def respJson = response.contentAsString
    def resp = fromObject(respJson)
    def expected = fromObject(json)
    processJsonResponse(resp, expected)
    assert resp == expected
}

private void processJsonResponse(net.sf.json.JSONObject resp, net.sf.json.JSONObject expected) {
    def keys = expected.keys().findAll { def key ->
        def value = expected[key]
        value ==~ /<.+>/
    }
    if (keys) {
        def respKeys = resp.keys()
        keys.each { key ->
            def respKey = respKeys.find {
                it == key
            }
            if (respKey)
                expected[key] = resp[key]
        }
    }
}

When(~'^the client requests GET /(.+)/(.+)/(.+)$') { String base, String spId, String resource ->
    try {
        response = getProjectResources(client, base, spId, resource)
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}

And(~'^the exception message contains text "([^"]*)"$') { String errorMessage ->
    String resp = response.contentAsString
    assert resp.contains(errorMessage)
}

When(~'^the client requests POST /(.+) with JSON:$') { String base, String json ->
    try {
        response = postContent(client, base, new JSONObject(json))
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}

When(~'^the client requests PUT /(.+) with JSON:$') { String base, String json ->
    try {
        response = putContent(client, base, new JSONObject(json))
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}

When(~'^the client requests GET /([^/]+)$') { String path ->
    try {
        response = getResource(client, path)
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}


When(~'^the client requests GET /([^\\/]+)/(\\d+)$') { String path, Long id ->
    try {
        response = getResource(client, path, id)
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}

When(~'^the client requests GET /([^\\/]+)/([^\\/]+)$') { String path, String value ->
    try {
        response = getResource(client, path, value)
    } catch (HTTPClientException rce) {
        response = rce.response
    }
}

And(~'^the response is a list containing (.*) and (.*)$') { String baseType, String type -> //sequencing project id, sow items
    def respJson = response.contentAsString
    def resp = fromObject(respJson)

    def spId = resp[baseType.toLowerCase().replace(' ', '-')]
    assert spId
    def sowItems = resp[type.toLowerCase().replace(' ', '-')]
    assert sowItems?.size() > 0
}

And(~'^(.*) \\[id: (\\d+)\\] has the following attributes:$') { String propertyType, String sowId, json ->
    def respJson = response.contentAsString
    def resp = fromObject(respJson)
    resource = propertyType.toLowerCase().replace(' ', '-') //sow-item
    resourceIdName = resource.plus('-id') //sow-item-id
    def sowItems = resp[resource.plus('s')]

    def expectedItem = fromObject(json)
    def sowItem = sowItems.find{
        it[resourceIdName] as String == sowId
    }
    expectedItem.each{ key,value ->
        assert sowItem[key] == value
    }
}


